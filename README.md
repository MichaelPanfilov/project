# syntegon-shopfloor

## Table of Contents

- [syntegon-shopfloor](#syntegon-shopfloor)
  - [Table of Contents](#table-of-contents)
  - [Development environment: Docker shell](#development-environment-docker-shell)
  - [Development environment: VBOX](#development-environment-vbox)
    - [MySQL](#mysql)
    - [phpMyAdmin](#phpmyadmin)
    - [Notes](#notes)
  - [Environment variables](#environment-variables)

Place copies of this file into both `common` and `backend-service` folder next to the `package.json` file.

## Quickstart
- `npm run shell:build`
- `npm run shell`
- `npm i`
- `npm run migration:run`
- `npm run seed:roles`
- `npm run seed:initial-users`
- configure Azure IoTHub connection in ./datapoint-service/.env
- [init influx db](datapoint-service/README.md/#init-influxdb)
- `npm run dev`
- Browse to [localhost:4200](http://localhost:4200)
- Login with `syntegonAdmin` // `syntegon`


## Development environment: Docker shell

To drop into a shell inside a Docker container with the required services (MySQL) already started, run:

- `npm run shell`

Inside the shell, dependencies will have the correct versions.

## db schema & migrations
- `npm run migration:run` - create or update the db schema

## Seeding

Some initial data might be required to run the application:

- `npm run seed:roles` - create the required roles and rights for the application
- `npm run seed:initial-users` - create default users syntegonAdmin, shopUser, and shopAdmin, with the password "syntegon"

## Microservice landscape

The application consists of several typescript backend-services and an angular frontend:

- `acl-service`: `4070`: manages the access to the application with roles & rights
- `acl-service`: `4080`: manages files, qr-codes and file-previews
- `backend-service`: `3000`: business logic for actors, assets, instructions
- `datapoint-service`: `4040`: datapoint datalake

## Development environment: VBOX

- Make sure node v12+ is running: `nvm use 12`
- `npm i` in the shopfloor root folder
- create `.env` file for backend-service. see .env.example. `APP_PORT=3000`
- create `.env` file for acl-service. See .env.example. `APP_PORT=4070`
- acl-service: `npm run seed` to create basic users & roles
- backend-service: `npm run migration:run` to set up the database tables
- `npm run dev:backends` to start all backends
- frontend: set URLs to server.vbox in environments.ts
- start frontend via powershell in windows: `ng serve`

### MySQL

Also, two MySQL databases are available per service (e.g. `acl-db` and `acl-db-testing`). Environment variables for the access are set/passed
automatically to the main `app` container.

### phpMyAdmin

When inside the shell (when the `docker-compose` setup is `up`),
`phpMyAdmin` is reachable through port `8079`.

### Notes

Also available are:

- `npm run shell:build` builds the `sia` container in which the shell will be available.
  - This **must** be run once before `npm run shell`, also every time `Dockerfile.dev` has been updated
- `npm run shell:join` joins a running shell - `npm run shell` must have been run before. Useful if you need a second shell
  inside the dev container.
- `npm run shell:stop` when you exit the shell with `CTRL+C`, the `docker-compose` setup should be torn down
  automatically. If anything goes wrong (network not deleted, container still running and taking up ports, etc.), you can run this.
  It basically executes `docker-compose down`.

## Environment variables

In staging and production environments, a number of environment variables are required/available on the service containers.

These should be passed to the Docker container or used on the commandline. Please see the .env.example files in the microservice directories
