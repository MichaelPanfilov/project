import {createConnection} from "typeorm";
import { User } from "../src/actor/entities/user.entity";
import { ActorType } from "../../common/src/model";

(async () => {
    let users = [
        {
            name: 'syntegonAdmin',
            aclId: 'acl_user-6c654cc5-5402-4268-adba-0820cf61cd48'
        },
        {
            name: 'ShopfloorAdmin',
            aclId: 'acl_user-8736c921-ffcd-4ee1-be60-58a56890f4e5',
        },
        {
            name: 'ShopfloorUser',
            aclId: 'acl_user-cb9a4df1-9e27-4062-8e96-d425d09721d3',
        },
    ];

    const connection = await createConnection();
    let repo = connection.getRepository(User);
    
    await Promise.all(users.map(async user => {
        const createUser = new User;
        createUser.aclId = user.aclId;
        createUser.name = user.name;
        createUser.type = ActorType.USER;
        await repo.save(createUser);
    }));
    process.exit(0);
})();



