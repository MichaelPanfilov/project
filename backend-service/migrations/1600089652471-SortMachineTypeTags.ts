import { MigrationInterface, QueryRunner } from 'typeorm';
import { Task } from '../src/task/entities/task.entity';

export class SortMachineTypeTags1600089652471 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const repo = queryRunner.manager.getRepository(Task);
    const tasks = await repo
      .createQueryBuilder()
      .select()
      .where('machine_types IS NOT NULL')
      .getMany();

    const sortTaskTags = (tags?: string[]) => {
      return (
        tags &&
        tags.sort((a, b) => {
          if (a.toLowerCase() < b.toLowerCase()) {
            return -1;
          }
          if (a.toLowerCase() > b.toLowerCase()) {
            return 1;
          }
          return 0;
        })
      );
    };

    const toUpdate = tasks
      .filter(t => t.machineTypes && t.machineTypes.length)
      .map(t => ({ ...t, machineTypes: sortTaskTags(t.machineTypes) }));

    await repo.save(toUpdate);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
