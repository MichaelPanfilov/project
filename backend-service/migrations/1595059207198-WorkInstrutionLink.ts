import { MigrationInterface, QueryRunner } from 'typeorm';

export class WorkInstrutionLink1595059207198 implements MigrationInterface {
  name = 'WorkInstrutionLink1595059207198';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `instruction_link` DROP FOREIGN KEY `FK_09797cd8be1c62ee2154865b40c`',
    );
    await queryRunner.query(
      'ALTER TABLE `instruction_link` CHANGE `work_instruction_id_id` `instruction_id` varchar(255) NULL',
    );

    await queryRunner.query('CREATE TABLE `tmp_links` SELECT * FROM `instruction_link`');
    await queryRunner.query('TRUNCATE TABLE `instruction_link`');
    await queryRunner.query(
      'ALTER TABLE `instruction_link` ADD UNIQUE INDEX `IDX_36fd0c07800222a1bf5cfa2549` (`instruction_id`, `ref_id`)',
    );
    await queryRunner.query('INSERT IGNORE INTO `instruction_link` SELECT * from `tmp_links`');
    await queryRunner.query('DROP TABLE `tmp_links`');

    await queryRunner.query(
      'ALTER TABLE `instruction_link` ADD CONSTRAINT `FK_7902b26f1b428c416e68d5910c7` FOREIGN KEY (`instruction_id`) REFERENCES `instruction`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE `instruction_link` DROP FOREIGN KEY `FK_7902b26f1b428c416e68d5910c7`',
    );
    await queryRunner.query('DROP INDEX `IDX_36fd0c07800222a1bf5cfa2549` ON `instruction_link`');
    await queryRunner.query(
      'ALTER TABLE `instruction_link` CHANGE `instruction_id` `work_instruction_id_id` varchar(255) NULL',
    );
    await queryRunner.query(
      'ALTER TABLE `instruction_link` ADD CONSTRAINT `FK_09797cd8be1c62ee2154865b40c` FOREIGN KEY (`work_instruction_id_id`) REFERENCES `instruction`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION',
    );
  }
}
