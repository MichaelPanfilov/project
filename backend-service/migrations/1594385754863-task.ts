import {MigrationInterface, QueryRunner} from "typeorm";

export class task1594385754863 implements MigrationInterface {
    name = 'task1594385754863'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `task` (`id` varchar(255) NOT NULL, `title` varchar(255) NOT NULL, `description` varchar(255) NULL, `assigned_asset` varchar(255) NULL, `assigned_person` varchar(255) NULL, `created_by` varchar(255) NOT NULL, `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `due_date` timestamp NULL, `status` enum ('inactive', 'active', 'done', 'closed') NOT NULL DEFAULT 'inactive', `classification` varchar(255) NOT NULL, `machine_types` text NULL, `done_date` timestamp NULL, `repeat_interval` enum ('never', 'hours', 'days', 'weeks', 'months') NULL DEFAULT 'never', `calendar_date` timestamp NULL, `countdown_hours` float NULL, `unit_intervals` enum ('minutes', 'hours', 'days') NULL, `trigger_type` enum ('now', 'countdown', 'calendar') NOT NULL, INDEX `IDX_126309e371e6e528805cb19a96` (`trigger_type`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `task` CHANGE `countdown_hours` `countdown_hours` float NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP INDEX `IDX_126309e371e6e528805cb19a96` ON `task`", undefined);
        await queryRunner.query("DROP TABLE `task`", undefined);
    }

}
