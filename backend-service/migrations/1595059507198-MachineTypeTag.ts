import {MigrationInterface, QueryRunner, Table, TableColumn} from "typeorm";

export class MachineTypeTag1594911566486 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
      const isTable = await queryRunner.hasTable('machine_type_tag');
      if (!isTable) {
        await queryRunner.createTable(new Table({
          name: 'machine_type_tag',
          columns: [
            new TableColumn({
              name: 'name',
              type: 'varchar',
              isNullable: false,
              isPrimary: true,
              isUnique: true
            })
          ]
        }))
      }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      const isTable = await queryRunner.hasTable('machine_type_tag');
      if (isTable) {
        await queryRunner.dropTable('machine_type_tag');
      }
    }

}
