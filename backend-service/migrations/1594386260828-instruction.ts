import {MigrationInterface, QueryRunner} from "typeorm";

export class instruction1594386260828 implements MigrationInterface {
    name = 'instruction1594386260828'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `instruction` (`id` varchar(255) NOT NULL, `title` varchar(255) NOT NULL, `url` varchar(255) NULL, `classification` varchar(255) NULL, `machine_types` text NULL, `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `instruction_link` (`id` varchar(36) NOT NULL, `ref_id` varchar(255) NOT NULL, `work_instruction_id_id` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `instruction_link` ADD CONSTRAINT `FK_09797cd8be1c62ee2154865b40c` FOREIGN KEY (`work_instruction_id_id`) REFERENCES `instruction`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `instruction_link` DROP FOREIGN KEY `FK_09797cd8be1c62ee2154865b40c`", undefined);
        await queryRunner.query("DROP TABLE `instruction_link`", undefined);
        await queryRunner.query("DROP TABLE `instruction`", undefined);
    }

}
