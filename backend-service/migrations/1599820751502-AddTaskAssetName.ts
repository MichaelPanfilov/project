import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddTaskAssetName1599820751502 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'task',
      new TableColumn({
        name: 'asset_name',
        type: 'varchar',
        isNullable: true,
      }),
    );
    try {
      await queryRunner.query(
        'UPDATE task INNER JOIN asset_tree a ON task.assigned_asset = a.id SET task.asset_name=a.name WHERE assigned_asset IS NOT NULL;',
      );
    } catch (e) {
      // Catch error here to because after we separate these services there will be no more asset table.
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const hasColumn = await queryRunner.hasColumn('task', 'asset_name');
    if (hasColumn) {
      await queryRunner.dropColumn('task', 'asset_name');
    }
  }
}
