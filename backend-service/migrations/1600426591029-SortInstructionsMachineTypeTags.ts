import {MigrationInterface, QueryRunner} from "typeorm";
import {Instruction} from "../src/instruction/entities/instruction.entity";

export class SortInstructionsMachineTypeTags1600426591029 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        try {
            const repo = queryRunner.manager.getRepository(Instruction);
            const instructions = await repo
              .createQueryBuilder()
              .select()
              .where('machine_types IS NOT NULL')
              .getMany();

            const sortTaskTags = (tags?: string[]) => {
                return (
                  tags &&
                  tags.sort((a, b) => {
                      if (a.toLowerCase() < b.toLowerCase()) {
                          return -1;
                      }
                      if (a.toLowerCase() > b.toLowerCase()) {
                          return 1;
                      }
                      return 0;
                  })
                );
            };

            const toUpdate = instructions
              .filter(t => t.machineTypes && t.machineTypes.length)
              .map(t => ({ ...t, machineTypes: sortTaskTags(t.machineTypes) }));

            await repo.save(toUpdate);
        } catch (e) {
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
