import {MigrationInterface, QueryRunner} from "typeorm";

export class actor1594385297920 implements MigrationInterface {
    name = 'actor1594385297920'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `actor` (`id` varchar(255) NOT NULL, `name` varchar(255) NOT NULL, `acl_id` varchar(255) NOT NULL, `language` enum ('english', 'german') NULL DEFAULT 'english', `type` enum ('actor', 'user', 'system', 'machine') NOT NULL, INDEX `IDX_583b84dd5007044a89bd1b8fa9` (`type`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `task` CHANGE `countdown_hours` `countdown_hours` float(12) NULL", undefined);
    }
}
