import {MigrationInterface, QueryRunner} from "typeorm";

export class asset1594385651330 implements MigrationInterface {
    name = 'asset1594385651330'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `asset_tree` (`id` varchar(255) NOT NULL, `name` varchar(255) NOT NULL, `description` varchar(255) NOT NULL DEFAULT '', `position` int NOT NULL, `manufacturer` varchar(255) NULL DEFAULT '', `machine_type` varchar(255) NULL DEFAULT '', `serial_number` varchar(255) NULL DEFAULT '', `construction_year` int NULL DEFAULT 2020, `device` varchar(36) NULL, `is_line_output` tinyint NULL DEFAULT 0, `postal_code` varchar(255) NULL, `type` enum ('machine', 'line', 'factory') NOT NULL, `mpath` varchar(255) NULL DEFAULT '', `parent_id` varchar(255) NULL, INDEX `IDX_6bccdfa668ad9fa8bc9a04a0c1` (`type`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `asset_tree` CHANGE `construction_year` `construction_year` int NULL DEFAULT 2020", undefined);
        await queryRunner.query("ALTER TABLE `asset_tree` ADD CONSTRAINT `FK_db117b0c63e3a06445b06d8a914` FOREIGN KEY (`parent_id`) REFERENCES `asset_tree`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `asset_tree` DROP FOREIGN KEY `FK_db117b0c63e3a06445b06d8a914`", undefined);
        await queryRunner.query("DROP INDEX `IDX_6bccdfa668ad9fa8bc9a04a0c1` ON `asset_tree`", undefined);
        await queryRunner.query("DROP TABLE `asset_tree`", undefined);
    }

}
