import { InstructionAcl } from '@common';
import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';

import { AllowRights, getRoutesFor } from '../util/api';

import { Instruction } from './entities/instruction.entity';
import { InstructionService } from './instruction.service';

@Crud({
  model: {
    type: Instruction,
  },
  params: {
    instructionId: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  routes: getRoutesFor(Instruction, {
    getOneBase: {
      decorators: [AllowRights([InstructionAcl.READ])],
    },
    getManyBase: {
      decorators: [AllowRights([InstructionAcl.READ])],
    },
    createOneBase: { decorators: [AllowRights([InstructionAcl.CREATE])] },
    replaceOneBase: { decorators: [AllowRights([InstructionAcl.UPDATE])] },
    deleteOneBase: { decorators: [AllowRights([InstructionAcl.DELETE])] },
  }),
  query: {
    alwaysPaginate: true,
    join: {
      links: {
        eager: true,
      },
    },
  },
  serialize: {
    // https://github.com/nestjsx/crud/issues/525
    // https://github.com/nestjsx/crud/pull/526
    getMany: Instruction,
  },
})
@Controller('/v1/work-instructions')
@ApiBearerAuth()
@ApiTags('Work instructions')
export class InstructionController implements CrudController<Instruction> {
  constructor(public service: InstructionService) {}

  @Get('/:instructionId/links')
  @AllowRights([InstructionAcl.READ])
  @ApiOkResponse({ type: String, isArray: true })
  getLinks(@Param('instructionId') instructionId: string): Promise<string[]> {
    return this.service.getLinks(instructionId);
  }

  @Post('/:instructionId/link')
  @UseInterceptors(ClassSerializerInterceptor)
  @AllowRights([InstructionAcl.UPDATE])
  @ApiOkResponse({ type: Instruction })
  linkMany(
    @Param('instructionId') instructionId: string,
    @Body() body: string[],
  ): Promise<Instruction> {
    return this.service.createLinks(instructionId, body);
  }

  @Post('/:instructionId/unlink')
  @UseInterceptors(ClassSerializerInterceptor)
  @AllowRights([InstructionAcl.UPDATE])
  @ApiOkResponse({ type: Instruction })
  unlinkMany(
    @Param('instructionId') instructionId: string,
    @Body() body: string[],
  ): Promise<Instruction> {
    return this.service.removeLinks(instructionId, body);
  }

  /*
  @Post('/:instructionId/link/:refId')
  @AllowRights([InstructionAcl.UPDATE])
  @ApiOkResponse({ type: Instruction })
  link(
    @Param('instructionId') instructionId: string,
    @Param('refId') refId: string,
  ): Promise<Instruction> {
    return this.service.createLink(instructionId, refId);
  }

  @Delete('/:instructionId/link/:refId')
  @AllowRights([InstructionAcl.UPDATE])
  @ApiOkResponse({ type: Instruction })
  async unlink(
    @Param('instructionId') instructionId: string,
    @Param('refId') refId: string,
  ): Promise<Instruction> {
    return this.service.removeLink(instructionId, refId);
  }
  */

  @Get('/:instructionId/qr-code')
  @AllowRights([InstructionAcl.READ])
  @ApiOkResponse({ type: String })
  getQrCode(@Param('instructionId') instructionId: string): Promise<string> {
    return this.service.createQRCode(instructionId);
  }
}
