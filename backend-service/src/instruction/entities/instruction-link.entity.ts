import { IsString } from 'class-validator';
import { Column, Entity, ManyToOne, Unique } from 'typeorm';

import { BaseModel } from '../../util/api';

import { Instruction } from './instruction.entity';

@Entity()
@Unique(['instruction', 'refId'])
export class InstructionLink extends BaseModel {
  @IsString()
  @ManyToOne(
    type => Instruction,
    instruction => instruction.links,
    { onDelete: 'CASCADE' },
  )
  instruction!: Instruction;

  @IsString()
  @Column()
  refId!: string;
}
