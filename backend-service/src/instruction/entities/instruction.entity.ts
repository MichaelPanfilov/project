import { generateId, InstructionBaseDto, ReferencedEntities } from '@common';
import { ApiProperty, ApiPropertyOptional, ApiResponseProperty } from '@nestjs/swagger';
import { CrudValidationGroups } from '@nestjsx/crud';
import { Transform } from 'class-transformer';
import { IsArray, IsOptional, IsString } from 'class-validator';
import { Column, CreateDateColumn, Entity, OneToMany, UpdateDateColumn } from 'typeorm';

import { BaseModelPrefixed, TagsSortTransformer } from '../../util';

import { InstructionLink } from './instruction-link.entity';

const { UPDATE } = CrudValidationGroups;

@Entity()
export class Instruction extends BaseModelPrefixed implements InstructionBaseDto {
  @ApiProperty()
  @IsString()
  @IsOptional({ groups: [UPDATE] })
  @Column()
  title!: string;

  @ApiProperty()
  @IsString()
  @IsOptional({ groups: [UPDATE] })
  @Column({ nullable: true })
  url!: string;

  @ApiProperty()
  @IsString()
  @IsOptional({ groups: [UPDATE] })
  @Column({ nullable: true })
  classification!: string;

  @ApiPropertyOptional()
  @IsArray()
  @IsOptional()
  @Column({ type: 'simple-array', nullable: true, transformer: new TagsSortTransformer() })
  machineTypes?: string[];

  @ApiResponseProperty({ type: [String] })
  @OneToMany(
    type => InstructionLink,
    link => link.instruction,
    { eager: true },
  )
  @Transform((links: InstructionLink[]) => links.map(l => l.refId), { toPlainOnly: true })
  links!: InstructionLink[];

  @ApiResponseProperty()
  @CreateDateColumn()
  createdAt!: Date;

  @ApiResponseProperty()
  @UpdateDateColumn()
  updatedAt!: Date;

  constructor() {
    super(generateId(ReferencedEntities.WORK_INSTRUCTION));
  }
}
