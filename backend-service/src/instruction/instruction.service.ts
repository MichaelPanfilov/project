import { Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { CrudRequest, GetManyDefaultResponse } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import * as normalizeUrl from 'normalize-url';
import * as QRCode from 'qrcode';
import { EntityManager, In, Repository } from 'typeorm';
import { URL } from 'url';

import { addSimpleArrayWhere, extractParamsFilter } from '../util/api';

import { InstructionLink } from './entities/instruction-link.entity';
import { Instruction } from './entities/instruction.entity';

@Injectable()
export class InstructionService extends TypeOrmCrudService<Instruction> {
  private linkRepo = this.manager.getRepository(InstructionLink);

  constructor(
    @InjectRepository(Instruction) repo: Repository<Instruction>,
    @InjectEntityManager() private manager: EntityManager,
  ) {
    super(repo);
  }

  async getMany(req: CrudRequest): Promise<GetManyDefaultResponse<Instruction> | Instruction[]> {
    // Typeorm simple-array column must be treated special to.
    const field: keyof Instruction = 'machineTypes';
    const { parsed, items } = extractParamsFilter<Instruction>(req.parsed, field);

    let builder = await this.createBuilder(parsed, req.options);
    builder = addSimpleArrayWhere(builder, this.alias, field, items);
    builder = builder.leftJoinAndSelect(`${this.alias}.links`, 'link');

    return super.doGetMany(builder, parsed, req.options);
  }

  getByRefId(refId: string): Promise<Instruction[]> {
    return this.repo
      .createQueryBuilder('instruction')
      .select('instruction')
      .leftJoinAndSelect('instruction.links', 'link')
      .where('link.refId = :refId', { refId })
      .getMany();
  }

  async createLink(instructionId: string, refId: string): Promise<Instruction> {
    return this.createLinks(instructionId, [refId]);
  }

  async removeLink(instructionId: string, refId: string): Promise<Instruction> {
    return this.removeLinks(instructionId, [refId]);
  }

  async createLinks(instructionId: string, links: string[]): Promise<Instruction> {
    const instruction = await this.repo.findOneOrFail(instructionId);
    // Catch duplicate entries without failing the request
    await Promise.all(
      links.map(refId => this.linkRepo.save({ refId, instruction }).catch(() => {})),
    );
    return this.repo.findOneOrFail(instructionId);
  }

  async removeLinks(instructionId: string, links: string[]): Promise<Instruction> {
    const instruction = await this.repo.findOneOrFail(instructionId);
    const entities = await this.linkRepo.find({ where: { instruction, refId: In(links) } });
    await this.linkRepo.remove(entities);
    return this.repo.findOneOrFail(instructionId);
  }

  async getLinks(instructionId: string): Promise<string[]> {
    const instruction = await this.repo.findOneOrFail(instructionId);
    const links = await this.linkRepo.find({ where: { instruction } });
    return links.map(l => l.refId);
  }

  async createQRCode(instructionId: string): Promise<string> {
    try {
      const instruction = await this.repo.findOneOrFail(instructionId);
      const url = new URL(normalizeUrl(instruction.url));
      return await QRCode.toDataURL(url.href, { errorCorrectionLevel: 'H' });
    } catch (e) {
      throw e;
    }
  }
}
