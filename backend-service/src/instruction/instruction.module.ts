import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { InstructionLink } from './entities/instruction-link.entity';
import { Instruction } from './entities/instruction.entity';
import { InstructionController } from './instruction.controller';
import { InstructionService } from './instruction.service';

@Module({
  imports: [TypeOrmModule.forFeature([Instruction, InstructionLink])],
  providers: [InstructionService],
  exports: [InstructionService],
  controllers: [InstructionController],
})
export class InstructionModule {}
