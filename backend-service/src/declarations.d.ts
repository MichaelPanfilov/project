declare namespace Express {
  interface Request {
    auth: import('@common').JWTPayload;
  }
}
