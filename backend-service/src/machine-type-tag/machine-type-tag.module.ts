import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MachineTypeTag } from './entities/machine-type-tag.entity';
import { MachineTypeTagController } from './machine-type-tag.controller';
import { MachineTypeTagService } from './machine-type-tag.service';

@Module({
  imports: [TypeOrmModule.forFeature([MachineTypeTag])],
  providers: [MachineTypeTagService],
  exports: [MachineTypeTagService],
  controllers: [MachineTypeTagController],
})
export class MachineTypeTagModule {}
