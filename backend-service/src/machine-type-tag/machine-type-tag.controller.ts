import { MachineTypeTag } from '@common';
import { Body, Controller, Get, Post } from '@nestjs/common';

import { MachineTypeTagService } from './machine-type-tag.service';

@Controller('/v1/machine-type-tags')
export class MachineTypeTagController {
  constructor(public service: MachineTypeTagService) {}

  @Get('/')
  getAll(): Promise<string[]> {
    return this.service.getAll();
  }

  @Post('/')
  create(@Body() body: MachineTypeTag) {
    return this.service.create(body);
  }
}
