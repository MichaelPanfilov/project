import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { MachineTypeTag } from './entities/machine-type-tag.entity';

@Injectable()
export class MachineTypeTagService {
  constructor(@InjectRepository(MachineTypeTag) private repo: Repository<MachineTypeTag>) {}

  async create(tag: MachineTypeTag) {
    try {
      await this.repo.insert(tag);
      return this.repo.findOne(tag);
    } catch (e) {
      if (e.code === 'ER_DUP_ENTRY' && typeof e.sqlMessage === 'string') {
        throw new HttpException({ message: 'tag already exist' }, HttpStatus.CONFLICT);
      } else throw e;
    }
  }

  async getAll() {
    const tags = await this.repo.find();
    return tags.map(t => t.name);
  }
}
