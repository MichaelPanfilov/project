import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class MachineTypeTag {
  @Column()
  @ApiProperty()
  @PrimaryColumn({ type: 'varchar', unique: true })
  @IsString()
  name!: string;
}
