import {
  changedTriggerType,
  CreateTaskDto,
  DataResponse,
  dueDateNeedsUpdate,
  FileDto,
  isCalendarTask,
  isCountdownTask,
  isNowTask,
  RepeatTypes,
  TaskDto,
  TaskStatus,
  TriggerTypes,
} from '@common';
import { HttpService, Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { CrudRequest, GetManyDefaultResponse } from '@nestjsx/crud';
import { QuerySort } from '@nestjsx/crud-request';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { EntityManager, Repository, SelectQueryBuilder } from 'typeorm';

import { InstructionService } from '../instruction/instruction.service';
import { addSimpleArrayWhere, extractParamsFilter } from '../util/api';
import { InternalServicesConfig } from '../util/api';

import { CalendarTask, CountdownTask, NowTask, Task } from './entities/task.entity';

const DATE_FIELD = 'dueDate';
const TIME_LAG = 'timeLag';
const REMAINING_TIME = 'remainingTime';

const SPECIAL_SORTING_KEYS = [DATE_FIELD, TIME_LAG, REMAINING_TIME];

@Injectable()
export class TaskService extends TypeOrmCrudService<Task> {
  constructor(
    @InjectRepository(Task) repo: Repository<Task>,
    @InjectEntityManager() private manager: EntityManager,
    private http: HttpService,
    private instructionService: InstructionService,
    private urlConfig: InternalServicesConfig,
  ) {
    super(repo);
    this.urlConfig.assertServiceUrls('file');
  }

  async getMany(req: CrudRequest): Promise<GetManyDefaultResponse<Task> | Task[]> {
    // Typeorm simple-array column must be treated special to.
    const field: keyof Task = 'machineTypes';
    const { parsed, items } = extractParamsFilter<Task>(req.parsed, field);

    // Remove special sorting keys.
    const sort = parsed.sort;
    parsed.sort = sort.filter(s => !SPECIAL_SORTING_KEYS.includes(s.field));

    let builder = await this.createBuilder(parsed, req.options);
    builder = addSimpleArrayWhere(builder, this.alias, field, items);
    builder = this.addVirtualSorting(builder, sort);

    return super.doGetMany(builder, parsed, req.options);
  }

  async create(dto: CreateTaskDto & { assetName?: string | null }, createdBy: string) {
    const { assignedAsset, assetName, ...payload } = dto;
    const task = await this.saveTask({ ...payload, createdBy, status: TaskStatus.INACTIVE });
    if (!assignedAsset) {
      return task;
    }
    return this.assign(task.id, assignedAsset);
  }

  async update(taskId: string, dto: Partial<Task>, req: CrudRequest): Promise<Task> {
    let task = await this.repo.findOneOrFail(taskId);
    const { assignedAsset, status, dueDate, ...payload } = dto;
    if (assignedAsset && assignedAsset !== task.assignedAsset) {
      task = await this.assign(taskId, assignedAsset, true);
    } else if (!assignedAsset && task.assignedAsset) {
      task = await this.unassign(taskId);
    }

    // If triggerType changes we need to access the right repo.
    if (changedTriggerType(task as TaskDto, payload)) {
      const oldRepo = this.getTaskRepo(task);
      const newRepo = this.getTaskRepo(payload);
      await oldRepo.remove(task);
      (payload as Partial<Task>).dueDate = this.computeNewDueDate(dto);
      return newRepo.save({ ...task, ...payload });
    } else if (dueDateNeedsUpdate(task as TaskDto, payload)) {
      (payload as Partial<Task>).dueDate = this.computeNewDueDate(dto);
    }
    return this.updateOne(req, payload);
  }

  async delete(taskId: string, crudReq: CrudRequest): Promise<void | Task> {
    const task = await this.repo.findOneOrFail(taskId);

    const files = await this.getLinkedFiles(task);
    await Promise.all(files.map(f => this.unlinkFile(task.id, f.id, f.refIds)));

    const instructions = await this.instructionService.getByRefId(task.id);
    await Promise.all(instructions.map(i => this.unlinkInstruction(task.id, i.id)));

    return super.deleteOne(crudReq);
  }

  async assign(taskId: string, assetId: string, force?: boolean): Promise<Task> {
    const task = await this.repo.findOneOrFail(taskId);
    await this.saveTask({ ...task, assignedAsset: assetId });
    return this.activate(taskId, force);
  }

  async unassign(taskId: string): Promise<Task> {
    const task = await this.repo.findOneOrFail(taskId);
    return this.saveTask({
      ...task,
      assignedAsset: null,
      status: TaskStatus.INACTIVE,
      dueDate: null,
    });
  }

  // async unassignAllByAsset(assetId: string) {
  //   return this.repo
  //     .createQueryBuilder('task')
  //     .update()
  //     .where('task.assigned_asset = :assetId', { assetId })
  //     .andWhere('task.status = :active', { active: TaskStatus.ACTIVE })
  //     .set({ assignedAsset: undefined, status: TaskStatus.INACTIVE })
  //     .execute();
  // }

  async markDone(taskId: string): Promise<Task> {
    let task = await this.repo.findOneOrFail(taskId);
    if (task.status === TaskStatus.ACTIVE) {
      task = await this.saveTask({ ...task, status: TaskStatus.DONE, doneDate: new Date() });

      const isOnce = isCalendarTask(task) && task.repeatInterval === RepeatTypes.NEVER;
      if (task.triggerType !== TriggerTypes.NOW && !isOnce) {
        await this.repeat(task);
      }
      return task;
    } else if (task.status !== TaskStatus.DONE) {
      throw this.throwBadRequestException(`Cannot resolve task with "${task.status}" status`);
    } else {
      return task;
    }
  }

  computeNewDueDate(task: Partial<Task>, isRepeat?: boolean): Date {
    const dueDate = moment();
    if (isCountdownTask(task)) {
      return dueDate.add(task.countdownHours, task.unitIntervals).toDate();
    } else if (isCalendarTask(task)) {
      if (task.repeatInterval !== RepeatTypes.NEVER && isRepeat) {
        return dueDate.add(1, task.repeatInterval).toDate();
      }
      return task.calendarDate;
    }
    return dueDate.toDate();
  }

  async isOverDue(taskId: string) {
    const task = await this.repo.findOneOrFail(taskId);
    const now = new Date();
    return task.dueDate && task.dueDate > now;
  }

  /**
   * Called only when marked done and task has to be repeated after that (based on some interval).
   */
  private async repeat(task: Task): Promise<void> {
    const { id, status, dueDate, doneDate, ...copy } = task;
    const newTask = await this.saveTask({
      ...copy,
      status: TaskStatus.ACTIVE,
      dueDate: this.computeNewDueDate(task, true),
    });
    await this.copyRefs(task, newTask);
  }

  /**
   * Called when manually coping a task.
   */
  async clone(taskId: string) {
    const task = await this.repo.findOneOrFail(taskId);
    return this.copy(task, {
      title: `Copy of ${task.title}`,
      dueDate: this.computeNewDueDate(task),
      status: TaskStatus.INACTIVE,
      assignedAsset: undefined,
      assetName: undefined,
    });
  }

  async getByAssetId(assetId: string): Promise<Task[]> {
    return this.repo
      .createQueryBuilder('task')
      .select('task')
      .where('assigned_asset = :assetId', { assetId })
      .getMany();
  }

  async updateAssignedAssetName(assetId: string, assetName: string): Promise<void> {
    const tasks = await this.getByAssetId(assetId);
    // we have to call save as typeORM EventSubscriber doesn't work well with update
    await this.repo.save(tasks.map(t => ({ ...t, assetName })));
  }

  async copy(task: Task, overrides?: Partial<Task>): Promise<Task> {
    const { id, assignedAsset, status, dueDate, doneDate, ...payload } = task;
    const newTask = await this.saveTask({ ...payload, ...overrides });
    if (assignedAsset && (!overrides || typeof overrides.assignedAsset === 'string')) {
      await this.assign(newTask.id, assignedAsset);
    }

    await this.copyRefs(task, newTask);

    return this.repo.findOneOrFail(newTask.id);
  }

  private async copyRefs(task: Task, newTask: Task): Promise<void> {
    const files = await this.getLinkedFiles(task);
    await Promise.all(files.map(f => this.linkFile(newTask.id, f.id, f.refIds)));

    const instructions = await this.instructionService.getByRefId(task.id);
    await Promise.all(instructions.map(i => this.linkInstruction(newTask.id, i.id)));
  }

  private async activate(taskId: string, force?: boolean): Promise<Task> {
    const task = await this.repo.findOneOrFail(taskId);
    if (force || task.status === TaskStatus.INACTIVE) {
      return this.saveTask({
        ...task,
        status: TaskStatus.ACTIVE,
        dueDate: this.computeNewDueDate(task),
      });
    } else {
      throw this.throwBadRequestException(`Cannot activate task with "${task.status}" status`);
    }
  }

  private async linkFile(taskId: string, fileId: string, currentIds?: string[]): Promise<void> {
    const refIds = currentIds ? [...currentIds, taskId] : [taskId];
    await this.http
      .put<DataResponse<FileDto>>(
        this.urlConfig.resolveUrl('file', `v1/files/${fileId}`),
        { refIds },
        { headers: { authorization: this.urlConfig.authHeader } },
      )
      .toPromise();
  }

  private async linkInstruction(taskId: string, instructionId: string): Promise<void> {
    await this.instructionService.createLink(instructionId, taskId);
  }

  private async unlinkFile(taskId: string, fileId: string, currentIds?: string[]): Promise<void> {
    const refIds = currentIds ? currentIds.filter(id => id !== taskId) : [];
    await this.http
      .put<DataResponse<FileDto>>(
        this.urlConfig.resolveUrl('file', `v1/files/${fileId}`),
        { refIds },
        { headers: { authorization: this.urlConfig.authHeader } },
      )
      .toPromise();
  }

  private async unlinkInstruction(taskId: string, instructionId: string): Promise<void> {
    await this.instructionService.removeLink(instructionId, taskId);
  }

  getLinkedFiles(task: Task): Promise<FileDto[]> {
    return this.http
      .get<DataResponse<FileDto[]>>(this.urlConfig.resolveUrl('file', `v1/files`), {
        params: { refId: task.id },
        headers: { authorization: this.urlConfig.authHeader },
      })
      .pipe(map(res => res.data.data))
      .toPromise();
  }

  private saveTask(dto: Partial<Task>): Promise<Task> {
    if (dto.id) {
      return this.repo.save(dto);
    }
    // Calling .create() is needed so the id gets generated properly.
    // Also when creating the task we need to use the right repo so typeorm works.
    const repo = this.getTaskRepo(dto);
    return repo.save({ ...repo.create(dto), triggerType: dto.triggerType });
  }

  private getTaskRepo(dto: Partial<Task>): Repository<Task> {
    let repo: Repository<Task> | undefined;
    if (isNowTask(dto)) {
      repo = this.manager.getRepository(NowTask);
    } else if (isCountdownTask(dto)) {
      repo = this.manager.getRepository(CountdownTask);
    } else if (isCalendarTask(dto)) {
      repo = this.manager.getRepository(CalendarTask);
    }
    if (repo) {
      return repo;
    }
    throw this.throwBadRequestException('Unknown task triggerType');
  }

  /*
  private updateTask(old: Task, dto: Partial<Task>, calcDueDate = false) {
    // Only update due date if user changed the right properties.
    dto.dueDate = calcDueDate ? this.computeNewDueDate(dto) : old.dueDate;
    return this.repo.update(old.id, dto);
  }
  */

  private addVirtualSorting(
    builder: SelectQueryBuilder<Task>,
    sort: QuerySort[],
  ): SelectQueryBuilder<Task> {
    const dueDateSort = sort.find(s => s.field === DATE_FIELD);
    if (dueDateSort) {
      builder = builder
        .addSelect(
          `IF(${this.alias}.status="${TaskStatus.INACTIVE}", ${Number.MAX_SAFE_INTEGER}, IF(${this.alias}.due_date IS NOT NULL, UNIX_TIMESTAMP(${this.alias}.due_date), 0))`,
          'due_time',
        )
        .orderBy('due_time', dueDateSort.order);
    }

    const remainingTimeSort = sort.find(s => s.field === REMAINING_TIME);
    if (remainingTimeSort) {
      builder = builder
        .addSelect(
          `TIMESTAMPDIFF(SECOND,${this.alias}.due_date,CURRENT_TIMESTAMP())`,
          'remaining_time',
        )
        .orderBy('remaining_time', remainingTimeSort.order);
    }

    const timeLagSort = sort.find(s => s.field === TIME_LAG);
    if (timeLagSort) {
      builder = builder
        .addSelect(
          `TIMESTAMPDIFF(SECOND,${this.alias}.done_date,${this.alias}.due_date)`,
          'time_lag',
        )
        .orderBy('time_lag', timeLagSort.order);
    }

    return builder;
  }
}
