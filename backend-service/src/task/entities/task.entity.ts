import {
  BaseTaskDto,
  CalendarTaskDto,
  CountdownTaskDto,
  generateId,
  IntervalTypes,
  NowTaskDto,
  ReferencedEntities,
  RepeatTypes,
  TaskStatus,
  TriggerTypes,
} from '@common';
import { ApiProperty, ApiPropertyOptional, ApiResponseProperty } from '@nestjs/swagger';
import { CrudValidationGroups } from '@nestjsx/crud';
import {
  IsArray,
  IsDateString,
  IsEmpty,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import {
  ChildEntity,
  Column,
  CreateDateColumn,
  Entity,
  TableInheritance,
  UpdateDateColumn,
} from 'typeorm';

import { BaseModelPrefixed, TagsSortTransformer } from '../../util';

const { CREATE, UPDATE } = CrudValidationGroups;

@Entity()
@TableInheritance({ column: { type: 'enum', enum: TriggerTypes, name: 'trigger_type' } })
export abstract class Task<T extends TriggerTypes = TriggerTypes> extends BaseModelPrefixed
  implements BaseTaskDto<T> {
  @ApiProperty()
  @IsString()
  @IsOptional({ groups: [UPDATE] })
  @Column()
  title!: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  @Column({ nullable: true })
  description?: string;

  @ApiProperty({ type: String, nullable: true })
  @IsString()
  @IsEmpty({ groups: [CREATE, UPDATE] })
  @Column({ type: 'varchar', nullable: true })
  assignedAsset!: string | null;

  @ApiProperty({ type: String, nullable: true, readOnly: true })
  @IsString()
  @IsEmpty({ groups: [CREATE, UPDATE] })
  @Column({ type: 'varchar', nullable: true })
  assetName!: string | null;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional({ groups: [CREATE, UPDATE] })
  @Column({ nullable: true })
  assignedPerson?: string;

  @ApiResponseProperty()
  @IsEmpty()
  @Column()
  createdBy!: string;

  @ApiResponseProperty()
  @IsEmpty()
  @CreateDateColumn()
  createdAt!: Date;

  @ApiResponseProperty()
  @IsEmpty()
  @UpdateDateColumn()
  updatedAt!: Date;

  @ApiProperty({ type: Date, readOnly: true, nullable: true })
  @IsEmpty()
  @Column({ type: 'timestamp', nullable: true })
  dueDate!: Date | null;

  @ApiResponseProperty()
  @IsEmpty()
  @Column({ type: 'enum', enum: TaskStatus, default: TaskStatus.INACTIVE })
  status!: TaskStatus;

  @ApiProperty()
  @IsEnum(TriggerTypes)
  @IsOptional({ groups: [UPDATE] })
  // @ts-ignore
  triggerType!: T;

  @ApiProperty()
  @IsString()
  @IsOptional({ groups: [UPDATE] })
  @Column()
  classification!: string;

  @ApiProperty({ type: String, isArray: true, required: false })
  @IsArray()
  @IsOptional()
  @Column({ type: 'simple-array', nullable: true, transformer: new TagsSortTransformer() })
  machineTypes?: string[];

  @ApiProperty({ type: Date, readOnly: true, required: false })
  @IsOptional()
  @IsEmpty()
  @Column({ type: 'timestamp', nullable: true })
  doneDate?: Date;

  // We need to implement the constructor here to ensure the type is returned.
  // STI does not return the type by itself.
  // All sub classes have to call the constructor.
  constructor(triggerType: T) {
    super(generateId(ReferencedEntities.TASK));
    this.triggerType = triggerType;
  }
}

@ChildEntity(TriggerTypes.NOW)
export class NowTask extends Task<TriggerTypes.NOW> implements NowTaskDto {
  @ApiProperty({ enum: [TriggerTypes.NOW] })
  triggerType!: TriggerTypes.NOW;

  constructor() {
    super(TriggerTypes.NOW);
  }
}

@ChildEntity(TriggerTypes.CALENDAR)
export class CalendarTask extends Task<TriggerTypes.CALENDAR> implements CalendarTaskDto {
  @ApiProperty({ enum: [TriggerTypes.CALENDAR] })
  triggerType!: TriggerTypes.CALENDAR;

  @ApiProperty({ enum: RepeatTypes })
  @IsEnum(RepeatTypes)
  @IsOptional()
  @Column({ type: 'enum', enum: RepeatTypes, default: RepeatTypes.NEVER })
  repeatInterval!: RepeatTypes;

  @ApiProperty()
  @IsDateString()
  @IsOptional()
  @Column('timestamp')
  calendarDate!: Date;

  constructor() {
    super(TriggerTypes.CALENDAR);
  }
}

@ChildEntity(TriggerTypes.COUNTDOWN)
export class CountdownTask extends Task<TriggerTypes.COUNTDOWN> implements CountdownTaskDto {
  @ApiProperty({ enum: [TriggerTypes.COUNTDOWN] })
  triggerType!: TriggerTypes.COUNTDOWN;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
  @Column({ type: 'float' })
  countdownHours!: number;

  @ApiProperty({ enum: IntervalTypes })
  @IsEnum(IntervalTypes)
  @IsOptional()
  @Column({ type: 'enum', enum: IntervalTypes })
  unitIntervals!: IntervalTypes;

  constructor() {
    super(TriggerTypes.COUNTDOWN);
  }
}
