import { forwardRef, HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AssetModule } from '../asset/asset.module';
import { InstructionModule } from '../instruction/instruction.module';
import { InternalServicesConfig } from '../util';

import { CalendarTask, CountdownTask, NowTask, Task } from './entities/task.entity';
import { TaskAssetNameSubscriber } from './task-asset-name.subscriber';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Task, NowTask, CalendarTask, CountdownTask]),
    HttpModule.register({
      timeout: 7000,
      maxRedirects: 3,
    }),
    InstructionModule,
    forwardRef(() => AssetModule),
  ],
  providers: [TaskService, InternalServicesConfig, TaskAssetNameSubscriber],
  exports: [TaskService],
  controllers: [TaskController],
})
export class TaskModule {}
