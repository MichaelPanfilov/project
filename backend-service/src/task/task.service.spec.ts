import {
  ASSET_ID_MOCK,
  FileDto,
  FILES,
  RepeatTypes,
  TaskStatus,
  TASK_ID_MOCK,
  TriggerTypes,
} from '@common';
import { BadRequestException, HttpModule } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { getEntityManagerToken, getRepositoryToken } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import * as moment from 'moment';
import { DeepPartial } from 'typeorm';

import { MockInternalServicesConfig } from '../../test/util/config';
import { InternalServicesConfig } from '../util/api';

import { CalendarTask, CountdownTask, Task } from './entities/task.entity';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';

// most of the typeorm repository db methods e.g. save are private in the service
// In order to mock them properly they are retrieved via "any" assertion
// todo: remove if database is used for unit testing
/* tslint:disable:no-any */

const defaultFields = {
  title: 'Test Task #123',
  assignedPerson: 'user',
  createdBy: 'user',
  classification: 'Engineer inspection',
  machineTypes: [],
  triggerType: TriggerTypes.NOW,
  assetName: null,
};

export class MockRepo<T> {
  createQueryBuilder = jest.fn(() => this.queryBuilder);

  manager = {
    transaction: (a: Function) => Promise.resolve(a()),
    getRepository: jest.fn().mockImplementation(() => {
      return new MockRepo();
    }),
  };
  metadata = { connection: { options: { type: null } }, columns: [], relations: [] };

  save = jest.fn((dto: T) => Promise.resolve(dto));
  create = jest.fn((dto: T) => dto);
  delete = jest.fn();
  remove = jest.fn();
  update = jest.fn((dto: T) => Promise.resolve(dto));
  findOne = jest.fn();
  findOneOrFail = jest.fn();
  find = jest.fn();
  getMany = jest.fn();

  queryBuilder = {
    offset: jest.fn().mockReturnThis(),
    take: jest.fn().mockReturnThis(),
    orderBy: jest.fn().mockReturnThis(),
    skip: jest.fn().mockReturnThis(),
    limit: jest.fn().mockReturnThis(),
    from: jest.fn().mockReturnThis(),
    addFrom: jest.fn().mockReturnThis(),
    where: jest.fn().mockReturnThis(),
    andWhere: jest.fn().mockReturnThis(),
    innerJoinAndSelect: jest.fn().mockReturnThis(),
    leftJoinAndSelect: jest.fn().mockReturnThis(),
    getManyAndCount: jest.fn(),
    getMany: jest.fn(),
    getOne: jest.fn(),
    delete: jest.fn().mockReturnThis(),
    execute: jest.fn().mockReturnThis(),
  };
}

export class MockEntityManager {
  connection = { options: { type: null } };
  getRepository = jest.fn(() => {
    return new MockRepo();
  });
}

class MockInstructionService {
  createLink = jest.fn();
  removeLink = jest.fn();
  getList = jest.fn();
}

const getMockCrudRequest = (id: string) =>
  ({
    parsed: {
      fields: [],
      paramsFilter: [{ field: 'id', operator: '$eq', value: id }],
      authPersist: {},
      search: { $and: [{ id: { $eq: id } }] },
      filter: [],
      or: [],
      join: [],
      sort: [],
      limit: 1,
      offset: 0,
      page: 1,
      cache: 0,
    },
    options: { params: { taskId: { field: 'id', type: 'string', primary: true } } },
  } as CrudRequest);

function expectComputedDateCall(spy: jest.SpyInstance, expectedDate: Date) {
  expect(
    spy.mock.results.some(
      ({ value, type }) =>
        type === 'return' && value instanceof Date && value.getTime() === expectedDate.getTime(),
    ),
  ).toBe(true);
}

describe('TaskService', () => {
  let taskService: TaskService;

  beforeEach(async () => {
    jest.resetModules();
    process.env.APP_FILE_SERVICE_URL = 'https://google.com';
    const moduleRef = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [TaskController],
      providers: [
        TaskService,
        {
          provide: 'InstructionService',
          useClass: MockInstructionService,
        },
        {
          provide: getEntityManagerToken(),
          useClass: MockEntityManager,
        },
        {
          provide: getRepositoryToken(Task),
          useClass: MockRepo,
        },
        {
          provide: InternalServicesConfig,
          useClass: MockInternalServicesConfig,
        },
      ],
    }).compile();

    taskService = await moduleRef.get<TaskService>(TaskService);
  });

  describe('instance creation', () => {
    it('should be defined', () => {
      expect(taskService).toBeDefined();
    });
  });

  describe('create method', () => {
    it('should create inactive task', async () => {
      jest.spyOn(taskService, 'create');
      jest.spyOn(taskService, 'assign');
      const payload = {
        ...defaultFields,
        assignedAsset: null,
        triggerType: TriggerTypes.NOW,
      };

      const result = await taskService.create(payload, 'acl_user-test123');

      expect(result.status).toBe(TaskStatus.INACTIVE);
      expect(result.triggerType).toBe(TriggerTypes.NOW);
    });

    it('should create active task regardless of the payload', async () => {
      jest.spyOn(taskService, 'create');
      jest.spyOn(taskService, 'assign');
      const payload = {
        ...defaultFields,
        assignedAsset: ASSET_ID_MOCK,
        triggerType: TriggerTypes.NOW,
        status: TaskStatus.INACTIVE,
      };

      // mock repository save
      (taskService as any).repo.findOneOrFail
        // for assign method
        .mockResolvedValueOnce({
          ...payload,
          id: TASK_ID_MOCK,
          assignedAsset: null,
        })
        // for activate method
        .mockResolvedValueOnce({
          ...payload,
          id: TASK_ID_MOCK,
          assignedAsset: ASSET_ID_MOCK,
          status: TaskStatus.INACTIVE,
        });

      const result = await taskService.create(payload, 'acl_user-test123');
      expect(taskService.assign).toHaveBeenCalledTimes(1);
      expect(result.status).toBe(TaskStatus.ACTIVE);
    });

    it('should create inactive task regardless of the payload', async () => {
      jest.spyOn(taskService, 'create');
      jest.spyOn(taskService, 'assign');
      const payload = {
        ...defaultFields,
        assignedAsset: null,
        triggerType: TriggerTypes.NOW,
        status: TaskStatus.ACTIVE,
      };

      const result = await taskService.create(payload, 'acl_user-test123');

      expect(taskService.assign).toHaveBeenCalledTimes(0);
      expect(result.status).toBe(TaskStatus.INACTIVE);
      expect(result.triggerType).toBe(TriggerTypes.NOW);
    });

    it('should create countdown task with dueDate 2 hours until now', async () => {
      const now = Date.now();
      Date.now = jest.fn().mockReturnValue(now);
      const expectedDueDate = moment(now)
        .add(2, 'hours')
        .toDate();

      jest.spyOn(taskService, 'create');
      jest.spyOn(taskService, 'assign');
      jest.spyOn(taskService, 'computeNewDueDate');

      const payload = {
        ...defaultFields,
        assignedAsset: ASSET_ID_MOCK,
        triggerType: TriggerTypes.COUNTDOWN,
        countdownHours: 2,
        unitIntervals: 'hours',
      };

      // mock repository save
      (taskService as any).repo.findOneOrFail
        // for assign method
        .mockResolvedValueOnce({
          ...payload,
          id: TASK_ID_MOCK,
          assignedAsset: null,
        })
        // for activate method
        .mockResolvedValueOnce({
          ...payload,
          id: TASK_ID_MOCK,
          assignedAsset: ASSET_ID_MOCK,
          status: TaskStatus.INACTIVE,
        });

      const result = await taskService.create(payload, 'acl_user-test123');

      expect(result.status).toBe(TaskStatus.ACTIVE);
      expect(result.dueDate).toEqual(expectedDueDate);
    });

    it('should create countdown task with dueDate 2 days until now', async () => {
      const now = Date.now();
      Date.now = jest.fn().mockReturnValue(now);
      const expectedDueDate = moment(now)
        .add(2, 'days')
        .toDate();

      jest.spyOn(taskService, 'create');
      jest.spyOn(taskService, 'assign');
      jest.spyOn(taskService, 'computeNewDueDate');

      const payload = {
        ...defaultFields,
        assignedAsset: ASSET_ID_MOCK,
        triggerType: TriggerTypes.COUNTDOWN,
        countdownHours: 2,
        unitIntervals: 'days',
      };

      // mock repository save
      (taskService as any).repo.findOneOrFail
        // for assign method
        .mockResolvedValueOnce({
          ...payload,
          id: TASK_ID_MOCK,
          assignedAsset: null,
        })
        // for activate method
        .mockResolvedValueOnce({
          ...payload,
          id: TASK_ID_MOCK,
          assignedAsset: ASSET_ID_MOCK,
          status: TaskStatus.INACTIVE,
        });

      const result = await taskService.create(payload, 'acl_user-test123');

      expect(result.status).toBe(TaskStatus.ACTIVE);
      expect(result.dueDate).toEqual(expectedDueDate);
    });

    it('should create task with calendar dueDate', async () => {
      const calendarDate = new Date('2020-01-01T01:01:01');

      jest.spyOn(taskService, 'create');
      jest.spyOn(taskService, 'assign');
      jest.spyOn(taskService, 'computeNewDueDate');

      const payload = {
        ...defaultFields,
        assignedAsset: ASSET_ID_MOCK,
        triggerType: TriggerTypes.CALENDAR,
        repeatInterval: RepeatTypes.NEVER,
        calendarDate,
      };

      // mock repository save
      (taskService as any).repo.findOneOrFail
        // for assign method
        .mockResolvedValueOnce({
          ...payload,
          id: TASK_ID_MOCK,
          assignedAsset: null,
        })
        // for activate method
        .mockResolvedValueOnce({
          ...payload,
          id: TASK_ID_MOCK,
          assignedAsset: ASSET_ID_MOCK,
          status: TaskStatus.INACTIVE,
        });

      const result = await taskService.create(payload, 'acl_user-test123');

      expect(result.status).toBe(TaskStatus.ACTIVE);
      expect(result.dueDate).toEqual(calendarDate);
    });
  });

  describe('update method', () => {
    it('should set task status to active on asset update', async () => {
      const now = Date.now();
      Date.now = jest.fn().mockReturnValue(now);
      const expectedDueDate = moment(now).toDate();
      jest.spyOn(taskService, 'update');
      jest.spyOn(taskService, 'assign');
      jest
        .spyOn(taskService, 'updateOne')
        .mockImplementation(async (req: CrudRequest, task: DeepPartial<Task>) => task as Task);

      const payload = {
        title: 'Test Task Updated',
        classification: 'Engineer inspection',
        machineTypes: ['TTM', 'HRM'],
        assignedAsset: ASSET_ID_MOCK,
        status: TaskStatus.INACTIVE,
      };

      // mock existing task
      (taskService as any).repo.findOneOrFail
        .mockResolvedValueOnce({
          ...defaultFields,
          id: TASK_ID_MOCK,
          assignedAsset: null,
          status: TaskStatus.INACTIVE,
        })
        // mock for assign
        .mockResolvedValue({
          ...defaultFields,
          id: TASK_ID_MOCK,
          status: TaskStatus.INACTIVE,
        });

      await taskService.update(TASK_ID_MOCK, payload, getMockCrudRequest(TASK_ID_MOCK));

      const { status, assignedAsset, ...rest } = payload;
      expect(taskService.updateOne).toHaveBeenCalledWith(getMockCrudRequest(TASK_ID_MOCK), {
        ...rest,
        dueDate: expectedDueDate,
      });
    });

    it('should set task status to inactive on setting asset to null', async () => {
      jest.spyOn(taskService, 'unassign');
      jest
        .spyOn(taskService, 'updateOne')
        .mockImplementation(async (req: CrudRequest, task: DeepPartial<Task>) => task as Task);

      const payload = {
        title: 'Test Task Updated',
        classification: 'Engineer inspection',
        machineTypes: ['TTM', 'HRM'],
        assignedAsset: null,
        status: TaskStatus.ACTIVE,
        triggerType: TriggerTypes.NOW,
      };

      // mock existing task
      (taskService as any).repo.findOneOrFail.mockResolvedValue({
        ...defaultFields,
        id: TASK_ID_MOCK,
        assignedAsset: ASSET_ID_MOCK,
        triggerType: TriggerTypes.NOW,
        status: TaskStatus.ACTIVE,
      });

      await taskService.update(TASK_ID_MOCK, payload, getMockCrudRequest(TASK_ID_MOCK));
      expect(taskService.unassign).toHaveBeenCalled();
    });

    it('should recalculate dueDate on trigger type update', async () => {
      const now = Date.now();
      Date.now = jest.fn().mockReturnValue(now);
      const expectedDueDate = moment(now)
        .add(2, 'days')
        .toDate();
      jest
        .spyOn(taskService, 'updateOne')
        .mockImplementation(async (req: CrudRequest, task: DeepPartial<Task>) => task as Task);
      jest
        .spyOn((taskService as any).manager, 'getRepository')
        .mockImplementationOnce((entity: any) => {
          expect(entity).toEqual(CalendarTask);
          return new MockRepo();
        })
        .mockImplementationOnce((entity: any) => {
          expect(entity).toEqual(CountdownTask);
          return new MockRepo();
        });

      const payload = {
        title: 'Test Task Updated',
        classification: 'Engineer inspection',
        machineTypes: ['TTM', 'HRM'],
        assignedAsset: ASSET_ID_MOCK,
        status: TaskStatus.ACTIVE,
        triggerType: TriggerTypes.COUNTDOWN,
        countdownHours: 2,
        unitIntervals: 'days',
      };

      const expected = {
        ...defaultFields,
        title: 'Test Task Updated',
        id: TASK_ID_MOCK,
        classification: 'Engineer inspection',
        machineTypes: ['TTM', 'HRM'],
        dueDate: expectedDueDate,
        assignedAsset: ASSET_ID_MOCK,
        status: TaskStatus.ACTIVE,
        triggerType: TriggerTypes.COUNTDOWN,
        countdownHours: 2,
        unitIntervals: 'days',
        calendarDate: new Date(),
      };

      // mock existing task
      (taskService as any).repo.findOneOrFail.mockResolvedValue({
        ...defaultFields,
        id: TASK_ID_MOCK,
        status: TaskStatus.ACTIVE,
        triggerType: TriggerTypes.CALENDAR,
        calendarDate: new Date(),
        dueDate: new Date(),
        assignedAsset: ASSET_ID_MOCK,
      });

      const result = await taskService.update(
        TASK_ID_MOCK,
        payload,
        getMockCrudRequest(TASK_ID_MOCK),
      );
      expect(result).toEqual(expected as any);
    });
  });

  describe('activate method', () => {
    it('should throw error on attempt to activate already active task', async () => {
      jest.spyOn(taskService, 'assign');
      jest.spyOn(taskService as any, 'activate');

      // mock existing task to be updated
      (taskService as any).repo.findOneOrFail.mockResolvedValue({
        ...defaultFields,
        id: TASK_ID_MOCK,
        assignedAsset: ASSET_ID_MOCK,
        status: TaskStatus.ACTIVE,
      });
      await ((expect(
        taskService.assign(TASK_ID_MOCK, ASSET_ID_MOCK),
      ) as unknown) as jest.JestMatchers<any>).rejects.toBeInstanceOf(BadRequestException);
    });
  });

  describe('mark done method', () => {
    it('should not throw error on attempt to mark done already resolved task', async () => {
      jest.spyOn(taskService, 'getLinkedFiles').mockImplementation(async (task: Task) => {
        return FILES as FileDto[];
      });
      jest.spyOn(taskService, 'markDone');

      // mock existing task to be updated
      (taskService as any).repo.findOneOrFail.mockResolvedValue({
        ...defaultFields,
        id: TASK_ID_MOCK,
        assignedAsset: ASSET_ID_MOCK,
        status: TaskStatus.DONE,
      });

      await ((expect(taskService.markDone(TASK_ID_MOCK)) as unknown) as jest.JestMatchers<
        any
      >).resolves.toBeDefined();
    });

    it('should repeat recurring task on resolve with dueDate 1 hour until now', async () => {
      const now = Date.now();
      Date.now = jest.fn().mockReturnValue(now);
      const spy = jest.spyOn(taskService, 'computeNewDueDate');
      (taskService as any).copyRefs = jest.fn().mockImplementation(() => {});

      const calendarDate = new Date('2020-01-01T01:01:01');
      const expectedDate = moment(now)
        .add(1, 'hour')
        .toDate();

      // mock existing task to be updated
      (taskService as any).repo.findOneOrFail.mockResolvedValue({
        ...defaultFields,
        id: TASK_ID_MOCK,
        assignedAsset: ASSET_ID_MOCK,
        status: TaskStatus.ACTIVE,
        triggerType: TriggerTypes.CALENDAR,
        repeatInterval: RepeatTypes.HOURLY,
        calendarDate,
      });
      await taskService.markDone(TASK_ID_MOCK);
      expectComputedDateCall(spy, expectedDate);
    });

    it('should repeat recurring task on resolve with dueDate 1 hour until now', async () => {
      const now = Date.now();
      Date.now = jest.fn().mockReturnValue(now);
      jest.spyOn(taskService, 'markDone');
      const spy = jest.spyOn(taskService, 'computeNewDueDate');
      (taskService as any).copyRefs = jest.fn().mockImplementation(() => {});

      const calendarDate = new Date('2020-01-01T01:01:01');
      const expectedDate = moment(now)
        .add(1, 'hour')
        .toDate();

      // mock existing task to be updated
      (taskService as any).repo.findOneOrFail.mockResolvedValueOnce({
        ...defaultFields,
        id: TASK_ID_MOCK,
        assignedAsset: ASSET_ID_MOCK,
        status: TaskStatus.ACTIVE,
        triggerType: TriggerTypes.CALENDAR,
        repeatInterval: RepeatTypes.HOURLY,
        calendarDate,
      });
      await taskService.markDone(TASK_ID_MOCK);
      expectComputedDateCall(spy, expectedDate);
    });
  });

  describe('copy method', () => {
    it('should create copy of the task with status inactive and new dueDate', async () => {
      const now = Date.now();
      Date.now = jest.fn().mockReturnValue(now);
      jest.spyOn(taskService, 'clone');
      jest.spyOn(taskService, 'copy');
      jest.spyOn(taskService, 'assign');
      const spy = jest.spyOn(taskService, 'computeNewDueDate');
      jest.spyOn(taskService as any, 'saveTask');
      (taskService as any).copyRefs = jest.fn().mockImplementation(() => {});
      const calendarDate = new Date('2020-01-01T01:01:01');
      const expectedDate = new Date('2020-01-01T01:01:01');

      const payload = {
        ...defaultFields,
        id: TASK_ID_MOCK,
        assignedAsset: ASSET_ID_MOCK,
        status: TaskStatus.ACTIVE,
        triggerType: TriggerTypes.CALENDAR,
        repeatInterval: RepeatTypes.HOURLY,
        calendarDate,
      };
      // mock existing task to be updated
      (taskService as any).repo.findOneOrFail.mockResolvedValue(payload);

      await taskService.clone(TASK_ID_MOCK);

      expectComputedDateCall(spy, expectedDate);
      expect((taskService as any).saveTask).toHaveBeenCalledWith({
        title: 'Copy of Test Task #123',
        status: TaskStatus.INACTIVE,
        assignedAsset: undefined,
        dueDate: expectedDate,
        assignedPerson: 'user',
        repeatInterval: RepeatTypes.HOURLY,
        triggerType: TriggerTypes.CALENDAR,
        calendarDate,
        createdBy: 'user',
        classification: 'Engineer inspection',
        machineTypes: [],
      });
    });
  });
});
