import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { Connection, EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';

import { AssetService } from '../asset/asset.service';

import { Task } from './entities/task.entity';

@Injectable()
export class TaskAssetNameSubscriber implements EntitySubscriberInterface<Task> {
  constructor(@InjectConnection() connection: Connection, private assetService: AssetService) {
    connection.subscribers.push(this);
  }

  listenTo() {
    return Task;
  }

  async beforeInsert(event: InsertEvent<Task>): Promise<void> {
    if (event.entity.assignedAsset) {
      const asset = await this.assetService.findOne(event.entity.assignedAsset);
      if (asset) {
        event.entity.assetName = asset.name;
      }
    }
  }

  async beforeUpdate(event: UpdateEvent<Task>): Promise<void> {
    if (event.databaseEntity && event.databaseEntity.assignedAsset !== event.entity.assignedAsset) {
      if (event.entity.assignedAsset) {
        const asset = await this.assetService.findOne(event.entity.assignedAsset);
        if (asset) {
          event.entity.assetName = asset.name;
          return;
        }
      }
      event.entity.assetName = null;
    }
  }
}
