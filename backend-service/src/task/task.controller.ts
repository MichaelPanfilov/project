import { CreateTaskDto, TaskAcl } from '@common';
import { Controller, Param, Post, Req } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiExtraModels,
  ApiOkResponse,
  ApiTags,
  refs,
} from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Request } from 'express';

import { AllowRights, getResponseForOneOf, getRoutesFor } from '../util/api';

import { CalendarTask, CountdownTask, NowTask, Task } from './entities/task.entity';
import { TaskService } from './task.service';

@Crud({
  model: {
    type: Task,
  },
  params: {
    taskId: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  routes: getRoutesFor(
    { type: Task, schema: refs(NowTask, CalendarTask, CountdownTask) },
    {
      getOneBase: {
        decorators: [AllowRights([TaskAcl.READ])],
      },
      getManyBase: {
        decorators: [AllowRights([TaskAcl.READ])],
      },
      createOneBase: { decorators: [AllowRights([TaskAcl.CREATE])] },
      replaceOneBase: { decorators: [AllowRights([TaskAcl.UPDATE])] },
      deleteOneBase: { decorators: [AllowRights([TaskAcl.DELETE])] },
    },
  ),
  query: {
    alwaysPaginate: true,
  },
})
@Controller('/v1/tasks')
@ApiBearerAuth()
@ApiTags('Task')
@ApiExtraModels(NowTask, CalendarTask, CountdownTask)
export class TaskController implements CrudController<Task> {
  constructor(public service: TaskService) {}

  @Override('createOneBase')
  @AllowRights([TaskAcl.CREATE])
  @ApiBody({
    schema: {
      oneOf: refs(NowTask, CalendarTask, CountdownTask),
    },
  })
  async createOne(@Req() req: Request, @ParsedBody() dto: CreateTaskDto): Promise<Task> {
    return this.service.create(dto, req.auth.userId);
  }

  @Override('replaceOneBase')
  @AllowRights([TaskAcl.UPDATE])
  @ApiBody({
    schema: {
      oneOf: refs(NowTask, CalendarTask, CountdownTask),
    },
  })
  async replaceOne(
    @Param('taskId') taskId: string,
    @ParsedBody() task: Partial<Task>,
    @ParsedRequest() req: CrudRequest,
  ): Promise<Task> {
    return this.service.update(taskId, task, req);
  }

  @Override('deleteOneBase')
  @AllowRights([TaskAcl.DELETE])
  async deleteOne(
    @Param('taskId') taskId: string,
    @ParsedRequest() crudReq: CrudRequest,
  ): Promise<void> {
    await this.service.delete(taskId, crudReq);
  }

  @Post('/:taskId/asset/:assetId')
  @AllowRights([TaskAcl.UPDATE])
  @ApiOkResponse({
    description: 'Assigns asset to task. Computes dueDate if asset trigger type is countdown',
    type: getResponseForOneOf(Task, refs(NowTask, CalendarTask, CountdownTask)),
  })
  async assignTask(@Param('taskId') taskId: string, @Param('assetId') assetId: string) {
    return this.service.assign(taskId, assetId);
  }

  @Post('/:taskId/mark-done')
  @AllowRights([TaskAcl.EXECUTE])
  @ApiOkResponse({
    description: 'Resolves an active task. Clones a Task if has repeated interval',
    type: getResponseForOneOf(Task, refs(NowTask, CalendarTask, CountdownTask)),
  })
  async resolveTask(@Param('taskId') taskId: string) {
    return this.service.markDone(taskId);
  }

  @Post(':taskId/clone')
  @AllowRights([TaskAcl.CREATE])
  @ApiOkResponse({
    description: 'Clone task',
    type: getResponseForOneOf(Task, refs(NowTask, CalendarTask, CountdownTask)),
  })
  async cloneTask(@Param('taskId') taskId: string) {
    return this.service.clone(taskId);
  }
}
