import { ApiCreatedResponse, ApiOkResponse } from '@nestjs/swagger';
import {
  ReferenceObject,
  SchemaObject,
} from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { RoutesOptions } from '@nestjsx/crud';
import { ParsedRequestParams } from '@nestjsx/crud-request';
import { Brackets, SelectQueryBuilder } from 'typeorm';

import { PaginationTransformInterceptor } from './interceptors';
import {
  getResponseFor,
  getResponseForOneOf,
  getResponseForOneOfPagination,
  getResponseForPagination,
} from './responses';

export function getRoutesFor(
  typeOrSchema: Function | { schema: (SchemaObject | ReferenceObject)[]; type: Function },
  overrides: Exclude<RoutesOptions, 'createManyBase' | 'updateOneBase'> = {},
): RoutesOptions {
  return {
    exclude: Array.from(new Set(getOpts(['createManyBase', 'updateOneBase'], overrides.exclude))),
    getOneBase: {
      decorators: getOpts(
        [
          ApiOkResponse({
            type:
              typeof typeOrSchema === 'function'
                ? getResponseFor(typeOrSchema)
                : getResponseForOneOf(typeOrSchema.type, typeOrSchema.schema),
          }),
        ],
        overrides.getOneBase ? overrides.getOneBase.decorators : [],
      ),
    },
    getManyBase: {
      decorators: getOpts(
        [
          ApiOkResponse({
            type:
              typeof typeOrSchema === 'function'
                ? getResponseForPagination(typeOrSchema)
                : getResponseForOneOfPagination(typeOrSchema.type, typeOrSchema.schema),
          }),
        ],
        overrides.getManyBase ? overrides.getManyBase.decorators : [],
      ),
      interceptors: [
        getOpts(
          [PaginationTransformInterceptor],
          overrides.getManyBase ? overrides.getManyBase.interceptors : [],
        ),
      ],
    },
    createOneBase: {
      decorators: getOpts(
        [
          ApiCreatedResponse({
            type:
              typeof typeOrSchema === 'function'
                ? getResponseFor(typeOrSchema)
                : getResponseForOneOf(typeOrSchema.type, typeOrSchema.schema),
          }),
        ],
        overrides.createOneBase ? overrides.createOneBase.decorators : [],
      ),
      interceptors: getOpts(
        [],
        overrides.createOneBase ? overrides.createOneBase.interceptors : [],
      ),
    },
    replaceOneBase: {
      decorators: getOpts(
        [
          ApiOkResponse({
            type:
              typeof typeOrSchema === 'function'
                ? getResponseFor(typeOrSchema)
                : getResponseForOneOf(typeOrSchema.type, typeOrSchema.schema),
          }),
        ],
        overrides.replaceOneBase ? overrides.replaceOneBase.decorators : [],
      ),
    },
    deleteOneBase: {
      decorators: overrides.deleteOneBase ? overrides.deleteOneBase.decorators : [],
    },
  };
}

export function getRoutesForRetrieveAndDelete(
  typeOrSchema: Function | { schema: (SchemaObject | ReferenceObject)[]; type: Function },
  overrides: Exclude<
    RoutesOptions,
    'createManyBase' | 'replaceOneBase' | 'createOneBase' | 'updateOneBase'
  > = {},
): RoutesOptions {
  return getRoutesFor(typeOrSchema, {
    exclude: ['createManyBase', 'replaceOneBase', 'createOneBase', 'updateOneBase'],
    getOneBase: overrides.getOneBase,
    getManyBase: overrides.getManyBase,
  });
}

function getOpts<T>(options: T[], overrides: T[] = []): T[] {
  return [...options, ...overrides];
}

/**
 * Extracts a filter from crud parsed params.
 */
export function extractParamsFilter<T>(parsed: ParsedRequestParams, field: keyof T) {
  // Find the field
  const index = parsed.filter.findIndex(f => f.field === field);
  const items = [] as string[];
  if (index > -1) {
    // Filter out the field filter from @nestjsx/crud parsed request and keep them.
    items.push(...parsed.filter[index].value);
    parsed.filter.splice(index, 1);
  }

  const and = parsed.search.$and;
  if (and) {
    // Filter out the field filter from @nestjsx/crud parsed request.
    parsed.search.$and = and.filter(a => a && !Object.keys(a).includes(field as string));
  }
  // Return the modified request and filtered items.
  return { parsed, items };
}

/**
 * Simple array column type in typeorm must be filtered with LIKE operator, as it stored as
 * text in the database. This function adds a OR WHERE clause for every item in the array,
 * which makes the clause to act as an WHERE IN operator on that array.
 */
export function addSimpleArrayWhere<T>(
  builder: SelectQueryBuilder<T>,
  alias: string,
  field: keyof T,
  arr: string[],
) {
  if (arr.length) {
    // Add custom where query for items in array.
    const brackets = new Brackets(qb =>
      arr.forEach(item => {
        // Create unique aliases.
        const [s, ns] = process.hrtime();
        const value = `${field}${s}${ns}`;
        qb.orWhere(`${alias}.${field} LIKE :${value}`, { [value]: `%${item}%` });
      }),
    );
    return builder.andWhere(brackets);
  }
  return builder;
}
