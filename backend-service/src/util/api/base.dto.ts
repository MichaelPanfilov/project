import { BaseDto } from '@common';
import { ApiResponseProperty } from '@nestjs/swagger';
import { PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';

export class BaseModel implements BaseDto {
  @PrimaryGeneratedColumn('uuid')
  @ApiResponseProperty()
  id!: string;
}

export abstract class BaseModelPrefixed implements BaseDto {
  @PrimaryColumn({ type: 'varchar' })
  @ApiResponseProperty()
  id!: string;

  constructor(id: string) {
    this.id = id;
  }
}
