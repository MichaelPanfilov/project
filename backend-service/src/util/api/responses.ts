import { DataResponse, DataResponseMeta, PagingResponseMeta } from '@common';
import { ApiProperty } from '@nestjs/swagger';
import {
  ReferenceObject,
  SchemaObject,
} from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export class ApiResponse<T, U = DataResponseMeta> implements DataResponse<T, U> {
  @ApiProperty({ type: Object })
  data!: T;

  @ApiProperty({ type: Object })
  meta!: U;
}

export class ApiPaginationMeta implements PagingResponseMeta {
  @ApiProperty({ type: Number })
  count!: number;

  @ApiProperty({ type: Number })
  total!: number;

  @ApiProperty({ type: Number })
  page!: number;

  @ApiProperty({ type: Number })
  pageCount!: number;
}

export class ApiManyData<T, U = DataResponseMeta> extends ApiResponse<T[], U> {
  @ApiProperty({ type: Object, isArray: true })
  data!: T[];

  @ApiProperty({ type: Object })
  meta!: U;
}

export class ApiPaginationData<T> implements DataResponse<T[]> {
  @ApiProperty({ type: Object, isArray: true })
  data!: T[];

  @ApiProperty({ type: ApiPaginationMeta })
  meta!: ApiPaginationMeta;
}

export function getResponseFor<T extends Function>(type: T): typeof ApiResponse {
  class ApiResponseForEntity extends ApiResponse<T> {
    @ApiProperty({ type })
    data!: T;
  }
  Object.defineProperty(ApiResponseForEntity, 'name', {
    value: `ApiResponseForEntity${type.name}`,
  });

  return ApiResponseForEntity as typeof ApiResponse;
}

export function getResponseForMany<T extends Function>(type: T): typeof ApiResponse {
  class ApiResponseForEntityArray extends ApiManyData<T> {
    @ApiProperty({
      type,
      isArray: true,
    })
    data!: T[];
  }
  Object.defineProperty(ApiResponseForEntityArray, 'name', {
    value: `ApiResponseForEntityMany${type.name}`,
  });

  return ApiResponseForEntityArray as typeof ApiResponse;
}

export function getResponseForPagination<T extends Function>(type: T): typeof ApiResponse {
  class ApiResponseForEntityArray extends ApiPaginationData<T> {
    @ApiProperty({
      type,
      isArray: true,
    })
    data!: T[];
  }
  Object.defineProperty(ApiResponseForEntityArray, 'name', {
    value: `ApiResponseForEntityManyPaginated${type.name}`,
  });

  return ApiResponseForEntityArray as typeof ApiResponse;
}

export function getResponseForOneOf<T extends Function>(
  type: T,
  oneOf: (SchemaObject | ReferenceObject)[],
): typeof ApiResponse {
  class ApiResponseForEntity extends ApiResponse<T> {
    @ApiProperty({ oneOf })
    data!: T;
  }
  Object.defineProperty(ApiResponseForEntity, 'name', {
    value: `ApiResponseForOneOfEntity${type.name}`,
  });

  return ApiResponseForEntity as typeof ApiResponse;
}

export function getResponseForOneOfMany<T extends Function>(
  type: T,
  oneOf: (SchemaObject | ReferenceObject)[],
): typeof ApiResponse {
  class ApiResponseForEntityArray extends ApiManyData<T> {
    @ApiProperty({
      type: 'array',
      items: { oneOf },
    })
    data!: T[];
  }
  Object.defineProperty(ApiResponseForEntityArray, 'name', {
    value: `ApiResponseForOneOfEntityMany${type.name}`,
  });

  return ApiResponseForEntityArray as typeof ApiResponse;
}

export function getResponseForOneOfPagination<T extends Function>(
  type: T,
  oneOf: (SchemaObject | ReferenceObject)[],
): typeof ApiResponse {
  class ApiResponseForEntityArray extends ApiPaginationData<T> {
    @ApiProperty({
      type: 'array',
      items: { oneOf },
    })
    data!: T[];
  }
  Object.defineProperty(ApiResponseForEntityArray, 'name', {
    value: `ApiResponseForOneOfEntityManyPaginated${type.name}`,
  });

  return ApiResponseForEntityArray as typeof ApiResponse;
}
