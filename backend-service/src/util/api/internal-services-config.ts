import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as jwt from 'jsonwebtoken';
import { join } from 'path';
import * as url from 'url';

export type ServiceName = 'acl' | 'file';

function getKey(keyName: string): Buffer {
  const certPath = join(
    process.cwd(),
    `/data/certs/${process.env.NODE_ENV === 'production' ? 'production' : 'dev'}`,
  );

  return fs.readFileSync(join(certPath, keyName));
}

function getInternalToken(internalKey: Buffer): string {
  return `Bearer ${jwt.sign({ isAllAllowed: true }, internalKey, { algorithm: 'RS256' })}`;
}

@Injectable()
export class InternalServicesConfig {
  readonly serviceUrls: { [key in ServiceName]: string } = {
    acl: process.env.APP_ACL_SERVICE_URL as string,
    file: process.env.APP_FILE_SERVICE_URL as string,
  };
  readonly publicKey = getKey('key.pub');
  readonly internalKey = getKey('internal.key');
  readonly authHeader = getInternalToken(this.internalKey);

  assertServiceUrls(...services: ServiceName[]) {
    const missing = services.find(
      s => !this.serviceUrls[s] || typeof this.serviceUrls[s] !== 'string',
    );
    if (missing) {
      throw new Error(`${missing} service URL is not set`);
    }
  }

  resolveUrl(service: ServiceName, path: string): string {
    return url.resolve(this.serviceUrls[service], path);
  }
}
