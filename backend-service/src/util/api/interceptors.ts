import { asResponse, DataResponse, PagingResponseMeta } from '@common';
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { GetManyDefaultResponse } from '@nestjsx/crud';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiResponse } from './responses';

@Injectable()
export class ResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<DataResponse<unknown>> {
    const now = Date.now();
    return next.handle().pipe(
      map(res => {
        const responseTime = Date.now() - now;
        if (res && isPaginatedResponse(res)) {
          return asResponse(res.data, {
            count: res.count,
            total: res.total,
            page: res.page,
            pageCount: res.pageCount,
            responseTime,
          });
        }
        return asResponse(res, { responseTime });
      }),
    );
  }
}

@Injectable()
export class PaginationTransformInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<GetManyDefaultResponse<unknown>>,
  ): Observable<ApiResponse<unknown, PagingResponseMeta>> {
    return next.handle().pipe(
      map(res =>
        asResponse(res.data, {
          count: res.count,
          total: res.total,
          page: res.page,
          pageCount: res.pageCount,
        }),
      ),
    );
  }
}

// tslint:disable-next-line
function isPaginatedResponse(res: any): res is GetManyDefaultResponse<unknown> {
  const keys: (keyof GetManyDefaultResponse<unknown>)[] = ['count', 'page', 'total'];
  return !keys.some(key => typeof res[key] !== 'number') && Array.isArray(res.data);
}
