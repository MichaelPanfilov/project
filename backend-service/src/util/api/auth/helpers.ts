import { Request } from 'express';

export function getAuthObj(req: Request) {
  // @ts-ignore
  return req.auth;
}
