import { Acl } from '@common';
import { CustomDecorator, SetMetadata } from '@nestjs/common';
import { Request } from 'express';

type RightDecorator = (acl: Acl[]) => CustomDecorator;
export const RIGHTS_KEY = 'requiredRights';
export const AllowRights: RightDecorator = (acl: Acl[]) => SetMetadata(RIGHTS_KEY, acl);

export type AllowedCallback = (req: Request) => boolean;
type AllowDecorator = (cb: AllowedCallback) => CustomDecorator;
export const CALLBACK_KEY = 'allowCb';
export const AllowIf: AllowDecorator = (cb: AllowedCallback) => SetMetadata(CALLBACK_KEY, cb);
