import { Acl, hasAclResource, matchesAcl } from '@common';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { Observable } from 'rxjs';

import { AllowedCallback, CALLBACK_KEY, RIGHTS_KEY } from './allow-rights';
import { getAuthObj } from './helpers';

abstract class AclGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  abstract hasRight(reqRights: string[], routeAcls: Acl[]): boolean;

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const handler = context.getHandler();
    const req = context.switchToHttp().getRequest() as Request;

    const cb = this.reflector.get<AllowedCallback | undefined>(CALLBACK_KEY, handler);

    if (cb && cb(req)) {
      return true;
    }

    const routeAcls = this.reflector.get<Acl[] | undefined>(RIGHTS_KEY, handler);

    if (!routeAcls) {
      return true;
    }

    const reqRights = getAuthObj(req).rights;
    if (Array.isArray(reqRights)) {
      if (this.hasRight(reqRights, routeAcls)) {
        return true;
      }
    }

    return false;
  }
}

@Injectable()
export class ResourceGuard extends AclGuard {
  constructor(reflector: Reflector) {
    super(reflector);
  }

  hasRight(reqRights: string[], routeAcls: Acl[]): boolean {
    return hasAclResource(reqRights, routeAcls);
  }
}

@Injectable()
export class RightGuard extends AclGuard {
  constructor(reflector: Reflector) {
    super(reflector);
  }

  hasRight(reqRights: string[], routeAcls: Acl[]): boolean {
    return matchesAcl(reqRights, routeAcls);
  }
}
