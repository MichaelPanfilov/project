import { JWTPayload, SESSION_COOKIE_NAME } from '@common';
import { UnauthorizedException } from '@nestjs/common';
import { INestApplication } from '@nestjs/common/interfaces';
import * as cookieParser from 'cookie-parser';
import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';

import { InternalServicesConfig } from '../internal-services-config';

const PUBLIC_ROUTES = ['/docs/', '/v1/licenses'];

function verifyToken(token: string, key: Buffer): boolean {
  try {
    return !!jwt.verify(token, key, { algorithms: ['RS512'] });
  } catch (e) {
    return false;
  }
}

function decodeToken(token: string): JWTPayload {
  return jwt.decode(token, { json: true }) as JWTPayload;
}

function createAuthorizationMiddleware(publicKey: Buffer) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (PUBLIC_ROUTES.some(route => req.url.startsWith(route))) {
      next();
      return;
    }
    // Somehow the headers are not written by nest in this middleware so we have to do it.
    res.header('Access-Control-Allow-Origin', '*');
    if (req.method.toLowerCase() === 'options') {
      next();
      return;
    }
    const authRequest = req.headers['authorization'] || req.cookies[SESSION_COOKIE_NAME];

    if (typeof authRequest !== 'string') {
      next(new UnauthorizedException('No authorization token provided'));
      return;
    }

    const token = authRequest.toLowerCase().startsWith('bearer ')
      ? authRequest.substring(7)
      : authRequest;

    if (!verifyToken(token, publicKey)) {
      next(new UnauthorizedException('Token invalid'));
      return;
    }

    // @ts-ignore
    req.auth = decodeToken(token);
    next();
  };
}

export function authMiddleware(app: INestApplication): void {
  const config = app.get(InternalServicesConfig);
  app.use(cookieParser());
  app.use(createAuthorizationMiddleware(config.publicKey));
}
