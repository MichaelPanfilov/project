import { AssetType, ASSET_HIERARCHY } from '@common';
import { registerDecorator, ValidationArguments } from 'class-validator';

import { AssetTree } from '../../asset/entities/asset.entity';

/**
 * Decorator to check weather the assets parent is of correct type.
 * @param assetType Type of the parent.
 */
export function AssetParent(assetType: AssetType | null) {
  return (asset: AssetTree, propertyName: string) => {
    registerDecorator({
      name: 'AssetParent',
      target: asset.constructor,
      propertyName,
      constraints: [assetType],
      validator: {
        validate(value: AssetTree, args: ValidationArguments) {
          const [type] = args.constraints as [AssetType];
          const hierarchy = ASSET_HIERARCHY[value.type];
          return hierarchy && hierarchy.parentType === type;
        },
      },
    });
  };
}

/**
 * Decorator to check weather the assets children are of correct type.
 * @param assetType Type of the children.
 */
export function AssetChildren(assetType: AssetType | null) {
  return (asset: AssetTree, propertyName: string) => {
    registerDecorator({
      name: 'AssetParent',
      target: asset.constructor,
      propertyName,
      constraints: [assetType],
      validator: {
        validate(value: AssetTree[], args: ValidationArguments) {
          const types = Array.from(new Set(value.map(v => v.type)));
          if (types.length !== 1) {
            return false;
          }
          const [type] = args.constraints as [AssetType];
          const hierarchy = ASSET_HIERARCHY[types[0]];
          return hierarchy && hierarchy.parentType === type;
        },
      },
    });
  };
}
