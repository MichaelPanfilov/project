import { asError } from '@common';
import { LogService } from '@elunic/logger';
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  NotFoundException,
} from '@nestjs/common';
import { Response } from 'express';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';

import { isAxiosError } from './helpers';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(private logger?: LogService) {}

  catch(err: Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();
    // Handle axios errors (they result from http calls to other services) differently.
    if (isAxiosError(err)) {
      const res = err.response || { status: 400, data: { error: {} } };
      if (this.logger) {
        this.logger.error([res.data.error, request.url]);
      }

      response.status(res.status).json(asError(res.data.error || res.data));
    } else {
      const error = err instanceof HttpException ? err : new HttpException(err, 400);
      if (this.logger) {
        this.logger.error([err, err.stack]);
      }

      response.status(error.getStatus()).json(asError(error));
    }
  }
}

@Catch(EntityNotFoundError)
export class TypeormExceptionFilter implements ExceptionFilter {
  catch(exception: EntityNotFoundError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const error = new NotFoundException(exception.message);

    response.status(error.getStatus()).json(asError(error));
  }
}
