import { ValueTransformer } from 'typeorm';

import { sortTags } from '../../../common/src/model';

export class TagsSortTransformer implements ValueTransformer {
  to(value?: string[]): string[] | undefined {
    return sortTags(value);
  }

  from(value?: string[]): string[] | undefined {
    return value;
  }
}
