import { Controller, Get, Param } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';

import { UserAcl } from '../../../common/src/auth';
import { AllowRights, getResponseFor, getRoutesFor } from '../util/api';

import { ActorService } from './actor.service';
import { Actor } from './entities/actor.entity';

@Crud({
  model: {
    type: Actor,
  },
  params: {
    actorId: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  routes: getRoutesFor(Actor, {
    exclude: ['createOneBase', 'replaceOneBase'],
    getOneBase: { decorators: [AllowRights([UserAcl.READ])] },
    getManyBase: { decorators: [AllowRights([UserAcl.READ])] },
    deleteOneBase: { decorators: [AllowRights([UserAcl.DELETE])] },
  }),
  query: {
    alwaysPaginate: true,
  },
})
@Controller('/v1/actors')
@ApiTags('Actor')
@ApiBearerAuth()
export class ActorController implements CrudController<Actor> {
  constructor(public service: ActorService) {}

  @Get('/acl/:aclId')
  @ApiOkResponse({ type: getResponseFor(Actor) })
  async getOne(@Param('aclId') aclId: string): Promise<Actor> {
    return this.service.getByAclId(aclId);
  }
}
