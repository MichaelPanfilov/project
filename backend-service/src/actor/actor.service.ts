import { HttpService, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { InternalServicesConfig } from '../util/api';

import { ActorBaseService } from './actor-base.service';
import { Actor } from './entities/actor.entity';

@Injectable()
export class ActorService extends ActorBaseService<Actor> {
  constructor(
    @InjectRepository(Actor) repo: Repository<Actor>,
    internalServiceConfig: InternalServicesConfig,
    http: HttpService,
  ) {
    super(repo, internalServiceConfig, http);
  }
}
