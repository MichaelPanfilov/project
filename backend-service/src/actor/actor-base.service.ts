import { AclUserDto, CreateAclUserDto, CreateActorDto, DataResponse } from '@common';
import { HttpService } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { map } from 'rxjs/operators';
import { Repository } from 'typeorm';

import { InternalServicesConfig, isAxiosError } from '../util/api';

import { Actor } from './entities/actor.entity';

export abstract class ActorBaseService<T extends Actor> extends TypeOrmCrudService<T> {
  constructor(
    repo: Repository<T>,
    protected internalServiceConfig: InternalServicesConfig,
    protected http: HttpService,
  ) {
    super(repo);
    this.internalServiceConfig.assertServiceUrls('acl');
  }

  async getByAclId(aclId: string): Promise<T> {
    return this.repo.findOneOrFail({ where: { aclId } });
  }

  async create(dto: CreateActorDto): Promise<T> {
    const { name, password, email, ...actor } = dto;
    // For now set name to username.
    const { id } = await this.createAclUser({ username: name, password, email });
    // tslint:disable-next-line
    return this.repo.save(this.repo.create({ ...actor, name, aclId: id } as any) as any);
  }

  async delete(actorId: string): Promise<void> {
    const actor = await this.repo.findOneOrFail(actorId);
    try {
      await this.deleteAclUser(actor.aclId);
    } catch (e) {
      // Catch NotFoundError and proceed with deleting the actor.
      if (!(isAxiosError(e) && e.response && e.response.status === 404)) {
        throw e;
      }
    }
    await this.repo.delete(actorId);
  }

  private createAclUser(actor: CreateAclUserDto): Promise<AclUserDto> {
    return this.http
      .post<DataResponse<AclUserDto>>(
        this.internalServiceConfig.resolveUrl('acl', `v1/users`),
        actor,
        {
          headers: { authorization: this.internalServiceConfig.authHeader },
        },
      )
      .pipe(map(res => res.data.data))
      .toPromise();
  }

  private async deleteAclUser(aclId: string): Promise<void> {
    await this.http
      .delete<DataResponse<void>>(
        this.internalServiceConfig.resolveUrl('acl', `v1/users/${aclId}`),
        {
          headers: { authorization: this.internalServiceConfig.authHeader },
        },
      )
      .toPromise();
  }
}
