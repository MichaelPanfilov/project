import {
  ActorDto,
  ActorType,
  generateId,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
  ReferencedEntities,
  USERNAME_MAX_LENGTH,
  USERNAME_MIN_LENGTH,
} from '@common';
import { ApiProperty, ApiResponseProperty } from '@nestjs/swagger';
import { CrudValidationGroups } from '@nestjsx/crud';
import { IsEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { ChildEntity, Column, Entity, TableInheritance } from 'typeorm';

import { BaseModelPrefixed } from '../../util/api';

const { CREATE } = CrudValidationGroups;

@Entity()
@TableInheritance({ column: { type: 'enum', enum: ActorType, name: 'type' } })
export abstract class Actor<T extends ActorType = ActorType> extends BaseModelPrefixed
  implements ActorDto {
  @Column()
  @IsOptional({ groups: [CREATE] })
  @IsString()
  @MinLength(USERNAME_MIN_LENGTH)
  @MaxLength(USERNAME_MAX_LENGTH)
  name!: string;

  @ApiResponseProperty({ enum: ActorType })
  type!: T;

  @Column()
  @IsEmpty()
  @ApiResponseProperty()
  aclId!: string;

  // We need to implement the constructor here to ensure the type is returned.
  // STI does not return the type by itself.
  // All sub classes have to call the constructor.
  constructor(type: T) {
    super(generateId(ReferencedEntities.ACTOR));
    this.type = type;
  }
}

@ChildEntity(ActorType.ACTOR)
export class BaseActor extends Actor<ActorType.ACTOR> {
  @ApiResponseProperty({ enum: [ActorType.ACTOR] })
  type!: ActorType.ACTOR;

  constructor() {
    super(ActorType.ACTOR);
  }
}

// tslint:disable-next-line
export function toCreateActorDto(Ctor: new (...args: any) => Actor): typeof Actor {
  class ActorWithPassword extends Ctor {
    @ApiProperty({ minLength: USERNAME_MIN_LENGTH, maxLength: USERNAME_MAX_LENGTH })
    name!: string;

    // Only required for creation of acl user.
    @IsString()
    @MinLength(PASSWORD_MIN_LENGTH)
    @MaxLength(PASSWORD_MAX_LENGTH)
    @ApiProperty({
      writeOnly: true,
      minLength: PASSWORD_MIN_LENGTH,
      maxLength: PASSWORD_MAX_LENGTH,
    })
    password!: string;

    @ApiResponseProperty()
    aclId!: string;
  }
  Object.defineProperty(ActorWithPassword, 'name', {
    value: `Create${Ctor.name}Dto`,
  });

  return ActorWithPassword as typeof Actor;
}
