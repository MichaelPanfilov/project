import { ActorType, Languages, UserDto } from '@common';
import { ApiPropertyOptional, ApiResponseProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsOptional } from 'class-validator';
import { ChildEntity, Column } from 'typeorm';

import { Actor } from './actor.entity';

@ChildEntity(ActorType.USER)
export class User extends Actor<ActorType.USER> implements UserDto {
  @ApiResponseProperty({ enum: [ActorType.USER] })
  type!: ActorType.USER;

  @Column({ type: 'enum', enum: Languages, default: Languages.ENGLISH })
  @IsEnum(Languages)
  @IsOptional()
  @ApiPropertyOptional({ default: Languages.ENGLISH })
  language!: Languages;

  @IsEmail()
  @IsOptional()
  @ApiPropertyOptional()
  email?: string;

  constructor() {
    super(ActorType.USER);
  }
}
