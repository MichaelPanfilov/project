import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { InternalServicesConfig } from '../util/api';

import { ActorController } from './actor.controller';
import { ActorService } from './actor.service';
import { Actor, BaseActor } from './entities/actor.entity';
import { User } from './entities/user.entity';
import { UserController, UserService } from './user';

@Module({
  imports: [TypeOrmModule.forFeature([Actor, BaseActor, User]), HttpModule],
  providers: [ActorService, UserService, InternalServicesConfig],
  exports: [ActorService, UserService],
  controllers: [ActorController, UserController],
})
export class ActorModule {}
