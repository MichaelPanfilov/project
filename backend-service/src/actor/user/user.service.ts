import { HttpService, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { InternalServicesConfig } from '../../util/api';
import { ActorBaseService } from '../actor-base.service';
import { User } from '../entities/user.entity';

@Injectable()
export class UserService extends ActorBaseService<User> {
  constructor(
    @InjectRepository(User) repo: Repository<User>,
    internalServiceConfig: InternalServicesConfig,
    http: HttpService,
  ) {
    super(repo, internalServiceConfig, http);
  }
}
