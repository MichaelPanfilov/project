import { CreateUserDto, matchesAcl, UserAcl } from '@common';
import { ForbiddenException, Get, Param, Req } from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Request } from 'express';

import { AllowRights, getAuthObj, getResponseFor, getRoutesFor } from '../../util/api';
import { toCreateActorDto } from '../entities/actor.entity';
import { User } from '../entities/user.entity';

import { UserService } from './user.service';

@Crud({
  model: {
    type: User,
  },
  params: {
    userId: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  routes: getRoutesFor(User, {
    getOneBase: { decorators: [AllowRights([UserAcl.READ])] },
    getManyBase: { decorators: [AllowRights([UserAcl.READ])] },
    createOneBase: { decorators: [AllowRights([UserAcl.CREATE])] },
    deleteOneBase: { decorators: [AllowRights([UserAcl.DELETE])] },
  }),
  query: {
    alwaysPaginate: true,
  },
})
@Controller('/v1/users')
@ApiTags('User')
@ApiBearerAuth()
export class UserController implements CrudController<User> {
  constructor(public service: UserService) {}

  @Override('createOneBase')
  @ApiBody({ type: toCreateActorDto(User) })
  createOne(@ParsedBody() dto: CreateUserDto): Promise<User> {
    return this.service.create(dto);
  }

  @Override('replaceOneBase')
  async replaceOne(
    @Req() req: Request,
    @ParsedRequest() crudReq: CrudRequest,
    @ParsedBody() dto: CreateUserDto,
  ): Promise<User> {
    // Users can update themselves, we need to check via the userId in auth, which is the acl userId, if
    // the requested user maps to that acl user.
    const { rights, userId } = getAuthObj(req);
    if (matchesAcl(rights, [UserAcl.UPDATE]) || (await this.isMe(userId, req))) {
      return this.service.replaceOne(crudReq, dto);
    }
    throw new ForbiddenException();
  }

  @Override('deleteOneBase')
  deleteOne(@Param('userId') userId: string): Promise<void> {
    return this.service.delete(userId);
  }

  @Get('/acl/:aclId')
  @ApiOkResponse({ type: getResponseFor(User) })
  async getOne(@Param('aclId') aclId: string): Promise<User> {
    return this.service.getByAclId(aclId);
  }

  private async isMe(aclId: string, req: Request): Promise<boolean> {
    return (await this.service.getByAclId(aclId)).id === req.params.userId;
  }
}
