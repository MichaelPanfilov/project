import { AssetDto, AssetType, ASSET_HIERARCHY } from '@common';
import { ForbiddenException } from '@nestjs/common';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { DeepPartial, EntityManager, Repository, TreeRepository } from 'typeorm';

import { AssetTree } from './entities/asset.entity';

export class AssetTreeService<
  T extends AssetTree,
  P extends AssetType,
  C extends AssetType
> extends TypeOrmCrudService<T> {
  private treeRepo: TreeRepository<AssetTree>;

  constructor(repo: Repository<T>, manager: EntityManager) {
    super(repo);
    this.treeRepo = manager.getTreeRepository(AssetTree);
  }

  async createOne(req: CrudRequest, dto: DeepPartial<T>): Promise<T> {
    const result = await super.createOne(req, dto);
    // Enables setting parent on creation of entity.
    if (dto.parent && typeof dto.parent === 'string') {
      return this.setParent(result.id, dto.parent);
    }
    return result;
  }

  async getAllTrees(): Promise<AssetTree[]> {
    const trees = await this.treeRepo.findRoots();
    return Promise.all(trees.map(tree => this.getFullTree(tree.id)));
  }

  async getFullTree(rootId: string): Promise<AssetTree> {
    const asset = await this.getAsset(rootId);
    return this.treeRepo.findDescendantsTree(asset);
  }

  async getParent(id: string): Promise<AssetTree<P> | undefined> {
    const asset = await this.getAsset(id);
    const tree = await this.treeRepo.findAncestorsTree(asset);
    return tree.parent ? (this.mapAsset(tree.parent) as AssetTree<P>) : undefined;
  }

  async getChildren(id: string): Promise<AssetDto<C>[]> {
    const asset = await this.getAsset(id);
    const tree = await this.treeRepo.findDescendantsTree(asset);
    return (tree.children as AssetTree<C>[]).map(child => this.mapAsset(child));
  }

  async setParent(id: string, parentId: string): Promise<T> {
    const asset = await this.getAsset(id);
    const parent = await this.treeRepo.findOneOrFail(parentId);
    this.assertParent(asset, parent);
    asset.parent = parent;
    return this.saveAsset(asset);
  }

  async removeParent(id: string): Promise<T> {
    const asset = await this.getAsset(id);
    asset.parent = undefined;
    return this.saveAsset(asset);
  }

  private getAsset(id: string): Promise<T> {
    return this.repo.findOneOrFail(id);
  }

  private saveAsset(asset: T): Promise<T> {
    // tslint:disable-next-line
    return this.treeRepo.save<any>(asset);
  }

  private mapAsset<R extends AssetType>(tree: AssetTree<R>): AssetDto<R> {
    const { id, name, description, type, position } = tree;
    return { id, name, description, type, position };
  }

  private assertParent(entity: T, parent: AssetTree): asserts entity {
    const allowedParentType = ASSET_HIERARCHY[entity.type].parentType;
    if (allowedParentType !== parent.type) {
      throw new ForbiddenException(
        `Parent of type '${parent.type}' can not be assigned to type '${entity.type}'. Allowed type: '${allowedParentType}'`,
      );
    }
  }
}
