import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { Connection, EntitySubscriberInterface, UpdateEvent } from 'typeorm';

import { TaskService } from '../task/task.service';

import { AssetTree } from './entities/asset.entity';

@Injectable()
export class AssetNameChangeSubscriber implements EntitySubscriberInterface<AssetTree> {
  constructor(@InjectConnection() connection: Connection, private taskService: TaskService) {
    connection.subscribers.push(this);
  }

  listenTo() {
    return AssetTree;
  }

  async beforeUpdate(event: UpdateEvent<AssetTree>): Promise<void> {
    if (event.entity.name !== event.databaseEntity.name) {
      await this.taskService.updateAssignedAssetName(event.entity.id, event.entity.name);
    }
  }
}
