import { AssetDto, AssetType, generateId, ReferencedEntities } from '@common';
import { ApiProperty, ApiPropertyOptional, ApiResponseProperty } from '@nestjs/swagger';
import { CrudValidationGroups } from '@nestjsx/crud';
import { IsInt, IsOptional, IsString, Min } from 'class-validator';
import { Column, Entity, TableInheritance, Tree, TreeChildren, TreeParent } from 'typeorm';

import { BaseModelPrefixed } from '../../util/api';

const { UPDATE } = CrudValidationGroups;

export abstract class Asset<T extends AssetType> extends BaseModelPrefixed implements AssetDto<T> {
  @ApiResponseProperty({ enum: AssetType })
  type!: T;

  @ApiProperty()
  @IsOptional({ groups: [UPDATE] })
  @IsString()
  @Column()
  name!: string;

  @ApiPropertyOptional({ default: '' })
  @IsOptional()
  @IsString()
  @Column({ default: '' })
  description!: string;

  @ApiProperty({ minimum: 0 })
  @IsInt()
  @Min(0)
  @Column()
  position!: number;

  // We need to implement the constructor here to ensure the type is returned.
  // STI does not return the type by itself.
  // All sub classes have to call the constructor.
  constructor(type: T) {
    super(generateId(ReferencedEntities.ASSET));
    this.type = type;
  }
}

export class GenericAsset extends Asset<AssetType> {}

@Entity()
@Tree('materialized-path')
@TableInheritance({ column: { type: 'enum', enum: AssetType, name: 'type' } })
export abstract class AssetTree<T extends AssetType = AssetType> extends Asset<T> {
  @ApiResponseProperty({ type: () => AssetTree })
  @TreeParent()
  parent?: AssetTree;

  @ApiProperty({ type: () => AssetTree, isArray: true })
  @TreeChildren()
  children!: AssetTree[];
}
