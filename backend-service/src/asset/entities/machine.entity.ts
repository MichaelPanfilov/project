import { AssetType, MachineDto, MachineTreeDto } from '@common';
import { ApiProperty, ApiPropertyOptional, ApiResponseProperty } from '@nestjs/swagger';
import { IsBoolean, IsInt, IsOptional, IsString } from 'class-validator';
import { ChildEntity, Column, TreeChildren, TreeParent } from 'typeorm';

import { Asset, AssetTree } from './asset.entity';
import { LineTree } from './line.entity';

export class Machine<T extends string = string> extends Asset<AssetType.MACHINE>
  implements MachineDto<T> {
  @ApiResponseProperty({ enum: [AssetType.MACHINE] })
  type!: AssetType.MACHINE;

  @ApiPropertyOptional({ default: '' })
  @IsOptional()
  @IsString()
  manufacturer?: string;

  // TODO: Set type to enum and default value
  @ApiPropertyOptional({ default: '' })
  @IsOptional()
  @IsString()
  machineType?: T;

  @ApiPropertyOptional({ default: '' })
  @IsOptional()
  @IsString()
  serialNumber?: string;

  @ApiPropertyOptional({ default: new Date().getFullYear() })
  @IsOptional()
  @IsInt()
  constructionYear?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  device?: string;

  @ApiPropertyOptional({ default: false })
  @IsBoolean()
  @IsOptional()
  isLineOutput?: boolean;

  @ApiProperty({ writeOnly: true })
  @IsString()
  parent?: string;
}

@ChildEntity(AssetType.MACHINE)
export class MachineTree<T extends string = string> extends AssetTree<AssetType.MACHINE>
  implements MachineTreeDto<T> {
  @Column({ default: '' })
  manufacturer?: string;

  // TODO: Set type to enum and default value
  @Column({ default: '' })
  machineType?: T;

  @Column({ default: '' })
  serialNumber?: string;

  @Column({ default: () => new Date().getFullYear() })
  constructionYear?: number;

  @Column({ length: 36, nullable: true })
  device?: string;

  @Column({ default: false })
  isLineOutput!: boolean;

  @TreeParent()
  parent!: LineTree;

  @TreeChildren()
  children!: never[];

  constructor() {
    super(AssetType.MACHINE);
  }
}
