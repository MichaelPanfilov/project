import { AssetType, LineDto, LineTreeDto } from '@common';
import { ApiProperty, ApiResponseProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { ChildEntity, TreeChildren, TreeParent } from 'typeorm';

import { Asset, AssetTree } from './asset.entity';
import { FactoryTree } from './factory.entity';
import { MachineTree } from './machine.entity';

export class Line extends Asset<AssetType.LINE> implements LineDto {
  @ApiResponseProperty({ enum: [AssetType.LINE] })
  type!: AssetType.LINE;

  @ApiProperty({ writeOnly: true })
  @IsString()
  parent?: string;
}

@ChildEntity(AssetType.LINE)
export class LineTree extends AssetTree<AssetType.LINE> implements LineTreeDto {
  @TreeParent()
  parent!: FactoryTree;

  @ApiProperty({ type: () => MachineTree, readOnly: true, isArray: true })
  @TreeChildren()
  children!: MachineTree[];

  constructor() {
    super(AssetType.LINE);
  }
}
