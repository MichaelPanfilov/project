import { AssetType, FactoryDto, FactoryTreeDto } from '@common';
import { ApiProperty, ApiPropertyOptional, ApiResponseProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { ChildEntity, Column, TreeChildren, TreeParent } from 'typeorm';

import { Asset, AssetTree } from './asset.entity';
import { LineTree } from './line.entity';

export class Factory extends Asset<AssetType.FACTORY> implements FactoryDto {
  @ApiResponseProperty({ enum: [AssetType.FACTORY] })
  type!: AssetType.FACTORY;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  postalCode?: string;
}

@ChildEntity(AssetType.FACTORY)
export class FactoryTree extends AssetTree<AssetType.FACTORY> implements FactoryTreeDto {
  @Column({ nullable: true })
  postalCode?: string;

  @TreeParent()
  parent!: never;

  @ApiProperty({ type: () => LineTree, readOnly: true, isArray: true })
  @TreeChildren()
  children!: LineTree[];

  constructor() {
    super(AssetType.FACTORY);
  }
}
