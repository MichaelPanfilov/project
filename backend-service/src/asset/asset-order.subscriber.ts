import { AssetType, sortByPosition } from '@common';
import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectEntityManager } from '@nestjs/typeorm';
import {
  Connection,
  EntityManager,
  EntitySubscriberInterface,
  InsertEvent,
  RemoveEvent,
  TreeRepository,
} from 'typeorm';

import { Asset, AssetTree } from './entities/asset.entity';

@Injectable()
export class AssetOrderSubscriber implements EntitySubscriberInterface<AssetTree> {
  private treeRepo: TreeRepository<AssetTree>;

  constructor(
    @InjectConnection() connection: Connection,
    @InjectEntityManager() manager: EntityManager,
  ) {
    connection.subscribers.push(this);
    this.treeRepo = manager.getTreeRepository(AssetTree);
  }

  listenTo() {
    return AssetTree;
  }

  async beforeInsert(event: InsertEvent<AssetTree>): Promise<void> {
    return this.handleInsert(event);
  }

  async beforeUpdate(event: InsertEvent<AssetTree>): Promise<void> {
    return this.handleInsert(event);
  }

  async beforeRemove(event: RemoveEvent<AssetTree>): Promise<void> {
    return this.handleRemove(event);
  }

  private async handleInsert(event: InsertEvent<AssetTree>): Promise<void> {
    const parentId = event.entity.parent;
    if (parentId) {
      const parent = await this.treeRepo.findOneOrFail(parentId);
      const tree = await this.treeRepo.findDescendantsTree(parent);
      return this.updateChildrenOrder(tree.children, event.entity);
    } else {
      // Handle factories.
      const roots = await this.treeRepo.findRoots();
      return this.updateChildrenOrder(roots, event.entity);
    }
  }

  private async handleRemove(event: RemoveEvent<AssetTree>): Promise<void> {
    if (!event.entity) {
      return;
    }

    const entity = await this.treeRepo.findAncestorsTree(event.entity);
    if (entity.parent) {
      const tree = await this.treeRepo.findDescendantsTree(entity.parent);
      const siblings = tree.children.filter(c => c.id !== entity.id);
      siblings.sort(sortByPosition);
      return this.normalizeAndUpdate(siblings);
    } else {
      // Handle factories.
      const roots = await this.treeRepo.findRoots();
      const siblings = roots.filter(c => c.id !== entity.id);
      siblings.sort(sortByPosition);
      return this.normalizeAndUpdate(siblings);
    }
  }

  private updateChildrenOrder(
    siblings: Asset<AssetType>[],
    insert: Asset<AssetType>,
  ): Promise<void> {
    siblings.sort(sortByPosition);
    const insertIndex = siblings.findIndex(s => s.position >= insert.position);

    if (insertIndex > -1) {
      // Push sibling into position.
      siblings.splice(insertIndex, 0, insert);
    } else {
      siblings.push(insert);
    }

    // Remove the duplicate entry for updates.
    const removeIndex = siblings.findIndex(s => s.id === insert.id && s !== insert);
    if (removeIndex > -1) {
      siblings.splice(removeIndex, 1);
    }

    return this.normalizeAndUpdate(siblings, insert);
  }

  private async normalizeAndUpdate(
    assets: Asset<AssetType>[],
    insert?: Asset<AssetType>,
  ): Promise<void> {
    // Normalize position (position within a the list of children should be unique).
    assets.forEach((a, i) => (a.position = i + 1));
    // Update siblings except the inserted one
    // (this is done by the actual call to the repo already, we just hook into that).
    await this.treeRepo.save(
      assets.filter(a => !insert || (a.id && a.id !== insert.id)),
      { listeners: false },
    );
  }
}
