import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { TaskModule } from '../task/task.module';

import { AssetNameChangeSubscriber } from './asset-name-change.subscriber';
import { AssetOrderSubscriber } from './asset-order.subscriber';
import { AssetRemovalSubscriber } from './asset-removal.subscriber';
import { AssetController } from './asset.controller';
import { AssetService } from './asset.service';
import { AssetTree } from './entities/asset.entity';
import { FactoryTree } from './entities/factory.entity';
import { LineTree } from './entities/line.entity';
import { MachineTree } from './entities/machine.entity';
import { FactoryController, FactoryService } from './factory';
import { LineController, LineService } from './line';
import { MachineController, MachineService } from './machine';

@Module({
  imports: [TypeOrmModule.forFeature([AssetTree, MachineTree, LineTree, FactoryTree]), TaskModule],
  exports: [AssetService, LineService, FactoryService, MachineService],
  controllers: [AssetController, LineController, FactoryController, MachineController],
  providers: [
    AssetService,
    LineService,
    FactoryService,
    MachineService,
    AssetOrderSubscriber,
    AssetRemovalSubscriber,
    AssetNameChangeSubscriber,
  ],
})
export class AssetModule {}
