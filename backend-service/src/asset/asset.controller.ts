import { AssetAcl } from '@common';
import { Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags, refs } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';

import {
  AllowRights,
  getResponseFor,
  getResponseForMany,
  getRoutesForRetrieveAndDelete,
} from '../util/api';

import { AssetService } from './asset.service';
import { AssetTree, GenericAsset } from './entities/asset.entity';
import { Factory } from './entities/factory.entity';
import { Line } from './entities/line.entity';
import { Machine } from './entities/machine.entity';

const routes = getRoutesForRetrieveAndDelete(
  { type: GenericAsset, schema: refs(Machine, Line, Factory) },
  {
    getOneBase: {
      decorators: [AllowRights([AssetAcl.READ])],
    },
    getManyBase: {
      decorators: [AllowRights([AssetAcl.READ])],
    },
    deleteOneBase: {
      decorators: [AllowRights([AssetAcl.DELETE])],
    },
  },
);

@Crud({
  model: {
    type: GenericAsset,
  },
  params: {
    assetId: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  routes,
})
@Controller('/v1/assets')
@ApiTags('Asset')
@ApiBearerAuth()
export class AssetController implements CrudController<GenericAsset> {
  constructor(public service: AssetService) {}

  @Get('/trees')
  @ApiOkResponse({ type: getResponseForMany(AssetTree) })
  @AllowRights([AssetAcl.READ])
  getAllTrees() {
    return this.service.getAllTrees();
  }

  @Get('/:assetId/parent')
  @ApiOkResponse({
    type: getResponseFor(GenericAsset),
    description: 'Returns undefined of asset does not have parent',
  })
  @AllowRights([AssetAcl.READ])
  getParent(@Param('assetId') assetId: string) {
    return this.service.getParent(assetId);
  }

  @Get('/:assetId/children')
  @ApiOkResponse({ type: getResponseForMany(GenericAsset) })
  @AllowRights([AssetAcl.READ])
  getChildren(@Param('assetId') assetId: string) {
    return this.service.getChildren(assetId);
  }

  @Post('/:assetId/parent/:parentId')
  @ApiOkResponse({ type: getResponseFor(GenericAsset) })
  @AllowRights([AssetAcl.UPDATE])
  setParent(@Param('assetId') assetId: string, @Param('parentId') parentId: string) {
    return this.service.setParent(assetId, parentId);
  }

  @Delete('/:assetId/parent')
  @ApiOkResponse({ type: getResponseFor(GenericAsset) })
  @AllowRights([AssetAcl.UPDATE])
  removeParent(@Param('assetId') assetId: string) {
    return this.service.removeParent(assetId);
  }

  @Get('/:assetId/tree')
  @ApiOkResponse({ type: getResponseFor(AssetTree) })
  @AllowRights([AssetAcl.READ])
  getTrees(@Param('assetId') assetId: string) {
    return this.service.getFullTree(assetId);
  }

  // TODO: Is this used?
  @Get('/:assetId')
  @ApiOkResponse({ type: getResponseFor(AssetTree) })
  @AllowRights([AssetAcl.READ])
  test(@Param('assetId') assetId: string) {
    return this.service.getFullTree(assetId);
  }
}
