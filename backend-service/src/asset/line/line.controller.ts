import { AssetAcl } from '@common';
import { Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';

import { AllowRights, getResponseFor, getResponseForMany, getRoutesFor } from '../../util/api';
import { Factory } from '../entities/factory.entity';
import { Line, LineTree } from '../entities/line.entity';
import { Machine } from '../entities/machine.entity';

import { LineService } from './line.service';

const routes = getRoutesFor(Line, {
  getOneBase: {
    decorators: [AllowRights([AssetAcl.READ])],
  },
  getManyBase: {
    decorators: [AllowRights([AssetAcl.READ])],
  },
  createOneBase: { decorators: [AllowRights([AssetAcl.CREATE])] },
  replaceOneBase: { decorators: [AllowRights([AssetAcl.UPDATE])] },
  deleteOneBase: { decorators: [AllowRights([AssetAcl.DELETE])] },
});

@Crud({
  model: {
    type: Line,
  },
  params: {
    lineId: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  routes,
})
@Controller('/v1/lines')
@ApiTags('Line')
export class LineController implements CrudController<LineTree> {
  constructor(public service: LineService) {}

  @Get('/:lineId/factory')
  @ApiOkResponse({ type: getResponseFor(Factory) })
  @AllowRights([AssetAcl.CREATE])
  getParent(@Param('lineId') lineId: string) {
    return this.service.getParent(lineId);
  }

  @Get('/:lineId/machines')
  @ApiOkResponse({ type: getResponseForMany(Machine) })
  @AllowRights([AssetAcl.CREATE])
  getChildren(@Param('lineId') lineId: string) {
    return this.service.getChildren(lineId);
  }

  @Post('/:lineId/factory/:factoryId')
  @ApiOkResponse({ type: getResponseFor(Line) })
  @AllowRights([AssetAcl.UPDATE])
  setParent(@Param('lineId') lineId: string, @Param('factoryId') factoryId: string) {
    return this.service.setParent(lineId, factoryId);
  }

  @Delete('/:lineId/factory')
  @ApiOkResponse({ type: getResponseFor(Line) })
  @AllowRights([AssetAcl.UPDATE])
  removeParent(@Param('lineId') lineId: string) {
    return this.service.removeParent(lineId);
  }
}
