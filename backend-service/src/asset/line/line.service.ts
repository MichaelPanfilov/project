import { AssetType } from '@common';
import { Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';

import { AssetTreeService } from '../asset-tree.service';
import { LineTree } from '../entities/line.entity';

@Injectable()
export class LineService extends AssetTreeService<LineTree, AssetType.FACTORY, AssetType.MACHINE> {
  constructor(
    @InjectRepository(LineTree) repo: Repository<LineTree>,
    @InjectEntityManager() manager: EntityManager,
  ) {
    super(repo, manager);
  }
}
