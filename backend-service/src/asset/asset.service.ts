import { Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';

import { AssetTreeService } from './asset-tree.service';
import { AssetTree } from './entities/asset.entity';

@Injectable()
// tslint:disable-next-line
export class AssetService extends AssetTreeService<AssetTree, any, any> {
  constructor(
    @InjectRepository(AssetTree) repo: Repository<AssetTree>,
    @InjectEntityManager() manager: EntityManager,
  ) {
    super(repo, manager);
  }
}
