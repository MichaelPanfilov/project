import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { Connection, EntitySubscriberInterface, RemoveEvent } from 'typeorm';

import { TaskService } from '../task/task.service';

import { AssetTree } from './entities/asset.entity';

@Injectable()
export class AssetRemovalSubscriber implements EntitySubscriberInterface<AssetTree> {
  constructor(@InjectConnection() connection: Connection, private taskService: TaskService) {
    connection.subscribers.push(this);
  }

  listenTo() {
    return AssetTree;
  }

  async beforeRemove(event: RemoveEvent<AssetTree>) {
    const tasks = await this.taskService.getByAssetId(event.entityId);
    await Promise.all(tasks.map(task => this.taskService.unassign(task.id)));
  }
}
