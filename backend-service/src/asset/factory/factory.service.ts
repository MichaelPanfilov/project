import { AssetType } from '@common';
import { Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';

import { AssetTreeService } from '../asset-tree.service';
import { FactoryTree } from '../entities/factory.entity';

@Injectable()
export class FactoryService extends AssetTreeService<FactoryTree, never, AssetType.LINE> {
  constructor(
    @InjectRepository(FactoryTree) repo: Repository<FactoryTree>,
    @InjectEntityManager() manager: EntityManager,
  ) {
    super(repo, manager);
  }
}
