import { AssetAcl } from '@common';
import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';

import { AllowRights, getResponseForMany, getRoutesFor } from '../../util/api';
import { Factory, FactoryTree } from '../entities/factory.entity';
import { Line } from '../entities/line.entity';

import { FactoryService } from './factory.service';

const routes = getRoutesFor(Factory, {
  getOneBase: {
    decorators: [AllowRights([AssetAcl.READ])],
  },
  getManyBase: {
    decorators: [AllowRights([AssetAcl.READ])],
  },
  createOneBase: { decorators: [AllowRights([AssetAcl.CREATE])] },
  replaceOneBase: { decorators: [AllowRights([AssetAcl.UPDATE])] },
  deleteOneBase: { decorators: [AllowRights([AssetAcl.DELETE])] },
});

@Crud({
  model: {
    type: Factory,
  },
  params: {
    factoryId: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  routes,
})
@Controller('/v1/factories')
@ApiTags('Factory')
export class FactoryController implements CrudController<FactoryTree> {
  constructor(public service: FactoryService) {}

  @Get('/:factoryId/lines')
  @ApiOkResponse({ type: getResponseForMany(Line) })
  @AllowRights([AssetAcl.READ])
  getChildren(@Param('factoryId') factoryId: string) {
    return this.service.getChildren(factoryId);
  }
}
