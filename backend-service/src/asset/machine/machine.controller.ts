import { AssetAcl } from '@common';
import { Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';

import { AllowRights, getResponseFor, getRoutesFor } from '../../util/api';
import { Line } from '../entities/line.entity';
import { Machine, MachineTree } from '../entities/machine.entity';

import { MachineService } from './machine.service';

const routes = getRoutesFor(Machine, {
  getOneBase: {
    decorators: [AllowRights([AssetAcl.READ])],
  },
  getManyBase: {
    decorators: [AllowRights([AssetAcl.READ])],
  },
  createOneBase: { decorators: [AllowRights([AssetAcl.CREATE])] },
  replaceOneBase: { decorators: [AllowRights([AssetAcl.UPDATE])] },
  deleteOneBase: { decorators: [AllowRights([AssetAcl.DELETE])] },
});

@Crud({
  model: {
    type: Machine,
  },
  params: {
    machineId: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  routes,
})
@Controller('/v1/machines')
@ApiTags('Machine')
export class MachineController implements CrudController<MachineTree> {
  constructor(public service: MachineService) {}

  @Get('/:machineId/line')
  @ApiOkResponse({ type: getResponseFor(Line) })
  @AllowRights([AssetAcl.READ])
  getParent(@Param('machineId') machineId: string) {
    return this.service.getParent(machineId);
  }

  @Post('/:machineId/line/:lineId')
  @ApiOkResponse({ type: getResponseFor(Machine) })
  @AllowRights([AssetAcl.UPDATE])
  setParent(@Param('machineId') machineId: string, @Param('lineId') lineId: string) {
    return this.service.setParent(machineId, lineId);
  }

  @Delete('/:machineId/line')
  @ApiOkResponse({ type: getResponseFor(Machine) })
  @AllowRights([AssetAcl.UPDATE])
  removeParent(@Param('machineId') machineId: string) {
    return this.service.removeParent(machineId);
  }
}
