import { AssetType } from '@common';
import { Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';

import { AssetTreeService } from '../asset-tree.service';
import { MachineTree } from '../entities/machine.entity';

@Injectable()
export class MachineService extends AssetTreeService<MachineTree, AssetType.LINE, never> {
  constructor(
    @InjectRepository(MachineTree) repo: Repository<MachineTree>,
    @InjectEntityManager() manager: EntityManager,
  ) {
    super(repo, manager);
  }
}
