import { LicenseContentTypes, LicenseTypes } from '@common';
import { Controller, Get, Query, Res } from '@nestjs/common';
import { Response } from 'express';
import * as path from 'path';

@Controller('v1/')
export class LicenseController {
  @Get('licenses')
  licenses(@Query('type') type: LicenseTypes = 'csv', @Res() res: Response) {
    const filename = `backend-service-licenses.${type}`;
    const headers = {
      ['Content-Disposition']: `inline; filename="${filename}"`,
      ['Content-Type']: LicenseContentTypes[type],
    };

    res.sendFile(filename, {
      root: path.join(process.cwd(), 'data'),
      headers,
    });
  }
}
