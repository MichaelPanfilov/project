import { createLogger } from '@elunic/logger';
import { LoggerModule } from '@elunic/logger/nestjs';
import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import * as config from '../ormconfig';

import { ActorModule } from './actor/actor.module';
import { AssetModule } from './asset/asset.module';
import { InstructionModule } from './instruction/instruction.module';
import { LicenseController } from './license.controller';
import { MachineTypeTagModule } from './machine-type-tag/machine-type-tag.module';
import { NotificationModule } from './notification/notification.module';
import { TaskModule } from './task/task.module';
import { RightGuard } from './util/api';

const logger = createLogger('backend-service');

@Module({
  imports: [
    TypeOrmModule.forRoot(config),
    LoggerModule.forRoot(logger),
    TaskModule,
    NotificationModule,
    AssetModule,
    ActorModule,
    InstructionModule,
    MachineTypeTagModule,
  ],
  controllers: [LicenseController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: RightGuard,
    },
  ],
})
export class AppModule {}
