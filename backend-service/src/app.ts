require('module-alias/register');
require('dotenv/config');

process.env.TZ = 'UTC';

import { REQUEST_OPT } from '@common';
import { LOGGER } from '@elunic/logger/nestjs';
import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { IoAdapter } from '@nestjs/platform-socket.io';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { CrudConfigService } from '@nestjsx/crud';
import bodyParser = require('body-parser');

import { ActorModule } from './actor/actor.module';
import { AppModule } from './app.module';
import { AssetModule } from './asset/asset.module';
import { InstructionModule } from './instruction/instruction.module';
import { TaskModule } from './task/task.module';
import { HttpExceptionFilter, ResponseInterceptor, TypeormExceptionFilter } from './util/api';
import { authMiddleware } from './util/api/auth/auth-middleware';

// Needs to run before AppModule gets imported.
// tslint:disable-next-line
CrudConfigService.load({
  queryParser: REQUEST_OPT,
  query: {
    cache: false,
    alwaysPaginate: false,
    maxLimit: 100,
  },
});

console.log(`[${new Date()}] Starting application`);

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  authMiddleware(app);

  // TODO: The agent requests can be very big. It needs to be verified, what the limit here really should be
  // tslint:disable-next-line
  app.use(bodyParser.json({ limit: '8mb' }));
  // tslint:disable-next-line
  app.use(bodyParser.urlencoded({ limit: '8mb', extended: true }));
  app.useWebSocketAdapter(new IoAdapter(app));
  app.useGlobalInterceptors(new ResponseInterceptor());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
    }),
  );
  app.useGlobalFilters(new HttpExceptionFilter(app.get(LOGGER)), new TypeormExceptionFilter());

  // const config = app.get('ConfigService') as ConfigService;

  const options1 = new DocumentBuilder()
    .setTitle('Shopfloor Asset service')
    .setDescription('API documentation of all integrated modules')
    .addBearerAuth()
    .setVersion('3.0')
    .build();
  SwaggerModule.setup(
    '/docs/assets',
    app,
    SwaggerModule.createDocument(app, options1, {
      include: [AssetModule],
    }),
  );

  const options2 = new DocumentBuilder()
    .setTitle('Shopfloor Task service')
    .setDescription('API documentation of all integrated modules')
    .addBearerAuth()
    .setVersion('3.0')
    .build();
  SwaggerModule.setup(
    '/docs/tasks',
    app,
    SwaggerModule.createDocument(app, options2, {
      include: [TaskModule],
    }),
  );

  const options3 = new DocumentBuilder()
    .setTitle('Shopfloor Actor service')
    .setDescription('API documentation of all integrated modules')
    .addBearerAuth()
    .setVersion('3.0')
    .build();
  SwaggerModule.setup(
    '/docs/actors',
    app,
    SwaggerModule.createDocument(app, options3, {
      include: [ActorModule],
    }),
  );

  const options4 = new DocumentBuilder()
    .setTitle('Shopfloor Instruction service')
    .setDescription('API documentation of all integrated modules')
    .addBearerAuth()
    .setVersion('3.0')
    .build();
  SwaggerModule.setup(
    '/docs/instructions',
    app,
    SwaggerModule.createDocument(app, options4, {
      include: [InstructionModule],
    }),
  );

  const port = (process.env.APP_PORT as string) || '3000';
  await app.listen(port);
  console.log(`[${new Date()}] Listening on port ${port}`);
}
bootstrap();
