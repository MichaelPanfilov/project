import { JasmineAllureReporter } from 'allure-jasmine';
// This provides the type definitions
import 'jest-extended';
import 'source-map-support/register';

jasmine.getEnv().addReporter(
  new JasmineAllureReporter({
    resultsDir: 'allure-results',
  }),
);
