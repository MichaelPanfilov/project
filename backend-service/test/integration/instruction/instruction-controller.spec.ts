import { generateId, ReferencedEntities } from '@common';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as path from 'path';
import { InstructionLink } from 'src/instruction/entities/instruction-link.entity';
import { Instruction } from 'src/instruction/entities/instruction.entity';
import { InstructionModule } from 'src/instruction/instruction.module';
import { InternalServicesConfig } from 'src/util';
import { HttpExceptionFilter, ResponseInterceptor, TypeormExceptionFilter } from 'src/util/api';
import * as request from 'supertest';
import { Connection, ConnectionOptions, getConnection, Repository } from 'typeorm';

import { mockAuthMiddleware } from '../../util/auth';
import { MockInternalServicesConfig } from '../../util/config';
import { createTestDatabase, dropTestDatabase, getTestConnectionOpts } from '../../util/connection';

describe('InstructionController', () => {
  let testDbName: string;
  let app: INestApplication;
  let connection: Connection;
  let instructionRepo: Repository<Instruction>;
  let instructionLinkRepo: Repository<InstructionLink>;
  const opts = getTestConnectionOpts();
  let instructionObj: Instruction;

  beforeAll(async () => {
    testDbName = await createTestDatabase(opts);

    const module = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          ...opts,
          database: testDbName,
          synchronize: true,
          entities: [path.join(__dirname, '../../../src/**/*.entity.{ts,js}')],
        } as ConnectionOptions),
        InstructionModule,
      ],
      // Before enabling this we need to properly mock the users rights.
      /*
                providers: [
                  {
                    provide: APP_GUARD,
                    useClass: RightGuard,
                  },
                ],
                */
    })
      .overrideProvider(InternalServicesConfig)
      .useClass(MockInternalServicesConfig)
      .compile();

    connection = getConnection();
    instructionRepo = connection.getRepository(Instruction);
    instructionLinkRepo = connection.getRepository(InstructionLink);

    app = module.createNestApplication();

    mockAuthMiddleware(app);
    app.useGlobalInterceptors(new ResponseInterceptor());
    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    );
    app.useGlobalFilters(new HttpExceptionFilter(), new TypeormExceptionFilter());

    await app.init();
  }, 10000);

  afterAll(async () => {
    await app.close();
    await dropTestDatabase(testDbName, opts);
  });

  describe('API tests for /v1/work-instructions section', () => {
    let instructions: Instruction[] = [];

    beforeAll(async () => {
      instructionObj = await instructionRepo.save({
        id: generateId(ReferencedEntities.WORK_INSTRUCTION),
        title: 'Instruction for API tests',
        url: 'https://syntegon-shopfloor-testing.elunic.software',
        classification: 'class0',
        machineTypes: [],
      });
      instructions = await instructionRepo.find();
    });

    afterAll(async () => {
      await Promise.all(instructions.map(a => instructionRepo.delete(a)));
    });

    it('GET /v1/work-instructions - return all work instructions in db and data in the right format', () =>
      request(app.getHttpServer())
        .get(`/v1/work-instructions`)
        .expect(200)
        .expect(({ body }) => {
          expect(Array.isArray(body.data)).toBe(true);
          expect(body.data.length).toBe(instructions.length);
          expect(typeof body.meta === 'object').toBe(true);
        }));

    it('POST /v1/work-instructions - create 1 work instruction', () =>
      request(app.getHttpServer())
        .post(`/v1/work-instructions`)
        .send({
          title: 'API test work-instruction',
          url: 'https://syntegon-shopfloor-testing.elunic.software',
          classification: 'class1',
          machineTypes: [],
        })
        .expect(201)
        .expect(({ body }) => {
          expect(typeof body.data.id === 'string').toBe(true);
          expect(typeof body.meta === 'object').toBe(true);
        }));

    it('GET /v1/work-instructions/{instructionId} - retrieve 1 work instruction', () =>
      request(app.getHttpServer())
        .get(`/v1/work-instructions/${instructionObj.id}`)
        .expect(200)
        .expect(({ body }) => {
          expect(typeof body.data.id === 'string').toBe(true);
          expect(typeof body.meta === 'object').toBe(true);
        }));

    it('PUT /v1/work-instructions/{instructionId} - replace 1 work instruction', () =>
      request(app.getHttpServer())
        .put(`/v1/work-instructions/${instructionObj.id}`)
        .send({
          title: 'replacement API test work-instruction',
          url: 'https://syntegon-shopfloor-testing.elunic.software',
          classification: 'class1',
          machineTypes: [],
        })
        .expect(200)
        .expect(({ body }) => {
          expect(typeof body.data.id === 'string').toBe(true);
          expect(typeof body.meta === 'object').toBe(true);
        }));

    it('GET /v1/work-instructions/{instructionId}/qr-code - get instruction qr code', () =>
      request(app.getHttpServer())
        .get(`/v1/work-instructions/${instructionObj.id}/qr-code`)
        .expect(200)
        .expect(({ body }) => {
          expect(typeof body.data === 'string').toBe(true);
          expect(typeof body.meta === 'object').toBe(true);
        }));

    it('GET /v1/work-instructions/{instructionId}/links - get all links for work instructions', () =>
      request(app.getHttpServer())
        .get(`/v1/work-instructions/${instructionObj.id}/links`)
        .expect(200)
        .expect(({ body }) => {
          expect(Array.isArray(body.data)).toBe(true);
          expect(typeof body.meta === 'object').toBe(true);
        }));

    it('POST /v1/work-instructions/{instructionId}/link - link work instruction', () =>
      request(app.getHttpServer())
        .post(`/v1/work-instructions/${instructionObj.id}/link`)
        .send(['linkString'])
        .expect(201)
        .expect(({ body }) => {
          expect(Array.isArray(body.data.links)).toBe(true);
          expect(typeof body.meta === 'object').toBe(true);
        }));

    it('POST /v1/work-instructions/{instructionId}/unlink - unlink work instruction', () =>
      request(app.getHttpServer())
        .post(`/v1/work-instructions/${instructionObj.id}/link`)
        .send(['linkString'])
        .expect(201)
        .expect(({ body }) => {
          expect(Array.isArray(body.data.links)).toBe(true);
          expect(typeof body.meta === 'object').toBe(true);
        }));

    it('DELETE /v1/work-instructions/{instructionId} - delete one work instruction', () =>
      request(app.getHttpServer())
        .delete(`/v1/work-instructions/${instructionObj.id}`)
        .expect(200));
  });
});
