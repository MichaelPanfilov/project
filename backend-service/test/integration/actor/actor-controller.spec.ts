import { AclUserDto, ActorDto } from '@common';
import { HttpModule, HttpService, INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as path from 'path';
import { of } from 'rxjs';
import { ActorModule } from 'src/actor/actor.module';
import { Actor } from 'src/actor/entities/actor.entity';
import { InternalServicesConfig } from 'src/util';
import { HttpExceptionFilter, ResponseInterceptor, TypeormExceptionFilter } from 'src/util/api';
import * as request from 'supertest';
import { Connection, ConnectionOptions, getConnection, Repository } from 'typeorm';
import * as url from 'url';

import { ACL_MOCK_USER_ID, mockAuthMiddleware } from '../../util/auth';
import { MockInternalServicesConfig } from '../../util/config';
import { createTestDatabase, dropTestDatabase, getTestConnectionOpts } from '../../util/connection';

describe('ActorController', () => {
  let testDbName: string;
  let app: INestApplication;
  let connection: Connection;
  let actorRepo: Repository<Actor>;
  let userId: string;
  let actorId: string;
  const opts = getTestConnectionOpts();

  const aclUrl = 'http://acl-service';
  const aclId = ACL_MOCK_USER_ID;
  const aclMockedUser: AclUserDto = {
    activated: true,
    id: aclId,
    name: 'nameString',
    roleIds: [],
    email: 'email@email.com',
  };

  const mockHttpService = {
    post: jest.fn().mockReturnValue(of({ data: { data: aclMockedUser } })),
    delete: jest.fn().mockReturnValue(of(null)),
  };

  beforeAll(async () => {
    testDbName = await createTestDatabase(opts);

    const module = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          ...opts,
          database: testDbName,
          synchronize: true,
          entities: [path.join(__dirname, '../../../src/**/*.entity.{ts,js}')],
        } as ConnectionOptions),
        ActorModule,
        HttpModule,
      ],
    })
      .overrideProvider(InternalServicesConfig)
      .useValue(
        new MockInternalServicesConfig({
          acl: aclUrl,
          file: '',
        }),
      )
      .overrideProvider(HttpService)
      .useValue(mockHttpService)
      .compile();

    connection = getConnection();
    actorRepo = connection.getRepository(Actor);

    app = module.createNestApplication();

    mockAuthMiddleware(app);
    app.useGlobalInterceptors(new ResponseInterceptor());
    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    );
    app.useGlobalFilters(new HttpExceptionFilter(), new TypeormExceptionFilter());

    await app.init();
  }, 10000);

  afterAll(async () => {
    await app.close();

    await dropTestDatabase(testDbName, opts);
  });

  describe('API tests for /v1/actors section', () => {
    describe('/v1/actors', () => {
      let actors: ActorDto[] = [];

      beforeAll(async () => {
        actors = await actorRepo.find();
      });

      afterAll(async () => {
        await Promise.all(actors.map(a => actorRepo.delete(a)));
      });

      it('POST /v1/users - create 1 user', () =>
        request(app.getHttpServer())
          .post(`/v1/users`)
          .send({
            language: 'english',
            email: 'email@email.com',
            name: 'nameString',
            password: 'passwordString',
          })
          .expect(201)
          .expect(({ body }) => {
            userId = body.data.id;
            expect(body.data.id.startsWith('actor-')).toBe(true);
            expect(body.data.aclId.startsWith('acl_user-')).toBe(true);
            expect(mockHttpService.post).toHaveBeenCalledWith(
              url.resolve(aclUrl, '/v1/users'),
              jasmine.objectContaining({
                email: 'email@email.com',
                password: 'passwordString',
                username: 'nameString',
              }),
              jasmine.any(Object),
            );
          }));

      it('GET /v1/users - get many users', () =>
        request(app.getHttpServer())
          .get('/v1/users')
          .expect(200)
          .expect(({ body }) => {
            expect(Array.isArray(body.data)).toBe(true);
            expect(typeof body.meta === 'object').toBe(true);
          }));

      it('GET /v1/users/{userId} - get 1 user by id', () =>
        request(app.getHttpServer())
          .get(`/v1/users/${userId}`)
          .expect(200)
          .expect(({ body }) => {
            expect(Array.isArray(body.data)).toBe(false);
            expect(typeof body.data.id === 'string').toBe(true);
            expect(typeof body.meta === 'object').toBe(true);
          }));

      it('GET /v1/users/acl/{aclId} - get 1 user by aclId', () =>
        request(app.getHttpServer())
          .get(`/v1/users/acl/${aclId}`)
          .expect(200)
          .expect(({ body }) => {
            expect(Array.isArray(body.data)).toBe(false);
            expect(body.data.aclId).toBe(aclId);
            expect(typeof body.meta === 'object').toBe(true);
          }));

      it('PUT /v1/users/{userId} - replace 1 user', () =>
        request(app.getHttpServer())
          .put(`/v1/users/${userId}`)
          .send({ language: 'german' })
          .expect(200)
          .expect(({ body }) => {
            expect(body.data.id).toBe(userId);
            expect(body.data.aclId).toBe(aclId);
            expect(body.data.language).toBe('german');
          }));

      it('DELETE /v1/users/{userId} - delete 1 user', () =>
        request(app.getHttpServer())
          .delete(`/v1/users/${userId}`)
          .expect(200)
          .expect(() =>
            expect(mockHttpService.delete).toHaveBeenCalledWith(
              url.resolve(aclUrl, `/v1/users/${aclId}`),
              jasmine.any(Object),
            ),
          ));

      it('POST /v1/users - create 1 user again', () =>
        request(app.getHttpServer())
          .post(`/v1/users`)
          .send({
            language: 'english',
            email: 'email2@email.com',
            name: 'nameString',
            password: 'passwordString',
          })
          .expect(201)
          .expect(({ body }) => {
            userId = body.data.id;
            expect(body.data.id.startsWith('actor-')).toBe(true);
            expect(body.data.aclId.startsWith('acl_user-')).toBe(true);
            expect(mockHttpService.post).toHaveBeenCalledWith(
              url.resolve(aclUrl, '/v1/users'),
              jasmine.objectContaining({
                email: 'email2@email.com',
                password: 'passwordString',
                username: 'nameString',
              }),
              jasmine.any(Object),
            );
          }));

      it('GET /v1/actors - retrieve many actors', () =>
        request(app.getHttpServer())
          .get('/v1/actors')
          .expect(200)
          .expect(({ body }) => {
            expect(Array.isArray(body.data)).toBe(true);
          }));

      it('GET /v1/actors/acl/{aclId} - get 1 actor by aclId', () =>
        request(app.getHttpServer())
          .get(`/v1/actors/acl/${aclId}`)
          .expect(200)
          .expect(({ body }) => {
            actorId = body.data.id;
          }));

      it('GET /v1/actors/{actorId} - get 1 actor by actorId', () =>
        request(app.getHttpServer())
          .get(`/v1/actors/${actorId}`)
          .expect(200)
          .expect(({ body }) => {
            expect(body.data.id).toBe(actorId);
          }));

      it('DELETE /v1/actors/{actorId} - delete 1 actor by actorId', () =>
        request(app.getHttpServer())
          .delete(`/v1/actors/${actorId}`)
          .expect(200)
          .expect(() =>
            expect(mockHttpService.delete).toHaveBeenCalledWith(
              url.resolve(aclUrl, `/v1/users/${aclId}`),
              jasmine.any(Object),
            ),
          ));

      it('GET /v1/actors - retrieve many actors', () =>
        request(app.getHttpServer())
          .get('/v1/actors')
          .expect(200)
          .expect(({ body }) => {
            expect(Array.isArray(body.data)).toBe(true);
            expect(body.data).toEqual([]);
          }));
    });
  });
});
