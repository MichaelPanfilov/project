import { AssetDto, generateId, ReferencedEntities } from '@common';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as path from 'path';
import { AssetModule } from 'src/asset/asset.module';
import { AssetTree } from 'src/asset/entities/asset.entity';
import { FactoryTree } from 'src/asset/entities/factory.entity';
import { LineTree } from 'src/asset/entities/line.entity';
import { MachineTree } from 'src/asset/entities/machine.entity';
import { TaskService } from 'src/task/task.service';
import { InternalServicesConfig } from 'src/util';
import { HttpExceptionFilter, ResponseInterceptor, TypeormExceptionFilter } from 'src/util/api';
import * as request from 'supertest';
import { Connection, ConnectionOptions, getConnection, Repository, TreeRepository } from 'typeorm';

import { mockAuthMiddleware } from '../../util/auth';
import { MockInternalServicesConfig } from '../../util/config';
import { createTestDatabase, dropTestDatabase, getTestConnectionOpts } from '../../util/connection';
import { MockTaskService } from '../../util/mocks';

describe('AssetController - machines', () => {
  let testDbName: string;
  let app: INestApplication;
  let connection: Connection;
  let assetRepo: TreeRepository<AssetTree>;
  let factoryRepo: Repository<FactoryTree>;
  let lineRepo: Repository<LineTree>;
  let machineRepo: Repository<MachineTree>;
  const opts = getTestConnectionOpts();
  let factoryObj: FactoryTree;
  let lineObj: LineTree;
  let machineObj: MachineTree;

  beforeAll(async () => {
    testDbName = await createTestDatabase(opts);

    const module = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          ...opts,
          database: testDbName,
          synchronize: true,
          entities: [path.join(__dirname, '../../../src/**/*.entity.{ts,js}')],
        } as ConnectionOptions),
        AssetModule,
      ],
      // Before enabling this we need to properly mock the users rights.
      /*
      providers: [
        {
          provide: APP_GUARD,
          useClass: RightGuard,
        },
      ],
      */
    })
      .overrideProvider(InternalServicesConfig)
      .useClass(MockInternalServicesConfig)
      .overrideProvider(TaskService)
      .useClass(MockTaskService)
      .compile();

    connection = getConnection();
    assetRepo = connection.getTreeRepository(AssetTree);
    factoryRepo = connection.getRepository(FactoryTree);
    lineRepo = connection.getRepository(LineTree);
    machineRepo = connection.getRepository(MachineTree);

    app = module.createNestApplication();

    mockAuthMiddleware(app);
    app.useGlobalInterceptors(new ResponseInterceptor());
    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    );
    app.useGlobalFilters(new HttpExceptionFilter(), new TypeormExceptionFilter());

    await app.init();
  }, 10000);

  afterAll(async () => {
    await app.close();

    await dropTestDatabase(testDbName, opts);
  });

  describe('API tests for /v1/machines section', () => {
    describe('/v1/machines', () => {
      let assets: AssetDto[] = [];

      beforeAll(async () => {
        factoryObj = await factoryRepo.save({
          id: generateId(ReferencedEntities.ASSET),
          name: 'Test Factory',
          position: 0,
          postalCode: '80637',
        });
        lineObj = await lineRepo.save({
          id: generateId(ReferencedEntities.ASSET),
          name: 'Test Line',
          position: 0,
        });
        machineObj = await machineRepo.save({
          id: generateId(ReferencedEntities.ASSET),
          name: 'Test Machine',
          position: 0,
          constructionYear: 2020,
          manufacturer: 'Syntegon',
        });

        assets = await assetRepo.find();
      });

      afterAll(async () => {
        await Promise.all(assets.map(a => assetRepo.delete(a)));
      });
      // Machines requests
      it('GET /v1/machines/{machineId}/line - get line of machine', () =>
        request(app.getHttpServer())
          .get(`/v1/machines/${machineObj.id}/line`)
          .expect(200));

      it('DELETE ​/v1​/machines/{machineId}​/line - delete line of machine', () =>
        request(app.getHttpServer())
          .delete(`/v1/machines/${machineObj.id}/line`)
          .expect(200));

      it('POST ​/v1/machines/{machineId}/line/{lineId}', () =>
        request(app.getHttpServer())
          .post(`/v1/machines/${machineObj.id}/line/${lineObj.id}`)
          .expect(201));

      it('GET ​/v1/machines/{machineId} - get 1 machine', () =>
        request(app.getHttpServer())
          .get(`/v1/machines/${machineObj.id}`)
          .expect(200));

      it('PUT ​/v1/machines/{machineId} - replace 1 machine', () =>
        request(app.getHttpServer())
          .put(`/v1/machines/${machineObj.id}`)
          .send({
            name: 'Machine name',
            description: 'Machine description',
            position: 0,
            manufacturer: 'Machine manufacturer',
            machineType: 'Machine type',
            serialNumber: '777333',
            constructionYear: 2020,
            device: 'Shopfloor device',
            parent: `${lineObj.id}`,
            isLineOutput: true,
          })
          .expect(200));

      it('DELETE ​/v1/machines/{machineId} - delete 1 machine', () =>
        request(app.getHttpServer())
          .delete(`/v1/machines/${machineObj.id}`)
          .expect(200));

      it('GET /v1/machines - get many machines', () =>
        request(app.getHttpServer())
          .get(`/v1/machines`)
          .expect(200));

      it('POST /v1/machines - create 1 machine', () =>
        request(app.getHttpServer())
          .post(`/v1/machines`)
          .send({
            name: 'API test machine',
            description: 'Machine description',
            position: 0,
            manufacturer: 'Machine manufacturer',
            machineType: 'Machine type',
            serialNumber: '777327',
            constructionYear: 2020,
            device: 'Shopfloor device',
            parent: `${lineObj.id}`,
            isLineOutput: true,
          })
          .expect(201));
    });
  });
});
