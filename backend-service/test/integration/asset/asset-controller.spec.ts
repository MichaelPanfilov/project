import { AssetDto, generateId, ReferencedEntities } from '@common';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as path from 'path';
import { AssetModule } from 'src/asset/asset.module';
import { AssetTree } from 'src/asset/entities/asset.entity';
import { FactoryTree } from 'src/asset/entities/factory.entity';
import { LineTree } from 'src/asset/entities/line.entity';
import { MachineTree } from 'src/asset/entities/machine.entity';
import { InternalServicesConfig } from 'src/util';
import { HttpExceptionFilter, ResponseInterceptor, TypeormExceptionFilter } from 'src/util/api';
import * as request from 'supertest';
import { Connection, ConnectionOptions, getConnection, Repository, TreeRepository } from 'typeorm';

import { mockAuthMiddleware } from '../../util/auth';
import { MockInternalServicesConfig } from '../../util/config';
import { createTestDatabase, dropTestDatabase, getTestConnectionOpts } from '../../util/connection';

describe('AssetController', () => {
  let testDbName: string;
  let app: INestApplication;
  let connection: Connection;
  let assetRepo: TreeRepository<AssetTree>;
  let factoryRepo: Repository<FactoryTree>;
  let lineRepo: Repository<LineTree>;
  let machineRepo: Repository<MachineTree>;
  const opts = getTestConnectionOpts();
  let machineId: string;
  let factoryObj: FactoryTree;
  let lineObj: LineTree;
  let machineObj: MachineTree;

  beforeAll(async () => {
    testDbName = await createTestDatabase(opts);

    const module = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          ...opts,
          database: testDbName,
          synchronize: true,
          entities: [path.join(__dirname, '../../../src/**/*.entity.{ts,js}')],
        } as ConnectionOptions),
        AssetModule,
      ],
      // Before enabling this we need to properly mock the users rights.
      /*
          providers: [
            {
              provide: APP_GUARD,
              useClass: RightGuard,
            },
          ],
          */
    })
      .overrideProvider(InternalServicesConfig)
      .useClass(MockInternalServicesConfig)
      .compile();

    connection = getConnection();
    assetRepo = connection.getTreeRepository(AssetTree);
    factoryRepo = connection.getRepository(FactoryTree);
    lineRepo = connection.getRepository(LineTree);
    machineRepo = connection.getRepository(MachineTree);

    app = module.createNestApplication();

    mockAuthMiddleware(app);
    app.useGlobalInterceptors(new ResponseInterceptor());
    app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    );
    app.useGlobalFilters(new HttpExceptionFilter(), new TypeormExceptionFilter());

    await app.init();
  }, 10000);

  afterAll(async () => {
    await app.close();

    await dropTestDatabase(testDbName, opts);
  });

  describe('API tests for /v1/assets section', () => {
    describe('/v1/assets', () => {
      let assets: AssetDto[] = [];

      beforeAll(async () => {
        factoryObj = await factoryRepo.save({
          id: generateId(ReferencedEntities.ASSET),
          name: 'Test Factory',
          position: 0,
          postalCode: '80637',
        });
        lineObj = await lineRepo.save({
          id: generateId(ReferencedEntities.ASSET),
          name: 'Test Line',
          position: 0,
        });
        machineObj = await machineRepo.save({
          id: generateId(ReferencedEntities.ASSET),
          name: 'Test Machine',
          position: 0,
          constructionYear: 2020,
          manufacturer: 'Syntegon',
        });

        assets = await assetRepo.find();
      });

      afterAll(async () => {
        await Promise.all(assets.map(a => assetRepo.delete(a)));
      });

      it('GET /v1/assets - return all assets in db and data in the right format', () =>
        request(app.getHttpServer())
          .get(`/v1/assets`)
          .expect(200)
          .expect(({ body }) => {
            expect(Array.isArray(body.data)).toBe(true);
            expect(body.data.length).toBe(assets.length);
            expect(typeof body.meta === 'object').toBe(true);
          }));

      it('GET /v1/assets/trees - return all assets trees', () =>
        request(app.getHttpServer())
          .get(`/v1/assets/trees`)
          .expect(200));

      it('POST /v1/machines - create 1 machine', () =>
        request(app.getHttpServer())
          .post(`/v1/machines`)
          .send({
            name: 'API test Machine',
            description: 'Machine made during api testing',
            position: 0,
            manufacturer: 'API test manufacturer',
            machineType: 'Control',
            serialNumber: '71732',
            constructionYear: 2020,
            device: 'Shopfloor',
            parent: lineObj.id,
            isLineOutput: true,
          })
          .expect(201)
          .expect(({ body }) => {
            machineId = body.data.id;
          }));

      it('GET /v1/assets/{assetId} - retrieve one generic asset', () =>
        request(app.getHttpServer())
          .get(`/v1/assets/${machineId}`)
          .expect(200));

      it('GET /v1/assets/{assetId}/parent - retrieve id of parent asset of the machine', () =>
        request(app.getHttpServer())
          .get(`/v1/assets/${machineId}/parent`)
          .expect(200));

      it('GET /v1/assets/{assetId}/children - retrieve id of children of factory', () =>
        request(app.getHttpServer())
          .get(`/v1/assets/${factoryObj.id}/children`)
          .expect(200)
          .expect(({ body }) => {
            expect(Array.isArray(body.data)).toBe(true);
          }));

      it('GET /v1/assets/{assetId}/tree - get tree for factory based on asset id', () =>
        request(app.getHttpServer())
          .get(`/v1/assets/${factoryObj.id}/tree`)
          .expect(200));

      it('POST /v1/assets/{assetId}/parent/{parentId}', () =>
        request(app.getHttpServer())
          .post(`/v1/assets/${machineId}/parent/${lineObj.id}`)
          .expect(201));

      it('DELETE /v1/assets/{assetId}/parent - delete one generic assets parent', () =>
        request(app.getHttpServer())
          .delete(`/v1/assets/${machineId}/parent`)
          .expect(200));

      it('DELETE /v1/assets/{assetId} - delete one generic asset', () =>
        request(app.getHttpServer())
          .delete(`/v1/assets/${machineId}`)
          .expect(200));
    });
  });
});
