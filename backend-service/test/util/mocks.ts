export class MockTaskService {
  updateAssignedAssetName = () => Promise.resolve();
  getByAssetId = () => Promise.resolve([]);
  unassign = () => Promise.resolve();
}
