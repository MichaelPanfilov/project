import { INestApplication } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { Express, NextFunction, Request, Response } from 'express';

export const ACL_MOCK_USER_ID = 'acl_user-231e8e6c-ac4e-46fa-8ca9-230e35034d59';

export function mockAuthMiddleware(app: INestApplication): void {
  // Get the express app
  const adapterHost = app.get(HttpAdapterHost);
  const httpAdapter = adapterHost.httpAdapter;
  const instance = httpAdapter.getInstance() as Express;

  instance.use(AuthorizationMiddleware);

  async function AuthorizationMiddleware(req: Request, res: Response, next: NextFunction) {
    const now = new Date().getTime();
    req.auth = {
      userId: ACL_MOCK_USER_ID,
      username: 'test-user',
      type: 'user',
      roles: [],
      rights: [],
      iat: now,
      exp: now + 1000 * 60 * 60,
    };

    next();
  }
}
