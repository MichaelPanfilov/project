import * as url from 'url';

import { InternalServicesConfig, ServiceName } from '../../src/util/api';

export type ServiceUrlMock = { [key in ServiceName]: string };

export class MockInternalServicesConfig extends InternalServicesConfig {
  constructor(private mockUrls?: ServiceUrlMock) {
    super();
  }

  readonly publicKey = Buffer.from('');
  readonly internalKey = Buffer.from('');
  readonly authHeader = `Bearer some-test-token`;

  assertServiceUrls(...args: unknown[]) {}

  resolveUrl(service: ServiceName, path: string): string {
    return url.resolve(this.mockUrls ? this.mockUrls[service] : '', path);
  }
}
