import 'source-map-support/register';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  Generated,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  RelationId,
  UpdateDateColumn,
} from 'typeorm';

jest.mock('typeorm');

(Entity as jest.Mock).mockReturnValue(jest.fn());
(PrimaryColumn as jest.Mock).mockReturnValue(jest.fn());
(Generated as jest.Mock).mockReturnValue(jest.fn());
(Column as jest.Mock).mockReturnValue(jest.fn());
(Index as jest.Mock).mockReturnValue(jest.fn());
(PrimaryColumn as jest.Mock).mockReturnValue(jest.fn());
(BeforeUpdate as jest.Mock).mockReturnValue(jest.fn());
(BeforeInsert as jest.Mock).mockReturnValue(jest.fn());
(OneToMany as jest.Mock).mockReturnValue(jest.fn());

(ManyToOne as jest.Mock).mockReturnValue(jest.fn());
(JoinColumn as jest.Mock).mockReturnValue(jest.fn());
(UpdateDateColumn as jest.Mock).mockReturnValue(jest.fn());
(RelationId as jest.Mock).mockReturnValue(jest.fn());
