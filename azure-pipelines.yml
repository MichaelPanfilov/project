# Docker
# Build and push an image to Azure Container Registry
# https://docs.microsoft.com/azure/devops/pipelines/languages/docker

trigger:
  branches:
    include:
      - '*'
  paths:
    exclude:
      - package.json
      - package-lock.json
      - frontend/package.json
      - frontend/package-lock.json

variables:
  # Container registry service connection established during pipeline creation
  - name: containerRegistry
    value: 'acrshopfloor.azurecr.io'
  - name: tag
    value: 'latest'
  - group: 'deployment_variables'
  - name: vmImageName
    value: 'ubuntu-latest'
  - name: dockerRegistryServiceConnection
    value: 'c1aafea3-a81f-428d-b142-681da76fa0b1'
  - name: environment
    value: ${{replace(variables['Build.SourceBranchName'], 'master', 'development')}}

stages:
- stage: BumpVersions
  displayName: Bump Versions
  jobs:
    - job: BumpVersions
      displayName: Bump Versions
      pool:
        vmImage: $(vmImageName)
      steps:
        - checkout: self
          persistCredentials: "true"
        - script: "git config --global user.name 'Azure DevOps' && git config --global user.email 'undefined@undefined.undef'"
          displayName: Set git config
        - script: "cd frontend && npm version patch && git add package.json package-lock.json && git commit -m 'Bump frontend version'"
          displayName: Bump frontend version
        - script: "npm version --no-git-tag-version -m 'Bump shopfloor version' patch"
          displayName: Bump shopfloor version
        - script: "git push origin HEAD:$(Build.SourceBranchName)"
          displayName: Git push
- stage: Tests
  displayName: Tests
  jobs:
    - job: Tests
      displayName: Tests
      pool:
        vmImage: $(vmImageName)
      steps:
        - checkout: self
        - task: DockerInstaller@0
          displayName: Install Docker CLI
          inputs:
            dockerVersion: 17.09.0-ce
            releaseType: stable
        - script: "docker run -d -e 'MYSQL_ROOT_PASSWORD=toor' -e 'MYSQL_DATABASE=testing' -p 3306:3306 mysql:5.7"
          displayName: Start MySQL 5.7 Container
        - script: 'npx concurrently -n common,acl-service "cd common && npm ci" "cd acl-service && npm ci"'
          displayName: "acl-service Tests: Prepare"
        - script: 'chmod +x ../tools/wait-for-it/wait-for-it.sh && ../tools/wait-for-it/wait-for-it.sh -h 127.0.0.1 -p 3306 -s -t 60 -- npm run test:ci'
          displayName: "acl-service Tests: Run Tests"
          workingDirectory: $(Build.SourcesDirectory)/acl-service
          env:
            MYSQL_DATABASE: testing
            MYSQL_ROOT_PASSWORD: toor
            APP_TEST_DB_HOST: 127.0.0.1
            APP_TEST_DB_PORT: 3306
            APP_TEST_DB_USER: root
            APP_TEST_DB_PASS: toor
            APP_TEST_DB_NAME: testing
        - script: 'npx concurrently -n common,backend-service "cd common && npm ci" "cd backend-service && npm ci"'
          displayName: "backend-service Tests: Prepare"
        - script: 'chmod +x ../tools/wait-for-it/wait-for-it.sh && ../tools/wait-for-it/wait-for-it.sh -h 127.0.0.1 -p 3306 -s -t 60 -- npm run test:ci'
          displayName: "backend-service Tests: Run Tests"
          workingDirectory: $(Build.SourcesDirectory)/backend-service
          env:
            MYSQL_DATABASE: testing
            MYSQL_ROOT_PASSWORD: toor
            APP_TEST_DB_HOST: 127.0.0.1
            APP_TEST_DB_PORT: 3306
            APP_TEST_DB_USER: root
            APP_TEST_DB_PASS: toor
            APP_TEST_DB_NAME: testing
        - script: 'npx concurrently -n common,file-service "cd common && npm ci" "cd file-service && npm ci"'
          displayName: "file-service Tests: Prepare"
        - script: 'chmod +x ../tools/wait-for-it/wait-for-it.sh && ../tools/wait-for-it/wait-for-it.sh -h 127.0.0.1 -p 3306 -s -t 60 -- npm run test:ci'
          displayName: "file-service Tests: Run Tests"
          workingDirectory: $(Build.SourcesDirectory)/file-service
          env:
            MYSQL_DATABASE: testing
            MYSQL_ROOT_PASSWORD: toor
            APP_TEST_DB_HOST: 127.0.0.1
            APP_TEST_DB_PORT: 3306
            APP_TEST_DB_USER: root
            APP_TEST_DB_PASS: toor
            APP_TEST_DB_NAME: testing
        - script: 'npx concurrently -n common,datapoint-service "cd common && npm ci" "cd datapoint-service && npm ci"'
          displayName: "datapoint-service Tests: Prepare"
        - script: 'chmod +x ../tools/wait-for-it/wait-for-it.sh && ../tools/wait-for-it/wait-for-it.sh -h 127.0.0.1 -p 3306 -s -t 60 -- npm run test:ci'
          displayName: "datapoint-service Tests: Run Tests"
          workingDirectory: $(Build.SourcesDirectory)/datapoint-service
          env:
            MYSQL_DATABASE: testing
            MYSQL_ROOT_PASSWORD: toor
            APP_TEST_DB_HOST: 127.0.0.1
            APP_TEST_DB_PORT: 3306
            APP_TEST_DB_USER: root
            APP_TEST_DB_PASS: toor
            APP_TEST_DB_NAME: testing
- stage: Build_push
  displayName: Build and push
  jobs:
  - job: Build_push
    displayName: Build and push
    pool:
      vmImage: $(vmImageName)
    steps:
    - checkout: self
    - task: Docker@2
      displayName: Login to ACR
      inputs:
        command: login
        containerRegistry: $(dockerRegistryServiceConnection)
    - task: Docker@2
      displayName: Build acl-service
      inputs:
        command: build
        repository: 'acl-service-$(environment)'
        buildContext: $(Build.SourcesDirectory)
        dockerfile: '$(Build.SourcesDirectory)/acl-service/Dockerfile'
        tags: |
          $(tag)
        arguments: --build-arg="NPM_TOKEN=$(NPM_TOKEN)"
    - task: Docker@2
      displayName: Build backend-service
      inputs:
        command: build
        repository: 'backend-service-$(environment)'
        buildContext: $(Build.SourcesDirectory)
        dockerfile: '$(Build.SourcesDirectory)/backend-service/Dockerfile'
        tags: |
          $(tag)
        arguments: --build-arg="NPM_TOKEN=$(NPM_TOKEN)"
    - task: Docker@2
      displayName: Build datapoint-service
      inputs:
        command: build
        repository: 'datapoint-service-$(environment)'
        buildContext: $(Build.SourcesDirectory)
        dockerfile: '$(Build.SourcesDirectory)/datapoint-service/Dockerfile'
        tags: |
          $(tag)
        arguments: --build-arg="NPM_TOKEN=$(NPM_TOKEN)"
    - task: Docker@2
      displayName: Build file-service
      inputs:
        command: build
        repository: 'file-service-$(environment)'
        buildContext: $(Build.SourcesDirectory)
        dockerfile: '$(Build.SourcesDirectory)/file-service/Dockerfile'
        tags: |
          $(tag)
        arguments: --build-arg="NPM_TOKEN=$(NPM_TOKEN)"
    - task: Docker@2
      displayName: Build frontend
      inputs:
        command: build
        repository: 'frontend-$(environment)'
        buildContext: $(Build.SourcesDirectory)
        dockerfile: '$(Build.SourcesDirectory)/frontend/Dockerfile'
        tags: |
          $(tag)
        arguments: --build-arg="NPM_TOKEN=$(NPM_TOKEN)"
    - task: Docker@2
      displayName: Push acl-service
      inputs:
        command: push
        repository: 'acl-service-$(environment)'
        containerRegistry: $(dockerRegistryServiceConnection)
        tags: |
          $(tag)
    - task: Docker@2
      displayName: Push backend-service
      inputs:
        command: push
        repository: 'backend-service-$(environment)'
        containerRegistry: $(dockerRegistryServiceConnection)
        tags: |
          $(tag)
    - task: Docker@2
      displayName: Push datapoint-service
      inputs:
        command: push
        repository: 'datapoint-service-$(environment)'
        containerRegistry: $(dockerRegistryServiceConnection)
        tags: |
          $(tag)
    - task: Docker@2
      displayName: Push file-service
      inputs:
        command: push
        repository: 'file-service-$(environment)'
        containerRegistry: $(dockerRegistryServiceConnection)
        tags: |
          $(tag)
    - task: Docker@2
      displayName: Push frontend
      inputs:
        command: push
        repository: 'frontend-$(environment)'
        containerRegistry: $(dockerRegistryServiceConnection)
        tags: |
          $(tag)

