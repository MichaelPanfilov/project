import { CreateQueryParams, QueryFilter, RequestQueryBuilderOptions } from '@nestjsx/crud-request';

export const REQUEST_OPT: RequestQueryBuilderOptions = {
  paramNamesMap: {
    fields: 'fields',
    search: 'search',
    limit: 'limit',
  },
};

export type RequestFilter = Omit<QueryFilter, 'field'>;

export type ParsedFilter<T extends object> = {
  [key in keyof T]?: RequestFilter | RequestFilter[];
};

export type PaginationParams = Pick<CreateQueryParams, 'limit' | 'offset' | 'page'>;

export type Sorting = [string, 'asc' | 'desc'];
