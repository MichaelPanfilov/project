export enum ReferencedEntities {
  ASSET = 'asset',
  TASK = 'task',
  WORK_INSTRUCTION = 'wi',
  ACL_USER = 'acl_user',
  ACL_ROLE = 'acl_role',
  ACTOR = 'actor',
  FILE = 'file',
  CONTAINER = 'container',
  MACHINE_TYPE_TAG = 'machine-type-tag',
}
