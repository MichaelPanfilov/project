import { HttpException } from '@nestjs/common';
import { GetManyDefaultResponse } from '@nestjsx/crud';

export interface DataResponse<T, U extends {} = {}> {
  data: T;
  meta: U;
}

export interface ErrorResponse<T extends HttpException, U extends {} = {}> {
  error: T;
  meta: U;
}

export interface DataResponseMeta {
  [key: string]: unknown;
}

export function asResponse<T>(obj: T): DataResponse<T>;
export function asResponse<T, U extends {} = {}>(obj: T, metaObj: U): DataResponse<T, U>;
export function asResponse(obj: unknown, metaObj?: unknown) {
  if (metaObj) {
    return {
      data: obj,
      meta: metaObj,
    };
  } else {
    return {
      data: obj,
      meta: {},
    };
  }
}

export function asError<T extends {} | HttpException>(err: T): ErrorResponse<HttpException>;
export function asError<T extends {} | HttpException, U extends {} = {}>(
  err: T,
  metaObj: U,
): ErrorResponse<HttpException, U>;
export function asError<T extends {} | HttpException, U extends {} = {}>(err: T, metaObj?: U) {
  if (metaObj) {
    return {
      error: isHttpException(err) ? err.getResponse() : err,
      meta: metaObj,
    };
  } else {
    return {
      error: isHttpException(err) ? err.getResponse() : err,
      meta: {},
    };
  }
}

export type PagingResponseMeta = Omit<GetManyDefaultResponse<never>, 'data'>;

function isHttpException(err: unknown): err is HttpException {
  return typeof (err as HttpException).getResponse === 'function';
}
