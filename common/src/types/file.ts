export enum ContentTypes {
  PDF = 'application/pdf',
  SVG = 'image/svg',
  PNG = 'image/png',
  JPEG = 'image/jpeg',
  GIF = 'image/gif',
  WEBM = 'video/webm',
  MPEG = 'video/mpeg',
  MP4 = 'video/mp4',
  AVI = 'video/avi',
  WMV = 'video/x-ms-wmv',
  MOV = 'video/quicktime',
  MKV = 'video/x-matroska',
  PLAIN = 'text/plain',
  CSV = 'text/comma-separated-values',
  EXCEL = 'application/msexcel',
  POWER_POINT = 'application/mspowerpoint',
  WORD = 'application/msword',
  ZIP = 'application/zip',
  GZIP = 'application/gzip',
  JSON = 'application/json',
}

export type LicenseTypes = 'csv' | 'json';

export const LicenseContentTypes: { [key in LicenseTypes]: string } = {
  json: ContentTypes.JSON,
  csv: ContentTypes.CSV,
};
