import { BaseDto, CreateDto } from './base';

export interface AssetDto<T extends AssetType = AssetType> extends BaseDto {
  type: T;
  name: string;
  description?: string;
  position: number;
}

export interface AssetTree<
  T extends AssetType,
  P extends BaseAssetTree = BaseAssetTree,
  C extends BaseAssetTree = BaseAssetTree
> extends AssetDto<T> {
  type: T;
  parent: P;
  children: C[];
}

export interface BaseAssetTree<T extends AssetType = AssetType> extends AssetDto {
  type: T;
  parent?: BaseAssetTree;
  children?: BaseAssetTree[];
}

export interface FactoryDto extends AssetDto<AssetType.FACTORY> {
  postalCode?: string;
}
export type FactoryTreeDto = FactoryDto & AssetTree<AssetType.FACTORY, never, LineTreeDto>;

export type LineDto = AssetDto<AssetType.LINE>;
export type LineTreeDto = LineDto & AssetTree<AssetType.LINE, FactoryDto, MachineTreeDto>;

// machineType will become a fixed set of strings.
export interface MachineDto<T extends string = string> extends AssetDto<AssetType.MACHINE> {
  manufacturer?: string;
  machineType?: T;
  serialNumber?: string;
  constructionYear?: number;
  device?: string;
  isLineOutput?: boolean;
}
export type MachineTreeDto<T extends string = string> = MachineDto<T> &
  AssetTree<AssetType.MACHINE, LineDto, never>;

export type CreateAssetDto<T extends AssetDto> = CreateDto<Omit<T, 'type'>>;

export type CreateFactoryDto = CreateAssetDto<FactoryDto>;
export type CreateLineDto = CreateAssetDto<LineDto>;
export type CreateMachineDto = CreateAssetDto<MachineDto>;

export enum AssetType {
  MACHINE = 'machine',
  LINE = 'line',
  FACTORY = 'factory',
}

export const ASSET_TYPE_LABELS: { [key in AssetType]: string } = {
  [AssetType.FACTORY]: 'ASSET.TYPE.FACTORY',
  [AssetType.LINE]: 'ASSET.TYPE.LINE',
  [AssetType.MACHINE]: 'ASSET.TYPE.MACHINE',
};

export function getAssetTypeLabel(value: AssetType): string {
  const res = Object.keys(ASSET_TYPE_LABELS).find(key => key === value);
  return res ? ASSET_TYPE_LABELS[res as AssetType] : value;
}

export const ASSET_HIERARCHY: {
  [key in AssetType]: { childType: AssetType | undefined; parentType: AssetType | undefined };
} = {
  [AssetType.FACTORY]: {
    parentType: undefined,
    childType: AssetType.LINE,
  },
  [AssetType.LINE]: {
    parentType: AssetType.FACTORY,
    childType: AssetType.MACHINE,
  },
  [AssetType.MACHINE]: {
    parentType: AssetType.LINE,
    childType: undefined,
  },
};

export const ASSET_FILE_TAG = 'asset';

export enum MachineType {
  FLOW_WRAPPER = 'flow-wrapper',
  HRM = 'hrm',
}

export function sortByPosition(a: AssetDto, b: AssetDto): number {
  return a.position - b.position;
}

export function sortTrees(trees: BaseAssetTree[]): BaseAssetTree[] {
  trees.sort(sortByPosition);
  trees.forEach(tree => sortTrees(tree.children || []));
  return trees;
}

export function normalizePositions(trees: BaseAssetTree[]): BaseAssetTree[] {
  trees.forEach((tree, i) => (tree.position = i + 1));
  trees.forEach(tree => normalizePositions(tree.children || []));
  return trees;
}

export function flattenAssets(
  assets: BaseAssetTree[],
  prepend: BaseAssetTree[] = [],
): BaseAssetTree[] {
  const reducedAssets = assets.reduce((res: BaseAssetTree[], current: BaseAssetTree) => {
    const next = [...res, current];
    if (current.children) {
      return flattenAssets(current.children, next);
    }
    return next;
  }, []);
  return [...prepend, ...reducedAssets];
}

export function getChildrenCount(asset: BaseAssetTree) {
  return asset.children
    ? asset.children.reduce((res, current) => {
        let count = res;
        if (current.children && current.children.length) {
          count += current.children.length;
        }
        return count + 1;
      }, 0)
    : 0;
}

export function isMachine(asset: AssetDto): asset is MachineDto {
  return asset.type === AssetType.MACHINE;
}
