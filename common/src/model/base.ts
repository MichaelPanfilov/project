export interface BaseDto {
  id: string;
}

export type CreateDto<T extends BaseDto> = Omit<T, 'id'>;
