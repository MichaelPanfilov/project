import { SynMachineState } from '../syntegon';

export interface TimeRange {
  from: Date;
  to: Date;
}

export interface TimelineDto extends TimeRange {
  status: SynMachineState;
}

// tslint:disable-next-line
export function isTimeRange(obj: any): obj is TimeRange {
  const keys: (keyof TimeRange)[] = ['from', 'to'];
  return !keys.find(key => !(obj[key] instanceof Date));
}
