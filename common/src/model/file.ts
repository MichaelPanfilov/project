import { PaginationParams } from '../types';

import { BaseDto } from './base';

export interface FileDto extends BaseDto {
  contentType: string;
  contentTypeGroup: ContentTypeGroup;
  createdAt: string;
  createdBy: string;
  filename: string;
  id: string;
  privateUrl: string;
  publicUrl: string;
  viewUrl: string;
  refIds?: string[];
  machineTypeTags?: string[];
  relativePrivateUrl: string;
  relativePublicUrl: string;
  relativeThumbnailPrivateUrl: string;
  relativeThumbnailPublicUrl: string;
  relativeViewUrl: string;
  size: number;
  tags?: string[];
  thumbnailPrivateUrl: string;
  thumbnailPublicUrl: string;
  updatedAt: string;
  title: string;
  version: number | null;
  containerId: string | null;
}

export interface ContainerDto {
  id: string;
  createdAt: string;
  updatedAt: string;
  files: FileDto[];
}

export type FilePayload = Pick<FileDto, 'refIds' | 'title' | 'machineTypeTags'>;

export function findMaxVersion(docs: FileDto[]) {
  return docs.reduce((res: FileDto | null, next: FileDto) => {
    if (!res || !res.version) return next;
    return next.version && next.version >= Number(res.version) ? next : res;
  }, null);
}

export const DOCUMENT_FILE_TAG = 'documents';

export type ImageDto = FileDto & {
  image?: string;
};

export interface FilesFilter extends PaginationParams {
  id?: string | string[];
  title?: string;
  refId?: string | string[];
  tag?: string | string[];
  machineTypeTag?: string | string[];
  contentTypeGroup?: string | string[];
  contentType?: string | string[];
  order?: string;
}

export enum ContentTypeGroup {
  IMAGE = 'image',
  VIDEO = 'video',
  PDF = 'pdf',
  OTHER = 'other',
}

export const DOC_TYPES: { [key in ContentTypeGroup]?: string[] } = {
  [ContentTypeGroup.IMAGE]: [
    'image/apng',
    'image/bmp',
    'image/gif',
    'image/x-icon',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
    'image/tiff',
    'image/webp',
    'image/svg',
  ],
  [ContentTypeGroup.VIDEO]: [
    'video/3gpp',
    'video/mpeg',
    'video/mp4',
    'video/ogg',
    'video/quicktime',
    'video/webm',
  ],
  [ContentTypeGroup.PDF]: ['application/pdf'],
};
