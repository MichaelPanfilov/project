import { TimeRange } from './timeline';

export interface FaultStopDto extends TimeRange {
  reason: string;
}
