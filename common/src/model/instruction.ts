import { BaseDto, CreateDto } from './base';

export interface InstructionBaseDto extends BaseDto {
  url: string;
  title: string;
  classification: string;
  machineTypes?: string[];
  createdAt: Date;
  updatedAt: Date;
}

export interface InstructionDto extends InstructionBaseDto {
  links: string[];
}

export type CreateInstructionDto = Omit<CreateDto<InstructionDto>, 'createdAt' | 'updatedAt'>;
