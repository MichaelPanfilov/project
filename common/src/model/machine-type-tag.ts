export interface MachineTypeTag {
  name: string;
}

export function sortTags(tags?: string[]): string[] | undefined {
  if (!tags) {
    return undefined;
  }

  return tags.sort((a, b) => {
    if (a.toLowerCase() < b.toLowerCase()) {
      return -1;
    }
    if (a.toLowerCase() > b.toLowerCase()) {
      return 1;
    }
    return 0;
  });
}
