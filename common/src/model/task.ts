import { BaseDto, CreateDto } from './base';

export enum TriggerTypes {
  NOW = 'now',
  COUNTDOWN = 'countdown',
  CALENDAR = 'calendar',
}

export const TRIGGER_TYPE_LABELS: { [key in TriggerTypes]: string } = {
  [TriggerTypes.NOW]: 'TASK.TRIGGER_TYPE.NOW',
  [TriggerTypes.COUNTDOWN]: 'TASK.TRIGGER_TYPE.COUNTDOWN',
  [TriggerTypes.CALENDAR]: 'TASK.TRIGGER_TYPE.CALENDAR',
};

export enum IntervalTypes {
  MINUTES = 'minutes',
  HOURS = 'hours',
  DAYS = 'days',
}

export const INTERVAL_TYPE_LABELS: { [key in IntervalTypes]: string } = {
  [IntervalTypes.MINUTES]: 'TASK.INTERVAL_TYPE.MINUTES',
  [IntervalTypes.HOURS]: 'TASK.INTERVAL_TYPE.HOURS',
  [IntervalTypes.DAYS]: 'TASK.INTERVAL_TYPE.DAYS',
};

export enum RepeatTypes {
  NEVER = 'never',
  HOURLY = 'hours',
  DAILY = 'days',
  WEEKLY = 'weeks',
  MONTHLY = 'months',
}

export const REPEAT_TYPE_LABELS: { [key in RepeatTypes]: string } = {
  [RepeatTypes.NEVER]: 'TASK.REPEAT_TYPE.NEVER',
  [RepeatTypes.HOURLY]: 'TASK.REPEAT_TYPE.HOURLY',
  [RepeatTypes.DAILY]: 'TASK.REPEAT_TYPE.DAILY',
  [RepeatTypes.WEEKLY]: 'TASK.REPEAT_TYPE.WEEKLY',
  [RepeatTypes.MONTHLY]: 'TASK.REPEAT_TYPE.MONTHLY',
};

export function getTriggerTypeLabel(value: TriggerTypes): string {
  const res = Object.keys(TRIGGER_TYPE_LABELS).find(key => key === value);
  return res ? TRIGGER_TYPE_LABELS[res as TriggerTypes] : value;
}

export function getIntervalTypeLabel(value: IntervalTypes): string {
  const res = Object.keys(INTERVAL_TYPE_LABELS).find(key => key === value);
  return res ? INTERVAL_TYPE_LABELS[res as IntervalTypes] : value;
}

export function getRepeatTypeLabel(value: RepeatTypes): string {
  const res = Object.keys(REPEAT_TYPE_LABELS).find(key => key === value);
  return res ? REPEAT_TYPE_LABELS[res as RepeatTypes] : value;
}

export interface BaseTaskDto<T extends TriggerTypes> extends BaseDto {
  title: string;
  description?: string;
  assignedAsset: string | null;
  readonly assetName: string | null;
  createdBy: string;
  assignedPerson?: string;
  dueDate: Date | null;
  triggerType: T;
  status: TaskStatus;
  classification: string;
  machineTypes?: string[];
  doneDate?: Date;
}

export type NowTaskDto = BaseTaskDto<TriggerTypes.NOW>;

export interface CalendarTaskDto extends BaseTaskDto<TriggerTypes.CALENDAR> {
  repeatInterval: RepeatTypes;
  calendarDate: Date;
}

export interface CountdownTaskDto extends BaseTaskDto<TriggerTypes.COUNTDOWN> {
  countdownHours: number;
  unitIntervals: IntervalTypes;
}

export type TaskDto = NowTaskDto | CalendarTaskDto | CountdownTaskDto;

export type CreateTaskDto = Omit<
  CreateDto<TaskDto>,
  'dueDate' | 'status' | 'createdBy' | 'assetName'
>;
export type UpdateTaskDto = Partial<CreateTaskDto> & BaseDto;

export enum TaskStatus {
  INACTIVE = 'inactive',
  ACTIVE = 'active',
  DONE = 'done',
  CLOSED = 'closed',
}

export function isNowTask(task: Partial<TaskDto>): task is NowTaskDto {
  return task.triggerType === TriggerTypes.NOW;
}

export function isCalendarTask(task: Partial<TaskDto>): task is CalendarTaskDto {
  return task.triggerType === TriggerTypes.CALENDAR;
}

export function isCountdownTask(task: Partial<TaskDto>): task is CountdownTaskDto {
  return task.triggerType === TriggerTypes.COUNTDOWN;
}

export function changedTriggerType(old: TaskDto, dto: Partial<TaskDto>): boolean {
  return !!dto.triggerType && dto.triggerType !== old.triggerType;
}

export function dueDateNeedsUpdate(old: TaskDto, dto: Partial<TaskDto>): boolean {
  // Recompute always if trigger type changes.
  if (changedTriggerType(old, dto)) {
    return true;
  }

  if (isNowTask(old)) {
    return old.dueDate !== dto.dueDate;
  }
  if (isCalendarTask(old) && isCalendarTask(dto)) {
    return (
      new Date(old.calendarDate).getTime() !== new Date(dto.calendarDate).getTime() ||
      old.repeatInterval !== dto.repeatInterval
    );
  }
  if (isCountdownTask(old) && isCountdownTask(dto)) {
    return old.countdownHours !== dto.countdownHours || old.unitIntervals !== dto.unitIntervals;
  }
  return true;
}
