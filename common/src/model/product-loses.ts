export interface ProductLossDto {
  time: Date;
  reason: string;
}
