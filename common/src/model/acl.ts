import { BaseDto, CreateDto } from './base';

export interface AclUserDto extends BaseDto {
  name: string;
  email?: string;
  activated: boolean;
  roleIds: string[];
}

export interface CreateAclUserDto
  extends Omit<CreateDto<AclUserDto>, 'name' | 'activated' | 'roleIds'> {
  username: string;
  password: string;
}

export const USER_AVATAR_TAG = 'profile';
