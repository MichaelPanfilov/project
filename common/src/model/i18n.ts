export enum Languages {
  ENGLISH = 'english',
  GERMAN = 'german',
}

export const LANGUAGE_LABELS: { [key in Languages]: string } = {
  [Languages.ENGLISH]: 'MISC.LANGUAGE.ENGLISH',
  [Languages.GERMAN]: 'MISC.LANGUAGE.GERMAN',
};

export const DEFAULT_LANG = 'en';

export const LANG_MAP = {
  [Languages.ENGLISH]: DEFAULT_LANG,
  [Languages.GERMAN]: 'de',
};

export function getLanguageLabel(value: Languages): string {
  const res = Object.keys(LANGUAGE_LABELS).find(key => key === value);
  return res ? LANGUAGE_LABELS[res as Languages] : value;
}

export function toTranslatable<T extends string>(
  en: { [key in T]: string },
): { label: string; value: T }[] {
  return Object.entries(en).map(
    ([value, label]) => ({ label, value } as { label: string; value: T }),
  );
}
