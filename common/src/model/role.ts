import { BaseDto } from './base';

export interface RoleDto extends BaseDto {
  name: string;
  description: string;
}
