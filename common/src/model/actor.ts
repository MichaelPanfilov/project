import { AclUserDto, CreateAclUserDto } from './acl';
import { BaseDto, CreateDto } from './base';
import { Languages } from './i18n';

export enum ActorType {
  ACTOR = 'actor',
  USER = 'user',
  SYSTEM = 'system',
  MACHINE = 'machine',
}

export interface ActorDto extends BaseDto {
  name: string;
  type: ActorType;
  aclId: string;
}

export type CreateActorDto = Omit<
  CreateDto<ActorDto & CreateAclUserDto>,
  'username' | 'type' | 'aclId'
>;

export interface UserDto extends ActorDto {
  language: Languages;
}

// TODO: Once username !== name readd it.
export type CreateUserDto = Omit<
  CreateDto<UserDto & CreateAclUserDto>,
  'username' | 'type' | 'aclId'
>;

export type UpdateUserDto = Omit<CreateUserDto, 'aclId' | 'password' | 'email'>;

export type AppUser = UserDto & AclUserDto;
