export const CLASSIFICATIONS: { value: string; label: string }[] = [
  { value: 'Cleaning', label: 'CLASSIFICATION.OPTION.CLEANING' },
  { value: 'Engineer inspection', label: 'CLASSIFICATION.OPTION.ENGINEER_INSPECTION' },
  { value: 'Operator inspection', label: 'CLASSIFICATION.OPTION.OPERATOR_INSPECTION' },
  { value: 'Functional test', label: 'CLASSIFICATION.OPTION.FUNCTIONAL_TEST' },
  { value: 'Lubrication', label: 'CLASSIFICATION.OPTION.LUBRICATION' },
  { value: 'Modification', label: 'CLASSIFICATION.OPTION.MODIFICATION' },
  { value: 'Pre-use inspection', label: 'CLASSIFICATION.OPTION.PRE_USE_INSPECTION' },
  { value: 'Replace', label: 'CLASSIFICATION.OPTION.REPLACE' },
];

export function getClassificationLabel(value: string): string {
  const res = CLASSIFICATIONS.find(c => c.value === value);
  return res ? res.label : value;
}
