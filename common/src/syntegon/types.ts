export const SYN_STATES = [
  'Running',
  'Stopped',
  'Faulted',
  'Offline',
  'Blocked',
  'Starved',
  'Unknown',
] as const;

export type SynMachineState = typeof SYN_STATES[number];

export type SynDisconnectedState = 'Disconnected';
