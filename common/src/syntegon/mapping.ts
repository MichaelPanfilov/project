import { BaseDto } from '../model';

export interface LocalizedText {
  en: string;
  [lang: string]: string;
}

export enum MappingType {
  REJECT_REASONS = 'reject_reasons',
  STOP_REASONS = 'stop_reasons',
}

export interface MappingData {
  [id: number]: LocalizedText;
}

export interface MappingDto extends BaseDto {
  createdAt: Date;
  updatedAt: Date;
  device: string;
  type: MappingType;
  data: MappingData;
}
