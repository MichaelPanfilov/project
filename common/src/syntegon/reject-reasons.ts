interface Indexed<T> {
  [id: number]: T;
}

export interface Reason {
  en: string;
  de?: string;
}

export const PRODUCT_LOSS_REASON_MAP_TTM_SIM1: Indexed<Reason> & { default: Reason } = {
  '1000': {
    en: 'Products too long',
  },
  '1001': {
    en: 'Product too long infeed 1',
  },
  '1002': {
    en: 'Product too short infeed 1',
  },
  '1003': {
    en: 'Gap too short infeed 1',
  },
  '1004': {
    en: 'Product wrong positioned infeed 1',
  },
  '2000': {
    en: 'Empty Box',
  },
  '2001': {
    en: 'Miscount at LOD',
  },
  '2002': {
    en: 'Bad printed trays',
  },
  '2003': {
    en: 'Open flaps',
  },
  '3000': {
    en: 'Product too short infeed 2',
  },
  default: {
    en: 'Gap too short infeed 1',
  },
};

export const PRODUCT_LOSS_REASON_MAP_TTM_SIM2: Indexed<Reason> & { default: Reason } = {
  '1000': {
    en: 'INF1: PGR not ready',
  },
  '1001': {
    en: 'INF2: PGR not ready',
  },
  '1002': {
    en: 'INF2: Gap too short',
  },
  '1003': {
    en: 'INF2: Prod too long',
  },
  '1004': {
    en: 'INF2: Prod wrong positioned',
  },
  '2000': {
    en: 'FOR1: No reads',
  },
  '2001': {
    en: 'FOR1: Wrong reads',
  },
  '2002': {
    en: 'FOP1: No reads',
  },
  '2003': {
    en: 'INL1: No reads',
  },
  '3000': {
    en: 'TOC1: Miscount',
  },
  default: {
    en: 'INF2: Prod too long',
  },
};

export const FAULT_STOP_REASONS_TTM_SIM1: Indexed<string> & { default: string } = {
  '73305002': 'FOR: Wrong read code reader 1',
  '75333000': 'FOR/CLO: Glue unit not ready',
  '73302007': 'INF: Infeed crash',
  '73302001': 'FOR: Magazine is empty track 1',
  '73305001': 'INF: Infeed jam',
  default: 'TOC: Jam on Outfeed 2',
};

export const FAULT_STOP_REASONS_TTM_SIM2: Indexed<string> & { default: string } = {
  '73305002': 'EMT1/ProdInsp: Product too long',
  '75333000': 'FCC/SvShaft: Channel 2 blocked',
  '73302007': 'EMT1/HighInsp: Product too high',
  '73302001': 'HXX/Heater: Bad registration marks detected',
  '73305001': 'HXX/Splicer: Both film reels are empty',
  default: 'HXX/FilmTrack: Film out of window',
};
