export const FILES = [
  {
    id: '531f2240-6e80-11ea-9019-7d8b7371219c',
    filename: 'unnamed.jpg',
    title: 'Document # 1',
    size: 31800,
    contentType: 'image/jpeg',
    createdAt: '2020-03-25T10:06:41.764Z',
    createdBy: 'Test',
    updatedAt: '2020-03-25T10:06:41.765Z',
    relativePublicUrl:
      '/v1/files/531f2240-6e80-11ea-9019-7d8b7371219c/download/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlSWQiOiI1MzFmMjI0MC02ZTgwLTExZWEtOTAxOS03ZDhiNzM3MTIxOWMiLCJpYXQiOjE1ODUxMzA4MDF9.3DmTmjir14_5uZUTEglYbZppx6-D-DdYcUBbut3ZAx8',
    relativePrivateUrl: '/v1/files/531f2240-6e80-11ea-9019-7d8b7371219c/download',
    publicUrl:
      'http://localhost:4080/v1/files/531f2240-6e80-11ea-9019-7d8b7371219c/download/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlSWQiOiI1MzFmMjI0MC02ZTgwLTExZWEtOTAxOS03ZDhiNzM3MTIxOWMiLCJpYXQiOjE1ODUxMzA4MDF9.3DmTmjir14_5uZUTEglYbZppx6-D-DdYcUBbut3ZAx8',
    privateUrl: 'http://localhost:4080/v1/files/531f2240-6e80-11ea-9019-7d8b7371219c/download',
    relativeThumbnailPublicUrl:
      '/v1/files/531f2240-6e80-11ea-9019-7d8b7371219c/thumbnail/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlSWQiOiI1MzFmMjI0MC02ZTgwLTExZWEtOTAxOS03ZDhiNzM3MTIxOWMiLCJpYXQiOjE1ODUxMzA4MDF9.3DmTmjir14_5uZUTEglYbZppx6-D-DdYcUBbut3ZAx8',
    relativeThumbnailPrivateUrl: '/v1/files/531f2240-6e80-11ea-9019-7d8b7371219c/thumbnail',
    thumbnailPublicUrl:
      'http://localhost:4080/v1/files/531f2240-6e80-11ea-9019-7d8b7371219c/thumbnail/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlSWQiOiI1MzFmMjI0MC02ZTgwLTExZWEtOTAxOS03ZDhiNzM3MTIxOWMiLCJpYXQiOjE1ODUxMzA4MDF9.3DmTmjir14_5uZUTEglYbZppx6-D-DdYcUBbut3ZAx8',
    thumbnailPrivateUrl:
      'http://localhost:4080/v1/files/531f2240-6e80-11ea-9019-7d8b7371219c/thumbnail',
    relativeViewUrl: 'v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/view',
    viewUrl: 'http://localhost:4080/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/view',
    containerId: null,
    version: null,
    refIds: ['6ee16513-f5c8-4612-aff5-937dd1223c27'],
    documentType: 'PDF',
    machineTypes: ['DBSE (Product Transportation)', 'DGDE (Product Transportation)'],
    contentTypeGroup: 'image',
    // asset: {
    //   type: 'machine',
    //   id: '72d57c16-6336-4a84-9980-33fbd0a99783',
    //   name: 'DGDE-1',
    //   description: '',
    //   image: '',
    //   manufacturer: 'Syntegon',
    //   machineType: 'DGDE',
    //   serialNumber: 'B-77539-DGDE-740',
    //   constructionYear: 2015,
    // },
    tags: ['test-tags'],
  },
  {
    id: '45fc4630-6e88-11ea-9019-7d8b7371219c',
    filename: 'photo-1492684223066-81342ee5ff30.jpeg',
    title: 'Document # 2',
    size: 46100,
    contentType: 'application/pdf',
    createdAt: '2020-03-25T11:03:35.700Z',
    createdBy: 'Test',
    updatedAt: '2020-03-25T11:03:35.700Z',
    relativePublicUrl:
      '/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/download/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlSWQiOiI0NWZjNDYzMC02ZTg4LTExZWEtOTAxOS03ZDhiNzM3MTIxOWMiLCJpYXQiOjE1ODUxMzQyMTV9.vTd6b8_YjyUqOZSe0CXEjg6reDG6Yew_KG5xu9hQTww',
    relativePrivateUrl: '/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/download',
    publicUrl:
      'http://localhost:4080/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/download/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlSWQiOiI0NWZjNDYzMC02ZTg4LTExZWEtOTAxOS03ZDhiNzM3MTIxOWMiLCJpYXQiOjE1ODUxMzQyMTV9.vTd6b8_YjyUqOZSe0CXEjg6reDG6Yew_KG5xu9hQTww',
    privateUrl: 'http://localhost:4080/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/download',
    relativeThumbnailPublicUrl:
      '/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/thumbnail/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlSWQiOiI0NWZjNDYzMC02ZTg4LTExZWEtOTAxOS03ZDhiNzM3MTIxOWMiLCJpYXQiOjE1ODUxMzQyMTV9.vTd6b8_YjyUqOZSe0CXEjg6reDG6Yew_KG5xu9hQTww',
    relativeThumbnailPrivateUrl: '/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/thumbnail',
    thumbnailPublicUrl:
      'http://localhost:4080/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/thumbnail/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaWxlSWQiOiI0NWZjNDYzMC02ZTg4LTExZWEtOTAxOS03ZDhiNzM3MTIxOWMiLCJpYXQiOjE1ODUxMzQyMTV9.vTd6b8_YjyUqOZSe0CXEjg6reDG6Yew_KG5xu9hQTww',
    thumbnailPrivateUrl:
      'http://localhost:4080/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/thumbnail',
    relativeViewUrl: 'v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/view',
    viewUrl: 'http://localhost:4080/v1/files/45fc4630-6e88-11ea-9019-7d8b7371219c/view',
    containerId: null,
    version: null,
    refIds: ['6ee16513-f5c8-4612-aff5-937dd1223c27'],
    // asset: {
    //   type: 'line',
    //   id: '5376b6d7-761a-4ab8-a938-a017eb996ff2',
    //   name: 'Line A',
    //   description: '',
    //   image: null,
    // },
    documentType: 'Image',
    machineTypes: ['FGM/HCS (Product Grouping & Primary Packaging)', 'Fxx'],
    contentTypeGroup: 'video',
    tags: ['test-tags'],
  },
];
