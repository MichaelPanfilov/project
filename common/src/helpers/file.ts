export const supportedImageFormats = ['jpeg', 'jpg', 'png', 'gif', 'svg'];

export const supportedVideoFormats = [
  'webm',
  'mpeg',
  'mp4',
  'avi',
  'wmv',
  'mov',
  'mkv',
  'mk3d',
  'mka',
  'mks',
];

export const supportedExcelFormats = ['xla', 'xlam', 'xls', 'xlsb', 'xlsm', 'xlsx'];

export const supportedPowerPointFormats = ['ppt', 'pptm', 'pptx'];

export const supportedWordFormats = ['doc', 'docm', 'docx'];

export const allSupportedFormats = [
  ...supportedImageFormats,
  ...supportedVideoFormats,
  ...supportedExcelFormats,
  ...supportedPowerPointFormats,
  ...supportedWordFormats,
  'pdf',
  'txt',
  'csv',
  'zip',
  'gzip',
  'json',
];

export const getExtension = (filename: string): string =>
  filename.slice(((filename.lastIndexOf('.') - 1) >>> 0) + 2);

export const isImage = (ext: string): boolean => supportedImageFormats.includes(ext.toLowerCase());

export const isFileSupported = (ext: string): boolean =>
  allSupportedFormats.includes(ext.toLowerCase());
