import { v4 as uuidV4 } from 'uuid';

import { ReferencedEntities } from '../types';

import { getRefId } from './ref-id';

export function generateId(entity: ReferencedEntities) {
  const id = uuidV4();
  return getRefId(id, entity);
}
