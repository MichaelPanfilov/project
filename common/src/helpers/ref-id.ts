import { ReferencedEntities } from '../types';

export function getRefId(id: string, entity: ReferencedEntities) {
  return `${entity}-${id}`;
}

export function getEntityFromRefId(id: string): ReferencedEntities | undefined {
  return Object.values(ReferencedEntities).find(ref => id.startsWith(ref));
}

export function getRefIdsOf(entity: ReferencedEntities, ids: string[] = []): string[] {
  return ids.filter(id => getEntityFromRefId(id) === entity);
}
