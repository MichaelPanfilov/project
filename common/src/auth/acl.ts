export interface Acl {
  resources: AclResource[];
  right: AclRight;
}

export const ACL_RESOURCES = ['Task', 'Document', 'Instruction', 'Asset', 'User', 'Role'] as const;
export type AclResource = typeof ACL_RESOURCES[number];

export const ACL_RIGHTS = ['Read', 'Create', 'Update', 'Delete', 'Execute'] as const;
export type AclRight = typeof ACL_RIGHTS[number];

export const CRUD_RIGHTS = ['READ', 'CREATE', 'UPDATE', 'DELETE'] as const;
export type CrudRight = typeof CRUD_RIGHTS[number];

export type TaskExtraRight = 'EXECUTE';

export const TaskAcl: { [key in CrudRight | TaskExtraRight]: Acl } = {
  READ: {
    resources: ['Task'],
    right: 'Read',
  },
  CREATE: {
    resources: ['Task'],
    right: 'Create',
  },
  UPDATE: {
    resources: ['Task'],
    right: 'Update',
  },
  DELETE: {
    resources: ['Task'],
    right: 'Delete',
  },
  EXECUTE: {
    resources: ['Task'],
    right: 'Execute',
  },
};

export const DocumentAcl: { [key in CrudRight]: Acl } = {
  READ: {
    resources: ['Document'],
    right: 'Read',
  },
  CREATE: {
    resources: ['Document'],
    right: 'Create',
  },
  UPDATE: {
    resources: ['Document'],
    right: 'Update',
  },
  DELETE: {
    resources: ['Document'],
    right: 'Delete',
  },
};

export const InstructionAcl: { [key in CrudRight]: Acl } = {
  READ: {
    resources: ['Instruction'],
    right: 'Read',
  },
  CREATE: {
    resources: ['Instruction'],
    right: 'Create',
  },
  UPDATE: {
    resources: ['Instruction'],
    right: 'Update',
  },
  DELETE: {
    resources: ['Instruction'],
    right: 'Delete',
  },
};

export const AssetAcl: { [key in CrudRight]: Acl } = {
  READ: {
    resources: ['Asset'],
    right: 'Read',
  },
  CREATE: {
    resources: ['Asset'],
    right: 'Create',
  },
  UPDATE: {
    resources: ['Asset'],
    right: 'Update',
  },
  DELETE: {
    resources: ['Asset'],
    right: 'Delete',
  },
};

export const UserAcl: { [key in CrudRight]: Acl } = {
  READ: {
    resources: ['User'],
    right: 'Read',
  },
  CREATE: {
    resources: ['User'],
    right: 'Create',
  },
  UPDATE: {
    resources: ['User'],
    right: 'Update',
  },
  DELETE: {
    resources: ['User'],
    right: 'Delete',
  },
};

export const RoleAcl: { [key in CrudRight]: Acl } = {
  READ: {
    resources: ['Role'],
    right: 'Read',
  },
  CREATE: {
    resources: ['Role'],
    right: 'Create',
  },
  UPDATE: {
    resources: ['Role'],
    right: 'Update',
  },
  DELETE: {
    resources: ['Role'],
    right: 'Delete',
  },
};
