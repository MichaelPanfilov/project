export const SESSION_COOKIE_NAME = '__syn_session';

export interface JWTPayload {
  userId: string;
  username: string;
  type: string;
  roles: string[];
  rights: string[];
  iat: number;
  exp: number;
}
