import { Acl, AclResource, AclRight } from './acl';

function encode(resources: AclResource[], right: AclRight): string {
  if (!resources.length) {
    throw new Error('At least one resource must be supplied');
  }
  if (!right.length) {
    throw new Error('Right must be a valid string');
  }
  return `${resources.join('.')}:${right}`;
}

export function encodeRight(acl: Acl): string;
export function encodeRight(resource: AclResource[], right: AclRight): string;
export function encodeRight(aclOrRes: Acl | AclResource[], right?: AclRight): string {
  if (!right) {
    const acl = aclOrRes as Acl;
    return encode(acl.resources, acl.right);
  }
  return encode(aclOrRes as AclResource[], right);
}

export function decodeRight(rightStr: string): Acl {
  const split = rightStr.split(':');
  if (split.length !== 2) {
    throw new Error('Illegal right string');
  }
  return {
    resources: split[0].split('.') as AclResource[],
    right: split[1] as AclRight,
  };
}
