import { Acl } from './acl';
import { decodeRight } from './rights';

export function hasAclResource(rights: string[], acls: Acl[]): boolean {
  return rights.some(right => acls.some(acl => decodeRight(right).resources === acl.resources));
}

export function matchesAcl(rights: string[], acls: Acl[]): boolean {
  const reqAcls = rights.map(decodeRight);
  return reqAcls.some(a => {
    if (!acls.some(({ right }) => right === a.right)) {
      return false;
    }
    // Must mach all resources.
    if (a.resources.some(r => !acls.some(acl => acl.resources.includes(r)))) {
      return false;
    }
    return true;
  });
}
