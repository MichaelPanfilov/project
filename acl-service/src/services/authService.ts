import { AclResource, AclRight, encodeRight, JWTPayload } from '@common';
import * as bcrypt from 'bcryptjs';
import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import { Request } from 'express';
import * as jwt from 'jsonwebtoken';

import { Config } from '../core/config';
import { ErrorMessages } from '../core/consts';
import { LogoutAll } from '../models/logout_all';
import { User } from '../models/user';
import { AclRepo } from '../repos/aclRepo';
import { UserRepo } from '../repos/userRepo';
import { AuthenticatedRequest } from '../types';

export const serviceName = 'authService';

const isDev = process.env.NODE_ENV === 'development';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(serviceName, AuthService, 'AclRepo', 'UserRepo');
}

export class AuthService {
  constructor(private aclRepo: AclRepo, private userRepo: UserRepo) {}

  getToken(authorization: string) {
    return authorization.split(' ')[1];
  }

  async verifyToken(token: string): Promise<{ valid: boolean; expired: boolean }> {
    if (!token) {
      return { valid: false, expired: false };
    }

    if (
      !jwt.verify(token, Config.jwtPublic, {
        algorithms: [Config.jwtAlgorithm],
        ignoreExpiration: true,
      })
    ) {
      return { valid: false, expired: false };
    }

    const payload = this.getTokenPayload(token);

    if (!payload) {
      return { valid: false, expired: false };
    }

    const { iat: issueTime, exp: expireTime, userId } = payload;

    const data = await LogoutAll.query()
      .where('user_id', userId)
      .where('date', '>', issueTime * 1000);

    if (data.length) {
      return { valid: false, expired: false };
    }

    return { valid: true, expired: expireTime * 1000 < Date.now() };
  }

  async verifyInternalToken(token: string): Promise<boolean> {
    try {
      const verified = await jwt.verify(token, Config.jwtInternalPublic, {
        algorithms: [Config.jwtInternalAlgorithm],
      });
      return !!verified;
    } catch (e) {
      return false;
    }
  }

  getTokenPayload(jwsToken: string): JWTPayload | null {
    return jwt.decode(jwsToken, { json: true }) as JWTPayload;
  }

  verifyPassword(user: User, password: string) {
    if (!bcrypt.compareSync(password, user.password)) {
      throw new commonErrors.ValidationError('Invalid username / password');
    }
  }

  async generateToken({ id, name }: User) {
    const user = await User.query()
      .where('id', id)
      .eager('roles')
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    const rolesIds = user.roles.map(role => role.id);

    const rightsList = await this.aclRepo.getRightsList(id);
    const rights = rightsList.map(r =>
      encodeRight({ resources: [r.resourceId as AclResource], right: r.right as AclRight }),
    );

    // iat and exp will be added by the jwt module.
    const jwtPayload: Omit<JWTPayload, 'iat' | 'exp'> = {
      userId: id,
      username: user.name,
      type: 'user',
      roles: rolesIds,
      rights: Array.from(new Set(rights)),
    };

    return jwt.sign(jwtPayload, Config.jwtSecret, {
      algorithm: Config.jwtAlgorithm,
      expiresIn: Config.jwtValidityS + 's',
    });
  }

  async logout(userId: string) {
    const logout = await LogoutAll.query()
      .where('user_id', userId)
      .first();

    let logoutId;

    if (logout) {
      logoutId = logout.id;
    }

    return LogoutAll.query().upsertGraph(
      { id: logoutId, userId, date: Date.now() },
      { insertMissing: true },
    );
  }

  async renew(token: string) {
    const tokenPayload = await this.getTokenPayload(token);

    if (!tokenPayload) {
      throw new commonErrors.AuthenticationRequiredError('Invalid Authorization');
    }

    const { iat: issueTime, userId } = tokenPayload;

    const data = await LogoutAll.query()
      .where('user_id', userId)
      .where('date', '>', issueTime * 1000);

    if (data.length) {
      throw new commonErrors.AuthenticationRequiredError('Invalid Authorization');
    }

    const user = await this.userRepo.getUserById(userId);

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    return this.generateToken(user);
  }

  async requireAllowed(req: Request, resourceId: string, rightKey: string) {
    if (Config.allowAll || req.auth.isAllAllowed) {
      return;
    }

    const isAllowed = await this.aclRepo.isAllowed(req.auth.userId as string, resourceId, rightKey);

    if (!isAllowed) {
      const msg = isDev
        ? `Missing right - ${resourceId}:${rightKey}`
        : ErrorMessages.MISSING_RIGHTS;
      throw new commonErrors.ValidationError(msg);
    }
  }

  async requireAllowedMulti(req: AuthenticatedRequest, acls: { [key: string]: string }) {
    const result = await Promise.all(
      Object.keys(acls).map(key => this.aclRepo.isAllowed(req.auth.userId, key, acls[key])),
    );

    if (!result.includes(true)) {
      throw new commonErrors.ValidationError(ErrorMessages.MISSING_RIGHTS);
    }
  }
}
