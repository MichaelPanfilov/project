import * as Bottle from 'bottlejs';
import sinon = require('sinon');

import { userMock } from '../models/user.mock';

import { serviceName } from './authService';

export const mockServiceName = serviceName + 'Mock';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(mockServiceName, AuthServiceMock);
}

export class AuthServiceMock {
  static factory(bottle: Bottle) {
    bottle.factory(serviceName, () => {
      const stub = new this();

      return {
        login: stub.login,
        getToken: stub.getToken,
        verifyToken: stub.verifyToken,
        getTokenData: stub.getTokenData,
        verifyPassword: stub.verifyPassword,
        generateToken: stub.generateToken,
        requireAllowedMulti: stub.requireAllowedMulti,
        requireAllowed: stub.requireAllowed,
      };
    });
  }

  async login(...args: unknown[]) {
    return sinon.fake.resolves('tokenMock')(...args);
  }

  getToken(...args: unknown[]) {
    return sinon.fake.resolves('tokenMock')(...args);
  }

  verifyToken(...args: unknown[]) {
    const stub = sinon.stub();
    stub.returns(true);
    stub.withArgs('invalidLogin').returns(false);
    return stub(...args);
  }

  getTokenData(...args: unknown[]) {
    return sinon.stub().returns({ username: 'foo' })(...args);
  }

  verifyPassword(...args: unknown[]) {
    const stub = sinon.stub();
    stub.returns(false);
    stub.withArgs(userMock, 'fooBar').returns(true);
    return stub(...args);
  }

  generateToken(...args: unknown[]) {
    return sinon.stub().returns('tokenMock')(...args);
  }

  requireAllowedMulti(...args: unknown[]) {
    return sinon.stub().returns(true)(...args);
  }

  requireAllowed(...args: unknown[]) {
    return sinon.stub().returns(true)(...args);
  }
}
