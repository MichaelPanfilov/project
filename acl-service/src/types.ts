import * as Express from 'express';

export interface AuthenticatedRequest extends Express.Request {
  auth: import('@common').JWTPayload;
}
