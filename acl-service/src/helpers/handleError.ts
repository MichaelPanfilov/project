import * as httpErrors from 'http-errors';

interface CommonError {
  name: string;
  argumentName: string;
  entity_name: string;
  message: string;
}

export const handleError = (e: CommonError) => {
  switch (e.name) {
    case 'ArgumentError':
      throw new httpErrors.BadRequest(`${e.argumentName} is invalid`);
    case 'NotFoundError':
      throw new httpErrors.NotFound(`Cannot find ${e.entity_name}`);
    case 'AlreadyInUseError':
      throw new httpErrors.Conflict(`${e.entity_name} already exist with this args`);
    case 'AuthenticationRequiredError':
      throw new httpErrors.Unauthorized('Invalid Authorization');
    case 'ValidationError':
      throw new httpErrors.Forbidden(e.message);
    default:
      throw new httpErrors.InternalServerError('Server error');
  }
};
