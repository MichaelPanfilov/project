import * as glob from 'glob-promise';
import * as path from 'path';

import { createLogger } from '../core/logger';

const logger = createLogger('requireFolder');

export async function requireFolder(folderPath: string, ...args: unknown[]) {
  logger.trace(`requireFolder(${folderPath})`);

  const moduleFiles = await glob(path.join(folderPath, `/**/!(*.spec|*.mock).js`));

  for (const moduleFile of moduleFiles) {
    let requiredModule;
    try {
      requiredModule = require(moduleFile);
    } catch (ex) {
      logger.fatal(`Error during require(${moduleFile})`, ex);
      throw ex;
    }

    if (typeof requiredModule === 'function') {
      logger.trace(`require(${moduleFile})(..)`);
      requiredModule(...args);
    } else {
      let found = false;
      for (const moduleExportKey of Object.keys(requiredModule)) {
        if (moduleExportKey.match(/Factory$/)) {
          logger.trace(`require(${moduleFile}).${moduleExportKey}(..)`);
          requiredModule[moduleExportKey](...args);
          found = true;
          break;
        }
      }

      if (!found) {
        logger.fatal(`No Factory function found in module ${moduleFile}`);
        throw new Error(`No Factory function found in module ${moduleFile}`);
      }
    }
  }
}
