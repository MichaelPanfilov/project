import errorHandler from '@elunic/express-error-handler';
import { mockBottlejsLogService } from '@elunic/logger/mocks';
import * as bodyParser from 'body-parser';
import * as Bottle from 'bottlejs';
import { Express, NextFunction, Request, Response } from 'express';
import * as sinon from 'sinon';
import * as superagent from 'superagent';

export class SpecHelper {
  static init(bottle: Bottle, app: Express) {
    bottle.factory('log', mockBottlejsLogService('test'));

    // Setup express
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(bodyParser.text());
    app.use(bodyParser.raw());

    SpecHelper.mockAuthentication(app);
  }

  static errorHandler(app: Express) {
    // Setup error handler after all other middlewares/routes
    const errorMw = errorHandler({
      full: true,
    });
    app.use(errorMw);
    return errorMw;
  }

  static mockAuthentication(app: Express) {
    app.use((req: Request, res: Response, next: NextFunction) => {
      if (req.headers.authorization === 'valid' || req.headers.authorization === 'Bearer valid') {
        req.auth = {
          username: 'foo',
          userId: '1',
        };
      } else {
        req.auth = {
          username: undefined,
          userId: undefined,
        };
      }

      next();
    });
  }

  static mockInvalidAuthService(bottle: Bottle) {
    bottle.factory('authService', () => {
      return {
        login: sinon.fake.resolves('asdf'),
        getToken: sinon.fake.resolves('asdf'),
        verifyToken: sinon.fake.resolves(false),
        generateToken: sinon.stub().returns('asdf'),
      };
    });
  }

  // tslint:disable-next-line
  static expectApiResponse(res: superagent.Response, jasmineObject: any) {
    return expect(res.body).toEqual(
      jasmine.objectContaining({
        data: jasmineObject,
        meta: jasmine.any(Object),
      }),
    );
  }
}
