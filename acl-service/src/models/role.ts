import { QueryContext } from 'objection';

import { Acl } from './acl';
import { Model } from './model';
import { UsersRoles } from './users_roles';

export class Role extends Model {
  id!: string;
  name!: string;
  description!: string;

  static tableName = 'roles';

  static jsonSchema = {
    type: 'object',
    required: ['id', 'name'],

    properties: {
      id: { type: 'string' },
      createdAt: { type: 'string', format: 'date-time' },
      updatedAt: { type: 'string', format: 'date-time' },

      name: { type: 'string' },
      description: { type: 'string' },
    },
  };

  async $afterDelete(queryContext: QueryContext) {
    await Promise.all([
      UsersRoles.query()
        .delete()
        .where('role_id', this.id),
      Acl.query()
        .delete()
        .where('role_id', this.id),
    ]);
  }

  get publicData(): object {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
    };
  }
}
