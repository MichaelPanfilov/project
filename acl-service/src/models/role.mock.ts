import { Role } from './role';

export const roleMock = new Role();
roleMock.$set({
  roleId: '1',
  name: 'foo',
  description: 'bar',
});
