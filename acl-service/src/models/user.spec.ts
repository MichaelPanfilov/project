import { createDatabase, dropDatabase } from '../../test/helpers/connection';
import { knex } from '../core/knex';

describe('Model: User', () => {
  beforeAll(async () => {
    await createDatabase();
  });

  afterAll(async () => {
    await dropDatabase();
  });

  beforeEach(async () => {
    await knex.migrate.rollback();
    await knex.migrate.latest();
  });
});
