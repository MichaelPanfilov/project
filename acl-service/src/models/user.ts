import * as Objection from 'objection';
import * as path from 'path';

import { Model } from './model';
import { Role } from './role';

export class User extends Model {
  id!: string;
  name!: string;
  password!: string;
  email!: string | null;
  activated!: boolean;
  roles!: Role[];

  static tableName = 'users';

  static jsonSchema = {
    type: 'object',
    required: ['id', 'name', 'password'],

    properties: {
      id: { type: 'string' },
      createdAt: { type: 'string', format: 'date-time' },
      updatedAt: { type: 'string', format: 'date-time' },

      name: { type: 'string' },
      password: { type: 'string' },
      email: { type: ['string', 'null'] },
      activated: { type: 'boolean' },
    },
  };

  static relationMappings: Objection.RelationMappings = {
    roles: {
      relation: Model.ManyToManyRelation,
      modelClass: path.join(__dirname, '/role'),
      join: {
        from: 'users.id',
        through: {
          from: 'users_roles.user_id',
          to: 'users_roles.role_id',
        },
        to: 'roles.id',
      },
    },
  };

  get publicData(): object {
    return {
      name: this.name,
      email: this.email || '',
      activated: this.activated,
    };
  }

  get privateData(): object {
    return {
      id: this.id,
      name: this.name,
      email: this.email || '',
      activated: this.activated,
      roleIds: this.roles.map(r => r.id),
    };
  }
}
