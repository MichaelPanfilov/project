import { Model } from './model';

export class Acl extends Model {
  id!: number;
  roleId!: string;
  resourceId!: string;
  rightKey!: string;

  static tableName = 'acl';

  static jsonSchema = {
    type: 'object',
    required: ['roleId', 'resourceId', 'rightKey'],

    properties: {
      id: { type: 'integer' },
      createdAt: { type: 'string', format: 'date-time' },
      updatedAt: { type: 'string', format: 'date-time' },

      roleId: { type: 'string' },
      resourceId: { type: 'string' },
      rightKey: { type: 'string' },
    },
  };
}
