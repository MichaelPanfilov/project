import { Model } from './model';

export class UsersRoles extends Model {
  userId!: string;
  roleId!: string;

  static tableName = 'users_roles';

  static jsonSchema = {
    type: 'object',
    required: ['userId', 'roleId'],

    properties: {
      id: { type: 'integer' },
      createdAt: { type: 'string', format: 'date-time' },
      updatedAt: { type: 'string', format: 'date-time' },

      userId: { type: 'string' },
      roleId: { type: 'string' },
    },
  };
}
