import { Model } from './model';

export class LogoutAll extends Model {
  userId!: string;
  date!: number;

  static tableName = 'logout_all';

  static jsonSchema = {
    type: 'object',
    required: ['userId', 'date'],

    properties: {
      id: { type: 'integer' },
      userId: { type: 'string' },
      date: { type: 'integer' },

      createdAt: { type: 'string', format: 'date-time' },
      updatedAt: { type: 'string', format: 'date-time' },
    },
  };
}
