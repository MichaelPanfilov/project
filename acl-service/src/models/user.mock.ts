import { roleMock } from './role.mock';
import { User } from './user';

export const userMock = new User();
userMock.$set({
  id: '1',
  name: 'foo',
  password: 'pass',
  email: 'foo@example.com',
  activated: true,
  roles: [roleMock],

  // userId: 1,
  // name: 'foo',
  // description: "admin"
});
