declare namespace Express {
  interface Request {
    auth: {
      username?: string;
      userId?: string;
      expired?: boolean;
      isAllAllowed?: boolean;
    };
  }
}
declare module 'is-iso-date' {
  function isIsoDate(date: string): boolean;

  export = isIsoDate;
}
