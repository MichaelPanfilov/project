/* tslint:disable:variable-name (allow Config to be written in all caps to distinguish it from the config module and make it easier to read [wh]) */

import * as config from 'config';
import * as path from 'path';
import * as Knex from 'knex';

import cloneDeep = require('lodash.clonedeep');

const knexfile = require(path.join(process.cwd(), '/knexfile.js'));

if (!process.env.NODE_ENV || !(process.env.NODE_ENV in knexfile)) {
  throw new Error('NODE_ENV environment not set or not present in knexConfig');
}

// @ts-ignore: we can't declare the object using the keys in one step and TS complains about the empty object, same below [wh]
const Config: ConfigInterface = {};
for (const key of Object.getOwnPropertyNames(config)) {
  // @ts-ignore
  Config[key] = cloneDeep(config[key]);
}

Config.knexConfig = cloneDeep(knexfile[process.env.NODE_ENV]);

interface ConfigInterface {
  jwtPublic: string;
  jwtSecret: string;
  jwtInternalPublic: Buffer;
  jwtInternalSecret: Buffer;
  jwtAlgorithm: string;
  jwtInternalAlgorithm: string;
  jwtValidityS: number;

  httpPort: number;
  fullErrorStacks: boolean;

  dataPath: string;

  logNamespace: 'app';
  logPath: string;

  uploadPath: string;

  knexConfig: Knex.Config;

  allowAll: string;
}

export { Config };
