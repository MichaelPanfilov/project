import { AclResource, AclRight } from '@common';
import * as Bottle from 'bottlejs';
import { NextFunction, Request, Response } from 'express';

import { handleError } from '../../helpers/handleError';

export function requireAllowed(
  bottle: Bottle,
  resourceId: AclResource,
  rightKey: AclRight,
  allowSelf?: { idKey: string },
) {
  return async (req: Request, res: Response, next: NextFunction) => {
    if (allowSelf) {
      const id = req.params[allowSelf.idKey];
      if (id && id === req.auth.userId) {
        next();
        return;
      }
    }
    try {
      await bottle.container.authService.requireAllowed(req, resourceId, rightKey);
    } catch (e) {
      try {
        handleError(e);
      } catch (e) {
        return next(e);
      }
    }
    next();
  };
}

export function requireAllowedMulti(bottle: Bottle, acls: { [key in AclResource]: AclRight }) {
  return async (req: Request, res: Response, next: NextFunction) => {
    const aclsTarget = Object.entries(acls).reduce(
      (acc, [key, value]) => ({ [key]: value, ...acc }),
      {},
    );

    try {
      await bottle.container.authService.requireAllowedMulti(req, aclsTarget);
    } catch (e) {
      try {
        handleError(e);
      } catch (e) {
        return next(e);
      }
    }
    next();
  };
}
