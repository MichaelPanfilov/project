import { NextFunction, Request, Response } from 'express';
import * as httpErrors from 'http-errors';

import { Config } from '../config';

export function requireValidToken(req: Request, res: Response, next: NextFunction) {
  if (Config.allowAll) {
    next();
    return;
  }
  if (!req.auth.userId) {
    throw new httpErrors.Unauthorized('Invalid Authorization');
  }

  next();
}
