import { NextFunction, Request, Response } from 'express';
import * as httpErrors from 'http-errors';

import { Config } from '../config';

export function requireLogin(req: Request, res: Response, next: NextFunction) {
  if (Config.allowAll || req.auth.isAllAllowed) {
    next();
    return;
  }
  if (!req.auth.userId) {
    throw new httpErrors.Unauthorized('Invalid Authorization');
  }
  if (req.auth.expired) {
    throw new httpErrors.Unauthorized('Token expired');
  }

  next();
}
