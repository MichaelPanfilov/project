import * as Bottle from 'bottlejs';
import { makeMiddlewareInvoker } from 'bottlejs-express';
import { NextFunction, Request, Response, Router } from 'express';

import { AuthService } from '../../services/authService';
import { createLogger } from '../logger';

const logger = createLogger('authorization');

export function Factory({ expressApp, bottle }: { expressApp: Router; bottle: Bottle }) {
  expressApp.use(makeMiddlewareInvoker(bottle, AuthorizationMiddlewareFactory));
}

function AuthorizationMiddlewareFactory({ authService }: { authService: AuthService }) {
  return async function AuthorizationMiddleware(req: Request, res: Response, next: NextFunction) {
    req.auth = {};

    try {
      const authorization =
        req.header('Authorization') ||
        (req.query && req.query.Authorization) ||
        (req.body && req.body.Authorization);
      // || req.session.apiToken

      if (!authorization) {
        return next();
      }

      const token = authService.getToken(authorization);

      try {
        const isAllAllowed = await authService.verifyInternalToken(token);
        req.auth.isAllAllowed = isAllAllowed;
        if (isAllAllowed) {
          return next();
        }
      } catch {}

      const tokenData = authService.getTokenPayload(token);
      const { valid, expired } = await authService.verifyToken(token);

      if (!tokenData || !valid) {
        return next();
      }

      req.auth.expired = expired;
      req.auth.username = tokenData.username;
      req.auth.userId = tokenData.userId;

      next();
    } catch (ex) {
      // logger.error('Failed to authenticate request: ', ex);

      // Don't crash the server by propagating an error. Even without authorization, some routes
      // might be accessible. The others will return 401 - but we've logged the error. [wh]
      // next(new Error('Failed to authenticate request, ' + (ex.message ? ex.message : '')));

      // Instead: [wh]
      next();
    }
  };
}
