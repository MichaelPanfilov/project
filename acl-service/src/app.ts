import errorHandler from '@elunic/express-error-handler';
import { bottlejsLogService } from '@elunic/logger';
import * as bodyParser from 'body-parser';
import * as Bottle from 'bottlejs';
import 'bottlejs-express/strict';
import * as cors from 'cors';
import 'dotenv/config';
import * as express from 'express';
import 'module-alias/register';
import * as morgan from 'morgan';

import { Config } from './core/config';
import { logger } from './core/logger';
import { requireFolder } from './helpers/requireFolder';

logger.info(`Starting up`);
logger.info(`NODE_ENV=${process.env.NODE_ENV}`, `LOG_LEVEL=${process.env.LOG_LEVEL}`);

(async () => {
  // Dependecy injection framework
  const bottle = new Bottle('fcServer');

  bottle.factory('log', bottlejsLogService(logger));

  // Load all services
  await requireFolder(`${__dirname}/services`, { bottle });

  // Provide all repos in the DI container
  await requireFolder(`${__dirname}/repos`, { bottle });

  // Express
  const expressApp = express();

  // request logging middleware
  expressApp.use(morgan('combined'));
  expressApp.use(cors());
  // DO NOT USE EXPRESS-FORMIDABLE. It has numerous bugs that prevent correct async
  // route handling. [wh]
  expressApp.use(bodyParser.urlencoded({ extended: true }));
  expressApp.use(bodyParser.json());
  expressApp.use(bodyParser.text());
  expressApp.use(bodyParser.raw());

  // Misc options
  if (process.env.NODE_ENV !== 'production') {
    expressApp.set('json spaces', 4);
  }

  // Authorization middleware
  require('./core/express/authorization').Factory({ bottle, expressApp });

  // Routes
  expressApp.options('*', cors()); // include before other routes

  await requireFolder(`${__dirname}/routes`, { bottle, expressApp });

  // Automatic error response middleware, MUST BE INCLUDED AFTER ALL OTHER ROUTES/MIDDLEWARES
  // TO CATCH ERRORS EMITTED IN THEM. [wh]
  // Automatic error response middleware
  expressApp.use(
    errorHandler({
      full: process.env.NODE_ENV !== 'production',
      onError: (err: unknown) => {
        logger.error('Express stack error ', err);
      },
    }),
  );
  // Listen
  const port = Config.httpPort;
  await new Promise((resolve, reject) => {
    try {
      expressApp.listen(port, (err: Error) => {
        if (err) {
          return reject(err);
        }

        logger.info(`Listening on port ${port}`);
      });
    } catch (ex) {
      reject(ex);
    }
  });
})().catch(err => {
  logger.error(`Fatal error during startup`, err);
  process.exit(1);
});
