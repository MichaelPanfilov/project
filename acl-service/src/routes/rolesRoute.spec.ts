import * as Bottle from 'bottlejs';
import * as express from 'express';
import { Express } from 'express';
import getPort = require('get-port');
import { Server } from 'http';
import * as request from 'supertest';

import { SpecHelper } from '../helpers/specHelper';
import { AclRepoMock } from '../repos/aclRepo.mock';
import { RoleRepoMock } from '../repos/roleRepo.mock';
import { UserRepoMock } from '../repos/userRepo.mock';
import { AuthServiceMock } from '../services/authService.mock';

describe('ROUTE: Roles', () => {
  let app: Express;
  let bottle: Bottle;
  let server: Server;
  let errorMw;

  beforeEach(async () => {
    // DI bottle
    bottle = new Bottle('testBottle');
    app = express();

    SpecHelper.init(bottle, app);
    UserRepoMock.factory(bottle);
    RoleRepoMock.factory(bottle);
    AuthServiceMock.factory(bottle);
    AclRepoMock.factory(bottle);

    // Include route under test
    require('./rolesRoute').Factory({ bottle, expressApp: app });

    errorMw = SpecHelper.errorHandler(app);

    // Start express
    return new Promise(async (resolve, reject) => {
      try {
        server = app.listen(await getPort(), (err: Error) => {
          if (err) {
            return reject(err);
          }
          resolve();
        });
      } catch (ex) {
        reject(ex);
      }
    });
  });

  afterEach(async () => {
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');

    // Stop server
    return new Promise((resolve, reject) => {
      server.close((err: Error) => {
        if (err) {
          return reject(err);
        }

        resolve();
      });
    });
  });

  describe('GET /v1/roles', () => {
    it('should return roles on valid call', async () => {
      await request(app)
        .get('/v1/roles')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.arrayContaining([
                jasmine.objectContaining({
                  roleId: jasmine.any(String),
                  name: jasmine.any(String),
                  description: jasmine.any(String),
                }),
              ]),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .get('/v1/roles')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
  });
  describe('POST /v1/roles', () => {
    it('should return role on valid call', async () => {
      await request(app)
        .post('/v1/roles')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .send({
          name: 'newname',
          description: 'newdescription',
        })
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.objectContaining({
                roleId: jasmine.any(String),
                name: jasmine.any(String),
                description: jasmine.any(String),
              }),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .post('/v1/roles')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
    it('should return HTTP status 409 on call with name that already exists', async () => {
      await request(app)
        .post('/v1/roles')
        .send({
          name: 'alreadyexist',
          description: 'newdescription',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(409)
        .then();
    });

    it('should return HTTP status 400 if field name doesnt send', async () => {
      await request(app)
        .post('/v1/roles')
        .send({
          description: 'description',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });
    it('should return HTTP status 400 if fields doesnt send', async () => {
      await request(app)
        .post('/v1/roles')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });
    it('should return HTTP status 400 if field name too short', async () => {
      await request(app)
        .post('/v1/roles')
        .send({
          name: '',
          description: 'description',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });
  });
  describe('DELETE /v1/roles/:roleId', () => {
    it('should return success on valid call', async () => {
      await request(app)
        .delete('/v1/roles/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: 'success',
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 404 on non existing role', async () => {
      await request(app)
        .delete('/v1/roles/5')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .delete('/v1/roles/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
  });

  describe('POST /roles/:roleId/allow/:resourceId/:rightKey', () => {
    it('should return success on valid call', async () => {
      await request(app)
        .post('/v1/roles/1/allow/1/alwaysAllowed')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .send({
          name: 'newname',
          description: 'newdescription',
        })
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: 'success',
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .post('/v1/roles/1/allow/1/alwaysAllowed')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
    it('should return HTTP status 404 on non existing role', async () => {
      await request(app)
        .post('/v1/roles/5/allow/1/alwaysAllowed')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
  });
  describe('DELETE /roles/:roleId/remove_allow/:resourceId/:rightKey', () => {
    it('should return success on valid call', async () => {
      await request(app)
        .delete('/v1/roles/1/remove_allow/1/alwaysAllowed')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .send({
          name: 'newname',
          description: 'newdescription',
        })
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: 'success',
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .delete('/v1/roles/1/remove_allow/1/alwaysAllowed')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
    it('should return HTTP status 404 on non existing role', async () => {
      await request(app)
        .delete('/v1/roles/5/remove_allow/1/alwaysAllowed')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
    it('should return HTTP status 404 on non existing resourceId', async () => {
      await request(app)
        .delete('/v1/roles/1/remove_allow/notExistingResId/alwaysAllowed')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
    it('should return HTTP status 404 on non existing rightKey', async () => {
      await request(app)
        .delete('/v1/roles/1/remove_allow/1/notExistingRightKey')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
  });
});
