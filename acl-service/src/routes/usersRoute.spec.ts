import * as Bottle from 'bottlejs';
import * as express from 'express';
import { Express } from 'express';
import getPort = require('get-port');
import { Server } from 'http';
import * as request from 'supertest';

import { SpecHelper } from '../helpers/specHelper';
import { RoleRepoMock } from '../repos/roleRepo.mock';
import { UserRepoMock } from '../repos/userRepo.mock';
import { AuthServiceMock } from '../services/authService.mock';

describe('ROUTE: Users', () => {
  let app: Express;
  let bottle: Bottle;
  let server: Server;
  let errorMw;

  beforeEach(async () => {
    // DI bottle
    bottle = new Bottle('testBottle');
    app = express();

    SpecHelper.init(bottle, app);
    UserRepoMock.factory(bottle);
    RoleRepoMock.factory(bottle);
    AuthServiceMock.factory(bottle);

    // Include route under test
    require('./usersRoute').Factory({ bottle, expressApp: app });

    errorMw = SpecHelper.errorHandler(app);

    // Start express
    return new Promise(async (resolve, reject) => {
      try {
        server = app.listen(await getPort(), (err: Error) => {
          if (err) {
            return reject(err);
          }
          resolve();
        });
      } catch (ex) {
        reject(ex);
      }
    });
  });

  afterEach(async () => {
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');

    // Stop server
    return new Promise((resolve, reject) => {
      server.close((err: Error) => {
        if (err) {
          return reject(err);
        }

        resolve();
      });
    });
  });

  describe('GET /v1/users/:userId', () => {
    it('should return HTTP status 200 on valid call', async () => {
      await request(app)
        .get('/v1/users/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(200)
        .then();
    });
    it('should return User object', async () => {
      await request(app)
        .get('/v1/users/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.objectContaining({
                name: jasmine.any(String),
                email: jasmine.any(String),
                // userId: jasmine.any(Number),
                // name: jasmine.any(String),
                // description: jasmine.any(String),
              }),
              meta: jasmine.any(Object),
            }),
          );
        })

        .then();
    });

    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .get('/v1/users/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });

    it('should return HTTP status 404 on non existing user', async () => {
      await request(app)
        .get('/v1/users/5')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
  });

  describe('PUT /v1/users/:userId/roles/:roleId', () => {
    it('should return role on valid call', async () => {
      await request(app)
        .put('/v1/users/1/roles/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.objectContaining({
                roleId: jasmine.any(String),
                name: jasmine.any(String),
                description: jasmine.any(String),
              }),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .put('/v1/users/1/roles/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
    it('should return HTTP status 404 on non existing user', async () => {
      await request(app)
        .get('/v1/users/5/roles/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
    it('should return HTTP status 404 on non existing role', async () => {
      await request(app)
        .get('/v1/users/1/roles/5')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
  });

  describe('GET /v1/users', () => {
    it('should return users on valid call', async () => {
      await request(app)
        .get('/v1/users')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.arrayContaining([
                jasmine.objectContaining({
                  id: jasmine.any(String),
                  name: jasmine.any(String),
                  email: jasmine.any(String),
                  activated: jasmine.any(Boolean),
                  roleIds: [null],
                }),
              ]),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .get('/v1/users')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
  });
  describe('POST /v1/users', () => {
    it('should return user on valid call', async () => {
      await request(app)
        .post('/v1/users')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .send({
          username: 'newname',
          password: 'newpassword',
        })
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.objectContaining({
                name: jasmine.any(String),
                email: jasmine.any(String),
                // userId: jasmine.any(Number),
                // name: jasmine.any(String),
                // description: jasmine.any(String),
              }),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .post('/v1/users')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
    it('should return HTTP status 409 on call with username that already exists', async () => {
      await request(app)
        .post('/v1/users')
        .send({
          username: 'alreadyexist',
          password: 'password',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(409)
        .then();
    });
    it('should return HTTP status 400 if field password doesnt send', async () => {
      await request(app)
        .post('/v1/users')
        .send({
          username: 'foo1',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });
    it('should return HTTP status 400 if field username doesnt send', async () => {
      await request(app)
        .post('/v1/users')
        .send({
          password: 'password',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });
    it('should return HTTP status 400 if field password too short', async () => {
      await request(app)
        .post('/v1/users')
        .send({
          username: 'alreadyexist',
          password: '',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });
    it('should return HTTP status 400 if field username too short', async () => {
      await request(app)
        .post('/v1/users')
        .send({
          username: '',
          password: 'password',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });
  });
  describe('DELETE /v1/users/:userId', () => {
    it('should return HTTP status 400 if user trying to delete itself', async () => {
      await request(app)
        .delete('/v1/users/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });
    it('should return success on valid call', async () => {
      await request(app)
        .delete('/v1/users/2')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: 'success',
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 404 on non existing user', async () => {
      await request(app)
        .delete('/v1/users/5')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .delete('/v1/users/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
  });
  describe('DELETE /v1/users/:userId/roles/:roleId', () => {
    it('should return Role on valid call', async () => {
      await request(app)
        .delete('/v1/users/1/roles/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: 'success',
              // data: jasmine.objectContaining({
              //   roleId: jasmine.any(Number),
              //   name: jasmine.any(String),
              //   description: jasmine.any(String),
              // }),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 404 on non existing user', async () => {
      await request(app)
        .delete('/v1/users/5/roles/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
    it('should return HTTP status 404 on non existing role', async () => {
      await request(app)
        .delete('/v1/users/1/roles/5')
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .delete('/v1/users/1/roles/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
  });

  describe('PUT /:userId/change_password', () => {
    it('valid request should return HTTP status 200', async () => {
      spyOn(bottle.container.UserRepo, 'changePassword');
      spyOn(bottle.container.authService, 'requireAllowed');
      const userId = 'acl_user-asdfasdf-asdf-asdf';
      const newPassword = 'test';

      await request(app)
        .put(`/v1/users/${userId}/change_password`)
        .send({ newPassword })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(200)
        .expect(() => {
          expect(bottle.container.authService.requireAllowed).toHaveBeenCalledTimes(1);
          expect(bottle.container.authService.requireAllowed).toHaveBeenCalledWith(
            jasmine.any(Object),
            'User',
            'Update',
          );
          expect(bottle.container.UserRepo.changePassword).toHaveBeenCalledTimes(1);
          expect(bottle.container.UserRepo.changePassword).toHaveBeenCalledWith(
            userId,
            newPassword,
          );
        })
        .then();
    });

    it('invalid request should return HTTP status 401', async () => {
      await request(app)
        .put('/v1/users/asdf/change_password')
        .send({
          newPassword: 'test',
        })
        .set('Accept', 'application/json')
        .expect(401)
        .then();
    });
  });
});
