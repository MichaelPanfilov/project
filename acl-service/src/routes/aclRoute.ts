import { PASSWORD_MAX_LENGTH, PASSWORD_MIN_LENGTH } from '@common';
import { Logger, LogService } from '@elunic/logger';
import * as Bottle from 'bottlejs';
import { makeRouteInvoker } from 'bottlejs-express';
import * as commonErrors from 'common-errors';
import { Request, Response, Router } from 'express';
import * as httpErrors from 'http-errors';
import * as Joi from 'joi';

import { requireLogin } from '../core/express/requireLogin';
import { requireValidToken } from '../core/express/requireValidToken';
import { handleError } from '../helpers/handleError';
import { User } from '../models/user';
import { AclRepo } from '../repos/aclRepo';
import { RoleRepo } from '../repos/roleRepo';
import { UserRepo } from '../repos/userRepo';
import { AuthService } from '../services/authService';
import { AuthenticatedRequest } from '../types';

export function Factory({ bottle, expressApp }: { bottle: Bottle; expressApp: Router }) {
  const routeInvoker = makeRouteInvoker(
    bottle,
    AclRoute,
    'log',
    'UserRepo',
    'AclRepo',
    'RoleRepo',
    'authService',
  );

  expressApp.post('/v1/login', routeInvoker('login'));
  expressApp.put('/v1/logout_all', requireLogin, routeInvoker('logoutAll'));
  expressApp.get('/v1/renew', requireValidToken, routeInvoker('renew'));
  expressApp.put('/v1/me/change_password', requireLogin, routeInvoker('changePassword'));
  expressApp.get('/v1/me', requireLogin, routeInvoker('me'));
  expressApp.get('/v1/me/rights', requireLogin, routeInvoker('allRights'));
  expressApp.get('/v1/me/rights/:resourceId', requireLogin, routeInvoker('rightsByResource'));
  expressApp.get('/v1/me/roles', requireLogin, routeInvoker('allRoles'));
  expressApp.get(
    '/v1/me/is_allowed/:resourceId/:rightKey',
    requireLogin,
    routeInvoker('isAllowed'),
  );
}

class AclRoute {
  private logger: Logger;

  constructor(
    private log: LogService,
    private userRepo: UserRepo,
    private aclRepo: AclRepo,
    private roleRepo: RoleRepo,
    private authService: AuthService,
  ) {
    this.logger = this.log.createLogger('LoginRoute');
  }

  async changePassword(req: AuthenticatedRequest, res: Response) {
    const userId = req.auth.userId;
    const { oldPassword, newPassword } = Joi.attempt(
      req.body,
      Joi.object().keys({
        oldPassword: Joi.string()
          .min(PASSWORD_MIN_LENGTH)
          .max(PASSWORD_MAX_LENGTH)
          .required(),
        newPassword: Joi.string()
          .min(PASSWORD_MIN_LENGTH)
          .max(PASSWORD_MAX_LENGTH)
          .required(),
      }),
      'Missing oldPassword or newPassword',
    );

    try {
      const user = await this.userRepo.getUserById(userId);

      try {
        this.authService.verifyPassword(user, oldPassword);
      } catch (e) {
        throw new commonErrors.ValidationError('Invalid old password');
      }

      await this.userRepo.changePassword(userId, newPassword);

      res.json({
        data: user,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async logoutAll(req: AuthenticatedRequest, res: Response) {
    await this.authService.logout(req.auth.userId);

    res.json({
      data: 'success',
      meta: {},
    });
  }

  async renew(req: AuthenticatedRequest, res: Response) {
    const authorization = req.headers.authorization;

    if (!authorization) {
      throw new httpErrors.Unauthorized('Invalid Authorization');
    }

    const token = await this.authService.getToken(authorization);

    try {
      const newToken = await this.authService.renew(token);

      res.json({
        data: newToken,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async login(req: Request, res: Response) {
    const { username, password } = Joi.attempt(
      req.body,
      Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required(),
      }),
      'Missing name or password',
    );

    let user!: User;
    try {
      user = await this.userRepo.getLoginByName(username);
      this.authService.verifyPassword(user, password);
    } catch {
      throw new commonErrors.AuthenticationRequiredError('Invalid username / password');
    }

    try {
      const token = await this.authService.generateToken(user);

      res.json({
        data: token,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async me(req: AuthenticatedRequest, res: Response) {
    try {
      const user = await this.userRepo.getLoginByName(req.auth.username);

      res.json({
        data: user.privateData,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async allRights(req: AuthenticatedRequest, res: Response) {
    const userId = req.auth.userId;

    try {
      const result = await this.aclRepo.getRights(userId);

      res.json({
        data: result,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async rightsByResource(req: AuthenticatedRequest, res: Response) {
    const { resourceId } = Joi.attempt(
      req.params,
      Joi.object().keys({
        resourceId: Joi.string().required(),
      }),
      'Missing resourceId',
    );

    const userId = req.auth.userId;

    try {
      const result = await this.aclRepo.getRightsByResource(userId, resourceId);

      res.json({
        data: result,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async allRoles(req: AuthenticatedRequest, res: Response) {
    const userId = req.auth.userId;

    try {
      const result = await this.roleRepo.getAllByUserId(userId);

      res.json({
        data: result,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async isAllowed(req: AuthenticatedRequest, res: Response) {
    const { resourceId, rightKey } = Joi.attempt(
      req.params,
      Joi.object().keys({
        resourceId: Joi.string().required(),
        rightKey: Joi.string().required(),
      }),
      'Missing resourceId or rightKey',
    );

    const userId = req.auth.userId;

    const result = await this.aclRepo.isAllowed(userId, resourceId, rightKey);

    res.json({
      data: result,
      meta: {},
    });
  }
}
