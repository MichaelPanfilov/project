import { LicenseContentTypes, LicenseTypes } from '@common';
import * as Bottle from 'bottlejs';
import * as Express from 'express';
import { Request, Response } from 'express';

import { Config } from '../core/config';

export function Factory({ bottle, expressApp }: { bottle: Bottle; expressApp: Express.Router }) {
  expressApp.use('/v1/licenses', (req: Request, res: Response) => {
    const type: LicenseTypes = req.query.type in LicenseContentTypes ? req.query.type : 'csv';
    const filename = `acl-service-licenses.${type}`;
    const headers = {
      ['Content-Disposition']: `inline; filename="${filename}"`,
      ['Content-Type']: LicenseContentTypes[type],
    };

    res.sendFile(filename, {
      root: Config.dataPath,
      headers,
    });
  });
}
