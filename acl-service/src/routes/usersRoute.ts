import {
  EMAIL_REGEX,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
  USERNAME_MAX_LENGTH,
  USERNAME_MIN_LENGTH,
} from '@common';
import { Logger, LogService } from '@elunic/logger';
import * as Bottle from 'bottlejs';
import { makeRouteInvoker } from 'bottlejs-express';
import { Response, Router } from 'express';
import * as httpErrors from 'http-errors';
import * as Joi from 'joi';

import { requireAllowed } from '../core/express/requireAllowed';
import { requireLogin } from '../core/express/requireLogin';
import { handleError } from '../helpers/handleError';
import { RoleRepo } from '../repos/roleRepo';
import { UserRepo } from '../repos/userRepo';
import { AuthService } from '../services/authService';
import { AuthenticatedRequest } from '../types';

export function Factory({ bottle, expressApp }: { bottle: Bottle; expressApp: Router }) {
  const routeInvoker = makeRouteInvoker(
    bottle,
    UsersRoute,
    'log',
    'UserRepo',
    'RoleRepo',
    'authService',
  );

  expressApp.get(
    '/v1/users',
    [requireLogin, requireAllowed(bottle, 'User', 'Read')],
    routeInvoker('getAllUsers'),
  );

  expressApp.post(
    '/v1/users',
    [requireLogin, requireAllowed(bottle, 'User', 'Create')],
    routeInvoker('createUser'),
  );

  expressApp.put(
    '/v1/users/:userId',
    [requireLogin, requireAllowed(bottle, 'User', 'Update', { idKey: 'userId' })],
    routeInvoker('updateUser'),
  );

  expressApp.delete(
    '/v1/users/:userId',
    [requireLogin, requireAllowed(bottle, 'User', 'Delete')],
    routeInvoker('deleteUser'),
  );

  expressApp.put(
    '/v1/users/:userId/roles',
    [requireLogin, requireAllowed(bottle, 'User', 'Update')],
    routeInvoker('updateRoles'),
  );

  expressApp.get(
    '/v1/users/:userId/roles',
    [requireLogin, requireAllowed(bottle, 'User', 'Read', { idKey: 'userId' })],
    routeInvoker('getUserRoles'),
  );

  expressApp.put(
    '/v1/users/:userId/roles/:roleId',
    [requireLogin, requireAllowed(bottle, 'User', 'Update')],
    routeInvoker('addRole'),
  );

  expressApp.delete(
    '/v1/users/:userId/roles/:roleId',
    [requireLogin, requireAllowed(bottle, 'User', 'Update')],
    routeInvoker('deleteRole'),
  );

  expressApp.get(
    '/v1/users/:userId',
    [requireLogin, requireAllowed(bottle, 'User', 'Read', { idKey: 'userId' })],
    routeInvoker('getUser'),
  );

  expressApp.post(
    '/v1/users/:email/request_password_recovery',
    requireLogin,
    routeInvoker('requestPasswordRecovery'),
  );

  expressApp.put(
    '/v1/users/:userId/change_password',
    [requireLogin, requireAllowed(bottle, 'User', 'Update', { idKey: 'userId' })],
    routeInvoker('changeUserPassword'),
  );

  expressApp.put(
    '/v1/users/:userId/logout_all',
    [requireLogin, requireAllowed(bottle, 'User', 'Update', { idKey: 'userId' })],
    routeInvoker('logoutAll'),
  );
}

class UsersRoute {
  private logger: Logger;

  constructor(
    private log: LogService,
    private userRepo: UserRepo,
    private roleRepo: RoleRepo,
    private authService: AuthService,
  ) {
    this.logger = this.log.createLogger('LoginRoute');
  }

  async getAllUsers(req: AuthenticatedRequest, res: Response) {
    const users = await this.userRepo.getAll();

    res.json({
      data: users.map(user => user.privateData),
      meta: {},
    });
  }

  async createUser(req: AuthenticatedRequest, res: Response) {
    const userBody = Joi.attempt(
      req.body,
      Joi.object().keys({
        username: Joi.string()
          .min(USERNAME_MIN_LENGTH)
          .max(USERNAME_MAX_LENGTH)
          .required(),
        password: Joi.string()
          .min(PASSWORD_MIN_LENGTH)
          .max(PASSWORD_MAX_LENGTH)
          .required(),
        email: Joi.string()
          .allow('')
          .regex(EMAIL_REGEX),
      }),
      'Missing username or password or invalid email',
    );

    try {
      const user = await this.userRepo.create(userBody);

      res.json({
        data: user.privateData,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async updateUser(req: AuthenticatedRequest, res: Response) {
    const userId = Joi.attempt(
      req.params.userId,
      Joi.string().required(),
      'UserId must be a string',
    );

    const userBody = Joi.attempt(
      req.body,
      Joi.object().keys({
        name: Joi.string()
          .min(USERNAME_MIN_LENGTH)
          .max(USERNAME_MAX_LENGTH),
        email: Joi.string()
          .allow('')
          .regex(EMAIL_REGEX),
      }),
    ) as { name?: string; email?: string };

    if (!userBody.name && !userBody.email) {
      throw new httpErrors.BadRequest('Request body is missing');
    }

    try {
      const user = await this.userRepo.update(userId, userBody);

      res.json({
        data: user.privateData,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async deleteUser(req: AuthenticatedRequest, res: Response) {
    const userId = Joi.attempt(
      req.params.userId,
      Joi.string().required(),
      'userId must be a string',
    );

    if (req.auth.userId === userId) {
      throw new httpErrors.BadRequest('You cannot delete yourself');
    }

    try {
      await this.userRepo.delete(userId);

      res.json({
        data: 'success',
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async updateRoles(req: AuthenticatedRequest, res: Response) {
    const userId = Joi.attempt(
      req.params.userId,
      Joi.string().required(),
      'UserId must be a string',
    );

    const { roleIds } = Joi.attempt(
      req.body,
      Joi.object().keys({
        roleIds: Joi.array()
          .items(Joi.string())
          .required(),
      }),
      'Missing roleIds',
    );

    if (req.auth.userId === userId) {
      throw new httpErrors.BadRequest('Cannot update self roles');
    }

    try {
      const role = await this.roleRepo.updateRoles(userId, roleIds);

      res.json({
        data: role,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async getUserRoles(req: AuthenticatedRequest, res: Response) {
    const userId = Joi.attempt(
      req.params.userId,
      Joi.string().required(),
      'UserId must be a string',
    );

    try {
      const roles = await this.roleRepo.getAllByUserId(userId);

      res.json({
        data: roles,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async addRole(req: AuthenticatedRequest, res: Response) {
    const { userId, roleId } = Joi.attempt(
      req.params,
      Joi.object().keys({
        userId: Joi.string().required(),
        roleId: Joi.string().required(),
      }),
      'Missing userId or roleId',
    );

    try {
      const role = await this.roleRepo.addRole(userId, roleId);

      res.json({
        data: role,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async deleteRole(req: AuthenticatedRequest, res: Response) {
    const { userId, roleId } = Joi.attempt(
      req.params,
      Joi.object().keys({
        userId: Joi.string().required(),
        roleId: Joi.string().required(),
      }),
      'Missing userId or roleId',
    );

    try {
      await this.roleRepo.removeRole(userId, roleId);

      res.json({
        data: 'success',
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async getUser(req: AuthenticatedRequest, res: Response) {
    const userId = Joi.attempt(
      req.params.userId,
      Joi.string().required(),
      'userId must be a string',
    );

    try {
      const user = await this.userRepo.get(userId);

      res.json({
        data: user.privateData,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  requestPasswordRecovery(req: AuthenticatedRequest, res: Response) {
    res.json({
      data: 'success',
      meta: {},
    });
  }

  async changeUserPassword(req: AuthenticatedRequest, res: Response) {
    const userId = Joi.attempt(
      req.params.userId,
      Joi.string().required(),
      'UserId must be a string',
    );

    const { newPassword } = Joi.attempt(
      req.body,
      Joi.object().keys({
        newPassword: Joi.string()
          .min(PASSWORD_MIN_LENGTH)
          .max(PASSWORD_MAX_LENGTH)
          .required(),
      }),
      'Missing newPassword',
    );

    try {
      await this.userRepo.changePassword(userId, newPassword);

      res.send({
        data: 'success',
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async logoutAll(req: AuthenticatedRequest, res: Response) {
    const userId = Joi.attempt(
      req.params.userId,
      Joi.string().required(),
      'userId must be a string',
    );

    try {
      await this.userRepo.get(userId);
      await this.authService.logout(userId);

      res.json({
        data: 'success',
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }
}
