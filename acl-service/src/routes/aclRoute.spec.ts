import * as Bottle from 'bottlejs';
import * as express from 'express';
import { Express } from 'express';
import getPort = require('get-port');
import { Server } from 'http';
import * as request from 'supertest';

import { SpecHelper } from '../helpers/specHelper';
import { AclRepoMock } from '../repos/aclRepo.mock';
import { RoleRepoMock } from '../repos/roleRepo.mock';
import { UserRepoMock } from '../repos/userRepo.mock';
import { AuthServiceMock } from '../services/authService.mock';

const app = express();

describe('ROUTE: ACL', () => {
  let bottle: Bottle;
  let server: Server;
  let errorMw;

  beforeEach(async () => {
    // DI bottle
    bottle = new Bottle('testBottle');

    SpecHelper.init(bottle, app);

    UserRepoMock.factory(bottle);
    AuthServiceMock.factory(bottle);
    AclRepoMock.factory(bottle);
    RoleRepoMock.factory(bottle);

    // Include route under test
    require('./aclRoute').Factory({ bottle, expressApp: app });

    errorMw = SpecHelper.errorHandler(app);

    // Start express
    return new Promise(async (resolve, reject) => {
      try {
        server = app.listen(await getPort(), (err: Error) => {
          if (err) {
            return reject(err);
          }
          resolve();
        });
      } catch (ex) {
        reject(ex);
      }
    });
  });

  afterEach(async () => {
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');

    // Stop server
    return new Promise((resolve, reject) => {
      server.close((err: Error) => {
        if (err) {
          return reject(err);
        }

        resolve();
      });
    });
  });

  describe('POST /login', () => {
    it('valid login returns HTTP status 200', async () => {
      await request(app)
        .post('/v1/login')
        .send({
          username: 'foo',
          password: 'fooBar',
        })
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .then();
    });

    it('invalid login should throw 401', async () => {
      await request(app)
        .post('/v1/login')
        .send({
          username: 'invalidLogin',
          password: 'bar2',
        })
        .set('Accept', 'application/json')
        .expect(401)
        .then();
    });

    it('login should return JWT', async () => {
      await request(app)
        .post('/v1/login')
        .send({
          username: 'foo',
          password: 'fooBar',
        })
        .set('Accept', 'application/json')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.any(String),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
  });

  describe('PUT /me/change_password', () => {
    it('valid request should return HTTP status 200', async () => {
      await request(app)
        .put('/v1/me/change_password')
        .send({
          oldPassword: 'fooBar',
          newPassword: 'test',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(200)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .then();
    });

    it('invalid request should return HTTP status 401', async () => {
      await request(app)
        .put('/v1/me/change_password')
        .send({
          oldPassword: 'foo',
          newPassword: 'test',
        })
        .set('Accept', 'application/json')
        .expect(401)
        .then();
    });

    it('should return a user', async () => {
      await request(app)
        .put('/v1/me/change_password')
        .send({
          oldPassword: 'fooBar',
          newPassword: 'test',
        })
        .set('Accept', 'application/json')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.objectContaining({
                id: jasmine.any(String),
                name: jasmine.any(String),
                email: jasmine.any(String),
              }),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
  });

  describe('PUT /me/is_allowed/:resourceId/:rightKey', () => {
    it('valid request should return HTTP status 200', async () => {
      await request(app)
        .get('/v1/me/is_allowed/1/alwaysAllowed')
        .send()
        .set('Authorization', 'valid')
        .expect(200)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .then();
    });

    it('invalid request should return HTTP status 401', async () => {
      await request(app)
        .get('/v1/me/is_allowed/1/alwaysAllowed')
        .send()
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });

    it('should return true on allowed', async () => {
      await request(app)
        .get('/v1/me/is_allowed/1/alwaysAllowed')
        .send()
        .set('Authorization', 'valid')
        .expect(res => {
          SpecHelper.expectApiResponse(res, true);
        })
        .then();
    });

    it('should return true on allowed', async () => {
      await request(app)
        .get('/v1/me/is_allowed/1/notAllowed')
        .send()
        .set('Authorization', 'valid')
        .expect(res => {
          SpecHelper.expectApiResponse(res, false);
        })
        .then();
    });
  });
  describe('GET /me', () => {
    it('should return user object on valid call', async () => {
      await request(app)
        .get('/v1/me')
        .set('Authorization', 'Bearer valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.objectContaining({
                name: jasmine.any(String),
                email: jasmine.any(String),
                activated: jasmine.any(Boolean),
                // userId: jasmine.any(Number),
                // name: jasmine.any(String),
                // description: jasmine.any(String),
              }),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .get('/v1/me')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
  });
  describe('GET /me/rights', () => {
    it('should return rights object', async () => {
      await request(app)
        .get('/v1/me/rights')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.any(Object),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .get('/v1/me/rights')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
  });
  describe('GET /me/rights/:resourceId', () => {
    it('should return rights object', async () => {
      await request(app)
        .get('/v1/me/rights/1')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.any(Object),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .get('/v1/me/rights/1')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
    it('should return HTTP status 404 on call with invalid resource id', async () => {
      await request(app)
        .get('/v1/me/rights/5')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
  });
  describe('GET /v1/me/roles', () => {
    it('should return roles array on valid call', async () => {
      await request(app)
        .get('/v1/me/roles')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.arrayContaining([
                jasmine.objectContaining({
                  roleId: jasmine.any(String),
                  name: jasmine.any(String),
                  description: jasmine.any(String),
                }),
              ]),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });
    it('should return HTTP status 401 on call with invalid authentication', async () => {
      await request(app)
        .get('/v1/me/roles')
        .set('Accept', 'application/json')
        .set('Authorization', 'invalid')
        .expect(401)
        .then();
    });
  });
});
