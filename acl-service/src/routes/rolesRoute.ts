import { Logger, LogService } from '@elunic/logger';
import * as Bottle from 'bottlejs';
import { makeRouteInvoker } from 'bottlejs-express';
import { Response, Router } from 'express';
import * as httpErrors from 'http-errors';
import * as Joi from 'joi';

import { requireAllowed } from '../core/express/requireAllowed';
import { requireLogin } from '../core/express/requireLogin';
import { handleError } from '../helpers/handleError';
import { AclRepo } from '../repos/aclRepo';
import { RoleRepo } from '../repos/roleRepo';
import { AuthenticatedRequest } from '../types';

export function Factory({ bottle, expressApp }: { bottle: Bottle; expressApp: Router }) {
  const routeInvoker = makeRouteInvoker(
    bottle,
    RolesRoute,
    'log',
    'RoleRepo',
    'AclRepo',
    'authService',
  );

  expressApp.get(
    '/v1/roles',
    [requireLogin, requireAllowed(bottle, 'Role', 'Read')],
    routeInvoker('getRoles'),
  );

  expressApp.post(
    '/v1/joined_roles',
    [requireLogin, requireAllowed(bottle, 'Role', 'Read')],
    routeInvoker('getJoinedRoles'),
  );

  expressApp.post(
    '/v1/roles',
    [requireLogin, requireAllowed(bottle, 'Role', 'Create')],
    routeInvoker('createRole'),
  );

  expressApp.put(
    '/v1/roles/:roleId',
    [requireLogin, requireAllowed(bottle, 'Role', 'Update')],
    routeInvoker('updateRole'),
  );

  expressApp.delete(
    '/v1/roles/:roleId',
    [requireLogin, requireAllowed(bottle, 'Role', 'Delete')],
    routeInvoker('deleteRole'),
  );

  expressApp.post(
    '/v1/roles/:roleId/allow/:resourceId/:rightKey',
    [requireLogin, requireAllowed(bottle, 'Role', 'Update')],
    routeInvoker('allow'),
  );
  expressApp.delete(
    '/v1/roles/:roleId/remove_allow/:resourceId/:rightKey',
    [requireLogin, requireAllowed(bottle, 'Role', 'Update')],
    routeInvoker('removeAllow'),
  );
}

class RolesRoute {
  private logger: Logger;

  constructor(private log: LogService, private roleRepo: RoleRepo, private aclRepo: AclRepo) {
    this.logger = this.log.createLogger('LoginRoute');
  }

  async getRoles(req: AuthenticatedRequest, res: Response) {
    const roles = await this.roleRepo.getAll();

    res.json({
      data: roles,
      meta: {},
    });
  }

  async getJoinedRoles(req: AuthenticatedRequest, res: Response) {
    const { userIds } = Joi.attempt(
      req.body,
      Joi.object().keys({
        userIds: [Joi.array().required(), Joi.array().items(Joi.string())],
      }),
      'Missing userIds',
    );

    const roles = userIds.length ? await this.roleRepo.getAllByUserIds(userIds) : [];

    res.json({
      data: roles,
      meta: {},
    });
  }

  async createRole(req: AuthenticatedRequest, res: Response) {
    const roleBody = Joi.attempt(
      req.body,
      Joi.object().keys({
        name: Joi.string().required(),
        description: [Joi.string(), Joi.empty()],
      }),
      'Missing name',
    );

    try {
      const role = await this.roleRepo.create(roleBody);

      res.json({
        data: role,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async updateRole(req: AuthenticatedRequest, res: Response) {
    const roleId = Joi.attempt(
      req.params.roleId,
      Joi.string().required(),
      'RoleId must be a string',
    );

    const roleBody = Joi.attempt(
      req.body,
      Joi.object().keys({
        name: Joi.string(),
        description: Joi.string(),
      }),
      'Name and description must be a string',
    );

    if (!roleBody.name && !roleBody.description) {
      throw new httpErrors.BadRequest('Request body is missing');
    }

    try {
      const role = await this.roleRepo.update(roleId, roleBody);

      res.json({
        data: role.publicData,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async deleteRole(req: AuthenticatedRequest, res: Response) {
    const roleId = Joi.attempt(
      req.params.roleId,
      Joi.string().required(),
      'RoleId must be a string',
    );

    try {
      await this.roleRepo.delete(roleId);

      res.json({
        data: 'success',
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async allow(req: AuthenticatedRequest, res: Response) {
    const roleId = Joi.attempt(
      req.params.roleId,
      Joi.string().required(),
      'roleId must be a string',
    );
    const resourceId = Joi.attempt(
      req.params.resourceId,
      Joi.string().required(),
      'resourceId must be a string',
    );
    const rightKey = Joi.attempt(
      req.params.rightKey,
      Joi.string().required(),
      'rightKey must be a string',
    );

    try {
      await this.roleRepo.get(roleId);
      await this.aclRepo.allow(roleId, resourceId, rightKey);

      res.json({
        data: 'success',
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async removeAllow(req: AuthenticatedRequest, res: Response) {
    const roleId = Joi.attempt(
      req.params.roleId,
      Joi.string().required(),
      'roleId must be a string',
    );

    const resourceId = Joi.attempt(
      req.params.resourceId,
      Joi.string().required(),
      'resourceId must be a string',
    );

    const rightKey = Joi.attempt(
      req.params.rightKey,
      Joi.string().required(),
      'rightKey must be a string',
    );

    try {
      await this.roleRepo.get(roleId);
      await this.aclRepo.removeAllow(roleId, resourceId, rightKey);

      res.json({
        data: 'success',
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }
}
