import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import sinon = require('sinon');

import { userMock } from '../models/user.mock';

import { serviceName } from './userRepo';

export const mockServiceName = serviceName + 'Mock';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(mockServiceName, UserRepoMock);
}

export class UserRepoMock {
  static factory(bottle: Bottle) {
    bottle.factory(serviceName, () => {
      const stub = new UserRepoMock();

      return {
        getAll: stub.getAll,
        get: stub.get,
        changePassword: stub.changePassword,
        getLoginByName: stub.getLoginByName,
        create: stub.create,
        delete: stub.delete,
        getUserById: stub.getUserById,
      };
    });
  }

  async getAll(...args: unknown[]) {
    return sinon.stub().resolves([userMock])(...args);
  }

  async get(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('user'));
    stub.withArgs('1').resolves(userMock);
    return stub(...args);
  }

  async getUserById(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('user'));
    stub.withArgs('1').resolves(userMock);
    return stub(...args);
  }

  async create(...args: unknown[]) {
    const stub = sinon.stub();
    stub.resolves(userMock);
    stub
      .withArgs({ username: 'alreadyexist', password: 'password' })
      .throws(new commonErrors.AlreadyInUseError('user'));
    return stub(...args);
  }

  async delete(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('user'));
    stub.withArgs('1').resolves(userMock);
    stub.withArgs('2').resolves(userMock);
    return stub(...args);
  }

  async changePassword(...args: unknown[]) {
    return sinon.stub().resolves(userMock)(...args);
  }

  async getLoginByName(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('user'));
    stub.withArgs('foo').resolves(userMock);
    return stub(...args);
  }
}
