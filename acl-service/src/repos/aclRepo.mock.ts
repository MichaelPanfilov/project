import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import sinon = require('sinon');

import { userMock } from '../models/user.mock';

import { serviceName } from './aclRepo';

export const mockServiceName = serviceName + 'Mock';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(mockServiceName, AclRepoMock);
}

export class AclRepoMock {
  static factory(bottle: Bottle) {
    bottle.factory(serviceName, () => {
      const stub = new AclRepoMock();

      return {
        allow: stub.allow,
        isAllowed: stub.isAllowed,
        removeAllow: stub.removeAllow,
        getRights: stub.getRights,
        getRightsByResource: stub.getRightsByResource,
      };
    });
  }

  async allow(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('acl'));
    stub.withArgs('1', '1', 'alwaysAllowed').resolves(userMock);
    return stub(...args);
  }

  async removeAllow(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('acl'));
    stub.withArgs('1', '1', 'alwaysAllowed').resolves(userMock);
    return stub(...args);
  }

  async isAllowed(...args: unknown[]) {
    const stub = sinon.stub();
    stub.resolves(false);
    stub.withArgs(sinon.match.any, sinon.match.any, 'alwaysAllowed').resolves(true);
    return stub(...args);
  }

  async getRights(...args: unknown[]) {
    const stub = sinon.stub();
    stub.resolves({ '1': { alwaysAllowed: true } });
    return stub(...args);
  }

  async getRightsByResource(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('acl'));
    stub.withArgs('1').resolves({ alwaysAllowed: true });
    return stub(...args);
  }
}
