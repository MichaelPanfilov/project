import { getRefId, ReferencedEntities } from '@common';
import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import ow from 'ow';

import { generateId } from '../helpers/generateId';
import { Role } from '../models/role';
import { User } from '../models/user';
import { UsersRoles } from '../models/users_roles';

export const serviceName = 'RoleRepo';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(serviceName, RoleRepo);
}

export class RoleRepo {
  async getAll(): Promise<Role[]> {
    return Role.query();
  }

  async get(roleId: string) {
    ow(roleId, 'roleId', ow.string);

    const role = await Role.query()
      .where('id', '=', roleId)
      .first();

    if (!role) {
      throw new commonErrors.NotFoundError('role');
    }

    return role;
  }

  async getAllByUserIds(userIds: string[]) {
    ow(userIds, 'userIds', ow.array.ofType(ow.string));

    const users = await User.query()
      .findByIds(userIds)
      .eager('roles');

    if (!users.length) {
      throw new commonErrors.NotFoundError('users');
    }

    return users.map(({ id, roles }) => ({ id, roles }));
  }

  async getAllByUserId(userId: string) {
    ow(userId, 'userId', ow.string);

    const user = await User.query()
      .eager('roles')
      .where('users.id', '=', userId)
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    return user.roles;
  }

  async updateRoles(userId: string, roleIds: string[]) {
    ow(userId, 'userId', ow.string);
    ow(roleIds, 'roleIds', ow.array);

    const user = await User.query()
      .where('id', userId)
      .first();
    const roles = await Role.query().whereIn('id', roleIds);

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    await User.query().upsertGraph(
      {
        id: userId,
        roles,
      },
      {
        relate: true,
        unrelate: true,
      },
    );

    return Role.query().whereIn('id', roleIds);
  }

  async addRole(userId: string, roleId: string): Promise<Role> {
    ow(userId, 'userId', ow.string);
    ow(roleId, 'roleId', ow.string);

    const user = await User.query()
      .where('id', userId)
      .first();
    const role = await Role.query()
      .where('id', roleId)
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    if (!role) {
      throw new commonErrors.NotFoundError('role');
    }

    await User.query().upsertGraph(
      {
        id: userId,
        roles: [{ id: roleId }],
      },
      { relate: ['roles'], noDelete: true },
    );

    return Role.query()
      .where('id', roleId)
      .first()
      .throwIfNotFound();
  }

  async removeRole(userId: string, roleId: string) {
    ow(userId, 'userId', ow.string);
    ow(roleId, 'roleId', ow.string);

    const user = await User.query()
      .where('id', userId)
      .first();
    const role = await Role.query()
      .where('id', roleId)
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    if (!role) {
      throw new commonErrors.NotFoundError('role');
    }

    await UsersRoles.query()
      .delete()
      .where('user_id', userId)
      .where('role_id', roleId);
  }

  async create(roleBody: { name: string; description: string }): Promise<Role> {
    try {
      const id = getRefId(generateId(), ReferencedEntities.ACL_ROLE);
      return await Role.query().insert({ id, ...roleBody });
    } catch (e) {
      throw e.code === 'ER_DUP_ENTRY' || e.code === 'SQLITE_CONSTRAINT'
        ? new commonErrors.AlreadyInUseError('role')
        : e;
    }
  }

  async update(roleId: string, roleBody: { name?: string; description?: string }): Promise<Role> {
    ow(roleId, 'roleId', ow.string);

    const role = await Role.query()
      .where('id', roleId)
      .first();

    if (!role) {
      throw new commonErrors.NotFoundError('role');
    }

    try {
      return await Role.query().upsertGraph({
        id: roleId,
        ...roleBody,
      });
    } catch (e) {
      throw e.code === 'ER_DUP_ENTRY' ? new commonErrors.AlreadyInUseError('role') : e;
    }
  }

  async delete(roleId: string): Promise<void> {
    const role = await Role.query()
      .where('id', roleId)
      .first();

    if (!role) {
      throw new commonErrors.NotFoundError('role');
    }

    await role.$query().delete();
  }
}
