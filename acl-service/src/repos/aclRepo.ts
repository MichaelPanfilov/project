import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import ow from 'ow';

import { Config } from '../core/config';
import { Acl } from '../models/acl';
import { Role } from '../models/role';
import { User } from '../models/user';

export const serviceName = 'AclRepo';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(serviceName, AclRepo);
}

export class AclRepo {
  async allow(roleId: string, resourceId: string, rightKey: string) {
    ow(roleId, 'roleId', ow.string);

    const acl = await Acl.query()
      .where('role_id', roleId)
      .where('resource_id', resourceId)
      .where('right_key', rightKey)
      .first();

    if (acl) {
      throw new commonErrors.AlreadyInUseError('acl');
    }

    const role = await Role.query()
      .where('id', roleId)
      .first();

    if (!role) {
      throw new commonErrors.NotFoundError('role');
    }

    return Acl.query().insert({
      roleId,
      resourceId,
      rightKey,
    });
  }

  async removeAllow(roleId: string, resourceId: string, rightKey: string): Promise<void> {
    ow(roleId, 'roleId', ow.string);

    const removeCount = await Acl.query()
      .delete()
      .where('role_id', roleId)
      .where('resource_id', resourceId)
      .where('right_key', rightKey);

    if (!removeCount) {
      throw new commonErrors.NotFoundError('acl');
    }
  }

  async isAllowed(userId: string, resourceId: string, rightKey: string): Promise<boolean> {
    if (Config.allowAll) {
      return true;
    }
    ow(userId, 'userId', ow.string);

    const user = await User.query()
      .eager('roles')
      .where('users.id', userId)
      .first();

    if (!user) {
      return false;
    }

    const roleIds = user.roles.map(({ id }) => id);

    const aclResult = await Acl.query()
      .whereIn('role_id', roleIds)
      .where('resource_id', resourceId)
      .where('right_key', rightKey);

    return aclResult.length > 0;
  }

  async getRights(userId: string) {
    ow(userId, 'userId', ow.string);

    const user = await User.query()
      .eager('roles')
      .where('users.id', userId)
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    const roleIds = user.roles.map(({ id }) => id);

    return (await Acl.query().whereIn('role_id', roleIds)).reduce(
      (acc: { [key: string]: { [key: string]: boolean } }, item: Acl) => {
        if (!acc[item.resourceId]) {
          acc[item.resourceId] = {};
        }

        Object.assign(acc[item.resourceId], { [item.rightKey]: true });

        return acc;
      },
      {},
    );
  }

  async getRightsList(userId: string) {
    const rights = await this.getRights(userId);
    return Object.entries(rights).reduce(
      (prev, [resourceId, rightObj]) => {
        return prev.concat(Object.keys(rightObj).map(right => ({ resourceId, right })));
      },
      [] as { resourceId: string; right: string }[],
    );
  }

  async getRightsByResource(userId: string, resourceId: string) {
    ow(userId, 'userId', ow.string);

    const user = await User.query()
      .eager('roles')
      .where('users.id', userId)
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    const roleIds = user.roles.map(({ id }) => id);

    const rights = await Acl.query()
      .whereIn('role_id', roleIds)
      .where('resource_id', resourceId);

    if (rights.length === 0) {
      return rights;
    } else {
      return rights.reduce((acc: { [key: string]: boolean }, item: Acl) => {
        acc[item.rightKey] = true;

        return acc;
      }, {});
    }
  }
}
