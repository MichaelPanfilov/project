import {
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
  USERNAME_MAX_LENGTH,
  USERNAME_MIN_LENGTH,
} from '@common';
import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';

import { createDatabase, dropDatabase } from '../../test/helpers/connection';
import { knex } from '../core/knex';
import { Role } from '../models/role';
import { User } from '../models/user';

import { UserRepo } from './userRepo';

describe('Repo: USER', () => {
  let bottle: Bottle;
  let repo: UserRepo;

  beforeAll(async () => {
    await createDatabase();
  });

  afterAll(async () => {
    await dropDatabase();
  });

  beforeEach(async () => {
    bottle = new Bottle('testBottle');

    await knex.migrate.rollback();
    await knex.migrate.latest();

    await knex(User.tableName).insert([
      {
        id: '1',
        name: 'testuser',
        password: 'asdf',
        email: 'asdf',
        activated: 0,
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);
    await knex(Role.tableName).insert([
      {
        id: '1',
        name: 'testrole',
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);
    await knex('users_roles').insert([
      {
        user_id: '1',
        role_id: '1',
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);

    require('./userRepo').Factory({ bottle });
    repo = bottle.container.UserRepo;
  });

  afterEach(async () => {
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');
  });

  describe('get', () => {
    it('should return user object', async () => {
      const user = await repo.get('1');
      expect(user as unknown).toEqual(
        jasmine.objectContaining({
          id: '1',
          name: 'testuser',
          password: 'asdf',
          email: 'asdf',
          activated: 0,
          createdAt: jasmine.any(String),
          updatedAt: jasmine.any(String),
        }),
      );
    });
    it('should throw an Error if userId does not exist', async () => {
      try {
        await repo.get('51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
  });

  describe('getAll', () => {
    it('should return user array', async () => {
      const users = await repo.getAll();
      expect(users as unknown).toEqual(
        jasmine.arrayContaining([
          jasmine.objectContaining({
            id: jasmine.any(String),
            name: jasmine.any(String),
            password: jasmine.any(String),
            email: jasmine.any(String),
            activated: jasmine.any(Number),
            createdAt: jasmine.any(String),
            updatedAt: jasmine.any(String),
          }),
        ]),
      );
    });
  });

  describe('changePassword', () => {
    it('should return user with a new password', async () => {
      const user = await repo.changePassword('1', 'newpass');
      expect(user as unknown).toEqual(
        jasmine.objectContaining({
          id: jasmine.any(String),
          name: jasmine.any(String),
          password: jasmine.any(String),
          email: jasmine.any(String),
          activated: jasmine.any(Number),
          createdAt: jasmine.any(String),
          updatedAt: jasmine.any(String),
        }),
      );
    });
    it('should throw error if password is too short', async () => {
      try {
        await repo.changePassword('1', '');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
    it('should throw error if userId does not exist', async () => {
      try {
        await repo.changePassword('51', 'newpass');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
  });

  describe('getLoginByName', () => {
    it('should return a user', async () => {
      const user = await repo.getLoginByName('testuser');
      expect(user as unknown).toEqual(
        jasmine.objectContaining({
          id: jasmine.any(String),
          name: jasmine.any(String),
          password: jasmine.any(String),
          email: jasmine.any(String),
          activated: jasmine.any(Number),
          createdAt: jasmine.any(String),
          updatedAt: jasmine.any(String),
        }),
      );
    });
    it('should throw error if login is too short', async () => {
      try {
        await repo.getLoginByName('');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
    it('should throw error if login does not exist', async () => {
      try {
        await repo.getLoginByName('testuser51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
  });

  describe('create', () => {
    it('should return a new user', async () => {
      const user = await repo.create({
        username: '09_-.validName',
        password: 'newpassword',
        email: 'test@test.com',
      });
      expect(user as unknown).toEqual(
        jasmine.objectContaining({
          id: jasmine.any(String),
          name: '09_-.validName',
          password: jasmine.any(String),
          activated: jasmine.any(Number),
          createdAt: jasmine.any(String),
          updatedAt: jasmine.any(String),
          email: 'test@test.com',
        }),
      );
    });
    it('should throw error if user with current name exists', async () => {
      try {
        await repo.create({
          username: 'testuser',
          password: 'newpassword',
          email: 'test@test.com',
        });
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.AlreadyInUseError('user'));
      }
    });
    describe('username validation', () => {
      it('should throw error if username incorrect', async () => {
        try {
          await repo.create({
            username: ' !"№;%:?* ',
            password: 'newpassword',
            email: 'test@test.com',
          });
          throw new Error('Should not be thrown');
        } catch (error) {
          expect(error.message).toContain('custom validation');
          expect(error.name).toBe('ArgumentError');
        }
      });
      it('should throw error if username too long', async () => {
        try {
          await repo.create({
            username: 'tooo_looong-username',
            password: 'newpassword',
            email: 'test@test.com',
          });
          throw new Error('Should not be thrown');
        } catch (error) {
          expect(error.message).toContain(`maximum length of \`${USERNAME_MAX_LENGTH}\``);
          expect(error.name).toBe('ArgumentError');
        }
      });
      it('should throw error if username too short', async () => {
        try {
          await repo.create({
            username: '1',
            password: 'newpassword',
            email: 'test@test.com',
          });
          throw new Error('Should not be thrown');
        } catch (error) {
          expect(error.message).toContain(`minimum length of \`${USERNAME_MIN_LENGTH}\``);
          expect(error.name).toBe('ArgumentError');
        }
      });
    });
    describe('password validation', () => {
      it('should throw error if username too long', async () => {
        try {
          await repo.create({
            username: 'username',
            password: 'tooo_looong-password',
            email: 'test@test.com',
          });
          throw new Error('Should not be thrown');
        } catch (error) {
          expect(error.message).toContain(`maximum length of \`${PASSWORD_MAX_LENGTH}\``);
          expect(error.name).toBe('ArgumentError');
        }
      });
      it('should throw error if password too short', async () => {
        try {
          await repo.create({
            username: 'username',
            password: '1',
            email: 'test@test.com',
          });
          throw new Error('Should not be thrown');
        } catch (error) {
          expect(error.message).toContain(`minimum length of \`${PASSWORD_MIN_LENGTH}\``);
          expect(error.name).toBe('ArgumentError');
        }
      });
    });
    describe('email validation', () => {
      it('should throw error if email incorrect', async () => {
        try {
          await repo.create({
            username: 'username',
            password: 'password',
            email: 'testtest.com',
          });
          throw new Error('Should not be thrown');
        } catch (error) {
          expect(error.message).toContain('custom validation');
          expect(error.name).toBe('ArgumentError');
        }
      });
    });
  });

  describe('delete', () => {
    // todo should return true?
    it('should not throw an error if user removed successfully', async () => {
      await repo.delete('1');
    });
    it('should throw an error if user already exists.', async () => {
      try {
        await repo.delete('51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('user'));
      }
    });
  });
});
