import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import sinon = require('sinon');

import { roleMock } from '../models/role.mock';

import { serviceName } from './roleRepo';

export const mockServiceName = serviceName + 'Mock';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(mockServiceName, RoleRepoMock);
}

export class RoleRepoMock {
  static factory(bottle: Bottle) {
    bottle.factory(serviceName, () => {
      const stub = new RoleRepoMock();

      return {
        get: stub.get,
        getAll: stub.getAll,
        addRole: stub.addRole,
        removeRole: stub.removeRole,
        create: stub.create,
        delete: stub.delete,
        getAllByUserId: stub.getAllByUserId,
      };
    });
  }

  async get(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('role'));
    stub.withArgs('1').resolves(roleMock);
    return stub(...args);
  }

  async getAll(...args: unknown[]) {
    return sinon.stub().resolves([roleMock])(...args);
  }

  async addRole(...args: unknown[]) {
    const stub = sinon.stub();
    stub.resolves(undefined);
    stub.withArgs('1', '1').resolves(roleMock);
    return stub(...args);
  }
  async removeRole(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('role'));
    stub.withArgs('1', '1').resolves(roleMock);
    return stub(...args);
  }

  async create(...args: unknown[]) {
    const stub = sinon.stub();
    stub.resolves(roleMock);
    stub
      .withArgs({ name: 'alreadyexist', description: 'newdescription' })
      .throws(new commonErrors.AlreadyInUseError('role'));
    return stub(...args);
  }

  async delete(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('role'));
    stub.withArgs('1').resolves(roleMock);
    return stub(...args);
  }

  async getAllByUserId(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('user'));
    stub.withArgs('1').resolves([roleMock]);
    return stub(...args);
  }
}
