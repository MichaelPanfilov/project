import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';

import { createDatabase, dropDatabase } from '../../test/helpers/connection';
import { knex } from '../core/knex';
import { Acl } from '../models/acl';
import { Role } from '../models/role';
import { User } from '../models/user';

import { AclRepo } from './aclRepo';

describe('Repo: ACL', () => {
  let bottle: Bottle;
  let repo: AclRepo;

  beforeAll(async () => {
    await createDatabase();
  });

  afterAll(async () => {
    await dropDatabase();
  });

  beforeEach(async () => {
    bottle = new Bottle('testBottle');

    await knex.migrate.rollback();
    await knex.migrate.latest();

    await knex(Acl.tableName).insert([
      {
        role_id: '1',
        resource_id: 'resource-1',
        right_key: 'alwaysAllowed',
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);
    await knex(User.tableName).insert([
      {
        id: '1',
        name: 'testuser',
        password: 'asdf',
        email: 'asdf',
        activated: 0,
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);
    await knex(Role.tableName).insert([
      {
        id: '1',
        name: 'testrole',
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);
    await knex('users_roles').insert([
      {
        user_id: '1',
        role_id: '1',
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);
    require('./aclRepo').Factory({ bottle });
    repo = bottle.container.AclRepo;
  });

  afterEach(async () => {
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');
  });

  describe('allow', () => {
    it('grant acces should return acl object', async () => {
      const acl = await repo.allow('1', 'resource-42', 'read');
      expect(acl as unknown).toEqual(
        jasmine.objectContaining({
          id: jasmine.any(Number),
          roleId: '1',
          resourceId: 'resource-42',
          rightKey: 'read',
          createdAt: jasmine.any(String),
          updatedAt: jasmine.any(String),
        }),
      );
    });

    // it('grant acces twice should throw an Error', async () => {
    //   expect(await repo.allow(1, 'resource-1', 'alwaysAllowed'))
    //     .toThrow(new Error("access already granted."))
    //   // @TODO custom error Type?
    // });

    // expect(..).toThrow does not work with async functions
    it('grant acces twice should throw an Error', async () => {
      try {
        await repo.allow('1', 'resource-1', 'alwaysAllowed');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.AlreadyInUseError('acl'));
      }
    });
    it('grant acces to not existing role should throw error', async () => {
      try {
        await repo.allow('51', 'resource-42', 'read');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
  });

  describe('removeAllow', () => {
    // todo why 1?
    it('removeAllow on existing acl entry should not throw an error', async () => {
      await repo.removeAllow('1', 'resource-1', 'alwaysAllowed');
    });
    it('should throw an error if roleId does not exist', async () => {
      try {
        await repo.removeAllow('51', 'resource-1', 'alwaysAllowed');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
    it('should throw an error if resource does not exist', async () => {
      try {
        await repo.removeAllow('1', 'resource-51', 'alwaysAllowed');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
    it('should throw an error if right does not exist', async () => {
      try {
        await repo.removeAllow('1', 'resource-1', 'alwaysAllowed51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
  });

  describe('getRights', () => {
    it('should return resource right', async () => {
      const acl = await repo.getRights('1');
      expect(acl as unknown).toEqual(
        jasmine.objectContaining({
          'resource-1': jasmine.objectContaining({
            alwaysAllowed: jasmine.any(Boolean),
          }),
        }),
      );
    });
    it('should throw error if userId does not exist', async () => {
      try {
        await repo.getRights('51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('user'));
      }
    });
  });

  describe('getRightsByResource', () => {
    it('should return resource right', async () => {
      const acl = await repo.getRightsByResource('1', 'resource-1');
      expect(acl as unknown).toEqual(
        jasmine.objectContaining({
          alwaysAllowed: jasmine.any(Boolean),
        }),
      );
    });
    it('should throw error if userId does not exist', async () => {
      try {
        await repo.getRightsByResource('51', 'resource-1');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('user'));
      }
    });
  });

  describe('isAllowed', () => {
    it('should return true (allowed)', async () => {
      const allowed = await repo.isAllowed('1', 'resource-1', 'alwaysAllowed');
      expect(allowed).toBe(true);
    });

    it('should return false (not allowed)', async () => {
      const allowed = await repo.isAllowed('1', 'resource-42', 'alwaysAllowed');
      expect(allowed).toBe(false);
    });

    it('should return false 2 (not allowed)', async () => {
      const allowed = await repo.isAllowed('42', 'resource-1', 'alwaysAllowed');
      expect(allowed).toBe(false);
    });
  });
});
