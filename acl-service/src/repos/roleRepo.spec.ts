import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';

import { createDatabase, dropDatabase } from '../../test/helpers/connection';
import { knex } from '../core/knex';
import { Role } from '../models/role';
import { User } from '../models/user';

import { RoleRepo } from './roleRepo';

describe('Repo: ROLE', () => {
  let bottle: Bottle;
  let repo: RoleRepo;

  beforeAll(async () => {
    await createDatabase();
  });

  afterAll(async () => {
    await dropDatabase();
  });

  beforeEach(async () => {
    bottle = new Bottle('testBottle');

    await knex.migrate.rollback();
    await knex.migrate.latest();

    await knex(User.tableName).insert([
      {
        id: '1',
        name: 'testuser',
        password: 'asdf',
        email: 'asdf',
        activated: 0,
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);
    await knex(Role.tableName).insert([
      {
        id: '1',
        name: 'testrole',
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);
    await knex('users_roles').insert([
      {
        user_id: '1',
        role_id: '1',
        created_at: '2019-02-24 23:37:53',
        updated_at: '2019-02-24 23:37:53',
      },
    ]);

    require('./roleRepo').Factory({ bottle });
    repo = bottle.container.RoleRepo;
  });

  afterEach(async () => {
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');
  });

  describe('get', () => {
    it('should return role object', async () => {
      const role = await repo.get('1');
      expect(role as unknown).toEqual(
        jasmine.objectContaining({
          id: '1',
          name: 'testrole',
          createdAt: jasmine.any(String),
          updatedAt: jasmine.any(String),
        }),
      );
    });
    it('should throw an Error if roleId not exists', async () => {
      try {
        await repo.get('51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('Should not be thrown');
      }
    });
  });
  describe('getAll', () => {
    it('should return role array', async () => {
      const roles = await repo.getAll();
      expect(roles as unknown).toEqual(
        jasmine.arrayContaining([
          jasmine.objectContaining({
            id: jasmine.any(String),
            name: jasmine.any(String),
            createdAt: jasmine.any(String),
            updatedAt: jasmine.any(String),
          }),
        ]),
      );
    });
  });

  describe('getAllByUserId', () => {
    it('should return role array', async () => {
      const roles = await repo.getAllByUserId('1');
      expect(roles as unknown).toEqual(
        jasmine.arrayContaining([
          jasmine.objectContaining({
            id: jasmine.any(String),
            name: jasmine.any(String),
            description: null,
            createdAt: jasmine.any(String),
            updatedAt: jasmine.any(String),
          }),
        ]),
      );
    });
    it('should throw error if userId is not exist', async () => {
      try {
        await repo.getAllByUserId('51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('user'));
      }
    });
  });

  describe('addRole', () => {
    it('should not throw an error if user and role exists', async () => {
      await repo.addRole('1', '1');
    });
    it('should throw an error if user exists and role does not exists', async () => {
      try {
        await repo.addRole('1', '51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('role'));
      }
    });
    it('should throw error if user does not exist and role exists', async () => {
      try {
        await repo.addRole('51', '1');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('user'));
      }
    });
    it('should throw error if user and role does not exist', async () => {
      try {
        await repo.addRole('51', '51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).not.toEqual(new Error('Should not be thrown'));
      }
    });
  });

  describe('removeRole', () => {
    it('should not throw error if user and role exists', async () => {
      await repo.removeRole('1', '1');
    });
    it('should throw error if user exists and role does not exists', async () => {
      try {
        await repo.removeRole('1', '51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('role'));
      }
    });
    it('should throw error if user does not exist and role exists', async () => {
      try {
        await repo.removeRole('51', '1');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('user'));
      }
    });
    it('should throw error if user and role does not exist', async () => {
      try {
        await repo.removeRole('51', '51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).not.toEqual(new Error('Should not be thrown'));
      }
    });
  });

  describe('create', () => {
    it('should return a role', async () => {
      const role = await repo.create({ name: ' !";%:?* ', description: 'description2' });
      expect(role as unknown).toEqual(
        jasmine.objectContaining({
          id: jasmine.any(String),
          name: ' !";%:?* ',
          description: 'description2',
          createdAt: jasmine.any(String),
          updatedAt: jasmine.any(String),
        }),
      );
    });
    it('should throw error if role with current name exists', async () => {
      try {
        await repo.create({ name: 'testrole', description: 'description' });
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).toEqual(new commonErrors.AlreadyInUseError('role'));
      }
    });
  });

  describe('delete', () => {
    it('should not throw an error if current role exists', async () => {
      await repo.delete('1');
    });
    it('should throw an error if current role does not exist', async () => {
      try {
        await repo.delete('51');
        throw new Error('Should not be thrown');
      } catch (error) {
        expect(error).not.toEqual(new Error('Should not be thrown'));
      }
    });
  });
});
