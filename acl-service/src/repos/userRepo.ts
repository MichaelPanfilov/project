import {
  CreateAclUserDto,
  getRefId,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
  ReferencedEntities,
  USERNAME_MAX_LENGTH,
  USERNAME_MIN_LENGTH,
  validateEmail,
  validatePassword,
  validateUsername,
} from '@common';
import * as bcrypt from 'bcryptjs';
import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import ow from 'ow';

import { generateId } from '../helpers/generateId';
import { User } from '../models/user';
import { UsersRoles } from '../models/users_roles';

export const serviceName = 'UserRepo';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(serviceName, UserRepo);
}

export class UserRepo {
  getAll(): Promise<User[]> {
    return User.query().eager('roles');
  }

  async get(userId: string) {
    ow(userId, 'userId', ow.string);

    const user = await User.query()
      .where('id', userId)
      .eager('roles')
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    return user;
  }

  async changePassword(userId: string, password: string): Promise<User> {
    ow(userId, 'userId', ow.string);
    ow(
      password,
      'password',
      ow.string
        .minLength(PASSWORD_MIN_LENGTH)
        .maxLength(PASSWORD_MAX_LENGTH)
        .is(validatePassword),
    );

    const salt = bcrypt.genSaltSync(10);
    const passwordHash = bcrypt.hashSync(password, salt);

    const user = await User.query()
      .eager('roles')
      .patchAndFetchById(userId, {
        password: passwordHash,
      });

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    return user;
  }

  async getLoginByName(name: string): Promise<User> {
    const user = await User.query()
      .where('name', '=', name)
      .eager('roles')
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    return user;
  }

  async getUserById(id: string): Promise<User> {
    ow(id, ow.string);

    const user = await User.query()
      .where('id', '=', id)
      .eager('roles')
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    return user;
  }

  async create({ username: name, password, email }: CreateAclUserDto): Promise<User> {
    ow(
      name,
      'username',
      ow.string
        .minLength(USERNAME_MIN_LENGTH)
        .maxLength(USERNAME_MAX_LENGTH)
        .is(validateUsername),
    );
    ow(
      password,
      'password',
      ow.string
        .minLength(PASSWORD_MIN_LENGTH)
        .maxLength(PASSWORD_MAX_LENGTH)
        .is(validatePassword),
    );
    ow(email, 'email', ow.any(ow.optional.string.empty, ow.string.is(validateEmail)));

    const salt = bcrypt.genSaltSync(10);
    const passwordHash = bcrypt.hashSync(password, salt);

    const id = getRefId(generateId(), ReferencedEntities.ACL_USER);
    try {
      return await User.query()
        .eager('roles')
        .insertAndFetch({
          id,
          name,
          email: email || null,
          password: passwordHash,
          activated: true,
        });
    } catch (e) {
      throw this.mapUpsertError(e);
    }
  }

  async update(userId: string, userBody: { name?: string; email?: string }) {
    const { email, name } = userBody;

    ow(userId, 'userId', ow.string);
    ow(
      name,
      'username',
      ow.any(
        ow.optional.string.empty,
        ow.string
          .minLength(USERNAME_MIN_LENGTH)
          .maxLength(USERNAME_MAX_LENGTH)
          .is(validateUsername),
      ),
    );
    ow(email, 'email', ow.any(ow.optional.string.empty, ow.string.is(validateEmail)));

    const user = await User.query()
      .where('id', userId)
      .first();

    if (!user) {
      throw new commonErrors.NotFoundError('user');
    }

    try {
      return await User.query()
        .eager('roles')
        .upsertGraphAndFetch({
          id: userId,
          name,
          email: email || null,
        });
    } catch (e) {
      throw this.mapUpsertError(e);
    }
  }

  async delete(userId: string): Promise<void> {
    ow(userId, 'userId', ow.string);

    const removedCount = await User.query()
      .delete()
      .where('id', userId);

    if (!removedCount) {
      throw new commonErrors.NotFoundError('user');
    }

    await UsersRoles.query()
      .delete()
      .where('user_id', userId);
  }

  private mapUpsertError(e: { code?: string; sqlMessage?: string }): Error {
    if (
      (e.code === 'ER_DUP_ENTRY' || e.code === 'SQLITE_CONSTRAINT') &&
      typeof e.sqlMessage === 'string'
    ) {
      return new commonErrors.AlreadyInUseError(
        e.sqlMessage.includes('users_email_unique') ? 'email' : 'user',
      );
    }
    return e as Error;
  }
}
