# acl-service

## db schema & migrations
- `npm run migration:run` - create or update the db schema

## Seeding

Some initial data might be required to run the application:

- `npm run seed:roles` - create the required roles and rights for the application
- `npm run seed:initial-users` - create default users syntegonAdmin, shopUser, and shopAdmin, with the password "syntegon"

