DELETE FROM `knex_migrations` where `name` IN (
'20200406122359_drop_db.js',
'20181007100333_dataseries.js',
'20190224230347_create_users_table.js',
'20190301012159_create_roles_table.js',
'20190301012216_create_acl_table.js',
'20190301012225_create_users_roles_table.js',
'20190311125427_modify_user_name_column.js',
'20190311131515_modify_roles_name_column.js',
'20190311154802_create_logoutAll_table.js',
'20190503190709_change_users_primary_key.js',
'20190503192255_change_roles_primary_key.js',
'20190503193100_modify_acl_roleID_column.js',
'20190503193159_modify_logoutAll_userId_column.js',
'20190506112458_modify_users_roles_id_keys.js',
'20200225163224_modify_users_language_column.js',
'20200511195913_drop_dataseries.js'
);

INSERT INTO `knex_migrations` (`name`, `batch`, `migration_time`) VALUES
('20190224230347_create_users_table.js', 1, '2020-07-16 15:52:22'),
('20190301012159_create_roles_table.js', 1, '2020-07-16 15:52:23'),
('20190301012216_create_acl_table.js', 1, '2020-07-16 15:52:23'),
('20190301012225_create_users_roles_table.js', 1, '2020-07-16 15:52:23'),
('20190311154802_create_logoutAll_table.js', 1, '2020-07-16 15:52:23');
