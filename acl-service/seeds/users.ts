
import * as bcrypt from 'bcryptjs';
import * as Knex from 'knex';

exports.seed = (knex: Knex) => seed(knex);

async function seed(knex: Knex) {
  const synAdminRole = await knex('roles').where({name: 'Syntegon Admin'}).first();
  const shopAdminRole = await knex('roles').where({name: 'Shopfloor Admin'}).first();
  const shopUserRole = await knex('roles').where({name: 'Shopfloor User'}).first();

  const salt = bcrypt.genSaltSync(10);
  const passwordHash = bcrypt.hashSync('syntegon', salt);
  // const synAdminId = generateId(ReferencedEntities.ACL_USER);
  // The acl ids must be hard coded, because they must be known in the seedings for the actor of the backend-service seedings
  const synAdminId = 'acl_user-6c654cc5-5402-4268-adba-0820cf61cd48';
  const synAdmin = {
    id: synAdminId,
    name: 'SyntegonAdmin',
    password: passwordHash,
    activated: 1,
  };
  const shopAdminId = 'acl_user-8736c921-ffcd-4ee1-be60-58a56890f4e5';
  const shopAdmin = {
    id: shopAdminId,
    name: 'ShopfloorAdmin',
    password: passwordHash,
    activated: 1,
  };
  const shopUserId = 'acl_user-cb9a4df1-9e27-4062-8e96-d425d09721d3';
  const shopUser = {
    id: shopUserId,
    name: 'ShopfloorUser',
    password: passwordHash,
    activated: 1,
  };
  await knex('users').insert([synAdmin, shopAdmin, shopUser]);

  await knex('users_roles').insert([
    { user_id: synAdmin.id, role_id: synAdminRole.id },
    { user_id: shopAdmin.id, role_id: shopAdminRole.id },
    { user_id: shopUser.id, role_id: shopUserRole.id },
  ]);
}
