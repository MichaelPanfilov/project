import { AclResource, AclRight, generateId, ReferencedEntities } from '@common';
import * as Knex from 'knex';

exports.seed = (knex: Knex) => seed(knex);

async function seed(knex: Knex) {
  // Deletes ALL existing entries
  await knex('roles').del();
  await knex('users').del();
  await knex('acl').del();
  await knex('users_roles').del();

  // Insert Entities
  const synAdminRole = {
    id: generateId(ReferencedEntities.ACL_ROLE),
    name: 'Syntegon Admin',
    description: 'Syntegon admin role',
  };
  const shopAdminRole = {
    id: generateId(ReferencedEntities.ACL_ROLE),
    name: 'Shopfloor Admin',
    description: 'Shopfloor admin role',
  };
  const shopUserRole = {
    id: generateId(ReferencedEntities.ACL_ROLE),
    name: 'Shopfloor User',
    description: 'Shopfloor user role',
  };
  await knex('roles').insert([synAdminRole, shopAdminRole, shopUserRole]);

  const acls: {
    role_id: string;
    resource_id: AclResource | AclResource[];
    right_key: AclRight;
  }[] = [
    // -- Syntegon Admin Role --
    // Task
    { role_id: synAdminRole.id, resource_id: 'Task', right_key: 'Read' },
    { role_id: synAdminRole.id, resource_id: 'Task', right_key: 'Create' },
    { role_id: synAdminRole.id, resource_id: 'Task', right_key: 'Update' },
    { role_id: synAdminRole.id, resource_id: 'Task', right_key: 'Delete' },
    { role_id: synAdminRole.id, resource_id: 'Task', right_key: 'Execute' },

    // { role_id: synAdminRole.id, resource_id: ['Task', 'Operator'], right_key: 'Read' },
    // { role_id: synAdminRole.id, resource_id: ['Task', 'Operator'], right_key: 'Create' },
    // { role_id: synAdminRole.id, resource_id: ['Task', 'Operator'], right_key: 'Update' },
    // { role_id: synAdminRole.id, resource_id: ['Task', 'Operator'], right_key: 'Delete' },
    // { role_id: synAdminRole.id, resource_id: ['Task', 'Operator'], right_key: 'Execute' },

    // Document
    { role_id: synAdminRole.id, resource_id: 'Document', right_key: 'Read' },
    { role_id: synAdminRole.id, resource_id: 'Document', right_key: 'Create' },
    { role_id: synAdminRole.id, resource_id: 'Document', right_key: 'Update' },
    { role_id: synAdminRole.id, resource_id: 'Document', right_key: 'Delete' },

    // Work Instruction
    { role_id: synAdminRole.id, resource_id: 'Instruction', right_key: 'Read' },
    { role_id: synAdminRole.id, resource_id: 'Instruction', right_key: 'Create' },
    { role_id: synAdminRole.id, resource_id: 'Instruction', right_key: 'Update' },
    { role_id: synAdminRole.id, resource_id: 'Instruction', right_key: 'Delete' },

    // Asset
    { role_id: synAdminRole.id, resource_id: 'Asset', right_key: 'Read' },
    { role_id: synAdminRole.id, resource_id: 'Asset', right_key: 'Create' },
    { role_id: synAdminRole.id, resource_id: 'Asset', right_key: 'Update' },
    { role_id: synAdminRole.id, resource_id: 'Asset', right_key: 'Delete' },

    // User
    { role_id: synAdminRole.id, resource_id: 'User', right_key: 'Read' },
    { role_id: synAdminRole.id, resource_id: 'User', right_key: 'Create' },
    { role_id: synAdminRole.id, resource_id: 'User', right_key: 'Update' },
    { role_id: synAdminRole.id, resource_id: 'User', right_key: 'Delete' },

    // Role
    { role_id: synAdminRole.id, resource_id: 'Role', right_key: 'Read' },
    { role_id: synAdminRole.id, resource_id: 'Role', right_key: 'Create' },
    { role_id: synAdminRole.id, resource_id: 'Role', right_key: 'Update' },
    { role_id: synAdminRole.id, resource_id: 'Role', right_key: 'Delete' },

    // -- Shopfloor Admin Role --
    // Task
    { role_id: shopAdminRole.id, resource_id: 'Task', right_key: 'Read' },
    { role_id: shopAdminRole.id, resource_id: 'Task', right_key: 'Create' },
    { role_id: shopAdminRole.id, resource_id: 'Task', right_key: 'Update' },
    { role_id: shopAdminRole.id, resource_id: 'Task', right_key: 'Delete' },
    { role_id: shopAdminRole.id, resource_id: 'Task', right_key: 'Execute' },

    // Document
    { role_id: shopAdminRole.id, resource_id: 'Document', right_key: 'Read' },
    { role_id: shopAdminRole.id, resource_id: 'Document', right_key: 'Create' },
    { role_id: shopAdminRole.id, resource_id: 'Document', right_key: 'Update' },
    { role_id: shopAdminRole.id, resource_id: 'Document', right_key: 'Delete' },

    // Work Instruction
    { role_id: shopAdminRole.id, resource_id: 'Instruction', right_key: 'Read' },
    { role_id: shopAdminRole.id, resource_id: 'Instruction', right_key: 'Create' },
    { role_id: shopAdminRole.id, resource_id: 'Instruction', right_key: 'Update' },
    { role_id: shopAdminRole.id, resource_id: 'Instruction', right_key: 'Delete' },

    // Asset
    { role_id: shopAdminRole.id, resource_id: 'Asset', right_key: 'Read' },

    // User
    { role_id: shopAdminRole.id, resource_id: 'User', right_key: 'Read' },

    // Role
    { role_id: shopAdminRole.id, resource_id: 'Role', right_key: 'Read' },

    // -- Shopfloor User Role --
    // Task
    { role_id: shopUserRole.id, resource_id: 'Task', right_key: 'Read' },
    { role_id: shopUserRole.id, resource_id: 'Task', right_key: 'Execute' },

    // Document
    { role_id: shopUserRole.id, resource_id: 'Document', right_key: 'Read' },

    // Work Instruction
    { role_id: shopUserRole.id, resource_id: 'Instruction', right_key: 'Read' },

    // Asset
    { role_id: shopUserRole.id, resource_id: 'Asset', right_key: 'Read' },

    // User
    { role_id: shopUserRole.id, resource_id: 'User', right_key: 'Read' },

    // Role
    { role_id: shopUserRole.id, resource_id: 'Role', right_key: 'Read' },
  ];

  await knex('acl').insert(
    acls.map(({ role_id, resource_id, right_key }) => ({
      role_id,
      resource_id: Array.isArray(resource_id) ? resource_id.join('.') : resource_id,
      right_key,
    })),
  );
}
