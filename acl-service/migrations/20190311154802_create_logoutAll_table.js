exports.up = function(knex, Promise) {
  return knex.schema.createTable('logout_all', function(table) {
    table.increments();
    table.string('user_id').notNullable();
    table.bigInteger('date').defaultTo(Date.now());
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('logout_all');
};
