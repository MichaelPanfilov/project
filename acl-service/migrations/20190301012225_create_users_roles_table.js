exports.up = function(knex, Promise) {
  return knex.schema.createTable('users_roles', function(table) {
    table.increments();
    table.string('user_id').notNullable();
    table.string('role_id').notNullable();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('users_roles');
};
