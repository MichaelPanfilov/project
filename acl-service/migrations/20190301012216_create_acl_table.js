exports.up = function(knex, Promise) {
  return knex.schema.createTable('acl', function(table) {
    table.increments();
    table.string('role_id').notNullable();
    table.string('resource_id').notNullable();
    table.string('right_key').notNullable();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
    table.unique(['role_id', 'resource_id', 'right_key']);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('acl');
};
