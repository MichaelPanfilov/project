const fs = require('fs');

module.exports = {
  jwtSecret: fs.readFileSync('/data/certs/staging/key'),
  jwtPublic: fs.readFileSync('/data/certs/staging/key.pub'),

  httpPort: 8080,

  dataPath: '/data/',
  logPath: '/data/logs/',
  uploadPath: '/data/uploads/',
};
