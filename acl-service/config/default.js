const path = require('path');

module.exports = {
  jwtAlgorithm: 'RS512',
  jwtValidityS: 60 * 60 * 24 * 20,
  jwtInternalAlgorithm: 'RS256',

  httpPort: 4070,
  fullErrorStacks: false,

  dataPath: path.join(process.cwd(), 'data/'),

  logNamespace: 'app',
  logPath: path.join(process.cwd(), 'data/logs/'),

  uploadPath: path.join(process.cwd(), 'data/uploads/'),
};
