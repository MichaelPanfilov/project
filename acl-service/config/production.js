const path = require('path');
const fs = require('fs');

module.exports = {
  jwtSecret: fs.readFileSync('/data/certs/production/key'),
  jwtPublic: fs.readFileSync('/data/certs/production/key.pub'),
  jwtInternalPublic: fs.readFileSync(
    path.join(process.cwd(), 'data/certs/production/internal.key.pub'),
  ),
  jwtInternalSecret: fs.readFileSync(
    path.join(process.cwd(), 'data/certs/production/internal.key'),
  ),

  httpPort: 8080,

  dataPath: '/data/',
  logPath: '/data/logs/',
  uploadPath: '/data/uploads/',
};
