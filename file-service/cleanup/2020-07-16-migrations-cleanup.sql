DELETE FROM `knex_migrations` WHERE `name` IN (
'20190312175018_create_files_table.js',
'20190327135803_add_token_column_to_files_table.js',
'20190503123122_change_primary_key.js',
'20200324135043_add_title_column_to_file_table.js',
'20200325121712_create_containers_table.js',
'20200325121751_add_files_container_id.js',
'20200326145506_add_machine_type_tags_column_to_file_table.js',
'20200326175850_update_ref_id_column_in_files_table.js',
'20200330103813_rename_ref_id_column_to_ref_ids_in_file_table.js',
'20200619155822_update_ref_ids_type_to_text_in_file_table.js'
);

INSERT INTO `knex_migrations` (`name`, `batch`, `migration_time`) VALUES
('20190312175018_create_files_table.js', 1, '2020-07-16 15:57:33'),
('20200325121712_create_containers_table.js', 1, '2020-07-16 15:57:33'),
('20200325121751_add_files_container_id.js', 1, '2020-07-16 15:57:33');
