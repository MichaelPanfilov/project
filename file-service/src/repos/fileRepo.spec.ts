import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';

import { createDatabase, dropDatabase } from '../../test/helpers/connection';
import { knex } from '../core/knex';
import { FileData } from '../models/file';

import { FileRepo } from './fileRepo';

describe('Repo: File', () => {
  let bottle: Bottle;
  let repo: FileRepo;

  beforeAll(async () => {
    await createDatabase();
  });

  afterAll(async () => {
    await dropDatabase();
  });

  beforeEach(async () => {
    bottle = new Bottle('testBottle');
    try {
      await knex.migrate.rollback();
      await knex.migrate.latest();
      await knex(FileData.tableName).insert([
        {
          id: '1',
          filename: 'test1',
          ref_ids: '["refId1"]',
          tags: '["tag1"]',
        },
        {
          id: '2',
          filename: 'test2',
          ref_ids: '["testRefId"]',
          tags: '["testTag"]',
        },
        {
          id: '3',
          filename: 'test3',
          ref_ids: '["testRefId"]',
          tags: '["testTag"]',
        },
      ]);
    } catch (e) {
      await knex.migrate.rollback();
      await knex.migrate.latest();
    }

    require('./fileRepo').Factory({ bottle });
    repo = bottle.container.FileRepo;
  });

  afterEach(async () => {
    // await knex.raw(`DROP DATABASE \`${process.env.APP_TEST_DB_NAME}\``);
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');
  });

  describe('save', () => {
    it('should return uploaded file', async () => {
      const file = await repo.save({ filename: 'testFile.txt', size: 10 }, {});
      expect(file as unknown).toEqual(
        jasmine.objectContaining({
          id: jasmine.any(String),
          filename: 'testFile.txt',
          size: 10,
          contentType: jasmine.any(String),
          customData: null,
          createdAt: jasmine.any(String),
          updatedAt: jasmine.any(String),
        }),
      );
    });

    it('should throw error when big data size', async () => {
      try {
        await repo.save({ filename: 'test.png', size: 513 }, {});
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotSupportedError('big file size is not supported'));
      }
    });
  });

  describe('saveToken', () => {
    it('should return file with token', async () => {
      const file = await repo.saveToken('1', 'valid');
      expect(file.token).toEqual('valid');
    });

    it('should throw error when wrong fileID', async () => {
      try {
        await repo.saveToken('99', 'valid');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('file'));
      }
    });
  });

  describe('update', () => {
    it('should return updated file', async () => {
      const file = await repo.update('1', { filename: 'newFileName.txt' });
      expect(file.filename).toEqual('newFileName.txt');
    });

    it('should throw error when wrong fileID', async () => {
      try {
        await repo.update('99', { filename: 'newFileName.txt' });
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('file'));
      }
    });
  });

  describe('delete', () => {
    it('should return correct deleted count', async () => {
      const count = await repo.delete('2');
      expect(count).toEqual(1);
    });

    it('should throw error when wrong fileID', async () => {
      try {
        await repo.delete('99');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('file'));
      }
    });
  });

  describe('getFilesByFilter', () => {
    it('should return correct data by filter', async () => {
      const res = await repo.getFilesByFilter({ refId: 'testRefId', tag: 'testTag' });
      const refs = res.files.map(file => file.refIds);
      const tags = res.files.map(file => file.tags);
      expect(refs).toEqual(['["testRefId"]', '["testRefId"]']);
      expect(tags).toEqual(['["testTag"]', '["testTag"]']);
    });
  });

  describe('getById', () => {
    it('should return file when correct ID', async () => {
      const file = await repo.getById('1');
      expect(file.id).toEqual('1');
    });

    it('should throw error when wrong ID', async () => {
      try {
        await repo.getById('99');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('file'));
      }
    });
  });
});
