import { getExtension } from '@common';
import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import sinon = require('sinon');

import { assignDefined } from '../helpers/assignDefined';
import { getContentType } from '../helpers/filesHelpers';
import { FileData } from '../models/file';
import { fileMock, fileMock2, filesMock } from '../models/file.mock';

import { serviceName } from './fileRepo';

export const mockServiceName = serviceName + 'Mock';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(mockServiceName, FileRepoMock);
}

export class FileRepoMock {
  fakeFile: FileData;

  constructor() {
    this.fakeFile = new FileData();
    this.fakeFile.$set({
      filename: 'foo',
    });
  }

  static factory(bottle: Bottle) {
    bottle.factory(serviceName, () => {
      const stub = new FileRepoMock();

      return {
        save: stub.save,
        saveToken: stub.saveToken,
        getFilesByFilter: stub.getFilesByFilter,
        getById: stub.getById,
        update: stub.update,
        delete: stub.delete,
      };
    });
  }

  async save(...args: unknown[]) {
    const [file] = args as FileData[];
    const stub = sinon.stub();

    getContentType(getExtension(file.filename));

    if (file.size > 512) {
      stub.throws(new commonErrors.NotSupportedError('file is too big'));
    } else {
      stub.resolves(fileMock);
    }

    return stub(...args);
  }

  async saveToken(...args: unknown[]) {
    return sinon.stub().resolves({ token: 'valid' })(...args);
  }

  async getFilesByFilter(...args: unknown[]) {
    const stub = sinon.stub();

    stub
      .withArgs({ refId: 'string', tag: 'string' })
      .resolves({ files: filesMock, meta: { count: 2, page: 1, pageCount: 1, total: 2 } });

    return stub.resolves({ files: [], meta: { count: 0, page: 1, pageCount: 1, total: 0 } })(
      ...args,
    );
  }

  async getById(...args: unknown[]) {
    const stub = sinon.stub();

    stub.throws(new commonErrors.NotFoundError('file'));
    stub.withArgs('1').resolves(fileMock);
    stub.withArgs('2').resolves(fileMock2);

    return stub(...args);
  }

  async update(...args: unknown[]) {
    const body = args[1] as { [key: string]: unknown };
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('file'));
    // tslint:disable-next-line
    stub.withArgs('1').resolves(assignDefined(fileMock as { [key: string]: any }, body));
    return stub(...args);
  }

  async delete(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(new commonErrors.NotFoundError('file'));
    stub.withArgs('1').resolves(1);
    return stub(...args);
  }
}
