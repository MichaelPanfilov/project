import { FilesFilter, PagingResponseMeta, ReferencedEntities } from '@common';
import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';

import { Config } from '../core/config';
import { createPageMeta } from '../helpers/createPageMeta';
import { applyFileFilter } from '../helpers/filesHelpers';
import { Container } from '../models/container';
import { FileData } from '../models/file';
import { FileService } from '../services/fileService';
import { RealFileBody } from '../types';

import { FileRepo } from './fileRepo';

export const serviceName = 'ContainerRepo';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(serviceName, ContainerRepo, 'FileRepo', 'fileService');
}

export class ContainerRepo {
  constructor(private fileRepo: FileRepo, private fileService: FileService) {}
  async addVersion(oldId: string, newFile: RealFileBody) {
    const file = await FileData.query().findById(oldId);
    if (!file) {
      throw new commonErrors.NotFoundError('file');
    }
    if (file.containerId) {
      return this.updateContainerVersion(file.containerId, newFile);
    }
    return this.createContainer(file, newFile);
  }

  async createContainer(file: FileData, newFile: RealFileBody) {
    try {
      const container = await Container.query().insertGraphAndFetch({
        id: file.id.replace(ReferencedEntities.FILE, ReferencedEntities.CONTAINER),
      });
      const [storedFile] = await Promise.all([
        this.fileRepo.save(newFile, {
          ...file.parsableData,
          title: file.title,
          version: 1,
          containerId: container.id,
        }),
        file.$query().patch({ version: 0, containerId: container.id }),
      ]);
      let token = file.token;
      if (!token) {
        token = this.fileService.createFileToken(storedFile.id);
      }
      await this.fileRepo.saveToken(storedFile.id, token);

      await this.fileService.saveFile(storedFile.id, newFile);
      return this.getLatestVersion(container.id);
    } catch (e) {
      throw e;
    }
  }

  async updateContainerVersion(containerId: string, newFile: RealFileBody) {
    try {
      const container = await Container.query().findById(containerId);
      if (!container) {
        throw new commonErrors.NotFoundError('container');
      }

      const [latestVersionedFile] = await container
        .$relatedQuery('files')
        .orderBy('version', 'DESC')
        .limit(1);
      if (!latestVersionedFile) {
        throw new commonErrors.NotFoundError('file');
      }

      const file = latestVersionedFile as FileData;
      const version = file.version || file.version === 0 ? file.version : 0;
      const storedFile = await this.fileRepo.save(newFile, {
        ...file.parsableData,
        title: file.title,
        version: version + 1,
        containerId: container.id,
      });
      const token = file.token || this.fileService.createFileToken(storedFile.id);

      await this.fileRepo.saveToken(storedFile.id, token);
      await this.fileService.saveFile(storedFile.id, newFile);

      return this.getLatestVersion(container.id);
    } catch (e) {
      throw e;
    }
  }

  async getAllVersions(containerId: string) {
    const container = await Container.query()
      .where('id', containerId)
      .eager('files')
      .first();
    if (!container) {
      throw new commonErrors.NotFoundError('container');
    }
    return container;
  }

  async getLatestVersion(containerId: string) {
    const version = await FileData.query()
      .where('container_id', containerId)
      .orderBy('version', 'DESC')
      .first();

    if (!version) {
      throw new commonErrors.NotFoundError('file');
    }

    return version as FileData;
  }

  async getLatestFiles(
    filter: Partial<FilesFilter>,
  ): Promise<{ files: FileData[]; meta: PagingResponseMeta }> {
    const { page = 0, limit = Config.queryMaxLimit } = filter;
    const query = FileData.query()
      .skipUndefined()
      .from('files as f')
      .whereRaw(
        '(SELECT COUNT(*) FROM `files` AS `temp` WHERE f.container_id = temp.container_id AND f.version < temp.version) < 1',
      );

    const { results, total } = await applyFileFilter(filter, query);

    return {
      files: results,
      meta: createPageMeta(results, page, total, limit),
    };
  }
}
