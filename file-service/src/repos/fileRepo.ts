import { FilesFilter, getRefId, PagingResponseMeta, ReferencedEntities } from '@common';
import { getExtension } from '@common';
import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import omitBy = require('lodash.omitby');
import ow from 'ow';

import { Config } from '../core/config';
import { createPageMeta } from '../helpers/createPageMeta';
import { applyFileFilter, getContentType } from '../helpers/filesHelpers';
import { generateId } from '../helpers/generateId';
import { toStringifyArray } from '../helpers/toStringifyArray';
import { FileData } from '../models/file';
import { CreateFileBody, UpdateFileBody } from '../types';

export const serviceName = 'FileRepo';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(serviceName, FileRepo);
}

export class FileRepo {
  async save(
    { filename, size }: CreateFileBody,
    options: {
      title?: string;
      refIds?: string | string[];
      tags?: string | string[];
      machineTypeTags?: string | string[];
      uploaderId?: string;
      containerId?: string;
      version?: number;
    },
  ): Promise<FileData> {
    if (size > Config.uploadMaxFileSize) {
      throw new commonErrors.NotSupportedError('big file size is not supported');
    }

    const { title, refIds, tags, uploaderId, containerId, version, machineTypeTags } = options;

    const contentType = getContentType(getExtension(filename));
    let createdBy;

    if (uploaderId) {
      createdBy = uploaderId;
    }

    const mappedTags = toStringifyArray(tags);
    const mappedRefIds = toStringifyArray(refIds);
    const mappedMachineTypeTags = toStringifyArray(machineTypeTags);
    const id = getRefId(generateId(), ReferencedEntities.FILE);

    const payload = omitBy(
      {
        id,
        filename,
        size,
        title,
        refIds: mappedRefIds,
        tags: mappedTags,
        machineTypeTags: mappedMachineTypeTags,
        contentType,
        createdBy,
        containerId,
        version,
      },
      v => !v,
    );
    const file = await FileData.query()
      .insert(payload)
      .first();

    if (!file) {
      throw new commonErrors.Error('file');
    }

    return FileData.query().findById(file.id) as Promise<FileData>;
  }

  async saveToken(fileId: string, token: string): Promise<FileData> {
    await this.getById(fileId);

    return FileData.query().patchAndFetchById(fileId, { token });
  }

  async update(
    id: string,
    { filename, refIds, tags, machineTypeTags, customData, title }: UpdateFileBody,
  ): Promise<FileData> {
    ow(id, 'fileId', ow.string);

    const mappedTags = toStringifyArray(tags);
    const mappedRefIds = toStringifyArray(refIds);
    const mappedMachineTypeTags = toStringifyArray(machineTypeTags);

    try {
      await FileData.query().upsertGraph({
        id,
        filename,
        refIds: mappedRefIds,
        tags: mappedTags,
        machineTypeTags: mappedMachineTypeTags,
        customData: JSON.stringify(customData),
        title,
      });

      return FileData.query()
        .where('id', id)
        .first()
        .throwIfNotFound();
    } catch (e) {
      throw e.statusCode === 404 ? new commonErrors.NotFoundError('file') : e;
    }
  }

  async delete(id: string): Promise<number> {
    ow(id, 'fileId', ow.string);

    await this.getById(id);

    return FileData.query().deleteById(id);
  }

  async getFilesByFilter(
    filter: Partial<FilesFilter>,
  ): Promise<{ files: FileData[]; meta: PagingResponseMeta }> {
    const { page = 0, limit = Config.queryMaxLimit } = filter;

    const query = FileData.query().skipUndefined();

    const { results, total } = await applyFileFilter(filter, query);

    return {
      files: results,
      meta: createPageMeta(results, page, total, limit),
    };
  }

  async getById(id: string): Promise<FileData> {
    const file = await FileData.query()
      .where('id', id)
      .first();

    if (!file) {
      throw new commonErrors.NotFoundError('file');
    }

    return file;
  }
}
