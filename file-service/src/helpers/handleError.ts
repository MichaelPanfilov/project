import * as httpErrors from 'http-errors';

import { CommonError } from '../types';

import { logger } from './../core/logger';

export const handleError = (e: CommonError) => {
  logger.error('handle error in controller: ', e);
  switch (e.name) {
    case 'NotSupportedError':
      throw new httpErrors.BadRequest(e.message);
    case 'ArgumentError':
      throw new httpErrors.BadRequest(`${e.argumentName} is invalid`);
    case 'NotFoundError':
      throw new httpErrors.NotFound(`Cannot find ${e.entity_name}`);
    case 'AlreadyInUseError':
      throw new httpErrors.Conflict(`${e.entity_name} already exist with this args`);
    case 'AuthenticationRequiredError':
      throw new httpErrors.Unauthorized('Invalid Authorization');
    case 'ValidationError':
      throw new httpErrors.Forbidden(e.message);
    default:
      throw new httpErrors.InternalServerError('Server error');
  }
};
