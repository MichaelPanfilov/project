import { Response } from 'express';
import * as fs from 'fs';
import { Headers } from 'request';

export function sendResponse(
  response: Response,
  responseStatus: number,
  responseHeaders: Headers,
  readable: fs.ReadStream | null,
) {
  if (responseHeaders) {
    response.writeHead(responseStatus, responseHeaders);
  }

  if (readable == null) {
    response.end();
  } else {
    readable.on('open', () => readable.pipe(response));
  }

  return null;
}
