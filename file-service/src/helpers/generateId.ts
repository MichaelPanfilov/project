const uuidv1 = require('uuid/v1');

export const generateId = () => uuidv1();
