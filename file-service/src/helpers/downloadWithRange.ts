import { getExtension } from '@common';
import { Response } from 'express';
import * as fs from 'fs';
import { Headers } from 'request';

import { getContentType } from './filesHelpers';
import { sendResponse } from './sendResponse';

export function downloadWithRange(
  res: Response,
  storedFilename: string,
  realFilename: string,
  size: number,
  { start, end }: { [key: string]: number },
  filePath: string,
) {
  const responseHeaders: Headers = {};

  if (start >= size || end >= size) {
    responseHeaders['Content-Range'] = 'bytes */' + size;
    sendResponse(res, 416, responseHeaders, null);
  } else {
    responseHeaders['Content-Range'] = 'bytes ' + start + '-' + end + '/' + size;
    responseHeaders['Content-Length'] = start === end ? 0 : end - start + 1;
    responseHeaders['Content-Type'] = getContentType(getExtension(realFilename));
    responseHeaders['Accept-Ranges'] = 'bytes';
    responseHeaders['Cache-Control'] = 'no-cache';

    sendResponse(res, 206, responseHeaders, fs.createReadStream(filePath, { start, end }));
  }
}
