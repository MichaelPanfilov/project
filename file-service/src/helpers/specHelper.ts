import errorHandler from '@elunic/express-error-handler';
import { mockBottlejsLogService } from '@elunic/logger/mocks';
import * as bodyParser from 'body-parser';
import * as Bottle from 'bottlejs';
import { Express, NextFunction, Request, Response } from 'express';
import * as sinon from 'sinon';
import * as superagent from 'superagent';

export class SpecHelper {
  static init(bottle: Bottle, app: Express, withMw = true) {
    bottle.factory('log', mockBottlejsLogService('test'));

    // Setup express
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(bodyParser.text());
    app.use(bodyParser.raw());

    if (withMw) {
      SpecHelper.mockAuthentication(app);
    }
  }

  static errorHandler(app: Express) {
    // Setup error handler after all other middlewares/routes
    const errorMw = errorHandler({
      full: true,
    });
    app.use(errorMw);
    return errorMw;
  }

  static mockAuthentication(app: Express) {
    app.use((req: Request, res: Response, next: NextFunction) => {
      const validAuth = ['valid', 'Bearer valid', 'no_permission', 'Bearer no_permission'];
      if (req.headers.authorization && validAuth.includes(req.headers.authorization)) {
        req.auth = {
          username: 'foo',
          userId: 'user-test123',
        };
      } else {
        req.auth = {
          username: undefined,
          userId: undefined,
        };
      }

      next();
    });
  }

  static mockInvalidAuthService(bottle: Bottle) {
    bottle.factory('authService', () => {
      return {
        login: sinon.fake.resolves('asdf'),
        getToken: sinon.fake.resolves('asdf'),
        verifyToken: sinon.fake.resolves(false),
        generateToken: sinon.stub().returns('asdf'),
      };
    });
  }

  // tslint:disable-next-line
  static expectApiResponse(res: superagent.Response, jasmineObject: any) {
    return expect(res.body).toEqual(
      jasmine.objectContaining({
        data: jasmineObject,
        meta: jasmine.any(Object),
      }),
    );
  }
}
