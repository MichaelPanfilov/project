export function toStringifyArray(data: undefined | string | string[]) {
  return data && typeof data === 'string' ? `["${data}"]` : JSON.stringify(data);
}
