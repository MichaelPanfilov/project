import * as httpErrors from 'http-errors';

export function readRangeHeader(range: string, totalLength: number) {
  if (!range || !range.length) {
    return null;
  }

  if (!range.startsWith('bytes')) {
    throw new httpErrors.BadRequest('Incorrect range header');
  }

  const array = range.split(/bytes=([0-9]*)-([0-9]*)/);

  //tslint:disable
  const start = parseInt(array[1], 10);
  const end = parseInt(array[2], 10);
  // tslint:enable

  const result = {
    start: isNaN(start) ? 0 : start,
    end: isNaN(end) ? totalLength - 1 : end,
  };

  if (!isNaN(start) && isNaN(end)) {
    result.start = start;
    result.end = totalLength - 1;
  }

  if (isNaN(start) && !isNaN(end)) {
    result.start = totalLength - end;
    result.end = totalLength - 1;
  }

  return result;
}
