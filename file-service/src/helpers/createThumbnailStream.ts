import * as fs from 'fs';
import * as sharp from 'sharp';

type Fit = 'contain' | 'cover' | 'fill' | 'inside' | 'outside' | undefined;

export const createThumbnailStream = (
  path: string,
  width: number,
  height: number,
  _fit: string,
): sharp.Sharp => {
  let fit: Fit;

  for (const f in sharp.fit) {
    if (f === _fit) {
      fit = f as Fit;
    }
  }

  const transformer = sharp().resize(width, height, { fit });

  return fs.createReadStream(path).pipe(transformer);
};
