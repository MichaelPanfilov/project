import { PagingResponseMeta } from '@common';

export function createPageMeta(
  results: unknown[],
  page: number,
  total: number,
  limit: number,
): PagingResponseMeta {
  return {
    count: results.length,
    // In the api we propose pages to start from 1 but objections start at index 0.
    page: page + 1,
    pageCount: Math.max(Math.floor(total / limit), 1),
    total,
  };
}
