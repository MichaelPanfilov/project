import { ContentTypes, FilesFilter } from '@common';
import * as commonErrors from 'common-errors';
import { Page, QueryBuilder, snakeCaseMappers } from 'objection';

import { Config } from '../core/config';
import { Model } from '../models/model';

export function getContentType(ext: string): string {
  switch (ext.toLowerCase()) {
    case 'pdf':
      return ContentTypes.PDF;
    case 'svg':
      return ContentTypes.SVG;
    case 'png':
      return ContentTypes.PNG;
    case 'jpeg':
    case 'jpg':
      return ContentTypes.JPEG;
    case 'gif':
      return ContentTypes.GIF;
    case 'webm':
      return ContentTypes.WEBM;
    case 'mpeg':
      return ContentTypes.MPEG;
    case 'mp4':
      return ContentTypes.MP4;
    case 'avi':
      return ContentTypes.AVI;
    case 'wmv':
      return ContentTypes.WMV;
    case 'mov':
      return ContentTypes.MOV;
    case 'mkv':
    case 'mk3d':
    case 'mka':
    case 'mks':
      return ContentTypes.MKV;
    case 'txt':
      return ContentTypes.PLAIN;
    case 'csv':
      return ContentTypes.CSV;
    case 'xla':
    case 'xlam':
    case 'xls':
    case 'xlsb':
    case 'xlsm':
    case 'xlsx':
      return ContentTypes.EXCEL;
    case 'ppt':
    case 'pptm':
    case 'pptx':
      return ContentTypes.POWER_POINT;
    case 'doc':
    case 'docm':
    case 'docx':
      return ContentTypes.WORD;
    case 'zip':
      return ContentTypes.ZIP;
    case 'gzip':
      return ContentTypes.GZIP;
    case 'json':
      return ContentTypes.JSON;
    default:
      throw new commonErrors.NotSupportedError('This content type is not supported');
  }
}

export function getThumbnailByExt(ext: string): string {
  switch (ext.toLowerCase()) {
    case 'mkv':
    case 'mk3d':
    case 'mka':
    case 'mks':
      return 'mkv.png';
    case 'xla':
    case 'xlam':
    case 'xls':
    case 'xlsb':
    case 'xlsm':
    case 'xlsx':
      return 'excel.png';
    case 'ppt':
    case 'pptm':
    case 'pptx':
      return 'ppt.png';
    case 'doc':
    case 'docm':
    case 'docx':
      return 'doc.png';
    default:
      return `${ext}.png`; // @README All saved static thumbnails should be .png extension
  }
}

export async function applyFileFilter<T extends Model & { contentType: string }>(
  filter: Partial<FilesFilter>,
  query: QueryBuilder<T>,
): Promise<Page<T>> {
  const {
    id,
    title,
    refId,
    tag,
    contentType,
    machineTypeTag,
    contentTypeGroup,
    limit = Config.queryMaxLimit,
    page = 0,
    offset = 0,
    order = '',
  } = filter;

  if (id) {
    const arr = Array.isArray(id) ? id : [id];
    query = query.whereIn('id', arr);
  }
  if (title) {
    query = query.where('title', 'like', `%${title}%`);
  }
  if (refId) {
    query = addFilter(refId, query, 'ref_ids');
  }
  if (tag) {
    query = addFilter(tag, query, 'tags');
  }
  if (contentType) {
    const arr = Array.isArray(contentType) ? contentType : [contentType];
    query.whereIn('content_type', arr);
  }
  if (contentTypeGroup) {
    const arr = Array.isArray(contentTypeGroup) ? contentTypeGroup : [contentTypeGroup];
    query.whereIn('content_type_group', arr);
  }
  if (machineTypeTag) {
    query = addFilter(machineTypeTag, query, 'machine_type_tags');
  }

  const orderProp = order.split(' ')[0];
  const parsed = Object.keys(snakeCaseMappers().format({ [orderProp]: orderProp }));
  const orderDirection = order.split(' ')[1];
  const { results, total } = await query
    .offset(offset)
    .page(Math.max(page - 1, 0), limit)
    .orderBy(parsed[0], orderDirection)
    .execute();

  return { results, total };
}

export function addFilter<T extends Model>(
  value: string | string[],
  query: QueryBuilder<T>,
  field: string,
): QueryBuilder<T> {
  const arr = Array.isArray(value) ? value : [value];
  return query.whereWrapped(qb => arr.forEach(v => (qb = qb.orWhere(field, 'like', `%${v}%`))));
}

export function arrayIncludes(keys: string | string[], arrayOrStr?: string | string[]): boolean {
  if (!arrayOrStr) {
    return false;
  }

  const keysArr = typeof keys === 'string' ? [keys] : keys;
  try {
    const arr = Array.isArray(arrayOrStr) ? arrayOrStr : (JSON.parse(arrayOrStr) as string[]);
    if (!Array.isArray(arr)) {
      return keysArr.includes(arr);
    }
    return !!arr.find(item => keysArr.includes(item));
  } catch {
    return false;
  }
}
