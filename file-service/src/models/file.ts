import { ContentTypeGroup, DOC_TYPES, sortTags } from '@common';
import Objection = require('objection');

import { Config } from '../core/config';
import { toStringifyArray } from '../helpers/toStringifyArray';
import { AuthenticatedRequest } from '../types';

import { Container } from './container';
import { Model } from './model';

export class FileData extends Model {
  filename!: string;
  refIds?: string;
  tags?: string;
  machineTypeTags?: string;
  token!: string;
  createdBy!: string;
  size!: number;
  contentType!: string;
  contentTypeGroup!: ContentTypeGroup;
  customData?: string;
  title?: string;
  version?: number;
  containerId?: string;

  static tableName = 'files';

  static jsonSchema = {
    type: 'object',
    required: ['filename'],

    properties: {
      id: { type: 'string' },
      createdAt: { type: 'string', format: 'date-time' },
      updatedAt: { type: 'string', format: 'date-time' },
      containerId: { type: 'string' },

      filename: { type: 'string' },
      refIds: { type: 'string' },
      token: { type: 'string' },
      version: { type: 'int' },
    },
  };

  static get relationMappings() {
    return {
      containers: {
        relation: Model.BelongsToOneRelation,
        modelClass: Container,
        join: {
          from: 'files.container_id',
          to: 'containers.id',
        },
      },
    };
  }

  get parsableData() {
    return {
      refIds: Array.isArray(this.refIds)
        ? this.refIds
        : this.refIds
        ? JSON.parse(this.refIds)
        : undefined,
      tags: Array.isArray(this.tags) ? this.tags : this.tags ? JSON.parse(this.tags) : undefined,
      machineTypeTags: Array.isArray(this.machineTypeTags)
        ? this.machineTypeTags
        : this.machineTypeTags
        ? JSON.parse(this.machineTypeTags)
        : undefined,
    };
  }

  get withUrls() {
    return {
      ...this.parsableData,
      id: this.id,
      filename: this.filename,
      createdBy: this.createdBy,
      size: this.size,
      contentType: this.contentType,
      contentTypeGroup: this.contentTypeGroup,
      customData: this.customData,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      title: this.title,
      relativePublicUrl: `/v1/files/${this.id}/download/${this.token}`,
      relativePrivateUrl: `/v1/files/${this.id}/download`,
      relativeThumbnailPublicUrl: `/v1/files/${this.id}/thumbnail/${this.token}`,
      relativeThumbnailPrivateUrl: `/v1/files/${this.id}/thumbnail`,
      relativeViewUrl: `/v1/files/${this.id}/view`,
    };
  }

  withFullUrls(req: AuthenticatedRequest) {
    const relativePublicUrl = `/v1/files/${this.id}/download/${this.token}`;
    const relativePrivateUrl = `/v1/files/${this.id}/download`;

    let fileServiceUrl = Config.fileServicePublicUrl;
    if (process.env.NODE_ENV === 'testing') {
      fileServiceUrl = `${req.protocol}://${req.get('host')}/v1`;
    }
    fileServiceUrl = fileServiceUrl.replace('/v1', '');

    const publicUrl = `${fileServiceUrl}${relativePublicUrl}`;
    const privateUrl = `${fileServiceUrl}${relativePrivateUrl}`;

    return {
      ...this.withUrls,
      publicUrl,
      privateUrl,
      thumbnailPublicUrl: `${fileServiceUrl}${this.withUrls.relativeThumbnailPublicUrl}`,
      thumbnailPrivateUrl: `${fileServiceUrl}${this.withUrls.relativeThumbnailPrivateUrl}`,
      viewUrl: `${fileServiceUrl}${this.withUrls.relativeViewUrl}`,
    };
  }

  async $beforeInsert(queryContext: Objection.QueryContext) {
    await super.$beforeInsert(queryContext);
    const tags = this.machineTypeTags;
    this.machineTypeTags = tags ? toStringifyArray(sortTags(JSON.parse(tags))) : undefined;
    this.contentTypeGroup =
      (Object.keys(DOC_TYPES).find(k => {
        const arr = DOC_TYPES[k as ContentTypeGroup];
        return arr ? arr.includes(this.contentType) : false;
      }) as ContentTypeGroup) || ContentTypeGroup.OTHER;
  }

  async $beforeUpdate(
    opt: Objection.ModelOptions,
    queryContext: Objection.QueryContext,
  ): Promise<void> {
    await super.$beforeUpdate(opt, queryContext);
    const tags = this.machineTypeTags;
    this.machineTypeTags = tags ? toStringifyArray(sortTags(JSON.parse(tags))) : undefined;
  }
}
