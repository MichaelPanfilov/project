import { FileData } from './file';

export const fileMock = new FileData();
fileMock.$set({
  id: '1',
  filename: 'foo',
  refIds: '["string"]',
  tags: '["string", "documents"]',
  createdBy: '',
  size: 10,
  contentType: '',
  customData: '{}',
  token: '',
  createdAt: '',
  updatedAt: '',
  publicUrl: '',
  privateUrl: '',
});

export const fileMock2 = new FileData();
fileMock2.$set({
  id: '2',
  filename: 'bar',
  refIds: '["need_permission"]',
  tags: '["documents"]',
  createdBy: '',
  size: 10,
  contentType: '',
  customData: '{}',
  token: '',
  createdAt: '',
  updatedAt: '',
  publicUrl: '',
  privateUrl: '',
});

export const filesMock = [fileMock, fileMock2];
