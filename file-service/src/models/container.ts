import { FileData } from './file';
import { Model } from './model';

export class Container extends Model {
  static tableName = 'containers';

  static jsonSchema = {
    type: 'object',

    properties: {
      id: { type: 'string' },
      createdAt: { type: 'string', format: 'date-time' },
      updatedAt: { type: 'string', format: 'date-time' },
    },
  };
  static get relationMappings() {
    return {
      files: {
        relation: Model.HasManyRelation,
        modelClass: FileData,
        join: {
          from: 'containers.id',
          to: 'files.container_id',
        },
      },
    };
  }
}
