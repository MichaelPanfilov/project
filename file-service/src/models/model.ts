import * as moment from 'moment';
import * as Objection from 'objection';
import 'reflect-metadata';

import { knex } from '../core/knex';

Objection.Model.knex(knex);

const snakeCaseMappers = Objection.snakeCaseMappers();

export const MODEL_DB_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

const metadataKey = Symbol('modelDateProperties');
const formatPropMap = new Map();
const parsePropMap = new Map();

export class Model extends Objection.Model {
  readonly id!: string;
  @dbDate() createdAt!: string;
  @dbDate() updatedAt!: string;

  static columnNameMappers = snakeCaseMappers;

  async $beforeInsert(queryContext: Objection.QueryContext) {
    await super.$beforeInsert(queryContext);

    this.createdAt = moment.utc().toISOString();
    this.updatedAt = moment.utc().toISOString();
  }

  async $beforeUpdate(
    opt: Objection.ModelOptions,
    queryContext: Objection.QueryContext,
  ): Promise<void> {
    await super.$beforeUpdate(opt, queryContext);

    this.updatedAt = moment.utc().toISOString();
  }

  $parseDatabaseJson(json: { [key: string]: string | number | null }) {
    json = super.$parseDatabaseJson(json);

    const dateKeys = getParsingDatePropertyKeys(this);
    for (const dateKey of dateKeys) {
      if (json[dateKey]) {
        json[dateKey] = moment.utc(json[dateKey] || '', MODEL_DB_DATE_FORMAT).toISOString();
      }
    }

    return json;
  }

  $formatDatabaseJson(json: { [key: string]: string | object | number | null | undefined }) {
    json = super.$formatDatabaseJson(json);

    const dateKeys = getFormattingDatePropertyKeys(this);
    for (const dateKey of dateKeys) {
      if (json[dateKey]) {
        json[dateKey] = moment.utc(json[dateKey] || '').format(MODEL_DB_DATE_FORMAT);
      }
    }

    return json;
  }
}

export function dbDate() {
  return (target: object, propertyKey: string) => {
    if (typeof target === 'function') {
      throw new Error('The @dbDate decorator can only be used on prototype properties');
    }
    if (typeof propertyKey === 'undefined' || !propertyKey) {
      throw new Error('The @dbDate decorator must be called on a prototype property');
    }

    // @ts-ignore: declaration of reflect-metadata was not being read [wh]
    let storedModelDates = Reflect.getOwnMetadata(metadataKey, target);

    if (!storedModelDates) {
      storedModelDates = [];
    }

    storedModelDates.push(propertyKey);

    // @ts-ignore: declaration of reflect-metadata was not being read [wh]
    Reflect.defineMetadata(metadataKey, storedModelDates, target);
  };
}

function getDatePropertyKeys(target: Model): string[] {
  const targetProto = Object.getPrototypeOf(target);

  let storedDateKeys: string[] = [];
  let currentProto = targetProto;
  while (currentProto) {
    const targetKeys = Reflect.getOwnMetadata(metadataKey, currentProto);
    storedDateKeys = storedDateKeys.concat(targetKeys || []);
    currentProto = Object.getPrototypeOf(currentProto);
  }

  return storedDateKeys;
}

function getFormattingDatePropertyKeys(target: Model): string[] {
  const targetProto = Object.getPrototypeOf(target);

  if (formatPropMap.has(targetProto)) {
    return formatPropMap.get(targetProto);
  }

  const datePropKeys = getDatePropertyKeys(target);
  const formattingKeys = [];

  for (const targetKey of datePropKeys) {
    const containerObj = {
      [targetKey]: targetKey,
    };
    const formattedObj = snakeCaseMappers.format(containerObj);
    const formattedKey = Object.keys(formattedObj)[0];
    formattingKeys.push(formattedKey);
  }

  formatPropMap.set(targetProto, formattingKeys);
  return formattingKeys;
}

function getParsingDatePropertyKeys(target: Model): string[] {
  const targetProto = Object.getPrototypeOf(target);

  if (parsePropMap.has(targetProto)) {
    return parsePropMap.get(targetProto);
  }

  const datePropKeys = getDatePropertyKeys(target);
  const parsingKeys = [];

  for (const targetKey of datePropKeys) {
    const containerObj = {
      [targetKey]: targetKey,
    };
    const formattedObj = snakeCaseMappers.parse(containerObj);
    const formattedKey = Object.keys(formattedObj)[0];
    parsingKeys.push(formattedKey);
  }

  parsePropMap.set(targetProto, parsingKeys);
  return parsingKeys;
}
