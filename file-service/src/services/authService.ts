import { AclResource, AclRight, JWTPayload } from '@common';
import axios from 'axios';
import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import { Request } from 'express';
import * as jwt from 'jsonwebtoken';

import { Config } from '../core/config';
import { ErrorMessages } from '../core/consts';
import { handleError } from '../helpers/handleError';
import { AuthenticatedRequest } from '../types';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service('authService', AuthService);
}

export class AuthService {
  getToken(authorization: string) {
    return authorization.split(' ')[1];
  }

  getAuthorization(req: Request): string {
    const authorization =
      req.header('Authorization') ||
      (req.query && req.query.Authorization) ||
      (req.body && req.body.Authorization) ||
      (req.cookies && req.cookies.token);
    if (!authorization) {
      throw new commonErrors.NotFoundError('token');
    }
    return authorization;
  }

  async getTokenData(authorization: string): Promise<JWTPayload | void> {
    try {
      if (Config.jwtPublic) {
        const token = this.getToken(authorization);

        jwt.verify(token, Config.jwtPublic, { algorithms: [Config.jwtAlgorithm] });

        return jwt.decode(token, { json: true }) as JWTPayload;
      } else {
        const {
          data: { data },
        } = await axios.get(Config.aclServiceUrl + '/me', {
          headers: { Authorization: authorization },
        });

        return data;
      }
    } catch {
      handleError(new commonErrors.AuthenticationRequiredError('Invalid authorization'));
    }
  }

  async requireRights(
    req: AuthenticatedRequest,
    rights: { resourceId: AclResource; rightKey: AclRight }[],
  ): Promise<void> {
    const token = this.getToken(this.getAuthorization(req));
    try {
      this.verifyInternalToken(token);
      return;
    } catch {}

    const data = await Promise.all(
      rights.map(({ resourceId, rightKey }) => this.hasRight(req, resourceId, rightKey)),
    );

    if (!data.includes(true)) {
      handleError(new commonErrors.ValidationError(ErrorMessages.MISSING_RIGHTS));
    }
  }

  verifyInternalToken(token: string) {
    return jwt.verify(token, Config.jwtInternalPublic, {
      algorithms: [Config.jwtInternalAlgorithm],
    });
  }

  private async hasRight(
    req: AuthenticatedRequest,
    resourceId: AclResource,
    rightKey: AclRight,
  ): Promise<boolean> {
    const {
      data: { data },
    } = await axios.get(Config.aclServiceUrl + `/me/is_allowed/${resourceId}/${rightKey}`, {
      headers: { Authorization: this.getAuthorization(req) },
    });

    return data;
  }
}
