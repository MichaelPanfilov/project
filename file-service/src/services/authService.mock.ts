import * as Bottle from 'bottlejs';
import * as httpErrors from 'http-errors';
import sinon = require('sinon');

import { ErrorMessages } from '../core/consts';

export const serviceName = 'authService';
export const mockServiceName = serviceName + 'Mock';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(mockServiceName, AuthServiceMock);
}

export class AuthServiceMock {
  static factory(bottle: Bottle) {
    bottle.factory(serviceName, () => {
      const stub = new AuthServiceMock();

      return {
        getToken: stub.getToken,
        getTokenData: stub.getTokenData,
        verifyToken: stub.verifyToken,
        requireRights: stub.requireRights,
        hasRight: stub.hasRight,
      };
    });
  }

  getToken(...args: unknown[]) {
    return sinon.stub().returns('tokenMock')(...args);
  }

  getTokenData(...args: unknown[]) {
    return sinon.stub().returns({})(...args);
  }

  verifyToken(...args: unknown[]) {
    return sinon.stub().returns(true)(...args);
  }

  requireRights(...args: unknown[]) {
    const stub = sinon.stub();
    stub
      .withArgs(
        sinon.match.has('headers', sinon.match.has('authorization', 'no_permission')),
        sinon.match.any,
      )
      .throws(new httpErrors.Forbidden(ErrorMessages.MISSING_RIGHTS));

    return stub.returns(true)(...args);
  }

  hasRight(...args: unknown[]) {
    const stub = sinon.stub();

    stub.withArgs(sinon.match.any, 'files').returns(false);
    stub.withArgs(sinon.match.any, 'need_permission').returns(false);

    return stub.returns(true)(...args);
  }
}
