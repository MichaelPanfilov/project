import * as Bottle from 'bottlejs';

import { AuthServiceMock } from './authService.mock';

describe('SERVICE: Auth', () => {
  let bottle: Bottle;
  let auth: AuthServiceMock;

  beforeEach(async () => {
    bottle = new Bottle('testBottle');

    auth = new AuthServiceMock();
  });

  afterEach(async () => {
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');
  });

  describe('getToken', () => {
    it('should return token', async () => {
      const token = await auth.getToken();
      expect(token).toEqual('tokenMock');
    });
  });

  describe('getTokenData', () => {
    it('should return token data', async () => {
      const data = await auth.getTokenData();
      expect(data).toEqual(jasmine.objectContaining({}));
    });
  });

  describe('verifyToken', () => {
    it('should return true', async () => {
      const verify = await auth.verifyToken();
      expect(verify).toBe(true);
    });
  });

  describe('hasRight', () => {
    it('should return true', async () => {
      const hasRight = await auth.hasRight();
      expect(hasRight).toBe(true);
    });
  });
});
