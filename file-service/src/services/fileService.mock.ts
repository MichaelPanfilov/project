import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import * as fs from 'fs';
import * as httpErrors from 'http-errors';
import * as path from 'path';
import sinon = require('sinon');

import { Config } from '../core/config';
export const serviceName = 'fileService';
export const mockServiceName = serviceName + 'Mock';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(mockServiceName, FileServiceMock);
}

export class FileServiceMock {
  static factory(bottle: Bottle) {
    bottle.factory(serviceName, () => {
      const stub = new FileServiceMock();

      return {
        getFilenameById: stub.getFilenameById,
        saveFile: stub.saveFile,
        deleteFile: stub.deleteFile,
        createThumbnail: stub.createThumbnail,
        createFileToken: stub.createFileToken,
        getFileTokenPayload: stub.getFileTokenPayload,
        verifyFileToken: stub.verifyFileToken,
        getUrlSet: stub.getUrlSet,
      };
    });
  }

  getUrlSet(fileId: string, token: string, protocol: string, host: string | undefined) {
    const relativePublicUrl = `/v1/files/${fileId}/download/${token}`;
    const fileServiceUrl = `${protocol}://${host}/v1`;

    return {
      fileServiceUrl,
      relativePublicUrl,
      publicUrl: `${fileServiceUrl}${relativePublicUrl}`,
      viewUrl: `${fileServiceUrl}/v1/files/${fileId}/view`,
    };
  }

  getFilenameById(...args: unknown[]) {
    const stub = sinon.stub();

    stub.throws(new commonErrors.NotFoundError('file'));
    stub.withArgs('1').resolves('testFilename.txt');
    stub.withArgs('2').resolves('testFilename.txt');

    return stub(...args);
  }

  saveFile(...args: unknown[]) {
    const stub = sinon.stub();

    stub.throws(new commonErrors.ArgumentError('file'));
    stub.withArgs(sinon.match.any, sinon.match.has('filename', sinon.match.string)).resolves();

    return stub(...args);
  }

  deleteFile(...args: unknown[]) {
    const stub = sinon.stub();

    this.getFilenameById(...args);

    return stub(...args);
  }

  createThumbnail(...args: unknown[]) {
    return sinon
      .stub()
      .returns(fs.createReadStream(path.join(Config.uploadPath + '/test_thumbnail.png')))(...args);
  }

  createFileToken(...args: unknown[]) {
    const stub = sinon.stub();

    stub.withArgs(sinon.match.string).resolves('valid');
    stub.throws(new Error('fileId should be positive integer'));

    return stub(...args);
  }

  getFileTokenPayload(...args: unknown[]) {
    const stub = sinon.stub();

    stub.throws(new commonErrors.NotFoundError('token_payload'));
    stub.withArgs('valid').resolves({ fileId: '1' });

    return stub(...args);
  }

  verifyFileToken(...args: unknown[]) {
    const stub = sinon.stub();

    stub.throws(new httpErrors.Forbidden('Invalid file token'));
    stub.withArgs(sinon.match.any, 'valid').resolves(true);

    return stub(...args);
  }
}
