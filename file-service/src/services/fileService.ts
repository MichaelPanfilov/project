import { getExtension, isFileSupported, isImage } from '@common';
import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import * as fs from 'fs-extra';
import * as jwt from 'jsonwebtoken';
import ow from 'ow';
import * as path from 'path';
import * as QRCode from 'qrcode';

import { Config } from '../core/config';
import { createThumbnailStream } from '../helpers/createThumbnailStream';
import { getThumbnailByExt } from '../helpers/filesHelpers';
import { handleError } from '../helpers/handleError';
import { RealFileBody, URLSet } from '../types';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service('fileService', FileService);
}

export class FileService {
  constructor() {}

  getUrlSet(fileId: string, token: string, protocol: string, host: string | undefined): URLSet {
    const relativePublicUrl = `/v1/files/${fileId}/download/${token}`;

    let fileServiceUrl = Config.fileServicePublicUrl;
    if (process.env.NODE_ENV === 'testing') {
      fileServiceUrl = `${protocol}://${host}/v1`;
    }
    fileServiceUrl = fileServiceUrl.replace('/v1', '');

    return {
      fileServiceUrl,
      relativePublicUrl,
      publicUrl: `${fileServiceUrl}${relativePublicUrl}`,
      viewUrl: `${fileServiceUrl}/v1/files/${fileId}/view`,
      privateUrl: `${fileServiceUrl}/v1/files/${fileId}/download`,
    };
  }

  getFilenameById(id: string): string {
    const data = fs.readdirSync(Config.uploadPath);
    const filename = data.find(file => file.startsWith(id + '.'));

    if (!filename) {
      throw new commonErrors.NotFoundError('file');
    }

    return filename;
  }

  async saveFile(id: string, file: RealFileBody): Promise<void> {
    ow(id, 'fileId', ow.string);

    if (!file || !file.filename) {
      throw new commonErrors.ArgumentError('file');
    }

    const ext = path.parse(file.filename).ext;
    const finalPath = path.join(Config.uploadPath, `${id}.${ext}`);

    await fs.move(file.path, finalPath);
  }

  async deleteFile(id: string): Promise<void> {
    const filename = await this.getFilenameById(id);

    const finalPath = path.join(Config.uploadPath, filename);

    await fs.remove(finalPath);
  }

  async createQRCode(filename: string) {
    return await QRCode.toDataURL(filename, { errorCorrectionLevel: 'H' });
  }

  createThumbnail(filename: string, width: number, height: number, fit: string) {
    const ext = getExtension(filename);

    let filePath: string;

    if (isImage(ext)) {
      filePath = path.join(Config.uploadPath, filename);
    } else if (isFileSupported(ext)) {
      filePath = path.join(Config.thumbnailPath, getThumbnailByExt(ext));
    } else {
      throw new commonErrors.NotSupportedError('Unsupported format');
    }

    return createThumbnailStream(filePath, width, height, fit);
  }

  createFileToken(fileId: string): string {
    ow(fileId, 'fileId', ow.string);
    const jwtPayload = { fileId };
    return jwt.sign(jwtPayload, Config.jwtSecret);
  }

  // tslint:disable:no-any (jsonwebtoken return data with type string | {[key: string]: any})
  private getFileTokenPayload(token: string): { [key: string]: any } {
    const payload = jwt.decode(token, { json: true }) as { [key: string]: any };

    if (!payload) {
      throw new commonErrors.NotFoundError('token_payload');
    }

    return payload;
  }
  // tslint:enable:no-any

  verifyFileToken(fileId: string, token: string): boolean {
    if (!token) {
      return false;
    }

    try {
      jwt.verify(token, Config.jwtSecret);
    } catch {
      handleError(new commonErrors.ValidationError('Invalid file token'));
    }

    const payload = this.getFileTokenPayload(token);

    if (!payload) {
      return false;
    }

    const { fileId: id } = payload;

    return id === fileId;
  }
}
