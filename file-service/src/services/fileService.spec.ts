import * as Bottle from 'bottlejs';
import * as commonErrors from 'common-errors';
import * as httpErrors from 'http-errors';

import { FileServiceMock } from './fileService.mock';

describe('SERVICE: file', () => {
  let bottle: Bottle;
  let file: FileServiceMock;

  beforeEach(async () => {
    bottle = new Bottle('testBottle');

    file = new FileServiceMock();
  });

  afterEach(async () => {
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');
  });

  describe('getFilenameById', () => {
    it('should return filename with valid fileID', async () => {
      const filename = await file.getFilenameById('1');
      expect(filename).toEqual('testFilename.txt');
    });

    it('should throw error with wrong fileID', async () => {
      try {
        await file.getFilenameById('99');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('file'));
      }
    });
  });

  describe('saveFile', () => {
    it('should not throw an error if file saved successfully', async () => {
      try {
        await file.saveFile('1', { filename: 'any' });
        throw new Error('should be thrown');
      } catch (error) {
        expect(error.message).toEqual('should be thrown');
      }
    });

    it('should throw error when wrong fileData', async () => {
      try {
        await file.saveFile('1', {});
      } catch (error) {
        expect(error).toEqual(new commonErrors.ArgumentError('file'));
      }
    });
  });

  describe('deleteFile', () => {
    it('should not throw an error if file deleted successfully', async () => {
      try {
        await file.deleteFile('1');
        throw new Error('should be thrown');
      } catch (error) {
        expect(error.message).toEqual('should be thrown');
      }
    });

    it('should throw error when wrong fileID', async () => {
      try {
        await file.deleteFile('99');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('file'));
      }
    });
  });

  describe('createThumbnail', () => {
    // todo design tests after implement createThumbnail function
  });

  describe('createFileToken', () => {
    it('should return file token', async () => {
      const token = await file.createFileToken('1');
      expect(token).toEqual('valid');
    });

    it('should throw error when wrong fileID', async () => {
      try {
        await file.createFileToken(1);
        throw new Error('should not be thrown');
      } catch (error) {
        expect(error.message).not.toEqual('should not be thrown');
      }
    });
  });

  describe('getFileTokenPayload', () => {
    it('should return correct token payload when valid token', async () => {
      const payload = await file.getFileTokenPayload('valid');
      expect(payload).toEqual({ fileId: '1' });
    });

    it('should throw error when invalid token', async () => {
      try {
        await file.getFileTokenPayload('invalid');
      } catch (error) {
        expect(error).toEqual(new commonErrors.NotFoundError('token_payload'));
      }
    });
  });

  describe('verifyFileToken', () => {
    it('should return true when valid token', async () => {
      const verify = await file.verifyFileToken(1, 'valid');
      expect(verify).toEqual(true);
    });

    it('should throw error when invalid token', async () => {
      try {
        await file.verifyFileToken('1', 'invalid');
      } catch (error) {
        expect(error).toEqual(new httpErrors.Forbidden('Invalid file token'));
      }
    });
  });
});
