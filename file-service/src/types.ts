import * as Express from 'express';

export interface AuthenticatedRequest extends Express.Request {
  auth: import('@common').JWTPayload;
}

export interface RealFileBody {
  fieldname: string;
  path: string;
  size: number;
  filename: string;
  encoding: string;
  mimetype: string;
}

export interface CreateFileBody {
  size: number;
  filename: string;
}

export interface CreateFileData {
  title?: string;
  refIds?: string | string[];
  tags?: string | string[];
  machineTypeTags?: string | string[];
}

export interface UpdateFileBody {
  filename?: string;
  machineTypeTags?: string | string[];
  refIds?: string | string[];
  tags?: string | string[];
  customData?: object;
  title?: string;
}

export interface CommonError {
  name: string;
  argumentName?: string;
  entity_name?: string;
  message?: string;
}

export interface URLSet {
  relativePublicUrl: string;
  publicUrl: string;
  fileServiceUrl: string;
  viewUrl: string;
  privateUrl: string;
}
