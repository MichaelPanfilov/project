import * as Bottle from 'bottlejs';
import { makeMiddlewareInvoker } from 'bottlejs-express';
import { NextFunction, Request, Response, Router } from 'express';

import { AuthService } from '../../services/authService';

export function Factory({ expressApp, bottle }: { expressApp: Router; bottle: Bottle }) {
  expressApp.use(makeMiddlewareInvoker(bottle, AuthorizationMiddlewareFactory));
}

function AuthorizationMiddlewareFactory({ authService }: { authService: AuthService }) {
  return async function AuthorizationMiddleware(req: Request, res: Response, next: NextFunction) {
    req.auth = {};

    try {
      const authorization = authService.getAuthorization(req);

      if (!authorization) {
        return next();
      }

      try {
        const token = authService.getToken(authorization);
        authService.verifyInternalToken(token);
        req.auth.isAllAllowed = true;
        return next();
      } catch {}

      const tokenData = await authService.getTokenData(authorization);

      if (!tokenData) {
        return next();
      }

      req.auth.username = tokenData.username;
      req.auth.userId = tokenData.userId;

      next();
    } catch (ex) {
      next();

      // Don't crash the server by propagating an error. Even without authorization, some routes
      // might be accessible. The others will return 401 - but we've logged the error. [wh]
      // next(new Error('Failed to authenticate request, ' + (ex.message ? ex.message : '')));

      // Instead: [wh]
    }
  };
}
