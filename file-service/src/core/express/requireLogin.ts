import { NextFunction, Request, Response } from 'express';
import * as httpErrors from 'http-errors';

export function requireLogin(req: Request, res: Response, next: NextFunction) {
  if (req.auth.isAllAllowed) {
    next();
    return;
  }
  if (!req.auth.userId) {
    throw new httpErrors.Unauthorized('Invalid Authorization');
  }

  next();
}

export function requireLoginOrRedirect(req: Request, res: Response, next: NextFunction) {
  try {
    requireLogin(req, res, next);
  } catch {
    return res.redirect('/');
  }
}
