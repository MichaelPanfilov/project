import * as Knex from 'knex';

import { Config } from './config';

const knex = Knex({
  ...Config.knexConfig,
});

export { knex };
