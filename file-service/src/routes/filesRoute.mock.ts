import * as Bottle from 'bottlejs';
import * as httpErrors from 'http-errors';
import sinon = require('sinon');

import { Config } from '../core/config';
import { FileData } from '../models/file';

export const serviceName = 'FilesRoute';
export const mockServiceName = serviceName + 'Mock';

export function Factory({ bottle }: { bottle: Bottle }) {
  bottle.service(mockServiceName, FilesRouteMock);
}

export class FilesRouteMock {
  static factory(bottle: Bottle) {
    bottle.factory(serviceName, () => {
      const stub = new FilesRouteMock();

      return {
        uploadFile: stub.uploadFile,
        updateFile: stub.updateFile,
        removeFile: stub.removeFile,
        getFiles: stub.getFiles,
        getFileById: stub.getFileById,
        downloadPublicThumbnail: stub.downloadPublicThumbnail,
        downloadPublicFile: stub.downloadPublicFile,
        downloadPrivateThumbnail: stub.downloadPrivateThumbnail,
        downloadPrivateFile: stub.downloadPrivateFile,
      };
    });
  }

  async uploadFile(...args: unknown[]) {
    return sinon.stub().resolves(new FileData())(...args);
  }

  async updateFile(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(() => new httpErrors.NotFound('File not found'));
    stub.withArgs('1').resolves(new FileData());
    return stub(...args);
  }

  async removeFile(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(() => new httpErrors.NotFound('File not found'));
    stub.withArgs('1').resolves('success');
    return stub(...args);
  }

  async getFiles(...args: unknown[]) {
    return sinon.stub().resolves([new FileData()])(...args);
  }

  async getFileById(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(() => new httpErrors.NotFound('File not found'));
    stub.withArgs('1').resolves(new FileData());
    return stub(...args);
  }

  async downloadPublicThumbnail(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(() => new httpErrors.NotFound('File not found'));
    stub.withArgs('1', 'valid').resolves(Config.mockUploadPath + 'mock_thumbnail.png');
    return stub(...args);
  }

  async downloadPublicFile(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(() => new httpErrors.NotFound('File not found'));
    stub.withArgs('1', 'valid').resolves(Config.mockUploadPath + 'mock_file.txt');
    return stub(...args);
  }

  async downloadPrivateThumbnail(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(() => new httpErrors.NotFound('File not found'));
    stub
      .withArgs('1', 'Bearer valid')
      .resolves(Config.mockUploadPath + 'mock_private_thumbnail.png');
    return stub(...args);
  }

  async downloadPrivateFile(...args: unknown[]) {
    const stub = sinon.stub();
    stub.throws(() => new httpErrors.NotFound('File not found'));
    stub.withArgs('1', 'Bearer valid').resolves(Config.mockUploadPath + 'mock_private_file.txt');
    return stub(...args);
  }
}
