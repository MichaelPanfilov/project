import * as Bottle from 'bottlejs';
import * as express from 'express';
import * as fs from 'fs-extra';
import getPort from 'get-port';
import { Server } from 'http';
import * as request from 'supertest';

import { Config } from '../core/config';
import { SpecHelper } from '../helpers/specHelper';
import { FileRepoMock } from '../repos/fileRepo.mock';
import { AuthServiceMock } from '../services/authService.mock';
import { FileServiceMock } from '../services/fileService.mock';

const app = express();

describe('ROUTE: File', () => {
  let bottle: Bottle;
  let server: Server;
  let errorMw;
  let ID: string;

  beforeAll(async () => {
    // DI bottle
    bottle = new Bottle('testBottle');

    SpecHelper.init(bottle, app);

    FileRepoMock.factory(bottle);
    AuthServiceMock.factory(bottle);
    FileServiceMock.factory(bottle);

    // Include route under test
    require('./filesRoute').Factory({ bottle, expressApp: app });

    errorMw = SpecHelper.errorHandler(app);

    fs.copyFileSync(
      Config.mockUploadPath + 'mock_file.txt',
      Config.uploadPath + '/testFilename.txt',
    );
    fs.copyFileSync(
      Config.mockUploadPath + 'mock_thumbnail.png',
      Config.uploadPath + '/test_thumbnail.png',
    );
    fs.copyFileSync(
      Config.mockUploadPath + 'mock_private_thumbnail.png',
      Config.uploadPath + '/test_private_thumbnail.png',
    );

    // Start express
    return new Promise(async (resolve, reject) => {
      try {
        server = app.listen(await getPort(), (err: Error) => {
          if (err) {
            return reject(err);
          }
          resolve();
        });
      } catch (ex) {
        reject(ex);
      }
    });
  });

  afterAll(async () => {
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');

    // Stop server
    return new Promise((resolve, reject) => {
      server.close((err?: Error) => {
        if (err) {
          return reject(err);
        }

        resolve();
      });
    });
  });

  describe('POST /files', () => {
    it('should return uploaded file', async () => {
      await request(app)
        .post('/v1/files')
        .attach('file', Config.mockUploadPath + '/mock_file.txt')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.objectContaining({
                id: jasmine.any(String),
                filename: jasmine.any(String),
                refIds: jasmine.any(Array),
                tags: jasmine.any(Array),
                createdBy: jasmine.any(String),
                size: jasmine.any(Number),
                contentType: jasmine.any(String),
                customData: jasmine.any(String),
                token: jasmine.any(String),
                createdAt: jasmine.any(String),
                updatedAt: jasmine.any(String),
                publicUrl: jasmine.any(String),
                privateUrl: jasmine.any(String),
              }),
              meta: jasmine.any(Object),
            }),
          );

          return res;
        })
        .then(res => (ID = res.body.data.id));
    });

    it('should throw error when wrong file type', async () => {
      await request(app)
        .post('/v1/files')
        .attach('file', Config.mockUploadPath + '/mock_file.bbb')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });

    it('should throw error when big size file', async () => {
      await request(app)
        .post('/v1/files')
        .attach('file', Config.mockUploadPath + '/mock_big_file.txt')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });
  });

  describe('PUT /files/:id', () => {
    it('should return updated file', async () => {
      await request(app)
        .put('/v1/files/1')
        .send({ refIds: '["newRefId"]' })
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.objectContaining({
                id: '1',
                refIds: jasmine.arrayContaining(['newRefId']),
                createdBy: jasmine.any(String),
                size: jasmine.any(Number),
                contentType: jasmine.any(String),
                createdAt: jasmine.any(String),
                updatedAt: jasmine.any(String),
                publicUrl: jasmine.any(String),
                privateUrl: jasmine.any(String),
              }),
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });

    it('should throw error when wrong file id', async () => {
      await request(app)
        .put('/v1/files/99')
        .send({ refIds: 'newRefId' })
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });

    it('should throw error when user has no permissions', async () => {
      await request(app)
        .put('/v1/files/1')
        .send({ refIds: '["need_permission"]', tags: ['documents'] })
        .set('Authorization', 'no_permission')
        .expect(403)
        .then();
    });
  });

  describe('DELETE /files/:id', () => {
    it("should return 'success' when valid fileID", async () => {
      await request(app)
        .delete('/v1/files/' + ID)
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: 'success',
              meta: jasmine.any(Object),
            }),
          );
        })
        .then();
    });

    it('should throw error when wrong fileID', async () => {
      await request(app)
        .delete('/v1/files/99')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });
  });

  describe('GET /files', () => {
    it('valid request should return 200 HTTP status', async () => {
      await request(app)
        .get('/v1/files?refId=string')
        .set('Authorization', 'valid')
        .expect(200)
        .then();
    });

    it('invalid request should throw error', async () => {
      await request(app)
        .get('/v1/files')
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });

    it('should return 2 files by valid tag', async () => {
      await request(app)
        .get('/v1/files?refId=string&tag=string')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body.data.length).toEqual(2);
        })
        .then();
    });
    it('should return 0 files by invalid tag', async () => {
      await request(app)
        .get('/v1/files?refId=string&tag=invalid')
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body.data.length).toEqual(0);
        })
        .then();
    });
  });

  describe('GET /files/:id', () => {
    it('should return file when correct fileID', async () => {
      await request(app)
        .get('/v1/files/' + ID)
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.body).toEqual(
            jasmine.objectContaining({
              data: jasmine.objectContaining({
                id: '1',
                refIds: jasmine.any(Array),
                createdBy: jasmine.any(String),
                size: jasmine.any(Number),
                contentType: jasmine.any(String),
                createdAt: jasmine.any(String),
                updatedAt: jasmine.any(String),
                publicUrl: jasmine.any(String),
                privateUrl: jasmine.any(String),
              }),
              meta: jasmine.any(Object),
            }),
          );
        });
    });

    it('should throw error when wrong fileID', async () => {
      await request(app)
        .get('/v1/files/99')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });

    it('should throw error when user has no permissions', async () => {
      await request(app)
        .get('/v1/files/2')
        .set('Authorization', 'no_permission')
        .expect(403)
        .then();
    });
  });

  describe('GET /files/:id/thumbnail/:token', () => {
    it('valid request should return 200 HTTP status', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail/valid`)
        .expect(200)
        .then();
    });

    it('should throw error when wrong fileID', async () => {
      await request(app)
        .get('/v1/files/99/thumbnail/valid')
        .expect(404)
        .then();
    });

    it('should throw error when wrong token', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail/invalid`)
        .expect(403)
        .then();
    });

    it('should throw error when negative params', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail/valid?width=-1`)
        .expect(400)
        .then();
    });

    it('should throw error when too large params', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail/valid?height=${Config.maxThumbnailSize + 1}`)
        .expect(400)
        .then();
    });

    it('valid request should return valid headers', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail/valid?width=40&height=60&fit=cover`)
        .expect(res => {
          expect(res.header['content-disposition']).toEqual('inline; filename="foo_thumb.png"');
          expect(res.header['content-type']).toEqual('image/png');
        })
        .then();
    });
  });

  describe('GET /files/:id/download/:token', () => {
    it('valid request should return 200 HTTP status', async () => {
      await request(app)
        .get(`/v1/files/${ID}/download/valid`)
        .expect(200)
        .then();
    });

    it('should throw error when wrong fileID', async () => {
      await request(app)
        .get('/v1/files/99/download/valid')
        .expect(404)
        .then();
    });

    it('should throw error when wrong token', async () => {
      await request(app)
        .get(`/v1/files/${ID}/download/invalid`)
        .expect(403)
        .then();
    });
    // todo HTTP Range tests
  });

  describe('GET /files/:id/thumbnail', () => {
    it('valid request should return 200 HTTP status', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail`)
        .set('Authorization', 'valid')
        .expect(200)
        .then();
    });

    it('should throw error when wrong fileID', async () => {
      await request(app)
        .get('/v1/files/99/thumbnail')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });

    it('should throw error when wrong token in header', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail`)
        .set('Authorization', 'invalid')
        .expect('Location', /\//)
        .then();
    });

    it('should throw error when negative params', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail?width=-1`)
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });

    it('should throw error when too large params', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail?height=${Config.maxThumbnailSize + 1}`)
        .set('Authorization', 'valid')
        .expect(400)
        .then();
    });

    it('valid request should return valid headers', async () => {
      await request(app)
        .get(`/v1/files/${ID}/thumbnail?width=40&height=60&fit=cover`)
        .set('Authorization', 'valid')
        .expect(res => {
          expect(res.header['content-disposition']).toEqual('inline; filename="foo_thumb.png"');
          expect(res.header['content-type']).toEqual('image/png');
        })
        .then();
    });
  });

  describe('GET /files/:id/download', () => {
    it('valid request should return 200 HTTP status', async () => {
      await request(app)
        .get(`/v1/files/${ID}/download`)
        .set('Authorization', 'valid')
        .expect(200)
        .then();
    });

    it('should throw error when wrong fileID', async () => {
      await request(app)
        .get('/v1/files/99/download')
        .set('Authorization', 'valid')
        .expect(404)
        .then();
    });

    it('should throw error when wrong token in header', async () => {
      await request(app)
        .get(`/v1/files/${ID}/download`)
        .set('Authorization', 'invalid')
        .expect('Location', /\//)
        .then();
    });

    it('should throw error when user has no permissions', async () => {
      await request(app)
        .get('/v1/files/2/download')
        .set('Authorization', 'no_permission')
        .expect(403)
        .then();
    });
    // todo HTTP Range tests
  });
});
