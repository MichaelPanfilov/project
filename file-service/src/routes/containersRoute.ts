import { AclRight, ContentTypeGroup, DOCUMENT_FILE_TAG, getExtension } from '@common';
import { Logger, LogService } from '@elunic/logger';
import * as Bottle from 'bottlejs';
import { makeRouteInvoker } from 'bottlejs-express';
import busboy from 'busboy-express';
import { Response, Router } from 'express';
import * as fs from 'fs';
import * as Joi from 'joi';
import ow from 'ow';
import * as path from 'path';

import { Config } from '../core/config';
import { requireLogin, requireLoginOrRedirect } from '../core/express/requireLogin';
import { downloadWithRange } from '../helpers/downloadWithRange';
import { arrayIncludes, getContentType } from '../helpers/filesHelpers';
import { handleError } from '../helpers/handleError';
import { readRangeHeader } from '../helpers/readRangeHeader';
import { sendResponse } from '../helpers/sendResponse';
import { Container } from '../models/container';
import { FileData } from '../models/file';
import { ContainerRepo } from '../repos/containerRepo';
import { AuthService } from '../services/authService';
import { FileService } from '../services/fileService';
import { AuthenticatedRequest } from '../types';

export interface ExtendedContainer extends Container {
  files: FileData[];
}

export function Factory({ bottle, expressApp }: { bottle: Bottle; expressApp: Router }) {
  const routeInvoker = makeRouteInvoker(
    bottle,
    ContainersRoute,
    'log',
    'ContainerRepo',
    'fileService',
    'authService',
  );

  expressApp.post(
    '/v1/versions/:id/new',
    [requireLogin, busboy({ files: ['file'], multipartOnly: false })],
    routeInvoker('updateVersion'),
  );
  expressApp.get('/v1/versions/:id', requireLogin, routeInvoker('getLatestVersion'));
  expressApp.get('/v1/versions/:id/list', requireLogin, routeInvoker('getAllVersions'));
  expressApp.get('/v1/versions', requireLogin, routeInvoker('getLatestVersions'));
  expressApp.get('/v1/versions/:id/qr-code', requireLogin, routeInvoker('getLatestVersionQRCode'));

  expressApp.get(
    '/v1/versions/:id/thumbnail/:token',
    routeInvoker('downloadVersionPublicThumbnail'),
  );
  expressApp.get(
    '/v1/versions/:id/thumbnail',
    requireLoginOrRedirect,
    routeInvoker('downloadVersionPrivateThumbnail'),
  );
  expressApp.get('/v1/versions/:id/download/:token', routeInvoker('downloadVersionPublicFile'));
  expressApp.get(
    '/v1/versions/:id/download',
    requireLoginOrRedirect,
    routeInvoker('downloadVersionPrivateFile'),
  );
}

class ContainersRoute {
  private logger: Logger;

  constructor(
    private log: LogService,
    private containerRepo: ContainerRepo,
    private fileService: FileService,
    private authService: AuthService,
  ) {
    this.logger = this.log.createLogger('ContainerRoute');
  }

  async updateVersion(req: AuthenticatedRequest, res: Response) {
    ow(req.files, 'files', ow.object.nonEmpty.hasKeys('file'));

    const oldId = Joi.attempt(req.params.id, Joi.string(), 'Id must be a string');

    await this.authService.requireRights(req, [{ resourceId: 'Document', rightKey: 'Update' }]);

    const file = req.files.file[0];

    try {
      const updated = await this.containerRepo.addVersion(oldId, file);

      res.json({
        data: {
          ...updated,
          ...updated.parsableData,
        },
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async getLatestVersion(req: AuthenticatedRequest, res: Response) {
    const containerId = Joi.attempt(req.params.id, Joi.string(), 'Id must be a string');

    await this.authService.requireRights(req, [{ resourceId: 'Document', rightKey: 'Read' }]);

    try {
      const version = await this.containerRepo.getLatestVersion(containerId);

      res.json({
        data: (version as FileData).withFullUrls(req),
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async getAllVersions(req: AuthenticatedRequest, res: Response) {
    const containerId = Joi.attempt(req.params.id, Joi.string(), 'Id must be a string');

    await this.authService.requireRights(req, [{ resourceId: 'Document', rightKey: 'Read' }]);

    try {
      const container = await this.containerRepo.getAllVersions(containerId);

      res.json({
        data: (container as ExtendedContainer).files,
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async getLatestVersions(req: AuthenticatedRequest, res: Response) {
    const filter = Joi.attempt(
      req.query,
      Joi.object()
        .keys({
          id: [Joi.array().items([Joi.string()]), Joi.string()],
          title: Joi.string(),
          refId: [Joi.array().items([Joi.string()]), Joi.string()],
          tag: [Joi.array().items([Joi.string()]), Joi.string()],
          contentType: [Joi.array().items([Joi.string()]), Joi.string()],
          machineTypeTag: [Joi.array().items([Joi.string()]), Joi.string()],
          contentTypeGroup: [
            Joi.array().items([Joi.string().valid(...Object.values(ContentTypeGroup))]),
            Joi.string().valid(...Object.values(ContentTypeGroup)),
          ],
          limit: Joi.number(),
          page: Joi.number(),
          offset: Joi.number(),
          order: Joi.string(),
        })
        .or('refId', 'tag'),
      'Incorrect filter',
    );

    await this.authService.requireRights(req, [{ resourceId: 'Document', rightKey: 'Read' }]);

    const { files, meta } = await this.containerRepo.getLatestFiles(filter);

    res.json({
      data: files.map(file => ({
        ...file.withFullUrls(req),
        containerId: file.containerId,
        version: file.version,
      })),
      meta,
    });
  }

  async getLatestVersionQRCode(req: AuthenticatedRequest, res: Response) {
    const containerId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');
    try {
      const file = await this.containerRepo.getLatestVersion(containerId);
      const { publicUrl } = this.fileService.getUrlSet(
        file.id,
        file.token,
        req.protocol,
        req.get('host'),
      );
      const code = await this.fileService.createQRCode(publicUrl);
      res.send(code);
    } catch (e) {
      handleError(e);
    }
  }

  async downloadVersionPublicThumbnail(req: AuthenticatedRequest, res: Response) {
    const containerId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');
    const token = Joi.attempt(req.params.token, Joi.string(), 'Token must be a string');

    const { width, height, fit } = Joi.attempt(
      req.query,
      Joi.object().keys({
        width: Joi.number()
          .positive()
          .integer()
          .max(Config.maxThumbnailSize),
        height: Joi.number()
          .positive()
          .integer()
          .max(Config.maxThumbnailSize),
        fit: Joi.string(),
      }),
      'Missing query',
    );

    let realFilename!: string;
    let file!: FileData;

    try {
      file = await this.containerRepo.getLatestVersion(containerId);
      realFilename = await this.fileService.getFilenameById(file.id);
      await this.fileService.verifyFileToken(file.id, token);
    } catch (e) {
      handleError(e);
    }

    const stream = this.fileService.createThumbnail(
      realFilename,
      Number(width),
      Number(height),
      fit as string,
    );

    const storedFilename = `${path.parse(file.filename).name}_thumb.png`;

    const headers = {
      ['Content-Disposition']: `inline; filename="${storedFilename}"`,
      ['Content-Type']: getContentType('png'),
    };

    res.writeHead(200, headers);
    stream.pipe(res);
  }

  async downloadVersionPrivateThumbnail(req: AuthenticatedRequest, res: Response) {
    const containerId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');

    const { width, height, fit } = Joi.attempt(
      req.query,
      Joi.object().keys({
        width: Joi.number()
          .positive()
          .integer()
          .max(Config.maxThumbnailSize),
        height: Joi.number()
          .positive()
          .integer()
          .max(Config.maxThumbnailSize),
        fit: Joi.string(),
      }),
      'Missing query',
    );

    let realFilename!: string;
    let file!: FileData;

    try {
      file = await this.containerRepo.getLatestVersion(containerId);
      realFilename = await this.fileService.getFilenameById(file.id);
    } catch (e) {
      handleError(e);
    }

    await this.requireDocAuth(req, 'Read', file.tags);

    const stream = this.fileService.createThumbnail(
      realFilename,
      Number(width),
      Number(height),
      fit as string,
    );

    const storedFilename = `${path.parse(file.filename).name}_thumb.png`;

    const headers = {
      ['Content-Disposition']: `inline; filename="${storedFilename}"`,
      ['Content-Type']: getContentType('png'),
    };

    res.writeHead(200, headers);
    stream.pipe(res);
  }

  async downloadVersionPublicFile(req: AuthenticatedRequest, res: Response) {
    const containerId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');
    const token = Joi.attempt(req.params.token, Joi.string(), 'Token must be a string');

    let realFilename!: string;
    let file!: FileData;

    try {
      file = await this.containerRepo.getLatestVersion(containerId);

      await this.fileService.verifyFileToken(file.id, token);

      realFilename = await this.fileService.getFilenameById(file.id);
    } catch (e) {
      handleError(e);
    }

    const filePath = path.join(Config.uploadPath, realFilename);
    const storedFilename = file.filename;

    const stat = fs.statSync(filePath);
    const rangeRequest = readRangeHeader(req.headers.range as string, stat.size);

    if (rangeRequest) {
      downloadWithRange(res, storedFilename, realFilename, stat.size, rangeRequest, filePath);
    } else {
      const headers = {
        ['Content-Disposition']: `attachment; filename="${storedFilename}"`,
        ['Content-Length']: stat.size,
        ['Content-Type']: getContentType(getExtension(realFilename)),
      };

      sendResponse(
        res,
        200,
        headers,
        fs.createReadStream(path.join(Config.uploadPath, realFilename)),
      );
    }
  }

  async downloadVersionPrivateFile(req: AuthenticatedRequest, res: Response) {
    const containerId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');

    let realFilename!: string;
    let file!: FileData;

    try {
      file = await this.containerRepo.getLatestVersion(containerId);
      realFilename = await this.fileService.getFilenameById(file.id);
    } catch (e) {
      handleError(e);
    }

    await this.requireDocAuth(req, 'Read', file.tags);

    const filePath = path.join(Config.uploadPath, realFilename);
    const storedFilename = file.filename;

    const stat = fs.statSync(filePath);
    const rangeRequest = readRangeHeader(req.headers.range as string, stat.size);

    if (rangeRequest) {
      downloadWithRange(res, storedFilename, realFilename, stat.size, rangeRequest, filePath);
    } else {
      const headers = {
        ['Content-Disposition']: `attachment; filename="${storedFilename}"`,
        ['Content-Length']: stat.size,
        ['Content-Type']: getContentType(getExtension(realFilename)),
      };

      sendResponse(
        res,
        200,
        headers,
        fs.createReadStream(path.join(Config.uploadPath, realFilename)),
      );
    }
  }

  private async requireDocAuth(
    req: AuthenticatedRequest,
    rightKey: AclRight,
    tags?: string | string[],
  ) {
    if (arrayIncludes(DOCUMENT_FILE_TAG, tags)) {
      await this.authService.requireRights(req, [{ resourceId: 'Document', rightKey }]);
    }
  }
}
