import { AclRight, ContentTypeGroup, DOCUMENT_FILE_TAG, getExtension } from '@common';
import { Logger, LogService } from '@elunic/logger';
import * as Bottle from 'bottlejs';
import { makeRouteInvoker } from 'bottlejs-express';
import busboy from 'busboy-express';
import { Response, Router } from 'express';
import * as fs from 'fs';
import * as Joi from 'joi';
import ow from 'ow';
import * as path from 'path';

import { Config } from '../core/config';
import { requireLogin, requireLoginOrRedirect } from '../core/express/requireLogin';
import { downloadWithRange } from '../helpers/downloadWithRange';
import { arrayIncludes, getContentType } from '../helpers/filesHelpers';
import { handleError } from '../helpers/handleError';
import { readRangeHeader } from '../helpers/readRangeHeader';
import { sendResponse } from '../helpers/sendResponse';
import { FileData } from '../models/file';
import { FileRepo } from '../repos/fileRepo';
import { AuthService } from '../services/authService';
import { FileService } from '../services/fileService';
import { AuthenticatedRequest, CreateFileData } from '../types';

export function Factory({ bottle, expressApp }: { bottle: Bottle; expressApp: Router }) {
  const routeInvoker = makeRouteInvoker(
    bottle,
    FilesRoute,
    'log',
    'FileRepo',
    'fileService',
    'authService',
  );

  expressApp.post(
    '/v1/files',
    [
      requireLogin,
      busboy({
        files: ['file'],
        fields: ['title', 'refIds', 'tags', 'machineTypeTags'],
        multipartOnly: false,
      }),
    ],
    routeInvoker('uploadFile'),
  );
  expressApp.put('/v1/files/:id', requireLogin, routeInvoker('updateFile'));
  expressApp.delete('/v1/files/:id', requireLogin, routeInvoker('removeFile'));
  expressApp.get('/v1/files', requireLogin, routeInvoker('getFiles'));
  expressApp.get('/v1/files/:id', requireLogin, routeInvoker('getFileById'));
  expressApp.get('/v1/files/:id/view', requireLoginOrRedirect, routeInvoker('viewFile'));
  expressApp.get('/v1/files/:id/thumbnail/:token', routeInvoker('downloadPublicThumbnail'));
  expressApp.get('/v1/files/:id/download/:token', routeInvoker('downloadPublicFile'));
  expressApp.get(
    '/v1/files/:id/thumbnail',
    requireLoginOrRedirect,
    routeInvoker('downloadPrivateThumbnail'),
  );
  expressApp.get(
    '/v1/files/:id/download',
    requireLoginOrRedirect,
    routeInvoker('downloadPrivateFile'),
  );
  expressApp.get('/v1/files/:id/qr-code', requireLogin, routeInvoker('getQRCode'));
}

class FilesRoute {
  private logger: Logger;

  constructor(
    private log: LogService,
    private fileRepo: FileRepo,
    private fileService: FileService,
    private authService: AuthService,
  ) {
    this.logger = this.log.createLogger('LoginRoute');
  }

  async getQRCode(req: AuthenticatedRequest, res: Response) {
    const fileId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');

    try {
      const { token } = await this.fileRepo.getById(fileId);
      const { privateUrl } = this.fileService.getUrlSet(
        fileId,
        token,
        req.protocol,
        req.get('host'),
      );
      const code = await this.fileService.createQRCode(privateUrl);

      res.send({
        data: { code },
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async uploadFile(req: AuthenticatedRequest, res: Response) {
    ow(req.files, 'files', ow.object.nonEmpty.hasKeys('file'));

    const { title, refIds, tags, machineTypeTags } = Joi.attempt<CreateFileData>(
      req.fields,
      Joi.object().keys({
        title: [Joi.array().items([Joi.string()]), Joi.string()], // title is a signle value but busboy get it from formdata as array
        refIds: [Joi.array().items([Joi.string()]), Joi.string()],
        tags: [Joi.array().items([Joi.string()]), Joi.string()],
        machineTypeTags: Joi.array().items([Joi.string()]),
      }),
      'Incorrect query params',
    );

    // const rights = [{ resourceId: 'Document', rightKey: 'Create' }];

    /**
     * complex file access isn't tested and implemented yet. There must be a bridge for deactivating
     */
    // if (refIds) {
    //   rights.push({ resourceId: refIds, rightKey: 'create_file' });
    // }

    // if (tags instanceof Array) {
    //   tags.forEach((tag: string) =>
    //     rights.push({ resourceId: refIds, rightKey: `create_${tag}_file` }),
    //   );
    // } else if (tags instanceof String) {
    //   rights.push({ resourceId: refIds, rightKey: `create_${tags}_file` });
    // }

    await this.requireDocAuth(req, 'Create', tags);

    const file = req.files.file[0];

    try {
      const storedFile = await this.fileRepo.save(file, {
        title,
        refIds,
        tags,
        machineTypeTags,
        uploaderId: req.auth ? req.auth.userId : undefined,
      });

      const token = this.fileService.createFileToken(storedFile.id);

      await this.fileRepo.saveToken(storedFile.id, token);
      await this.fileService.saveFile(storedFile.id, file);

      const { fileServiceUrl, relativePublicUrl, publicUrl } = this.fileService.getUrlSet(
        storedFile.id,
        token,
        req.protocol,
        req.get('host'),
      );

      const relativePrivateUrl = `/v1/files/${storedFile.id}/download`;

      const privateUrl = `${fileServiceUrl}${relativePrivateUrl}`;
      const relativeThumbnailPublicUrl = `/v1/files/${storedFile.id}/thumbnail/${token}`;
      const relativeThumbnailPrivateUrl = `/v1/files/${storedFile.id}/thumbnail`;
      const relativeViewUrl = `/v1/files/${storedFile.id}/view`;
      const thumbnailPublicUrl = `${fileServiceUrl}${relativeThumbnailPublicUrl}`;
      const thumbnailPrivateUrl = `${fileServiceUrl}${relativeThumbnailPrivateUrl}`;
      const viewUrl = `${fileServiceUrl}${relativeViewUrl}`;

      res.json({
        data: Object.assign(storedFile, {
          ...storedFile.parsableData,
          relativePublicUrl,
          relativePrivateUrl,
          publicUrl,
          privateUrl,
          relativeThumbnailPublicUrl,
          relativeThumbnailPrivateUrl,
          thumbnailPublicUrl,
          thumbnailPrivateUrl,
          relativeViewUrl,
          viewUrl,
        }),
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async updateFile(req: AuthenticatedRequest, res: Response) {
    const fileId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');

    const { filename, refIds, tags, machineTypeTags, customData, title } = Joi.attempt(
      req.body,
      Joi.object().keys({
        filename: Joi.string(),
        refIds: [Joi.array().items([Joi.string()]), Joi.string()],
        tags: [Joi.array().items([Joi.string()]), Joi.string()],
        machineTypeTags: [Joi.array().items([Joi.string()]), Joi.string()],
        customData: Joi.object(),
        title: Joi.string(),
      }),
      'Incorrect request body',
    );

    // const rights = [{ resourceId: 'files', rightKey: 'create_file' }];

    // if (refIds) {
    //   rights.push({ resourceId: refIds, rightKey: 'create_file' });
    // }

    // if (tags) {
    //   tags.forEach((tag: string) =>
    //     rights.push({ resourceId: refIds, rightKey: `create_${tag}_file` }),
    //   );
    // }

    const currentFile = await this.fileRepo.getById(fileId);
    await this.requireDocAuth(req, 'Update', currentFile.tags);

    try {
      const updatedFile = await this.fileRepo.update(fileId, {
        filename,
        refIds,
        tags,
        machineTypeTags,
        customData,
        title,
      });

      res.json({
        data: updatedFile.withFullUrls(req),
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async removeFile(req: AuthenticatedRequest, res: Response) {
    const fileId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');

    const currentFile = await this.fileRepo.getById(fileId);
    await this.requireDocAuth(req, 'Delete', currentFile.tags);

    try {
      await this.fileRepo.delete(fileId);
      await this.fileService.deleteFile(fileId);

      res.json({
        data: 'success',
        meta: {},
      });
    } catch (e) {
      handleError(e);
    }
  }

  async getFiles(req: AuthenticatedRequest, res: Response) {
    const filter = Joi.attempt(
      req.query,
      Joi.object()
        .keys({
          id: [Joi.array().items([Joi.string()]), Joi.string()],
          title: Joi.string(),
          refId: [Joi.array().items([Joi.string()]), Joi.string()],
          tag: [Joi.array().items([Joi.string()]), Joi.string()],
          machineTypeTag: [Joi.array().items([Joi.string()]), Joi.string()],
          contentType: Joi.string(),
          contentTypeGroup: [
            Joi.array().items([Joi.string().valid(...Object.values(ContentTypeGroup))]),
            Joi.string().valid(...Object.values(ContentTypeGroup)),
          ],
          limit: Joi.number(),
          page: Joi.number(),
          offset: Joi.number(),
          order: Joi.string(),
        })
        .or('refId', 'tag'),
      'Incorrect filter',
    );

    // Check first if for given tags, one requires authentication.
    // This fill throw, so we return feedback for not allowed tag querying.
    await this.requireDocAuth(req, 'Read', filter.tags as string[]);

    const { files, meta } = await this.fileRepo.getFilesByFilter(filter);

    // Now check for every file again if its tag requires authentication.
    // TODO: This breaks pagination.
    const tuples = await Promise.all(
      files.map(async f => {
        const auth = await this.requireDocAuth(req, 'Read', f.parsableData.tags)
          .then(() => true)
          .catch(() => false);
        return [f, auth] as [FileData, boolean];
      }),
    );

    res.json({
      data: tuples.filter(([, auth]) => auth).map(([file]) => file.withFullUrls(req)),
      meta,
    });
  }

  async getFileById(req: AuthenticatedRequest, res: Response) {
    const fileId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');

    let file!: FileData;

    try {
      file = await this.fileRepo.getById(fileId);
    } catch (e) {
      handleError(e);
    }

    // const rights = [{ resourceId: 'files', rightKey: 'read' }];

    // if (file.refIds) {
    //   rights.push({ resourceId: file.refIds, rightKey: 'read_files' });

    //   if (file.tags) {
    //     JSON.parse(file.tags).forEach((tag: string) =>
    //       // @ts-ignore it is strange but ts doesn't understand what if I check if (file.refIds), refIds cannot be undefined...
    //       rights.push({ resourceId: file.refIds, rightKey: `read_${tag}_files` }),
    //     );
    //   }
    // }

    await this.requireDocAuth(req, 'Read', file.tags);

    res.json({
      data: file.withFullUrls(req),
      meta: {},
    });
  }

  async downloadPublicThumbnail(req: AuthenticatedRequest, res: Response) {
    const fileId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');
    const token = Joi.attempt(req.params.token, Joi.string(), 'Token must be a string');

    const { width, height, fit } = Joi.attempt(
      req.query,
      Joi.object().keys({
        width: Joi.number()
          .positive()
          .integer()
          .max(Config.maxThumbnailSize),
        height: Joi.number()
          .positive()
          .integer()
          .max(Config.maxThumbnailSize),
        fit: Joi.string(),
      }),
      'Missing query',
    );

    await this.fileService.verifyFileToken(fileId, token);

    let realFilename!: string;
    let file!: FileData;

    try {
      realFilename = await this.fileService.getFilenameById(fileId);
      file = await this.fileRepo.getById(fileId);
    } catch (e) {
      handleError(e);
    }

    const stream = this.fileService.createThumbnail(
      realFilename,
      Number(width),
      Number(height),
      fit as string,
    );

    const storedFilename = `${path.parse(file.filename).name}_thumb.png`;

    const headers = {
      ['Content-Disposition']: `inline; filename="${storedFilename}"`,
      ['Content-Type']: getContentType('png'),
    };

    res.writeHead(200, headers);
    stream.pipe(res);
  }

  async downloadPublicFile(req: AuthenticatedRequest, res: Response) {
    const fileId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');
    const token = Joi.attempt(req.params.token, Joi.string(), 'Token must be a string');

    await this.fileService.verifyFileToken(fileId, token);

    let realFilename!: string;
    let file!: FileData;

    try {
      realFilename = await this.fileService.getFilenameById(fileId);
      file = await this.fileRepo.getById(fileId);
    } catch (e) {
      handleError(e);
    }

    const filePath = path.join(Config.uploadPath, realFilename);
    const storedFilename = file.filename;

    const stat = fs.statSync(filePath);
    const rangeRequest = readRangeHeader(req.headers.range as string, stat.size);

    if (rangeRequest) {
      downloadWithRange(res, storedFilename, realFilename, stat.size, rangeRequest, filePath);
    } else {
      const headers = {
        ['Content-Disposition']: `attachment; filename="${storedFilename}"`,
        ['Content-Length']: stat.size,
        ['Content-Type']: getContentType(getExtension(realFilename)),
      };

      sendResponse(
        res,
        200,
        headers,
        fs.createReadStream(path.join(Config.uploadPath, realFilename)),
      );
    }
  }

  async downloadPrivateThumbnail(req: AuthenticatedRequest, res: Response) {
    const fileId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');

    const { width, height, fit } = Joi.attempt(
      req.query,
      Joi.object().keys({
        width: Joi.number()
          .positive()
          .integer()
          .max(Config.maxThumbnailSize),
        height: Joi.number()
          .positive()
          .integer()
          .max(Config.maxThumbnailSize),
        fit: Joi.string(),
      }),
      'Missing query',
    );

    let realFilename!: string;
    let file!: FileData;

    try {
      realFilename = await this.fileService.getFilenameById(fileId);
      file = await this.fileRepo.getById(fileId);
    } catch (e) {
      handleError(e);
    }

    await this.requireDocAuth(req, 'Read', file.tags);

    const stream = this.fileService.createThumbnail(
      realFilename,
      Number(width),
      Number(height),
      fit as string,
    );

    const storedFilename = `${path.parse(file.filename).name}_thumb.png`;

    const headers = {
      ['Content-Disposition']: `inline; filename="${storedFilename}"`,
      ['Content-Type']: getContentType('png'),
    };

    res.writeHead(200, headers);
    stream.pipe(res);
  }

  async downloadPrivateFile(req: AuthenticatedRequest, res: Response) {
    const fileId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');

    let realFilename!: string;
    let file!: FileData;

    try {
      realFilename = await this.fileService.getFilenameById(fileId);
      file = await this.fileRepo.getById(fileId);
    } catch (e) {
      handleError(e);
    }

    await this.requireDocAuth(req, 'Read', file.tags);

    const filePath = path.join(Config.uploadPath, realFilename);
    const storedFilename = file.filename;

    const stat = fs.statSync(filePath);
    const rangeRequest = readRangeHeader(req.headers.range as string, stat.size);

    if (rangeRequest) {
      downloadWithRange(res, storedFilename, realFilename, stat.size, rangeRequest, filePath);
    } else {
      const headers = {
        ['Content-Disposition']: `attachment; filename="${storedFilename}"`,
        ['Content-Length']: stat.size,
        ['Content-Type']: getContentType(getExtension(realFilename)),
      };

      sendResponse(
        res,
        200,
        headers,
        fs.createReadStream(path.join(Config.uploadPath, realFilename)),
      );
    }
  }

  async viewFile(req: AuthenticatedRequest, res: Response) {
    const fileId = Joi.attempt(req.params.id, Joi.string(), 'FileId must be a string');

    let realFilename!: string;
    let file!: FileData;

    try {
      realFilename = await this.fileService.getFilenameById(fileId);
      file = await this.fileRepo.getById(fileId);
    } catch (e) {
      handleError(e);
    }

    await this.requireDocAuth(req, 'Read', file.tags);

    const storedFilename = file.filename;

    const headers = {
      ['Content-Disposition']: `inline; filename="${storedFilename}"`,
      ['Content-Type']: getContentType(getExtension(realFilename)),
    };

    res.sendFile(realFilename, {
      root: Config.uploadPath,
      headers,
    });
  }

  private async requireDocAuth(
    req: AuthenticatedRequest,
    rightKey: AclRight,
    tags?: string | string[],
  ) {
    if (arrayIncludes(DOCUMENT_FILE_TAG, tags)) {
      await this.authService.requireRights(req, [{ resourceId: 'Document', rightKey }]);
    }
  }
}
