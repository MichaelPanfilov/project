import * as Bottle from 'bottlejs';
import * as Express from 'express';
import { join } from 'path';

const yamljs = require('yamljs');
const swaggerUi = require('swagger-ui-express');

export function Factory({ bottle, expressApp }: { bottle: Bottle; expressApp: Express.Router }) {
  const swaggerFilePath: string = join(__dirname, '../../../../swagger/swagger.yaml');
  const swaggerDocument = yamljs.load(swaggerFilePath);
  expressApp.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
}
