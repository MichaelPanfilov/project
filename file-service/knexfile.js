require('dotenv').config({ path: '.env' });

module.exports = {
  development: {
    client: 'mysql2',
    version: '5.7',
    connection: {
      host: process.env.APP_DB_HOST_FILE || process.env.APP_DB_HOST,
      user: process.env.APP_DB_USER,
      password: process.env.APP_DB_PASS,
      database: process.env.APP_DB_NAME,
    },
    seeds: {
      directory: './seeds',
    },
    useNullAsDefault: true,
  },

  testing: {
    client: 'mysql2',
    version: '5.7',
    connection: {
      host:
        process.env.APP_TEST_DB_HOST_FILE ||
        process.env.APP_TEST_DB_HOST ||
        process.env.APP_DB_HOST,
      user: process.env.APP_TEST_DB_USER,
      password: process.env.APP_TEST_DB_PASS,
      database: process.env.APP_TEST_DB_NAME,
    },
    seeds: {
      directory: './seeds',
    },
    useNullAsDefault: true,
  },

  staging: {
    client: 'mysql2',
    version: '5.7',
    connection: {
      host: process.env.APP_DB_HOST,
      user: process.env.APP_DB_USER,
      password: process.env.APP_DB_PASS,
      database: process.env.APP_DB_NAME,
    },
    seeds: {
      directory: './seeds',
    },
    useNullAsDefault: true,
  },

  production: {
    client: 'mysql2',
    version: '5.7',
    connection: {
      host: process.env.APP_DB_HOST,
      user: process.env.APP_DB_USER,
      password: process.env.APP_DB_PASS,
      database: process.env.APP_DB_NAME,
    },
    seeds: {
      directory: './seeds',
    },
    useNullAsDefault: true,
  },
};
