exports.up = async function(knex, Promise) {
  await knex.schema.alterTable('files', function(t) {
    // Enums are ordered by their index which was done in previous migration.
    // The sorting here fixes this so that alphabetical sorting works on sql query.
    t.enum('content_type_group', ['image', 'video', 'pdf', 'other'].sort())
      .defaultTo('other')
      .alter();
  });
};

exports.down = function(knex, Promise) {};
