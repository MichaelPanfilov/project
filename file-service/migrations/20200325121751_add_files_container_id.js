exports.up = function(knex, Promise) {
  return knex.schema.alterTable('files', table => {
    table.string('container_id');
    table
      .foreign('container_id')
      .references('id')
      .inTable('containers');
    table.integer('version');
  });
};

exports.down = function(knex, Promise) {
  return Promise.all(
    ['container_id', 'version'].map(name => knex.schema.hasColumn('files', name)),
  ).then(([isContainer, isVersion]) => {
    return knex.schema.alterTable('files', table => {
      if (isContainer) {
        table.dropForeign('container_id');
        table.dropColumn('container_id');
      }
      if (isVersion) {
        table.dropColumn('version');
      }
    });
  });
};
