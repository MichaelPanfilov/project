exports.up = async function(knex, Promise) {
  const files = await knex('files')
    .select()
    .whereNotNull('machine_type_tags');

  const sortTags = tags => {
    if (!tags) {
      return tags;
    }
    try {
      const parsed = JSON.parse(tags);
      const sorted = parsed.sort((a, b) => {
        if (a.toLowerCase() < b.toLowerCase()) {
          return -1;
        }
        if (a.toLowerCase() > b.toLowerCase()) {
          return 1;
        }
        return 0;
      });
      return JSON.stringify(sorted);
    } catch (e) {
      return tags;
    }
  };

  return knex.transaction(trx => {
    const queries = files.map(file =>
      knex('files')
        .where('id', file.id)
        .update({ ...file, machine_type_tags: sortTags(file.machine_type_tags) })
        .transacting(trx),
    );
    return Promise.all(queries)
      .then(trx.commit)
      .catch(trx.rollback);
  });
};

exports.down = async function(knex, Promise) {};
