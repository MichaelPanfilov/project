exports.up = async function(knex, Promise) {
  const isExist = await knex.schema.hasTable('files');
  if (!isExist) {
    return knex.schema.createTable('files', function(table) {
      table.string('id').primary();
      table.string('filename').notNullable();
      table.string('title');
      table.text('ref_ids');
      table.text('tags');
      table.text('token');
      table.string('created_by');
      table.bigInteger('size');
      table.string('content_type');
      table.text('custom_data');
      table.text('machine_type_tags');
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
    });
  }
  return knex.schema.alterTable('files', function(table) {
    table
      .string('filename')
      .notNullable()
      .alter();
    table.string('title').alter();
    table.text('ref_ids').alter();
    table.text('tags').alter();
    table.text('token').alter();
    table.string('created_by').alter();
    table.bigInteger('size').alter();
    table.string('content_type').alter();
    table.text('custom_data').alter();
    table.text('machine_type_tags').alter();
    table
      .timestamp('created_at')
      .defaultTo(knex.fn.now())
      .alter();
    table
      .timestamp('updated_at')
      .defaultTo(knex.fn.now())
      .alter();
  });
};

exports.down = async function(knex, Promise) {
  const isExist = await knex.schema.hasTable('files');
  if (isExist) {
    await knex.schema.dropTable('files');
  }
};
