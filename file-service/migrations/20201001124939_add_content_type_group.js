const DOC_TYPES = {
  image: [
    'image/apng',
    'image/bmp',
    'image/gif',
    'image/x-icon',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
    'image/tiff',
    'image/webp',
    'image/svg',
  ],
  video: ['video/3gpp', 'video/mpeg', 'video/mp4', 'video/ogg', 'video/quicktime', 'video/webm'],
  pdf: ['application/pdf'],
};

exports.up = async function(knex, Promise) {
  await knex.schema.alterTable('files', function(t) {
    t.enum('content_type_group', ['image', 'video', 'pdf', 'other']).defaultTo('other');
  });

  const getContentTypeGroup = file =>
    Object.keys(DOC_TYPES).find(k => {
      const arr = DOC_TYPES[k];
      return arr ? arr.includes(file.content_type) : false;
    }) || 'other';

  const files = await knex('files').select();

  return knex.transaction(trx => {
    const queries = files.map(file =>
      knex('files')
        .where('id', file.id)
        .update({ ...file, content_type_group: getContentTypeGroup(file) })
        .transacting(trx),
    );
    return Promise.all(queries)
      .then(trx.commit)
      .catch(trx.rollback);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('files', function(t) {
    t.dropColumn('content_type_group');
  });
};
