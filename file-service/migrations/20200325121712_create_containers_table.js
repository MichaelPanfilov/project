exports.up = function(knex, Promise) {
  return knex.schema.createTable('containers', function(table) {
    table.string('id').primary();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('containers');
};
