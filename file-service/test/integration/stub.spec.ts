import axios from 'axios';
import Bottle = require('bottlejs');
import * as express from 'express';
import * as fs from 'fs-extra';
import { Server } from 'http';
import * as path from 'path';
import * as request from 'supertest';

import { Config } from '../../src/core/config';
import { knex } from '../../src/core/knex';
import { SpecHelper } from '../../src/helpers/specHelper';
import { Factory as FilesRepoFactory } from '../../src/repos/fileRepo';
import { Factory } from '../../src/routes/filesRoute';
import { Factory as AuthServiceFactory } from '../../src/services/authService';
import { AuthServiceMock } from '../../src/services/authService.mock';
import { Factory as FilesServiceFactory } from '../../src/services/fileService';
import { createDatabase, dropDatabase } from '../../test/helpers/connection';

const app = express();

describe('ROUTER: files', () => {
  let bottle: Bottle;
  let server: Server;
  let errorMw;

  beforeAll(async () => {
    bottle = new Bottle('testBottle');

    SpecHelper.init(bottle, app, !process.env.TEST_ENV);

    require('../../src/models/file');
    FilesRepoFactory({ bottle });
    FilesServiceFactory({ bottle });

    if (process.env.TEST_ENV === 'local') {
      AuthServiceFactory({ bottle });
      require('../../src/core/express/authorization').Factory({ expressApp: app, bottle });
    } else {
      AuthServiceMock.factory(bottle);
    }

    Factory({ bottle, expressApp: app });

    errorMw = SpecHelper.errorHandler(app);

    await createDatabase();
    await knex.migrate.rollback();
    await knex.migrate.latest();

    return new Promise((resolve, reject) => {
      try {
        if (app) {
          server = app.listen(Config.httpPort, (err: Error) => {
            if (err) {
              return reject(err);
            }

            resolve();
          });
        }
      } catch (e) {
        reject(e);
      }
    });
  });

  afterAll(async () => {
    await dropDatabase();
    Bottle.clear('testBottle');
    bottle = new Bottle('testBottle');

    return new Promise((resolve, reject) => {
      if (server) {
        server.close((err?: Error) => {
          if (err) {
            return reject(err);
          }

          resolve();
        });
      }
    });
  });

  describe('POST /files', () => {
    let authorization: string;

    beforeAll(async () => {
      if (process.env.TEST_ENV === 'local') {
        const {
          data: { data },
        } = await axios.post(Config.aclServiceUrl + '/login', {
          username: 'testUser1',
          password: 'testUser1',
        });
        authorization = `Bearer ${data}`;
      } else {
        authorization = 'valid';
      }
    });

    afterAll(() => {
      const dir = fs.readdirSync(path.join(Config.uploadPath));
      for (const file of dir) {
        if (path.extname(file) === '.txt') {
          fs.removeSync(path.join(Config.uploadPath, file));
        }
      }
    });

    let ID: string;
    let publicUrl: string;
    let privateUrl: string;
    let thumbnailPublicUrl: string;
    let thumbnailPrivateUrl: string;
    let token: string;

    it('should return publicUrl', async () => {
      await request(app)
        .post('/v1/files')
        .attach('file', path.join(Config.mockUploadPath, 'mock_file.txt'))
        .set('Authorization', authorization)
        .expect(res => {
          expect(res.body.data.publicUrl).toBeDefined();
          return res;
        })
        .then(res => {
          ID = res.body.data.id;
          publicUrl = res.body.data.relativePublicUrl;
          token = publicUrl.slice(publicUrl.lastIndexOf('/') + 1);
        });
    });

    it('should return privateUrl', async () => {
      await request(app)
        .post('/v1/files')
        .attach('file', path.join(Config.mockUploadPath, 'mock_file.txt'))
        .set('Authorization', authorization)
        .expect(res => {
          expect(res.body.data.privateUrl).toBeDefined();
          return res;
        })
        .then(res => (privateUrl = res.body.data.relativePrivateUrl));
    });

    it('should return thumbnailPublicUrl', async () => {
      await request(app)
        .post('/v1/files')
        .attach('file', path.join(Config.mockUploadPath, 'mock_file.txt'))
        .set('Authorization', authorization)
        .expect(res => {
          expect(res.body.data.thumbnailPublicUrl).toBeDefined();
          return res;
        })
        .then(res => (thumbnailPublicUrl = res.body.data.relativeThumbnailPublicUrl));
    });

    it('should return thumbnailPrivateUrl', async () => {
      await request(app)
        .post('/v1/files')
        .attach('file', path.join(Config.mockUploadPath, 'mock_file.txt'))
        .set('Authorization', authorization)
        .expect(res => {
          expect(res.body.data.thumbnailPrivateUrl).toBeDefined();
          return res;
        })
        .then(res => (thumbnailPrivateUrl = res.body.data.relativeThumbnailPrivateUrl));
    });

    describe('GET /files/:id', () => {
      it('should return uploaded file', async () => {
        await request(app)
          .get('/v1/files/' + ID)
          .set('Authorization', authorization)
          .expect(res => {
            expect(res.body.data).toEqual(
              jasmine.objectContaining({
                id: ID,
                filename: 'mock_file.txt',
                refId: null,
                tags: null,
                createdBy: '1',
                size: 10,
                contentType: 'text/plain',
                customData: null,
                createdAt: jasmine.any(String),
                updatedAt: jasmine.any(String),
                relativePublicUrl: jasmine.any(String),
                relativePrivateUrl: jasmine.any(String),
                relativeThumbnailPublicUrl: jasmine.any(String),
                relativeThumbnailPrivateUrl: jasmine.any(String),
                publicUrl: jasmine.any(String),
                privateUrl: jasmine.any(String),
                thumbnailPublicUrl: jasmine.any(String),
                thumbnailPrivateUrl: jasmine.any(String),
              }),
            );
          });
      });
    });

    describe('GET /files/:id/download/:token', () => {
      it('should download valid file by publicUrl', async () => {
        await request(app)
          .get(publicUrl)
          .expect(200)
          .then();
      });

      it('should throw 403 error with invalid token', async () => {
        await request(app)
          .get(publicUrl.slice(0, publicUrl.length - 1))
          .expect(403)
          .then();
      });
    });

    describe('GET /files/:id/download', () => {
      it('should download valid file by privateUrl', async () => {
        await request(app)
          .get(privateUrl)
          .set('Authorization', authorization)
          .expect(200)
          .then();
      });

      it('should throw 401 error with invalid token', async () => {
        await request(app)
          .get(privateUrl)
          .set('Authorization', 'invalid')
          .expect(401)
          .then();
      });
    });

    describe('GET /files/:id/thumbnail/:token', () => {
      it('should download valid thumbnail by thumbnailPublicUrl', async () => {
        await request(app)
          .get(thumbnailPublicUrl)
          .expect(200)
          .then();
      });

      it('should throw 403 error with invalid token', async () => {
        await request(app)
          .get(thumbnailPublicUrl.slice(0, thumbnailPublicUrl.length - 1))
          .expect(403)
          .then();
      });
    });

    describe('GET /files/:id/thumbnail', () => {
      it('should download valid thumbnail by thumbnailPrivateUrl', async () => {
        await request(app)
          .get(thumbnailPrivateUrl)
          .set('Authorization', authorization)
          .expect(200)
          .then();
      });

      it('should throw 401 error with invalid token', async () => {
        await request(app)
          .get(thumbnailPrivateUrl)
          .set('Authorization', 'invalid')
          .expect(401)
          .then();
      });
    });

    describe('PUT /files/:id', () => {
      it('should return file with updated refId & tags', async () => {
        await request(app)
          .put('/v1/files/' + ID)
          .send({ refId: 'updated_refId', tags: ['updated_tag'] })
          .set('Authorization', authorization)
          .expect(res => {
            expect(res.body.data).toEqual(
              jasmine.objectContaining({
                id: ID,
                refId: 'updated_refId',
                tags: '["updated_tag"]',
              }),
            );
          })
          .then();
      });

      describe('GET /files?refId=updated_refId&tag=updated_tag', () => {
        it('should return correct file by filter', async () => {
          await request(app)
            .get('/v1/files?refId=updated_refId&tag=updated_tag')
            .set('Authorization', authorization)
            .expect(res => {
              expect(res.body.data).toEqual(
                jasmine.arrayContaining([
                  jasmine.objectContaining({
                    id: ID,
                    refId: 'updated_refId',
                    tags: '["updated_tag"]',
                  }),
                ]),
              );
            })
            .then();
        });
      });
    });

    describe('DELETE /files/:id', () => {
      it('should return "success"', async () => {
        await request(app)
          .delete('/v1/files/' + ID)
          .set('Authorization', authorization)
          .expect(res => expect(res.body.data).toEqual('success'))
          .then();
      });

      describe('GET /files/:id', () => {
        it('should throw 404 error', async () => {
          await request(app)
            .get('/v1/files/' + ID)
            .set('Authorization', authorization)
            .expect(404)
            .then();
        });
      });

      describe('GET /files/:id/download/:token', () => {
        it('should throw 404 error', async () => {
          await request(app)
            .get(`/v1/files/${ID}/download/${token}`)
            .expect(404)
            .then();
        });
      });

      describe('GET /files/:id/download', () => {
        it('should throw 404 error', async () => {
          await request(app)
            .get(`/v1/files/${ID}/download`)
            .set('Authorization', authorization)
            .expect(404)
            .then();
        });
      });

      describe('GET /files/:id/thumbnail/:token', () => {
        it('should throw 404 error', async () => {
          await request(app)
            .get(`/v1/files/${ID}/thumbnail/${token}`)
            .expect(404)
            .then();
        });
      });

      describe('GET /files/:id/thumbnail', () => {
        it('should throw 404 error', async () => {
          await request(app)
            .get(`/v1/files/${ID}/thumbnail`)
            .set('Authorization', authorization)
            .expect(404)
            .then();
        });
      });
    });
  });
});

// describe('POST /files', () => {
//   it('should return filename', async () => {});
//   it('should return size', async () => {});
//   it('should return contentType', async () => {});
//
//   describe('GET /files/:id/download/:token', () => {
//     it('should return Content-Disposition attachment; filename=valid_filename', async () => {});
//     it('should return Content-Length valid_size', async () => {});
//     it('should return Content-Type valid_contentType', async () => {});
//   });
//
//   describe('GET /files/:id/download', () => {
//     it('should return Content-Disposition attachment; filename=valid_filename', async () => {});
//     it('should return Content-Length valid_size', async () => {});
//     it('should return Content-Type valid_contentType', async () => {});
//   });
//
//   describe('GET /files/:id/thumbnail/:token', () => {
//     it('should return Content-Disposition inline', async () => {});
//   });
//
//   describe('GET /files/:id/thumbnail', () => {
//     it('should return Content-Disposition inline', async () => {});
//   });
// });
//
// describe('PUT /files', () => {
//   it('should return updated filename', async () => {});
//   it('should return updated refID', async () => {});
//   it('should return updated tags', async () => {});
//
//   describe('GET /files/:id/download/:token', () => {
//     it('should download file with updated filename', async () => {});
//   });
//
//   describe('GET /files/:id/download', () => {
//     it('should download file with updated filename', async () => {});
//   });
//
// });
//
