import * as Knex from 'knex';
import * as path from 'path';

function getRawKnex(): [Knex, string] {
  const config = { ...require(path.join(process.cwd(), 'knexfile.js')) };
  const connection = config.testing.connection;
  const knex = require('knex')({
    ...config.testing,
    connection: { ...connection, database: null },
  });

  return [knex, connection.database];
}

export async function createDatabase() {
  const [knex, database] = getRawKnex();

  await knex.raw(`DROP DATABASE IF EXISTS \`${database}\``);
  await knex.raw(`CREATE DATABASE IF NOT EXISTS \`${database}\``);
  await knex.destroy();
}

export async function dropDatabase() {
  const [knex, database] = getRawKnex();

  await knex.raw(`DROP DATABASE IF EXISTS \`${database}\``);
  await knex.destroy();
}
