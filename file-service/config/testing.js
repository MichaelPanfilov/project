const os = require('os');
const path = require('path');
const fs = require('fs');

module.exports = {
  jwtPublic: fs.readFileSync(path.join(process.cwd(), '/data/certs/dev/key.pub')),

  httpPort: 8080,
  fullErrorStacks: true,
  uploadPath: os.tmpdir(),
};
