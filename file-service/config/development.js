const path = require('path');
const fs = require('fs');

module.exports = {
  jwtPublic: fs.readFileSync(path.join(process.cwd(), 'data/certs/dev/key.pub')),
  jwtInternalPublic: fs.readFileSync(path.join(process.cwd(), 'data/certs/dev/internal.key.pub')),
  jwtInternalSecret: fs.readFileSync(path.join(process.cwd(), 'data/certs/dev/internal.key')),

  httpPort: 4080,
  fullErrorStacks: true,
};
