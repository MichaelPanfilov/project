const path = require('path');

module.exports = {
  jwtSecret: 'secret',
  jwtAlgorithm: 'RS512',
  jwtInternalAlgorithm: 'RS256',
  jwtValidityS: 60 * 60 * 24 * 20,

  httpPort: 4080,
  fullErrorStacks: false,

  dataPath: path.join(process.cwd(), 'data/'),

  logNamespace: 'app',
  logPath: path.join(process.cwd(), 'data/logs/'),

  uploadPath: path.join(process.cwd(), 'data/uploads/'),
  mockUploadPath: path.join(process.cwd(), 'mock_uploads/'),

  thumbnailPath: path.join(process.cwd(), 'data/thumbnails/'),

  aclServiceUrl: 'http://localhost:4070/v1',
  fileServiceUrl: 'http://localhost:4080/v1',
  fileServicePublicUrl: 'http://localhost:4080/v1',

  uploadMaxFileSize: 512000000,
  queryMaxLimit: 1000,
  maxThumbnailSize: 1000,
};
