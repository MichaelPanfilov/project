const fs = require('fs');
const path = require('path');
const urljoin = require('url-join');

module.exports = {
  jwtPublic: fs.readFileSync(path.join(process.cwd(), '/data/certs/staging/key.pub')),

  httpPort: 8080,

  dataPath: path.join(process.cwd(), 'data/'),
  logPath: path.join(process.cwd(), 'data/logs/'),
  uploadPath: path.join(process.cwd(), 'data/uploads/'),

  aclServiceUrl: urljoin(process.env.APP_ACL_SERVICE_URL, '/v1'),
  fileServiceUrl: urljoin(process.env.APP_FILE_SERVICE_URL, '/v1'),
  fileServicePublicUrl: urljoin(process.env.APP_FILE_SERVICE_PUBLIC_URL, '/v1'),
};
