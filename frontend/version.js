const { writeFileSync } = require('fs');
const { resolve } = require('path');
const packageJSON = require('./package.json');
const projectPackageJSON = require('../package.json');
const filePath = resolve(__dirname, 'src', 'environments', 'version.ts');
writeFileSync(
  filePath,
  `// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT!
  /* tslint:disable */
  export const FRONTEND_VERSION = "${packageJSON.version}";
  export const SHOPFLOOR_VERSION = "${projectPackageJSON.version}";
  /* tslint:enable */`,
  { encoding: 'utf-8' }
);
console.log(`Wrote version info to: ${filePath}`);