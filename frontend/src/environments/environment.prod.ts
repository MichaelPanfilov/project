export const environment = {
  production: true,
  aclServiceUrl: '/api/acl',
  fileServiceUrl: '/api/file',
  apiUrl: '/api',
  datapointServiceUrl: '/api/datapoint',
};
