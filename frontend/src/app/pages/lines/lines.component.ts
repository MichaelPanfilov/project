import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LineDto, MachineDto } from '@common';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { lineDetailPath, machineDetailPath } from 'src/app/core/navigation';
import { Asset, State } from 'src/app/core/store';

@Component({
  selector: 'syn-lines',
  templateUrl: './lines.component.html',
  styleUrls: ['./lines.component.scss'],
})
export class LinesComponent {
  lines$ = this.store
    .select(Asset.selectCurrentFactoryTree)
    .pipe(map(factory => (factory ? factory.children || [] : [])));

  constructor(private router: Router, private store: Store<State>) {}

  toLineDetailPage(line: LineDto) {
    this.router.navigate(lineDetailPath(line.id));
  }

  toMachineDetailPage(machine: MachineDto) {
    this.router.navigate(machineDetailPath(machine.id));
  }
}
