import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { map, startWith, take } from 'rxjs/operators';
import { AuthService, NotificationService, NotificationVariants } from 'src/app/core/services';
import { Asset, State, User } from 'src/app/core/store';

@Component({
  selector: 'login',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html',
  providers: [MatSnackBar],
})
export class LoginComponent implements OnInit {
  loginForm = this.formBuilder.group({
    username: ['', [Validators.required, Validators.minLength(1)]],
    password: ['', [Validators.required, Validators.minLength(1)]],
  });
  disabled$ = this.loginForm.valueChanges.pipe(
    startWith(null),
    map(() => !this.loginForm.valid),
  );

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private store: Store<State>,
    private actions$: Actions,
    private notificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.greetUserOnLogin();
  }

  async onLogin() {
    if (this.loginForm && this.loginForm.value) {
      this.authService.login(this.loginForm.value).subscribe(
        () => {
          this.router.navigate(['/']);
          this.store.dispatch(Asset.TreeActions.loadTrees({}));
        },
        (err: HttpErrorResponse) =>
          err.status === 401 &&
          this.notificationService.notify({
            message: 'COMMON.ERROR.FAILED_LOGIN',
            variant: NotificationVariants.ERROR,
          }),
      );
    }
  }

  greetUserOnLogin() {
    this.actions$.pipe(ofType(User.loadedMe), take(1)).subscribe(({ me }) => {
      this.notificationService.notify({
        message: 'COMMON.MESSAGE.WELCOME_USER',
        param: me,
        variant: NotificationVariants.SUCCESS,
      });
    });
  }
}
