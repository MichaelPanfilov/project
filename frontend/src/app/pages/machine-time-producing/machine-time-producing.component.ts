import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { isTimeRange, MachineDto, SynMachineState, TimeRange } from '@common';
import { Color } from '@kurkle/color';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import * as moment from 'moment';
import { BaseChartDirective } from 'ng2-charts';
import { BehaviorSubject, combineLatest, interval, of, Subject } from 'rxjs';
import { delay, filter, map, startWith, switchMap, take, takeUntil } from 'rxjs/operators';
import { Asset, PerformanceRangeFilter, Settings, State } from 'src/app/core/store';
import { STATUS_COLORS } from 'src/app/util';

export interface ConfigInterface {
  countSections: number;
}

// TODO: Replace using websocket
const UPDATING_INTERVAL = 10000;

@Component({
  selector: 'syn-machine-time-producing',
  templateUrl: './machine-time-producing.component.html',
  styleUrls: ['./machine-time-producing.component.scss'],
})
export class MachineTimeProducingComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private colors$ = new BehaviorSubject<{ [key in SynMachineState]: string }>(STATUS_COLORS);
  private filter$ = this.store.select(PerformanceRangeFilter.selectIsCustomFilter).pipe(
    switchMap(isCustom =>
      combineLatest([
        this.store.select(Asset.selectCurrentMachine),
        isCustom
          ? this.store.select(PerformanceRangeFilter.selectRangeFilterDates)
          : this.store.select(PerformanceRangeFilter.selectRangeFilterValue),
      ]),
    ),
    filter(([machine]) => !!machine),
  );

  @ViewChild(BaseChartDirective)
  chart!: BaseChartDirective;

  CONFIGS: ConfigInterface = {
    countSections: 15,
  };

  pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
      labels: {
        fontSize: 14,
        fontFamily: 'Syntegon, Roboto, Helvetica Neue, sans-serif',
        generateLabels: (chart: Chart) => {
          const { data } = chart;
          if (!(data && data.datasets && data.datasets.length && data.labels)) {
            return [];
          }

          const backgrounds = data.datasets[0].backgroundColor as string[];
          const dataset = data.datasets[0].data as number[];

          const totalProgress = dataset.reduce((res: number, current: number) => res + current, 0);
          const progressPercent = totalProgress / 100;

          const colors = this.colors$.value;
          return backgrounds
            .map(bg => Object.keys(colors).find(key => colors[key] === bg))
            .map((label, index) => {
              const value = dataset[index];

              const translation = this.translate.instant(
                `ASSET.STATE.${((label || 'Unknown') as SynMachineState).toUpperCase()}`,
              );

              return {
                text: `${translation}: ${Math.round(value / progressPercent)}%`,
                fillStyle: backgrounds[index],
              };
            });
        },
      },
      onClick: (_, labelItem) => {
        const state = this.findStateByColor(labelItem.fillStyle || '');
        this.highlightState(state);
      },
    },
    animation: {
      duration: 0,
    },
    tooltips: {
      enabled: false,
    },
    onClick: (event: MouseEvent) => {
      const state = this.getClickedStatusData(event);
      this.highlightState(state);
    },
  };

  pieChartData: ChartDataSets[] = [
    {
      data: [],
    },
  ];

  machineTimeline$ = combineLatest([
    this.store.select(Asset.selectCurrentMachineTimeline),
    this.colors$,
  ]).pipe(
    map(([timeline, colors]) =>
      (timeline || []).map(tl => ({
        ...tl,
        color: colors[tl.status],
      })),
    ),
  );

  secondsRunning$ = combineLatest([this.machineTimeline$, this.colors$]).pipe(
    map(([timeline, colors]) => {
      const uniqKeys = Array.from(new Set(timeline.map(({ status }) => status)));
      const uniqStatistic: { [key in SynMachineState]?: number } = uniqKeys.reduce((res, key) => {
        const value = timeline
          .filter(({ status }) => status === key)
          .reduce((result, item) => {
            const to = new Date(item.to);
            const from = new Date(item.from);

            const timeDiff = Math.round((+to - +from) / 1000);

            return result + Math.max(timeDiff, 0);
          }, 0);

        return { ...res, [key]: value };
      }, {});

      this.pieChartData = [
        {
          data: Object.values(uniqStatistic),
          backgroundColor: Object.keys(uniqStatistic).map(key => colors[key]),
        },
      ];

      return uniqStatistic['Running'] || 0;
    }),
  );

  workingTime$ = this.secondsRunning$.pipe(
    map(seconds => moment.utc(seconds * 1000).format('HH:mm')),
  );
  workingTimePercentage$ = combineLatest([
    this.secondsRunning$,
    this.store.select(PerformanceRangeFilter.selectRangeFilterValue),
  ]).pipe(
    map(([seconds, filterValue]) => {
      const timePercent = filterValue / 100;
      return Math.round((seconds * 1000) / timePercent);
    }),
  );

  focused = true;
  // Flag will only be used for changed the user makes to the filter.
  // Auto updating of the timeline will not trigger the loading placeholders.
  loaded = true;

  constructor(
    private store: Store<State>,
    private actions$: Actions,
    private translate: TranslateService,
  ) {}

  @HostListener('window:focus', ['$event'])
  onFocus($event: Event) {
    this.focused = true;
  }

  @HostListener('window:blur', ['$event'])
  onBlur($event: Event) {
    this.focused = false;
  }

  ngOnInit() {
    interval(UPDATING_INTERVAL)
      .pipe(
        takeUntil(this.destroy$),
        filter(() => this.focused),
        startWith(0),
        switchMap(() => this.filter$.pipe(take(1))),
      )
      .subscribe(([machine, value]) => this.loadTimeline(value, machine as MachineDto));

    this.filter$.pipe(takeUntil(this.destroy$)).subscribe(([machine, value]) => {
      of(null)
        .pipe(
          delay(333),
          takeUntil(this.actions$.pipe(ofType(Asset.MachineActions.loadedTimeline))),
        )
        .subscribe(() => (this.loaded = false));
      this.loadTimeline(value, machine as MachineDto);
    });

    this.store
      .select(Settings.selectLoadingByAction(Asset.MachineActions.loadTimeline))
      .pipe(
        takeUntil(this.destroy$),
        filter(loading => !loading),
      )
      .subscribe(() => (this.loaded = true));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  highlightState(state: SynMachineState | undefined) {
    if (!state) {
      this.colors$.next(STATUS_COLORS);
    } else {
      const colors = Object.entries(STATUS_COLORS).reduce(
        (prev, [key, value]) => ({
          ...prev,
          [key]:
            key === state
              ? value
              : (new Color(value).alpha(0.15).hexString() as string).toLowerCase(),
        }),
        {} as { [key in SynMachineState]: string },
      );
      this.colors$.next(colors);
    }
  }

  private loadTimeline(value: TimeRange | number, machine: MachineDto) {
    const to = new Date();
    const range = isTimeRange(value) ? value : { from: new Date(to.getTime() - value), to };
    this.store.dispatch(Asset.MachineActions.loadTimeline({ machine, ...range }));
  }

  private getClickedStatusData(event: MouseEvent): SynMachineState | undefined {
    // tslint:disable-next-line
    const activePoint = this.chart.chart.getElementAtEvent(event)[0] as any | undefined;
    if (!activePoint) {
      return undefined;
    }

    const data = activePoint._chart.data;
    const datasetIndex = activePoint._datasetIndex;
    const bgColor = data.datasets[datasetIndex].backgroundColor[activePoint._index];
    return this.findStateByColor(bgColor);
  }

  private findStateByColor(color: string) {
    const colors = this.colors$.value;
    // Make sure we dont match the alpha part of the color.
    return Object.keys(colors).find(
      key => color.substring(0, 7) === colors[key].substring(0, 7),
    ) as SynMachineState | undefined;
  }
}
