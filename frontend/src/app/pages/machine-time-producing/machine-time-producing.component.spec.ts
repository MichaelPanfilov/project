import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineTimeProducingComponent } from './machine-time-producing.component';

describe('MachineTimeProducingComponent', () => {
  let component: MachineTimeProducingComponent;
  let fixture: ComponentFixture<MachineTimeProducingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachineTimeProducingComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineTimeProducingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
