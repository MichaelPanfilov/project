import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AssetDto, AssetType, InstructionDto, LineTreeDto, MachineDto } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { machineDetailPath } from 'src/app/core/navigation';
import {
  Asset,
  Document,
  Instruction,
  PerformanceRangeFilter,
  Settings,
  State,
  Task,
} from 'src/app/core/store';
import { ModalService } from 'src/app/modals/modal.service';
import { AssetTreeElementActions, isTaskDue, ModalModes } from 'src/app/util';

@Component({
  selector: 'syn-line-detail',
  templateUrl: './line-detail.component.html',
  styleUrls: ['./line-detail.component.scss'],
})
export class LineDetailComponent implements OnInit, OnDestroy {
  private lineId!: string;
  private destroy$ = new Subject<void>();

  currentLine$ = this.store
    .select(Asset.selectCurrentLineTree)
    .pipe(filter(line => !!line)) as Observable<LineTreeDto>;

  machines$: Observable<MachineDto[]> = this.currentLine$.pipe(map(line => line.children || []));

  totalCount$: Observable<number> = this.machines$.pipe(
    switchMap(machines => {
      const ids = machines.filter(({ isLineOutput }) => isLineOutput).map(({ id }) => id);
      if (!ids.length) {
        return of([]);
      }
      return combineLatest(ids.map(id => this.store.select(Asset.selectMachineOutputCount(id))));
    }),
    map(counts => counts.map(count => count || 0).reduce((curr, prev) => curr + prev, 0)),
  );

  machinesDueTasksCount$: Observable<number> = this.machines$.pipe(
    map(machines => machines.map(m => m.id)),
    switchMap(ids => this.store.select(Task.selectActiveTasksOfAssets(ids))),
    map(tasks => tasks && tasks.filter(isTaskDue).length),
  );

  lineDueTasksCount$: Observable<number> = this.currentLine$.pipe(
    switchMap(line => this.store.select(Task.selectActiveTasksOfAsset(line.id))),
    map(tasks => tasks && tasks.filter(isTaskDue).length),
  );

  instructionList$: Observable<InstructionDto[]> = this.currentLine$.pipe(
    switchMap(line => this.store.select(Instruction.selectInstructionsByRefId(line.id))),
  );

  taskRefIds$ = this.currentLine$.pipe(map(line => line.id));
  loading$ = this.actions$.pipe(
    ofType(Settings.showLoading, Settings.hideLoading),
    filter(({ action }) => {
      return ([
        Asset.TreeActions.loadTrees.type,
        Document.loadDocuments.type,
        Task.loadTasks.type,
      ] as string[]).includes(action.type);
    }),
    map(a => a.type === Settings.showLoading.type),
  );

  constructor(
    private store: Store<State>,
    private router: Router,
    private modalService: ModalService,
    private actions$: Actions,
  ) {}

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngOnInit(): void {
    this.currentLine$.pipe(takeUntil(this.destroy$)).subscribe(line => {
      this.store.dispatch(Task.loadActiveTasksOfAsset(line.id));
      this.store.dispatch(Instruction.loadInstructionsByRefIds([line.id]));
    });

    combineLatest([
      this.currentLine$,
      this.store.select(PerformanceRangeFilter.selectRangeFilterDates),
    ])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([line, { from, to }]) => {
        this.lineId = line.id;
        (line.children || []).forEach(machine => {
          const payload = { machine, from, to };
          this.store.dispatch(Asset.MachineActions.loadOutput(payload));
          this.store.dispatch(Asset.MachineActions.loadTimeline(payload));
        });
      });
  }

  onCreateTask() {
    this.modalService.openTaskModal(ModalModes.CREATE, { assignedAsset: this.lineId });
  }

  onCreateDocument() {
    this.modalService.openDocumentModal({
      document: { refIds: [this.lineId] },
      mode: ModalModes.CREATE,
    });
  }

  onCreateMachine() {
    this.modalService.openAssetModal(AssetType.MACHINE, { parent: this.lineId });
  }

  onAssetView(asset: AssetDto) {
    const dialogRef = this.modalService.openViewAssetModal(asset);
    dialogRef
      .afterClosed()
      .pipe(filter(action => action && action === AssetTreeElementActions.EDIT))
      .subscribe(() => this.modalService.openEditAssetModal(AssetType.LINE, asset));
  }

  onCreateInstruction() {
    this.modalService.openWorkInstructionModal({
      instruction: { links: [this.lineId] },
      mode: ModalModes.CREATE,
    });
  }

  toMachineDetailPage(machine: MachineDto) {
    this.router.navigate(machineDetailPath(machine.id));
  }
}
