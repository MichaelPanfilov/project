import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineMetricComponent } from './machine-metric.component';

describe('MachineMetricComponent', () => {
  let component: MachineMetricComponent;
  let fixture: ComponentFixture<MachineMetricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachineMetricComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineMetricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
