import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FaultStopDto, MachineDto, ProductLossDto, TimelineDto } from '@common';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { machineFaultStopCountsPath, machineFaultStopDurationsPath } from 'src/app/core/navigation';
import { Asset, PerformanceRangeFilter, Settings, State } from 'src/app/core/store';
import { STATUS_COLORS, wrapToLoadedObservable } from 'src/app/util';

export type MetricType = 'product-losses' | 'fault-stops/durations' | 'fault-stops/counts';

export interface MetricItem {
  label: string;
  value: number;
  displayValue: string;
}

export interface MachineMetricsConfig {
  summaryTitle: string;
  summaryTitleBrackets: string;
  chartTitle: string;
}

interface MetricsData extends MachineMetricsConfig {
  type: MetricType;
}

@Component({
  selector: 'syn-machine-metric',
  templateUrl: './machine-metric.component.html',
  styleUrls: ['./machine-metric.component.scss'],
})
export class MachineMetricComponent implements OnDestroy {
  // This could become an dynamic value later.
  num = 5;
  config: MachineMetricsConfig = {
    summaryTitle: '',
    summaryTitleBrackets: '',
    chartTitle: '',
  };
  navLinks: { label: string; path: string[] }[] = [];
  colors = STATUS_COLORS;
  selectedReason: null | string = null;

  currentFilter$ = this.store.select(PerformanceRangeFilter.selectRangeFilterDates);
  destroy$ = new Subject<void>();
  type$ = new BehaviorSubject<MetricType>('product-losses');

  faultStops$ = this.store.select(Asset.selectCurrentMachineFaultStops);
  faultStopCounts$ = this.store.select(Asset.selectCurrentMachineFaultStopCounts);
  productLosses$ = this.store.select(Asset.selectCurrentMachineProductLoses);

  percent$ = this.type$.pipe(
    switchMap(type =>
      type === 'product-losses'
        ? this.store.select(Asset.selectCurrentMachineProductLossPercentage)
        : combineLatest([
            this.store.select(Asset.selectCurrentMachineTimeline),
            this.store.select(PerformanceRangeFilter.selectRangeFilterValue),
          ]).pipe(
            map(([timeline, filterValue]) =>
              this.calcFaultedTimePercentage(timeline || [], filterValue),
            ),
          ),
    ),
  );

  data$ = this.type$.pipe(
    switchMap(type => this.getData(type)),
    map(data => data.sort((a, b) => b.value - a.value).slice(0, this.num)),
  );

  total$ = this.type$.pipe(
    switchMap(type =>
      type === 'product-losses'
        ? this.store.select(Asset.selectCurrentMachineProductLossCount)
        : this.data$.pipe(map(stops => stops.reduce((prev, curr) => prev + curr.value, 0))),
    ),
    map(value => value || 0),
  );

  maxValue$ = this.data$.pipe(map(data => Math.max(...data.map(d => d.value))));

  totalValue$ = this.type$.pipe(switchMap(type => this.getTotalValue(type)));

  loadedTop$ = wrapToLoadedObservable(this.type$.pipe(switchMap(type => this.getLoadingTop(type))));
  loadedMiddle$ = wrapToLoadedObservable(
    this.store.select(
      Settings.selectLoadingByAction(
        Asset.MachineActions.loadFaultStops,
        Asset.MachineActions.loadFaultStopCounts,
        Asset.MachineActions.loadProductLoses,
      ),
    ),
  );
  loadedBottom$ = wrapToLoadedObservable(
    this.store.select(
      Settings.selectLoadingByAction(
        Asset.MachineActions.loadFaultStops,
        Asset.MachineActions.loadProductLoses,
      ),
    ),
  );

  constructor(private store: Store<State>, route: ActivatedRoute) {
    combineLatest([route.data, store.select(Asset.selectCurrentMachineId)])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, machineId]) => !!machineId),
        map(([data, machineId]) => [data['metrics'], machineId] as [MetricsData, string]),
      )
      .subscribe(([config, machineId]) => {
        this.config = config;
        this.navLinks = [
          {
            label: 'NAVIGATION.TITLE.DURATIONS',
            path: ['/', ...machineFaultStopDurationsPath(machineId as string)],
          },
          {
            label: 'NAVIGATION.TITLE.COUNTS',
            path: ['/', ...machineFaultStopCountsPath(machineId as string)],
          },
        ];
        this.type$.next(config.type);
      });

    combineLatest([
      this.type$,
      this.store.select(Asset.selectCurrentMachine).pipe(filter(machine => !!machine)),
      this.currentFilter$,
    ])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([type, machine, { from, to }]) =>
        this.dispatchActions(type, machine as MachineDto, from, to),
      );

    this.currentFilter$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => (this.selectedReason = null));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private getLoadingTop(type: MetricType) {
    if (type === 'product-losses') {
      return this.store.select(
        Settings.selectLoadingByAction(
          Asset.MachineActions.loadProductLoses,
          Asset.MachineActions.loadProductLossCount,
          Asset.MachineActions.loadOutput,
        ),
      );
    }
    return this.store.select(
      Settings.selectLoadingByAction(
        Asset.MachineActions.loadTimeline,
        Asset.MachineActions.loadFaultStopCounts,
        Asset.MachineActions.loadFaultStops,
      ),
    );
  }

  private getData(type: MetricType): Observable<MetricItem[]> {
    if (type === 'fault-stops/counts') {
      return this.faultStopCounts$.pipe(map(counts => this.getStopCountMetrics(counts || [])));
    } else if (type === 'product-losses') {
      return this.productLosses$.pipe(map(data => this.getLossMetrics(data || [])));
    }
    return this.faultStops$.pipe(map(data => this.getStopReasonMetrics(data || [])));
  }

  private getTotalValue(
    type: MetricType,
  ): Observable<{ formatted: string | number; unit: string }> {
    if (type === 'fault-stops/durations') {
      return this.faultStops$.pipe(map(stops => this.getTotalFormatted(stops || [])));
    }
    if (type === 'fault-stops/counts') {
      return this.faultStopCounts$.pipe(
        map(counts => ({
          formatted: (counts || []).reduce((prev, curr) => prev + curr.count, 0) || '0',
          unit: 'COMMON.NUMERIC.UNIT_COUNT',
        })),
      );
    }
    return this.total$.pipe(
      map(value => ({ formatted: value, unit: 'COMMON.NUMERIC.UNIT_COUNT' })),
    );
  }

  private dispatchActions(type: MetricType, machine: MachineDto, from: Date, to: Date) {
    const payload = { machine, from, to };
    if (type === 'product-losses') {
      this.store.dispatch(Asset.MachineActions.loadProductLoses(payload));
      this.store.dispatch(Asset.MachineActions.loadProductLossCount(payload));
      this.store.dispatch(Asset.MachineActions.loadOutput(payload));
    } else {
      this.store.dispatch(Asset.MachineActions.loadTimeline(payload));
      this.store.dispatch(Asset.MachineActions.loadFaultStopCounts(payload));
      this.store.dispatch(Asset.MachineActions.loadFaultStops(payload));
    }
  }

  private getStopCountMetrics(counts: { label: string; count: number }[]): MetricItem[] {
    return counts.map(({ label, count }) => ({
      label,
      value: count,
      displayValue: count.toString(),
    }));
  }

  private getStopReasonMetrics(stops: FaultStopDto[]): MetricItem[] {
    const sortedArray = stops
      .slice()
      .sort((a, b) => this.getTimeInSeconds(b.from, b.to) - this.getTimeInSeconds(a.from, a.to));

    const res = sortedArray.reduce((acc, item) => {
      const last = acc[item.reason as string];
      const value = (last ? last.value : 0) + this.getTimeInSeconds(item.from, item.to);
      const formatted = this.formateSeconds(value);
      return {
        ...acc,
        [item.reason as string]: {
          value,
          displayValue: formatted.formatted + ' ' + formatted.unit,
          label: item.reason,
        },
      };
    }, {} as { [key: string]: MetricItem });

    return Object.values(res);
  }

  private getLossMetrics(losses: ProductLossDto[]): MetricItem[] {
    const res = losses.reduce((acc, item) => {
      const count = acc[item.reason as string] ? acc[item.reason as string].value + 1 : 1;

      return {
        ...acc,
        [item.reason as string]: {
          value: count,
          label: item.reason,
          displayValue: count + ' cts',
        },
      };
    }, {} as { [key: string]: MetricItem });

    return Object.values(res);
  }

  private getTimeInSeconds(from: Date, to: Date) {
    return Math.abs(moment(from).diff(moment(to), 'seconds'));
  }

  private formateSeconds(seconds: number): { formatted: string; unit: string } {
    const timeAlias = (time: number) => {
      const aliases = [
        {
          condition: time < 60,
          measure: 'ss',
        },
        {
          condition: time <= 1500,
          measure: 'mm:ss',
        },
        {
          condition: time > 3600,
          measure: 'HH:mm',
        },
      ];
      const targetMeasure = aliases.find(({ condition }) => condition);
      return targetMeasure ? targetMeasure.measure : 'HH:mm';
    };

    const formatted = moment.utc(seconds * 1000).format(timeAlias(seconds));
    const unit = seconds < 60 ? 's' : seconds <= 1500 ? 'min' : 'h';

    return {
      formatted: unit === 'min' || unit === 's' ? formatted.replace(/^0+/, '') || '0' : formatted,
      unit,
    };
  }

  private getTotalFormatted(stops: FaultStopDto[]): { formatted: string; unit: string } {
    const totalTimeDiff = stops.reduce((res, item) => {
      return res + moment(item.from).diff(moment(item.to), 'seconds');
    }, 0);
    return this.formateSeconds(Math.abs(totalTimeDiff));
  }

  private calcFaultedTimePercentage(timeline: TimelineDto[], rangeFilter: number): number {
    const seconds = timeline
      .filter(item => item.status === 'Faulted')
      .reduce(
        (curr, prev) =>
          curr + Math.max(new Date(prev.to).getTime() - new Date(prev.from).getTime(), 0) / 1000,
        0,
      );
    const timePercent = rangeFilter / 100;
    return Math.round((seconds * 1000) / timePercent);
  }
}
