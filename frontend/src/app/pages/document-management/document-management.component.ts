import { Component } from '@angular/core';
import { RemoteActions } from 'src/app/core/abstract';
import { NavbarActionsViewService } from 'src/app/core/services';
import { ModalService } from 'src/app/modals/modal.service';
import { ModalModes } from 'src/app/util';

@Component({
  selector: 'syn-document-management',
  templateUrl: './document-management.component.html',
  styleUrls: ['./document-management.component.scss'],
})
export class DocumentManagementComponent extends RemoteActions {
  constructor(navActions: NavbarActionsViewService, private modalService: ModalService) {
    super(navActions);
  }

  onCreateDoc() {
    this.modalService.openDocumentModal({ mode: ModalModes.CREATE });
  }
}
