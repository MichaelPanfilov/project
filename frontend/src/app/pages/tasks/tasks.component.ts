import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TaskDto } from '@common';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { TaskTableDisplayMode } from 'src/app/components';
import { RemoteActions } from 'src/app/core/abstract';
import { NavbarActionsViewService } from 'src/app/core/services';
import { ModalService } from 'src/app/modals/modal.service';
import { isTaskActive, isTaskArchived, ModalModes } from 'src/app/util';

type TaskPageMode = Exclude<TaskTableDisplayMode, 'asset'>;

@Component({
  selector: 'syn-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent extends RemoteActions implements OnDestroy {
  private destroy$ = new Subject<void>();

  mode$: Observable<TaskPageMode> = this.route.data.pipe(map(data => data.mode || 'all'));

  constructor(
    navActions: NavbarActionsViewService,
    private route: ActivatedRoute,
    private modalService: ModalService,
  ) {
    super(navActions);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    super.ngOnDestroy();
  }

  filterTasks(tasks: TaskDto[], mode: TaskPageMode): TaskDto[] {
    if (mode === 'all') {
      return tasks.filter(isTaskActive);
    }
    return tasks.filter(isTaskArchived);
  }

  onCreateTask() {
    this.modalService.openTaskModal(ModalModes.CREATE);
  }
}
