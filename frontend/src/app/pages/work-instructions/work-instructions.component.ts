import { Component } from '@angular/core';
import { RemoteActions } from 'src/app/core/abstract';
import { NavbarActionsViewService } from 'src/app/core/services';
import { ModalService } from 'src/app/modals/modal.service';
import { ModalModes } from 'src/app/util';

@Component({
  selector: 'syn-work-instructions',
  templateUrl: './work-instructions.component.html',
  styleUrls: ['./work-instructions.component.scss'],
})
export class WorkInstructionsComponent extends RemoteActions {
  constructor(navActions: NavbarActionsViewService, private modalService: ModalService) {
    super(navActions);
  }

  onNewInstruction() {
    this.modalService.openWorkInstructionModal({ mode: ModalModes.CREATE });
  }
}
