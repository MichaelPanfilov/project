import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Line } from 'src/app/core/store';

import { MachinePerformanceComponent } from './machine-performance.component';

describe('MachinePerformanceComponent', () => {
  let component: MachinePerformanceComponent;
  let fixture: ComponentFixture<MachinePerformanceComponent>;
  let mockStore: MockStore<{}>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachinePerformanceComponent],
      imports: [StoreModule.forRoot({})],
      providers: [provideMockStore({ initialState: { performance: undefined } })],
    }).compileComponents();

    mockStore = TestBed.get(Store);
    mockStore.overrideSelector(Line.selectLinePerformance, undefined);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinePerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
