import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  AssetDto,
  AssetType,
  InstructionDto,
  MachineDto,
  SynMachineState,
  TimelineDto,
} from '@common';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { combineLatest, Observable, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import {
  machineFaultStopsPath,
  machineProductLossPath,
  machineTimeProducingPath,
} from 'src/app/core/navigation';
import {
  Asset,
  Instruction,
  PerformanceRangeFilter,
  Settings,
  State,
  Task,
} from 'src/app/core/store';
import { ModalService } from 'src/app/modals/modal.service';
import {
  AssetTreeElementActions,
  isTaskDue,
  ModalModes,
  wrapToLoadedObservable,
} from 'src/app/util';

@Component({
  selector: 'syn-machine-performance.',
  templateUrl: './machine-performance.component.html',
  styleUrls: ['./machine-performance.component.scss'],
})
export class MachinePerformanceComponent implements OnInit, OnDestroy {
  private machineId!: string;
  private destroy$ = new Subject<void>();

  placeholder = '../../../assets/imgs/machine-default.png';

  machine$ = this.store
    .select(Asset.selectCurrentMachine)
    .pipe(filter(machine => !!machine)) as Observable<MachineDto>;

  totalCount$ = this.store
    .select(Asset.selectCurrentMachineOutputCount)
    .pipe(map(value => value || 0));

  instructionList$: Observable<InstructionDto[]> = this.machine$.pipe(
    switchMap(machine => this.store.select(Instruction.selectInstructionsByRefId(machine.id))),
  );

  dueTasksCount$: Observable<number> = this.machine$.pipe(
    switchMap(machine => this.store.select(Task.selectActiveTasksOfAsset(machine.id))),
    map(tasks => tasks && tasks.filter(isTaskDue).length),
  );

  timeProducingLoaded$ = wrapToLoadedObservable(
    this.store.select(Settings.selectLoadingByAction(Asset.MachineActions.loadTimeline)),
  );
  productLossesLoaded$ = wrapToLoadedObservable(
    combineLatest([
      this.store.select(
        Settings.selectLoadingByAction(
          Asset.MachineActions.loadProductLossCount,
          Asset.MachineActions.loadOutput,
        ),
      ),
      this.store.select(Settings.selectLoadingByAction(Asset.MachineActions.loadProductLossCount)),
    ]).pipe(map(res => !res.includes(false))),
  );
  faultStopTimeLoaded$ = wrapToLoadedObservable(
    combineLatest([
      this.store.select(Settings.selectLoadingByAction(Asset.MachineActions.loadTimeline)),
      this.store.select(Settings.selectLoadingByAction(Asset.MachineActions.loadTimeline)),
      this.store.select(Settings.selectLoadingByAction(Asset.MachineActions.loadFaultStopCounts)),
    ]).pipe(map(res => !res.includes(false))),
  );

  performance$: Observable<{
    timeProducing: number;
    timeProducingDuration: string;
    productLoss: number;
    productLossCount: number;
    faultStop: number;
    faultStopDuration: string;
    stopsCount: number;
  }> = combineLatest([
    this.store.select(Asset.selectCurrentMachineTimeline),
    this.store.select(Asset.selectCurrentMachineProductLossPercentage),
    this.store.select(PerformanceRangeFilter.selectRangeFilterValue),
    this.store.select(Asset.selectCurrentMachineProductLossCount),
    this.store.select(Asset.selectCurrentMachineFaultStopCounts),
  ]).pipe(
    map(([timeline, productLossPercentage, filterValue, productLossCount, stopsCounts]) => {
      return {
        timeProducing: this.calcTimePercentage(timeline || [], 'Running', filterValue),
        timeProducingDuration: this.getTimeDurationString(timeline || [], 'Running'),
        productLoss: productLossPercentage,
        productLossCount: productLossCount || 0,
        faultStop: this.calcTimePercentage(timeline || [], 'Faulted', filterValue),
        faultStopDuration: this.getTimeDurationString(timeline || [], 'Faulted'),
        stopsCount: stopsCounts ? stopsCounts.reduce((acc, next) => acc + (next.count || 0), 0) : 0,
      };
    }),
  );

  constructor(
    private store: Store<State>,
    private router: Router,
    private modalService: ModalService,
  ) {}

  ngOnInit() {
    this.machine$.pipe(takeUntil(this.destroy$)).subscribe(machine => {
      this.machineId = machine.id;
      this.store.dispatch(Task.loadActiveTasksOfAsset(machine.id));
      this.store.dispatch(Instruction.loadInstructionsByRefIds([machine.id]));
    });

    combineLatest([this.machine$, this.store.select(PerformanceRangeFilter.selectRangeFilterDates)])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([machine, { from, to }]) => {
        const payload = {
          machine,
          from,
          to,
        };
        this.store.dispatch(Asset.MachineActions.loadTimeline(payload));
        this.store.dispatch(Asset.MachineActions.loadOutput(payload));
        this.store.dispatch(Asset.MachineActions.loadProductLossCount(payload));
        this.store.dispatch(Asset.MachineActions.loadFaultStops(payload));
        this.store.dispatch(Asset.MachineActions.loadFaultStopCounts(payload));
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  toTimeProducingPage() {
    this.router.navigate(machineTimeProducingPath(this.machineId));
  }

  toProductLossPage() {
    this.router.navigate(machineProductLossPath(this.machineId));
  }

  toFaultStopsPage() {
    this.router.navigate(machineFaultStopsPath(this.machineId));
  }

  onCreateTask() {
    this.modalService.openTaskModal(ModalModes.CREATE, { assignedAsset: this.machineId });
  }

  onCreateDocument() {
    this.modalService.openDocumentModal({
      document: { refIds: [this.machineId] },
      mode: ModalModes.CREATE,
    });
  }

  onCreateInstruction() {
    this.modalService.openWorkInstructionModal({
      instruction: { links: [this.machineId] },
      mode: ModalModes.CREATE,
    });
  }

  private calcTimePercentage(
    timeline: TimelineDto[],
    status: SynMachineState,
    rangeFilter: number,
  ): number {
    const seconds = timeline
      .filter(item => item.status === status)
      .reduce(
        (curr, prev) =>
          curr + Math.max(new Date(prev.to).getTime() - new Date(prev.from).getTime(), 0) / 1000,
        0,
      );
    const timePercent = rangeFilter / 100;
    return Math.round((seconds * 1000) / timePercent);
  }

  private getTimeDurationString(timeline: TimelineDto[], status: SynMachineState): string {
    const seconds = this.calcStatusTime(timeline, status);
    return moment.utc(seconds).format('HH:mm [h]');
  }

  private calcStatusTime(timeline: TimelineDto[], status: SynMachineState): number {
    return timeline
      .filter(item => item.status === status)
      .reduce(
        (curr, prev) =>
          curr + Math.max(new Date(prev.to).getTime() - new Date(prev.from).getTime(), 0),
        0,
      );
  }

  onAssetView(asset: AssetDto) {
    const dialogRef = this.modalService.openViewAssetModal(asset);
    dialogRef
      .afterClosed()
      .pipe(filter(action => action && action === AssetTreeElementActions.EDIT))
      .subscribe(() => this.modalService.openEditAssetModal(AssetType.MACHINE, asset));
  }
}
