import { Component, ViewChild } from '@angular/core';
import { UserListComponent } from 'src/app/components/user-list/user-list.component';
import { RemoteActions } from 'src/app/core/abstract';
import { NavbarActionsViewService } from 'src/app/core/services';
import { ModalService } from 'src/app/modals/modal.service';

@Component({
  selector: 'syn-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent extends RemoteActions {
  @ViewChild(UserListComponent)
  userTable!: UserListComponent;

  constructor(navActions: NavbarActionsViewService, private modalService: ModalService) {
    super(navActions);
  }

  onNewUser() {
    this.modalService.openUserModal();
  }

  onSearch(filterValue: string) {
    this.userTable.selectedFilters.name = filterValue.trim().toLowerCase();
    this.userTable.applyFilter();
  }
}
