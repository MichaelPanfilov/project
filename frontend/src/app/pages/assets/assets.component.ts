import { Component, ViewChild } from '@angular/core';
import { AssetDto, AssetType, ASSET_TYPE_LABELS, toTranslatable } from '@common';
import { Store } from '@ngrx/store';
import { startWith } from 'rxjs/operators';
import { AssetTreeComponent } from 'src/app/components';
import { RemoteActions } from 'src/app/core/abstract';
import { NavbarActionsViewService } from 'src/app/core/services';
import { Asset, State } from 'src/app/core/store';
import { ModalService } from 'src/app/modals/modal.service';

@Component({
  selector: 'syn-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.scss'],
})
export class AssetsComponent extends RemoteActions {
  @ViewChild('tree') tree!: AssetTreeComponent;

  assetTrees$ = this.store.select(Asset.selectAllAssetTrees).pipe(startWith([]));
  dropdownItems = toTranslatable(ASSET_TYPE_LABELS);

  constructor(
    navActions: NavbarActionsViewService,
    private store: Store<State>,
    private modalService: ModalService,
  ) {
    super(navActions);
  }

  createAsset(type: AssetType) {
    this.modalService.openAssetModal(type);
  }

  onEdit(asset: AssetDto) {
    this.modalService.openAssetModal(asset.type, asset);
  }

  collapseAll() {
    this.tree.collapseAll();
  }

  expandAll() {
    this.tree.expandAll();
  }
}
