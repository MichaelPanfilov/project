import { Overlay } from '@angular/cdk/overlay';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkTreeModule } from '@angular/cdk/tree';
import { registerLocaleData } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import localeEn from '@angular/common/locales/en-GB';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule, MatSpinner } from '@angular/material/progress-spinner';
import { MatSelectModule, MAT_SELECT_SCROLL_STRATEGY } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  OwlDateTimeModule,
  OwlNativeDateTimeModule,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
} from '@danielmoncada/angular-datetime-picker';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ClickOutsideModule } from 'ng-click-outside';
import { ChartsModule } from 'ng2-charts';
import { CookieService } from 'ngx-cookie-service';
import { NgxGaugeModule } from 'ngx-gauge';

import { environment } from '../environments/environment';

import { initApp } from './app-init';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  AssetCardComponent,
  AssetImageComponent,
  AssetPageContainerComponent,
  AssetSelectComponent,
  AssetSelectTreeComponent,
  AssetTreeComponent,
  AssetTreeElementComponent,
  AvatarComponent,
  BreadcrumbsComponent,
  DocumentListComponent,
  ErrorListComponent,
  FactoryViewFieldsComponent,
  FaultStopsHistogramComponent,
  FaultStopsTimelineComponent,
  FilterSelectComponent,
  FooterComponent,
  GaugeComponent,
  LineViewFieldsComponent,
  LinkDocumentsComponent,
  LinkInstructionsComponent,
  LinkTagsComponent,
  LinkTaskComponent,
  MachineButtonComponent,
  MachineViewFieldsComponent,
  NavigationBarComponent,
  NewDropdownComponent,
  PageContainerComponent,
  PerformanceRangeFilterComponent,
  ProductLossesHistogramComponent,
  QrFabComponent,
  RangeComponent,
  SidebarComponent,
  StateTimelineComponent,
  TableActionsComponent,
  TaskListComponent,
  UploadProgressComponent,
  UserListComponent,
  UserViewFieldsComponent,
  WorkInstructionListComponent,
} from './components';
import {
  AclDirective,
  ButtonDirective,
  ClickActiveDirective,
  FileValueAccessorDirective,
  SearchInputDirective,
} from './core/directives';
import { AuthGuard, NonAuthGuard } from './core/guards';
import { AuthInterceptor, ErrorInterceptor } from './core/interceptors';
import { FormatNumberPipe, HideMorePipe, StartCasePipe, TruncateTextPipe } from './core/pipes';
import { AssetResolver } from './core/resolvers';
import { AuthService } from './core/services';
import { appEffects, appReducers, metaReducers } from './core/store';
import {
  AssetFormComponent,
  DocumentFormComponent,
  FactoryFormComponent,
  FormControlComponent,
  LineFormComponent,
  MachineFormComponent,
  MachineTypeTagFormComponent,
  MappingFormComponent,
  TaskFormComponent,
  UserFormComponent,
  WorkInstructionFormComponent,
} from './forms';
import {
  AboutComponent,
  AssetModalComponent,
  ConfirmationModalComponent,
  DocumentComponent,
  ImprintComponent,
  LicensesComponent,
  MachineTypeTagComponent,
  MappingComponent,
  ModalContainerComponent,
  QrCodeScanComponent,
  QrCodeViewComponent,
  TaskModalComponent,
  UserComponent,
  ViewAssetComponent,
  ViewTaskComponent,
  ViewUserComponent,
  WorkInstructionComponent,
} from './modals';
import {
  AssetsComponent,
  DocumentManagementComponent,
  LineDetailComponent,
  LinesComponent,
  LoginComponent,
  MachineMetricComponent,
  MachinePerformanceComponent,
  MachineTimeProducingComponent,
  TasksComponent,
  UsersComponent,
  WorkInstructionsComponent,
} from './pages';
import { CustomMatPaginatorIntl, DATE_FORMATS } from './util';

registerLocaleData(localeEn, 'en-GB');

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json?v=' + Date.now());
}

export function ScrollStrategyFactory(overlay: Overlay) {
  return () => overlay.scrollStrategies.reposition();
}

const MAT_MODULES = [
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatIconModule,
  MatAutocompleteModule,
  MatDialogModule,
  MatNativeDateModule,
  MatTableModule,
  MatPaginatorModule,
  MatChipsModule,
  MatTooltipModule,
  MatSortModule,
  MatMenuModule,
  MatSnackBarModule,
  MatCheckboxModule,
  MatRippleModule,
  MatTabsModule,
  MatSidenavModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatGridListModule,
  MatChipsModule,
  MatTooltipModule,
  ScrollingModule,
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LinesComponent,
    LineDetailComponent,
    MachinePerformanceComponent,
    PerformanceRangeFilterComponent,
    SidebarComponent,
    NavigationBarComponent,
    AssetCardComponent,
    MachineButtonComponent,
    FormatNumberPipe,
    RangeComponent,
    PageContainerComponent,
    GaugeComponent,
    ErrorListComponent,
    ConfirmationModalComponent,
    StateTimelineComponent,
    NewDropdownComponent,
    AssetSelectComponent,
    AssetSelectTreeComponent,
    MachineTimeProducingComponent,
    AssetTreeElementComponent,
    UsersComponent,
    TasksComponent,
    TruncateTextPipe,
    AssetTreeComponent,
    MachineFormComponent,
    LineFormComponent,
    FactoryFormComponent,
    FormControlComponent,
    TaskModalComponent,
    ViewTaskComponent,
    AssetsComponent,
    AssetPageContainerComponent,
    AvatarComponent,
    TaskListComponent,
    MachineMetricComponent,
    DocumentManagementComponent,
    FilterSelectComponent,
    QrCodeViewComponent,
    TableActionsComponent,
    QrCodeScanComponent,
    WorkInstructionsComponent,
    WorkInstructionFormComponent,
    DocumentComponent,
    DocumentFormComponent,
    DocumentListComponent,
    WorkInstructionListComponent,
    QrFabComponent,
    WorkInstructionComponent,
    HideMorePipe,
    ModalContainerComponent,
    TaskFormComponent,
    ButtonDirective,
    ClickActiveDirective,
    AclDirective,
    StartCasePipe,
    LicensesComponent,
    AssetFormComponent,
    ViewAssetComponent,
    FactoryViewFieldsComponent,
    LineViewFieldsComponent,
    MachineViewFieldsComponent,
    UserFormComponent,
    UserComponent,
    ViewUserComponent,
    UserViewFieldsComponent,
    AssetModalComponent,
    UserListComponent,
    SearchInputDirective,
    FileValueAccessorDirective,
    AssetImageComponent,
    AssetCardComponent,
    FaultStopsHistogramComponent,
    ProductLossesHistogramComponent,
    FaultStopsTimelineComponent,
    LinkInstructionsComponent,
    LinkTagsComponent,
    LinkDocumentsComponent,
    LinkTaskComponent,
    MappingFormComponent,
    AboutComponent,
    FooterComponent,
    ImprintComponent,
    MappingComponent,
    MachineTypeTagComponent,
    MachineTypeTagFormComponent,
    BreadcrumbsComponent,
    UploadProgressComponent,
    TruncateTextPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(appReducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      },
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot(appEffects),
    AngularSvgIconModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      defaultLanguage: 'en',
    }),
    BrowserAnimationsModule,
    ChartsModule,
    ReactiveFormsModule,
    NgxGaugeModule,
    ClickOutsideModule,
    FormsModule,
    CdkTreeModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ZXingScannerModule,
    ...MAT_MODULES,
  ],
  providers: [
    AuthService,
    AuthGuard,
    NonAuthGuard,
    Storage,
    AssetResolver,
    CookieService,
    {
      provide: APP_INITIALIZER,
      useFactory: initApp,
      multi: true,
      deps: [AuthService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
    {
      provide: LOCALE_ID,
      useValue: 'en-GB',
    },
    {
      provide: OWL_DATE_TIME_FORMATS,
      useValue: DATE_FORMATS,
    },
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-GB' },
    { provide: MatPaginatorIntl, useClass: CustomMatPaginatorIntl, deps: [TranslateService] },
    { provide: MAT_SELECT_SCROLL_STRATEGY, useFactory: ScrollStrategyFactory, deps: [Overlay] },
  ],
  entryComponents: [
    ConfirmationModalComponent,
    TaskModalComponent,
    ViewTaskComponent,
    ErrorListComponent,
    QrCodeScanComponent,
    QrCodeViewComponent,
    WorkInstructionComponent,
    DocumentComponent,
    MatSpinner,
    ViewAssetComponent,
    UserComponent,
    ViewUserComponent,
    AssetModalComponent,
    AboutComponent,
    LicensesComponent,
    ImprintComponent,
    MappingComponent,
    MachineTypeTagComponent,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
