import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { debounceTime, distinctUntilChanged, scan } from 'rxjs/operators';

import { Settings, State } from '../../core/store';

@Component({
  selector: 'syn-upload-progress',
  templateUrl: './upload-progress.component.html',
  styleUrls: ['./upload-progress.component.scss'],
})
export class UploadProgressComponent {
  uploadProgress$ = this.store.select(Settings.selectUploadStatus).pipe(
    debounceTime(250),
    scan((acc, curr) => Math.max(acc, curr), 0),
    distinctUntilChanged(),
  );

  constructor(private store: Store<State>) {}
}
