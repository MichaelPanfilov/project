import { Component, Input } from '@angular/core';
import { MachineDto, SynMachineState } from '@common';

@Component({
  selector: 'syn-machine-button',
  templateUrl: './machine-button.component.html',
  styleUrls: ['./machine-button.component.scss'],
})
export class MachineButtonComponent {
  @Input() machine?: MachineDto;

  @Input()
  status: SynMachineState = 'Unknown';
}
