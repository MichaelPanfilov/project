import { Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ImageDto } from '@common';
import { of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { ImageService } from '../../core/services/image.service';

@Component({
  selector: 'syn-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnInit {
  private init: Promise<void>;
  private initCb!: () => void;

  @ViewChild('avatar', { static: true })
  avatar!: ElementRef;

  @Input()
  highlighted = false;

  @Input()
  alt = 'Image not found';

  @Input()
  placeholder = '';

  @Input()
  set imageData(imageData: ImageDto | undefined) {
    this.updateImg(imageData);
  }

  @Input()
  size = '48px';

  constructor(private renderer: Renderer2, private imageService: ImageService) {
    // imageData might be set before placeholder but we need it.
    // With this Promise we can wait for all inputs to be set before updating the avatar.
    this.init = new Promise(res => (this.initCb = res));
  }

  ngOnInit() {
    const el = this.avatar.nativeElement;
    this.renderer.setStyle(el, 'width', this.size);
    this.renderer.setStyle(el, 'height', this.size);
    this.initCb();
  }

  private async updateImg(imageData?: ImageDto) {
    await this.init;
    if (!imageData) {
      this.setStyle(this.placeholder);
      return;
    }

    this.imageService
      .getImageBlob(imageData)
      .pipe(
        take(1),
        catchError(() => of(null)),
        map(url => url || this.placeholder),
      )
      .subscribe(url => this.setStyle(url));
  }

  private setStyle(image: string): void {
    const el = this.avatar.nativeElement;
    this.renderer.setStyle(el, 'background-image', `url(${image})`);
    this.renderer.setStyle(el, 'background-size', 'cover');
  }
}
