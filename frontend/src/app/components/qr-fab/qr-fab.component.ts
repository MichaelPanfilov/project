import { Component } from '@angular/core';
import { ModalService } from 'src/app/modals/modal.service';

@Component({
  selector: 'syn-qr-fab',
  templateUrl: './qr-fab.component.html',
  styleUrls: ['./qr-fab.component.scss'],
})
export class QrFabComponent {
  constructor(private modalService: ModalService) {}

  openQrScanner() {
    this.modalService.openQrCodeScanModal();
  }
}
