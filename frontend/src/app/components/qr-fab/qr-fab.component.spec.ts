import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrFabComponent } from './qr-fab.component';

describe('QrFabComponent', () => {
  let component: QrFabComponent;
  let fixture: ComponentFixture<QrFabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QrFabComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrFabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
