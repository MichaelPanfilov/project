import { Component, Input } from '@angular/core';
import { ProductLossDto, TimeRange } from '@common';
import { ChartDataSets } from 'chart.js';
import { HistogramComponent } from 'src/app/core/abstract';
import { STATUS_COLORS } from 'src/app/util';

@Component({
  selector: 'syn-product-losses-histogram',
  templateUrl: './product-losses-histogram.component.html',
  styleUrls: ['./product-losses-histogram.component.scss'],
})
export class ProductLossesHistogramComponent extends HistogramComponent<ProductLossDto> {
  @Input()
  set productLosses(losses: ProductLossDto[]) {
    this.data = losses;
    this.buildHistogram(losses, this.range);
  }

  get productLosses() {
    return this.data;
  }

  protected getChartDataSets(losses: ProductLossDto[], range: TimeRange): ChartDataSets[] {
    const timeSegments = this.getTimeSegments(range, this.numSegments);
    const datasets: ChartDataSets[] = [
      {
        data: this.buildChartData(losses, timeSegments, range, this.highlighted),
        backgroundColor: STATUS_COLORS.Faulted,
        hoverBackgroundColor: STATUS_COLORS.Faulted,
        borderColor: STATUS_COLORS.Faulted,
        lineTension: 0,
        pointRadius: 0,
      },
      {
        data: this.buildChartData(losses, timeSegments, range, this.highlighted, true),
        backgroundColor: STATUS_COLORS.Offline,
        hoverBackgroundColor: STATUS_COLORS.Offline,
        borderColor: STATUS_COLORS.Offline,
        lineTension: 0,
        pointRadius: 0,
      },
    ];

    return datasets;
  }

  private buildChartData(
    data: ProductLossDto[],
    timeSegments: Date[],
    range: TimeRange,
    highlighted: string | null,
    inverse = false,
  ): number[] {
    return timeSegments.map((seg, i, arr) => {
      const next = arr[i + 1] || range.to;
      const stopsWithin = data.filter(({ time }) => new Date(time) > seg && new Date(time) <= next);
      const values = stopsWithin.filter(stop => {
        const marked = !highlighted || highlighted === stop.reason;
        return inverse ? !marked : marked;
      });
      return values.length;
    });
  }
}
