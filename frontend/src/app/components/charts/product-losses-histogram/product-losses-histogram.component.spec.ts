import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductLossesHistogramComponent } from './product-losses-histogram.component';

describe('ProductLossesHistogramComponent', () => {
  let component: ProductLossesHistogramComponent;
  let fixture: ComponentFixture<ProductLossesHistogramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductLossesHistogramComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductLossesHistogramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
