import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateTimelineComponent } from './state-timeline.component';

describe('StateTimelineComponent', () => {
  let component: StateTimelineComponent;
  let fixture: ComponentFixture<StateTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StateTimelineComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
