import { Component, Input } from '@angular/core';
import { TimelineDto, TimeRange } from '@common';
import * as moment from 'moment';

export interface TimePerformance extends TimelineDto {
  color: string;
  disableTooltip?: boolean;
}

interface TimeSections extends TimePerformance {
  percent?: number;
}

@Component({
  selector: 'syn-state-timeline',
  templateUrl: './state-timeline.component.html',
  styleUrls: ['./state-timeline.component.scss'],
})
export class StateTimelineComponent {
  private _data: TimeSections[] = [];

  @Input()
  set data(data: TimeSections[]) {
    this._data = data;
    this.sortData();
    this.calculatePercent();
    this.generateLabels();
  }

  get data() {
    return this._data;
  }

  @Input()
  numSegments = 15;

  timeArray: string[] = [];
  dateArray: string[] = [];
  tooltipText = '';

  get showDateAxis() {
    return this.dateArray.some(d => !!d.length);
  }

  sortData() {
    this.data.sort((a, b) => {
      if (new Date(a.from) > new Date(b.from)) {
        return 1;
      }
      if (new Date(a.from) < new Date(b.from)) {
        return -1;
      }
      return 0;
    });
  }

  calculatePercent() {
    const t = new Date(this.data[this.data.length - 1].to);
    const f = new Date(this.data[0].from);
    const lengthSec = Math.round((+t - +f) / 1000);

    let sum = 0;
    this._data = this.data.map(item => {
      const to = new Date(item.to);
      const from = new Date(item.from);
      const diffSec = Math.round((+to - +from) / 1000);
      const percent = Math.min(Math.round((diffSec / lengthSec) * 1000) / 10, 100);
      sum += percent;
      item.percent = percent;
      return item;
    });
    // I case we don´t end up with 100%, we prolong the last segment.
    if (sum < 100) {
      const last = this.data[this.data.length - 1];
      if (last) {
        last.percent = (last.percent as number) + sum - 100;
      }
    }
  }

  mouseEnter(item: TimeSections) {
    const to = new Date(item.to);
    const from = new Date(item.from);

    this.tooltipText = moment.utc(+to - +from).format('HH:mm:ss') /* + " " + item.percent + "%"*/;
  }

  generateLabels() {
    this.timeArray = [];
    this.dateArray = [];
    if (!this.data.length) {
      return;
    }

    const to = new Date(this.data[this.data.length - 1].to);
    const segments = this.getTimeSegments(
      { from: new Date(this.data[0].from), to },
      this.numSegments,
    );

    this.dateArray.push('');
    segments.forEach((seg, index, arr) => {
      const curr = moment(seg);
      this.timeArray.push(curr.format('HH:mm'));
      const next = arr[index + 1] || to;
      if (!curr.startOf('day').isSame(moment(next).startOf('day'))) {
        this.dateArray.push(moment(next).format('DD/MM'));
      } else {
        this.dateArray.push('');
      }
    });
    this.timeArray.push(moment(to).format('HH:mm'));
  }

  getStyle(item: TimeSections) {
    return {
      'background-color': item.color,
      width: (item.percent || 0) * 1 + '%',
      'min-width': '1px',
    };
  }

  private getTimeSegments(range: TimeRange, num: number): Date[] {
    const firstTs = range.from.getTime();
    const durationMs = range.to.getTime() - firstTs;
    return [...new Array(num).keys()].map(
      key => new Date((Number(key) / num) * durationMs + firstTs),
    );
  }
}
