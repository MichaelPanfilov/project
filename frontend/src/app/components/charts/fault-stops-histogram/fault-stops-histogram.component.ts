import { Component, Input } from '@angular/core';
import { FaultStopDto, TimeRange } from '@common';
import { ChartDataSets } from 'chart.js';
import { HistogramComponent } from 'src/app/core/abstract';
import { STATUS_COLORS } from 'src/app/util';

@Component({
  selector: 'syn-fault-stops-histogram',
  templateUrl: './fault-stops-histogram.component.html',
  styleUrls: ['./fault-stops-histogram.component.scss'],
})
export class FaultStopsHistogramComponent extends HistogramComponent<FaultStopDto> {
  @Input()
  set faultStops(stops: FaultStopDto[]) {
    this.data = stops;
    this.buildHistogram(stops, this.range);
  }

  get faultStops() {
    return this.data;
  }

  protected getChartDataSets(stops: FaultStopDto[], range: TimeRange): ChartDataSets[] {
    const timeSegments = this.getTimeSegments(range, this.numSegments);
    return [
      {
        data: timeSegments.map((seg, i, arr) => {
          const next = arr[i + 1] || range.to;
          const stopsWithin = stops.filter(
            stop => new Date(stop.from) > seg && new Date(stop.from) <= next,
          );
          const values = stopsWithin.filter(
            stop => !this.highlighted || this.highlighted === stop.reason,
          );
          return values.length;
        }),
        backgroundColor: STATUS_COLORS.Faulted,
        hoverBackgroundColor: STATUS_COLORS.Faulted,
        borderColor: STATUS_COLORS.Faulted,
        lineTension: 0,
        pointRadius: 0,
      },
      {
        data: timeSegments.map((seg, i, arr) => {
          const next = arr[i + 1] || range.to;
          const stopsWithin = stops.filter(
            stop => new Date(stop.from) > seg && new Date(stop.from) <= next,
          );
          const values = stopsWithin.filter(
            stop => !(!this.highlighted || this.highlighted === stop.reason),
          );
          return values.length;
        }),
        backgroundColor: STATUS_COLORS.Offline,
        hoverBackgroundColor: STATUS_COLORS.Offline,
        borderColor: STATUS_COLORS.Offline,
        lineTension: 0,
        pointRadius: 0,
      },
    ];
  }
}
