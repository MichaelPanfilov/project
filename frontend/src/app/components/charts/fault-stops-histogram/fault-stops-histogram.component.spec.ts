import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaultStopsHistogramComponent } from './fault-stops-histogram.component';

describe('FaultStopsHistogramComponent', () => {
  let component: FaultStopsHistogramComponent;
  let fixture: ComponentFixture<FaultStopsHistogramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FaultStopsHistogramComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaultStopsHistogramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
