import { Component, Input } from '@angular/core';
import { FaultStopDto, TimeRange } from '@common';
import { STATUS_COLORS } from 'src/app/util';

import { TimePerformance } from '../state-timeline/state-timeline.component';

@Component({
  selector: 'syn-fault-stops-timeline',
  templateUrl: './fault-stops-timeline.component.html',
  styleUrls: ['./fault-stops-timeline.component.scss'],
})
export class FaultStopsTimelineComponent {
  private _range: TimeRange = { from: new Date(), to: new Date() };
  private _faultStops: FaultStopDto[] = [];
  private highlighted: string | null = null;

  @Input()
  set faultStops(stops: FaultStopDto[]) {
    this._faultStops = stops || [];
    this.buildTimeline(stops || [], this.range);
  }

  get faultStops() {
    return this._faultStops;
  }

  @Input()
  set range(range: TimeRange) {
    this._range = range;
    this.buildTimeline(this.faultStops, range);
  }

  get range() {
    return this._range;
  }

  @Input()
  set highlightedReason(reason: string | null) {
    this.highlighted = reason;
    this.buildTimeline(this.faultStops, this.range);
  }

  timeline: TimePerformance[] = [];

  private buildTimeline(stops: FaultStopDto[], range: TimeRange) {
    this.timeline = stops.reduce((prev, curr, index, arr) => {
      const next = arr[index + 1] as FaultStopDto | undefined;
      if (index === 0 && range.from.getTime() < (next || curr).from.getTime()) {
        // Insert one empty element at the start before the first fault stop.
        prev = prev.concat(this.getDummySegment(range.from, (next || curr).from));
      }

      // Fault stop item to be inserted.
      const item: TimePerformance = {
        color: this.getSegmentColor(curr.reason),
        from: curr.from,
        to: curr.to,
        status: 'Faulted',
        disableTooltip: (this.highlighted && this.highlighted !== curr.reason) || undefined,
      };

      if (next && curr.to.getTime() < next.from.getTime()) {
        // Insert empty item between two fault stops.
        return prev.concat(item, this.getDummySegment(curr.to, next.from));
      } else if (!next && curr.to.getTime() < range.to.getTime()) {
        // insert one empty element at the end.
        return prev.concat(item, this.getDummySegment(curr.to, range.to));
      }
      return prev.concat(item);
    }, [] as TimePerformance[]);
  }

  private getSegmentColor(reason: string) {
    return !this.highlighted || this.highlighted === reason
      ? STATUS_COLORS.Faulted
      : STATUS_COLORS.Offline;
  }

  private getDummySegment(from: Date, to: Date): TimePerformance {
    return {
      color: '#ffffff',
      from,
      to,
      status: 'Unknown',
      disableTooltip: true,
    };
  }
}
