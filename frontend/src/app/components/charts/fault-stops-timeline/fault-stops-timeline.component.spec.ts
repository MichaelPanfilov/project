import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaultStopsTimelineComponent } from './fault-stops-timeline.component';

describe('FaultStopsTimelineComponent', () => {
  let component: FaultStopsTimelineComponent;
  let fixture: ComponentFixture<FaultStopsTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FaultStopsTimelineComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaultStopsTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
