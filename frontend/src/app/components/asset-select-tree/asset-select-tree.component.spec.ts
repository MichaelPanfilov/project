import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetSelectTreeComponent } from './asset-select-tree.component';

describe('DropdownItemComponent', () => {
  let component: AssetSelectTreeComponent;
  let fixture: ComponentFixture<AssetSelectTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssetSelectTreeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetSelectTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
