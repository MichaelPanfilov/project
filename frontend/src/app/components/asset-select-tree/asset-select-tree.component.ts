import { ArrayDataSource, SelectionModel } from '@angular/cdk/collections';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AssetType, getChildrenCount } from '@common';

import { AssetSelectItem } from '../../util';

@Component({
  selector: 'syn-asset-select-tree',
  templateUrl: './asset-select-tree.component.html',
  styleUrls: ['./asset-select-tree.component.scss'],
})
export class AssetSelectTreeComponent {
  private selection = new SelectionModel<AssetSelectItem>(false, []);

  @Input()
  disableCollapse?: boolean;

  @Input()
  set assets(assets: AssetSelectItem[]) {
    this.dataSource = new ArrayDataSource(assets);
    if (this.disableCollapse) {
      this.treeControl.dataNodes = assets;
      this.treeControl.expandAll();
    }
  }

  @Input()
  set multiple(value: boolean) {
    if (value !== this.selection.isMultipleSelection()) {
      this.selection = new SelectionModel<AssetSelectItem>(value, this.selection.selected);
    }
  }

  get multiple() {
    return this.selection.isMultipleSelection();
  }

  @Input()
  set selectedNodes(assets: AssetSelectItem[]) {
    if (assets.length > 1) {
      this.multiple = true;
    }
    this.selection.clear();
    this.selection.select(...assets);
  }

  @Output()
  selectedNodesChange = new EventEmitter<AssetSelectItem[]>();

  treeControl = new NestedTreeControl<AssetSelectItem>(node => node.children);
  dataSource!: ArrayDataSource<AssetSelectItem>;
  assetTypes = AssetType;

  getChildrenCount(node: AssetSelectItem): number {
    return node.childrenCount || getChildrenCount(node);
  }

  isSelected(node: AssetSelectItem) {
    return this.selection.selected.some(s => s.id === node.id);
  }

  onSelectItem(event: Event, node: AssetSelectItem) {
    event.preventDefault();
    event.stopPropagation();
    if (!node.disabled) {
      // Clicked the already selected item.
      if (!this.multiple && this.selection.selected.includes(node)) {
        this.selectedNodesChange.emit(this.selection.selected);
        return;
      }

      this.selection.toggle(node);
      this.selectedNodesChange.emit(this.selection.selected);
      if (!this.multiple) {
        this.treeControl.collapseAll();
      }
    }
  }
}
