import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AssetDto, AssetType } from '@common';
import { Store } from '@ngrx/store';
import { merge, Subject } from 'rxjs';
import { filter, map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { Asset, State } from 'src/app/core/store';

interface NavigationSegment {
  title: string;
  path: string[];
  action?: () => void;
}

@Component({
  selector: 'syn-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss'],
})
export class BreadcrumbsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private assets: AssetDto[] = [];

  breadcrumbHistory: NavigationSegment[] = [];

  get isMainPage() {
    return this.router.url.endsWith('/lines');
  }

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit() {
    merge(
      ...[
        Asset.selectCurrentFactory,
        Asset.selectCurrentLine,
        Asset.selectCurrentMachine,
      ].map(selector => this.store.select(selector)),
    )
      .pipe(
        takeUntil(this.destroy$),
        filter(asset => !!asset),
        switchMap(asset => this.store.select(Asset.selectAssetHierarchyOrder(asset as AssetDto))),
      )
      .subscribe(assets => (this.assets = assets));

    this.router.events
      .pipe(
        takeUntil(this.destroy$),
        filter(ev => ev instanceof NavigationEnd),
        map(ev => (ev as NavigationEnd).urlAfterRedirects),
        // Initial page load.
        startWith(this.router.url),
      )
      .subscribe(() => this.updateBreadcrumbs());
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onNavigate(nav: NavigationSegment): void {
    if (nav.action) {
      nav.action();
    }
    this.router.navigate(nav.path);
  }

  private updateBreadcrumbs() {
    const assets = this.isMainPage && this.assets.length ? [this.assets[0]] : this.assets;
    if (this.router.url.includes('/lines') && assets.length > 2) {
      assets.pop();
    }

    if (this.assets.length && this.assets[0].type !== AssetType.FACTORY) {
      this.breadcrumbHistory = [
        {
          title: 'Shopfloor',
          path: ['/lines'],
        },
      ];
      return;
    }

    this.breadcrumbHistory = assets.map(asset => {
      switch (asset.type) {
        case AssetType.FACTORY:
          return {
            title: !this.isMainPage ? asset.name : 'Shopfloor',
            path: ['/lines'],
          };
        case AssetType.LINE:
          return {
            title: asset.name,
            path: ['/lines', asset.id],
          };
        case AssetType.MACHINE:
          return {
            title: asset.name,
            path: ['/machines', asset.id],
          };
        default:
          return {
            title: asset.name,
            path: ['/lines'],
          };
      }
    });

    if (!this.breadcrumbHistory.length) {
      this.breadcrumbHistory = [
        {
          title: 'Shopfloor',
          path: ['/lines'],
        },
      ];
    }
  }
}
