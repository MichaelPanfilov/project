import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDropdownComponent } from './new-dropdown.component';

describe('DropdownComponent', () => {
  let component: NewDropdownComponent;
  let fixture: ComponentFixture<NewDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewDropdownComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
