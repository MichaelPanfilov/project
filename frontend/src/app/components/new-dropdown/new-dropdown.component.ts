import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'syn-new-dropdown',
  templateUrl: './new-dropdown.component.html',
  styleUrls: ['./new-dropdown.component.scss'],
})
export class NewDropdownComponent {
  @Input()
  placeholder = '';

  @Input()
  items: { label: string; value: string }[] = [];

  @Output()
  valueChange = new EventEmitter<string>();

  isOpen = false;

  onSelectedItem(value: string) {
    this.valueChange.emit(value);
    this.isOpen = false;
  }
}
