import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, Input, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {
  CLASSIFICATIONS,
  getClassificationLabel,
  getEntityFromRefId,
  InstructionDto,
  ParsedFilter,
  ReferencedEntities,
} from '@common';
import { CondOperator } from '@nestjsx/crud-request/lib';
import { Store } from '@ngrx/store';
import * as normalizeUrl from 'normalize-url';
import { combineLatest, Observable, of } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { Column, TableComponent } from 'src/app/core/abstract';
import { InstructionService } from 'src/app/core/services';
import { Asset, Instruction, MachineTypeTag, Settings, State } from 'src/app/core/store';
import { ModalService } from 'src/app/modals/modal.service';
import {
  ActionOption,
  AssetSelectItem,
  Filter,
  FilterOption,
  InstructionDataSource,
  ModalModes,
  parseFilter,
  wrapToLoadedObservable,
} from 'src/app/util';

export type InstructionTableDisplayMode = 'asset' | 'all';

enum MenuActions {
  EDIT = 'edit',
  DELETE = 'delete',
  VIEW_QR = 'view_qr',
}

type TableColumn = keyof InstructionDto | 'actions';

const DATE_FIELD = 'createdAt';

const ALL_COLUMNS: Column<TableColumn>[] = [
  'title',
  'classification',
  { name: 'machineTypes', breakpoints: ['(min-width: 1023px)'] },
  { name: 'links', breakpoints: ['(min-width: 839px)'] },
  DATE_FIELD,
  'actions',
];

const ASSET_COLUMNS: Column<TableColumn>[] = ['title', 'classification', 'actions'];

const COLUMN_CONFIG: { [key in InstructionTableDisplayMode]: Column<TableColumn>[] } = {
  all: ALL_COLUMNS,
  asset: ASSET_COLUMNS,
};

@Component({
  selector: 'syn-work-instruction-list',
  templateUrl: './work-instruction-list.component.html',
  styleUrls: ['./work-instruction-list.component.scss'],
})
export class WorkInstructionListComponent extends TableComponent<InstructionDto> {
  private defaultFilter: ParsedFilter<InstructionDto> = {};

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  @Input()
  set refId(refId: string) {
    this.defaultFilter = {
      links: {
        value: refId,
        operator: CondOperator.IN,
      },
    };
    this.applyFilter();
  }

  @Input()
  set mode(mode: InstructionTableDisplayMode) {
    this.columns = COLUMN_CONFIG[mode];
    this.data = [];
    this.applyFilter();
  }

  @Input()
  set titleWidth(width: number) {
    this.style.width = width + '%';
  }

  @Input()
  hideFilter = false;

  style = { width: '' };

  readonly actions: ActionOption<MenuActions>[] = [
    {
      label: 'COMMON.ACTION.EDIT',
      value: MenuActions.EDIT,
      acl: { resources: ['Instruction'], right: 'Update' },
    },
    {
      label: 'COMMON.ACTION.VIEW_QR_CODE',
      value: MenuActions.VIEW_QR,
    },
    {
      label: 'COMMON.ACTION.DELETE',
      value: MenuActions.DELETE,
      acl: { resources: ['Instruction'], right: 'Delete' },
    },
  ];

  selectedFilters: Filter<InstructionDto> & { refIds?: string[] } = {
    refIds: [],
    classification: [],
    machineTypes: [],
    title: '',
  };

  applyFilter() {
    this.dataSource.resetPage();
    const { refIds, ...rest } = this.selectedFilters;
    const parsedFilter = parseFilter({ ...rest, links: this.selectedFilters.refIds });
    this.dataSource.loadInstructions({
      ...parsedFilter,
      ...this.defaultFilter,
    });
  }
  dataSource = new InstructionDataSource(this.store, `instruction-table`);
  classifications: FilterOption[] = CLASSIFICATIONS;
  machineTypesTagsOptions$: Observable<FilterOption[]> = this.store
    .select(MachineTypeTag.selectAll)
    .pipe(map(tags => tags.map(t => ({ value: t, label: t }))));
  assetTrees$: Observable<AssetSelectItem[]> = this.store.select(Asset.selectAllAssetTrees);

  loading$ = wrapToLoadedObservable(
    combineLatest(
      this.dataSource.loading$.pipe(map(loading => !loading)),
      this.store.select(Settings.selectLoadingByAction(Instruction.deleteInstruction)),
    ),
  ).pipe(map(loaded => !loaded));

  constructor(
    private store: Store<State>,
    private instructionService: InstructionService,
    private modalService: ModalService,
    breakpointObserver: BreakpointObserver,
  ) {
    super(breakpointObserver, []);
    this.store.dispatch(MachineTypeTag.loadMachineTypeTags({}));
  }

  onSearch(input: string) {
    this.selectedFilters.title = input.trim().toLowerCase();
    this.applyFilter();
  }

  getLinkedAssets(instruction: InstructionDto): Observable<string[]> {
    if (!(instruction.links && instruction.links.length)) {
      return of([]);
    }

    const assetIds = instruction.links.reduce((res: string[], next) => {
      const entity = getEntityFromRefId(next);
      if (entity && entity === ReferencedEntities.ASSET) {
        return [...res, next];
      }
      return res;
    }, []);

    return this.store.select(Asset.selectAllAssets).pipe(
      map(assets =>
        assets.reduce((res: string[], next) => {
          if (assetIds.includes(next.id)) {
            return [...res, next.name];
          }
          return res;
        }, []),
      ),
    );
  }

  getClassificationLabel(value: string) {
    return getClassificationLabel(value);
  }

  openDeleteConfirmModal(instruction: InstructionDto) {
    const dialogRef = this.modalService.openDeleteModal({
      entity: 'work instruction',
      name: instruction.title,
    });

    return dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter(res => !!res),
      )
      .subscribe(() =>
        this.store.dispatch(Instruction.deleteInstruction({ instructionId: instruction.id })),
      );
  }

  openEditModal(instruction: InstructionDto) {
    this.modalService.openWorkInstructionModal({ instruction, mode: ModalModes.CREATE });
  }

  onActionClick(action: ActionOption<MenuActions>, instruction: InstructionDto) {
    switch (action.value) {
      case MenuActions.EDIT: {
        return this.openEditModal(instruction as InstructionDto);
      }
      case MenuActions.VIEW_QR: {
        this.instructionService
          .getInstructionQrCode(instruction.id)
          .pipe(take(1))
          .subscribe(code =>
            this.modalService.openQrCodeViewModal(code, instruction.url, instruction.title),
          );
        return;
      }
      case MenuActions.DELETE: {
        return this.openDeleteConfirmModal(instruction);
      }
      default:
        return;
    }
  }

  openInstruction(instruction: InstructionDto): void {
    window.open(normalizeUrl(instruction.url), '_blank');
  }
}
