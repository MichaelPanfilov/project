import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkInstructionListComponent } from './work-instruction-list.component';

describe('WorkInstructionListComponent', () => {
  let component: WorkInstructionListComponent;
  let fixture: ComponentFixture<WorkInstructionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WorkInstructionListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkInstructionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
