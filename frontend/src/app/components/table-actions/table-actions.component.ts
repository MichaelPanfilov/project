import { ConnectedPosition, Overlay } from '@angular/cdk/overlay';
import { DOCUMENT } from '@angular/common';
import {
  Component,
  ElementRef,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { fromEvent } from 'rxjs';
import { debounceTime, filter, takeUntil } from 'rxjs/operators';
import { DropdownMenu } from 'src/app/core/abstract';
import { AuthService } from 'src/app/core/services';
import { ActionOption } from 'src/app/util';

@Component({
  selector: 'syn-table-actions',
  templateUrl: './table-actions.component.html',
  styleUrls: ['./table-actions.component.scss'],
})
export class TableActionsComponent extends DropdownMenu implements OnInit {
  protected positions: ConnectedPosition[] = [
    { originX: 'start', originY: 'top', overlayX: 'end', overlayY: 'top' },
    { originX: 'start', originY: 'bottom', overlayX: 'end', overlayY: 'bottom' },
  ];
  protected defaultOffsetX = -55;

  @ViewChild('dropdownMenu')
  menu!: ElementRef<HTMLDivElement>;

  @Input()
  label?: string;

  @Input()
  menuActions!: ActionOption<string>[];

  @Output()
  actionSelected = new EventEmitter<ActionOption<string>>();

  actions: ActionOption<string>[] = [];

  constructor(
    overlay: Overlay,
    viewContainerRef: ViewContainerRef,
    router: Router,
    private authService: AuthService,
    @Inject(DOCUMENT) private doc: Document,
  ) {
    super(overlay, viewContainerRef, router);
  }

  ngOnInit() {
    super.ngOnInit();
    this.actions = this.menuActions.filter(
      action => !action.acl || this.authService.hasRight([action.acl]),
    );

    // Needed to trigger change detection and flip the components template.
    fromEvent(this.doc, 'scroll')
      .pipe(
        takeUntil(this.destroy$),
        filter(() => this.isOpen),
        debounceTime(25),
      )
      .subscribe(() => {});
  }

  onSelectAction(action: ActionOption<string>) {
    this.actionSelected.emit(action);
  }
}
