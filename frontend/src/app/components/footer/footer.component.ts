import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { take } from 'rxjs/operators';
import { ModalService } from 'src/app/modals/modal.service';
import { FRONTEND_VERSION, SHOPFLOOR_VERSION } from 'src/environments/version';

@Component({
  selector: 'syn-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  copyrightYear = '2020';
  companyName = 'Syntegon Technology GmbH';
  // currentVersion = '1.2';
  frontendVersion = FRONTEND_VERSION;
  shopfloorVersion = SHOPFLOOR_VERSION;

  constructor(private http: HttpClient, public modalService: ModalService) {}

  loadTermsOfUse() {
    return this.http.get('/assets/about/terms-of-use.en.txt', { responseType: 'text' });
  }

  openLicensesModal() {
    return this.modalService.openLicensesModal();
  }

  openImprintModal() {
    return this.modalService.openImprintModal();
  }

  openPrivacyModal() {
    return this.modalService.openAboutModal({
      text:
        '<a href="https://www.syntegon.com/privacy-statement" target="_blank">https://www.syntegon.com/privacy-statement</a>',
      title: 'MISC.LABEL.DATA_PROTECTION_POLICY',
    });
  }

  openTermsOfUseModal() {
    this.loadTermsOfUse()
      .pipe(take(1))
      .subscribe(text => this.modalService.openAboutModal({ text }));
  }
}
