import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppUser, ImageDto, RoleDto, USER_AVATAR_TAG } from '@common';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';
import { Column, TableComponent } from 'src/app/core/abstract';
import { Image, Settings, State, User } from 'src/app/core/store';
import { ModalService } from 'src/app/modals/modal.service';
import {
  ActionOption,
  Filter,
  parseFilter,
  UserDataSource,
  UserTableActions,
  wrapToLoadedObservable,
} from 'src/app/util';

type TableColumn = keyof AppUser | 'roles' | 'avatar' | 'actions';

const ROLES_FIELD = 'roles';

const DISPLAYED_COLUMNS: Column<TableColumn>[] = ['avatar', 'name', ROLES_FIELD, 'actions'];

const TABLE_ACTIONS: ActionOption<UserTableActions>[] = [
  {
    label: 'COMMON.ACTION.EDIT',
    value: UserTableActions.EDIT,
    acl: {
      resources: ['User'],
      right: 'Update',
    },
  },
  {
    label: 'COMMON.ACTION.DELETE',
    value: UserTableActions.DELETE,
    acl: {
      resources: ['User'],
      right: 'Delete',
    },
  },
];

@Component({
  selector: 'syn-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent extends TableComponent<AppUser> implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  loading$ = wrapToLoadedObservable(
    this.store.select(Settings.selectLoadingByAction(User.loadUserTable)),
  ).pipe(map(loading => !loading));

  me?: AppUser;
  roles: RoleDto[] = [];
  images: ImageDto[] = [];
  placeholder = `../../../assets/imgs/avatar-placeholder.png`;

  dataSource = new UserDataSource(this.store, 'user-table');
  selectedFilters: Filter<AppUser> = { name: '' };

  constructor(
    private store: Store<State>,
    private modalService: ModalService,
    breakpointObserver: BreakpointObserver,
  ) {
    super(breakpointObserver, DISPLAYED_COLUMNS);
  }

  async ngOnInit() {
    this.applyFilter();
    this.store.dispatch(User.loadRoles({}));
    this.store.dispatch(Image.loadImagesData({ tag: USER_AVATAR_TAG }));

    this.store
      .select(Image.selectImagesByType(USER_AVATAR_TAG))
      .pipe(takeUntil(this.destroy$))
      .subscribe(images => (this.images = images));

    this.store
      .select(User.selectMe)
      .pipe(takeUntil(this.destroy$))
      .subscribe(me => (this.me = me));

    this.store
      .select(User.selectRoles)
      .pipe(takeUntil(this.destroy$))
      .subscribe(roles => (this.roles = roles));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  applyFilter() {
    this.dataSource.resetPage();
    const parsedFilter = parseFilter(this.selectedFilters);
    this.dataSource.loadUsers(parsedFilter);
  }

  getTableActions(user: AppUser): ActionOption<UserTableActions>[] {
    if (this.me && this.me.id === user.id) {
      // User cannot delete himself, we filter that option.
      return TABLE_ACTIONS.filter(action => action.value !== UserTableActions.DELETE);
    }
    return TABLE_ACTIONS;
  }

  onUserView(user: AppUser) {
    const dialogRef = this.modalService.openViewUserModal(user);
    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe(action => {
        if (action && action === UserTableActions.EDIT) {
          this.modalService.openUserModal(user);
        }
      });
  }

  onActionClick(action: ActionOption<UserTableActions>, user: AppUser): void {
    switch (action.value) {
      case UserTableActions.EDIT: {
        return this.openEditModal(user);
      }
      case UserTableActions.DELETE: {
        return this.openDeleteConfirmModal(user);
      }
      default:
        return;
    }
  }

  openEditModal(user: AppUser): void {
    this.modalService.openUserModal(user);
  }

  openDeleteConfirmModal(user: AppUser): void {
    const dialogRef = this.modalService.openDeleteModal({ entity: 'user', name: user.name });

    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe(confirm => {
        if (confirm) {
          this.store.dispatch(User.deleteUser({ userId: user.id }));
        }
      });
  }

  getRole(user: AppUser) {
    const roles = this.roles.filter(r => user.roleIds.includes(r.id));
    return roles.length ? roles[0].name : '-';
  }

  getImage(user: AppUser): ImageDto | undefined {
    return this.images.find(({ refIds }) => refIds && refIds.some(refId => refId === user.id));
  }
}
