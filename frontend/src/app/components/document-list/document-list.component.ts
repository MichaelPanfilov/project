import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, Input, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {
  BaseAssetTree,
  BaseDto,
  FileDto,
  FilesFilter,
  getRefIdsOf,
  ReferencedEntities,
} from '@common';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, Subject } from 'rxjs';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { Column, TableComponent } from 'src/app/core/abstract';
import { FileService } from 'src/app/core/services';
import { Asset, Document, MachineTypeTag, Settings, State } from 'src/app/core/store';
import { ModalService } from 'src/app/modals/modal.service';
import {
  ActionOption,
  DocumentDataSource,
  DocumentFilter,
  DOC_TYPE_OPTIONS,
  FilterConfig,
  FilterOption,
  ModalModes,
  wrapToLoadedObservable,
} from 'src/app/util';

export type DocTableDisplayMode = 'asset' | 'all';

enum DropdownActions {
  EDIT = 'edit',
  VIEW_QR = 'view_qr',
  DELETE = 'delete',
}

type TableColumn = keyof FileDto | 'assets' | 'actions' | 'contentTypeGroup';
type FilterKeys = keyof DocumentFilter | 'documentGroup';

type DocumentFilterConfig = {
  [key in FilterKeys]?: FilterConfig;
};

const DATE_FIELD = 'createdAt';

const OVERVIEW_COLUMNS: Column<TableColumn>[] = [
  'title',
  { name: 'machineTypeTags', breakpoints: ['(min-width: 1023px)'] },
  'assets',
  { name: 'contentTypeGroup', breakpoints: ['(min-width: 1279px)'] },
  DATE_FIELD,
  'actions',
];

const ASSET_COLUMNS: Column<TableColumn>[] = ['title', 'actions', 'contentTypeGroup'];

const COLUMN_CONFIG: { [key in DocTableDisplayMode]: Column<TableColumn>[] } = {
  all: OVERVIEW_COLUMNS,
  asset: ASSET_COLUMNS,
};

@Component({
  selector: 'syn-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss'],
})
export class DocumentListComponent extends TableComponent<FileDto> implements OnDestroy {
  private destroy$: Subject<void> = new Subject();
  private defaultFilter: FilesFilter = {};
  machineTags: FilterOption[] = [];

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  @Input()
  set refId(refId: string) {
    this.defaultFilter = {
      ...this.defaultFilter,
      refId: [refId],
    };
    this.applyFilter();
  }

  @Input()
  set mode(mode: DocTableDisplayMode) {
    this.columns = COLUMN_CONFIG[mode];
    this.data = [];
    this.applyFilter();
  }

  @Input()
  hideFilter = false;

  @Input()
  set titleWidth(width: number) {
    this.style.width = width + '%';
  }

  style = { width: '' };
  selectedFilters: {
    [key in FilterKeys]?: string | (string | FilterOption | BaseDto | { value: string[] })[];
  } = {
    contentTypeGroup: [],
    machineTypeTag: [],
    refId: [],
    title: '',
  };

  readonly filterConfig: DocumentFilterConfig = {
    contentTypeGroup: {
      options: DOC_TYPE_OPTIONS,
      placeholder: 'SHARED.SEARCH.FILTER.DOCUMENT_TYPE',
      multiple: true,
    },
    machineTypeTag: {
      options: [],
      placeholder: 'SHARED.SEARCH.FILTER.MACHINE_TYPES',
      multiple: true,
    },
  };

  readonly actions: ActionOption<DropdownActions>[] = [
    {
      label: 'COMMON.ACTION.EDIT',
      value: DropdownActions.EDIT,
      acl: {
        resources: ['Document'],
        right: 'Update',
      },
    },
    {
      label: 'COMMON.ACTION.VIEW_QR_CODE',
      value: DropdownActions.VIEW_QR,
    },
    {
      label: 'COMMON.ACTION.DELETE',
      value: DropdownActions.DELETE,
      acl: {
        resources: ['Document'],
        right: 'Delete',
      },
    },
  ];

  dataSource = new DocumentDataSource(this.store, `document-table`);

  assetTrees$: Observable<BaseAssetTree[]> = this.store.select(Asset.selectAllAssetTrees);
  loading$ = wrapToLoadedObservable(
    combineLatest([
      this.dataSource.loading$.pipe(map(loading => !loading)),
      this.store.select(Settings.selectLoadingByAction(Document.deleteDocument)),
    ]),
  ).pipe(map(loaded => !loaded));

  constructor(
    private store: Store<State>,
    private fileService: FileService,
    private modalService: ModalService,
    breakpointObserver: BreakpointObserver,
  ) {
    super(breakpointObserver, []);
    this.store.dispatch(MachineTypeTag.loadMachineTypeTags({}));
    this.store
      .select(MachineTypeTag.selectAll)
      .pipe(takeUntil(this.destroy$))
      .subscribe(tags => {
        if (this.filterConfig.machineTypeTag) {
          this.filterConfig.machineTypeTag.options = tags.map(t => ({ label: t, value: t }));
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onActionClick(action: ActionOption<DropdownActions>, doc: FileDto) {
    switch (action.value) {
      case DropdownActions.VIEW_QR: {
        this.fileService
          .getQRCodeById(doc.id)
          .pipe(take(1))
          .subscribe(({ code }) =>
            this.modalService.openQrCodeViewModal(code, doc.filename, doc.title),
          );
        return;
      }
      case DropdownActions.DELETE: {
        const dialogRef = this.modalService.openDeleteModal({
          entity: 'document',
          name: doc.title,
        });

        dialogRef
          .afterClosed()
          .pipe(
            take(1),
            filter(res => !!res),
          )
          .subscribe(() => this.store.dispatch(Document.deleteDocument({ documentId: doc.id })));
        return;
      }
      case DropdownActions.EDIT: {
        this.modalService.openDocumentModal({
          document: doc,
          mode: ModalModes.EDIT,
        });
        return;
      }
      default:
        return;
    }
  }

  applyFilter() {
    this.dataSource.resetPage();
    const parsedFilter = this.getParsedFilter(this.selectedFilters as FilesFilter);
    this.dataSource.loadDocuments({ ...parsedFilter, ...this.defaultFilter });
  }

  onSearch(input: string) {
    this.selectedFilters.title = input.trim().toLowerCase();
    this.applyFilter();
  }

  openDoc(doc: FileDto): void {
    window.open(doc.viewUrl, '_blank');
  }

  getAssetNames(doc: FileDto): Observable<string[]> {
    const assetIds = getRefIdsOf(ReferencedEntities.ASSET, doc.refIds);
    return this.store
      .select(Asset.selectAllAssets)
      .pipe(
        map(assets => assets.filter(asset => assetIds.includes(asset.id)).map(asset => asset.name)),
      );
  }

  private getParsedFilter(filters: FilesFilter): FilesFilter {
    const { contentTypeGroup, ...rest } = filters;
    if (Array.isArray(contentTypeGroup)) {
      return {
        contentTypeGroup: contentTypeGroup.map(item => item['value']),
        ...rest,
      } as FilesFilter;
    }
    return { contentTypeGroup, ...rest } as FilesFilter;
  }
}
