import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { OwlDateTimeComponent } from '@danielmoncada/angular-datetime-picker';
import { Store } from '@ngrx/store';
import { last } from 'lodash';
import * as moment from 'moment';
import { DeviceDetectorService } from 'ngx-device-detector';
import { interval, Subject } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { NotificationService, NotificationVariants } from 'src/app/core/services';
import { PerformanceRangeFilter, State } from 'src/app/core/store';

@Component({
  selector: 'syn-performance-range-filter',
  templateUrl: './performance-range-filter.component.html',
  styleUrls: ['./performance-range-filter.component.scss'],
})
export class PerformanceRangeFilterComponent implements OnInit, OnDestroy {
  private readonly destroy$ = new Subject<void>();
  @ViewChild(OwlDateTimeComponent)
  dateTimePicker?: OwlDateTimeComponent<Date>;

  AVAILABLE_INTERVALS = [
    { name: 'Last 15m', value: 15 * 60 * 1000 },
    { name: 'Last 1h', value: 60 * 60 * 1000 },
    { name: 'Last 4h', value: 4 * 60 * 60 * 1000 },
    { name: 'Last 8h', value: 8 * 60 * 60 * 1000 },
  ];

  startTime = new Date();
  defaultStart = moment().format('YYYY-MM-DDTHH:mm:ss');
  defaultFinish?: string;
  finishTime?: Date;

  currentFilter$ = this.store.select(PerformanceRangeFilter.selectRangeFilterValue);
  customRangeSelected$ = this.store.select(PerformanceRangeFilter.selectIsCustomFilter);
  limitStart$ = interval(60 * 1000).pipe(map(() => this.getCurrentTimeLimit()));

  get isDesktop() {
    return this.deviceService.isDesktop();
  }

  constructor(
    private store: Store<State>,
    private deviceService: DeviceDetectorService,
    private notificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.store
      .select(PerformanceRangeFilter.selectRangeFilterDates)
      .pipe(take(1))
      .subscribe(({ from, to }) => {
        this.defaultStart = moment(from).format('YYYY-MM-DDTHH:mm:ss');
        if (to) {
          this.defaultFinish = moment(to).format('YYYY-MM-DDTHH:mm:ss');
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onChipSelect(value: number) {
    this.store.dispatch(PerformanceRangeFilter.updatePerformanceRangeFilterValue({ value }));
  }

  change(value: Date[]) {
    const [startTime, finishTime] = value;
    this.startTime = startTime;
    this.finishTime = finishTime;
    if (this.finishTime < startTime) {
      this.notificationService.notify({
        message: 'Incorrect data range',
        variant: NotificationVariants.ERROR,
      });
    }
    this.compareDates(startTime, finishTime);
  }

  onMobilePickerChange(timeIsoString: string, target: 'startTime' | 'finishTime') {
    if (!timeIsoString) {
      this.onChipSelect((last(this.AVAILABLE_INTERVALS) as { name: string; value: number }).value);
      return;
    }

    target === 'startTime'
      ? (this.startTime = new Date(timeIsoString))
      : (this.finishTime = new Date(timeIsoString));
    this.compareDates(this.startTime, this.finishTime || new Date());
  }

  private dispatchRangeChange(from: Date, to: Date) {
    this.store.dispatch(PerformanceRangeFilter.updatePerformanceRangeFilterDates({ from, to }));
  }

  private compareDates(startTime: Date, finishTime?: Date) {
    if (finishTime) {
      this.dispatchRangeChange(startTime, finishTime);
    }
  }

  private getCurrentTimeLimit() {
    return moment().format('YYYY-MM-DDTHH:mm:ss');
  }
}
