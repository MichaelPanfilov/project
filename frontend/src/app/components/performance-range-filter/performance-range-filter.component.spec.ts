import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSelectModule } from '@angular/material/select';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { PerformanceRangeFilter } from 'src/app/core/store';

import { PerformanceRangeFilterComponent } from './performance-range-filter.component';

describe('PerformanceRangeFilterComponent', () => {
  let component: PerformanceRangeFilterComponent;
  let fixture: ComponentFixture<PerformanceRangeFilterComponent>;
  let mockStore: MockStore<{ rangeFilter: string }>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PerformanceRangeFilterComponent],
      imports: [MatSelectModule, NoopAnimationsModule],
      providers: [provideMockStore({ initialState: { rangeFilter: 0 } })],
    }).compileComponents();

    mockStore = TestBed.get(Store);
    mockStore.overrideSelector(PerformanceRangeFilter.selectRangeFilterValue, 0);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerformanceRangeFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
