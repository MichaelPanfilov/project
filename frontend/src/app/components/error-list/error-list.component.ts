import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
  selector: 'error-list',
  templateUrl: 'error-list.component.html',
})
export class ErrorListComponent {
  constructor(
    public snackBarRef: MatSnackBarRef<ErrorListComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: string[],
  ) {}
}
