import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Store } from '@ngrx/store';

import { MachineTypeTag, State } from '../../core/store';

@Component({
  selector: 'syn-link-tags',
  templateUrl: './link-tags.component.html',
  styleUrls: ['./link-tags.component.scss'],
})
export class LinkTagsComponent {
  private tags: string[] = [];

  readonly machineTags$ = this.store.select(MachineTypeTag.selectAll);

  @Input()
  set selectedTags(tags: string[] | null) {
    this.tags = [...(tags || [])];
  }

  @Output()
  selectedTagsChange = new EventEmitter<string[]>();

  constructor(private store: Store<State>) {
    this.store.dispatch(MachineTypeTag.loadMachineTypeTags({}));
  }

  isSelected(option: string) {
    return ~this.tags.indexOf(option);
  }

  onChangeSelection(isChecked: boolean, option: string): void {
    if (isChecked) {
      this.tags.push(option);
    } else {
      this.tags = this.tags.filter(tag => tag !== option);
    }
    this.selectedTagsChange.emit(this.tags);
  }
}
