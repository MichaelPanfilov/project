import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkTagsComponent } from './link-tags.component';

describe('LinkTagsComponent', () => {
  let component: LinkTagsComponent;
  let fixture: ComponentFixture<LinkTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LinkTagsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
