import { Component, ElementRef, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { AssetDto, ImageDto } from '@common';
import { Store } from '@ngrx/store';
import { BehaviorSubject, iif, of, Subject } from 'rxjs';
import { catchError, filter, switchMap, takeUntil } from 'rxjs/operators';
import { ImageService } from 'src/app/core/services';
import { Image, State } from 'src/app/core/store';

@Component({
  selector: 'syn-asset-image',
  template: '',
  styleUrls: ['./asset-image.component.scss'],
})
export class AssetImageComponent implements OnDestroy, OnInit {
  asset$ = new BehaviorSubject<AssetDto | undefined>(undefined);

  @Input()
  placeholder = '';

  @Input()
  set asset(asset: AssetDto) {
    // Resets background.
    this.setBackground(this.placeholder);
    this.asset$.next(asset);
  }

  private destroy$ = new Subject<void>();

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private imageService: ImageService,
    private store: Store<State>,
  ) {}

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private init() {
    this.setBackground(this.placeholder);

    this.asset$
      .pipe(
        filter(asset => !!asset),
        switchMap(asset =>
          this.store.select(Image.selectImageByRefId((asset as AssetDto).id)).pipe(
            switchMap(imgData =>
              iif(
                () => !!imgData,
                this.imageService.getImageBlob(imgData as ImageDto),
                of(undefined),
              ),
            ),
            catchError(() => of(undefined)),
          ),
        ),
        takeUntil(this.destroy$),
      )
      .subscribe(url => this.setBackground(url || this.placeholder));
  }

  private setBackground(url: string) {
    const el = this.el.nativeElement;
    this.renderer.setStyle(el, 'background-image', `url(${url})`);
    this.renderer.setStyle(el, 'background-size', 'cover');
    this.renderer.setStyle(el, 'pointer-events', 'auto');
  }
}
