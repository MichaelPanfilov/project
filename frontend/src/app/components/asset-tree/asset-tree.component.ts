import { ArrayDataSource } from '@angular/cdk/collections';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { AssetDto, AssetType, BaseAssetTree, normalizePositions, sortTrees } from '@common';
import { Store } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import { filter, take } from 'rxjs/operators';
import { Asset, State } from 'src/app/core/store';
import { ModalService } from 'src/app/modals/modal.service';
import { ActionOption, AssetTreeElementActions, AssetTreeVariants } from 'src/app/util';

@Component({
  selector: 'syn-asset-tree',
  templateUrl: './asset-tree.component.html',
  styleUrls: ['./asset-tree.component.scss'],
})
export class AssetTreeComponent {
  @ViewChild('tree') tree!: ElementRef;
  assetType = AssetType;

  @Input()
  set assetTrees(trees: BaseAssetTree[]) {
    const assetOrder = Object.values(AssetType).reverse();

    const assetsOrderer = (collection: BaseAssetTree[]) =>
      assetOrder.reduce<BaseAssetTree[]>(
        (res: BaseAssetTree[], orderKey: string) => [
          ...res,
          ...collection.filter(({ type }: BaseAssetTree) => type === orderKey),
        ],
        [],
      );

    const assetsWithChild = trees.filter(el => el.children && el.children.length > 0);
    const assetsWithoutChild = trees.filter(el => !el.children || el.children.length === 0);

    this.assetsSource = cloneDeep([
      ...assetsOrderer(assetsWithChild),
      ...assetsOrderer(assetsWithoutChild),
    ]);
    sortTrees(this.assetsSource);
    normalizePositions(this.assetsSource);

    this.assetsFirstLevelDeep = trees.map(item => item.id);
    this.dataSource = new ArrayDataSource(this.assetsSource);
    this.expandAll();
  }

  @Input()
  hideParentChange?: boolean;

  @Input()
  variant: AssetTreeVariants = AssetTreeVariants.PAGE;

  @Output() edit = new EventEmitter<AssetDto>();

  @Output() removeAsset = new EventEmitter<AssetDto>();

  assetsSource: BaseAssetTree[] = [];
  assetsFirstLevelDeep: string[] = [];
  treeControl = new NestedTreeControl<BaseAssetTree>(node => node.children);
  dataSource = new ArrayDataSource(this.assetsSource);

  get checkSource(): boolean {
    return !!this.assetsSource;
  }

  get backgroundVariant() {
    return { [this.variant]: true };
  }

  constructor(private modalService: ModalService, private store: Store<State>) {}

  expandAll(): void {
    this.treeControl.dataNodes = this.assetsSource;
    this.treeControl.expandAll();
  }

  collapseAll(): void {
    this.treeControl.collapseAll();
  }

  onAction(opts: { asset: AssetDto; action: ActionOption<AssetTreeElementActions> }): void {
    const { asset, action } = opts;
    switch (action.value) {
      case AssetTreeElementActions.DELETE: {
        return this.onAssetRemove(asset);
      }
      case AssetTreeElementActions.EDIT: {
        return this.onAssetEdit(asset);
      }
      default: {
        return;
      }
    }
  }

  onAssetView(asset: AssetDto) {
    const dialogRef = this.modalService.openViewAssetModal(asset);
    dialogRef
      .afterClosed()
      .pipe(filter(action => action && action === AssetTreeElementActions.EDIT))
      .subscribe(() => this.edit.emit(asset));
  }

  onAssetEdit(asset: AssetDto) {
    this.edit.emit(asset);
  }

  onAssetRemove(asset: AssetDto) {
    const dialogRef = this.modalService.openDeleteModal({ entity: 'asset', name: asset.name });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter(res => !!res),
      )
      .subscribe(() => this.store.dispatch(Asset.deleteAsset({ asset })));
  }

  hasChildren(n: number, node: BaseAssetTree) {
    return !!node.children && node.children.length > 0;
  }
}
