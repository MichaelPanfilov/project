import { Component, Input, OnInit } from '@angular/core';
import { SYN_COLORS } from 'src/app/util';

@Component({
  selector: 'syn-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.scss'],
})
export class GaugeComponent implements OnInit {
  readonly colors = SYN_COLORS;

  @Input()
  value = 0;

  @Input()
  additional?: string;

  @Input()
  threshold = 100;

  @Input()
  invert = false;

  @Input()
  sizeFactor = 1;

  get color(): string {
    if (this.invert) {
      return this.value > this.threshold ? SYN_COLORS.danger : SYN_COLORS.primary;
    }
    return this.value < this.threshold ? SYN_COLORS.danger : SYN_COLORS.primary;
  }

  constructor() {}

  ngOnInit() {
    this.value = Math.round(this.value);
  }
}
