import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetTreeElementComponent } from './asset-tree-element.component';

describe('AssetTreeElementComponent', () => {
  let component: AssetTreeElementComponent;
  let fixture: ComponentFixture<AssetTreeElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssetTreeElementComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetTreeElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
