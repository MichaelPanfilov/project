import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import {
  AssetDto,
  AssetType,
  ASSET_FILE_TAG,
  ASSET_HIERARCHY,
  BaseAssetTree,
  getChildrenCount,
  ImageDto,
} from '@common';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Asset, Image, State } from 'src/app/core/store';
import {
  ActionOption,
  AssetSelectItem,
  AssetTreeElementActions,
  AssetTreeVariants,
} from 'src/app/util';

const ACTIONS: ActionOption<AssetTreeElementActions>[] = [
  {
    label: 'COMMON.ACTION.EDIT',
    value: AssetTreeElementActions.EDIT,
    acl: {
      resources: ['Asset'],
      right: 'Update',
    },
  },
  {
    label: 'COMMON.ACTION.DELETE',
    value: AssetTreeElementActions.DELETE,
    acl: {
      resources: ['Asset'],
      right: 'Delete',
    },
  },
];

@Component({
  selector: 'syn-asset-tree-element',
  templateUrl: './asset-tree-element.component.html',
  styleUrls: ['./asset-tree-element.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AssetTreeElementComponent implements OnInit {
  @Input()
  asset!: AssetDto;

  @Input()
  hideParentChange?: boolean;

  @Input()
  variant: AssetTreeVariants = AssetTreeVariants.PAGE;

  @Output()
  unassign: EventEmitter<AssetDto> = new EventEmitter();

  @Output()
  view: EventEmitter<AssetDto> = new EventEmitter();

  @Output()
  action: EventEmitter<{
    action: ActionOption<AssetTreeElementActions>;
    asset: AssetDto;
  }> = new EventEmitter();

  items$ = this.store
    .select(Asset.selectAllAssetTrees)
    .pipe(map(trees => trees.map(tree => this.mapToItem(this.asset, tree))));
  parent$!: Observable<AssetDto | undefined>;
  image$!: Observable<ImageDto | undefined>;

  fileTag = ASSET_FILE_TAG;

  constructor(private store: Store<State>) {}

  ngOnInit() {
    this.image$ = this.store.select(Image.selectImageByRefId(this.asset.id));
    this.parent$ = this.store.select(Asset.selectParent(this.asset.id));
  }

  showDropDown$ = this.items$.pipe(
    map(items => items.length > 0 && this.asset.type !== AssetType.FACTORY),
  );

  get placeholder() {
    return this.asset ? `../../../assets/imgs/${this.asset.type}-placeholder.png` : '';
  }

  get paneClass() {
    return {
      [this.variant]: true,
    };
  }

  emitUnassign() {
    this.unassign.emit(this.asset);
  }

  emitView() {
    this.view.emit(this.asset);
  }

  emitAction(action: ActionOption<AssetTreeElementActions>) {
    this.action.emit({ action, asset: this.asset });
  }

  setParent(parent: AssetDto) {
    this.parent$.pipe(take(1)).subscribe(p => {
      // Only change the parent if its different from the current.
      if (!p || p.id !== parent.id) {
        this.store.dispatch(
          Asset.TreeActions.updateParent({ asset: this.asset, parentId: parent.id }),
        );
      }
    });
  }

  getActions(asset: BaseAssetTree): ActionOption<AssetTreeElementActions>[] {
    if (asset.children && asset.children.length) {
      return ACTIONS.filter(action => action.value !== AssetTreeElementActions.DELETE);
    }
    return ACTIONS;
  }

  private mapToItem(asset: AssetDto, tree: BaseAssetTree): AssetSelectItem {
    // Disabled if possible parent type is not matching.
    const disabled = ASSET_HIERARCHY[asset.type].parentType !== tree.type;
    return {
      ...tree,
      children: (tree.children || [])
        .filter(child => child.type !== asset.type)
        .reduce((prev, curr) => prev.concat(this.mapToItem(asset, curr)), [] as AssetSelectItem[]),
      disabled,
      childrenCount: getChildrenCount(tree),
    };
  }
}
