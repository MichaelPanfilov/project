import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSortable } from '@angular/material/sort';
import {
  CLASSIFICATIONS,
  getClassificationLabel,
  ParsedFilter,
  Sorting,
  SynMachineState,
  TaskDto,
  TaskStatus,
} from '@common';
import { CondOperator } from '@nestjsx/crud-request';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { combineLatest, Observable, Subject } from 'rxjs';
import { debounceTime, filter, map, take, takeUntil } from 'rxjs/operators';
import { Column, TableComponent } from 'src/app/core/abstract';
import {
  Asset,
  Document,
  Instruction,
  MachineTypeTag,
  Settings,
  State,
  Task,
} from 'src/app/core/store';
import { ViewTaskComponent } from 'src/app/modals';
import { ModalService } from 'src/app/modals/modal.service';
import {
  ActionOption,
  ACTIVE_TASK_STATES,
  ARCHIVED_TASK_STATES,
  AssetSelectItem,
  chunkArray,
  FilterOption,
  isTaskDue,
  isTaskOverdue,
  ModalModes,
  parseFilter,
  TaskDataSource,
  TaskFilter,
  wrapToLoadedObservable,
} from 'src/app/util';

export type TaskTableDisplayMode = 'asset' | 'all' | 'archive';

enum TableActions {
  EDIT = 'edit',
  COPY = 'copy',
  DELETE = 'delete',
}

type TableColumn =
  | keyof TaskDto
  | 'timeLag'
  | 'remainingTime'
  | 'attachments'
  | 'changeState'
  | 'actions';

const ACTIVE_COLUMNS: Column<TableColumn>[] = [
  'status',
  'title',
  { name: 'classification', breakpoints: ['(min-width: 1279px)'] },
  { name: 'machineTypes', breakpoints: ['(min-width: 1439px)'] },
  { name: 'assetName', breakpoints: ['(min-width: 1023px)'] },
  'dueDate',
  'attachments',
  'changeState',
  'actions',
];

const ARCHIVE_COLUMNS: Column<TableColumn>[] = [
  'status',
  'title',
  { name: 'classification', breakpoints: ['(min-width: 1023px)'] },
  { name: 'assetName', breakpoints: ['(min-width: 839px)'] },
  'dueDate',
  'timeLag',
  'attachments',
  'actions',
];

const ASSET_COLUMNS: Column<TableColumn>[] = [
  'status',
  'title',
  { name: 'classification', breakpoints: ['(min-width: 839px)'] },
  'remainingTime',
  'attachments',
  'changeState',
  'actions',
];

const COLUMN_CONFIG: { [key in TaskTableDisplayMode]: Column<TableColumn>[] } = {
  all: ACTIVE_COLUMNS,
  archive: ARCHIVE_COLUMNS,
  asset: ASSET_COLUMNS,
};

const DEFAULT_SORT: { [key in TaskTableDisplayMode]: MatSortable } = {
  all: { id: 'dueDate', start: 'asc', disableClear: true },
  archive: { id: 'timeLag', start: 'asc', disableClear: true },
  asset: { id: 'remainingTime', start: 'asc', disableClear: true },
};

const ARCHIVE_TABLE_ACTIONS: ActionOption<TableActions>[] = [
  {
    label: 'COMMON.ACTION.COPY',
    value: TableActions.COPY,
    acl: {
      resources: ['Task'],
      right: 'Update',
    },
  },
  {
    label: 'COMMON.ACTION.DELETE',
    value: TableActions.DELETE,
    acl: {
      resources: ['Task'],
      right: 'Delete',
    },
  },
];

const DEFAULT_TABLE_ACTIONS: ActionOption<TableActions>[] = [
  {
    label: 'COMMON.ACTION.EDIT',
    value: TableActions.EDIT,
    acl: {
      resources: ['Task'],
      right: 'Update',
    },
  },
  ...ARCHIVE_TABLE_ACTIONS,
];

const ACTION_CONFIG: { [key in TaskTableDisplayMode]: ActionOption<TableActions>[] } = {
  all: DEFAULT_TABLE_ACTIONS,
  archive: ARCHIVE_TABLE_ACTIONS,
  asset: DEFAULT_TABLE_ACTIONS,
};

@Component({
  selector: 'syn-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
})
export class TaskListComponent extends TableComponent<TaskDto> implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private defaultFilter: ParsedFilter<TaskDto> = {};

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  @Input()
  set refIds(refIds: string[]) {
    this.defaultFilter = {
      ...this.defaultFilter,
      status: { value: TaskStatus.ACTIVE, operator: CondOperator.EQUALS },
      assignedAsset: { value: refIds, operator: CondOperator.IN },
    };
    this.applyFilter();
  }

  @Input()
  set mode(mode: TaskTableDisplayMode) {
    // Trigger change detection.
    this.data = [];
    this.columns = COLUMN_CONFIG[mode];
    this.actions = ACTION_CONFIG[mode];
    this.defaultFilter = this.getDefaultFilter(mode);
    // Its important to apply the filter before sorting.
    this.applyFilter(DEFAULT_SORT[mode]);
    this.sortTable(DEFAULT_SORT[mode]);
  }

  @Input()
  hideFilter = false;

  taskStatus = TaskStatus;
  dataSource = new TaskDataSource(this.store, `task-table`);

  actions = DEFAULT_TABLE_ACTIONS;
  classifications: FilterOption[] = CLASSIFICATIONS;
  machineTypesTagsOptions$: Observable<FilterOption[]> = this.store
    .select(MachineTypeTag.selectAll)
    .pipe(map(tags => tags.map(t => ({ value: t, label: t }))));
  assetTrees$: Observable<AssetSelectItem[]> = this.store
    .select(Asset.selectAllAssetTrees)
    .pipe(map(factories => factories.map(factory => ({ ...factory, disabled: true }))));

  selectedFilters: TaskFilter = {
    assignedAsset: [],
    classification: [],
    machineTypes: [],
    title: '',
  };

  loading$ = wrapToLoadedObservable(
    combineLatest([
      this.dataSource.loading$.pipe(map(loading => !loading)),
      this.store.select(Settings.selectLoadingByAction(Task.deleteTask, Task.cloneTask)),
    ]),
  ).pipe(map(loaded => !loaded));

  constructor(
    private store: Store<State>,
    private modalService: ModalService,
    private dialog: MatDialog,
    breakpointObserver: BreakpointObserver,
  ) {
    super(breakpointObserver, []);
  }

  ngOnInit() {
    this.store.dispatch(MachineTypeTag.loadMachineTypeTags({}));
    // Load linked documents and instructions to show the amount of attachments.
    this.dataSource.dataChange
      .pipe(takeUntil(this.destroy$), debounceTime(100))
      .subscribe(tasks => {
        if (tasks.length) {
          const refIds = tasks.map(task => task.id);
          // Split up into multiple request to prevent url exceeding.
          const chunks = chunkArray(refIds, 15);
          chunks.forEach(chunk => {
            this.store.dispatch(Document.loadDocumentsByRefIds(chunk));
            this.store.dispatch(Instruction.loadInstructionsByRefIds(chunk));
          });
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  applyFilter(sortable?: MatSortable) {
    this.dataSource.resetPage();
    const parsedFilter = parseFilter(this.selectedFilters);
    const sorting = sortable ? ([sortable.id, sortable.start] as Sorting) : undefined;
    this.dataSource.loadTasks(
      {
        ...parsedFilter,
        ...this.defaultFilter,
      },
      sorting,
    );
  }

  onSearch(input: string) {
    this.selectedFilters.title = input.trim().toLowerCase();
    this.applyFilter();
  }

  getState(task: TaskDto): SynMachineState {
    return {
      [TaskStatus.INACTIVE]: 'Stopped',
      [TaskStatus.ACTIVE]: isTaskDue(task) ? 'Faulted' : 'Running',
    }[task.status];
  }

  buttonText(status: TaskStatus): string {
    return {
      [TaskStatus.INACTIVE]: 'COMMON.ACTION.ACTIVATE',
      [TaskStatus.ACTIVE]: 'COMMON.ACTION.MARK_DONE',
    }[status];
  }

  onViewTask(task: TaskDto) {
    this.dialog.open(ViewTaskComponent, { data: { task }, panelClass: 'view-modal' });
  }

  onActionClick(action: ActionOption<TableActions>, task: TaskDto) {
    switch (action.value) {
      case TableActions.EDIT: {
        return this.modalService.openTaskModal(ModalModes.EDIT, task);
      }
      case TableActions.COPY: {
        this.store.dispatch(Task.cloneTask({ taskId: task.id }));
        return;
      }
      case TableActions.DELETE: {
        this.openDeletePrompt(task);
        return;
      }
      default:
        return;
    }
  }

  isOverdue(task: TaskDto) {
    return isTaskOverdue(task);
  }

  displayTimeLag(task: TaskDto) {
    const sign = this.isOverdue(task) ? '+' : '-';
    const absLag = this.getTimeLag(task);
    if (!absLag) {
      return absLag;
    }

    if (absLag === '00:00:00') {
      return '0';
    }

    return `${sign}${absLag}`;
  }

  getAttachmentCount(task: TaskDto): Observable<number> {
    const refId = task.id;
    return combineLatest([
      this.store.select(Document.selectDocumentsByRefId(refId)),
      this.store.select(Instruction.selectInstructionsByRefId(refId)),
    ]).pipe(map(([docs, instructions]) => docs.length + instructions.length));
  }

  getRemainingTime(task: TaskDto) {
    const dueDate = moment(task.dueDate as Date);
    const now = moment();
    if (task.status === TaskStatus.INACTIVE || dueDate.isBefore()) {
      return '00:00 h';
    }

    const diff = dueDate.diff(now);
    const diffDuration = moment.duration(diff);

    return Math.floor(diffDuration.asHours()) + moment.utc(diff).format(':mm [h]');
  }

  getClassificationLabel(value: string) {
    return getClassificationLabel(value);
  }

  private openDeletePrompt(task: TaskDto) {
    const dialogRef = this.modalService.openDeleteModal({ entity: 'task', name: task.title });

    return dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter(res => !!res),
      )
      .subscribe(() => this.store.dispatch(Task.deleteTask({ taskId: task.id })));
  }

  private getTimeLag(task: TaskDto): string | null {
    const due = moment(task.dueDate as Date);
    const done = moment(task.doneDate);
    if (!due.isValid() || !done.isValid()) {
      return null;
    }

    const duration = moment
      .duration(due.diff(done))
      .abs()
      .asMilliseconds();
    const n = 24 * 60 * 60 * 1000;
    const days = Math.floor(duration / n);
    const time = moment.utc(duration % n).format('HH:mm');
    const padding = days < 10 ? '0' : '';

    return `${padding}${days}:${time}`;
  }

  private getDefaultFilter(mode: TaskTableDisplayMode): ParsedFilter<TaskDto> {
    return {
      ...this.defaultFilter,
      status: {
        operator: CondOperator.IN,
        value: mode === 'archive' ? ARCHIVED_TASK_STATES : ACTIVE_TASK_STATES,
      },
    };
  }
}
