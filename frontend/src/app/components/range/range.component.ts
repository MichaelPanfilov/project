import { Component, ElementRef, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { interval, Subject } from 'rxjs';
import { animationFrameScheduler } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'syn-range',
  templateUrl: './range.component.html',
  styleUrls: ['./range.component.scss'],
})
export class RangeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  @Input()
  value!: number;

  @Input()
  max!: number;

  @Input()
  set color(color: string) {
    this.renderer.setStyle(this.el.nativeElement.children[0], 'background', `${color}`);
  }

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    interval(10, animationFrameScheduler)
      .pipe(takeUntil(this.destroy$), take(this.percentage + 1))
      .subscribe(x => {
        this.renderer.setStyle(this.el.nativeElement.children[0], 'width', `${x}%`);
        this.renderer.setStyle(
          this.el.nativeElement.children[0],
          'opacity',
          `${x / 100 < 0.1 ? 0.1 : x / 100}`,
        );
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  get percentage() {
    return this.value / (this.max / 100);
  }
}
