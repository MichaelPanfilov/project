import { Component } from '@angular/core';
import { FactoryDto } from '@common';
import { Store } from '@ngrx/store';

import { AbstractAssetView } from '../../core/abstract/asset-view';
import { State } from '../../core/store';

@Component({
  selector: 'syn-factory-view-fields',
  template: `
    <ng-container *ngIf="asset$ | async as asset">
      <div class="row">
        <span class="field-label">{{ 'ASSET.LABEL.TYPE' | translate }}</span>
        <span>{{ (type$ | async) || '' | translate }}</span>
      </div>
      <div class="row">
        <span class="field-label">{{ 'COMMON.LABEL.POSTAL_CODE' | translate }}</span>
        <span *ngIf="asset.postalCode; else empty">{{ asset.postalCode }}</span>
      </div>
      <div class="row">
        <span class="field-label">{{ 'ASSET.FORM.PRECEDING_ASSET' | translate }}</span>
        <span *ngIf="predecessor$ | async as predecessor; else first" class="capitalize">
          {{ predecessor.name }}
        </span>
      </div>
      <div class="row">
        <span class="field-label">{{ 'ASSET.LABEL.LINES_AND_MACHINES' | translate }}</span>
        <span *ngIf="children$ | async as children; else empty">
          <ng-container *ngIf="children.length; else empty">
            {{ children | hideMore: 2 }}
          </ng-container>
        </span>
      </div>
      <ng-template #empty>-</ng-template>
      <ng-template #first>{{ 'COMMON.LABEL.NONE' | translate }}</ng-template>
    </ng-container>
  `,
  styleUrls: ['./asset-view-fields.component.scss'],
})
export class FactoryViewFieldsComponent extends AbstractAssetView<FactoryDto> {
  constructor(protected store: Store<State>) {
    super(store);
  }
}
