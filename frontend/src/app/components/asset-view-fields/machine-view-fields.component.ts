import { Component, OnDestroy, OnInit } from '@angular/core';
import { MachineDto } from '@common';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { AbstractAssetView } from 'src/app/core/abstract';
import { Asset, State } from 'src/app/core/store';

@Component({
  selector: 'syn-machine-view-fields',
  template: `
    <ng-container *ngIf="asset$ | async as asset">
      <div class="header row">{{ 'ASSET.LABEL.MAINTENANCE_DATA' | translate }}:</div>
      <div class="row">
        <span class="field-label">{{ 'ASSET.LABEL.TOTAL_PRODUCING_TIME' | translate }}</span>
        <span *ngIf="producingHours$ | async as hours; else empty"
          >{{ hours | formatNumber }}h</span
        >
      </div>
      <div class="row">
        <span class="field-label">{{ 'ASSET.LABEL.TOTAL_NUM_CYCLES' | translate }}</span>
        <span *ngIf="cycles$ | async as cycles; else empty">
          {{ cycles | formatNumber }} {{ 'ASSET.LABEL.CYCLES' | translate }}
        </span>
      </div>
      <div class="row">
        <span class="field-label">{{ 'ASSET.LABEL.TOTAL_POWER_ON' | translate }}</span>
        <span *ngIf="powerOnHours$ | async as hours; else empty">{{ hours | formatNumber }}h</span>
      </div>
      <div class="divider"></div>
      <div class="header row">{{ 'ASSET.LABEL.INFORMATION' | translate }}:</div>
      <div class="row">
        <span class="field-label">{{ 'ASSET.FORM.MANUFACTURER' | translate }}</span>
        <span *ngIf="asset.manufacturer; else empty">{{ asset.manufacturer }}</span>
      </div>
      <div class="row">
        <span class="field-label">{{ 'COMMON.LABEL.SERIAL_NUMBER' | translate }}</span>
        <span *ngIf="asset.serialNumber; else empty">{{ asset.serialNumber }}</span>
      </div>
      <div class="row">
        <span class="field-label">{{ 'ASSET.FORM.CONSTRUCTION_YEAR' | translate }}</span>
        <span *ngIf="asset.constructionYear; else empty">{{ asset.constructionYear }}</span>
      </div>
      <div class="row">
        <span class="field-label">{{ 'SHARED.RELATION.MACHINE_TYPE' | translate }}</span>
        <span *ngIf="asset.machineType; else empty">{{ asset.machineType }}</span>
      </div>
      <ng-template #empty>-</ng-template>
      <ng-template #first>{{ 'COMMON.LABEL.NONE' | translate }}</ng-template>
    </ng-container>
  `,
  styleUrls: ['./asset-view-fields.component.scss'],
})
export class MachineViewFieldsComponent extends AbstractAssetView<MachineDto>
  implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private assetId$ = this.asset$.pipe(
    filter(asset => !!asset),
    map(asset => (asset as MachineDto).id),
  );

  producingHours$ = this.assetId$.pipe(
    switchMap(id => this.store.select(Asset.selectMachineProducingHours(id))),
  );
  cycles$ = this.assetId$.pipe(switchMap(id => this.store.select(Asset.selectMachineCycles(id))));
  powerOnHours$ = this.assetId$.pipe(
    switchMap(id => this.store.select(Asset.selectMachinePowerOnHours(id))),
  );

  constructor(protected store: Store<State>) {
    super(store);
  }

  ngOnInit() {
    this.store.dispatch(Asset.MachineActions.loadDevices({}));
    this.asset$.pipe(takeUntil(this.destroy$)).subscribe(machine => {
      if (machine) {
        this.store.dispatch(Asset.MachineActions.loadPowerOnHours({ machine }));
        this.store.dispatch(Asset.MachineActions.loadProducingHours({ machine }));
        this.store.dispatch(Asset.MachineActions.loadCycles({ machine }));
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
