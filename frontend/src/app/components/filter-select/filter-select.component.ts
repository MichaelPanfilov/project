import { SelectionModel } from '@angular/cdk/collections';
import { Overlay } from '@angular/cdk/overlay';
import { Component, EventEmitter, Input, OnInit, Output, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { delay, takeUntil } from 'rxjs/operators';
import { FilterOption } from 'src/app/util';

import { DropdownMenu } from '../../core/abstract';

@Component({
  selector: 'syn-filter-select',
  templateUrl: './filter-select.component.html',
  styleUrls: ['./filter-select.component.scss'],
})
export class FilterSelectComponent extends DropdownMenu implements OnInit {
  @Input()
  multiple = false;

  @Input()
  placeholder!: string;

  @Input()
  options!: FilterOption[];

  @Input()
  set selected(items: FilterOption[]) {
    this.selection = new SelectionModel<FilterOption>(this.multiple, items);
  }

  @Output()
  selectedChange: EventEmitter<FilterOption[]> = new EventEmitter<FilterOption[]>();

  selection!: SelectionModel<FilterOption>;

  get label() {
    return this.selection.selected.length ? this.selection.selected[0].label : this.placeholder;
  }
  constructor(overlay: Overlay, viewContainerRef: ViewContainerRef, router: Router) {
    super(overlay, viewContainerRef, router);
  }

  ngOnInit() {
    super.ngOnInit();
    // Make sure, for big inputs which grow the placeholder, to keep the dropdown overlay in sync.
    this.selectedChange
      .pipe(takeUntil(this.destroy$), delay(100))
      .subscribe(() => this.syncWidth());
  }

  onSelect(event: Event, item: FilterOption) {
    event.preventDefault();
    event.stopPropagation();
    this.selection.toggle(item);
    this.selectedChange.emit(this.selection.selected);
    if (!this.multiple) {
      this.hideMenu();
    }
  }

  reset(event: Event) {
    event.stopPropagation();
    this.selection.deselect(...this.selection.selected);
    this.selectedChange.emit(this.selection.selected);
  }
}
