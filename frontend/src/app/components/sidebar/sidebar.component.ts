import { Component, OnDestroy } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, startWith, take, takeUntil } from 'rxjs/operators';
import { ROUTE_SEGMENTS } from 'src/app/core/navigation';
import { AuthService } from 'src/app/core/services';
import { State, User } from 'src/app/core/store';
import { ModalService } from 'src/app/modals/modal.service';

interface BaseSidebarItem {
  label: string;
  alt?: string[];
  svg: {
    active: string;
    inactive: string;
  };
  title: string;
}

interface NavigatedSidebarItem extends BaseSidebarItem {
  path: string;
}

interface ClickableSidebarItem extends BaseSidebarItem {
  onClick: () => unknown;
}

interface NavSidebar {
  top: (NavigatedSidebarItem | ClickableSidebarItem)[];
  bottom: (NavigatedSidebarItem | ClickableSidebarItem)[];
}

function isNavigatedSidebarItem(
  item: NavigatedSidebarItem | ClickableSidebarItem,
): item is NavigatedSidebarItem {
  return 'path' in item;
}

@Component({
  selector: 'syn-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnDestroy {
  private destroy$ = new Subject<void>();

  readonly homeRoute = ROUTE_SEGMENTS.home.path;

  readonly sidebar: NavSidebar = {
    top: [
      {
        ...ROUTE_SEGMENTS.lines,
        title: 'NAVIGATION.TITLE.SHOPFLOOR',
        label: 'NAVIGATION.LABEL.SHOPFLOOR',
        alt: ['/machines', '/factories'],
        svg: {
          active: './assets/icons/Syntegon-icon-shopfloor-active.svg',
          inactive: './assets/icons/Syntegon-icon-shopfloor-inactive.svg',
        },
      },
      {
        ...ROUTE_SEGMENTS.taskManagement,
        title: 'NAVIGATION.TITLE.TASKS',
        svg: {
          active: './assets/icons/Syntegon-icon-tasks-active.svg',
          inactive: './assets/icons/Syntegon-icon-tasks-inactive.svg',
        },
      },
      {
        ...ROUTE_SEGMENTS.documentManagement,
        title: 'NAVIGATION.TITLE.DOCUMENTS',
        svg: {
          active: './assets/icons/Syntegon-icon-documents-active.svg',
          inactive: './assets/icons/Syntegon-icon-documents-inactive.svg',
        },
      },
      {
        ...ROUTE_SEGMENTS.workInstructions,
        title: 'NAVIGATION.TITLE.WORK_INSTRUCTIONS',
        svg: {
          active: './assets/icons/Syntegon-icon-work-instructions-active.svg',
          inactive: './assets/icons/Syntegon-icon-work-instructions-inactive.svg',
        },
      },
    ],
    bottom: [
      {
        ...ROUTE_SEGMENTS.settings,
        title: 'NAVIGATION.TITLE.SETTINGS',
        svg: {
          active: './assets/icons/Syntegon-icon-settings-active.svg',
          inactive: './assets/icons/Syntegon-icon-settings-inactive.svg',
        },
      },
      {
        label: 'NAVIGATION.LABEL.PROFILE',
        title: 'NAVIGATION.TITLE.PROFILE',
        onClick: () => this.openMyProfile(),
        svg: {
          active: './assets/icons/Syntegon-icon-user-profile-active.svg',
          inactive: './assets/icons/Syntegon-icon-user-profile-inactive.svg',
        },
      },
      {
        label: 'NAVIGATION.LABEL.LOGOUT',
        title: 'NAVIGATION.TITLE.LOGOUT',
        onClick: () => this.logout(),
        svg: {
          active: './assets/icons/Syntegon-icon-logout-active.svg',
          inactive: './assets/icons/Syntegon-icon-logout-inactive.svg',
        },
      },
    ],
  };

  activeItem?: NavigatedSidebarItem | ClickableSidebarItem;

  constructor(
    private router: Router,
    private store: Store<State>,
    private authService: AuthService,
    private modalService: ModalService,
  ) {
    this.router.events
      .pipe(
        takeUntil(this.destroy$),
        filter(ev => ev instanceof NavigationEnd),
        map(ev => (ev as NavigationEnd).urlAfterRedirects),
        distinctUntilChanged(),
        startWith(this.router.url),
        map(url => this.findActiveItem(url)),
      )
      .subscribe(item => (this.activeItem = item));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  async openMyProfile() {
    this.store
      .select(User.selectMe)
      .pipe(take(1))
      .subscribe(me => this.modalService.openUserModal(me));
  }

  logout() {
    this.authService
      .logout()
      .pipe(take(1))
      .subscribe(() => this.router.navigate(['/login']));
  }

  noop() {}

  private findActiveItem(url: string) {
    return [...this.sidebar.top, ...this.sidebar.bottom].find(item => {
      if (!isNavigatedSidebarItem(item)) {
        return undefined;
      }
      return url.includes(item.path) || (item.alt && !!item.alt.find(alt => url.includes(alt)));
    });
  }
}
