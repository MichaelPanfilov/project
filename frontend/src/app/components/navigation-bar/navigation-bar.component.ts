import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { flatten } from 'lodash';
import { Subject } from 'rxjs';
import { filter, map, startWith, takeUntil } from 'rxjs/operators';
import { NavData } from 'src/app/core/navigation';
import { NavbarActionsViewService } from 'src/app/core/services';

interface NavigationSegment {
  title: string;
  path: string;
}

@Component({
  selector: 'syn-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss'],
})
export class NavigationBarComponent implements OnInit, AfterViewInit, OnDestroy {
  private destroy$ = new Subject<void>();

  @ViewChild('viewContainer', { read: ViewContainerRef })
  viewContainer!: ViewContainerRef;

  navHistory: NavigationSegment[] = [];
  fullHistory: NavData[] = [];
  subPages: NavData[] = [];

  private get lastPathSegment(): NavData {
    return this.fullHistory[this.fullHistory.length - 1];
  }

  private get routeSegments(): ActivatedRoute[] {
    return this.findRoute(
      this.activatedRoute,
      this.router.url.split('/').filter(seg => seg.length > 0),
    );
  }

  get pageTitle(): string {
    const { title } = this.navHistory[this.navHistory.length - 1];
    return title;
  }

  get submenuHidden(): boolean {
    return !!this.lastPathSegment.submenuHidden;
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private navActions: NavbarActionsViewService,
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(
        takeUntil(this.destroy$),
        filter(ev => ev instanceof NavigationEnd),
        map(ev => (ev as NavigationEnd).urlAfterRedirects),
        // Initial page load.
        startWith(this.router.url),
      )
      .subscribe(() => {
        if (this.routeSegments.length) {
          this.updateNav(this.routeSegments.map(r => r.snapshot));
        }
      });
  }

  ngAfterViewInit() {
    this.navActions.initViewContainer(this.viewContainer);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private updateNav(snapshots: ActivatedRouteSnapshot[]): void {
    this.fullHistory = [];
    this.navHistory = [];

    snapshots.forEach((snapshot, index) => {
      const data = snapshot.data;
      const nav = data['nav'] as Required<NavData>;
      if (nav) {
        const title = nav.param && data[nav.param] ? data[nav.param].name : nav.title;
        const path = nav.path.length ? nav.path : snapshot.params[nav.dynamicParam];

        this.fullHistory.push(nav);

        if (!nav.navBarIgnore) {
          this.navHistory.push({ title, path });
        }

        if (index === 0) {
          this.subPages = nav.subPages || [];
        }
      }
    });
  }

  private findRoute(route: ActivatedRoute, segments: string[]): ActivatedRoute[] {
    if (!segments.length) {
      return [route];
    }

    const nav = route.snapshot.data['nav'] as NavData;
    if (nav.path === segments[0]) {
      const childRoutes = route.children.map(child => this.findRoute(child, segments.slice(1)));
      return [route, ...flatten(childRoutes)];
    }
    if (nav.param) {
      return [route];
    }

    return [];
  }
}
