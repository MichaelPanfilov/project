import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkTaskComponent } from './link-task.component';

describe('LinkTaskComponent', () => {
  let component: LinkTaskComponent;
  let fixture: ComponentFixture<LinkTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LinkTaskComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
