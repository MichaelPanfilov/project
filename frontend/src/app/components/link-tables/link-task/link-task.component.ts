import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { getClassificationLabel, TaskDto, TaskStatus } from '@common';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Column } from 'src/app/core/abstract';
import { Asset, State, Task } from 'src/app/core/store';
import { chunkArray, parseFilter, TaskDataSource } from 'src/app/util';

import { LinkTable } from '../link-table';

const COLUMNS: Column<keyof TaskDto | 'select'>[] = [
  'select',
  'title',
  { name: 'classification', breakpoints: ['(min-width: 599px)'] },
  { name: 'machineTypes', breakpoints: ['(min-width: 719px)'] },
  'assignedAsset',
];

@Component({
  selector: 'syn-link-task',
  templateUrl: './link-task.component.html',
  styleUrls: ['../link-table.component.scss'],
})
export class LinkTaskComponent extends LinkTable<TaskDto> implements OnInit {
  private defaultFilter = parseFilter({ status: TaskStatus.ACTIVE });

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  dataSource = new TaskDataSource(this.store, `link-task-table-${this.uniqueId}`);
  selectionChanged$ = combineLatest([
    this.store.select(Task.selectTasks),
    this.selection.changed.pipe(startWith(null)),
  ]).pipe(map(([tasks]) => tasks.filter(task => this.selectedIds.includes(task.id))));

  constructor(private store: Store<State>, breakpointObserver: BreakpointObserver) {
    super(breakpointObserver, COLUMNS);
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.selectedIds.length) {
      // Ensure we have the already selected tasks in store.
      const chunks = chunkArray(this.selectedIds, 25);
      chunks.forEach(chunk => this.store.dispatch(Task.loadTasksByIds(chunk)));
    }
    // Init data source for table.
    this.dataSource.loadTasks(this.defaultFilter);
  }

  showOnlySelected(event: MatCheckboxChange) {
    if (event.checked) {
      this.data = [...this.selectedItems];
    } else {
      this.dataSource.loadTasks(this.defaultFilter);
    }
  }

  getAssetName(assetId: string): Observable<string> {
    return this.store
      .select(Asset.selectAsset(assetId))
      .pipe(map(asset => (asset ? asset.name : '')));
  }

  getClassificationLabel(value: string) {
    return getClassificationLabel(value);
  }
}
