import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { BaseDto, InstructionDto } from '@common';

import { LinkTable } from './link-table';

@Directive()
export abstract class LinkTableForm<T extends BaseDto> extends LinkTable<T> {
  private formDisabled = true;

  @Input()
  currentTab = 0;

  @Output()
  currentTabChange = new EventEmitter<[number, boolean]>();

  @Output()
  created = new EventEmitter();

  @Input()
  get disabled() {
    return this.formDisabled;
  }

  set disabled(state: boolean) {
    this.formDisabled = state;
    this.disabledChange.emit(state);
  }

  @Output()
  disabledChange = new EventEmitter<boolean>();

  onTabChange(event: MatTabChangeEvent) {
    this.currentTabChange.emit([event.index, event.index > 0 || this.formDisabled]);
    this.currentTab = event.index;
  }

  onCreate(instruction: InstructionDto) {
    this.created.emit(instruction);
  }
}
