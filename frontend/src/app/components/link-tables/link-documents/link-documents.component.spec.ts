import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkDocumentsComponent } from './link-documents.component';

describe('LinkDocumentsComponent', () => {
  let component: LinkDocumentsComponent;
  let fixture: ComponentFixture<LinkDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LinkDocumentsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
