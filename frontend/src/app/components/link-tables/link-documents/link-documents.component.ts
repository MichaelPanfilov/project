import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FileDto } from '@common';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';
import { Column } from 'src/app/core/abstract';
import { Document, State } from 'src/app/core/store';
import { DocumentFormComponent } from 'src/app/forms';
import {
  chunkArray,
  DocumentDataSource,
  DocumentFormContent,
  getDocTypeDisplayName,
} from 'src/app/util';

import { LinkTableForm } from '../link-table-form';

const COLUMNS: Column<keyof FileDto | 'select'>[] = ['select', 'title', 'contentType'];

@Component({
  selector: 'syn-link-documents',
  templateUrl: './link-documents.component.html',
  styleUrls: ['../link-table.component.scss'],
})
export class LinkDocumentsComponent extends LinkTableForm<FileDto> implements OnInit {
  @ViewChild('documentForm')
  documentForm!: DocumentFormComponent;

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  @Output()
  openAssignment = new EventEmitter<DocumentFormContent>();

  dataSource = new DocumentDataSource(this.store, `link-documents-table-${this.uniqueId}`);
  selectionChanged$ = combineLatest([
    this.store.select(Document.selectDocuments),
    this.selection.changed.pipe(startWith(null)),
  ]).pipe(
    takeUntil(this.destroy$),
    map(([docs]) => docs.filter(doc => this.selectedIds.includes(doc.id))),
  );

  constructor(private store: Store<State>, breakpointObserver: BreakpointObserver) {
    super(breakpointObserver, COLUMNS);
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.selectedIds.length) {
      // Ensure we have the already selected documents in store.
      const chunks = chunkArray(this.selectedIds, 25);
      chunks.forEach(chunk => this.store.dispatch(Document.loadDocumentsByIds(chunk)));
    }
    // Init data source for table.
    this.dataSource.loadDocuments();
  }

  getDocTypeDisplayName(doc: FileDto): string {
    return getDocTypeDisplayName(doc);
  }
}
