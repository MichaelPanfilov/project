import { SelectionModel } from '@angular/cdk/collections';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Directive, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { BaseDto } from '@common';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BaseDataSource, Column, TableComponent } from 'src/app/core/abstract';

// Generate a unique table id because there could be multiple of theses components
// in the view at the same time. The id helps the store to differentiate them.
let tableId = 0;

@Directive()
export abstract class LinkTable<T extends BaseDto> extends TableComponent<T>
  implements OnInit, OnDestroy {
  protected destroy$ = new Subject<void>();
  protected selectedItems: T[] = [];

  selection = new SelectionModel<string>(true);

  get uniqueId() {
    return tableId++;
  }

  get selectedIds() {
    return this.selection.selected;
  }

  @Input()
  set selectedIds(ids: string[]) {
    this.selection = new SelectionModel(true, ids);
  }

  @Output()
  selectedIdsChange = new EventEmitter<string[]>();

  abstract paginator: MatPaginator;
  abstract dataSource: BaseDataSource<T>;
  abstract selectionChanged$: Observable<T[]>;

  constructor(breakpointObserver: BreakpointObserver, columns: Column[]) {
    super(breakpointObserver, columns);
  }

  ngOnInit() {
    this.selectionChanged$
      .pipe(takeUntil(this.destroy$))
      .subscribe(tasks => (this.selectedItems = tasks));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSelectionChange(item: T) {
    this.selection.toggle(item.id);
    this.selectedIdsChange.emit(this.selection.selected);
  }

  isOnlySelected() {
    const items = this.dataSource.data;
    // We compare to the selectedItems, not the ids, as there might be ids that belong to deleted items.
    const sameSize = items.length === this.selectedItems.length;
    return sameSize && !items.some(item => !this.selectedItems.find(i => i.id === item.id));
  }
}
