import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { getClassificationLabel, InstructionDto } from '@common';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';
import { Column } from 'src/app/core/abstract';
import { Instruction, State } from 'src/app/core/store';
import { WorkInstructionFormComponent } from 'src/app/forms';
import { chunkArray, InstructionDataSource, WorkInstructionFormContent } from 'src/app/util';

import { LinkTableForm } from '../link-table-form';

const COLUMNS: Column<keyof InstructionDto | 'select'>[] = [
  'select',
  'title',
  'machineTypes',
  'classification',
];

@Component({
  selector: 'syn-link-instructions',
  templateUrl: './link-instructions.component.html',
  styleUrls: ['../link-table.component.scss'],
})
export class LinkInstructionsComponent extends LinkTableForm<InstructionDto> implements OnInit {
  @ViewChild('workInstructionForm')
  instructionForm!: WorkInstructionFormComponent;

  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  @Output()
  openAssignment = new EventEmitter<WorkInstructionFormContent>();

  dataSource = new InstructionDataSource(this.store, `link-instruction-table-${this.uniqueId}`);
  selectionChanged$ = combineLatest([
    this.store.select(Instruction.selectInstructions),
    this.selection.changed.pipe(startWith(null)),
  ]).pipe(
    takeUntil(this.destroy$),
    map(([instructions]) => instructions.filter(instr => this.selectedIds.includes(instr.id))),
  );

  constructor(private store: Store<State>, breakpointObserver: BreakpointObserver) {
    super(breakpointObserver, COLUMNS);
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.selectedIds.length) {
      // Ensure we have the already selected instructions in store.
      const chunks = chunkArray(this.selectedIds, 25);
      chunks.forEach(chunk => this.store.dispatch(Instruction.loadInstructionsByIds(chunk)));
    }
    // Init data source for table.
    this.dataSource.loadInstructions();
  }

  getClassificationLabel(value: string) {
    return getClassificationLabel(value);
  }
}
