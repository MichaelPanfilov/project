import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkInstructionsComponent } from './link-instructions.component';

describe('LinkInstructionsComponent', () => {
  let component: LinkInstructionsComponent;
  let fixture: ComponentFixture<LinkInstructionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LinkInstructionsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
