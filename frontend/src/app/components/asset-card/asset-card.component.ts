import {
  Component,
  ElementRef,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import {
  AssetDto,
  AssetType,
  ImageDto,
  LineDto,
  MachineDto,
  SynDisconnectedState,
  SynMachineState,
  TaskStatus,
} from '@common';
import { CondOperator } from '@nestjsx/crud-request';
import { Store } from '@ngrx/store';
import { BehaviorSubject, fromEvent, iif, interval, Observable, of, Subject } from 'rxjs';
import { catchError, filter, map, mapTo, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { ImageService } from 'src/app/core/services';
import { Asset, Image as StoreImage, State, Task } from 'src/app/core/store';
import { chunkArray, isTaskDue, STATUS_COLORS } from 'src/app/util';

// TODO: Replace using websocket
const STATUS_UPDATING_INTERVAL = 5000;

@Component({
  selector: 'syn-asset-card',
  templateUrl: './asset-card.component.html',
  styleUrls: ['./asset-card.component.scss'],
})
export class AssetCardComponent implements OnDestroy, OnInit {
  @ViewChild('assetImg', { static: true })
  assetImg!: ElementRef;

  asset$ = new BehaviorSubject<AssetDto | undefined>(undefined);

  imageLoaded = false;

  @Input()
  set asset(asset: AssetDto) {
    // Resets background.
    // this.setBackground(this.placeholder);
    this.asset$.next(asset);
  }

  @Input()
  placeholder = '../../../assets/imgs/machine-default.png';

  @Input()
  hideTasksBadge?: boolean;

  readonly assetType = AssetType;
  readonly colors = STATUS_COLORS;

  private focused = true;
  private destroy$ = new Subject<void>();

  private machine$ = this.asset$.pipe(
    filter(asset => !!asset && asset.type === AssetType.MACHINE),
  ) as Observable<MachineDto>;

  private line$ = this.asset$.pipe(
    filter(asset => !!asset && asset.type === AssetType.LINE),
    switchMap(line => this.store.select(Asset.selectAssetTree((line as LineDto).id))),
  );

  dueTasks$ = this.asset$.pipe(
    filter(asset => !!asset),
    switchMap(asset => {
      const { id, type } = asset as AssetDto;
      if (type === AssetType.LINE) {
        return this.line$.pipe(
          map(line => (line ? [line, ...line.children] : [])),
          switchMap(assets =>
            this.store.select(Task.selectActiveTasksOfAssets(assets.map(a => a.id))),
          ),
        );
      }
      return this.store.select(Task.selectActiveTasksOfAsset(id));
    }),
    map(tasks => tasks.filter(task => isTaskDue(task))),
  );

  status$: Observable<SynMachineState | SynDisconnectedState> = this.machine$.pipe(
    switchMap(m => this.store.select(Asset.selectMachineStatus(m.id))),
    map(status => status || 'Unknown'),
  );

  constructor(
    private renderer: Renderer2,
    private store: Store<State>,
    private imageService: ImageService,
  ) {}

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  @HostListener('window:focus', ['$event'])
  onFocus($event: Event) {
    this.focused = true;
  }

  @HostListener('window:blur', ['$event'])
  onBlur($event: Event) {
    this.focused = false;
  }

  private init() {
    // Load displayed asset tasks.
    this.asset$
      .pipe(
        takeUntil(this.destroy$),
        filter(asset => !!asset),
      )
      .subscribe(asset => this.store.dispatch(Task.loadActiveTasksOfAsset((asset as AssetDto).id)));

    // Update machine state.
    this.machine$
      .pipe(
        takeUntil(this.destroy$),
        switchMap(machine =>
          interval(STATUS_UPDATING_INTERVAL).pipe(
            takeUntil(this.destroy$),
            filter(() => this.focused),
            startWith(0),
            mapTo(machine),
          ),
        ),
      )
      .subscribe(machine => this.store.dispatch(Asset.MachineActions.loadStatus({ machine })));

    // Load tasks of lines children.
    this.line$.pipe(takeUntil(this.destroy$)).subscribe(line => {
      if (line) {
        // Lines own tasks will be loaded above.
        const ids = line.children.map(c => c.id);
        const chunks = chunkArray(ids, 25);
        chunks.forEach(chunk =>
          this.store.dispatch(
            Task.loadTasks({
              filter: {
                assignedAsset: { value: chunk, operator: CondOperator.IN },
                status: { value: TaskStatus.ACTIVE, operator: CondOperator.EQUALS },
              },
            }),
          ),
        );
      }
    });

    // Get background image.
    const img = new Image();
    this.asset$
      .pipe(
        filter(asset => !!asset),
        switchMap(asset =>
          this.store.select(StoreImage.selectImageByRefId((asset as AssetDto).id)).pipe(
            switchMap(imgData =>
              iif(
                () => !!imgData,
                this.imageService.getImageBlob(imgData as ImageDto),
                of(undefined),
              ),
            ),
            catchError(() => of(undefined)),
          ),
        ),
        takeUntil(this.destroy$),
        // set background image only when it's completely loaded
        switchMap(url => {
          const src = url || this.placeholder;
          img.src = src;
          return fromEvent(img, 'load').pipe(map(() => src));
        }),
      )
      .subscribe(url => {
        this.setBackground(url);
        this.imageLoaded = true;
      });
  }

  private setBackground(url: string) {
    this.renderer.setStyle(this.assetImg.nativeElement, 'background-image', `url(${url})`);
  }

  truncateName(str: string) {
    return str.length <= 30 ? str : `${str.slice(0, 30)}...`;
  }
}
