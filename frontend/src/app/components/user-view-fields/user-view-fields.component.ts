import { Component, Input, OnInit } from '@angular/core';
import { AppUser } from '@common';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { State, User } from 'src/app/core/store';

@Component({
  selector: 'syn-user-view-fields',
  templateUrl: './user-view-fields.component.html',
  styleUrls: ['./user-view-fields.component.scss'],
})
export class UserViewFieldsComponent implements OnInit {
  @Input()
  user!: AppUser;

  roles$!: Observable<string>;

  constructor(private store: Store<State>) {}

  ngOnInit() {
    this.roles$ = this.store.select(User.selectRoles).pipe(
      map(roles =>
        roles
          .filter(r => this.user.roleIds.includes(r.id))
          .map(r => r.name)
          .join(', '),
      ),
    );
  }
}
