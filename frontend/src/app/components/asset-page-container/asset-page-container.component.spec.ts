import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetPageContainerComponent } from './asset-page-container.component';

describe('AssetPageContainerComponent', () => {
  let component: AssetPageContainerComponent;
  let fixture: ComponentFixture<AssetPageContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssetPageContainerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetPageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
