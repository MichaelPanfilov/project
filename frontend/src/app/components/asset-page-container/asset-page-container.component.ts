import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AssetDto, AssetType, BaseAssetTree } from '@common';
import { DefaultProjectorFn, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { distinctUntilChanged, filter, map, startWith, switchMap, take } from 'rxjs/operators';
import { lineDetailPath, machineDetailPath } from 'src/app/core/navigation';
import { Asset, State } from 'src/app/core/store';

const ASSET_UUID_REGEX = /^asset-[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

@Component({
  selector: 'syn-asset-page-container',
  templateUrl: './asset-page-container.component.html',
  styleUrls: ['./asset-page-container.component.scss'],
})
export class AssetPageContainerComponent implements OnInit {
  private assetType$: Observable<AssetType> = this.router.events.pipe(
    filter(ev => ev instanceof NavigationEnd),
    startWith(0),
    map(() => {
      // TODO: This approach does not scale well
      const url = this.router.url;
      if (url.endsWith('/machines') || url.includes('/machines/')) {
        return AssetType.MACHINE;
      }
      if (url.endsWith('/lines') || url.endsWith('/lines/')) {
        return AssetType.LINE;
      }
      return AssetType.FACTORY;
    }),
    distinctUntilChanged(),
  );

  loading$: Observable<boolean> = this.store
    .select(Asset.selectLoadedOnce)
    .pipe(map(loaded => !loaded));

  asset$ = this.assetType$.pipe(
    switchMap(type =>
      this.store.select(
        type === AssetType.MACHINE
          ? Asset.selectCurrentMachine
          : type === AssetType.LINE
          ? Asset.selectCurrentFactory
          : Asset.selectCurrentLine,
      ),
    ),
  );

  assets$ = this.assetType$.pipe(
    switchMap(type =>
      this.store.select(
        (type === AssetType.MACHINE
          ? Asset.selectCurrentLineTree
          : type === AssetType.LINE
          ? Asset.selectRoot
          : Asset.selectCurrentFactoryTree) as DefaultProjectorFn<BaseAssetTree>,
      ),
    ),
    map(tree => (tree ? tree.children : []) as BaseAssetTree[]),
  );

  get showBackButton() {
    return !this.router.url.endsWith('/lines');
  }

  constructor(private store: Store<State>, private router: Router) {}
  ngOnInit() {
    // TODO: Remove when factory can be selected by a user.
    // For now such page is missing.
    this.asset$
      .pipe(
        take(1),
        switchMap(asset =>
          asset
            ? this.store.select(Asset.selectTreeIncluding((asset as AssetDto).id))
            : this.store.select(Asset.selectFactoryTrees).pipe(
                filter(factories => !!factories.length),
                map(factories => factories[0]),
              ),
        ),
      )
      .subscribe(factory => {
        this.store.dispatch(Asset.TreeActions.setCurrent({ id: (factory as AssetDto).id }));
      });
  }

  changeAsset(asset?: AssetDto) {
    if (!asset) {
      return;
    }

    const routeSegments = this.router.url.split('/');
    const idSegmentIndex = routeSegments.findIndex(seg => seg.match(ASSET_UUID_REGEX));
    // TODO: Search the router tree and check the current route for dynamic route.
    // This solutions relies on uuid matching and will not work for more nested routes.
    const addSegments =
      idSegmentIndex === routeSegments.length - 1 ? [] : routeSegments.slice(idSegmentIndex + 1);

    // TODO: Move this to store.
    if (asset.type === AssetType.MACHINE) {
      this.router.navigate([...machineDetailPath(asset.id), ...addSegments]);
    } else if (asset.type === AssetType.LINE) {
      this.router.navigate([...lineDetailPath(asset.id), ...addSegments]);
    } else if (asset.type === AssetType.FACTORY) {
      this.store.dispatch(Asset.TreeActions.setCurrent({ id: asset.id }));
    }
  }

  onNavigateBack(): void {
    const routeSegments = this.router.url.split('/');
    const idSegmentIndex = routeSegments.findIndex(seg => seg.match(ASSET_UUID_REGEX));
    const current = routeSegments[idSegmentIndex - 1] || routeSegments[routeSegments.length - 1];

    if (
      current === 'machines' &&
      (this.router.url.endsWith('/durations') ||
        this.router.url.endsWith('/counts') ||
        this.router.url.endsWith('/product-losses') ||
        this.router.url.endsWith('/time-producing'))
    ) {
      this.store
        .select(Asset.selectCurrentMachine)
        .pipe(take(1))
        .subscribe(currentMachine => {
          if (currentMachine && currentMachine.id) {
            this.router.navigate(['/machines', currentMachine.id]);
          }
        });
    } else if (current === 'machines') {
      this.store
        .select(Asset.selectCurrentLine)
        .pipe(take(1))
        .subscribe(currentLine => {
          if (currentLine && currentLine.id) {
            this.router.navigate(['/lines', currentLine.id]);
          }
        });
    } else if (current === 'lines') {
      this.router.navigate(['/lines']);
    }
  }

  compareWith = (a: AssetDto, b: AssetDto) => a && b && a.id === b.id;
}
