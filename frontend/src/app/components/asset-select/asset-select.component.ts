import { ConnectedPosition, Overlay } from '@angular/cdk/overlay';
import { DOCUMENT } from '@angular/common';
import {
  Component,
  EventEmitter,
  forwardRef,
  Inject,
  Input,
  OnInit,
  Output,
  ViewContainerRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Router } from '@angular/router';
import { flattenAssets } from '@common';
import { isArray, isEmpty } from 'lodash';
import { fromEvent } from 'rxjs';
import { debounceTime, filter, takeUntil } from 'rxjs/operators';
import { DropdownMenu } from 'src/app/core/abstract';
import { AssetSelectItem } from 'src/app/util';

@Component({
  selector: 'syn-asset-select',
  templateUrl: './asset-select.component.html',
  styleUrls: ['./asset-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AssetSelectComponent),
      multi: true,
    },
  ],
})
export class AssetSelectComponent extends DropdownMenu implements ControlValueAccessor, OnInit {
  private onChange?: (value: string | string[]) => void;
  private onTouch?: () => void;
  private override?: string;

  protected positions: ConnectedPosition[] = [
    { originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'top' },
    { originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'bottom' },
  ];
  protected defaultOffsetX = 0;

  @Input()
  placeholder!: string;

  @Input()
  set label(label: string) {
    this.override = label;
  }

  get label(): string {
    if (this.override) {
      return this.override;
    }
    if (isEmpty(this.selected)) {
      return this.placeholder;
    }
    const asset = isArray(this.selected) ? this.selected[0] : this.selected;
    return asset ? asset.name : this.placeholder;
  }

  @Input()
  assets!: AssetSelectItem[];

  @Input()
  multiple?: boolean;

  @Input()
  isFilter?: boolean;

  @Input()
  error = false;

  @Input()
  disableShadow = false;

  @Input()
  selected?: AssetSelectItem | AssetSelectItem[];

  @Output()
  selectedChange = new EventEmitter<AssetSelectItem | AssetSelectItem[]>();

  set selectedAssets(items: AssetSelectItem[]) {
    if (!this.multiple) {
      this.selectedChange.emit(items[0]);
      this.hideMenu();
    } else {
      this.selectedChange.emit(items);
    }
  }

  get selectedAssets(): AssetSelectItem[] {
    if (isEmpty(this.selected)) {
      return [];
    }
    return isArray(this.selected) ? this.selected : [this.selected as AssetSelectItem];
  }

  get isResettable(): boolean {
    return !!(this.isFilter && this.selected && (!isArray(this.selected) || this.selected.length));
  }

  get selectedLength(): number {
    if (Array.isArray(this.selected)) {
      return this.selected.length;
    }
    return this.selected ? 1 : 0;
  }

  constructor(
    overlay: Overlay,
    viewContainerRef: ViewContainerRef,
    router: Router,
    @Inject(DOCUMENT) private doc: Document,
  ) {
    super(overlay, viewContainerRef, router);
  }

  ngOnInit() {
    super.ngOnInit();
    this.selectedChange.pipe(takeUntil(this.destroy$)).subscribe(selected => {
      // If used in a form we emit the assetId.
      if (this.onChange) {
        this.onChange(isArray(selected) ? selected.map(s => s.id) : selected.id);
      }
      if (this.onTouch) {
        this.onTouch();
      }
    });

    // Needed to trigger change detection and flip the components template.
    fromEvent(this.doc, 'scroll')
      .pipe(
        takeUntil(this.destroy$),
        filter(() => this.isOpen),
        debounceTime(25),
      )
      .subscribe(() => {});
  }

  writeValue(value?: string | string[]) {
    const ids = isArray(value) ? value : [value];
    const assets = flattenAssets(this.assets);

    if (this.multiple) {
      this.selected = assets.filter(asset => ids.includes(asset.id));
    } else {
      this.selected = assets.find(asset => ids.includes(asset.id));
    }
  }

  registerOnChange(fn: (value: string | string[]) => void) {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void) {
    this.onTouch = fn;
  }

  reset(e: Event) {
    e.preventDefault();
    e.stopPropagation();
    this.selected = [];
    this.selectedChange.emit(this.selected);
  }
}
