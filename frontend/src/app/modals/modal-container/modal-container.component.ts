import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'syn-modal-container',
  templateUrl: './modal-container.component.html',
  styleUrls: ['./modal-container.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalContainerComponent {
  @Input()
  title!: string;

  @Input()
  maxWidth?: string;

  @Input()
  isFullscreen?: boolean;

  @Input()
  backInsteadClose?: boolean;

  @Output()
  cancel = new EventEmitter();

  @Output()
  back = new EventEmitter();

  onCancel() {
    this.cancel.emit();
  }

  onBack() {
    this.back.emit();
  }
}
