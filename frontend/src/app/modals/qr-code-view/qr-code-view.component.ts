import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'syn-qr-code-view',
  templateUrl: './qr-code-view.component.html',
  styleUrls: ['./qr-code-view.component.scss'],
})
export class QrCodeViewComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { code: string; title: string; qrDownloadName?: string },
    public dialogRef: MatDialogRef<QrCodeViewComponent>,
  ) {}

  onOpenFile() {
    if (this.data.code) {
      window.print();
    }
  }

  onOpenPng() {
    if (this.data.code) {
      const image = new Image();
      image.src = this.data.code;
      const base64 = this.getBase64Image(image);

      this.openBase64InNewTab(base64, 'image/png');
    }
  }

  onClose() {
    this.dialogRef.close();
  }

  private getBase64Image(img: HTMLImageElement) {
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;

    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
    ctx.drawImage(img, 0, 0);
    const dataURL = canvas.toDataURL('image/png');

    return dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
  }

  private openBase64InNewTab(data: string, mimeType: string) {
    const byteCharacters = atob(data);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    const file = new Blob([byteArray], { type: mimeType + ';base64' });
    const fileURL = URL.createObjectURL(file);
    window.open(fileURL);
  }
}
