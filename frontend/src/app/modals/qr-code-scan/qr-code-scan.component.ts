import { Component, HostListener } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'syn-qr-code-scan',
  templateUrl: './qr-code-scan.component.html',
  styleUrls: ['./qr-code-scan.component.scss'],
})
export class QrCodeScanComponent {
  isCameraFound = true;
  focused = true;

  constructor(private dialogRef: MatDialogRef<QrCodeScanComponent>) {}

  @HostListener('window:focus', ['$event'])
  onFocus(event: Event) {
    this.focused = true;
  }

  @HostListener('window:blur', ['$event'])
  onBlur(event: Event) {
    this.focused = false;
  }

  displayNoCameraMessage() {
    this.isCameraFound = false;
  }

  redirect(e: string) {
    if (this.focused) {
      window.open(e);
    }
  }

  onClose() {
    this.dialogRef.close();
  }
}
