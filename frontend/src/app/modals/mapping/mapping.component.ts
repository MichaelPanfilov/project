import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface MappingModalData {
  device: string;
}

@Component({
  selector: 'syn-mapping',
  templateUrl: './mapping.component.html',
  styleUrls: ['./mapping.component.scss'],
})
export class MappingComponent {
  constructor(
    public dialogRef: MatDialogRef<MappingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MappingModalData,
  ) {}

  onClose() {
    this.dialogRef.close();
  }
}
