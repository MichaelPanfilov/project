import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'syn-machine-type-tag',
  templateUrl: './machine-type-tag.component.html',
  styleUrls: ['./machine-type-tag.component.scss'],
})
export class MachineTypeTagComponent {
  constructor(private dialogRef: MatDialogRef<MachineTypeTagComponent>) {}

  onClose() {
    this.dialogRef.close();
  }
}
