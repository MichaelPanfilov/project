import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineTypeTagComponent } from './machine-type-tag.component';

describe('MachineTypeTagComponent', () => {
  let component: MachineTypeTagComponent;
  let fixture: ComponentFixture<MachineTypeTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachineTypeTagComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineTypeTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
