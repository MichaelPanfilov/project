import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppUser, AssetDto, AssetType, TaskDto } from '@common';
import { take } from 'rxjs/operators';
import { AssetWithParent, getDeleteActionName, ModalModes } from 'src/app/util';

import { AboutComponent } from './about/about.component';
import { AssetModalComponent } from './asset/asset.component';
import {
  ConfirmationModalComponent,
  ConfirmationModalData,
} from './confirmation-modal/confirmation-modal.component';
import { DocumentComponent, DocumentModalData } from './document/document.component';
import { ImprintComponent } from './imprint/imprint.component';
import { LicensesComponent } from './licenses/licenses.component';
import { MachineTypeTagComponent } from './machine-type-tag/machine-type-tag.component';
import { MappingComponent, MappingModalData } from './mapping/mapping.component';
import { QrCodeScanComponent } from './qr-code-scan/qr-code-scan.component';
import { QrCodeViewComponent } from './qr-code-view/qr-code-view.component';
import { TaskModalComponent } from './task/task-modal.component';
import { UserComponent } from './user/user.component';
import { ViewAssetComponent } from './view-asset/view-asset.component';
import { ViewUserComponent } from './view-user/view-user.component';
import {
  InstructionModalData,
  WorkInstructionComponent,
} from './work-instruction/work-instruction.component';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  constructor(private dialog: MatDialog) {}

  openUserModal(user?: AppUser) {
    return this.dialog.open(UserComponent, {
      disableClose: true,
      data: { user },
      panelClass: ['fullscreen-modal', 'edit-modal'],
    });
  }

  openViewUserModal(user: AppUser) {
    return this.dialog.open(ViewUserComponent, {
      data: { user },
      panelClass: ['view-modal', 'user-view-modal'],
    });
  }

  openDocumentModal(data: DocumentModalData) {
    return this.dialog.open(DocumentComponent, {
      disableClose: true,
      data,
      panelClass: ['fullscreen-modal', 'edit-modal'],
    });
  }

  openTaskModal(mode: ModalModes, task?: Partial<TaskDto>) {
    return this.dialog.open(TaskModalComponent, {
      disableClose: true,
      data: { mode, task },
      panelClass: ['task-modal', 'fullscreen-modal', 'edit-modal'],
    });
  }

  openViewAssetModal(asset: AssetDto) {
    return this.dialog.open(ViewAssetComponent, {
      data: { asset },
      panelClass: ['view-modal', 'user-view-modal'],
    });
  }

  openAssetModal(type: AssetType, asset?: Partial<AssetWithParent<AssetDto>>) {
    return this.dialog.open(AssetModalComponent, {
      data: { asset, type },
      panelClass: ['fullscreen-modal', 'edit-modal'],
    });
  }

  openEditAssetModal(type: AssetType, asset: Partial<AssetWithParent<AssetDto>>) {
    return this.dialog.open(AssetModalComponent, {
      data: { asset, type: asset.type },
      panelClass: ['fullscreen-modal', 'edit-modal'],
    });
  }

  openConfirmModal(data: ConfirmationModalData<string>) {
    return this.dialog.open(ConfirmationModalComponent, {
      data,
      panelClass: ['confirm-modal'],
    });
  }

  openDeleteModal(data: { entity: string; name: string }) {
    return this.openConfirmModal({
      value: data.name,
      message: 'COMMON.ACTION.DELETE_CONFIRM',
      actionName: getDeleteActionName(data.entity),
    });
  }

  openAboutModal(data: { text: string; title?: string }) {
    return this.dialog.open(AboutComponent, {
      data,
      panelClass: ['fullscreen-modal'],
    });
  }

  openLicensesModal() {
    return this.dialog.open(LicensesComponent, { panelClass: ['fullscreen-modal'] });
  }

  openImprintModal() {
    return this.dialog.open(ImprintComponent, { panelClass: ['fullscreen-modal'] });
  }

  openMappingModal(device: string) {
    this.dialog.open<MappingComponent, MappingModalData>(MappingComponent, { data: { device } });
  }

  openTagCreateModal() {
    this.dialog.open(MachineTypeTagComponent);
  }

  openQrCodeViewModal(code: string, title: string, downloadName: string) {
    this.dialog.open(QrCodeViewComponent, {
      data: { code, title, qrDownloadName: `QR-${downloadName}.png` },
      panelClass: 'qr-modal',
    });
  }

  openQrCodeScanModal() {
    this.dialog.open(QrCodeScanComponent);
  }

  openWorkInstructionModal(data: InstructionModalData) {
    return this.dialog.open(WorkInstructionComponent, {
      disableClose: true,
      data,
      panelClass: ['fullscreen-modal', 'edit-modal'],
    });
  }

  closeAll() {
    this.dialog.closeAll();
    return this.dialog.afterAllClosed.pipe(take(1));
  }
}
