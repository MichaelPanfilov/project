import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkInstructionFormComponent } from './work-instruction-form.component';

describe('WorkInstructionFormComponent', () => {
  let component: WorkInstructionFormComponent;
  let fixture: ComponentFixture<WorkInstructionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WorkInstructionFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkInstructionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
