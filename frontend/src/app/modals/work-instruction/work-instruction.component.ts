import { Component, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import {
  BaseAssetTree,
  flattenAssets,
  getRefIdsOf,
  InstructionDto,
  ReferencedEntities,
} from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { MultiForm } from 'src/app/core/abstract/multi-form';
import { Asset, Instruction, Settings, State } from 'src/app/core/store';
import {
  InstructionForm,
  WorkInstructionFormComponent,
} from 'src/app/forms/work-instruction-form/work-instruction-form.component';
import { getDeleteActionName, ModalModes, WorkInstructionFormContent } from 'src/app/util';

import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';

export interface InstructionModalData {
  mode: ModalModes;
  instruction?: Partial<InstructionDto>;
  assetIds?: string[];
}

const TITLES = {
  [WorkInstructionFormContent.MACHINE_TAGS]: 'SHARED.RELATION.MACHINE_TYPE_TAGS',
  [WorkInstructionFormContent.TASKS]: 'SHARED.RELATION.TASKS',
  [WorkInstructionFormContent.ASSETS]: 'SHARED.RELATION.ASSETS',
};

@Component({
  selector: 'syn-work-instruction',
  templateUrl: './work-instruction.component.html',
  styleUrls: ['./work-instruction.component.scss'],
})
export class WorkInstructionComponent extends MultiForm {
  @ViewChild('instructionForm', { static: true })
  instructionForm!: WorkInstructionFormComponent;

  currentTab = 0;
  formType = WorkInstructionFormContent.WORK_INSTRUCTION;
  selectedMachineTags: string[] = [];
  selectedTasks: string[] = [];

  saveDisabled$ = new BehaviorSubject(true);
  assets$ = this.store.select(Asset.selectAllAssetTrees);
  selectedAssetsIds$ = new BehaviorSubject<string[]>([]);
  selectedAssets$ = combineLatest([this.selectedAssetsIds$, this.assets$]).pipe(
    map(([selectedIds, assets]) => flattenAssets(assets).filter(a => selectedIds.includes(a.id))),
  );
  disabled$ = combineLatest([
    this.saveDisabled$,
    this.store.select(
      Settings.selectLoadingByAction(Instruction.createInstruction, Instruction.updateInstruction),
    ),
  ]).pipe(map(([disabled, loading]) => disabled || loading));

  get title() {
    if (!this.formType) {
      return this.isCreate
        ? 'WORK_INSTRUCTION.CREATE_MODAL_TITLE'
        : 'WORK_INSTRUCTION.UPDATE_MODAL_TITLE';
    }
    return TITLES[this.formType];
  }

  get isCreate() {
    return !this.data.instruction || !this.data.instruction.id;
  }

  get assetIds() {
    return this.data.assetIds || [];
  }

  get isMainForm(): boolean {
    return !this.formType;
  }

  get isLinkForm(): boolean {
    return !!this.formType;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: InstructionModalData,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<WorkInstructionComponent>,
    private store: Store<State>,
    private actions$: Actions,
  ) {
    super();
    if (this.data.instruction) {
      this.selectedMachineTags = this.data.instruction.machineTypes || [];
      if (this.data.instruction.links) {
        const refIds = this.data.instruction.links;
        this.selectedAssetsIds$.next(getRefIdsOf(ReferencedEntities.ASSET, refIds));
        this.selectedTasks = getRefIdsOf(ReferencedEntities.TASK, refIds);
      }
    }
  }

  onClose() {
    if (this.instructionForm.hasBeenUpdated()) {
      const confirmDialogRef = this.dialog.open(ConfirmationModalComponent, {
        data: {
          message: 'COMMON.ACTION.CANCEL_CONFIRM',
          actionName: 'COMMON.ACTION.LEAVE_PAGE',
        },
        panelClass: ['confirm-modal'],
      });
      confirmDialogRef.afterClosed().subscribe(res => (!!res ? this.dialogRef.close() : null));
    } else {
      this.dialogRef.close();
    }
  }

  onEscape(event: KeyboardEvent) {
    event.stopPropagation();
    this.onClose();
  }

  onExit() {
    this.dialogRef.close();
  }

  onDelete() {
    if (!this.isCreate) {
      const dialogRef = this.dialog.open(ConfirmationModalComponent, {
        data: {
          value: (this.data.instruction as InstructionDto).title,
          message: 'COMMON.ACTION.DELETE_CONFIRM',
          actionName: getDeleteActionName('work instruction'),
        },
        panelClass: ['confirm-modal'],
      });

      dialogRef
        .afterClosed()
        .pipe(
          take(1),
          filter(res => !!res),
        )
        .subscribe(() => {
          if (this.data.instruction) {
            this.store.dispatch(
              Instruction.deleteInstruction({ instructionId: this.data.instruction.id as string }),
            );
            this.actions$
              .pipe(ofType(Instruction.deletedInstruction), take(1))
              .subscribe(() => this.onExit());
          }
        });
    }
  }

  addLinks() {
    if (this.formType === WorkInstructionFormContent.MACHINE_TAGS) {
      this.updateSelectedEntities('machineTypes', this.selectedMachineTags);
    } else if (this.formType === WorkInstructionFormContent.ASSETS) {
      this.updateSelectedEntities('assetIds', this.selectedAssetsIds$.value);
    } else if (this.formType === WorkInstructionFormContent.TASKS) {
      this.updateSelectedEntities('taskIds', this.selectedTasks);
    }
  }

  updateSelectedEntities(controlName: keyof InstructionForm, value: string[]) {
    this.instructionForm.getControl(controlName).setValue(value);
    this.switchToMainForm();
  }

  onTabChange(event: MatTabChangeEvent) {
    this.currentTab = event.index;
    this.formType = this.currentTab;
  }

  onOpenAssignment(type: WorkInstructionFormContent) {
    this.formType = type;
    this.currentTab = this.formType;
  }

  onSelectedAssetsChange(assets: BaseAssetTree[]) {
    this.selectedAssetsIds$.next(assets.map(a => a.id));
  }

  switchToMainForm() {
    this.formType = WorkInstructionFormContent.WORK_INSTRUCTION;
    this.currentTab = this.formType;
  }
}
