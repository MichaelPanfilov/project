import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AssetDto, AssetType } from '@common';
import { AssetTreeElementActions } from 'src/app/util';

@Component({
  selector: 'syn-view-asset',
  templateUrl: './view-asset.component.html',
  styleUrls: ['./view-asset.component.scss'],
})
export class ViewAssetComponent {
  @ViewChild('assetImg', { static: true })
  assetImg!: ElementRef;

  assetType = AssetType;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { asset: AssetDto },
    public dialogRef: MatDialogRef<ViewAssetComponent>,
  ) {}

  async onEditAsset() {
    this.dialogRef.close(AssetTreeElementActions.EDIT);
  }
}
