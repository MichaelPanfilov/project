import { Component, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { BaseAssetTree, FileDto, flattenAssets, getRefIdsOf, ReferencedEntities } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { MultiForm } from 'src/app/core/abstract/multi-form';
import { Asset, Document, Settings, State } from 'src/app/core/store';
import { DocumentForm, DocumentFormComponent } from 'src/app/forms';
import { DocumentFormContent, getDeleteActionName, ModalModes } from 'src/app/util';

import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';

export interface DocumentModalData {
  mode: ModalModes;
  document?: Partial<FileDto>;
}

const TITLES = {
  [DocumentFormContent.MACHINE_TAGS]: 'SHARED.RELATION.MACHINE_TYPE_TAGS',
  [DocumentFormContent.ASSETS]: 'SHARED.RELATION.LINKED_ASSETS',
};

@Component({
  selector: 'syn-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
})
export class DocumentComponent extends MultiForm {
  @ViewChild('documentForm', { static: true })
  documentForm!: DocumentFormComponent;

  currentTab = 0;
  formType = DocumentFormContent.DOCUMENT;

  selectedMachineTags: string[] = [];

  saveDisabled$ = new BehaviorSubject(true);
  selectedAssetsIds$ = new BehaviorSubject<string[]>([]);
  assets$ = this.store.select(Asset.selectAllAssetTrees);
  selectedAssets$ = combineLatest([this.selectedAssetsIds$, this.assets$]).pipe(
    map(([selectedIds, assets]) => flattenAssets(assets).filter(a => selectedIds.includes(a.id))),
  );
  disabled$ = combineLatest([
    this.saveDisabled$,
    this.store.select(
      Settings.selectLoadingByAction(Document.uploadDocument, Document.updateDocument),
    ),
  ]).pipe(map(([disabled, loading]) => disabled || loading));

  get title() {
    if (!this.formType) {
      return this.isCreate ? 'DOCUMENT.CREATE_MODAL_TITLE' : 'DOCUMENT.UPDATE_MODAL_TITLE';
    }
    return TITLES[this.formType];
  }

  get isCreate() {
    return this.data.mode === ModalModes.CREATE || !(this.data.document && this.data.document.id);
  }

  get isMainForm(): boolean {
    return !this.formType;
  }

  get isLinkForm(): boolean {
    return !!this.formType;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: DocumentModalData,
    public dialog: MatDialog,
    private dialogRef: MatDialogRef<DocumentComponent>,
    private store: Store<State>,
    private actions$: Actions,
  ) {
    super();
    if (this.data.document) {
      if (this.data.document.refIds && this.data.document.refIds.length) {
        this.selectedAssetsIds$.next(
          getRefIdsOf(ReferencedEntities.ASSET, this.data.document.refIds),
        );
      }
      if (this.data.document.machineTypeTags && this.data.document.machineTypeTags.length) {
        this.selectedMachineTags = this.data.document.machineTypeTags;
      }
    }
  }

  onClose() {
    if (this.documentForm.hasBeenUpdated()) {
      const confirmDialogRef = this.dialog.open(ConfirmationModalComponent, {
        data: {
          message: 'COMMON.ACTION.CANCEL_CONFIRM',
          actionName: 'COMMON.ACTION.LEAVE_PAGE',
        },
        panelClass: ['confirm-modal'],
      });
      confirmDialogRef.afterClosed().subscribe(res => (!!res ? this.dialogRef.close() : null));
    } else {
      this.dialogRef.close();
    }
  }

  onExit() {
    this.dialogRef.close();
  }

  onEscape(event: KeyboardEvent) {
    event.stopPropagation();
    this.onClose();
  }

  onDelete() {
    if (!this.isCreate) {
      const dialogRef = this.dialog.open(ConfirmationModalComponent, {
        data: {
          value: (this.data.document as FileDto).title,
          message: 'COMMON.ACTION.DELETE_CONFIRM',
          actionName: getDeleteActionName('document'),
        },
        panelClass: ['confirm-modal'],
      });

      dialogRef
        .afterClosed()
        .pipe(
          take(1),
          filter(res => !!res),
        )
        .subscribe(() => {
          if (this.data.document) {
            this.store.dispatch(
              Document.deleteDocument({ documentId: this.data.document.id as string }),
            );
            this.actions$
              .pipe(ofType(Document.deletedDocument), take(1))
              .subscribe(() => this.onExit());
          }
        });
    }
  }

  onOpenAssignment(type: DocumentFormContent) {
    this.formType = type;
    this.currentTab = this.formType;
  }

  onTabChange(event: MatTabChangeEvent) {
    this.currentTab = event.index;
    this.formType = this.currentTab;
  }

  addLinks() {
    if (this.formType === DocumentFormContent.MACHINE_TAGS) {
      this.updateSelectedEntities('machineTypeTags', this.selectedMachineTags);
    } else if (this.formType === DocumentFormContent.ASSETS) {
      this.updateSelectedEntities('assetIds', this.selectedAssetsIds$.value);
    }
  }

  updateSelectedEntities(controlName: keyof DocumentForm, value: string[]) {
    this.documentForm.getControl(controlName).setValue(value);
    this.switchToMainForm();
  }

  onSelectedAssetsChange(assets: BaseAssetTree[]) {
    this.selectedAssetsIds$.next(assets.map(a => a.id));
  }

  switchToMainForm() {
    this.formType = DocumentFormContent.DOCUMENT;
    this.currentTab = this.formType;
  }
}
