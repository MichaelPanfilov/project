import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AssetDto, AssetType, BaseAssetTree, getAssetTypeLabel } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { Asset, Settings, State } from 'src/app/core/store';
import { AssetWithParent, getDeleteActionName } from 'src/app/util';

import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';

@Component({
  selector: 'syn-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.scss'],
})
export class AssetModalComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  asset?: Partial<AssetWithParent<AssetDto>>;
  changed = false;
  type = AssetType.FACTORY;
  valid$ = new BehaviorSubject(false);
  assetTrees$: Observable<BaseAssetTree[] | undefined> = of(undefined);
  deletable$ = of(true);
  disabled$ = combineLatest([
    this.valid$,
    this.store.select(
      Settings.selectLoadingByAction(
        Asset.createAsset,
        Asset.updateAsset,
        Asset.uploadAssetImage,
        Asset.updateAssetImage,
      ),
    ),
  ]).pipe(map(([valid, loading]) => !valid || loading));

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { asset?: Partial<AssetWithParent<AssetDto>>; type: AssetType; parent?: string },
    private store: Store<State>,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<AssetModalComponent>,
    private actions$: Actions,
  ) {}

  ngOnInit() {
    this.type = this.data.type;
    this.asset = this.data.asset;
    if (this.data.asset && this.data.asset.id) {
      this.store
        .select(Asset.selectParent(this.data.asset.id))
        .pipe(takeUntil(this.destroy$))
        .subscribe(parent => {
          this.asset = {
            parent: parent ? parent.id : undefined,
            ...this.asset,
          } as AssetWithParent<AssetDto>;
        });
      this.assetTrees$ = this.store.select(Asset.selectAssetTree(this.data.asset.id)).pipe(
        takeUntil(this.destroy$),
        map(tree => (tree && tree.children ? (tree.children as BaseAssetTree[]) : [])),
      );
      this.deletable$ = this.assetTrees$.pipe(map(tree => !tree || tree.length < 1));
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onDelete() {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      data: {
        value: (this.asset as AssetDto).name,
        message: 'COMMON.ACTION.DELETE_CONFIRM',
        actionName: getDeleteActionName('asset'),
      },
      panelClass: ['confirm-modal'],
    });

    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter(res => !!res),
      )
      .subscribe(() => {
        if (this.asset) {
          this.store.dispatch(Asset.deleteAsset({ asset: this.asset as AssetDto }));
          this.actions$.pipe(ofType(Asset.deletedAsset), take(1)).subscribe(() => this.onClose());
        }
      });
  }

  onClose(asset?: AssetDto) {
    if (this.changed) {
      const confirmDialogRef = this.dialog.open(ConfirmationModalComponent, {
        data: {
          message: 'COMMON.ACTION.CANCEL_CONFIRM',
          actionName: 'COMMON.ACTION.LEAVE_PAGE',
        },
        panelClass: ['confirm-modal'],
      });
      confirmDialogRef.afterClosed().subscribe(res => (!!res ? this.onExit(asset) : null));
    } else {
      this.onExit(asset);
    }
  }

  onEscape(event: KeyboardEvent) {
    event.stopPropagation();
    this.onClose();
  }

  onExit(asset?: AssetDto) {
    if (asset) {
      this.dialogRef
        .afterClosed()
        .pipe(take(1))
        .subscribe(() =>
          this.dialog.open(AssetModalComponent, {
            data: { asset, type: asset.type },
            panelClass: ['fullscreen-modal', 'edit-modal'],
          }),
        );
    }
    this.dialogRef.close();
  }

  getAssetTypeLabel(type: AssetType): string {
    return getAssetTypeLabel(type);
  }
}
