import { Component, ElementRef, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppUser, USER_AVATAR_TAG } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { ImageUploader } from 'src/app/core/abstract';
import { FileService } from 'src/app/core/services/api/file.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Settings, State, User } from 'src/app/core/store';
import { getDeleteActionName } from 'src/app/util';

import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';

const AVATAR_PLACEHOLDER = '../../../assets/imgs/avatar-placeholder.png';

@Component({
  selector: 'syn-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent extends ImageUploader implements OnInit {
  protected backgroundSize = 'cover';

  @ViewChild('avatarImg', { static: true })
  imgRef!: ElementRef;

  get isCreate() {
    return !(this.data.user && this.data.user.id);
  }

  changed = false;
  valid$ = new BehaviorSubject(false);
  disabled$ = combineLatest([
    this.valid$,
    this.store.select(
      Settings.selectLoadingByAction(
        User.createUser,
        User.updateUser,
        User.uploadUserImg,
        User.updateRoles,
        User.changePassword,
        User.changeUserPassword,
        User.loadMe,
      ),
    ),
  ]).pipe(map(([valid, loading]) => !valid || loading));

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { user: AppUser },
    private store: Store<State>,
    private actions$: Actions,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<UserComponent>,
    fileService: FileService,
    notificationService: NotificationService,
    renderer: Renderer2,
  ) {
    super(fileService, notificationService, renderer);
  }

  ngOnInit() {
    this.setImg('');
    const { user } = this.data;
    if (user) {
      this.initImage(user.id, USER_AVATAR_TAG);
    }
  }

  onClose() {
    if (this.changed) {
      const confirmDialogRef = this.dialog.open(ConfirmationModalComponent, {
        data: {
          message: 'COMMON.ACTION.CANCEL_CONFIRM',
          actionName: 'COMMON.ACTION.LEAVE_PAGE',
        },
        panelClass: ['confirm-modal'],
      });
      confirmDialogRef.afterClosed().subscribe(res => (!!res ? this.dialogRef.close() : null));
    } else {
      this.dialogRef.close();
    }
  }

  onEscape(event: KeyboardEvent) {
    event.stopPropagation();
    this.onClose();
  }

  onDelete() {
    const { user } = this.data;

    if (user) {
      const dialogRef = this.dialog.open(ConfirmationModalComponent, {
        data: {
          value: user.name,
          message: 'COMMON.ACTION.DELETE_CONFIRM',
          actionName: getDeleteActionName('user'),
        },
        panelClass: ['confirm-modal'],
      });

      dialogRef
        .afterClosed()
        .pipe(
          take(1),
          filter(res => !!res),
        )
        .subscribe(() => {
          this.store.dispatch(User.deleteUser({ userId: user.id }));
          this.dialogRef.close();
        });
    }
  }

  onUserCreated(user: AppUser) {
    if (this.uploadedImage) {
      this.store.dispatch(
        User.uploadUserImg({ image: this.uploadedImage, prevImageId: this.imageId, user }),
      );

      this.actions$
        .pipe(ofType(User.uploadedUserImg), take(1))
        .subscribe(() => this.dialogRef.close());
    } else {
      this.dialogRef.close();
    }
  }

  protected getPlaceholder(): string {
    return `url(${AVATAR_PLACEHOLDER}) 50% 50% no-repeat`;
  }
}
