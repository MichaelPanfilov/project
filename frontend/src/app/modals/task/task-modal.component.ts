import { Component, Inject, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { BaseAssetTree, FileDto, flattenAssets, InstructionDto, TaskDto } from '@common';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { LinkDocumentsComponent, LinkInstructionsComponent } from 'src/app/components/link-tables';
import { MultiForm } from 'src/app/core/abstract';
import { Asset, Document, Instruction, Settings, State, Task } from 'src/app/core/store';
import { DocumentForm } from 'src/app/forms/document-form/document-form.component';
import { TaskForm, TaskFormComponent } from 'src/app/forms/task-form/task-form.component';
import { InstructionForm } from 'src/app/forms/work-instruction-form/work-instruction-form.component';
import {
  DocumentFormContent,
  getDeleteActionName,
  ModalModes,
  TaskFormContent,
  WorkInstructionFormContent,
} from 'src/app/util';

import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';

const TITLES = {
  [TaskFormContent.WORK_INSTRUCTIONS]: 'WORK_INSTRUCTION.NAME',
  [TaskFormContent.MACHINE_TAGS]: 'SHARED.RELATION.MACHINE_TYPE_TAGS',
  [TaskFormContent.DOCUMENTS]: 'SHARED.RELATION.DOCUMENTS',
  [TaskFormContent.ASSETS]: 'SHARED.RELATION.LINKED_ASSETS',
};

const FORM_IDS = {
  [TaskFormContent.TASK]: 'task-form',
  [TaskFormContent.WORK_INSTRUCTIONS]: 'link-wi-form',
  [TaskFormContent.DOCUMENTS]: 'link-doc-form',
};

interface SubTabs {
  documents?: number;
  instructions?: number;
}

@Component({
  selector: 'syn-task-modal',
  templateUrl: './task-modal.component.html',
  styleUrls: ['./task-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TaskModalComponent extends MultiForm implements OnInit {
  @ViewChild('taskForm', { static: true })
  taskForm!: TaskFormComponent;

  @ViewChild('submitBtn')
  submitBtn!: HTMLButtonElement;

  @ViewChild('linkInstructions')
  linkInstructions!: LinkInstructionsComponent;

  @ViewChild('linkDocuments')
  linkDocuments!: LinkDocumentsComponent;

  get isCreate() {
    return !(this.data.task && this.data.task.id);
  }

  get title() {
    if (!this.formType) {
      return this.isCreate ? 'TASK.CREATE_MODAL_TITLE' : 'TASK.UPDATE_MODAL_TITLE';
    }
    return TITLES[this.formType];
  }

  get isMainForm(): boolean {
    return this.formType === TaskFormContent.TASK;
  }

  get isLinkForm(): boolean {
    return this.formType !== TaskFormContent.TASK;
  }
  isAddForm = false;

  saveDisabled$ = new BehaviorSubject(true);
  formDisabled$ = new BehaviorSubject(true);
  selectedAssetsIds$ = new BehaviorSubject<string[]>([]);
  assets$: Observable<BaseAssetTree[] | undefined> = this.store.select(Asset.selectAllAssetTrees);
  selectedAssets$ = combineLatest([this.selectedAssetsIds$, this.assets$]).pipe(
    map(([selectedIds, assets]) =>
      assets ? flattenAssets(assets).filter(a => selectedIds.includes(a.id)) : [],
    ),
  );
  disabled$ = combineLatest([
    combineLatest([this.saveDisabled$, this.formDisabled$]).pipe(
      map(([saveDisabled, formDisabled]) => (this.isMainForm ? formDisabled : saveDisabled)),
    ),
    this.store.select(
      Settings.selectLoadingByAction(
        Task.createTask,
        Task.updateTask,
        Instruction.createInstruction,
        Instruction.linkToTask,
        Instruction.unlinkFromTask,
        Document.uploadDocument,
        Document.assignToTask,
        Document.unassignFromTask,
      ),
    ),
  ]).pipe(map(([disabled, loading]) => disabled || loading));

  subTabs: SubTabs = {};

  selectedInstructions: string[] = [];
  selectedMachineTags: string[] = [];
  selectedDocuments: string[] = [];
  selectedTasksIds: string[] = [];

  formType: TaskFormContent = TaskFormContent.TASK;
  sourceForm: TaskFormContent = TaskFormContent.TASK;
  formIds = FORM_IDS;
  taskInstructionsTabs: Omit<Record<WorkInstructionFormContent, TaskFormContent>, 0> = {
    [WorkInstructionFormContent.MACHINE_TAGS]: TaskFormContent.MACHINE_TAGS,
    [WorkInstructionFormContent.ASSETS]: TaskFormContent.ASSETS,
    [WorkInstructionFormContent.TASKS]: TaskFormContent.TASKS_LINK,
  };

  taskDocumentsTabs: Omit<Record<DocumentFormContent, TaskFormContent>, 0> = {
    [DocumentFormContent.ASSETS]: TaskFormContent.ASSETS,
    [DocumentFormContent.MACHINE_TAGS]: TaskFormContent.MACHINE_TAGS,
  };

  set currentTab(index: TaskFormContent) {
    if (
      [TaskFormContent.MACHINE_TAGS, TaskFormContent.ASSETS, TaskFormContent.TASKS_LINK].includes(
        index,
      )
    ) {
      this.isAddForm = false;
      if (this.sourceForm === TaskFormContent.TASK) {
        this.applyTaskSelectedEntitites();
      }
    } else if (TaskFormContent.DOCUMENTS === index) {
      this.isAddForm = !this.subTabs.documents;
      if (this.sourceForm !== TaskFormContent.TASK) {
        this.applyNewDocumentSelectedEntities();
      }
    } else if (TaskFormContent.WORK_INSTRUCTIONS === index) {
      this.isAddForm = !this.subTabs.instructions;
      if (this.sourceForm !== TaskFormContent.TASK) {
        this.applyNewInstructionSelectedEntities();
      }
    }
    this.currentTabIndex = index;
  }

  get currentTab() {
    return this.currentTabIndex;
  }

  currentTabIndex = 0;
  isTaskFormExpanded = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { mode: ModalModes; task?: Partial<TaskDto> },
    private store: Store<State>,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<TaskModalComponent>,
  ) {
    super();
  }

  ngOnInit(): void {
    if (!this.isCreate) {
      this.taskForm.initialized$.subscribe(() => {
        combineLatest([
          this.taskForm.getControl('instructionIds').valueChanges,
          this.taskForm.getControl('docIds').valueChanges,
        ])
          .pipe(take(1))
          .subscribe(([instructionsIds, docIds]) => {
            this.selectedInstructions = instructionsIds;
            this.selectedDocuments = docIds;
            // Update the copy manually once we change this.
            this.taskForm.updateCopy();
          });
      });
    }
  }

  onClose() {
    if (this.taskForm.hasBeenUpdated()) {
      const confirmDialogRef = this.dialog.open(ConfirmationModalComponent, {
        data: {
          message: 'COMMON.ACTION.CANCEL_CONFIRM',
          actionName: 'COMMON.ACTION.LEAVE_PAGE',
        },
        panelClass: ['confirm-modal'],
      });
      confirmDialogRef.afterClosed().subscribe(res => (!!res ? this.dialogRef.close() : null));
    } else {
      this.dialogRef.close();
    }
  }

  onExit() {
    this.dialogRef.close();
  }

  onEscape(event: KeyboardEvent) {
    event.stopPropagation();
    this.onClose();
  }

  onDelete() {
    const { task } = this.data;

    if (task && task.id) {
      const dialogRef = this.dialog.open(ConfirmationModalComponent, {
        data: {
          value: task.title,
          message: 'COMMON.ACTION.DELETE_CONFIRM',
          actionName: getDeleteActionName('task'),
        },
        panelClass: ['confirm-modal'],
      });

      dialogRef
        .afterClosed()
        .pipe(
          take(1),
          filter(res => !!res),
        )
        .subscribe(() => {
          this.store.dispatch(Task.deleteTask({ taskId: task.id as string }));
          this.dialogRef.close();
        });
    }
  }

  // TODO: Extract links update to abstract class (?)
  addLinks() {
    this.isTaskFormExpanded = true;
    if (this.sourceForm === TaskFormContent.TASK) {
      this.addTaskLinks();
    } else if (this.sourceForm === TaskFormContent.DOCUMENTS) {
      this.addDocumentLinks();
    } else if (this.sourceForm === TaskFormContent.WORK_INSTRUCTIONS) {
      this.addInstructionLinks();
    }
  }

  addTaskLinks() {
    if (this.formType === TaskFormContent.WORK_INSTRUCTIONS) {
      this.updateInstructions();
    } else if (this.formType === TaskFormContent.MACHINE_TAGS) {
      this.updateMachineTags();
    } else if (this.formType === TaskFormContent.DOCUMENTS) {
      this.updateDocuments();
    }
  }

  addDocumentLinks() {
    if (this.formType === TaskFormContent.MACHINE_TAGS) {
      this.updateDocumentSelectedEntitites('machineTypeTags', this.selectedMachineTags);
    } else if (this.formType === TaskFormContent.ASSETS) {
      this.updateDocumentSelectedEntitites('assetIds', this.selectedAssetsIds$.value);
    }
  }

  addInstructionLinks() {
    if (this.formType === TaskFormContent.MACHINE_TAGS) {
      this.updateInstructionSelectedEntities('machineTypes', this.selectedMachineTags);
    } else if (this.formType === TaskFormContent.ASSETS) {
      this.updateInstructionSelectedEntities('assetIds', this.selectedAssetsIds$.value);
    } else if (this.formType === TaskFormContent.TASKS_LINK) {
      this.updateInstructionSelectedEntities('taskIds', this.selectedTasksIds);
    }
  }

  updateTaskSelectedEntities(controlName: keyof TaskForm, value: string[]) {
    this.taskForm.getControl(controlName).setValue(value);
    this.switchToMainForm();
  }

  updateDocumentSelectedEntitites(controlName: keyof DocumentForm, value: string[]) {
    this.linkDocuments.documentForm.getControl(controlName).setValue(value);
    this.switchToDocumentForm();
    this.sourceForm = TaskFormContent.TASK;
  }

  updateInstructionSelectedEntities(controlName: keyof InstructionForm, value: string[]) {
    this.linkInstructions.instructionForm.getControl(controlName).setValue(value);
    this.switchToInstructionForm();
    this.sourceForm = TaskFormContent.TASK;
  }

  resetSelectedEntities() {
    this.selectedDocuments = [];
    this.selectedInstructions = [];
    this.selectedMachineTags = [];
    this.selectedAssetsIds$.next([]);
    this.selectedTasksIds = [];
  }

  applyTaskSelectedEntitites() {
    this.selectedDocuments = this.taskForm.getControl('docIds').value;
    this.selectedInstructions = this.taskForm.getControl('instructionIds').value;
    this.selectedMachineTags = this.taskForm.getControl('machineTypes').value;
    this.selectedAssetsIds$.next([]);
    this.selectedTasksIds = [];
  }

  applyNewDocumentSelectedEntities() {
    this.selectedDocuments = [];
    this.selectedInstructions = [];
    this.selectedMachineTags = this.linkDocuments.documentForm.getControl('machineTypeTags').value;
    this.selectedAssetsIds$.next(this.linkDocuments.documentForm.getControl('assetIds').value);
    this.selectedTasksIds = [];
  }

  applyNewInstructionSelectedEntities() {
    this.selectedDocuments = [];
    this.selectedInstructions = [];
    this.selectedMachineTags = this.linkInstructions.instructionForm.getControl(
      'machineTypes',
    ).value;
    this.selectedAssetsIds$.next(
      this.linkInstructions.instructionForm.getControl('assetIds').value,
    );
    this.selectedTasksIds = this.linkInstructions.instructionForm.getControl('taskIds').value;
  }

  updateInstructions() {
    this.updateTaskSelectedEntities('instructionIds', this.selectedInstructions);
  }

  updateMachineTags() {
    this.updateTaskSelectedEntities('machineTypes', this.selectedMachineTags);
  }

  updateDocuments() {
    this.updateTaskSelectedEntities('docIds', this.selectedDocuments);
  }

  onSelectedAssetsChange(assets: BaseAssetTree[]) {
    this.selectedAssetsIds$.next(assets.map(a => a.id));
  }

  onSelectedInstructionsAdd(item: InstructionDto) {
    this.selectedInstructions = [...this.selectedInstructions, item.id];
    this.updateInstructions();
    this.linkInstructions.instructionForm.resetToInitialValue();
  }

  onSelectedDocumentsAdd(item: FileDto) {
    this.selectedDocuments = [...this.selectedDocuments, item.id];
    this.updateDocuments();
    this.linkDocuments.documentForm.resetToInitialValue();
    this.linkDocuments.documentForm.getControl('file').updateValueAndValidity();
  }

  onOpenAssignment(type: TaskFormContent) {
    this.formType = type;
    this.updateSubTab('instructions', 1);
    this.updateSubTab('documents', 1);
    this.currentTab = this.formType;
  }

  onOpenDocumentAssignment(type: DocumentFormContent) {
    this.sourceForm = TaskFormContent.DOCUMENTS;
    if (type in this.taskDocumentsTabs) {
      this.formType = this.taskDocumentsTabs[type];
      this.applyNewDocumentSelectedEntities();
    }
    this.currentTab = this.formType;
  }

  onOpenInstructionAssignment(type: WorkInstructionFormContent) {
    this.sourceForm = TaskFormContent.WORK_INSTRUCTIONS;
    if (type in this.taskInstructionsTabs) {
      this.formType = this.taskInstructionsTabs[type];
      this.applyNewInstructionSelectedEntities();
    }
    this.currentTab = this.formType;
  }

  onTabChange(event: MatTabChangeEvent) {
    this.currentTab = event.index;
    this.formType = this.currentTab;
  }

  switchToMainForm() {
    this.currentTab = this.sourceForm;
    this.sourceForm = TaskFormContent.TASK;
    this.applyTaskSelectedEntitites();
  }

  switchToDocumentForm() {
    this.currentTab = TaskFormContent.DOCUMENTS;
    this.sourceForm = TaskFormContent.TASK;
  }

  switchToInstructionForm() {
    this.currentTab = TaskFormContent.WORK_INSTRUCTIONS;
    this.applyNewInstructionSelectedEntities();
    this.sourceForm = TaskFormContent.TASK;
  }

  updateSubTab(key: keyof SubTabs, value: number) {
    this.subTabs[key] = value;
    this.isAddForm = !value;
  }
}
