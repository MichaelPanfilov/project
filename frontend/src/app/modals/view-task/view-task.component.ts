import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  AssetDto,
  AssetType,
  BaseAssetTree,
  FileDto,
  getClassificationLabel,
  InstructionDto,
  TaskDto,
  TaskStatus,
} from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { unitOfTime } from 'moment';
import { combineLatest, Observable, of } from 'rxjs';
import { filter, map, startWith, take } from 'rxjs/operators';
import { Asset, Document, Instruction, State, Task } from 'src/app/core/store';
import { AssetSelectItem, isTaskDue, ModalModes } from 'src/app/util';

import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';
import { TaskModalComponent } from '../task/task-modal.component';

@Component({
  selector: 'syn-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.scss'],
})
export class ViewTaskComponent implements OnInit, AfterViewInit {
  disableAnimation = true;

  assetTrees$: Observable<AssetSelectItem[]> = this.store.select(Asset.selectAllAssetTrees).pipe(
    startWith([]),
    map((tree: BaseAssetTree[]) => tree.map(asset => this.mapToItem(asset))),
  );

  selectedAsset?: AssetDto;

  attachments$: Observable<(FileDto | InstructionDto)[]> = of([]);

  get timeLeft(): { diff: number; unit: unitOfTime.DurationConstructor } {
    const dueDate = moment(this.data.task.dueDate as Date);

    const calculateDiff = (u: unitOfTime.DurationConstructor) => dueDate.diff(moment(), u);

    let unit: unitOfTime.DurationConstructor = 'minutes';

    if (this.isTaskInactive || dueDate.isBefore()) {
      return { diff: 0, unit };
    }

    let diff = calculateDiff(unit);
    const hour = 60;
    if (diff > hour) {
      const day = 24 * hour;
      // pluralize
      if (diff > day) {
        unit = diff > 2 * day ? 'days' : 'day';
      } else {
        unit = diff > 2 * hour ? 'hours' : 'hour';
      }

      diff = calculateDiff(unit);
    }

    return {
      diff,
      unit,
    };
  }

  get state() {
    if (this.isTaskInactive) {
      return 'neutral';
    }

    if (this.isTaskDone) {
      return 'primary';
    }

    return isTaskDue(this.data.task) ? 'danger' : 'primary';
  }

  get isTaskInactive() {
    return this.data.task.status === TaskStatus.INACTIVE;
  }

  get isTaskActive() {
    return this.data.task.status === TaskStatus.ACTIVE;
  }

  get isTaskDone() {
    return this.data.task.status === TaskStatus.DONE;
  }

  get isTaskNotDone() {
    return this.data.task.status !== TaskStatus.DONE;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { task: TaskDto },
    private store: Store<State>,
    private actions$: Actions,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<ViewTaskComponent>,
  ) {}

  ngOnInit() {
    const assetId = this.data.task.assignedAsset;
    if (assetId) {
      this.store
        .select(Asset.selectAsset(assetId))
        .pipe(take(1))
        .subscribe(asset => (this.selectedAsset = asset));
    }
    const id = this.data.task.id;
    if (id) {
      this.store.dispatch(Document.loadDocumentsByRefIds([id]));
      this.store.dispatch(Instruction.loadInstructionsByRefIds([id]));

      this.attachments$ = combineLatest([
        this.store.select(Document.selectDocumentsByRefId(id)),
        this.store.select(Instruction.selectInstructionsByRefId(id)),
      ]).pipe(map(([docs, instructions]) => [...docs, ...instructions]));
    }
  }

  // Workaround for angular component issue https://github.com/angular/components/issues/13870
  // for reference https://stackblitz.com/edit/angular-issue13870-workaround
  ngAfterViewInit(): void {
    // timeout required to avoid the dreaded 'ExpressionChangedAfterItHasBeenCheckedError'
    setTimeout(() => (this.disableAnimation = false));
  }

  onEditTask() {
    this.dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe(() =>
        this.dialog.open(TaskModalComponent, {
          disableClose: true,
          data: { mode: ModalModes.EDIT, task: this.data.task },
          panelClass: ['task-modal', 'fullscreen-modal', 'edit-modal'],
        }),
      );

    this.dialogRef.close();
  }

  onConfirm() {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      data: {
        title: 'COMMON.ACTION.CONFIRM_ACTION',
        message: 'TASK.ACTION.TASK_CARRIED_OUT_QUESTION',
        actionName: 'COMMON.ACTION.YES',
        buttonClasses: {
          confirm: 'primary',
          cancel: 'danger',
        },
      },
    });
    dialogRef
      .afterClosed()
      .pipe(
        take(1),
        filter(res => !!res),
      )
      .subscribe(() => {
        const taskId = this.data.task.id;
        this.store.dispatch(Task.markDoneTask({ taskId }));
        this.actions$
          .pipe(ofType(Task.markedDoneTask), take(1))
          .subscribe(() => this.dialogRef.close());
      });
  }

  onAssignAsset() {
    if (this.selectedAsset) {
      const params = {
        taskId: this.data.task.id,
        assetId: this.selectedAsset.id,
      };

      this.store.dispatch(Task.assignToAsset(params));
      this.actions$
        .pipe(ofType(Task.assignedToAsset), take(1))
        .subscribe(() => this.dialogRef.close());
    }
  }

  getLinkedAsset(): Observable<string> {
    const assetId = this.data.task.assignedAsset;
    if (assetId) {
      return this.store
        .select(Asset.selectAsset(assetId))
        .pipe(map(asset => (asset ? asset.name : '')));
    }
    return of(this.selectedAsset ? this.selectedAsset.name : '');
  }

  openAttachment(attachment: FileDto | InstructionDto): void {
    const url = this.isDoc(attachment) ? attachment.viewUrl : attachment.url;
    window.open(url, '_blank');
  }

  isDoc(attachment: FileDto | InstructionDto): attachment is FileDto {
    return 'viewUrl' in attachment;
  }

  getClassificationLabel(value: string): string {
    return getClassificationLabel(value);
  }

  private mapToItem(tree: BaseAssetTree): AssetSelectItem {
    return {
      ...tree,
      children: (tree.children || []).reduce(
        (prev, curr) => prev.concat(this.mapToItem(curr)),
        [] as AssetSelectItem[],
      ),
      // Tasks cannot be assigned to factories.
      disabled: tree.type === AssetType.FACTORY,
    };
  }
}
