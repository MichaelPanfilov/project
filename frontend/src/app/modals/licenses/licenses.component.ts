import { Component, OnInit } from '@angular/core';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LicensesService } from 'src/app/core/services/api/licenses.service';
import { Package } from 'src/app/util';

@Component({
  selector: 'syn-licenses',
  templateUrl: './licenses.component.html',
  styleUrls: ['./licenses.component.scss'],
})
export class LicensesComponent implements OnInit {
  frontendPackages: Package = {};
  backendPackages: Package = {};
  fileServicePackages: Package = {};
  aclServicePackages: Package = {};
  datapointServicePackages: Package = {};

  constructor(private licensesService: LicensesService) {}

  ngOnInit() {
    forkJoin([
      this.licensesService.getLicenses('frontend').pipe(catchError(() => of({}))),
      this.licensesService.getLicenses('backendService').pipe(catchError(() => of({}))),
      this.licensesService.getLicenses('fileService').pipe(catchError(() => of({}))),
      this.licensesService.getLicenses('aclService').pipe(catchError(() => of({}))),
      this.licensesService.getLicenses('datapointService').pipe(catchError(() => of({}))),
    ]).subscribe(([frontend, backend, file, acl, dp]) => {
      this.frontendPackages = frontend;
      this.fileServicePackages = file;
      this.aclServicePackages = acl;
      this.backendPackages = backend;
      this.datapointServicePackages = dp;
    });
  }
}
