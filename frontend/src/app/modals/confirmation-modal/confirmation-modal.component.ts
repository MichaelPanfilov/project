import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'syn-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss'],
})
export class ConfirmationModalComponent<T extends object | string> {
  buttonClasses = {
    confirm: 'neutral',
    cancel: 'danger',
  };
  param!: T | { value: string };

  constructor(
    public dialogRef: MatDialogRef<ConfirmationModalComponent<T>>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmationModalData<T>,
  ) {
    if (data.buttonClasses) {
      this.buttonClasses = data.buttonClasses;
    }
    const { value } = this.data;
    if (value) {
      this.param = typeof value === 'string' ? { value } : value;
    }
  }
}

export interface ConfirmationModalData<T extends object | string> {
  message: string;
  actionName: string;
  title?: string;
  value?: T;
  buttonClasses?: {
    confirm: string;
    cancel: string;
  };
}
