import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppUser } from '@common';
import { Store } from '@ngrx/store';
import { Image, State } from 'src/app/core/store';
import { UserTableActions } from 'src/app/util';

@Component({
  selector: 'syn-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss'],
})
export class ViewUserComponent {
  placeholder = '../../../assets/imgs/avatar-placeholder.png';

  image$ = this.store.select(Image.selectImageByRefId(this.data.user.id));

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { user: AppUser },
    private dialogRef: MatDialogRef<ViewUserComponent>,
    private store: Store<State>,
  ) {}

  onEditUser() {
    this.dialogRef.close(UserTableActions.EDIT);
  }
}
