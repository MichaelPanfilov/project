import {
  AppUser,
  FileDto,
  FilesFilter,
  InstructionDto,
  PaginationParams,
  ParsedFilter,
  Sorting,
  TaskDto,
} from '@common';
import { Store } from '@ngrx/store';

import { BaseDataSource } from '../core/abstract/data-source';
import { Document, Instruction, Settings, State, Task, User } from '../core/store';

export class TaskDataSource extends BaseDataSource<TaskDto> {
  loading$ = this.store.select(Settings.selectLoadingByAction(Task.loadTaskTable));

  constructor(protected store: Store<State>, public tableId: string) {
    super(
      store.select(Task.selectTableItems(tableId)),
      store.select(Task.selectTableMeta(tableId)),
    );
  }

  loadTasks(filter?: ParsedFilter<TaskDto>, sorting?: Sorting) {
    this.sorting = sorting || this.sorting;
    this.store.dispatch(
      Task.loadTaskTable({
        filter,
        tableId: this.tableId,
        paging: this.pageParams,
        sorting: this.sorting,
      }),
    );
  }

  loadPage(paging: PaginationParams, sorting?: Sorting) {
    this.sorting = sorting || this.sorting;
    this.store.dispatch(Task.loadTaskTable({ tableId: this.tableId, paging, sorting }));
  }

  disconnect() {
    this.store.dispatch(Task.removeTaskTable({ tableId: this.tableId }));
    super.disconnect();
  }
}

export class InstructionDataSource extends BaseDataSource<InstructionDto> {
  loading$ = this.store.select(Settings.selectLoadingByAction(Instruction.loadInstructionTable));

  constructor(protected store: Store<State>, public tableId: string) {
    super(
      store.select(Instruction.selectTableItems(tableId)),
      store.select(Instruction.selectTableMeta(tableId)),
    );
  }

  loadInstructions(filter?: ParsedFilter<InstructionDto>, sorting?: Sorting) {
    this.sorting = sorting || this.sorting;
    this.store.dispatch(
      Instruction.loadInstructionTable({
        tableId: this.tableId,
        filter,
        paging: this.pageParams,
        sorting: this.sorting,
      }),
    );
  }

  loadPage(paging: PaginationParams, sorting?: Sorting) {
    this.sorting = sorting || this.sorting;
    this.store.dispatch(
      Instruction.loadInstructionTable({ tableId: this.tableId, paging, sorting }),
    );
  }

  disconnect() {
    this.store.dispatch(Instruction.removeInstructionTable({ tableId: this.tableId }));
    super.disconnect();
  }
}

export class DocumentDataSource extends BaseDataSource<FileDto> {
  private lastFilter: FilesFilter = {};
  loading$ = this.store.select(Settings.selectLoadingByAction(Document.loadDocumentTable));

  constructor(protected store: Store<State>, public tableId: string) {
    super(
      store.select(Document.selectTableItems(tableId)),
      store.select(Document.selectTableMeta(tableId)),
    );
  }

  loadDocuments(filter?: FilesFilter, sorting?: Sorting) {
    this.sorting = sorting || this.sorting;
    this.lastFilter = filter || this.lastFilter;
    this.store.dispatch(
      Document.loadDocumentTable({
        tableId: this.tableId,
        filter: { ...filter, ...this.pageParams },
        sorting: this.sorting,
      }),
    );
  }

  loadPage(params: PaginationParams, sorting?: Sorting) {
    this.sorting = sorting || this.sorting;
    this.store.dispatch(
      Document.loadDocumentTable({
        tableId: this.tableId,
        sorting,
        filter: { ...this.lastFilter, ...params },
      }),
    );
  }

  disconnect() {
    this.store.dispatch(Document.removeDocumentTable({ tableId: this.tableId }));
    super.disconnect();
  }
}

export class UserDataSource extends BaseDataSource<AppUser> {
  loading$ = this.store.select(Settings.selectLoadingByAction(User.loadUserTable));

  constructor(protected store: Store<State>, public tableId: string) {
    super(
      store.select(User.selectTableItems(tableId)),
      store.select(User.selectTableMeta(tableId)),
    );
  }

  loadUsers(filter?: ParsedFilter<AppUser>, sorting?: Sorting) {
    this.sorting = sorting || this.sorting;
    this.store.dispatch(
      User.loadUserTable({
        tableId: this.tableId,
        filter,
        paging: this.pageParams,
        sorting: this.sorting,
      }),
    );
  }

  loadPage(paging: PaginationParams, sorting?: Sorting) {
    this.sorting = sorting || this.sorting;
    this.store.dispatch(User.loadUserTable({ tableId: this.tableId, paging, sorting }));
  }

  disconnect() {
    this.store.dispatch(User.removeUserTable({ tableId: this.tableId }));
    super.disconnect();
  }
}
