import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { switchMap } from 'rxjs/operators';

export class CustomMatPaginatorIntl extends MatPaginatorIntl {
  constructor(private translate: TranslateService) {
    super();
    this.translate.onLangChange
      .pipe(
        switchMap(() =>
          forkJoin([
            this.translate.get('COMMON.TABLE.ITEMS_PER_PAGE'),
            this.translate.get('COMMON.TABLE.NEXT_PAGE'),
            this.translate.get('COMMON.TABLE.PREV_PAGE'),
            this.translate.get('COMMON.TABLE.FIRST_PAGE'),
            this.translate.get('COMMON.TABLE.LAST_PAGE'),
          ]),
        ),
      )
      .subscribe(([a, b, c, d, e]) => {
        this.itemsPerPageLabel = a;
        this.nextPageLabel = b;
        this.previousPageLabel = c;
        this.firstPageLabel = d;
        this.lastPageLabel = e;
        this.changes.next();
      });
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `${this.translate.instant('COMMON.TABLE.ZERO_OF')} ${length}`;
    }

    length = Math.max(length, 0);

    const startIndex = page * pageSize;
    const endIndex =
      startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

    return `${startIndex + 1} – ${endIndex} ${this.translate.instant('COMMON.TABLE.OF')} ${length}`;
  };
}
