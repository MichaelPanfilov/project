import { BaseDto, DOC_TYPES, ParsedFilter, TaskDto, TaskStatus } from '@common';
import { CondOperator } from '@nestjsx/crud-request';
import * as moment from 'moment';
import { Observable, of, race } from 'rxjs';
import { delay, distinctUntilChanged, map, startWith, switchMap } from 'rxjs/operators';

import { DOCUMENT_GROUP_TRANSLATION, Filter, FilterOption } from './types';

export const ACTIVE_TASK_STATES = [TaskStatus.ACTIVE, TaskStatus.INACTIVE];
export const ARCHIVED_TASK_STATES = [TaskStatus.DONE, TaskStatus.CLOSED];

export function isTaskDue(task: TaskDto): boolean {
  return !!task.dueDate && moment(task.dueDate).isBefore();
}

export function isTaskOverdue(task: TaskDto) {
  return task.dueDate && moment(task.doneDate).isAfter(task.dueDate);
}

export function isTaskActive(task: TaskDto): boolean {
  return ACTIVE_TASK_STATES.includes(task.status);
}

export function isTaskArchived(task: TaskDto): boolean {
  return ARCHIVED_TASK_STATES.includes(task.status);
}

export function getDeleteActionName(entity: string): string {
  return `${entity.toUpperCase().replace(/ /g, '_')}.CONFIRM_DELETE_ACTION`;
}

type FOr = FilterOption[] | BaseDto[] | string[] | string;
type FAnd = FilterOption & BaseDto & string;

export function parseFilter<T extends object>(filter: Filter<T>): ParsedFilter<T> {
  return Object.entries(filter)
    .filter(([, value]) => !!(value as FOr).length)
    .reduce(
      (prev, [key, value]) => ({
        ...prev,
        [key]: {
          value:
            typeof value === 'string' ? value : (value as FAnd[]).map(v => v.value || v.id || v),
          operator: typeof value === 'string' ? CondOperator.CONTAINS : CondOperator.IN,
        },
      }),
      {},
    );
}

export function wrapToLoadedObservable(
  loading$: Observable<boolean | boolean[]>,
  delayMs = 333,
): Observable<boolean> {
  return race(
    loading$.pipe(
      map(res => (Array.isArray(res) ? !res.includes(true) : res)),
      switchMap(loading => (loading ? of(loading).pipe(delay(delayMs)) : of(loading))),
      map(loading => !loading),
      distinctUntilChanged(),
    ),
    of(false).pipe(delay(delayMs)),
  ).pipe(startWith(true));
}

export function getDocTypeDisplayName(doc: { contentType: string }): string {
  const type = Object.keys(DOC_TYPES).find(k => DOC_TYPES[k].includes(doc.contentType));
  return type ? DOCUMENT_GROUP_TRANSLATION[type] : 'DOCUMENT.INFO.OTHER';
}

export function chunkArray<T>(arr: T[], size: number): T[][] {
  const arrays = [] as T[][];

  while (arr.length > 0) {
    arrays.push(arr.splice(0, size));
  }

  return arrays;
}
