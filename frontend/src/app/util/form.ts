import { AsyncValidatorFn, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import {
  getExtension,
  isFileSupported,
  isImage,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
  USERNAME_MAX_LENGTH,
  USERNAME_MIN_LENGTH,
  validateEmail,
  validatePassword,
  validateUsername,
} from '@common';
import { cloneDeep } from 'lodash';

export type FormConfig<T extends object | string> = Record<
  T extends object ? keyof T : T,
  [
    (
      | { value: string | number; disabled: boolean }
      | { value: string | number; disabled: boolean }[]
      | string
      | string[]
      | number
      | number[]
      | boolean
      | boolean[]
      | undefined
    ),
    (ValidatorFn | ValidatorFn[])?,
    (AsyncValidatorFn | AsyncValidatorFn[])?,
  ]
>;

export function buildForm<T extends object>(
  fb: FormBuilder,
  config: FormConfig<T>,
  obj?: Partial<T>,
): FormGroup {
  const form = cloneDeep(config);

  if (obj) {
    Object.keys(obj).forEach(key => {
      if (form.hasOwnProperty(key)) {
        const value = form[key];
        if (Array.isArray(value)) {
          // Preserve the validators.
          if (value[0] && value[0].disabled) {
            form[key] = [{ value: obj[key], disabled: true }, ...value.splice(1)];
          } else {
            form[key] = [obj[key], ...value.splice(1)];
          }
        } else {
          form[key] = [obj[key]];
        }
      }
    });
  }

  return fb.group(form);
}

export const DEFAULT_VALIDATORS = [Validators.required, Validators.minLength(3)];

export const URL_VALIDATOR = Validators.pattern(
  '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?',
);

export const EMAIL_VALIDATOR: ValidatorFn = control => {
  if (control.value && !validateEmail(control.value)) {
    return { email: true };
  }
  return null;
};

const username: ValidatorFn = control => {
  if (control.value && !validateUsername(control.value)) {
    return { username: true };
  }
  return null;
};

export const USERNAME_VALIDATOR = Validators.compose([
  Validators.required,
  Validators.minLength(USERNAME_MIN_LENGTH),
  Validators.maxLength(USERNAME_MAX_LENGTH),
  username,
]) as ValidatorFn;

const password: ValidatorFn = control => {
  if (control.value && !validatePassword(control.value)) {
    return { password: true };
  }
  return null;
};

export const PASSWORD_VALIDATOR = Validators.compose([
  Validators.required,
  Validators.minLength(PASSWORD_MIN_LENGTH),
  Validators.maxLength(PASSWORD_MAX_LENGTH),
  password,
]) as ValidatorFn;

function validateFile(extCheckFn: (ext: string) => boolean): ValidatorFn {
  return control => {
    const file = control.value;
    if (file instanceof File) {
      // Max file size of service ~50Mb.
      if (file.size > 51200000) {
        return { fileSize: 'Max file size exceeded' };
      }
      const ext = getExtension(file.name);
      return extCheckFn(ext) ? null : { fileType: 'Unsupported file type' };
    }
    return null;
  };
}

export const FILE_VALIDATOR: ValidatorFn = validateFile(isFileSupported);

export const IMG_VALIDATOR: ValidatorFn = validateFile(isImage);

export const CSV_VALIDATOR: ValidatorFn = validateFile(ext => ext.toLowerCase() === 'csv');
