import { SynMachineState } from '@common';

export const SYN_COLORS = {
  primary: '#00be85',
  primaryLight: '#5bf2b2',
  primaryDark: '#008d55',

  secondary: '#28313e',
  secondaryLight: '#515a69',
  secondaryDark: '#000818',

  danger: '#ff2b52',
  textLight: '#c4c4c4',
  warning: '#f7b73a',

  pending: '#007eff',
};

export const STATUS_COLORS: { [key in SynMachineState]: string } = {
  Running: SYN_COLORS.primary,
  Stopped: SYN_COLORS.warning,
  Faulted: SYN_COLORS.danger,
  Offline: '#c7c9cc',
  Blocked: SYN_COLORS.secondaryLight,
  Starved: SYN_COLORS.secondaryDark,
  Unknown: SYN_COLORS.textLight,
};
