// Type that deeply transforms Date objects to string, like JSON.stringify() would do.
// Use this to gain additional type safty for server responses.
/*
export declare type Stringified<T> = {
  [P in keyof T]: T[P] extends Array<infer U>
    ? Array<Stringified<U extends Date ? string : U>>
    : T[P] extends ReadonlyArray<infer U>
    ? ReadonlyArray<Stringified<U extends Date ? string : U>>
    : Stringified<T[P] extends Date ? string : T[P]>;
};
*/

import {
  Acl,
  AssetDto,
  BaseAssetTree,
  BaseDto,
  ContentTypeGroup,
  FilesFilter,
  InstructionDto,
  TaskDto,
  toTranslatable,
} from '@common';

export type Stringified<T> = {
  [P in keyof T]: T[P] extends (infer U)[] ? StringifiedDate<U>[] : StringifiedDate<T[P]>;
};

declare type StringifiedDate<T> = T extends Date
  ? undefined extends T
    ? string | undefined
    : string
  : undefined extends T
  ? T | undefined
  : T;

export type AssetWithParent<T extends AssetDto> = T & { parent: string };

export interface ServicesUrls {
  fileService: string;
  backendService: string;
  datapointService: string;
  aclService: string;
  frontend: string;
}

export enum ModalModes {
  EDIT = 'edit',
  CREATE = 'create',
}

export interface ActionOption<T extends string> {
  label: string;
  value: T;
  acl?: Acl;
}

export interface FilterOption<T = string> {
  value: T;
  label: string;
}

export type Filter<T extends object> = {
  [key in keyof T]?: FilterOption[] | BaseDto[] | string[] | string;
};

export interface FilterConfig {
  options: FilterOption[];
  placeholder: string;
  multiple?: boolean;
}

export type TaskFilter = Filter<TaskDto>;
export type InstructionFilter = Filter<Omit<InstructionDto, 'links'>> & { refIds?: string[] };
export type DocumentFilter = FilesFilter;

export enum AssetTreeElementActions {
  EDIT = 'edit',
  DELETE = 'delete',
}

export enum UserTableActions {
  EDIT = 'edit',
  DELETE = 'delete',
}

export type AssetSelectItem = BaseAssetTree & { disabled?: boolean; childrenCount?: number };

export const DATE_FORMATS = {
  fullPickerInput: {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    hour12: false,
  },
  datePickerInput: { year: 'numeric', month: 'numeric', day: 'numeric', hour12: false },
  timePickerInput: { hour: 'numeric', minute: 'numeric', hour12: false },
  monthYearLabel: { year: 'numeric', month: 'short' },
  dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
  monthYearA11yLabel: { year: 'numeric', month: 'long' },
};

export const DOCUMENT_GROUP_TRANSLATION: { [key in ContentTypeGroup]: string } = {
  [ContentTypeGroup.IMAGE]: 'DOCUMENT.INFO.IMAGE',
  [ContentTypeGroup.VIDEO]: 'DOCUMENT.INFO.VIDEO',
  [ContentTypeGroup.PDF]: 'DOCUMENT.INFO.PDF',
  [ContentTypeGroup.OTHER]: 'DOCUMENT.INFO.OTHER',
};

export const DOC_TYPE_OPTIONS = toTranslatable(DOCUMENT_GROUP_TRANSLATION);

// Order of fields should be same as order of tabs in task modal
export enum TaskFormContent {
  TASK,
  WORK_INSTRUCTIONS,
  DOCUMENTS,
  ASSETS,
  MACHINE_TAGS,
  TASKS_LINK,
}

export enum WorkInstructionFormContent {
  WORK_INSTRUCTION,
  MACHINE_TAGS,
  ASSETS,
  TASKS,
}

export enum DocumentFormContent {
  DOCUMENT,
  MACHINE_TAGS,
  ASSETS,
}

export interface Package {
  [key: string]: {
    licenseFile: string;
    licenses: string;
    path: string;
    publisher: string;
    repository: string;
  };
}

export enum AssetTreeVariants {
  MODAL = 'modal',
  PAGE = 'page',
}
