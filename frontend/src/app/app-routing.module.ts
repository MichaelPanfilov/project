import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard, NonAuthGuard } from 'src/app/core/guards';
import { EnhancedRoutes } from 'src/app/core/navigation';
import { AssetResolver } from 'src/app/core/resolvers';
import {
  AssetsComponent,
  DocumentManagementComponent,
  LineDetailComponent,
  LinesComponent,
  LoginComponent,
  MachineMetricComponent,
  MachinePerformanceComponent,
  MachineTimeProducingComponent,
  TasksComponent,
  UsersComponent,
} from 'src/app/pages';

import { AssetPageContainerComponent, PageContainerComponent } from './components';
import { WorkInstructionsComponent } from './pages/work-instructions/work-instructions.component';

export const ROUTES: EnhancedRoutes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'lines',
    data: {
      nav: {
        title: 'NAVIGATION.LABEL.HOME',
        path: '',
      },
    },
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [NonAuthGuard],
    data: {
      nav: {
        title: 'NAVIGATION.LABEL.LOGIN',
        path: 'login',
      },
    },
  },
  {
    path: 'lines',
    component: AssetPageContainerComponent,
    canActivate: [AuthGuard],
    data: {
      nav: {
        title: 'NAVIGATION.LABEL.LINES_OVERVIEW',
        path: 'lines',
      },
    },
    children: [
      {
        path: '',
        component: LinesComponent,
        data: {
          nav: {
            path: '',
            title: 'NAVIGATION.LABEL.LINE_OVERVIEW',
            navBarIgnore: true,
          },
        },
      },
      {
        path: ':lineId',
        component: LineDetailComponent,
        resolve: {
          line: AssetResolver,
        },
        data: {
          nav: {
            path: '',
            dynamicParam: 'lineId',
            title: 'NAVIGATION.LABEL.LINE_PERFORMANCE',
            param: 'line',
          },
        },
      },
    ],
  },
  {
    path: 'tasks',
    component: PageContainerComponent,
    canActivate: [AuthGuard],
    data: {
      nav: {
        path: 'tasks',
        title: 'NAVIGATION.LABEL.TASK_MANAGEMENT',
        subPages: [
          {
            path: 'active',
            title: 'NAVIGATION.TITLE.ALL_TASKS',
          },
          {
            path: 'archive',
            title: 'NAVIGATION.TITLE.ARCHIVE',
          },
        ],
      },
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'active',
        data: {
          nav: {
            path: '',
            title: 'NAVIGATION.LABEL.TASK_MANAGEMENT',
          },
        },
      },
      {
        path: 'active',
        component: TasksComponent,
        data: {
          nav: {
            path: 'active',
            title: 'NAVIGATION.TITLE.ALL_TASKS',
            navBarIgnore: true,
          },
          mode: 'all',
        },
      },
      {
        path: 'archive',
        component: TasksComponent,
        data: {
          nav: {
            path: 'archive',
            title: 'NAVIGATION.TITLE.ARCHIVE',
            navBarIgnore: true,
          },
          mode: 'archive',
        },
      },
    ],
  },
  {
    // TODO: Redirect to lines if no id is provided (use resolver for this)
    path: 'machines',
    component: AssetPageContainerComponent,
    canActivate: [AuthGuard],
    data: {
      nav: {
        path: 'machines',
        title: 'NAVIGATION.LABEL.MACHINES_OVERVIEW',
      },
    },
    children: [
      {
        path: '',
        redirectTo: '/lines',
        pathMatch: 'full',
        data: {
          nav: {
            path: 'machines',
            title: 'NAVIGATION.LABEL.MACHINES_OVERVIEW',
          },
        },
      },
      {
        path: ':machineId',
        component: MachinePerformanceComponent,
        resolve: {
          machine: AssetResolver,
        },
        data: {
          nav: {
            path: '',
            dynamicParam: 'machineId',
            title: 'NAVIGATION.LABEL.MACHINE_PERFORMANCE',
            param: 'machine',
          },
        },
      },
      {
        path: ':machineId/time-producing',
        component: MachineTimeProducingComponent,
        resolve: {
          machine: AssetResolver,
        },
        data: {
          nav: {
            dynamicParam: 'machineId',
            path: 'time-producing',
            title: 'NAVIGATION.LABEL.MACHINE_TIME_PRODUCING',
            param: 'machine',
          },
        },
      },
      {
        path: ':machineId/product-losses',
        component: MachineMetricComponent,
        resolve: {
          machine: AssetResolver,
        },
        data: {
          nav: {
            dynamicParam: 'machineId',
            path: 'product-losses',
            title: 'NAVIGATION.LABEL.MACHINE_PRODUCT_LOSS',
            param: 'machine',
          },
          metrics: {
            type: 'product-losses',
            summaryTitle: 'ASSET.LABEL.PRODUCT_LOSSES',
            summaryTitleBrackets: 'ASSET.LABEL.ALL_LOSSES',
            chartTitle: 'ASSET.LABEL.PRODUCT_LOSS_REASONS',
          },
        },
      },
      {
        path: ':machineId/fault-stops',
        resolve: {
          machine: AssetResolver,
        },
        data: {
          nav: {
            path: 'fault-stops',
            title: 'NAVIGATION.LABEL.MACHINE_FAULT_STOPS',
            subPages: [
              {
                path: 'durations',
                title: 'NAVIGATION.TITLE.DURATIONS',
              },
              {
                path: 'counts',
                title: 'NAVIGATION.TITLE.COUNTS',
              },
            ],
          },
        },
        children: [
          {
            path: '',
            redirectTo: 'durations',
            pathMatch: 'full',
            data: {
              nav: {
                path: 'fault-stops',
                title: 'NAVIGATION.LABEL.MACHINE_FAULT_STOPS',
              },
            },
          },
          {
            path: 'durations',
            component: MachineMetricComponent,
            data: {
              nav: {
                dynamicParam: 'machineId',
                path: 'durations',
                title: 'NAVIGATION.LABEL.MACHINE_FAULT_STOPS_DURATIONS',
                param: 'machine',
              },
              metrics: {
                type: 'fault-stops/durations',
                summaryTitle: 'ASSET.LABEL.TOTAL_FAULT_STOP_DURATION',
                summaryTitleBrackets: 'ASSET.LABEL.ALL_FAULTS',
                chartTitle: 'ASSET.LABEL.FAULT_STOP_DURATIONS',
              },
            },
          },
          {
            path: 'counts',
            component: MachineMetricComponent,
            data: {
              nav: {
                dynamicParam: 'machineId',
                path: 'counts',
                title: 'ASSET.LABEL.MACHINE_FAULT_STOPS_COUNTS',
                param: 'machine',
              },
              metrics: {
                type: 'fault-stops/counts',
                summaryTitle: 'ASSET.LABEL.TOTAL_FAULT_STOP_COUNTS',
                summaryTitleBrackets: 'ASSET.LABEL.ALL_FAULTS',
                chartTitle: 'ASSET.LABEL.FAULT_STOP_COUNTS',
              },
            },
          },
        ],
      },
    ],
  },
  {
    path: 'settings',
    component: PageContainerComponent,
    canActivate: [AuthGuard],
    data: {
      nav: {
        path: 'settings',
        title: 'NAVIGATION.LABEL.SETTINGS',
        tabsSubmenu: true,
        subPages: [
          {
            path: 'users',
            title: 'NAVIGATION.LABEL.USER_MANAGEMENT',
          },
          {
            path: 'assets',
            title: 'NAVIGATION.LABEL.ASSET_MANAGEMENT',
          },
        ],
      },
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'users',
        data: {
          nav: {
            path: '',
            title: 'NAVIGATION.LABEL.SETTINGS',
          },
        },
      },
      {
        path: 'users',
        data: {
          nav: {
            path: 'users',
            title: 'NAVIGATION.LABEL.USER_MANAGEMENT',
            navBarIgnore: true,
            tabsSubmenu: true,
          },
        },
        children: [
          {
            path: '',
            component: UsersComponent,
            data: {
              nav: {
                path: '',
                title: 'NAVIGATION.LABEL.USER_MANAGEMENT',
                navBarIgnore: true,
              },
            },
          },
        ],
      },
      {
        path: 'assets',
        data: {
          nav: {
            path: 'assets',
            title: 'NAVIGATION.LABEL.SETTINGS',
            tabsSubmenu: true,
          },
        },
        children: [
          {
            path: '',
            component: AssetsComponent,
            data: {
              nav: {
                path: '',
                title: 'NAVIGATION.LABEL.ASSET_MANAGEMENT',
                navBarIgnore: true,
              },
            },
          },
        ],
      },
    ],
  },
  {
    path: 'documents',
    component: PageContainerComponent,
    canActivate: [AuthGuard],
    data: {
      nav: {
        path: 'documents',
        title: 'NAVIGATION.LABEL.DOCUMENT_MANAGEMENT',
        subPages: [
          {
            path: '/documents',
            title: 'NAVIGATION.TITLE.ALL_DOCUMENTS',
          },
        ],
      },
    },
    children: [
      {
        path: '',
        component: DocumentManagementComponent,
        data: {
          nav: {
            path: '',
            title: 'COMMON.LABEL.OVERVIEW',
            navBarIgnore: true,
            submenuHidden: true,
          },
        },
      },
    ],
  },
  {
    path: 'instructions',
    component: PageContainerComponent,
    canActivate: [AuthGuard],
    data: {
      nav: {
        path: 'instructions',
        title: 'NAVIGATION.LABEL.WORK_INSTRUCTIONS',
        subPages: [
          {
            path: '/instructions',
            title: 'NAVIGATION.TITLE.ALL_WORK_INSTRUCTIONS',
          },
        ],
      },
    },
    children: [
      {
        path: '',
        component: WorkInstructionsComponent,
        data: {
          nav: {
            path: '',
            title: 'COMMON.LABEL.OVERVIEW',
            navBarIgnore: true,
            submenuHidden: true,
          },
        },
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES, {
      useHash: true,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
