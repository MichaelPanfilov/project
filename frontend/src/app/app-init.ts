import { AuthService } from './core/services';

export function initApp(authService: AuthService) {
  return () => authService.init();
}
