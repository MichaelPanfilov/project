import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AssetType, FactoryDto } from '@common';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AssetForm } from 'src/app/core/abstract';
import { State } from 'src/app/core/store';
import { FormConfig } from 'src/app/util';

@Component({
  selector: 'syn-factory-form',
  templateUrl: './factory-form.component.html',
  styleUrls: ['./factory-form.component.scss'],
})
export class FactoryFormComponent extends AssetForm<FactoryDto> {
  type = AssetType.FACTORY;

  formConfig = {
    ...this.assetFormConfig,
    postalCode: [''],
  } as FormConfig<FactoryDto>;

  constructor(fb: FormBuilder, store: Store<State>, actions$: Actions) {
    super(fb, store, actions$);
  }
}
