import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'syn-form-control',
  templateUrl: './form-control.component.html',
  styleUrls: ['./form-control.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormControlComponent {
  @Input()
  title = '';

  @Input()
  name = '';

  @Input()
  control!: FormControl;

  @Input()
  error?: string;

  @Input()
  optional?: boolean;

  @Input()
  hideErrors?: boolean;
}
