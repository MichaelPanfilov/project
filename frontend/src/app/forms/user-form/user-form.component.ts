import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import {
  AppUser,
  CreateUserDto,
  getLanguageLabel,
  Languages,
  LANGUAGE_LABELS,
  toTranslatable,
} from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { isEqual } from 'lodash';
import { forkJoin, Observable, of } from 'rxjs';
import { filter, map, mapTo, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { FormComponent } from 'src/app/core/abstract';
import { AuthService } from 'src/app/core/services';
import { State, User } from 'src/app/core/store';
import { EMAIL_VALIDATOR, FormConfig, PASSWORD_VALIDATOR, USERNAME_VALIDATOR } from 'src/app/util';

type UserForm = Omit<CreateUserDto, 'activated'> & {
  oldPassword: string;
  passwordRepeat: string;
  roleIds: string[];
};

const USER_FORM_CONFIG: FormConfig<UserForm> = {
  // TODO: must be unique, add async check for that.
  name: ['', USERNAME_VALIDATOR],
  email: ['', EMAIL_VALIDATOR],
  language: [''],
  password: [''],
  oldPassword: [''],
  passwordRepeat: [''],
  roleIds: [[], Validators.required],
};

@Component({
  selector: 'syn-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent extends FormComponent<UserForm, AppUser> implements OnInit {
  protected formConfig = USER_FORM_CONFIG;

  @Input()
  user?: AppUser;

  isMe = false;
  roles$ = this.store.select(User.selectRoles);
  readonly languages = toTranslatable(LANGUAGE_LABELS);

  get isCreate() {
    return !(this.user && this.user.id);
  }

  get isUserEdit() {
    return !this.isMe && !this.isCreate;
  }

  get showPassword() {
    return this.isMe || this.isCreate;
  }

  constructor(
    fb: FormBuilder,
    private store: Store<State>,
    private actions$: Actions,
    private authService: AuthService,
  ) {
    super(fb);
  }

  ngOnInit() {
    super.ngOnInit();

    this.store.dispatch(User.loadRoles({}));

    this.form.setValidators(passwordValidator as ValidatorFn);

    if (this.isCreate) {
      this.getControl('password').setValidators(PASSWORD_VALIDATOR);
    } else {
      // For now we disable name update.
      this.getControl('name').disable();
    }
    this.form.updateValueAndValidity();

    const passwordRepeat = this.getControl('passwordRepeat');
    passwordRepeat.disable();
    this.getControl('password')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((value?: string) =>
        value && value.length ? passwordRepeat.enable() : passwordRepeat.disable(),
      );

    this.store
      .select(User.selectMe)
      .pipe(
        filter(me => !!me),
        take(1),
      )
      .subscribe(me => {
        this.isMe = !!this.user && this.user.id === (me as AppUser).id;
        if (this.isMe && !this.isCreate) {
          const roleControl = this.getControl('roleIds');
          roleControl.disable();
        }
      });

    if (!this.authService.hasRight([{ resources: ['User'], right: 'Update' }])) {
      this.getControl('roleIds').disable();
    }

    // Update copy after disabling some controls.
    this.updateCopy();

    // Add error to username form input on failed creation of already existing username.
    this.actions$
      .pipe(takeUntil(this.destroy$), ofType(User.failedCreatingUser, User.failedUpdatingUser))
      .subscribe(({ error }) => {
        if (error instanceof HttpErrorResponse && error.status === 409) {
          this.handleConflictError(error);
        }
      });
  }

  provideFormObj(): UserForm {
    return {
      name: '',
      email: '',
      language: Languages.ENGLISH,
      password: '',
      oldPassword: '',
      passwordRepeat: '',
      roleIds: [],
      ...this.user,
    };
  }

  handleSubmit(payload: UserForm): Observable<AppUser> {
    const { oldPassword, passwordRepeat, roleIds, ...user } = payload;
    if (this.isCreate) {
      this.store.dispatch(User.createUser({ user }));

      return this.actions$.pipe(
        ofType(User.createdUser),
        take(1),
        map(p => p.user),
        tap(u => this.store.dispatch(User.updateRoles({ aclId: u.aclId, roleIds }))),
        switchMap(u => this.actions$.pipe(ofType(User.updatedRoles)).pipe(take(1), mapTo(u))),
      );
    } else {
      const { password, ...changes } = user;
      const { id, aclId } = this.user as AppUser;
      this.updateAcl(payload)
        .pipe(take(1))
        .subscribe(() => this.store.dispatch(User.updateUser({ userId: id, aclId, changes })));
      return this.actions$.pipe(
        ofType(User.updatedUser),
        take(1),
        map(p => p.user),
        switchMap(u => {
          // Reload me on update.
          if (this.isMe) {
            this.store.dispatch(User.loadMe({}));
            return this.actions$.pipe(ofType(User.loadedMe), mapTo(u));
          }
          return of(u);
        }),
      );
    }
  }

  hasPasswordError(): boolean {
    const control = this.getControl('passwordRepeat');
    if (control) {
      const errors = this.form.errors || {};
      return (control.dirty || control.touched) && !!errors['passwordRepeat'];
    }
    return false;
  }

  // TODO: Remove when multiple roles are allowed.
  compareWith = (a: string[], b: string[]) => isEqual(a.sort(), b.sort());

  getLanguageLabel(lang: Languages) {
    return getLanguageLabel(lang);
  }

  private updateAcl(payload: UserForm): Observable<unknown> {
    const user = this.user as AppUser;
    const obs$: Observable<unknown>[] = [];
    const { password, oldPassword, passwordRepeat, roleIds } = payload;
    const needPasswordUpdate = password && passwordRepeat && password === passwordRepeat;

    if (needPasswordUpdate) {
      if (oldPassword) {
        this.store.dispatch(User.changePassword({ oldPassword, newPassword: password }));
        obs$.push(this.actions$.pipe(ofType(User.changedPassword), take(1)));
      } else if (this.authService.hasRight([{ resources: ['User'], right: 'Update' }])) {
        this.store.dispatch(User.changeUserPassword({ userId: user.aclId, newPassword: password }));
        obs$.push(this.actions$.pipe(ofType(User.changedUserPassword), take(1)));
      }
    }

    if (roleIds && !this.compareWith(roleIds, user.roleIds)) {
      this.store.dispatch(User.updateRoles({ aclId: user.aclId, roleIds }));
      obs$.push(this.actions$.pipe(ofType(User.updatedRoles), take(1)));
    }

    return obs$.length ? forkJoin(obs$) : of(null);
  }

  private handleConflictError(err: HttpErrorResponse) {
    const message = (err.error.error
      ? (err.error.error.message as string) || ''
      : ''
    ).toLowerCase();
    if (message.includes('email')) {
      const emailCtrl = this.getControl('email');
      emailCtrl.setErrors({ ...emailCtrl.errors, duplicate: { name: 'Email' } });
    } else if (message.includes('user')) {
      const nameCtrl = this.getControl('name');
      nameCtrl.setErrors({ ...nameCtrl.errors, duplicate: { name: 'Username' } });
    }
  }
}

function passwordValidator(group: FormGroup): { [key in keyof UserForm]?: string } | null {
  const value: UserForm = group.value;
  const password = value.password;
  if (!password) {
    return null;
  }

  if (password !== value.passwordRepeat) {
    return { passwordRepeat: 'mismatch' };
  }

  return null;
}
