import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {
  AssetDto,
  AssetType,
  BaseAssetTree,
  CalendarTaskDto,
  CLASSIFICATIONS,
  CountdownTaskDto,
  CreateTaskDto,
  dueDateNeedsUpdate,
  FileDto,
  getRepeatTypeLabel,
  InstructionDto,
  INTERVAL_TYPE_LABELS,
  isCalendarTask,
  isCountdownTask,
  NowTaskDto,
  RepeatTypes,
  REPEAT_TYPE_LABELS,
  TaskDto,
  TaskStatus,
  toTranslatable,
  TriggerTypes,
  TRIGGER_TYPE_LABELS,
  UpdateTaskDto,
} from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { DeviceDetectorService } from 'ngx-device-detector';
import { combineLatest, concat, forkJoin, Observable, of } from 'rxjs';
import { filter, map, mapTo, startWith, switchMap, take, takeUntil } from 'rxjs/operators';
import { FormComponent } from 'src/app/core/abstract';
import { Asset, Document, Instruction, State, Task } from 'src/app/core/store';
import {
  ConfirmationModalComponent,
  ConfirmationModalData,
} from 'src/app/modals/confirmation-modal/confirmation-modal.component';
import { AssetSelectItem, DEFAULT_VALIDATORS, FormConfig, TaskFormContent } from 'src/app/util';

// TODO: extract into separate util file
export type TaskForm = Pick<
  NowTaskDto & CalendarTaskDto & CountdownTaskDto,
  | 'title'
  | 'classification'
  | 'machineTypes'
  | 'countdownHours'
  | 'calendarDate'
  | 'unitIntervals'
  | 'repeatInterval'
  | 'assignedAsset'
> & {
  triggerType: TriggerTypes;
  calendarTime: string | Date;
  docIds: string[];
  instructionIds: string[];
};

const TASK_FORM_CONFIG: FormConfig<TaskForm> = {
  title: ['', DEFAULT_VALIDATORS],
  classification: [undefined, [Validators.required]],
  machineTypes: [[]],
  triggerType: ['', [Validators.required]],
  countdownHours: [undefined],
  unitIntervals: [''],
  repeatInterval: [''],
  calendarDate: [undefined],
  calendarTime: [''],
  assignedAsset: [undefined],
  docIds: [[]],
  instructionIds: [[]],
};

@Component({
  selector: 'syn-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TaskFormComponent extends FormComponent<TaskForm, TaskDto> implements OnInit {
  protected formConfig = TASK_FORM_CONFIG;

  @Input()
  task?: Partial<TaskDto>;

  @Input()
  expanded?: boolean;

  readonly triggerTypes = TriggerTypes;
  readonly triggers = toTranslatable(TRIGGER_TYPE_LABELS);
  readonly intervals = toTranslatable(INTERVAL_TYPE_LABELS);
  readonly repetitions = toTranslatable(REPEAT_TYPE_LABELS);
  readonly classifications = CLASSIFICATIONS;

  assets$ = this.store
    .select(Asset.selectAllAssets)
    .pipe(map(assets => assets.filter(asset => asset.type !== AssetType.FACTORY)));
  asset$: Observable<AssetDto | undefined> = of(undefined);
  assetTrees$: Observable<AssetSelectItem[]> = this.store
    .select(Asset.selectAllAssetTrees)
    .pipe(map(trees => this.mapToSelectItems(trees)));

  documents$ = this.store.select(Document.selectDocuments);
  instructions$ = this.store.select(Instruction.selectInstructions);
  triggerType$!: Observable<TriggerTypes>;

  // Workaround for angular component issue https://github.com/angular/components/issues/13870
  @Input()
  disableAnimation?: boolean;

  @Output()
  openAssignment = new EventEmitter<TaskFormContent>();

  @Output()
  selectedInstructionsChange = new EventEmitter<InstructionDto[]>();

  get taskId() {
    return this.task && this.task.id;
  }

  get repeatInterval() {
    return this.getControl('repeatInterval').value as RepeatTypes;
  }

  get repeatIntervalLabel() {
    return getRepeatTypeLabel(this.repeatInterval);
  }

  get machineTypeTags() {
    return this.getControl('machineTypes').value as string[];
  }

  get docIds() {
    return this.getControl('docIds').value as string[];
  }

  get instructionIds() {
    return this.getControl('instructionIds').value as string[];
  }

  get assignedAsset() {
    return this.getControl('assignedAsset').value;
  }

  get isDesktop() {
    return this.deviceService.isDesktop();
  }

  constructor(
    fb: FormBuilder,
    private store: Store<State>,
    private actions$: Actions,
    private dialog: MatDialog,
    private deviceService: DeviceDetectorService,
  ) {
    super(fb);
  }

  protected provideFormObj(): Partial<TaskForm> {
    const calendarTime =
      this.task && isCalendarTask(this.task) ? this.task.calendarDate : undefined;
    const calendarDate =
      this.task && isCalendarTask(this.task) ? this.task.calendarDate : undefined;

    return {
      triggerType: TriggerTypes.NOW,
      docIds: [],
      instructionIds: [],
      machineTypes: [],
      repeatInterval: RepeatTypes.NEVER,
      ...this.task,
      calendarDate,
      calendarTime,
      // tslint:disable-next-line
    } as any;
  }

  ngOnInit() {
    super.ngOnInit();

    this.asset$ = combineLatest([
      this.assets$,
      this.getControl('assignedAsset').valueChanges.pipe(
        startWith(this.task ? this.task.assignedAsset : undefined),
      ) as Observable<string | undefined>,
    ]).pipe(map(([assets, assetId]) => (assetId ? assets.find(a => a.id === assetId) : undefined)));

    if (this.task) {
      this.initRefValues(this.task);
    }

    this.form.setValidators(triggerValidator as ValidatorFn);
    this.form.updateValueAndValidity();

    const initTrigger = this.triggers.find(
      trigger => this.task && this.task.triggerType === trigger.value,
    );

    this.triggerType$ = this.getControl('triggerType').valueChanges.pipe(
      startWith(initTrigger && initTrigger.value),
    );

    if (this.task && this.task.id) {
      this.store.dispatch(Instruction.loadInstructionsByRefIds([this.task.id]));
      this.store.dispatch(Document.loadDocumentsByRefIds([this.task.id]));
      if (initTrigger && initTrigger.value === TriggerTypes.CALENDAR && !this.isDesktop) {
        const taskValue = (this.task as CalendarTaskDto).calendarDate;
        const formattedDate = moment(taskValue).format('YYYY-MM-DD');
        const formattedTime = moment(taskValue).format('HH:mm');
        this.patchValue('calendarDate', formattedDate);
        this.patchValue('calendarTime', formattedTime);
        this.updateCopy();
      }
    }
  }

  hasTriggerError(prop: string): boolean {
    const control = this.form.controls[prop];
    if (control) {
      const errors = this.form.errors || {};
      return !!errors[prop];
    }
    return false;
  }

  onMachineTagsSwitch() {
    this.openAssignment.emit(TaskFormContent.MACHINE_TAGS);
  }

  onDocumentsSwitch() {
    this.openAssignment.emit(TaskFormContent.DOCUMENTS);
  }

  onInstructionsSwitch() {
    this.openAssignment.emit(TaskFormContent.WORK_INSTRUCTIONS);
  }

  handleSubmit(payload: TaskForm): Observable<TaskDto | undefined> {
    const {
      docIds,
      instructionIds,
      countdownHours,
      unitIntervals,
      calendarDate,
      calendarTime,
      repeatInterval,
      ...task
    } = payload;
    // TODO: Add real user
    const dto: CreateTaskDto = {
      ...task,
    };

    if (isCountdownTask(dto)) {
      dto.countdownHours = countdownHours;
      dto.unitIntervals = unitIntervals;
    }

    if (isCalendarTask(dto)) {
      dto.calendarDate = this.calcCalendarDate(calendarDate, calendarTime);
      dto.repeatInterval = repeatInterval;
    }

    let proceed$: Observable<boolean> = of(true);
    if (
      this.task &&
      this.taskId &&
      (this.isAssetUpdated(dto) || dueDateNeedsUpdate(this.task as TaskDto, dto))
    ) {
      proceed$ = this.getUpdateConfirmation(!!this.task.assignedAsset && !dto.assignedAsset);
    }

    return proceed$.pipe(
      switchMap(confirm => {
        if (!confirm) {
          return of(undefined);
        }
        return combineLatest([this.instructions$, this.documents$]).pipe(
          take(1),
          switchMap(([allInstructions, allDocuments]) => {
            const instructions = allInstructions.filter(instr => instructionIds.includes(instr.id));
            const documents = allDocuments.filter(doc => docIds.includes(doc.id));

            if (this.task && this.task.id) {
              return this.updateTask(
                { id: this.task.id, ...dto } as UpdateTaskDto,
                instructions,
                documents,
              );
            }
            return this.createTask(dto, instructions, documents);
          }),
        );
      }),
    );
  }

  setSelectedAsset(asset: AssetDto) {
    this.patchValue('assignedAsset', asset.id);
  }

  resetSelectedAsset() {
    this.patchValue('assignedAsset', null);
  }

  private initRefValues(task: Partial<TaskDto>) {
    this.assets$
      .pipe(
        takeUntil(this.destroy$),
        filter(assets => !!assets.length),
        take(1),
      )
      .subscribe(assets => {
        const asset = assets.find(a => task.assignedAsset === a.id);
        this.patchValue('assignedAsset', asset ? asset.id : undefined);
        this.updateCopy();
      });

    const taskId = task.id;
    if (taskId) {
      combineLatest([this.documents$, this.instructions$])
        .pipe(
          takeUntil(this.destroy$),
          filter(([docs, instructions]) => !!docs.length && !!instructions.length),
          take(1),
        )
        .subscribe(([docs, instructions]) => {
          const docIds = this.getDocsOfTask(docs, taskId).map(({ id }) => id);
          const instructionIds = this.getInstructionsOfTask(instructions, taskId).map(
            ({ id }) => id,
          );
          this.patchValue('docIds', docIds);
          this.patchValue('instructionIds', instructionIds);
          this.updateCopy();
        });
    }
  }

  private createTask(
    dto: CreateTaskDto,
    instructions: InstructionDto[],
    documents: FileDto[],
  ): Observable<TaskDto> {
    this.store.dispatch(Task.createTask({ task: dto }));

    return this.actions$.pipe(
      ofType(Task.createdTask),
      take(1),
      switchMap(({ task }) =>
        forkJoin([
          this.linkDocument(task, documents),
          this.linkInstructions(task, instructions),
        ]).pipe(mapTo(task)),
      ),
    );
  }

  private updateTask(
    dto: UpdateTaskDto,
    instructions: InstructionDto[],
    documents: FileDto[],
  ): Observable<TaskDto> {
    const taskId = (this.task as TaskDto).id;
    this.store.dispatch(Task.updateTask({ taskId, changes: dto }));

    return this.actions$.pipe(
      ofType(Task.updatedTask),
      take(1),
      switchMap(({ task }) =>
        combineLatest([this.documents$, this.instructions$]).pipe(
          take(1),
          switchMap(([allDocuments, allInstructions]) => {
            const unassignedDocs = allDocuments.filter(
              doc =>
                (doc.refIds || []).includes(taskId) && !documents.some(({ id }) => id === doc.id),
            );
            const unassignedInstructions = allInstructions.filter(
              instr =>
                instr.links.find(refId => taskId === refId) &&
                !instructions.some(({ id }) => id === instr.id),
            );

            return concat(
              forkJoin([
                this.unlinkDocument(task, unassignedDocs),
                this.unlinkInstructions(task, unassignedInstructions),
                of(null),
              ]),
              forkJoin([
                this.linkDocument(task, documents),
                this.linkInstructions(task, instructions),
                of(null),
              ]),
            ).pipe(mapTo(task));
          }),
        ),
      ),
    );
  }

  private linkInstructions(task: TaskDto, instructions: InstructionDto[]): Observable<void> {
    if (instructions.length) {
      this.store.dispatch(
        Instruction.linkToTask({
          instructions,
          taskId: task.id,
        }),
      );
      return this.actions$.pipe(
        ofType(Instruction.linkedToTask),
        take(1),
        map(() => {}),
      );
    }
    return of(null).pipe(map(() => {}));
  }

  private unlinkInstructions(task: TaskDto, instructions: InstructionDto[]): Observable<void> {
    if (instructions.length) {
      this.store.dispatch(
        Instruction.unlinkFromTask({
          instructions,
          taskId: task.id,
        }),
      );
      return this.actions$.pipe(
        ofType(Instruction.unlinkedFromTask),
        take(1),
        map(() => {}),
      );
    }
    return of(null).pipe(map(() => {}));
  }

  private linkDocument(task: TaskDto, documents: FileDto[]): Observable<void> {
    if (documents.length) {
      this.store.dispatch(Document.assignToTask({ documents, taskId: task.id }));
      return this.actions$.pipe(
        ofType(Document.assignedToTask),
        take(1),
        map(() => {}),
      );
    }
    return of(null).pipe(map(() => {}));
  }

  private unlinkDocument(task: TaskDto, documents: FileDto[]): Observable<void> {
    if (documents.length) {
      this.store.dispatch(Document.unassignFromTask({ documents, taskId: task.id }));
      return this.actions$.pipe(
        ofType(Document.unassignedFromTask),
        take(1),
        map(() => {}),
      );
    }
    return of(null).pipe(map(() => {}));
  }

  private mapToSelectItems(trees: BaseAssetTree[]): AssetSelectItem[] {
    const mappedItems = [] as AssetSelectItem[];
    for (const tree of trees) {
      mappedItems.push({
        ...tree,
        disabled: tree.type === AssetType.FACTORY,
        children: this.mapToSelectItems(tree.children || []),
      });
    }
    return mappedItems;
  }

  private getDocsOfTask(docs: FileDto[], taskId: string): FileDto[] {
    return docs.filter(({ refIds }) => (refIds || []).includes(taskId));
  }

  private getInstructionsOfTask(instructions: InstructionDto[], taskId: string): InstructionDto[] {
    return instructions.filter(({ links }) => !!links.find(refId => taskId === refId));
  }

  private calcCalendarDate(calendarDate: string | Date, calendarTime: string | Date): Date {
    if (this.isDesktop) {
      return moment(
        moment(calendarDate).format('DD/MM/YYYY') + ' ' + moment(calendarTime).format('HH:mm'),
        'DD/MM/YYYY HH:mm',
      ).toDate();
    }
    return new Date(`${calendarDate}T${calendarTime}`);
  }

  private isAssetUpdated(dto: CreateTaskDto): boolean {
    return !!(
      this.task &&
      this.task.id &&
      this.task.status !== TaskStatus.INACTIVE &&
      dto.assignedAsset !== this.task.assignedAsset
    );
  }

  private getUpdateConfirmation(isUnassign: boolean): Observable<boolean> {
    const data: ConfirmationModalData<string> = isUnassign
      ? {
          title: 'TASK.ACTION.UNASSIGN_QUESTION',
          message: 'TASK.ACTION.UNASSIGN_ASSET_QUESTION',
          actionName: 'TASK.ACTION.UNASSIGN',
        }
      : {
          title: 'TASK.ACTION.DUE_DATE_RECALCULATING',
          message: 'TASK.ACTION.UPDATE_TASK_QUESTION',
          actionName: 'COMMON.ACTION.UPDATE',
        };

    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      data,
      panelClass: ['confirm-modal'],
    });
    return dialogRef.afterClosed();
  }
}

function triggerValidator(group: FormGroup): { [key: string]: string } | null {
  if (!group.value.triggerType) {
    return null;
  }

  const value = group.value.triggerType;
  const controls = group.controls;
  const errorKeys = [] as (keyof TaskForm)[];
  switch (value) {
    case TriggerTypes.COUNTDOWN: {
      const keys: (keyof TaskForm)[] = ['unitIntervals', 'countdownHours'];
      errorKeys.push(...keys.filter(key => !controls[key].value));
      break;
    }
    case TriggerTypes.CALENDAR: {
      const keys: (keyof TaskForm)[] = ['calendarDate', 'calendarTime'];
      errorKeys.push(...keys.filter(key => !controls[key].value));
      break;
    }
    default:
      break;
  }
  return errorKeys.reduce((prev, curr) => {
    prev[curr] = 'required';
    return prev;
  }, {});
}
