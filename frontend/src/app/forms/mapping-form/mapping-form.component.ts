import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MappingDto, MappingType } from '@common';
import { Observable, throwError } from 'rxjs';
import { catchError, mapTo } from 'rxjs/operators';
import { FormComponent } from 'src/app/core/abstract';
import { DatapointService, NotificationService, NotificationVariants } from 'src/app/core/services';
import { CSV_VALIDATOR, FormConfig } from 'src/app/util';

export type MappingForm = Pick<MappingDto, 'type'> & {
  file: File;
};

const MAPPING_FORM_CONFIG: FormConfig<MappingForm> = {
  file: [undefined, [Validators.required, CSV_VALIDATOR]],
  type: ['', [Validators.required, Validators.minLength(3)]],
};

@Component({
  selector: 'syn-mapping-form',
  templateUrl: './mapping-form.component.html',
  styleUrls: ['./mapping-form.component.scss'],
})
export class MappingFormComponent extends FormComponent<MappingForm> {
  @Input()
  device!: string;

  formConfig = MAPPING_FORM_CONFIG;

  get fileName() {
    const file = this.getControl('file').value as File;
    if (file) {
      return file.name;
    }

    return 'MAPPING.ACTION.SELECT_FOR_UPLOAD';
  }

  get file() {
    return this.getControl('file').value;
  }

  types = [
    { label: 'MAPPING.FORM.REJECT_REASONS', value: MappingType.REJECT_REASONS },
    { label: 'MAPPING.FORM.STOP_REASONS', value: MappingType.STOP_REASONS },
  ];

  constructor(
    fb: FormBuilder,
    private datapointService: DatapointService,
    private notificationService: NotificationService,
  ) {
    super(fb);
  }

  protected provideFormObj(): Partial<MappingDto> | undefined {
    return {
      type: MappingType.REJECT_REASONS,
    };
  }

  protected handleSubmit(payload: MappingForm): Observable<MappingForm | undefined> {
    const { type, file } = payload;
    return this.datapointService.uploadMapping(this.device, type, file).pipe(
      mapTo(payload),
      catchError((err: HttpErrorResponse) => {
        if (err.error.statusCode === 422) {
          this.notificationService.notify({
            message: 'MAPPING.ERROR.FILE_FORMAT',
            variant: NotificationVariants.ERROR,
          });
          this.getControl('file').setErrors({ fileProcessing: true });
        }
        return throwError(err);
      }),
    );
  }
}
