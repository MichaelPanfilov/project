import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkInstructionConstructorComponent } from './work-instruction-constructor.component';

describe('WorkInstructionConstructorComponent', () => {
  let component: WorkInstructionConstructorComponent;
  let fixture: ComponentFixture<WorkInstructionConstructorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WorkInstructionConstructorComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkInstructionConstructorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
