import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CLASSIFICATIONS, getRefIdsOf, InstructionDto, ReferencedEntities } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, startWith, switchMap, take } from 'rxjs/operators';
import { FormComponent } from 'src/app/core/abstract/form';
import { Asset, Instruction, State, Task } from 'src/app/core/store';
import {
  chunkArray,
  DEFAULT_VALIDATORS,
  FormConfig,
  URL_VALIDATOR,
  WorkInstructionFormContent,
} from 'src/app/util';

export type InstructionForm = Pick<
  InstructionDto,
  'title' | 'url' | 'classification' | 'machineTypes'
> & {
  assetIds: string[];
  taskIds: string[];
};

const INSTRUCTION_FORM_CONFIG: FormConfig<InstructionForm> = {
  title: ['', DEFAULT_VALIDATORS],
  url: ['', [Validators.required, URL_VALIDATOR]],
  classification: [[], Validators.required],
  assetIds: [[]],
  taskIds: [[]],
  machineTypes: [[]],
};

@Component({
  selector: 'syn-work-instruction-form',
  templateUrl: './work-instruction-form.component.html',
  styleUrls: ['./work-instruction-form.component.scss'],
})
export class WorkInstructionFormComponent extends FormComponent<InstructionForm, InstructionDto>
  implements OnInit {
  protected formConfig = INSTRUCTION_FORM_CONFIG;

  @Input()
  instruction?: InstructionDto;

  @Input()
  refIds: string[] = [];

  // Workaround for angular component issue https://github.com/angular/components/issues/13870
  @Input()
  disableAnimation?: boolean;

  @Output()
  openAssignment = new EventEmitter<WorkInstructionFormContent>();

  readonly classifications = CLASSIFICATIONS;

  assets$: Observable<string[]> = of([]);
  tasks$: Observable<string[]> = of([]);

  expanded = false;

  get machineTypeTags() {
    return this.getControl('machineTypes').value as string[];
  }

  get assetIds() {
    return this.getControl('assetIds').value as string[];
  }

  get taskIds() {
    return this.getControl('taskIds').value as string[];
  }

  constructor(fb: FormBuilder, private store: Store<State>, private actions$: Actions) {
    super(fb);
  }

  protected provideFormObj(): InstructionForm {
    const refIds = this.instruction ? this.instruction.links.map(refId => refId) : [];
    refIds.push(...this.refIds);
    return {
      title: '',
      url: '',
      classification: '',
      assetIds: getRefIdsOf(ReferencedEntities.ASSET, refIds),
      taskIds: getRefIdsOf(ReferencedEntities.TASK, refIds),
      machineTypes: this.instruction ? this.instruction.machineTypes : [],
      ...this.instruction,
    };
  }

  ngOnInit() {
    super.ngOnInit();

    this.assets$ = this.getControl('assetIds').valueChanges.pipe(
      startWith(this.assetIds),
      switchMap((ids: string[]) =>
        this.store
          .select(Asset.selectAllAssets)
          .pipe(
            map(assets => assets.filter(asset => ids.includes(asset.id)).map(asset => asset.name)),
          ),
      ),
    );
    this.tasks$ = this.getControl('taskIds').valueChanges.pipe(
      startWith(this.taskIds),
      switchMap((ids: string[]) =>
        this.store
          .select(Task.selectTasks)
          .pipe(map(tasks => tasks.filter(task => ids.includes(task.id)).map(task => task.title))),
      ),
    );

    // Load linked tasks.
    if (this.taskIds && this.taskIds.length) {
      const chunks = chunkArray(this.taskIds, 25);
      chunks.forEach(chunk => this.store.dispatch(Task.loadTasksByIds(chunk)));
    }
  }

  handleSubmit(payload: InstructionForm) {
    const links = [...payload.assetIds, ...payload.taskIds];

    if (this.instruction && this.instruction.id) {
      const currentLinkIds = this.instruction.links;
      const removedLinks = currentLinkIds.filter(currentLink =>
        links.every(nextLink => nextLink !== currentLink),
      );
      const linksAdded = links.filter(nextLink =>
        currentLinkIds.every(currentLink => nextLink !== currentLink),
      );

      this.store.dispatch(
        Instruction.updateInstruction({
          instructionId: this.instruction.id,
          changes: payload,
          links: linksAdded,
          removedLinks,
        }),
      );
    } else {
      this.store.dispatch(
        Instruction.createInstruction({
          instruction: payload,
          links,
        }),
      );
    }

    return this.actions$.pipe(
      ofType(Instruction.updatedInstruction, Instruction.createdInstruction),
      take(1),
      map(({ instruction }) => instruction),
    );
  }

  onMachineTagsSwitch() {
    this.openAssignment.emit(WorkInstructionFormContent.MACHINE_TAGS);
  }

  onAssetsSwitch() {
    this.openAssignment.emit(WorkInstructionFormContent.ASSETS);
  }

  onTasksSwitch() {
    this.openAssignment.emit(WorkInstructionFormContent.TASKS);
  }
}
