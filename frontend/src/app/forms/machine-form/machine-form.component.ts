import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {
  AssetDto,
  AssetType,
  ASSET_HIERARCHY,
  BaseAssetTree,
  getChildrenCount,
  MachineDto,
} from '@common';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';
import { AssetForm } from 'src/app/core/abstract';
import { Asset, MachineTypeTag, State } from 'src/app/core/store';
import { MachineTypeTagComponent } from 'src/app/modals/machine-type-tag/machine-type-tag.component';
import { MappingComponent, MappingModalData } from 'src/app/modals/mapping/mapping.component';
import { AssetSelectItem, FormConfig } from 'src/app/util';

@Component({
  selector: 'syn-machine-form',
  templateUrl: './machine-form.component.html',
  styleUrls: ['./machine-form.component.scss'],
})
export class MachineFormComponent extends AssetForm<MachineDto> implements OnInit {
  type = AssetType.MACHINE;

  mappingUploadEnabled = false;
  maxConstructionYear: number = new Date().getFullYear();
  formConfig = {
    ...this.assetFormConfig,
    parent: ['', Validators.required],
    device: [undefined],
    productionDate: [''],
    manufacturer: [''],
    machineType: [''],
    serialNumber: [''],
    constructionYear: [this.maxConstructionYear, Validators.max(this.maxConstructionYear)],
    isLineOutput: [false],
  } as FormConfig<MachineDto>;

  assetTrees$: Observable<AssetSelectItem[]> = this.store
    .select(Asset.selectAllAssetTrees)
    .pipe(map(trees => this.mapToParentSelectItems(trees)));

  devices$ = this.store
    .select(Asset.selectDevices)
    // Ensure even if the device is not existing on the backend, we keep the reference.
    .pipe(map(devices => Array.from(new Set([...devices, ...(this.device ? [this.device] : [])]))));
  tags$ = this.store.select(MachineTypeTag.selectAll);

  get device() {
    return this.getControl('device').value;
  }

  private mapToParentSelectItems(trees: BaseAssetTree[]): AssetSelectItem[] {
    const mappedItems = [] as AssetSelectItem[];
    for (const tree of trees) {
      mappedItems.push({
        ...tree,
        disabled: tree.type !== AssetType.LINE,
        children: this.mapToParentSelectItems(tree.children || []),
        childrenCount: getChildrenCount(tree),
      });
    }
    return mappedItems.filter(item => item.type !== AssetType.MACHINE);
  }

  private mapToItem(asset: AssetDto, tree: BaseAssetTree): AssetSelectItem {
    // Disable direct descendants.
    if (this.isChild(tree, asset)) {
      return {
        ...tree,
        children: [],
        disabled: true,
      };
    }

    // Disabled if possible parent type is not matching.
    const disabled = ASSET_HIERARCHY[asset.type].parentType !== tree.type;
    return {
      ...tree,
      children: (tree.children || [])
        .filter(child => child.type !== asset.type)
        .reduce((prev, curr) => prev.concat(this.mapToItem(asset, curr)), [] as AssetSelectItem[]),
      disabled,
    };
  }

  private isChild(tree: BaseAssetTree, asset?: AssetDto): boolean {
    return !!asset && !!(tree.children || []).find(child => asset.id === child.id);
  }

  items$ = this.store
    .select(Asset.selectAllAssetTrees)
    .pipe(map(trees => trees.map(tree => this.mapToItem(this.asset as AssetDto, tree))));

  constructor(
    fb: FormBuilder,
    protected store: Store<State>,
    actions$: Actions,
    private dialog: MatDialog,
  ) {
    super(fb, store, actions$);
  }

  ngOnInit() {
    super.ngOnInit();
    this.store.dispatch(MachineTypeTag.loadMachineTypeTags({}));
    this.store.dispatch(Asset.MachineActions.loadDevices({}));
    this.getControl('device')
      .valueChanges.pipe(startWith(this.device), takeUntil(this.destroy$))
      .subscribe(value => (this.mappingUploadEnabled = !!value));
  }

  openMappingModal() {
    if (this.device) {
      this.dialog.open<MappingComponent, MappingModalData>(MappingComponent, {
        data: { device: this.device },
      });
    }
  }
  openTagCreateModal() {
    this.dialog.open(MachineTypeTagComponent);
  }
}
