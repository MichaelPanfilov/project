import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AssetType, getChildrenCount, LineDto } from '@common';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AssetForm } from 'src/app/core/abstract';
import { Asset, State } from 'src/app/core/store';
import { AssetSelectItem, FormConfig } from 'src/app/util';

@Component({
  selector: 'syn-line-form',
  templateUrl: './line-form.component.html',
  styleUrls: ['./line-form.component.scss'],
})
export class LineFormComponent extends AssetForm<LineDto> {
  type = AssetType.LINE;

  formConfig = {
    ...this.assetFormConfig,
    parent: ['', Validators.required],
  } as FormConfig<LineDto>;

  factories$: Observable<AssetSelectItem[]> = this.store
    .select(Asset.selectFactoryTrees)
    .pipe(
      map(trees =>
        trees.map(tree => ({ ...tree, children: [], childrenCount: getChildrenCount(tree) })),
      ),
    );

  constructor(fb: FormBuilder, store: Store<State>, actions$: Actions) {
    super(fb, store, actions$);
  }
}
