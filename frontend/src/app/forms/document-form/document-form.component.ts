import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, ValidatorFn, Validators } from '@angular/forms';
import { FileDto, FilePayload, getRefIdsOf, ReferencedEntities } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, startWith, switchMap, take } from 'rxjs/operators';
import { FormComponent } from 'src/app/core/abstract';
import { Asset, Document, Settings, State } from 'src/app/core/store';
import { DocumentFormContent, FILE_VALIDATOR, FormConfig } from 'src/app/util';
// TODO: extract into separate util file
export type DocumentForm = Pick<FileDto, 'title' | 'machineTypeTags'> & {
  file?: File;
  assetIds: string[];
};

const DOCUMENT_FORM_CONFIG: FormConfig<DocumentForm> = {
  title: ['', [Validators.required, Validators.minLength(3)]],
  file: [undefined, [Validators.required, FILE_VALIDATOR]],
  machineTypeTags: [[]],
  assetIds: [[]],
};

@Component({
  selector: 'syn-document-form',
  templateUrl: './document-form.component.html',
  styleUrls: ['./document-form.component.scss'],
})
export class DocumentFormComponent extends FormComponent<DocumentForm, FileDto>
  implements OnInit, OnDestroy {
  protected formConfig = DOCUMENT_FORM_CONFIG;

  @Input()
  document?: FileDto;

  // Workaround for angular component issue https://github.com/angular/components/issues/13870
  @Input()
  disableAnimation?: boolean;

  @Output()
  openAssignment = new EventEmitter<DocumentFormContent>();

  assets$: Observable<string[]> = of([]);
  uploading$ = this.store.select(
    Settings.selectLoadingByAction(Document.updateDocumentFile, Document.uploadDocument),
  );

  expanded = false;

  get fileName() {
    const file = this.getControl('file').value as File;
    if (file) {
      return file.name;
    }

    return this.document ? this.document.filename : 'DOCUMENT.ACTION.SELECT_FOR_UPLOAD';
  }

  get machineTypeTags(): string[] {
    return this.getControl('machineTypeTags').value;
  }

  get assetIds(): string[] {
    return this.filterAssets(this.getControl('assetIds').value);
  }

  get uploadedFile() {
    return this.getControl('file').value;
  }

  constructor(fb: FormBuilder, private store: Store<State>, private actions$: Actions) {
    super(fb);
  }

  private filterAssets(refIds?: string[]): string[] {
    return getRefIdsOf(ReferencedEntities.ASSET, refIds);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.assets$ = this.getControl('assetIds').valueChanges.pipe(
      startWith(this.assetIds),
      switchMap((ids: string[]) =>
        this.store
          .select(Asset.selectAllAssets)
          .pipe(
            map(assets => assets.filter(asset => ids.includes(asset.id)).map(asset => asset.name)),
          ),
      ),
    );
    // Needed so that file is only required for creation.
    const fileCtrl = this.getControl('file');
    fileCtrl.setValidators(this.getFileValidators());
    fileCtrl.updateValueAndValidity();
  }

  provideFormObj(): DocumentForm {
    const assetIds = this.filterAssets(this.document ? this.document.refIds : []);
    return {
      title: '',
      machineTypeTags: [],
      assetIds,
      ...this.document,
    };
  }

  handleSubmit(payload: DocumentForm): Observable<FileDto> {
    const { assetIds, file } = payload;
    if (!(this.document && this.document.id) && file) {
      this.store.dispatch(Document.uploadDocument({ ...payload, file, refIds: assetIds }));
    } else if (this.document) {
      const docRefIds = this.document.refIds || [];
      const currentAssetIds = this.filterAssets(docRefIds);
      const refIdsWithoutAssetIds = docRefIds.filter(refId => !currentAssetIds.includes(refId));
      const refIds = Array.from(new Set([...assetIds, ...refIdsWithoutAssetIds]));

      if (file) {
        this.updateDocumentFile(this.document.id, file, { ...payload, refIds });
      } else {
        this.updateDocument(this.document.id, { ...payload, refIds });
      }
    }

    return this.actions$.pipe(
      ofType(Document.uploadedDocument, Document.updatedDocument),
      take(1),
      map(({ document }) => document),
    );
  }

  onOpenTagModal() {
    this.openAssignment.emit(DocumentFormContent.MACHINE_TAGS);
  }

  onOpenAssetLinkModal() {
    this.openAssignment.emit(DocumentFormContent.ASSETS);
  }

  private updateDocumentFile(documentId: string, file: File, payload: FilePayload) {
    this.store.dispatch(Document.updateDocumentFile({ documentId, file }));
    this.actions$.pipe(ofType(Document.updatedDocumentFile), take(1)).subscribe(({ document }) => {
      this.updateDocument(document.id, payload);
    });
  }

  private updateDocument(documentId: string, payload: FilePayload) {
    this.store.dispatch(Document.updateDocument({ documentId, ...payload }));
  }

  private getFileValidators(): ValidatorFn[] {
    const validators = [FILE_VALIDATOR];
    if (this.document && this.document.id) {
      return validators;
    }
    return [Validators.required, ...validators];
  }
}
