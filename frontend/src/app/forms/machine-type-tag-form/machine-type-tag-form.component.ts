import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MachineTypeTag } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { FormComponent } from 'src/app/core/abstract';
import { MachineTypeTag as TagStore, State } from 'src/app/core/store';
import { DEFAULT_VALIDATORS, FormConfig } from 'src/app/util';

@Component({
  selector: 'syn-machine-type-tag-form',
  templateUrl: './machine-type-tag-form.component.html',
  styleUrls: ['./machine-type-tag-form.component.scss'],
})
export class MachineTypeTagFormComponent extends FormComponent<MachineTypeTag, MachineTypeTag> {
  protected formConfig: FormConfig<MachineTypeTag> = {
    name: ['', DEFAULT_VALIDATORS],
  };

  constructor(fb: FormBuilder, private store: Store<State>, private actions$: Actions) {
    super(fb);
  }

  protected provideFormObj(): Partial<MachineTypeTag> | undefined {
    return {
      name: '',
    };
  }

  protected handleSubmit(payload: MachineTypeTag): Observable<MachineTypeTag | undefined> {
    this.store.dispatch(TagStore.createMachineTypeTag(payload));

    return this.actions$.pipe(
      ofType(TagStore.createdMachineTypeTag),
      take(1),
      map(({ machineTypeTag }) => machineTypeTag),
    );
  }
}
