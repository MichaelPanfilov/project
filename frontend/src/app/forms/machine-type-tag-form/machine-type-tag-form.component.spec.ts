import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineTypeTagFormComponent } from './machine-type-tag-form.component';

describe('MachineTypeTagFormComponent', () => {
  let component: MachineTypeTagFormComponent;
  let fixture: ComponentFixture<MachineTypeTagFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachineTypeTagFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineTypeTagFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
