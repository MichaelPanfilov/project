import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { AssetDto, AssetType, ASSET_FILE_TAG } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ImageUploader } from 'src/app/core/abstract';
import { FileService, NotificationService } from 'src/app/core/services';
import { Asset, State } from 'src/app/core/store';
import { AssetWithParent, SYN_COLORS } from 'src/app/util';

@Component({
  selector: 'syn-asset-form',
  templateUrl: './asset-form.component.html',
  styleUrls: ['./asset-form.component.scss'],
})
export class AssetFormComponent extends ImageUploader implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  protected backgroundSize = 'contain';

  @ViewChild('assetImg', { static: true })
  imgRef!: ElementRef;

  @Input()
  set asset(asset: AssetDto | undefined) {
    this.asset$.next(asset);
  }

  get asset() {
    return this.asset$.value;
  }

  @Input()
  type!: AssetType;

  @Output()
  exit = new EventEmitter();

  @Output()
  valid = new EventEmitter<boolean>();

  @Output()
  changed = new EventEmitter<boolean>();

  asset$ = new BehaviorSubject<AssetDto | undefined>(undefined);

  AssetTypes = AssetType;
  disabled = true;

  constructor(
    private store: Store<State>,
    private actions$: Actions,
    fileService: FileService,
    notificationService: NotificationService,
    renderer: Renderer2,
  ) {
    super(fileService, notificationService, renderer);
  }

  ngOnInit() {
    this.asset$.pipe(takeUntil(this.destroy$)).subscribe(asset => {
      if (asset && asset.id) {
        this.resetImg();
        this.setImg('');
        this.initImage(asset.id, ASSET_FILE_TAG);
        this.type = asset.type;
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onUploadImg(asset: AssetWithParent<AssetDto>) {
    // TODO: Rework action hide updating/uploading/deleting of images in the effects.
    if (this.uploadedImage) {
      const updateImgAction = this.imageId ? Asset.updateAssetImage : Asset.uploadAssetImage;
      this.store.dispatch(
        updateImgAction({
          image: this.uploadedImage,
          prevImageId: this.imageId,
          asset,
        }),
      );
      this.actions$
        .pipe(ofType(Asset.updatedAssetImage, Asset.uploadedAssetImage), take(1))
        .subscribe(() => this.exit.emit());
    } else {
      this.exit.emit();
    }
  }

  onFormValidate(e: boolean) {
    this.valid.emit(e);
  }

  protected getPlaceholder(hasImg: boolean): string {
    return hasImg ? 'white' : SYN_COLORS.textLight;
  }

  protected resetImg() {
    super.resetImg();
    if (this.asset) {
      this.type = this.asset.type;
    }
  }
}
