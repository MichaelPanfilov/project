import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { ASSET_FILE_TAG, LANG_MAP } from '@common';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { combineLatest, of } from 'rxjs';
import { distinctUntilChanged, filter, map, startWith } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services';
import { Asset, Image, State, User } from 'src/app/core/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  isSidebarOpen$ = combineLatest([
    this.router.events.pipe(
      startWith(this.router),
      filter(
        (event): event is NavigationStart | Router =>
          event instanceof NavigationStart || event instanceof Router,
      ),
    ),
    this.authService.loggedIn$,
  ]).pipe(map(([event, isLoggedIn]) => event.url !== '/login' && isLoggedIn));

  isLoading$ = of(false);
  // TODO: Enable this when we loading feedback is required.
  /*
   this.store
    .select(Settings.selectLoading)
    .pipe(startWith(false), distinctUntilChanged(), debounceTime(100));
  */

  constructor(
    private router: Router,
    private store: Store<State>,
    // private loading: LoadingService,
    private authService: AuthService,
    private translate: TranslateService,
  ) {}

  async ngOnInit(): Promise<void> {
    // TODO: Enable this when we loading feedback is required.
    /*
    this.store
      .select(Settings.selectBlocking)
      .pipe(startWith(false), distinctUntilChanged(), debounceTime(100))
      .subscribe(blocked => (blocked ? this.loading.show() : this.loading.hide()));
    */

    this.authService.loggedIn$.pipe(distinctUntilChanged()).subscribe(loggedIn => {
      if (loggedIn) {
        this.store.dispatch(Asset.TreeActions.loadTrees({}));
        this.store.dispatch(Image.loadImagesData({ tag: ASSET_FILE_TAG }));
      }
    });

    this.store
      .select(User.selectMe)
      .pipe(
        map(me => me && me.language),
        distinctUntilChanged(),
      )
      .subscribe(lang => {
        if (lang) {
          if (lang in LANG_MAP) {
            this.translate.use(LANG_MAP[lang]);
          } else {
            console.warn('Missing locale');
          }
        }
      });
  }
}
