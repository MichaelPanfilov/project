import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { MatRipple } from '@angular/material/core';

import { SYN_COLORS } from '../../util';

export type ButtonMode = 'basic' | 'strong' | 'weak';
export type ButtonColor = 'primary' | 'secondary' | 'danger' | 'pending';

const DEFAULT_MODE = 'basic';
const DEFAULT_COLOR = 'primary';

// add transparency to default color values
const RIPPLE_COLORS: { [key in ButtonColor]: string } = {
  primary: SYN_COLORS.primaryDark + '10',
  secondary: SYN_COLORS.secondaryLight + '99',
  danger: SYN_COLORS.danger + '99',
  pending: SYN_COLORS.pending + '55',
};

@Directive({
  selector: '[synButton]',
  providers: [MatRipple],
})
export class ButtonDirective {
  private _mode?: ButtonMode;
  private _color?: ButtonColor;

  @HostListener('disabled')
  // tslint:disable-next-line
  set disabled(value: any) {
    value ? this.addClass(`syn-button-disabled`) : this.removeClass(`syn-button-disabled`);
  }

  @HostListener('mousedown', ['$event']) onmousedown(event: MouseEvent) {
    const color = this._color && RIPPLE_COLORS[this._color];
    // this works only on position:relative elements
    this.ripple.launch(event.x, event.y, { color });
  }

  @Input()
  set mode(value: ButtonMode) {
    const mode = value || DEFAULT_MODE;
    if (mode !== this._mode) {
      if (this._mode) {
        this.removeClass(`syn-button-${this._mode}`);
      }
      if (mode) {
        this.addClass(`syn-button-${mode}`);
      }
    }
    this._mode = mode;
  }

  @Input()
  set color(value: ButtonColor) {
    const color = value || DEFAULT_COLOR;
    if (color !== this._color) {
      if (this._color) {
        this.removeClass(`syn-color-${this._color}`);
      }
      if (color) {
        this.addClass(`syn-color-${color}`);
      }
    }
    this._color = color;
  }

  constructor(private el: ElementRef, private ripple: MatRipple) {
    this.mode = DEFAULT_MODE;
    this.color = DEFAULT_COLOR;
  }

  private addClass(clazz: string) {
    this.el.nativeElement.classList.add(clazz);
  }

  private removeClass(clazz: string) {
    this.el.nativeElement.classList.remove(clazz);
  }
}
