import { Directive, ElementRef, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { fromEvent, merge, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[synSearch]',
})
export class SearchInputDirective implements OnDestroy {
  @Output() search = new EventEmitter<string>();

  @Input()
  searchDelay?: number | string;

  private destroy$ = new Subject<void>();

  constructor(private input: ElementRef) {
    this.initListeners();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private initListeners(): void {
    merge<KeyboardEvent, ClipboardEvent>(
      fromEvent(this.input.nativeElement, 'paste'),
      fromEvent(this.input.nativeElement, 'keyup'),
    )
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(isFinite(Number(this.searchDelay)) ? Number(this.searchDelay) : 500),
      )
      .subscribe((event: KeyboardEvent | ClipboardEvent) => {
        this.search.emit((event.target as HTMLInputElement).value);
      });
  }
}
