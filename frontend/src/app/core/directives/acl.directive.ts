import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AclResource, AclRight, AppUser } from '@common';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

import { AuthService } from '../services/auth.service';
import { State, User } from '../store';

@Directive({
  selector: '[synAcl]',
})
export class AclDirective implements OnInit {
  @Input()
  synAcl?: AclOptions;

  constructor(
    private viewContainer: ViewContainerRef,
    private templateRef: TemplateRef<unknown>,
    private authService: AuthService,
    private store: Store<State>,
  ) {}

  async ngOnInit() {
    if (!this.synAcl) {
      this.showTemplate();
      return;
    }

    const { allowMe, prohibitMe, aclResources, aclRight } = this.synAcl;
    if (allowMe && prohibitMe) {
      throw new Error('`allowMe` and `prohibitMe` can not be used at the same time');
    }

    this.hideTemplate();

    if (allowMe || prohibitMe) {
      const me = await this.store
        .select(User.selectMe)
        .pipe(take(1))
        .toPromise();
      if (me) {
        if (prohibitMe && prohibitMe.id === me.id) {
          return;
        } else if (me && allowMe && me.id === allowMe.id) {
          this.showTemplate();
          return;
        }
      }
    }

    if (this.authService.hasRight([{ resources: aclResources, right: aclRight }])) {
      this.showTemplate();
    }
  }

  private showTemplate() {
    this.viewContainer.createEmbeddedView(this.templateRef);
  }

  private hideTemplate() {
    this.viewContainer.clear();
  }
}

interface AclOptions {
  allowMe?: AppUser;
  prohibitMe?: AppUser;
  aclResources: AclResource[];
  aclRight: AclRight;
}
