import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[synClickActive]',
})
export class ClickActiveDirective {
  @HostListener('onmousedown')
  onDown() {
    this.addClass(`syn-active`);
  }

  @HostListener('onmouseup')
  onUp() {
    setTimeout(() => {
      this.removeClass(`syn-active`);
    }, 500);
  }

  constructor(private el: ElementRef) {}

  private addClass(clazz: string) {
    this.el.nativeElement.classList.add(clazz);
  }

  private removeClass(clazz: string) {
    this.el.nativeElement.classList.remove(clazz);
  }
}
