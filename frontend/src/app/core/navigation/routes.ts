import { Data, Route } from '@angular/router';

export const ROUTE_SEGMENTS = {
  home: {
    path: '',
    label: 'NAVIGATION.LABEL.HOME',
  },
  login: {
    path: 'login',
    label: 'NAVIGATION.LABEL.LOGIN',
  },
  lines: {
    path: 'lines',
    label: 'NAVIGATION.LABEL.LINES_OVERVIEW',
  },
  machines: {
    path: 'machines',
    label: 'NAVIGATION.LABEL.MACHINES_OVERVIEW',
  },
  machinePerformance: {
    path: 'performance',
    label: 'NAVIGATION.LABEL.MACHINE_PERFORMANCE',
  },
  machineTimeProducing: {
    path: 'time-producing',
    label: 'NAVIGATION.LABEL.MACHINE_TIME_PRODUCING',
  },
  machineProductLoss: {
    path: 'product-losses',
    label: 'NAVIGATION.LABEL.MACHINE_PRODUCT_LOSS',
  },
  machineFaultStops: {
    path: 'fault-stops',
    label: 'NAVIGATION.LABEL.MACHINE_FAULT_STOPS',
  },
  machineFaultStopCounts: {
    path: 'counts',
    label: 'NAVIGATION.LABEL.MACHINE_FAULT_STOPS_COUNTS',
  },
  machineFaultStopDurations: {
    path: 'durations',
    label: 'NAVIGATION.LABEL.MACHINE_FAULT_STOPS_DURATIONS',
  },
  settings: {
    path: 'settings',
    label: 'NAVIGATION.LABEL.SETTINGS',
  },
  userList: {
    path: 'users',
    label: 'NAVIGATION.LABEL.USER_MANAGEMENT',
  },
  assetManagement: {
    path: 'assets',
    label: 'NAVIGATION.LABEL.ASSET_MANAGEMENT',
  },
  taskManagement: {
    path: 'tasks',
    label: 'NAVIGATION.LABEL.TASK_MANAGEMENT',
  },
  documentManagement: {
    path: 'documents',
    label: 'NAVIGATION.LABEL.DOCUMENT_MANAGEMENT',
  },
  workInstructions: {
    path: 'instructions',
    label: 'NAVIGATION.LABEL.WORK_INSTRUCTIONS',
  },
};

export interface NavData {
  title: string;
  path: string;
  param?: string;
  subPages?: { title: string; path: string }[];
  dynamicParam?: string;
  navBarIgnore?: boolean;
  dynamicTitle?: string;
  submenuHidden?: boolean;
  tabsSubmenu?: boolean;
}

export interface EnhancedRoute extends Route {
  children?: EnhancedRoutes;
  data: Data & { nav: NavData };
}

export type EnhancedRoutes = EnhancedRoute[];

export function isRouteDynamic(route: string): boolean {
  return route.startsWith(':') || route.includes('/:');
}

export function getAllPaths(route: EnhancedRoute): string[] {
  if (!route.children) {
    return [route.path || ''];
  }
  return route.children.reduce((prev, curr) => prev.concat(getAllPaths(curr)), [] as string[]);
}

export function homePath(): string[] {
  return [''];
}

export function loginPath(): string[] {
  return [ROUTE_SEGMENTS.login.path];
}

export function lineDetailPath(lineId: string): string[] {
  return [ROUTE_SEGMENTS.lines.path, lineId];
}

export function machineDetailPath(machineId: string): string[] {
  return [ROUTE_SEGMENTS.machines.path, machineId];
}

export function machinePerformancePath(machineId: string): string[] {
  return [...machineDetailPath(machineId), ROUTE_SEGMENTS.machinePerformance.path];
}

export function machineTimeProducingPath(machineId: string): string[] {
  return [...machineDetailPath(machineId), ROUTE_SEGMENTS.machineTimeProducing.path];
}

export function machineProductLossPath(machineId: string): string[] {
  return [...machineDetailPath(machineId), ROUTE_SEGMENTS.machineProductLoss.path];
}

export function machineFaultStopsPath(machineId: string): string[] {
  return [...machineDetailPath(machineId), ROUTE_SEGMENTS.machineFaultStops.path];
}

export function machineFaultStopCountsPath(machineId: string): string[] {
  return [...machineFaultStopsPath(machineId), ROUTE_SEGMENTS.machineFaultStopCounts.path];
}

export function machineFaultStopDurationsPath(machineId: string): string[] {
  return [...machineFaultStopsPath(machineId), ROUTE_SEGMENTS.machineFaultStopDurations.path];
}

export function assetsPath(): string[] {
  return [ROUTE_SEGMENTS.settings.path, ROUTE_SEGMENTS.assetManagement.path];
}
