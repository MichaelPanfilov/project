import { Injectable } from '@angular/core';
import { Acl, JWTPayload, matchesAcl } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as jwtDecode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';

import { State, User } from '../store';
import { resetStore } from '../store/actions';

import { AclService, Credentials } from './api/acl.service';
import { Storage } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private rights: string[] = [];
  private initialized = false;

  readonly token$ = new BehaviorSubject<string | undefined>(undefined);
  readonly jwt$ = this.token$.pipe(
    map(token => (token ? jwtDecode<JWTPayload>(token) : undefined)),
  );
  readonly loggedIn$ = this.store.select(User.selectMe).pipe(map(me => this.initialized && !!me));

  constructor(
    private store: Store<State>,
    private actions$: Actions,
    private storage: Storage,
    private aclService: AclService,
    private cookieService: CookieService,
  ) {
    this.jwt$.subscribe(jwt => (this.rights = jwt ? jwt.rights : []));
  }

  async init(): Promise<void> {
    try {
      const token = await this.storage.getToken();
      if (token) {
        await this.onLogin(token);
      }
    } catch {}
    this.initialized = true;
  }

  login(credentials: Credentials): Observable<void> {
    return this.aclService.login(credentials).pipe(switchMap(token => this.onLogin(token)));
  }

  logout(): Observable<void> {
    return this.aclService.logout().pipe(switchMap(() => from(this.onLogout())));
  }

  renew(): Observable<string> {
    return this.aclService.renew().pipe(
      filter(token => !!token),
      tap(async token => this.onLogin(token)),
    );
  }

  hasRight(acl: Acl[]): boolean {
    return matchesAcl(this.rights, acl);
  }

  private async onLogin(token: string): Promise<void> {
    this.token$.next(token);
    await this.storage.setToken(token);
    try {
      this.store.dispatch(User.loadMe({}));
      const error = await this.actions$
        .pipe(
          ofType(User.loadedMe, User.failedLoadingMe),
          take(1),
          // tslint:disable-next-line
          map((payload: any) => payload.error),
        )
        .toPromise();

      if (error) {
        throw error;
      }
      this.cookieService.set(
        'token',
        'Bearer ' + token,
        undefined,
        undefined,
        undefined,
        undefined,
        'Strict',
      );
    } catch {
      if (!this.token$.value) {
        this.onLogout();
      }
    } finally {
      this.initialized = true;
    }
  }

  private async onLogout(): Promise<void> {
    this.token$.next(undefined);
    this.store.dispatch(resetStore());
    this.cookieService.delete('token');
    await this.storage.removeToken();
  }
}
