import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { switchMap } from 'rxjs/operators';

export enum NotificationVariants {
  ERROR = 'error',
  SUCCESS = 'success',
}

interface NotificationOptions {
  variant: NotificationVariants;
  message: string;
  param?: object;
  action?: string;
}

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(private snackBar: MatSnackBar, private translate: TranslateService) {}

  async notify(opts: NotificationOptions): Promise<void> {
    await this.translate
      .get(opts.message, opts.param)
      .pipe(
        switchMap(trans =>
          this.snackBar
            .open(trans, opts.action, {
              duration: 3500,
              panelClass: ['notification', opts.variant],
              verticalPosition: 'top',
              horizontalPosition: 'right',
            })
            .afterOpened(),
        ),
      )
      .toPromise();
  }
}
