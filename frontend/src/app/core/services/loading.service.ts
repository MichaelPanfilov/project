import { Overlay } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { MatSpinner } from '@angular/material/progress-spinner';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  private spinnerTopRef = this.cdkSpinnerCreate();

  constructor(private overlay: Overlay) {}

  show() {
    if (this.spinnerTopRef.hasAttached()) {
      return;
    }
    this.spinnerTopRef.attach(new ComponentPortal(MatSpinner));
  }

  hide() {
    if (this.spinnerTopRef.hasAttached()) {
      this.spinnerTopRef.detach();
    }
  }

  private cdkSpinnerCreate() {
    return this.overlay.create({
      hasBackdrop: true,
      backdropClass: 'dark-backdrop',
      positionStrategy: this.overlay
        .position()
        .global()
        .centerHorizontally()
        .centerVertically(),
    });
  }
}
