import { Injectable } from '@angular/core';

const KEYS = {
  TOKEN: 'token',
};

@Injectable({
  providedIn: 'root',
})
export class Storage {
  async getToken(): Promise<string | null> {
    return localStorage.getItem(KEYS.TOKEN);
  }

  async setToken(token: string): Promise<void> {
    return localStorage.setItem(KEYS.TOKEN, token);
  }

  async removeToken(): Promise<void> {
    localStorage.removeItem(KEYS.TOKEN);
  }
}
