import { TestBed } from '@angular/core/testing';

import { NavbarActionsViewService } from './navbar-actions-view.service';

describe('NavbarActionsViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavbarActionsViewService = TestBed.get(NavbarActionsViewService);
    expect(service).toBeTruthy();
  });
});
