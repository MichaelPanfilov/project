import { Injectable } from '@angular/core';
import { ImageDto } from '@common';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Image, State } from '../store';

@Injectable({
  providedIn: 'root',
})
export class ImageService {
  constructor(private store: Store<State>) {}

  getImageBlob(imageData: ImageDto): Observable<string | undefined> {
    return this.store.select(Image.selectImageById(imageData.id)).pipe(
      switchMap(fileData => {
        if (!fileData) {
          return of(undefined);
        }
        if (fileData.image) {
          return of(fileData.image);
        }
        this.store.dispatch(Image.loadImageFile({ imageData }));
        return this.store
          .select(Image.selectImageById(imageData.id))
          .pipe(map(imgData => imgData && imgData.image));
      }),
    );
  }
}
