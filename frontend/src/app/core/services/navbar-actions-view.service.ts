import { EmbeddedViewRef, Injectable, TemplateRef, ViewContainerRef } from '@angular/core';
import { BehaviorSubject, combineLatest } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NavbarActionsViewService {
  private viewRef$ = new BehaviorSubject<EmbeddedViewRef<unknown> | null>(null);
  private viewContainer$ = new BehaviorSubject<ViewContainerRef | null>(null);

  constructor() {
    combineLatest([this.viewRef$, this.viewContainer$]).subscribe(
      ([viewRef, viewContainer]) => viewRef && viewContainer && viewContainer.insert(viewRef),
    );
  }

  initViewContainer(viewContainer: ViewContainerRef) {
    this.viewContainer$.next(viewContainer);
  }

  insertActionTemplate(actionsTemplate: TemplateRef<unknown>) {
    this.viewRef$.next(actionsTemplate.createEmbeddedView(null));
  }

  clearActionView() {
    const viewContainer = this.viewContainer$.value;
    if (viewContainer) {
      viewContainer.detach();
      this.viewRef$.next(null);
    }
  }
}
