import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  DataResponse,
  DOCUMENT_FILE_TAG,
  FileDto,
  FilesFilter,
  PagingResponseMeta,
  Sorting,
} from '@common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { ApiService } from './api.service';

type OptionalArray = string | string[] | undefined;

@Injectable({
  providedIn: 'root',
})
export class FileService extends ApiService {
  private url = environment.fileServiceUrl;

  constructor(http: HttpClient) {
    super(http);
  }

  getDocuments(
    filter: FilesFilter = {},
    sorting?: Sorting,
  ): Observable<{ documents: FileDto[]; meta: PagingResponseMeta }> {
    let params = new HttpParams();
    Object.entries(filter).forEach(([key, value]) => (params = this.addParam(params, key, value)));
    params = params.set('tag', DOCUMENT_FILE_TAG);
    if (sorting) {
      params = params.set('order', sorting.join(' '));
    }

    return this.getManyWithMeta<FileDto>(`${this.url}/v1/versions`, { params }).pipe(
      map(res => ({ documents: res.data, meta: res.meta })),
    );
  }

  getFile(fileId: string): Observable<FileDto> {
    return this.getOne<FileDto>(`${this.url}/v1/files/${fileId}`);
  }

  getQRCodeById(fileId: string): Observable<{ code: string }> {
    return this.getOne<{ code: string }>(`${this.url}/v1/files/${fileId}/qr-code`);
  }

  getFileByFilter(refIds: string, tags?: string): Observable<FileDto | undefined> {
    const params = this.getParams(refIds, tags);
    return this.getMany<FileDto>(`${this.url}/v1/files`, { params }).pipe(map(files => files[0]));
  }

  getFilesByTag(tag: string): Observable<FileDto[]> {
    const params = new HttpParams().set('tag', tag);
    return this.getMany<FileDto>(`${this.url}/v1/files`, { params });
  }

  uploadFile(
    file: File,
    refIds?: OptionalArray,
    tags?: OptionalArray,
    title?: string,
    machineTypeTags?: OptionalArray,
  ): Observable<FileDto> {
    const fileForm = this.prepareFile(
      { refIds, tags, title, machineTypeTags } as Partial<FileDto>,
      file,
    );
    return this.post<FileDto>(`${this.url}/v1/files`, fileForm);
  }

  uploadFileProgress(
    file: File,
    refIds?: OptionalArray,
    tags?: OptionalArray,
    title?: string,
    machineTypeTags?: OptionalArray,
  ) {
    const fileForm = this.prepareFile(
      { refIds, tags, title, machineTypeTags } as Partial<FileDto>,
      file,
    );
    return this.http.post<DataResponse<FileDto>>(`${this.url}/v1/files`, fileForm, {
      reportProgress: true,
      observe: 'events',
    });
  }

  updateFile(changes: Partial<FileDto>, id: string): Observable<FileDto> {
    return this.put<FileDto>(`${this.url}/v1/files/${id}`, changes);
  }

  updateVersion(documentId: string, file: File) {
    const fileForm = this.prepareFile({}, file);
    return this.http.post<DataResponse<FileDto>>(
      `${this.url}/v1/versions/${documentId}/new`,
      fileForm,
      {
        reportProgress: true,
        observe: 'events',
      },
    );
  }

  deleteFile(id: string): Observable<void> {
    return super.delete(`${this.url}/v1/files/${id}`);
  }

  private getParams(refIds: string, tags?: string) {
    const params = new HttpParams().set('refId', refIds);
    if (tags) {
      return params.set('tag', tags);
    }
    return params;
  }

  private addParam(
    params: HttpParams,
    key: string,
    value?: number | string | string[],
  ): HttpParams {
    if (!value) {
      return params;
    }
    if (typeof value === 'number' || typeof value === 'string') {
      return params.set(key, String(value));
    }
    return value.length ? params.set(key, JSON.stringify(value)) : params;
  }

  private prepareFile(changes: Partial<FileDto>, file?: File): FormData {
    const fileForm = new FormData();
    if (file) {
      fileForm.set('file', file);
    }

    if (changes.title) {
      fileForm.append('title', changes.title);
    }

    const setArray = (key: string, data?: OptionalArray) => {
      if (Array.isArray(data)) {
        data.forEach(tag => fileForm.append(key, tag));
      } else if (data) {
        fileForm.set(key, data);
      }
    };

    setArray('machineTypeTags', changes.machineTypeTags);
    setArray('refIds', changes.refIds);
    setArray('tags', changes.tags);

    return fileForm;
  }
}
