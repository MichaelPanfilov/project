import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  isCalendarTask,
  PaginationParams,
  PagingResponseMeta,
  ParsedFilter,
  Sorting,
  TaskDto,
} from '@common';
import { QuerySortOperator } from '@nestjsx/crud-request';
import { Observable, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';
import { Stringified } from 'src/app/util';
import { environment } from 'src/environments/environment';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class TasksService extends ApiService {
  private readonly url = `${environment.apiUrl}/v1/tasks`;

  constructor(http: HttpClient) {
    super(http);
  }

  private transformDates(dto: Stringified<TaskDto>): TaskDto {
    const transformed = {
      ...dto,
    } as TaskDto;

    if (dto.dueDate) {
      transformed.dueDate = new Date(dto.dueDate);
    }
    if (dto.doneDate) {
      transformed.doneDate = new Date(dto.doneDate);
    }
    if (isCalendarTask(transformed)) {
      transformed.calendarDate = new Date(transformed.calendarDate);
    }

    return transformed;
  }

  private mapTask(): OperatorFunction<Stringified<TaskDto>, TaskDto> {
    return map((task: Stringified<TaskDto>) => this.transformDates(task));
  }

  getTasks(
    filter: ParsedFilter<TaskDto>,
    pageParams: PaginationParams = {},
    sortParams?: Sorting,
  ): Observable<{ tasks: TaskDto[]; meta: PagingResponseMeta }> {
    const query = this.qb
      .create({
        filter: this.buildFilter(filter),
        ...pageParams,
        sort: sortParams
          ? { field: sortParams[0], order: sortParams[1].toUpperCase() as QuerySortOperator }
          : undefined,
      })
      .query();

    return this.getManyWithMeta<TaskDto>(`${this.url}/?${query}`).pipe(
      map(res => ({ tasks: res.data.map(task => this.transformDates(task)), meta: res.meta })),
    );
  }

  createTask(task: Partial<TaskDto>): Observable<TaskDto> {
    return this.post<TaskDto>(this.url, task).pipe(this.mapTask());
  }

  updateTask(taskId: string, changes: Partial<TaskDto>): Observable<TaskDto> {
    return this.put<TaskDto>(`${this.url}/${taskId}`, changes).pipe(this.mapTask());
  }

  markDoneTask(taskId: string): Observable<TaskDto> {
    return this.post<TaskDto>(`${this.url}/${taskId}/mark-done`, {}).pipe(this.mapTask());
  }

  deleteTask(taskId: string): Observable<void> {
    return this.delete(`${this.url}/${taskId}`);
  }

  assignAsset(taskId: string, assetId: string): Observable<TaskDto> {
    return this.post<TaskDto>(`${this.url}/${taskId}/asset/${assetId}`, {}).pipe(this.mapTask());
  }

  cloneTask(taskId: string): Observable<TaskDto> {
    return this.post<TaskDto>(`${this.url}/${taskId}/clone`, {}).pipe(this.mapTask());
  }
}
