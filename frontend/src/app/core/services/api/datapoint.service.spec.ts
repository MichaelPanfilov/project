import { TestBed } from '@angular/core/testing';

import { DatapointService } from './datapoint.service';

describe('DatapointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatapointService = TestBed.get(DatapointService);
    expect(service).toBeTruthy();
  });
});
