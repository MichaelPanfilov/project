import { TestBed } from '@angular/core/testing';

import { MachineTypeTagService } from './machine-type-tag.service';

describe('MachineTypeTagService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MachineTypeTagService = TestBed.get(MachineTypeTagService);
    expect(service).toBeTruthy();
  });
});
