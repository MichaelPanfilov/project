import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AclUserDto, RoleDto } from '@common';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { ApiService } from './api.service';

export interface Credentials {
  login: string;
  password: string;
}

@Injectable({
  providedIn: 'root',
})
export class AclService extends ApiService {
  private readonly url = environment.aclServiceUrl;

  constructor(http: HttpClient) {
    super(http);
  }

  me(): Observable<AclUserDto> {
    return this.getOne<AclUserDto>(`${this.url}/v1/me`);
  }

  login(credentials: Credentials): Observable<string> {
    return this.post<string>(`${this.url}/v1/login`, credentials);
  }

  logout(): Observable<void> {
    return this.put(`${this.url}/v1/logout_all`, {}).pipe(
      catchError(() => of(null)),
      map(() => {}),
    );
  }

  renew(): Observable<string> {
    return this.getOne<string>(`${this.url}/v1/renew`);
  }

  changePassword(oldPassword: string, newPassword: string): Observable<AclUserDto> {
    return this.put<AclUserDto>(`${this.url}/v1/me/change_password`, {
      oldPassword,
      newPassword,
    });
  }

  changeUserPassword(userId: string, newPassword: string): Observable<AclUserDto> {
    return this.put<AclUserDto>(`${this.url}/v1/users/${userId}/change_password`, {
      newPassword,
    });
  }

  getUserById(aclId: string): Observable<AclUserDto> {
    return this.getOne<AclUserDto>(`${this.url}/v1/users/${aclId}`);
  }

  getUsers(): Observable<AclUserDto[]> {
    return this.getMany<AclUserDto>(`${this.url}/v1/users`);
  }

  updateUser(user: Partial<AclUserDto>, aclId: string): Observable<AclUserDto> {
    return this.put<AclUserDto>(`${this.url}/v1/users/${aclId}`, user);
  }

  getRoles(): Observable<RoleDto[]> {
    return this.getOne<RoleDto[]>(`${this.url}/v1/roles`);
  }

  updateUserRoles(roleIds: string[], aclId: string): Observable<RoleDto[]> {
    return this.getOne<RoleDto[]>(`${this.url}/v1/users/${aclId}/roles`).pipe(
      switchMap(roles => {
        const removedRoles = roles.filter(r => !roleIds.includes(r.id));
        if (removedRoles.length) {
          return forkJoin(removedRoles.map(r => this.removeUserRole(aclId, r)));
        }
        return of(null);
      }),
      switchMap(() => this.put<RoleDto[]>(`${this.url}/v1/users/${aclId}/roles`, { roleIds })),
    );
  }

  removeUserRole(aclId: string, role: RoleDto): Observable<void> {
    return this.delete(`${this.url}/v1/users/${aclId}/roles/${role.id}`);
  }
}
