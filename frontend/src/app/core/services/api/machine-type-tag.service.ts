import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MachineTypeTag } from '@common';

import { environment } from '../../../../environments/environment';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class MachineTypeTagService extends ApiService {
  private readonly url = `${environment.apiUrl}/v1/machine-type-tags`;
  constructor(http: HttpClient) {
    super(http);
  }

  getMachineTypeTags() {
    return this.getMany<string>(this.url);
  }

  createMachineTypeTag(name: string) {
    return this.post<MachineTypeTag>(this.url, { name });
  }
}
