import { HttpClient } from '@angular/common/http';
import {
  DataResponse,
  PagingResponseMeta,
  ParsedFilter,
  RequestFilter,
  REQUEST_OPT,
} from '@common';
import { QueryFilter, RequestQueryBuilder } from '@nestjsx/crud-request';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Stringified } from 'src/app/util';

RequestQueryBuilder.setOptions(REQUEST_OPT);

type Res<T, R = {}> = DataResponse<Stringified<T>, Stringified<R>>;
type ArrRes<T, R = {}> = DataResponse<Stringified<T>[], Stringified<R>>;

type StringifiedObs<T> = Observable<Stringified<T>>;

export abstract class ApiService {
  protected qb = RequestQueryBuilder;

  constructor(protected http: HttpClient) {}

  protected getOne<T>(url: string, opts = {}): StringifiedObs<T> {
    return this.http.get<Res<T>>(url, opts).pipe(this.extractData());
  }

  protected getMany<T>(url: string, opts = {}): Observable<Stringified<T>[]> {
    // TODO: Resolve type problems and replace with extractData()
    return this.http.get<ArrRes<T>>(url, opts).pipe(map(res => res.data));
  }

  protected getManyWithMeta<T>(
    url: string,
    opts = {},
  ): Observable<{ data: Stringified<T>[]; meta: PagingResponseMeta }> {
    // TODO: Resolve type problems and replace with extractData()
    return this.http.get<{ data: Stringified<T>[]; meta: PagingResponseMeta }>(url, opts);
  }

  protected post<T, R = unknown>(url: string, data: R, opts = {}): StringifiedObs<T> {
    return this.http.post<Res<T>>(url, data, opts).pipe(this.extractData());
  }

  protected put<T, R = unknown>(url: string, data: R, opts = {}): StringifiedObs<T> {
    return this.http.put<Res<T>>(url, data, opts).pipe(this.extractData());
  }

  protected delete<T = void>(url: string, opts = {}): StringifiedObs<T> {
    return this.http.delete<Res<T>>(url, opts).pipe(this.extractData());
  }

  protected getPage<T>(url: string): Observable<Res<T[], PagingResponseMeta>> {
    return this.http.get<Res<T[], PagingResponseMeta>>(url);
  }

  protected getNextPage<T>(
    url: string,
    meta: PagingResponseMeta,
  ): Observable<Res<T[], PagingResponseMeta>> {
    const query = this.qb
      .create()
      .setPage(meta.page + 1)
      .query();
    return this.getPage(`${url}/?${query}`);
  }

  protected buildFilter<T extends object>(filter: ParsedFilter<T>): QueryFilter[] {
    return Object.entries(filter).reduce((prev, [key, query]) => {
      const arr = Array.isArray(query)
        ? (query as RequestFilter[]).map(q => ({ field: key, ...q }))
        : [{ field: key, ...(query as RequestFilter) }];
      // We filter empty value properties (if it is an array) to avoid request errors.
      return prev.concat(arr.filter(a => !Array.isArray(a.value) || a.value.length > 0));
    }, [] as QueryFilter[]);
  }

  private extractData<T>() {
    return map((res: Res<T>) => res.data);
  }
}
