import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { Package, ServicesUrls } from '../../../util';

@Injectable({
  providedIn: 'root',
})
export class LicensesService {
  urls: ServicesUrls = {
    fileService: `${environment.fileServiceUrl}/v1/licenses?type=json`,
    backendService: `${environment.apiUrl}/v1/licenses?type=json`,
    datapointService: `${environment.datapointServiceUrl}/v1/licenses?type=json`,
    aclService: `${environment.aclServiceUrl}/v1/licenses?type=json`,
    frontend: `assets/licenses/frontend-licenses.json`,
  };

  constructor(private http: HttpClient) {}

  getLicenses(serviceType: keyof ServicesUrls) {
    const url = this.urls[serviceType];
    return this.http.get<Package>(url);
  }
}
