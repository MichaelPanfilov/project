import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  CreateInstructionDto,
  InstructionDto,
  PaginationParams,
  PagingResponseMeta,
  ParsedFilter,
  Sorting,
} from '@common';
import { CondOperator, QuerySortOperator } from '@nestjsx/crud-request';
import { Observable, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';
import { Stringified } from 'src/app/util';
import { environment } from 'src/environments/environment';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class InstructionService extends ApiService {
  private readonly url = `${environment.apiUrl}/v1/work-instructions`;

  constructor(http: HttpClient) {
    super(http);
  }

  private transformDates(dto: Stringified<InstructionDto>): InstructionDto {
    return {
      ...dto,
      createdAt: new Date(dto.createdAt),
      updatedAt: new Date(dto.updatedAt),
    } as InstructionDto;
  }

  private mapInstruction(): OperatorFunction<Stringified<InstructionDto>, InstructionDto> {
    return map((task: Stringified<InstructionDto>) => this.transformDates(task));
  }

  getInstructions(
    filter: ParsedFilter<InstructionDto>,
    pageParams: PaginationParams = {},
    sortParams?: Sorting,
  ): Observable<{ instructions: InstructionDto[]; meta: PagingResponseMeta }> {
    const { links, ...rest } = filter;
    const linksFilter = this.createRefIdFilter(links);
    const query = this.qb
      .create({
        filter: [...this.buildFilter(rest), ...linksFilter],
        join: links ? ['links'] : undefined,
        ...pageParams,
        sort: sortParams
          ? { field: sortParams[0], order: sortParams[1].toUpperCase() as QuerySortOperator }
          : undefined,
      })
      .query();

    return this.getManyWithMeta<InstructionDto>(`${this.url}/?${query}`).pipe(
      map(res => ({
        instructions: res.data.map(task => this.transformDates(task)),
        meta: res.meta,
      })),
    );
  }

  createInstruction(instruction: Omit<CreateInstructionDto, 'links'>): Observable<InstructionDto> {
    return this.post<InstructionDto>(this.url, instruction).pipe(this.mapInstruction());
  }

  linkInstruction(instructionId: string, links: string[]): Observable<InstructionDto> {
    return this.post<InstructionDto>(`${this.url}/${instructionId}/link`, links).pipe(
      this.mapInstruction(),
    );
  }

  unlinkInstruction(instructionId: string, links: string[]): Observable<InstructionDto> {
    return this.post<InstructionDto>(`${this.url}/${instructionId}/unlink`, links).pipe(
      this.mapInstruction(),
    );
  }

  updateInstruction(
    instructionId: string,
    changes: Partial<InstructionDto>,
  ): Observable<InstructionDto> {
    return this.put<InstructionDto>(`${this.url}/${instructionId}`, changes).pipe(
      this.mapInstruction(),
    );
  }

  getInstructionQrCode(instructionId: string): Observable<string> {
    return this.getOne<string>(`${this.url}/${instructionId}/qr-code`);
  }

  deleteInstruction(instructionId: string): Observable<void> {
    return this.delete(`${this.url}/${instructionId}`);
  }

  private createRefIdFilter(links?: ParsedFilter<InstructionDto>['links']) {
    if (!links || (Array.isArray(links) && !links.length)) {
      return [];
    }

    const value = Array.isArray(links) ? links.map(l => l.value) : links.value;
    if (!value.length) {
      return [];
    }

    return [
      {
        value,
        field: 'links.refId',
        operator: CondOperator.IN,
      },
    ];
  }
}
