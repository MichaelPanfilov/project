import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  CreateUserDto,
  PaginationParams,
  PagingResponseMeta,
  ParsedFilter,
  Sorting,
  UpdateUserDto,
  UserDto,
} from '@common';
import { QuerySortOperator } from '@nestjsx/crud-request';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class UserService extends ApiService {
  private readonly url = `${environment.apiUrl}/v1/users`;

  constructor(http: HttpClient) {
    super(http);
  }

  getUserById(userId: string): Observable<UserDto> {
    return this.getOne<UserDto>(`${this.url}/${userId}`);
  }

  getUserByAclId(aclId: string): Observable<UserDto> {
    return this.getOne<UserDto>(`${this.url}/acl/${aclId}`);
  }

  getUsersByAclIds(aclIds: string[]): Observable<UserDto[]> {
    const query = this.qb.create({ search: { $or: aclIds.map(aclId => ({ aclId })) } }).query();
    return this.getMany<UserDto>(`${this.url}/?${query}`);
  }

  getUsers(
    filter: ParsedFilter<UserDto>,
    pageParams: PaginationParams = {},
    sortParams?: Sorting,
  ): Observable<{ users: UserDto[]; meta: PagingResponseMeta }> {
    const query = this.qb
      .create({
        filter: this.buildFilter(filter),
        ...pageParams,
        sort: sortParams
          ? { field: sortParams[0], order: sortParams[1].toUpperCase() as QuerySortOperator }
          : undefined,
      })
      .query();

    return this.getManyWithMeta<UserDto>(`${this.url}/?${query}`).pipe(
      map(res => ({ users: res.data, meta: res.meta })),
    );
  }

  createUser(user: CreateUserDto): Observable<UserDto> {
    return this.post<UserDto>(this.url, user);
  }

  updateUser(user: Partial<UpdateUserDto>, userId: string): Observable<UserDto> {
    return this.put<UserDto>(`${this.url}/${userId}`, user);
  }

  deleteUser(userId: string): Observable<void> {
    return this.delete(`${this.url}/${userId}`);
  }
}
