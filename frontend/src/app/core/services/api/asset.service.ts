import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AssetDto,
  AssetType,
  BaseAssetTree,
  FactoryDto,
  FactoryTreeDto,
  LineDto,
  MachineDto,
  normalizePositions,
  sortTrees,
} from '@common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class AssetService extends ApiService {
  private readonly url = environment.apiUrl;

  constructor(http: HttpClient) {
    super(http);
  }

  getAsset(assetId: string): Observable<AssetDto> {
    return this.getOne<AssetDto>(`${this.url}/v1/assets/${assetId}`);
  }

  getAssetParent(assetId: string): Observable<AssetDto | undefined> {
    return this.getOne<AssetDto | undefined>(`${this.url}/v1/assets/${assetId}/parent`);
  }

  setAssetParent(assetId: string, parentId: string): Observable<void> {
    return this.post<void>(`${this.url}/v1/assets/${assetId}/parent/${parentId}`, {});
  }

  getAssetChildren<T extends AssetDto>(assetId: string): Observable<T[]> {
    // tslint:disable-next-line
    return this.getMany<any>(`${this.url}/v1/assets/${assetId}/children`);
  }

  getAssetTrees(): Observable<FactoryTreeDto[]> {
    return this.getMany<BaseAssetTree>(`${this.url}/v1/assets/trees`).pipe(
      map(trees => sortTrees(trees)),
      map(trees => normalizePositions(trees) as FactoryTreeDto[]),
    );
  }

  getFactories(): Observable<FactoryDto[]> {
    return this.getMany<FactoryDto>(`${this.url}/v1/factories`);
  }

  getLines(): Observable<LineDto[]> {
    return this.getMany<LineDto>(`${this.url}/v1/lines`);
  }

  getMachines(): Observable<MachineDto[]> {
    return this.getMany<MachineDto>(`${this.url}/v1/machines`);
  }

  findParent(assetId: string): Observable<LineDto | FactoryDto> {
    return this.getOne<LineDto | FactoryDto>(`${this.url}/v1/assets/${assetId}/parent`);
  }

  updateAsset(asset: Partial<AssetDto>): Observable<AssetDto> {
    // TODO: This might work with just UPDATE: /assets/:assetId
    const { id, type, ...rest } = asset;
    const url = `${this.getUrlByAssetType(asset.type)}/${id}`;
    return this.put<AssetDto>(url, rest);
  }

  createAsset(asset: Omit<AssetDto & { parent?: string }, 'id'>): Observable<AssetDto> {
    return this.post(this.getUrlByAssetType(asset.type), asset);
  }

  deleteAsset(assetId: string): Observable<void> {
    return this.delete(`${this.url}/v1/assets/${assetId}`);
  }

  private getUrlByAssetType(type?: AssetType) {
    switch (type) {
      case AssetType.MACHINE: {
        return `${this.url}/v1/machines`;
      }
      case AssetType.FACTORY: {
        return `${this.url}/v1/factories`;
      }
      case AssetType.LINE: {
        return `${this.url}/v1/lines`;
      }
      default:
        return `${this.url}/v1/assets`;
    }
  }
}
