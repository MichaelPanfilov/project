import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  DEFAULT_LANG,
  FaultStopDto,
  MappingDto,
  MappingType,
  ProductLossDto,
  SynMachineState,
  TimelineDto,
} from '@common';
import { Dictionary } from '@ngrx/entity';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { Observable, zip } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class DatapointService extends ApiService {
  private readonly datapointsUrl = `${environment.datapointServiceUrl}/v1/datapoints/performance-queries`;
  private readonly mappingsUrl = `${environment.datapointServiceUrl}/v1/mappings`;
  private lang = DEFAULT_LANG;

  constructor(http: HttpClient, private translate: TranslateService) {
    super(http);
    this.translate.onLangChange.subscribe((ev: LangChangeEvent) => (this.lang = ev.lang));
  }

  getDevices(): Observable<string[]> {
    return this.getMany<string>(`${this.datapointsUrl}/devices`);
  }

  getMachinePerformance(deviceId: string, from: Date, to: Date): Observable<TimelineDto[]> {
    return this.getOne<TimelineDto[]>(
      `${this.datapointsUrl}/status-history/${deviceId}?${this.getRangQuery(from, to)}`,
    );
  }

  getOutputCount(deviceId: string, from: Date, to: Date): Observable<number> {
    return this.getOne<number>(
      `${this.datapointsUrl}/part-output-count/${deviceId}?${this.getRangQuery(from, to)}`,
    );
  }

  getTotalOutputCount(deviceIds: string[], from: Date, to: Date): Observable<Dictionary<number>> {
    const observables = deviceIds.map(id =>
      this.getOutputCount(id, from, to).pipe(map(data => ({ data, id }))),
    );
    return zip(...observables).pipe(map(counts => this.mapById<number>(counts)));
  }

  getStatus(id: string): Observable<SynMachineState> {
    return this.getOne<SynMachineState>(`${this.datapointsUrl}/status/${id}`);
  }

  getStates(deviceIds: string[]): Observable<Dictionary<SynMachineState>> {
    const observables = deviceIds.map(id => this.getStatus(id).pipe(map(data => ({ data, id }))));
    return zip(...observables).pipe(map(states => this.mapById(states)));
  }

  getProductLossCount(deviceId: string, from: Date, to: Date): Observable<number> {
    return this.getOne<number>(
      `${this.datapointsUrl}/part-defect-count/${deviceId}?${this.getRangQuery(from, to)}`,
    );
  }

  getProductLoses(deviceId: string, from: Date, to: Date): Observable<ProductLossDto[]> {
    return this.getMany<ProductLossDto>(
      `${this.datapointsUrl}/product-losses/${deviceId}?${this.getRangQuery(from, to, this.lang)}`,
    ).pipe(map(losses => losses.map(loss => ({ ...loss, time: new Date(loss.time) }))));
  }

  getFaultStops(deviceId: string, from: Date, to: Date): Observable<FaultStopDto[]> {
    return this.getMany<FaultStopDto>(
      `${this.datapointsUrl}/fault-stops/${deviceId}?${this.getRangQuery(from, to, this.lang)}`,
    ).pipe(
      map(stops =>
        stops.map(stop => ({ ...stop, from: new Date(stop.from), to: new Date(stop.to) })),
      ),
    );
  }

  getFaultStopCounts(
    deviceId: string,
    from: Date,
    to: Date,
  ): Observable<{ label: string; count: number }[]> {
    return this.getMany<{ label: string; count: number }>(
      `${this.datapointsUrl}/fault-stops/${deviceId}/counts?${this.getRangQuery(
        from,
        to,
        this.lang,
      )}`,
    );
  }

  getPowerOnHours(deviceId: string): Observable<number> {
    return this.getOne<number>(`${this.datapointsUrl}/power-on-time/${deviceId}`);
  }

  getProducingHours(deviceId: string): Observable<number> {
    return this.getOne<number>(`${this.datapointsUrl}/producing-time/${deviceId}`);
  }

  getCycles(deviceId: string): Observable<number> {
    return this.getOne<number>(`${this.datapointsUrl}/cycles/${deviceId}`);
  }

  getMappings(): Observable<MappingDto[]> {
    return this.getMany<MappingDto>(`${this.mappingsUrl}`).pipe(
      map(dto =>
        dto.map(d => ({
          ...d,
          createdAt: new Date(d.createdAt),
          updatedAt: new Date(d.updatedAt),
        })),
      ),
    );
  }

  uploadMapping(device: string, type: MappingType, file: File): Observable<void> {
    const fileForm = new FormData();
    fileForm.set('file', file);

    const endpointMap = {
      [MappingType.REJECT_REASONS]: 'reject-reasons',
      [MappingType.STOP_REASONS]: 'fault-stop-reasons',
    };
    return this.post<void>(`${this.mappingsUrl}/${device}/upload/${endpointMap[type]}`, fileForm);
  }

  private getRangQuery(from: Date, to: Date, lang?: string): string {
    const base = `from=${from.toISOString()}&to=${to.toISOString()}`;
    return lang ? `${base}&lang=${lang}` : base;
  }

  private mapById<T>(items: { data: T; id: string }[]): Dictionary<T> {
    return items.reduce((acc, item) => {
      acc[item.id] = item.data;
      return acc;
    }, {} as Dictionary<T>);
  }
}
