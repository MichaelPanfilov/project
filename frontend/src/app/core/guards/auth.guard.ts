import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { loginPath } from '../navigation';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  private isLoggedIn = false;

  constructor(private router: Router, private authService: AuthService) {
    this.authService.loggedIn$.subscribe(result => (this.isLoggedIn = result));
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.isLoggedIn) {
      return true;
    }
    this.router.navigate(loginPath());
    return false;
  }
}
