import { TimeRange } from '@common';
import { Action, createReducer, on } from '@ngrx/store';

import * as RangeFilterActions from './performance-range-filter.actions';

export const key = 'performanceRangeFilter';

// if value is a number, the length in ms starting from now() the user wants to look back in time.
export interface State {
  value: TimeRange | number;
}

const initialState: State = {
  value: 8 * 60 * 60 * 1000,
};

const performanceRangeFilterReducer = createReducer(
  initialState,
  on(RangeFilterActions.updatePerformanceRangeFilterDates, (state, { from, to }) => ({
    ...state,
    value: { from, to },
  })),
  on(RangeFilterActions.updatePerformanceRangeFilterValue, (state, { value }) =>
    value
      ? {
          ...state,
          value,
        }
      : state,
  ),
);

export function reducer(state: State | undefined, action: Action) {
  return performanceRangeFilterReducer(state, action);
}
