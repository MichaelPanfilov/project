import { isTimeRange } from '@common';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import { key, State } from './performance-range-filter.reducer';

const featureSelector = createFeatureSelector<State>(key);

export const selectIsCustomFilter = createSelector(
  featureSelector,
  ({ value }: State) => typeof value !== 'number',
);

export const selectRangeFilterValue = createSelector(featureSelector, ({ value }: State) =>
  isTimeRange(value) ? value.to.getTime() - value.from.getTime() : value,
);

export const selectRangeFilterDates = createSelector(featureSelector, ({ value }: State) => {
  if (isTimeRange(value)) {
    return { from: value.from, to: value.to };
  } else {
  }
  const to = new Date();
  return { from: new Date(to.getTime() - value), to };
});
