import { TimeRange } from '@common';
import { createAction, props } from '@ngrx/store';

export const updatePerformanceRangeFilterDates = createAction(
  '[Range Filter] update value',
  props<TimeRange>(),
);

export const updatePerformanceRangeFilterValue = createAction(
  '[Range Filter] update value',
  props<{ value: number }>(),
);
