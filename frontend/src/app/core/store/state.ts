import * as Asset from './asset';
import * as Document from './document';
import * as Image from './image';
import * as Instruction from './instruction';
import * as MachineTypeTag from './machine-type-tag';
import * as PerformanceRangeFilter from './performance-range-filter';
import * as Settings from './settings';
import * as Task from './task';
import * as User from './user';

export interface State {
  [Asset.key]: Asset.State;
  [PerformanceRangeFilter.key]: PerformanceRangeFilter.State;
  [User.key]: User.State;
  [Task.key]: Task.State;
  [Document.key]: Document.State;
  [Instruction.key]: Instruction.State;
  [Settings.key]: Settings.State;
  [Image.key]: Image.State;
  [MachineTypeTag.key]: MachineTypeTag.State;
}
