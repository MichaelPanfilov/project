import { Action, ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';

import { resetStore } from './actions';
import * as Asset from './asset';
import * as Document from './document';
import * as Image from './image';
import * as Instruction from './instruction';
import * as MachineTypeTag from './machine-type-tag';
import * as PerformanceRangeFilter from './performance-range-filter';
import * as Settings from './settings';
import { State } from './state';
import * as Task from './task';
import * as User from './user';

export const appReducers: ActionReducerMap<State> = {
  performanceRangeFilter: PerformanceRangeFilter.reducer,
  assets: Asset.reducer,
  users: User.reducer,
  tasks: Task.reducer,
  documents: Document.reducer,
  instructions: Instruction.reducer,
  settings: Settings.reducer,
  images: Image.reducer,
  machineTypeTags: MachineTypeTag.reducer,
};

export function resetReducer(reducer: ActionReducer<State, Action>) {
  return (state: State, action: Action) => {
    return reducer(action.type === resetStore.type ? undefined : state, action);
  };
}

export const metaReducers: MetaReducer<State>[] = [resetReducer as MetaReducer];
