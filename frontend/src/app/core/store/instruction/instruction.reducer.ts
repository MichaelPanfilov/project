import { InstructionDto } from '@common';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import { removeTable, TableState, upsertManyAndTable } from '../util';

import * as InstructionActions from './instruction.actions';

export const key = 'instructions';

export interface State extends EntityState<InstructionDto>, TableState<InstructionDto> {}

export const adapter: EntityAdapter<InstructionDto> = createEntityAdapter<InstructionDto>();

export const initialState: State = adapter.getInitialState({
  tables: {},
  tableIds: [],
});

const instructionReducer = createReducer(
  initialState,
  on(InstructionActions.loadedInstructions, (state, { instructions }) =>
    adapter.upsertMany(instructions, state),
  ),
  on(InstructionActions.createdInstruction, (state, { instruction }) =>
    adapter.addOne(instruction, state),
  ),
  on(InstructionActions.deletedInstruction, (state, { instructionId }) =>
    adapter.removeOne(instructionId, state),
  ),
  on(InstructionActions.updatedInstruction, (state, { instruction }) =>
    adapter.updateOne({ id: instruction.id, changes: instruction }, state),
  ),
  on(
    InstructionActions.linkedToTask,
    InstructionActions.unlinkedFromTask,
    (state, { instructions }) =>
      adapter.updateMany(
        instructions.map(({ id, links }) => ({ id, changes: { links } })),
        state,
      ),
  ),
  // table
  on(
    InstructionActions.loadedInstructionTable,
    (state, { tableId, instructions, type, hideLoadingAction, filter, paging, sorting }) =>
      upsertManyAndTable(
        adapter.upsertMany,
        state,
        {
          items: instructions,
          filter: filter || {},
          paging: paging || {},
          sorting,
        },
        tableId,
      ),
  ),
  on(InstructionActions.removeInstructionTable, (state, { tableId }) =>
    removeTable(tableId, state),
  ),
);

export function reducer(state: State | undefined, action: Action) {
  return instructionReducer(state, action);
}

export const { selectAll } = adapter.getSelectors();
