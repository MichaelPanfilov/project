import { InstructionDto, ParsedFilter } from '@common';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import { createTableSelectors } from '../util';

import { key, selectAll, State } from './instruction.reducer';

const featureSelector = createFeatureSelector<State>(key);

export const selectInstructions = createSelector(featureSelector, selectAll);

export const selectInstructionsByRefId = (id: string) =>
  createSelector(featureSelector, state =>
    selectAll(state).filter(({ links }) => links.some(refId => refId === id)),
  );

export const { selectTableItems, selectTableMeta } = createTableSelectors<
  InstructionDto,
  ParsedFilter<InstructionDto>,
  State
>(featureSelector);
