import { Injectable } from '@angular/core';
import { InstructionDto } from '@common';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { forkJoin, of } from 'rxjs';
import { catchError, exhaustMap, map, mergeMap, switchMap, take } from 'rxjs/operators';

import { InstructionService } from '../../services/api/instruction.service';
import { State } from '../state';

import * as InstructionActions from './instruction.actions';
import * as InstructionSelectors from './instruction.selectors';

@Injectable()
export class InstructionEffects {
  private link(instruction: InstructionDto, links?: string[]) {
    if (!links || !links.length) {
      return of(instruction);
    }
    return this.instructionService.linkInstruction(instruction.id, links);
  }

  private unlink(instruction: InstructionDto, links?: string[]) {
    if (!links || !links.length) {
      return of(instruction);
    }
    return this.instructionService.unlinkInstruction(instruction.id, links);
  }

  loadInstructions = createEffect(() =>
    this.actions$.pipe(
      ofType(InstructionActions.loadInstructions),
      mergeMap(({ filter }) =>
        this.instructionService.getInstructions(filter || {}).pipe(
          map(({ instructions }) => InstructionActions.loadedInstructions({ instructions })),
          catchError(error => of(InstructionActions.failedLoadingInstructions({ error }))),
        ),
      ),
    ),
  );

  createInstruction = createEffect(() =>
    this.actions$.pipe(
      ofType(InstructionActions.createInstruction),
      exhaustMap(({ instruction, links }) =>
        this.instructionService.createInstruction(instruction).pipe(
          switchMap(created => this.link(created, links)),
          switchMap(created => [
            InstructionActions.createdInstruction({ instruction: created }),
            InstructionActions.loadInstructionTable({}),
          ]),
          catchError(error => of(InstructionActions.failedCreatingInstruction({ error }))),
        ),
      ),
    ),
  );

  updateInstruction = createEffect(() =>
    this.actions$.pipe(
      ofType(InstructionActions.updateInstruction),
      exhaustMap(({ instructionId, changes, links, removedLinks }) =>
        this.instructionService.updateInstruction(instructionId, changes).pipe(
          switchMap(updated => this.link(updated, links)),
          switchMap(updated => this.unlink(updated, removedLinks)),
          switchMap(instruction => [
            InstructionActions.updatedInstruction({ instruction }),
            InstructionActions.loadInstructionTable({}),
          ]),
          catchError(error => of(InstructionActions.failedUpdatingInstruction({ error }))),
        ),
      ),
    ),
  );

  deleteInstruction = createEffect(() =>
    this.actions$.pipe(
      ofType(InstructionActions.deleteInstruction),
      exhaustMap(({ instructionId }) =>
        this.instructionService.deleteInstruction(instructionId).pipe(
          switchMap(() => [
            InstructionActions.deletedInstruction({ instructionId }),
            InstructionActions.loadInstructionTable({}),
          ]),
          catchError(error => of(InstructionActions.failedDeletingInstruction({ error }))),
        ),
      ),
    ),
  );

  linkToTask = createEffect(() =>
    this.actions$.pipe(
      ofType(InstructionActions.linkToTask),
      exhaustMap(({ instructions, taskId }) =>
        forkJoin(
          instructions.map(({ id }) => this.instructionService.linkInstruction(id, [taskId])),
        ).pipe(
          map(updatedInstructions =>
            InstructionActions.linkedToTask({ instructions: updatedInstructions }),
          ),
          catchError(error => of(InstructionActions.failedLinkingToTask({ error }))),
        ),
      ),
    ),
  );

  unlinkFromTask = createEffect(() =>
    this.actions$.pipe(
      ofType(InstructionActions.unlinkFromTask),
      exhaustMap(({ instructions, taskId }) =>
        forkJoin(
          instructions.map(({ id }) => this.instructionService.unlinkInstruction(id, [taskId])),
        ).pipe(
          map(updatedInstructions =>
            InstructionActions.unlinkedFromTask({ instructions: updatedInstructions }),
          ),
          catchError(error => of(InstructionActions.failedUnlinkingFromTask({ error }))),
        ),
      ),
    ),
  );

  loadInstructionTable = createEffect(() =>
    this.actions$.pipe(
      ofType(InstructionActions.loadInstructionTable),
      switchMap(({ tableId, filter, paging, sorting }) =>
        this.store.select(InstructionSelectors.selectTableMeta(tableId)).pipe(
          take(1),
          switchMap(meta =>
            this.instructionService
              .getInstructions(
                filter || meta.filter,
                paging || meta.paging,
                sorting || meta.sorting,
              )
              .pipe(
                map(res =>
                  InstructionActions.loadedInstructionTable({
                    tableId,
                    instructions: res.instructions,
                    filter: filter || meta.filter,
                    paging: {
                      ...res.meta,
                      limit: paging ? paging.limit || meta.paging.limit : meta.paging.limit,
                    },
                    sorting: sorting || meta.sorting,
                  }),
                ),
                catchError(error =>
                  of(InstructionActions.failedLoadingInstructionTable({ error })),
                ),
              ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private store: Store<State>,
    private instructionService: InstructionService,
  ) {}
}
