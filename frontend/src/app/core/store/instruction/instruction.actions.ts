import {
  CreateInstructionDto,
  InstructionDto,
  PaginationParams,
  PagingResponseMeta,
  ParsedFilter,
  Sorting,
} from '@common';
import { CondOperator } from '@nestjsx/crud-request/lib';
import { createAction, props } from '@ngrx/store';

import {
  propsWithBlocking,
  propsWithBlockingComplete,
  propsWithLoading,
  propsWithLoadingComplete,
} from '../util';

// load instructions

export const loadInstructions = createAction(
  '[Instruction API] load instructions',
  propsWithLoading<{ filter?: ParsedFilter<InstructionDto> }>(),
);

export const loadedInstructions = createAction(
  '[Instruction API] loaded instructions',
  propsWithLoadingComplete<{ instructions: InstructionDto[] }>(loadInstructions),
);

export const failedLoadingInstructions = createAction(
  '[Instruction API] failed loading instructions',
  propsWithLoadingComplete<{ error: Error }>(loadInstructions),
);

// load tasks by ids

export const loadInstructionsByIds = (ids: string[]) =>
  loadInstructions({
    filter: ids.length
      ? {
          id: { value: ids, operator: CondOperator.IN },
        }
      : {},
  });

// load instruction by refIds

export const loadInstructionsByRefIds = (ids?: string[]) =>
  loadInstructions({ filter: { links: { value: ids, operator: CondOperator.IN } } });

// create instruction

export const createInstruction = createAction(
  '[Instruction API] create instruction',
  propsWithBlocking<{ instruction: Omit<CreateInstructionDto, 'links'>; links: string[] }>(),
);

export const createdInstruction = createAction(
  '[Instruction API] created instruction',
  propsWithBlockingComplete<{ instruction: InstructionDto }>(createInstruction, {
    success: true,
    message: 'WORK_INSTRUCTION.ACTION.CREATED',
  }),
);

export const failedCreatingInstruction = createAction(
  '[Instruction API] failed creating instruction',
  propsWithBlockingComplete<{ error: Error }>(createInstruction),
);

// update instruction

export const updateInstruction = createAction(
  '[Instruction API] update instruction',
  propsWithBlocking<{
    instructionId: string;
    changes: Partial<InstructionDto>;
    links: string[];
    removedLinks: string[];
  }>(),
);

export const updatedInstruction = createAction(
  '[Instruction API] updated instruction',
  propsWithBlockingComplete<{ instruction: InstructionDto }>(updateInstruction, {
    success: true,
    message: 'WORK_INSTRUCTION.ACTION.UPDATED',
  }),
);

export const failedUpdatingInstruction = createAction(
  '[Instruction API] failed updating instruction',
  propsWithBlockingComplete<{ error: Error }>(updateInstruction),
);

// batch link instructions to task

export const linkToTask = createAction(
  '[Instruction API] link instruction to task',
  propsWithBlocking<{ instructions: InstructionDto[]; taskId: string }>(),
);

export const linkedToTask = createAction(
  '[Instruction API] linked instruction to task',
  propsWithBlockingComplete<{ instructions: InstructionDto[] }>(linkToTask),
);

export const failedLinkingToTask = createAction(
  '[Instruction API] failed linking to task',
  propsWithBlockingComplete<{ error: Error }>(linkToTask),
);

// batch unlink instructions from task

export const unlinkFromTask = createAction(
  '[Instruction API] unlink instruction from task',
  propsWithBlocking<{ instructions: InstructionDto[]; taskId: string }>(),
);

export const unlinkedFromTask = createAction(
  '[Instruction API] unlinked instruction from task',
  propsWithBlockingComplete<{ instructions: InstructionDto[] }>(unlinkFromTask),
);

export const failedUnlinkingFromTask = createAction(
  '[Instruction API] failed unlinking from task',
  propsWithBlockingComplete<{ error: Error }>(unlinkFromTask),
);

// delete instruction

export const deleteInstruction = createAction(
  '[Instruction API] delete instruction',
  propsWithBlocking<{ instructionId: string }>(),
);

export const deletedInstruction = createAction(
  '[Instruction API] deleted instruction',
  propsWithBlockingComplete<{ instructionId: string }>(deleteInstruction, {
    success: true,
    message: 'WORK_INSTRUCTION.ACTION.DELETED',
  }),
);

export const failedDeletingInstruction = createAction(
  '[Instruction API] failed deleting instruction',
  propsWithBlockingComplete<{ error: Error }>(deleteInstruction),
);

// table

export const loadInstructionTable = createAction(
  '[Instruction API] load instruction table',
  propsWithLoading<{
    tableId?: string;
    filter?: ParsedFilter<InstructionDto>;
    paging?: PaginationParams;
    sorting?: Sorting;
  }>(),
);

export const loadedInstructionTable = createAction(
  '[Instruction API] loaded instruction table',
  propsWithLoadingComplete<{
    instructions: InstructionDto[];
    tableId?: string;
    filter?: ParsedFilter<InstructionDto>;
    paging?: Partial<PagingResponseMeta & { limit: number }>;
    sorting?: Sorting;
  }>(loadInstructionTable),
);

export const failedLoadingInstructionTable = createAction(
  '[Instruction API] failed loading instruction table',
  propsWithLoadingComplete<{ error: Error }>(loadInstructionTable),
);

export const removeInstructionTable = createAction(
  '[Instruction API] remove instruction table',
  props<{ tableId: string }>(),
);
