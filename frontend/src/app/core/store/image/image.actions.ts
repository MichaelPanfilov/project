import { ASSET_FILE_TAG, FileDto, ImageDto, USER_AVATAR_TAG } from '@common';
import { createAction, props } from '@ngrx/store';

import { propsWithLoading, propsWithLoadingComplete } from '../util';

// Load Images Data

export const IMG_TAGS = [USER_AVATAR_TAG, ASSET_FILE_TAG] as const;
export type ImageTag = typeof IMG_TAGS[number];

export const loadImagesData = createAction(
  '[Image API] load images data',
  propsWithLoading<{ tag: ImageTag }>(),
);

export const loadedImagesData = createAction(
  '[Image API] loaded images data',
  propsWithLoadingComplete<{ imagesData: ImageDto[] }>(loadImagesData),
);

export const failedLoadingImagesData = createAction(
  '[Image API] failed loading files',
  propsWithLoadingComplete<{ error: Error }>(loadImagesData),
);

export const loadImageDataByRefId = createAction(
  '[Image API] load image data',
  propsWithLoading<{ refId: string }>(),
);

export const loadImageData = createAction(
  '[Image API] load image data',
  propsWithLoading<{ id: string }>(),
);

export const loadedImageData = createAction(
  '[Image API] loaded image data',
  propsWithLoadingComplete<{ imageData?: ImageDto }>(loadImageData),
);

export const failedLoadingImageData = createAction(
  '[Image API] failed loading file',
  propsWithLoadingComplete<{ error: Error }>(loadImageDataByRefId),
);

// Load Image

export const loadImageFile = createAction(
  '[Image API] load image file',
  propsWithLoading<{ imageData: ImageDto }>(),
);

export const loadedImageFile = createAction(
  '[Image API] loaded image file',
  propsWithLoadingComplete<{ imageData: ImageDto; file: Blob }>(loadImageFile),
);

export const failedLoadingImageFile = createAction(
  '[Image API] failed loading image file',
  propsWithLoadingComplete<{ error: Error }>(loadImageFile),
);

// Update Image

export const updateImageFile = createAction(
  '[Image API] update image file',
  propsWithLoading<{ prevImageId?: string; imageData: ImageDto }>(),
);

export const updatedImageFile = createAction(
  '[Image API] updated image file',
  propsWithLoadingComplete<{ prevImageId?: string; imageData: ImageDto; file: Blob }>(
    updateImageFile,
  ),
);

export const failedUpdatingImageFile = createAction(
  '[Image API] failed updating image file',
  propsWithLoadingComplete<{ error: Error }>(updateImageFile),
);

// Upload Image

export const uploadImage = createAction(
  '[Image API] load images data',
  propsWithLoading<{ image: FileDto }>(),
);

// Remove Image

export const removedFile = createAction(
  '[Image API] remove image data',
  props<{ imageId?: string }>(),
);
