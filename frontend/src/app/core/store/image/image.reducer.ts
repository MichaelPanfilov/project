import { ImageDto } from '@common';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import {
  loadedImageData,
  loadedImageFile,
  loadedImagesData,
  removedFile,
  updatedImageFile,
} from './image.actions';

export const key = 'images';

export interface State extends EntityState<ImageDto> {}

export const adapter: EntityAdapter<ImageDto> = createEntityAdapter<ImageDto>();

export const initialState: State = adapter.getInitialState([]);

const taskReducer = createReducer(
  initialState,
  on(loadedImagesData, (state, { imagesData }) => adapter.upsertMany(imagesData, state)),
  on(loadedImageData, (state, { imageData }) =>
    imageData ? adapter.upsertOne(imageData, state) : state,
  ),
  on(loadedImageFile, (state, { imageData, file }) => {
    const existing = state.entities[imageData.id];
    if (imageData && imageData.id && existing && existing.image) {
      URL.revokeObjectURL(existing.image);
    }
    const url = URL.createObjectURL(file);
    return adapter.updateOne({ id: imageData.id, changes: { image: url } }, state);
  }),
  on(removedFile, (state, { imageId }) => {
    if (imageId) {
      const existing = state.entities[imageId];
      if (existing && existing.image) {
        URL.revokeObjectURL(existing.image);
      }
      return adapter.removeOne(imageId, state);
    }
    return state;
  }),
  on(updatedImageFile, (state, { imageData, prevImageId, file }) => {
    if (prevImageId) {
      const existing = state.entities[prevImageId];
      if (existing) {
        adapter.removeOne(prevImageId, state);
        if (existing.image) {
          URL.revokeObjectURL(existing.image);
        }
      }
    }
    const newExisting = state.entities[imageData.id];
    if (newExisting && newExisting.image) {
      URL.revokeObjectURL(newExisting.image);
    }
    const url = URL.createObjectURL(file);
    return adapter.upsertOne({ ...imageData, image: url }, state);
  }),
);

export function reducer(state: State | undefined, action: Action) {
  return taskReducer(state, action);
}

export const { selectAll, selectEntities } = adapter.getSelectors();
