import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { FileService } from '../../services/api';

import * as ImagesActions from './image.actions';

@Injectable()
export class ImageEffects {
  loadImagesData = createEffect(() =>
    this.actions$.pipe(
      ofType(ImagesActions.loadImagesData),
      mergeMap(({ tag }) =>
        this.fileService.getFilesByTag(tag).pipe(
          map(
            imagesData => ImagesActions.loadedImagesData({ imagesData }),
            catchError(error => of(ImagesActions.failedLoadingImagesData({ error }))),
          ),
        ),
      ),
    ),
  );

  loadImageDataByRefId = createEffect(() =>
    this.actions$.pipe(
      ofType(ImagesActions.loadImageDataByRefId),
      mergeMap(({ refId }) =>
        this.fileService.getFileByFilter(refId).pipe(
          map(
            imageData => ImagesActions.loadedImageData({ imageData }),
            catchError(error => of(ImagesActions.failedLoadingImageData({ error }))),
          ),
        ),
      ),
    ),
  );

  loadImageData = createEffect(() =>
    this.actions$.pipe(
      ofType(ImagesActions.loadImageData),
      mergeMap(({ id }) =>
        this.fileService.getFile(id).pipe(
          map(
            imageData => ImagesActions.loadedImageData({ imageData }),
            catchError(error => of(ImagesActions.failedLoadingImageData({ error }))),
          ),
        ),
      ),
    ),
  );

  loadImageFile = createEffect(() =>
    this.actions$.pipe(
      ofType(ImagesActions.loadImageFile),
      mergeMap(({ imageData }) =>
        this.http.get(imageData.viewUrl, { responseType: 'blob' }).pipe(
          map(
            file => ImagesActions.loadedImageFile({ imageData, file }),
            catchError(error => of(ImagesActions.failedLoadingImageFile({ error }))),
          ),
        ),
      ),
    ),
  );

  updateImageFile = createEffect(() =>
    this.actions$.pipe(
      ofType(ImagesActions.updateImageFile),
      mergeMap(({ imageData, prevImageId }) =>
        this.http.get(imageData.viewUrl, { responseType: 'blob' }).pipe(
          map(
            file => ImagesActions.updatedImageFile({ prevImageId, imageData, file }),
            catchError(error => of(ImagesActions.failedUpdatingImageFile({ error }))),
          ),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private fileService: FileService,
    private http: HttpClient,
  ) {}
}
