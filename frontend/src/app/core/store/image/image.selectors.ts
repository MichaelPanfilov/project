import { createFeatureSelector, createSelector } from '@ngrx/store';

import { ImageTag } from './image.actions';
import { key, selectAll, State } from './image.reducer';

const featureSelector = createFeatureSelector<State>(key);

export const selectTasks = createSelector(featureSelector, selectAll);

export const selectImageById = (id: string) =>
  createSelector(featureSelector, state => selectAll(state).find(image => image.id === id));

export const selectImagesByType = (tag: ImageTag) =>
  createSelector(featureSelector, state =>
    selectAll(state).filter(({ tags }) => tags && tags.includes(tag)),
  );

export const selectImageByRefId = (refId: string) =>
  createSelector(featureSelector, state =>
    selectAll(state).find(({ refIds }) => refIds && refIds.includes(refId)),
  );
