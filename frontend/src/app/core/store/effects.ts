import { AssetEffects, AssetTreeEffect, LineEffects, MachineEffects } from './asset';
import { DocumentEffects } from './document';
import { ImageEffects } from './image';
import { InstructionEffects } from './instruction';
import { MachineTypeTagEffects } from './machine-type-tag';
import { PerformanceRangeFilterEffects } from './performance-range-filter';
import { SettingsEffects } from './settings';
import { TaskEffects } from './task';
import { UserEffects } from './user';

export const appEffects = [
  PerformanceRangeFilterEffects,
  UserEffects,
  TaskEffects,
  LineEffects,
  MachineEffects,
  AssetTreeEffect,
  AssetEffects,
  DocumentEffects,
  InstructionEffects,
  SettingsEffects,
  ImageEffects,
  MachineTypeTagEffects,
];
