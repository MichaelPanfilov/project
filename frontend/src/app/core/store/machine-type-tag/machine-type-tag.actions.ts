import { MachineTypeTag } from '@common';
import { createAction } from '@ngrx/store';

import {
  propsWithBlocking,
  propsWithBlockingComplete,
  propsWithLoading,
  propsWithLoadingComplete,
} from '../util';

// load machine type tags
export const loadMachineTypeTags = createAction(
  '[Machine Type Tag API] load tags',
  propsWithLoading<{}>(),
);

export const loadedMachineTypeTags = createAction(
  '[Machine Type Tag API] loaded tags',
  propsWithLoadingComplete<{ machineTypeTags: string[] }>(loadMachineTypeTags),
);

export const failedLoadingMachineTypeTags = createAction(
  '[Machine Type Tag API] failed loading tags',
  propsWithLoadingComplete<{ error: Error }>(loadMachineTypeTags),
);

// create machine type tag

export const createMachineTypeTag = createAction(
  '[Machine Type Tag API] create tag',
  propsWithBlocking<{ name: string }>(),
);

export const createdMachineTypeTag = createAction(
  '[Machine Type Tag API] created tag',
  propsWithBlockingComplete<{ machineTypeTag: MachineTypeTag }>(createMachineTypeTag),
);

export const failedCreatingMachineTypeTag = createAction(
  '[Machine Type Tag API] failed creating tag',
  propsWithBlockingComplete<{ error: Error }>(createMachineTypeTag),
);
