import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import { uniq } from 'lodash';

import * as MachineTypeTagsActions from './machine-type-tag.actions';
export const key = 'machineTypeTags';

export const adapter: EntityAdapter<string> = createEntityAdapter<string>();

const initialState: string[] = [];

export type State = string[];

const machineTypeTagReducer = createReducer(
  initialState,
  on(MachineTypeTagsActions.loadedMachineTypeTags, (state, { machineTypeTags }) =>
    uniq([...state, ...machineTypeTags]),
  ),
  on(MachineTypeTagsActions.createdMachineTypeTag, (state, { machineTypeTag }) => [
    ...state,
    machineTypeTag.name,
  ]),
);

export function reducer(state: State | undefined, action: Action) {
  return machineTypeTagReducer(state, action);
}
