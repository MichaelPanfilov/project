import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { MachineTypeTagService } from '../../services/api';

import * as MachineTypeTagActions from './machine-type-tag.actions';

@Injectable()
export class MachineTypeTagEffects {
  loadMachineTypeTags = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineTypeTagActions.loadMachineTypeTags),
      mergeMap(() =>
        this.machineTypeTagService.getMachineTypeTags().pipe(
          map(machineTypeTags => MachineTypeTagActions.loadedMachineTypeTags({ machineTypeTags })),
          catchError(error => of(MachineTypeTagActions.failedLoadingMachineTypeTags({ error }))),
        ),
      ),
    ),
  );

  createMachineTypeTags = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineTypeTagActions.createMachineTypeTag),
      mergeMap(({ name }) =>
        this.machineTypeTagService.createMachineTypeTag(name).pipe(
          map(machineTypeTag => MachineTypeTagActions.createdMachineTypeTag({ machineTypeTag })),
          catchError(error => of(MachineTypeTagActions.failedCreatingMachineTypeTag({ error }))),
        ),
      ),
    ),
  );

  constructor(private actions$: Actions, private machineTypeTagService: MachineTypeTagService) {}
}
