import { createFeatureSelector, createSelector } from '@ngrx/store';

import { key, State } from './machine-type-tag.reducer';

const featureSelector = createFeatureSelector<State>(key);

export const selectAll = createSelector(featureSelector, (state: State) => state);
