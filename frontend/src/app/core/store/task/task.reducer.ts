import { TaskDto } from '@common';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import { removeTable, TableState, upsertManyAndTable } from '../util';

import * as TaskActions from './task.actions';

export const key = 'tasks';

export interface State extends EntityState<TaskDto>, TableState<TaskDto> {}

export const adapter: EntityAdapter<TaskDto> = createEntityAdapter<TaskDto>();

export const initialState: State = adapter.getInitialState({
  tables: {},
  tableIds: [],
});

const taskReducer = createReducer(
  initialState,
  on(TaskActions.loadedTasks, (state, { tasks }) => adapter.upsertMany(tasks, state)),
  on(TaskActions.createdTask, TaskActions.clonedTask, (state, { task }) =>
    adapter.addOne(task, state),
  ),
  on(TaskActions.deletedTask, (state, { taskId }) => adapter.removeOne(taskId, state)),
  on(
    TaskActions.updatedTask,
    TaskActions.assignedToAsset,
    TaskActions.markedDoneTask,
    (state, { task }) => {
      // TODO: why is it undefined?
      if (!task) {
        return state;
      }
      return adapter.updateOne({ id: task.id, changes: task }, state);
    },
  ),
  // table
  on(
    TaskActions.loadedTaskTable,
    (state, { tableId, tasks, type, hideLoadingAction, filter, paging, sorting }) =>
      upsertManyAndTable(
        adapter.upsertMany,
        state,
        {
          items: tasks,
          filter: filter || {},
          paging: paging || {},
          sorting,
        },
        tableId,
      ),
  ),
  on(TaskActions.removeTaskTable, (state, { tableId }) => removeTable(tableId, state)),
);

export function reducer(state: State | undefined, action: Action) {
  return taskReducer(state, action);
}

export const { selectAll } = adapter.getSelectors();
