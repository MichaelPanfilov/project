import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, mergeMap, switchMap, take } from 'rxjs/operators';

import { TasksService } from '../../services/api/tasks.service';
import * as DocumentActions from '../document/document.actions';
import * as InstructionActions from '../instruction/instruction.actions';
import { State } from '../state';

import * as TaskActions from './task.actions';
import * as TaskSelectors from './task.selectors';

@Injectable()
export class TaskEffects {
  loadTasks = createEffect(() =>
    this.actions$.pipe(
      ofType(TaskActions.loadTasks),
      mergeMap(({ filter }) =>
        this.taskService.getTasks(filter || {}).pipe(
          map(({ tasks }) => TaskActions.loadedTasks({ tasks })),
          catchError(error => of(TaskActions.failedLoadingTasks({ error }))),
        ),
      ),
    ),
  );

  cloneTask = createEffect(() =>
    this.actions$.pipe(
      ofType(TaskActions.cloneTask),
      exhaustMap(({ taskId }) =>
        this.taskService.cloneTask(taskId).pipe(
          switchMap(task => [
            TaskActions.clonedTask({ task }),
            // TODO: Clone should return both tasks so we dont have to reload all tasks.
            TaskActions.loadTaskTable({}),
            DocumentActions.loadDocumentsByRefIds([task.id]),
            InstructionActions.loadInstructionsByRefIds([task.id]),
          ]),
          catchError(error => of(TaskActions.failedCloningTask({ error }))),
        ),
      ),
    ),
  );

  createTask = createEffect(() =>
    this.actions$.pipe(
      ofType(TaskActions.createTask),
      exhaustMap(({ task }) =>
        this.taskService.createTask(task).pipe(
          switchMap(createdTask => [
            TaskActions.createdTask({ task: createdTask }),
            TaskActions.loadTaskTable({}),
          ]),
          catchError(error => of(TaskActions.failedCreatingTask({ error }))),
        ),
      ),
    ),
  );

  deleteTask = createEffect(() =>
    this.actions$.pipe(
      ofType(TaskActions.deleteTask),
      exhaustMap(({ taskId }) =>
        this.taskService.deleteTask(taskId).pipe(
          switchMap(() => [TaskActions.deletedTask({ taskId }), TaskActions.loadTaskTable({})]),
          catchError(error => of(TaskActions.failedDeletingTask({ error }))),
        ),
      ),
    ),
  );

  updateTask = createEffect(() =>
    this.actions$.pipe(
      ofType(TaskActions.updateTask),
      exhaustMap(({ taskId, changes }) =>
        this.taskService.updateTask(taskId, changes).pipe(
          switchMap(task => [TaskActions.updatedTask({ task }), TaskActions.loadTaskTable({})]),
          catchError(error => of(TaskActions.failedUpdatingTask({ error }))),
        ),
      ),
    ),
  );

  markDoneTask = createEffect(() =>
    this.actions$.pipe(
      ofType(TaskActions.markDoneTask),
      exhaustMap(({ taskId }) =>
        this.taskService.markDoneTask(taskId).pipe(
          switchMap(task => [
            TaskActions.markedDoneTask({ task }),
            // TODO: Mark done should return both tasks so we dont have to reload all tasks.
            TaskActions.loadTaskTable({}),
            // TODO: Why do we load this here?
            DocumentActions.loadDocumentsByRefIds([taskId]),
            InstructionActions.loadInstructionsByRefIds([taskId]),
          ]),
          catchError(error => of(TaskActions.failedMarkDoneTask({ error }))),
        ),
      ),
    ),
  );

  assignToAsset = createEffect(() =>
    this.actions$.pipe(
      ofType(TaskActions.assignToAsset),
      mergeMap(({ taskId, assetId }) =>
        this.taskService.assignAsset(taskId, assetId).pipe(
          switchMap(task => [TaskActions.assignedToAsset({ task }), TaskActions.loadTaskTable({})]),
          catchError(error => of(TaskActions.failedAssigningToAsset({ error }))),
        ),
      ),
    ),
  );

  loadTaskTable = createEffect(() =>
    this.actions$.pipe(
      ofType(TaskActions.loadTaskTable),
      switchMap(({ tableId, filter, paging, sorting }) =>
        this.store.select(TaskSelectors.selectTableMeta(tableId)).pipe(
          take(1),
          switchMap(meta =>
            this.taskService
              .getTasks(filter || meta.filter, paging || meta.paging, sorting || meta.sorting)
              .pipe(
                map(res =>
                  TaskActions.loadedTaskTable({
                    tableId,
                    tasks: res.tasks,
                    filter: filter || meta.filter,
                    paging: {
                      ...res.meta,
                      limit: paging ? paging.limit || meta.paging.limit : meta.paging.limit,
                    },
                    sorting: sorting || meta.sorting,
                  }),
                ),
                catchError(error => of(TaskActions.failedLoadingTaskTable({ error }))),
              ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private store: Store<State>,
    private taskService: TasksService,
  ) {}
}
