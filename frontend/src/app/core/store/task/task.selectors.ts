import { ParsedFilter, TaskDto, TaskStatus } from '@common';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import { createTableSelectors } from '../util';

import { key, selectAll, State } from './task.reducer';

const featureSelector = createFeatureSelector<State>(key);

export const selectTasks = createSelector(featureSelector, selectAll);

export const selectTasksOfAsset = (assetId: string) =>
  createSelector(featureSelector, state =>
    selectAll(state).filter(({ assignedAsset }) => assignedAsset === assetId),
  );

export const selectActiveTasksOfAsset = (assetId: string) =>
  createSelector(featureSelector, state =>
    selectAll(state).filter(
      ({ assignedAsset, status }) => assignedAsset === assetId && status === TaskStatus.ACTIVE,
    ),
  );

export const selectActiveTasksOfAssets = (assetIds: string[]) =>
  createSelector(featureSelector, state =>
    selectAll(state).filter(
      ({ assignedAsset, status }) =>
        assignedAsset && status === TaskStatus.ACTIVE && assetIds.includes(assignedAsset),
    ),
  );

export const { selectTableItems, selectTableMeta } = createTableSelectors<
  TaskDto,
  ParsedFilter<TaskDto>,
  State
>(featureSelector);
