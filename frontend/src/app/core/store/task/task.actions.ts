import {
  PaginationParams,
  PagingResponseMeta,
  ParsedFilter,
  Sorting,
  TaskDto,
  TaskStatus,
} from '@common';
import { CondOperator } from '@nestjsx/crud-request';
import { createAction, props } from '@ngrx/store';

import {
  propsWithBlocking,
  propsWithBlockingComplete,
  propsWithLoading,
  propsWithLoadingComplete,
} from '../util';

// load tasks

export const loadTasks = createAction(
  '[Task API] load tasks',
  propsWithLoading<{ filter?: ParsedFilter<TaskDto> }>(),
);

export const loadedTasks = createAction(
  '[Task API] loaded tasks',
  propsWithLoadingComplete<{ tasks: TaskDto[] }>(loadTasks),
);

export const failedLoadingTasks = createAction(
  '[Task API] failed loading tasks',
  propsWithLoadingComplete<{ error: Error }>(loadTasks),
);

// load tasks by ids

export const loadTasksByIds = (ids: string[]) =>
  loadTasks({
    filter: ids.length
      ? {
          id: { value: ids, operator: CondOperator.IN },
        }
      : {},
  });

// load active tasks of asset

export const loadActiveTasksOfAsset = (assetId: string) =>
  loadTasks({
    filter: {
      assignedAsset: { value: assetId, operator: CondOperator.EQUALS },
      status: { value: TaskStatus.ACTIVE, operator: CondOperator.EQUALS },
    },
  });

// clone task

export const cloneTask = createAction(
  '[Task API] clone task',
  propsWithLoading<{ taskId: string }>(),
);

export const clonedTask = createAction(
  '[Task API] cloned task',
  propsWithLoadingComplete<{ task: TaskDto }>(cloneTask, {
    success: true,
    message: 'TASK.ACTION.COPIED',
  }),
);

export const failedCloningTask = createAction(
  '[Task API] failed cloning task',
  propsWithLoadingComplete<{ error: Error }>(cloneTask),
);

// create task

export const createTask = createAction(
  '[Task API] create task',
  propsWithBlocking<{ task: Partial<TaskDto> }>(),
);

export const createdTask = createAction(
  '[Task API] created task',
  propsWithBlockingComplete<{ task: TaskDto }>(createTask, {
    success: true,
    message: 'TASK.ACTION.CREATED',
  }),
);

export const failedCreatingTask = createAction(
  '[Task API] failed creating task',
  propsWithBlockingComplete<{ error: Error }>(createTask),
);

// update task

export const updateTask = createAction(
  '[Task API] update task',
  propsWithBlocking<{ taskId: string; changes: Partial<TaskDto> }>(),
);

export const updatedTask = createAction(
  '[Task API] updated task',
  propsWithBlockingComplete<{ task: TaskDto }>(updateTask, {
    success: true,
    message: 'TASK.ACTION.UPDATED',
  }),
);

export const failedUpdatingTask = createAction(
  '[Task API] failed updating task',
  propsWithBlockingComplete<{ error: Error }>(updateTask),
);

// mark task done

export const markDoneTask = createAction(
  '[Task API] mark task done',
  propsWithBlocking<{ taskId: string }>(),
);

export const markedDoneTask = createAction(
  '[Task API] mark task done',
  propsWithBlockingComplete<{ task: TaskDto }>(markDoneTask, {
    success: true,
    message: 'TASK.ACTION.RESOLVED',
  }),
);

export const failedMarkDoneTask = createAction(
  '[Task API] failed resolving task',
  propsWithBlockingComplete<{ error: Error }>(markDoneTask),
);

// assign asset

export const assignToAsset = createAction(
  '[Task API] assign to asset',
  propsWithBlocking<{ taskId: string; assetId: string }>(),
);

export const assignedToAsset = createAction(
  '[Task API] assigned to asset',
  propsWithBlockingComplete<{ task: TaskDto }>(assignToAsset),
);

export const failedAssigningToAsset = createAction(
  '[Task API] failed assigning to asset',
  propsWithBlockingComplete<{ error: Error }>(assignToAsset),
);

// delete task

export const deleteTask = createAction(
  '[Task API] delete task',
  propsWithBlocking<{ taskId: string }>(),
);

export const deletedTask = createAction(
  '[Task API] deleted task',
  propsWithBlockingComplete<{ taskId: string }>(deleteTask, {
    success: true,
    message: 'TASK.ACTION.DELETED',
  }),
);

export const failedDeletingTask = createAction(
  '[Task API] failed deleting task',
  propsWithBlockingComplete<{ error: Error }>(deleteTask),
);

// table

export const loadTaskTable = createAction(
  '[Task API] load task table',
  propsWithLoading<{
    tableId?: string;
    filter?: ParsedFilter<TaskDto>;
    paging?: PaginationParams;
    sorting?: Sorting;
  }>(),
);

export const loadedTaskTable = createAction(
  '[Task API] loaded task table',
  propsWithLoadingComplete<{
    tasks: TaskDto[];
    tableId?: string;
    filter?: ParsedFilter<TaskDto>;
    paging?: Partial<PagingResponseMeta & { limit: number }>;
    sorting?: Sorting;
  }>(loadTaskTable),
);

export const failedLoadingTaskTable = createAction(
  '[Task API] failed loading task table',
  propsWithLoadingComplete<{ error: Error }>(loadTaskTable),
);

export const removeTaskTable = createAction(
  '[Task API] remove task table',
  props<{ tableId: string }>(),
);
