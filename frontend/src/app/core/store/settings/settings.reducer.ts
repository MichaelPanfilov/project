import { Action, createReducer, on } from '@ngrx/store';

import * as SettingsActions from './settings.actions';

export const key = 'settings';

export interface State {
  rangeFilter: number;
  loadingActions: Action[];
  blockingActions: Action[];
  uploadProgress: number;
}

export const initialState: State = {
  rangeFilter: 8 * 60 * 60 * 1000,
  loadingActions: [],
  blockingActions: [],
  uploadProgress: 0,
};

const settingsReducer = createReducer(
  initialState,
  on(SettingsActions.showLoading, (state, { action, parallel }) => {
    if (!parallel && !!state.loadingActions.find(a => a.type === action.type)) {
      return state;
    }
    return {
      ...state,
      loadingActions: [action, ...state.loadingActions],
    };
  }),
  on(SettingsActions.hideLoading, (state, { action }) => {
    const index = state.loadingActions.findIndex(a => a.type === action.type);
    const loadingActions = [...state.loadingActions];
    loadingActions.splice(index, 1);
    return { ...state, loadingActions };
  }),
  on(SettingsActions.blockInteraction, (state, { action, parallel }) => {
    if (!parallel && !!state.blockingActions.find(a => a.type === action.type)) {
      return state;
    }
    return {
      ...state,
      blockingActions: [action, ...state.blockingActions],
    };
  }),
  on(SettingsActions.allowInteraction, (state, { action }) => {
    const index = state.blockingActions.findIndex(a => a.type === action.type);
    const blockingActions = [...state.blockingActions];
    blockingActions.splice(index, 1);
    return { ...state, blockingActions };
  }),
  on(SettingsActions.updateFileUploadProgress, (state, { uploadProgress }) => {
    return { ...state, uploadProgress };
  }),
);

export function reducer(state: State | undefined, action: Action) {
  return settingsReducer(state, action);
}
