import { Action, createAction, props } from '@ngrx/store';

export const showLoading = createAction(
  '[Settings] Show loading',
  props<{ action: Action; parallel: boolean }>(),
);

export const hideLoading = createAction('[Settings] Hide loading', props<{ action: Action }>());

export const blockInteraction = createAction(
  '[Settings] Block interaction',
  props<{ action: Action; parallel: boolean }>(),
);

export const allowInteraction = createAction(
  '[Settings] Allow interaction',
  props<{ action: Action }>(),
);

export const updateFileUploadProgress = createAction(
  '[Settings] Update uploading progress',
  props<{ uploadProgress: number }>(),
);
