import { createAction, createFeatureSelector, createSelector } from '@ngrx/store';

import { key, State } from './settings.reducer';

const featureSelector = createFeatureSelector<State>(key);

export const selectLoading = createSelector(
  featureSelector,
  state => state.loadingActions.length > 0,
);

export const selectLoadingByAction = (...actions: ReturnType<typeof createAction>[]) =>
  createSelector(featureSelector, state =>
    [...state.loadingActions, ...state.blockingActions].some(
      a => !!actions.find(({ type }) => a.type === type),
    ),
  );

export const selectBlocking = createSelector(
  featureSelector,
  state => state.blockingActions.length > 0,
);

export const selectUploadStatus = createSelector(featureSelector, state => state.uploadProgress);

export const selectRangeFilter = createSelector(featureSelector, state => state.rangeFilter);
