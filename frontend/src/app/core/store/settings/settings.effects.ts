import { Injectable } from '@angular/core';
import { Actions, createEffect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { filter, map, switchMap, tap } from 'rxjs/operators';

import { NotificationService, NotificationVariants } from '../../services/notification.service';
import {
  ActionWithLoading,
  ALLOW_INTERACTION_ACTION_KEY,
  BLOCK_INTERACTION_KEY,
  BLOCK_INTERACTION_PARALLEL,
  HIDE_LOADING_ACTION_KEY,
  SHOW_LOADING_KEY,
  SHOW_LOADING_PARALLEL,
} from '../util';

import * as SettingsActions from './settings.actions';

@Injectable()
export class SettingsEffects {
  constructor(
    private actions$: Actions,
    private notificationService: NotificationService,
    private translate: TranslateService,
  ) {}

  showLoading$ = createEffect(() =>
    this.actions$.pipe(
      filter((action: ActionWithLoading) => !!action[SHOW_LOADING_KEY]),
      map((action: Action) =>
        SettingsActions.showLoading({ action, parallel: !!action[SHOW_LOADING_PARALLEL] }),
      ),
    ),
  );

  hideLoading$ = createEffect(() =>
    this.actions$.pipe(
      filter((action: ActionWithLoading) => !!action[HIDE_LOADING_ACTION_KEY]),
      map((action: ActionWithLoading) =>
        SettingsActions.hideLoading({ action: action[HIDE_LOADING_ACTION_KEY] as Action }),
      ),
    ),
  );

  blockInteraction$ = createEffect(() =>
    this.actions$.pipe(
      filter((action: ActionWithLoading) => !!action[BLOCK_INTERACTION_KEY]),
      map((action: ActionWithLoading) =>
        SettingsActions.blockInteraction({
          action,
          parallel: !!action[BLOCK_INTERACTION_PARALLEL],
        }),
      ),
    ),
  );

  allowInteraction$ = createEffect(() =>
    this.actions$.pipe(
      filter((action: ActionWithLoading) => !!action[ALLOW_INTERACTION_ACTION_KEY]),
      map((action: ActionWithLoading) =>
        SettingsActions.allowInteraction({
          action: action[ALLOW_INTERACTION_ACTION_KEY] as Action,
        }),
      ),
    ),
  );

  successComplete$ = createEffect(
    () =>
      this.actions$.pipe(
        filter(action => this.isActionWithLoading(action)),
        switchMap((action: ActionWithLoading) => this.translate.get(action.message as string)),
        tap((message: string) =>
          this.notificationService.notify({
            message,
            variant: NotificationVariants.SUCCESS,
          }),
        ),
      ),
    { dispatch: false },
  );

  private isActionWithLoading(action: ActionWithLoading): action is ActionWithLoading {
    const isCompleteAction = [ALLOW_INTERACTION_ACTION_KEY, HIDE_LOADING_ACTION_KEY].some(
      key => !!action[key],
    );
    return !!(isCompleteAction && action.success && action.message);
  }
}
