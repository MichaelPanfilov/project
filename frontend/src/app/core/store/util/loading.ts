import { Action } from '@ngrx/store';

export const SHOW_LOADING_KEY = 'showLoading';
export const SHOW_LOADING_PARALLEL = 'isParallelLoading';
export const HIDE_LOADING_ACTION_KEY = 'hideLoadingAction';
export const BLOCK_INTERACTION_KEY = 'blockInteraction';
export const BLOCK_INTERACTION_PARALLEL = 'isParallelBlocked';
export const ALLOW_INTERACTION_ACTION_KEY = 'allowInteractionAction';

export interface ActionWithLoading extends Action, ActionCompletionOption {
  [SHOW_LOADING_KEY]?: boolean;
  [SHOW_LOADING_PARALLEL]?: boolean;
  [HIDE_LOADING_ACTION_KEY]?: Action;
  [BLOCK_INTERACTION_KEY]?: boolean;
  [BLOCK_INTERACTION_PARALLEL]?: boolean;
  [ALLOW_INTERACTION_ACTION_KEY]?: Action;
}

export interface ActionCompletionOption {
  success?: boolean;
  message?: string;
}

export const propsWithLoading = <T extends object>(parallel = false) => (props: T) => ({
  ...props,
  [SHOW_LOADING_KEY]: true,
  [SHOW_LOADING_PARALLEL]: parallel,
});

export const propsWithLoadingComplete = <T extends object>(
  action: Action,
  options?: ActionCompletionOption,
) => (props: T) => ({
  ...props,
  ...options,
  [HIDE_LOADING_ACTION_KEY]: action,
});

export const propsWithBlocking = <T extends object>(parallel = false) => (props: T) => ({
  ...props,
  [BLOCK_INTERACTION_KEY]: true,
  [BLOCK_INTERACTION_PARALLEL]: parallel,
});

export const propsWithBlockingComplete = <T extends object>(
  action: Action,
  options?: ActionCompletionOption,
) => (props: T) => ({
  ...props,
  ...options,
  [ALLOW_INTERACTION_ACTION_KEY]: action,
});
