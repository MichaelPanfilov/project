import {
  AppUser,
  CreateUserDto,
  FileDto,
  PaginationParams,
  PagingResponseMeta,
  ParsedFilter,
  RoleDto,
  Sorting,
} from '@common';
import { createAction, props } from '@ngrx/store';

import {
  propsWithBlocking,
  propsWithBlockingComplete,
  propsWithLoading,
  propsWithLoadingComplete,
} from '../util';

// fetch profile

export const loadMe = createAction('[User API] load me', propsWithLoading());

export const loadedMe = createAction(
  '[User API] loaded me',
  propsWithLoadingComplete<{ me: AppUser }>(loadMe),
);

export const failedLoadingMe = createAction(
  '[User API] failed loading me',
  propsWithLoadingComplete<{ error: Error }>(loadMe),
);

// Change profile password

export const changePassword = createAction(
  '[User API] change password',
  propsWithBlocking<{ oldPassword: string; newPassword: string }>(),
);

export const changeUserPassword = createAction(
  '[User API] change user password',
  propsWithBlocking<{ userId: string; newPassword: string }>(),
);

export const changedPassword = createAction(
  '[User API] password changed',
  propsWithBlockingComplete(changePassword),
);

export const changedUserPassword = createAction(
  '[User API] user password changed',
  propsWithBlockingComplete(changeUserPassword),
);

export const failedChangingPassword = createAction(
  '[User API] password changing failed',
  propsWithBlockingComplete<{ error: Error }>(changePassword),
);

export const failedChangingUserPassword = createAction(
  '[User API] user password changing failed',
  propsWithBlockingComplete<{ error: Error }>(changeUserPassword),
);

// create user

export const createUser = createAction(
  '[User API] create user',
  propsWithBlocking<{ user: CreateUserDto }>(),
);

export const createdUser = createAction(
  '[User API] created user',
  propsWithBlockingComplete<{ user: AppUser }>(createUser, {
    success: true,
    message: 'USER.ACTION.CREATED',
  }),
);

export const failedCreatingUser = createAction(
  '[User API] failed creating user',
  propsWithBlockingComplete<{ error: Error }>(createUser),
);

// update user

export const updateUser = createAction(
  '[User API] update user',
  propsWithBlocking<{ changes: Partial<AppUser>; userId: string; aclId: string }>(),
);

export const updatedUser = createAction(
  '[User API] updated user',
  propsWithBlockingComplete<{ user: AppUser }>(updateUser, {
    success: true,
    message: 'USER.ACTION.UPDATED',
  }),
);

export const failedUpdatingUser = createAction(
  '[User API] failed updating user',
  propsWithBlockingComplete<{ error: Error }>(updateUser),
);

// delete user

export const deleteUser = createAction(
  '[User API] delete user',
  propsWithBlocking<{ userId: string }>(),
);

export const deletedUser = createAction(
  '[User API] deleted user',
  propsWithBlockingComplete<{ userId: string }>(deleteUser, {
    success: true,
    message: 'USER.ACTION.DELETED',
  }),
);

export const failedDeletingUser = createAction(
  '[User API] failed removing user',
  propsWithBlockingComplete<{ error: Error }>(deleteUser),
);

// fetch roles

export const loadRoles = createAction('[User API] load roles', propsWithLoading());

export const loadedRoles = createAction(
  '[User API] loaded roles',
  propsWithLoadingComplete<{ roles: RoleDto[] }>(loadRoles),
);

export const failedLoadingRoles = createAction(
  '[User API] failed loading roles',
  propsWithLoadingComplete<{ error: Error }>(loadRoles),
);

// update user roles

export const updateRoles = createAction(
  '[User API] update roles',
  propsWithBlocking<{ aclId: string; roleIds: string[] }>(),
);

export const updatedRoles = createAction(
  '[User API] updated roles',
  propsWithBlockingComplete<{ aclId: string; roles: RoleDto[] }>(updateRoles),
);

export const failedUpdatingRoles = createAction(
  '[User API] failed roles updating',
  propsWithBlockingComplete<{ error: Error }>(updateRoles),
);

// avatar

export const uploadUserImg = createAction(
  '[User API] Upload user image',
  propsWithBlocking<{ image: File; prevImageId?: string; user: AppUser }>(),
);

export const uploadedUserImg = createAction(
  '[User API] Uploaded user image',
  propsWithBlockingComplete<{ image: FileDto; prevImageId?: string }>(uploadUserImg),
);

export const failedUploadingUserImg = createAction(
  '[User API] Failed uploading user image',
  propsWithBlockingComplete<{ error: Error }>(uploadUserImg),
);

// table

export const loadUserTable = createAction(
  '[User API] load user table',
  propsWithLoading<{
    tableId?: string;
    filter?: ParsedFilter<AppUser>;
    paging?: PaginationParams;
    sorting?: Sorting;
  }>(),
);

export const loadedUserTable = createAction(
  '[User API] loaded user table',
  propsWithLoadingComplete<{
    users: AppUser[];
    tableId?: string;
    filter?: ParsedFilter<AppUser>;
    paging?: Partial<PagingResponseMeta & { limit: number }>;
    sorting?: Sorting;
  }>(loadUserTable),
);

export const failedLoadingUserTable = createAction(
  '[User API] failed loading user table',
  propsWithLoadingComplete<{ error: Error }>(loadUserTable),
);

export const removeUserTable = createAction(
  '[User API] remove user table',
  props<{ tableId: string }>(),
);
