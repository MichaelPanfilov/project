import { Injectable } from '@angular/core';
import { AppUser, FileDto, UserDto, USER_AVATAR_TAG } from '@common';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { forkJoin, iif, Observable, of } from 'rxjs';
import { catchError, exhaustMap, map, switchMap, take } from 'rxjs/operators';

import { AclService } from '../../services/api/acl.service';
import { FileService } from '../../services/api/file.service';
import { UserService } from '../../services/api/user.service';
import * as ImageActions from '../image/image.actions';
import { State } from '../state';

import * as UserActions from './user.actions';
import * as UserSelectors from './user.selectors';

@Injectable()
export class UserEffects {
  private joinUser(user: UserDto): Observable<AppUser> {
    return this.aclService.getUserById(user.aclId).pipe(map(aclUser => ({ ...aclUser, ...user })));
  }

  private joinUsers(users: UserDto[]): Observable<AppUser[]> {
    if (!users.length) {
      return of([]);
    }
    return this.aclService.getUsers().pipe(
      map(aclUsers =>
        // Use users here so we keep the sorting.
        users.reduce((prev, curr) => {
          const aclUser = aclUsers.find(u => u.id === curr.aclId);
          // aclUser must be deconstructed before the actor so we keep its id.
          return prev.concat(aclUser ? [{ ...aclUser, ...curr }] : []);
        }, [] as AppUser[]),
      ),
    );
  }

  private updateImg(user: UserDto, img: File, prevImageId?: string): Observable<FileDto> {
    return iif(
      () => !!prevImageId,
      this.fileService.deleteFile(prevImageId as string),
      of(null),
    ).pipe(switchMap(() => this.fileService.uploadFile(img, user.id, USER_AVATAR_TAG)));
  }

  private updateBoth(
    changes: Partial<AppUser>,
    userId: string,
    aclId: string,
  ): Observable<AppUser> {
    // TODO: This should be done by the backend.
    const { email, language, name } = changes;
    return forkJoin([
      this.userService.updateUser({ language, name }, userId),
      this.aclService.updateUser({ name, email }, aclId),
    ]).pipe(
      switchMap(([user, aclUser]) =>
        this.aclService.renew().pipe(map(() => ({ ...aclUser, ...user }))),
      ),
    );
  }

  loadMe = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.loadMe),
      exhaustMap(() =>
        this.aclService.me().pipe(
          switchMap(me =>
            this.userService
              .getUserByAclId(me.id)
              .pipe(map(user => UserActions.loadedMe({ me: { ...me, ...user } }))),
          ),
          catchError(error => of(UserActions.failedLoadingMe({ error }))),
        ),
      ),
    ),
  );

  changePassword = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.changePassword),
      exhaustMap(({ newPassword, oldPassword }) =>
        this.aclService.changePassword(oldPassword, newPassword).pipe(
          map(() => UserActions.changedPassword({})),
          catchError(error => of(UserActions.failedChangingPassword({ error }))),
        ),
      ),
    ),
  );

  changeUserPassword = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.changeUserPassword),
      exhaustMap(({ userId, newPassword }) =>
        this.aclService.changeUserPassword(userId, newPassword).pipe(
          map(() => UserActions.changedUserPassword({})),
          catchError(error => of(UserActions.failedChangingUserPassword({ error }))),
        ),
      ),
    ),
  );

  createUser = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.createUser),
      exhaustMap(({ user }) =>
        this.userService.createUser(user).pipe(
          switchMap(newUser => this.joinUser(newUser)),
          switchMap(newUser => [
            UserActions.createdUser({ user: newUser }),
            UserActions.loadUserTable({}),
          ]),
          catchError(error => of(UserActions.failedCreatingUser({ error }))),
        ),
      ),
    ),
  );

  updateUser = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.updateUser),
      exhaustMap(({ changes, userId, aclId }) =>
        this.updateBoth(changes, userId, aclId).pipe(
          switchMap(user => [UserActions.updatedUser({ user }), UserActions.loadUserTable({})]),
          catchError(error => of(UserActions.failedUpdatingUser({ error }))),
        ),
      ),
    ),
  );

  deleteUser = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.deleteUser),
      exhaustMap(({ userId }) =>
        this.userService.deleteUser(userId).pipe(
          switchMap(() => [UserActions.deletedUser({ userId }), UserActions.loadUserTable({})]),
          catchError(error => of(UserActions.failedDeletingUser({ error }))),
        ),
      ),
    ),
  );

  updateRoles = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.updateRoles),
      exhaustMap(({ aclId, roleIds }) =>
        this.aclService.updateUserRoles(roleIds, aclId).pipe(
          switchMap(roles => [
            UserActions.updatedRoles({ aclId, roles }),
            UserActions.loadUserTable({}),
          ]),
          catchError(error => of(UserActions.failedUpdatingRoles({ error }))),
        ),
      ),
    ),
  );

  loadRoles = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.loadRoles),
      exhaustMap(() =>
        this.aclService.getRoles().pipe(
          map(roles => UserActions.loadedRoles({ roles })),
          catchError(error => of(UserActions.failedLoadingRoles({ error }))),
        ),
      ),
    ),
  );

  uploadUserImg = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.uploadUserImg),
      exhaustMap(({ image, prevImageId, user }) =>
        this.updateImg(user, image, prevImageId).pipe(
          switchMap(uploadedImg => [
            UserActions.uploadedUserImg({ prevImageId, image: uploadedImg }),
            UserActions.loadUserTable({}),
            ImageActions.loadedImageData({ imageData: uploadedImg }),
            ImageActions.removedFile({ imageId: prevImageId }),
            ImageActions.loadImageFile({ imageData: uploadedImg }),
          ]),
          catchError(error => of(UserActions.failedUploadingUserImg({ error }))),
        ),
      ),
    ),
  );

  loadUserTable = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.loadUserTable),
      switchMap(({ tableId, filter, paging, sorting }) =>
        this.store.select(UserSelectors.selectTableMeta(tableId)).pipe(
          take(1),
          switchMap(meta =>
            this.userService
              .getUsers(filter || meta.filter, paging || meta.paging, sorting || meta.sorting)
              .pipe(
                switchMap(res =>
                  this.joinUsers(res.users).pipe(
                    map(users =>
                      UserActions.loadedUserTable({
                        tableId,
                        users,
                        filter: filter || meta.filter,
                        paging: {
                          ...res.meta,
                          limit: paging ? paging.limit || meta.paging.limit : meta.paging.limit,
                        },
                        sorting: sorting || meta.sorting,
                      }),
                    ),
                  ),
                ),
                catchError(error => of(UserActions.failedLoadingUserTable({ error }))),
              ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private store: Store<State>,
    private userService: UserService,
    private aclService: AclService,
    private fileService: FileService,
  ) {}
}
