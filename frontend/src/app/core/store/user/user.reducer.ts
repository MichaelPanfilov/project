import { AppUser, RoleDto } from '@common';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import { removeTable, TableState, upsertManyAndTable } from '../util';

import * as UserActions from './user.actions';

export const key = 'users';

export const roleAdapter: EntityAdapter<RoleDto> = createEntityAdapter<RoleDto>();
export const userAdapter: EntityAdapter<AppUser> = createEntityAdapter<AppUser>();

export interface State {
  me?: AppUser;
  roles: EntityState<RoleDto>;
  users: EntityState<AppUser> & TableState<AppUser>;
}

export const initialState: State = {
  roles: roleAdapter.getInitialState([]),
  users: { ...userAdapter.getInitialState([]), tables: {}, tableIds: [] },
};

const userReducer = createReducer(
  initialState,
  on(UserActions.loadedMe, (state, { me }) => ({ ...state, me })),
  on(UserActions.updatedUser, (state, { user }) => ({
    ...state,
    users: userAdapter.updateOne({ id: user.id, changes: user }, state.users),
  })),
  on(UserActions.loadedRoles, (state, { roles }) => ({
    ...state,
    roles: roleAdapter.addAll(roles, state.roles),
  })),
  on(UserActions.deletedUser, (state, { userId }) => ({
    ...state,
    users: userAdapter.removeOne(userId, state.users),
  })),
  on(UserActions.createdUser, (state, { user }) => ({
    ...state,
    users: userAdapter.addOne(user, state.users),
  })),
  on(UserActions.updatedRoles, (state, { aclId, roles }) => {
    const user = Object.values(state.users.entities).find(
      entity => entity && entity.aclId === aclId,
    );
    if (!user) {
      return state;
    }
    return {
      ...state,
      users: userAdapter.updateOne(
        { changes: { ...user, roleIds: roles.map(r => r.id) }, id: user.id },
        state.users,
      ),
    };
  }),
  // table
  on(UserActions.loadedUserTable, (state, { tableId, users, filter, paging, sorting }) => ({
    ...state,
    users: upsertManyAndTable(
      userAdapter.upsertMany,
      state.users,
      {
        items: users,
        filter: filter || {},
        paging: paging || {},
        sorting,
      },
      tableId,
    ),
  })),
  on(UserActions.removeUserTable, (state, { tableId }) => ({
    ...state,
    users: removeTable(tableId, state.users),
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return userReducer(state, action);
}

export const roleSelectors = roleAdapter.getSelectors();
export const userSelectors = userAdapter.getSelectors();
