import { AppUser, ParsedFilter } from '@common';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import { createTableSelectors } from '../util';

import { roleSelectors, userSelectors } from './user.reducer';
import { key, State } from './user.reducer';

const featureSelector = createFeatureSelector<State>(key);

export const selectMe = createSelector(featureSelector, state => state.me);

export const selectRoles = createSelector(featureSelector, state =>
  roleSelectors.selectAll(state.roles),
);

export const selectUsers = createSelector(featureSelector, state =>
  userSelectors.selectAll(state.users),
);

export const selectUserById = (id: string) =>
  createSelector(featureSelector, state => state.users.entities[id]);

export const { selectTableItems, selectTableMeta } = createTableSelectors<
  AppUser,
  ParsedFilter<AppUser>,
  State['users']
>(createSelector(featureSelector, state => state.users));
