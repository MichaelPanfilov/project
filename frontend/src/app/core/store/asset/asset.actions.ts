import { AssetDto } from '@common';
import { createAction } from '@ngrx/store';

import { propsWithBlocking, propsWithBlockingComplete } from '../util';

import * as LineActions from './line/line.actions';
import * as MachineActions from './machine/machine.actions';
import * as TreeActions from './tree/tree.actions';

export { LineActions, MachineActions, TreeActions };

// Load Asset

export const loadAsset = createAction(
  '[Asset/API] Load asset',
  propsWithBlocking<{ id: string }>(),
);

export const loadedAsset = createAction(
  '[Asset/API] Loaded asset',
  propsWithBlockingComplete<{ asset: AssetDto }>(loadAsset),
);

export const failedLoadingAsset = createAction(
  '[Asset/API] Failed loading asset',
  propsWithBlockingComplete<{ error: Error }>(loadAsset),
);

// Create Asset

export const createAsset = createAction(
  '[Asset/API] Create asset',
  propsWithBlocking<{ asset: AssetDto }>(),
);

export const createdAsset = createAction(
  '[Asset/API] Created asset',
  propsWithBlockingComplete<{ asset: AssetDto }>(createAsset, {
    success: true,
    message: 'ASSET.ACTION.CREATED',
  }),
);

export const failedCreatingAsset = createAction(
  '[Asset/API] Failed creating asset',
  propsWithBlockingComplete<{ error: Error }>(createAsset),
);

// Update asset

export const updateAsset = createAction(
  '[Asset/API] Update asset',
  propsWithBlocking<{ asset: AssetDto }>(),
);

export const updatedAsset = createAction(
  '[Asset/API] Updated asset',
  propsWithBlockingComplete<{ asset: AssetDto }>(updateAsset, {
    success: true,
    message: 'ASSET.ACTION.UPDATED',
  }),
);

export const failedUpdatingAsset = createAction(
  '[Asset/API] Failed updating asset',
  propsWithBlockingComplete<{ error: Error }>(updateAsset),
);

// Upload asset image

export const uploadAssetImage = createAction(
  '[Asset/API] Upload asset image',
  propsWithBlocking<{ image: File; prevImageId?: string; asset: AssetDto }>(),
);

export const uploadedAssetImage = createAction(
  '[Asset/API] Uploaded asset image',
  propsWithBlockingComplete<{ asset: AssetDto }>(uploadAssetImage),
);

export const failedUploadingAssetImage = createAction(
  '[Asset/API] Uploaded asset image',
  propsWithBlockingComplete<{ error: Error }>(uploadAssetImage),
);

// Update asset image

export const updateAssetImage = createAction(
  '[Asset/API] Update asset image',
  propsWithBlocking<{ image: File; prevImageId?: string; asset: AssetDto }>(),
);

export const updatedAssetImage = createAction(
  '[Asset/API] Updated asset image',
  propsWithBlockingComplete<{ asset: AssetDto }>(updateAssetImage),
);

export const failedUpdatingAssetImage = createAction(
  '[Asset/API] Update asset image',
  propsWithBlockingComplete<{ error: Error }>(updateAssetImage),
);

// Delete asset

export const deleteAsset = createAction(
  '[Asset/API] Delete asset',
  propsWithBlocking<{ asset: AssetDto }>(),
);

export const deletedAsset = createAction(
  '[Asset/API] Deleted asset',
  propsWithBlockingComplete(deleteAsset, {
    success: true,
    message: 'ASSET.ACTION.DELETED',
  }),
);

export const failedDeletingAsset = createAction(
  '[Asset/API] Failed deleting asset',
  propsWithBlockingComplete<{ error: Error }>(deleteAsset),
);
