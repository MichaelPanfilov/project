import { createFeatureSelector, createSelector } from '@ngrx/store';

import { key, State } from './line.reducer';

export const featureSelector = createFeatureSelector<State>(key);

export const selectCurrentId = createSelector(featureSelector, state => state.selectedId);
