import { Action, createReducer, on } from '@ngrx/store';

import * as LineActions from './line.actions';

export const key = 'lines';

export interface State {
  selectedId?: string;
}

export const initialState: State = {};

const lineReducer = createReducer(
  initialState,
  on(LineActions.setCurrent, (state, { id }) => ({ ...state, selectedId: id })),
);

export function reducer(state: State | undefined, action: Action) {
  return lineReducer(state, action);
}
