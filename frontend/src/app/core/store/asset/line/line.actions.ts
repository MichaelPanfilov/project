import { createAction, props } from '@ngrx/store';

export const setCurrent = createAction('[Asset/API] Set current line', props<{ id?: string }>());
