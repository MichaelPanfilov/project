import {
  AssetDto,
  AssetTree,
  AssetType,
  FactoryDto,
  FactoryTreeDto,
  LineDto,
  LineTreeDto,
  MachineDto,
  MachineTreeDto,
} from '@common';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import { key, State } from './asset.reducer';
import * as Line from './line';
import * as Machine from './machine';
import * as Tree from './tree';

const featureSelector = createFeatureSelector<State>(key);

// Generic

export const selectAsset = <T extends AssetDto>(id: string) =>
  createSelector(selectAssetTree(id), tree => tree && treeToAsset<T>(tree));

export const selectAssetTree = <T extends AssetTree<AssetType>>(id: string) =>
  createSelector(selectTrees, state => {
    for (const tree of Tree.selectAll(state)) {
      const asset = findAssetTree(id, tree as AssetTree<AssetType>);
      if (asset) {
        return asset as T;
      }
    }
    return undefined;
  });

// Tree

const selectTrees = createSelector(featureSelector, Tree.featureSelector);

export const selectAllAssetTrees = createSelector(selectTrees, Tree.selectAll);

export const selectRoot = createSelector(selectAllAssetTrees, trees => ({ children: trees }));

export const selectAllAssets = createSelector(selectAllAssetTrees, trees =>
  trees.reduce((curr, prev) => curr.concat(flattenTree(prev)), [] as AssetDto[]),
);

export const selectParent = <T extends AssetDto>(id: string) =>
  createSelector(selectAllAssetTrees, trees => {
    const tree = findAssetTreeIncluding(trees, id);
    return tree ? findParent<T>(tree, id) : undefined;
  });

export const selectParentTree = (id: string) =>
  createSelector(selectAllAssetTrees, trees => {
    const tree = findAssetTreeIncluding(trees, id);
    return tree ? findParentTree(tree, id) : undefined;
  });

export const selectTreeIncluding = (id: string) =>
  createSelector(selectAllAssetTrees, trees => {
    return findAssetTreeIncluding(trees, id);
  });

export const selectLoadedOnce = createSelector(selectTrees, state => state.loadedOnce);

// Factory

export const selectCurrentFactoryId = createSelector(featureSelector, Tree.selectCurrentId);

export const selectFactoryTrees = createSelector(selectAllAssetTrees, trees =>
  trees.reduce(
    (curr, prev) => curr.concat(findAssetTreesByType(AssetType.FACTORY, prev)),
    [] as FactoryTreeDto[],
  ),
);

export const selectAllFactories = createSelector(selectFactoryTrees, trees =>
  trees.map(tree => treeToAsset<FactoryDto>(tree)),
);

export const selectCurrentFactory = createSelector(
  selectCurrentFactoryId,
  selectTrees,
  (id, state) => findAssetById(Tree.selectAll(state), id),
);

export const selectCurrentFactoryTree = createSelector(
  selectCurrentFactoryId,
  selectAllAssetTrees,
  (id, trees) => findAssetTreeById<FactoryTreeDto>(trees, id),
);

// Line

export const selectCurrentLineId = createSelector(featureSelector, Line.selectCurrentId);

export const selectLineTrees = createSelector(selectAllAssetTrees, trees =>
  trees.reduce(
    (curr, prev) => curr.concat(findAssetTreesByType(AssetType.LINE, prev)),
    [] as LineTreeDto[],
  ),
);

export const selectCurrentLine = createSelector(selectCurrentLineId, selectTrees, (id, state) =>
  findAssetById(Tree.selectAll(state), id),
);

export const selectAllLines = createSelector(selectLineTrees, trees =>
  trees.map(tree => treeToAsset<LineDto>(tree)),
);

export const selectCurrentLineTree = createSelector(
  selectCurrentLineId,
  selectAllAssetTrees,
  (id, trees) => findAssetTreeById<LineTreeDto>(trees, id),
);

// Machine

export const selectCurrentMachineId = createSelector(featureSelector, Machine.selectCurrentId);

export const selectCurrentMachine = createSelector(
  selectCurrentMachineId,
  selectAllAssetTrees,
  (id, trees) => findAssetById<MachineDto>(trees, id),
);

export const selectMachineTrees = createSelector(selectAllAssetTrees, trees =>
  trees.reduce(
    (curr, prev) => curr.concat(findAssetTreesByType(AssetType.MACHINE, prev)),
    [] as MachineTreeDto[],
  ),
);

export const selectAllMachines = createSelector(selectMachineTrees, trees =>
  trees.map(tree => treeToAsset<MachineDto>(tree)),
);

export const selectAssetHierarchyOrder = (asset: AssetDto) =>
  createSelector(selectCurrentFactoryTree, tree => (tree ? getHierarchyOrder(tree, asset) : []));

export const selectCurrentMachineTimeline = createSelector(
  selectCurrentMachineId,
  featureSelector,
  (id, state) => (id ? Machine.selectTimeline(id)(state) : undefined),
);

export const selectMachineStatus = (id: string) =>
  createSelector(featureSelector, state => Machine.selectStatus(id)(state));

export const selectCurrentMachineStatus = createSelector(
  selectCurrentMachineId,
  featureSelector,
  (id, state) => (id ? Machine.selectStatus(id)(state) : undefined),
);

export const selectMachineOutputCount = (id: string) =>
  createSelector(featureSelector, state => Machine.selectOutput(id)(state));

export const selectCurrentMachineOutputCount = createSelector(
  selectCurrentMachineId,
  featureSelector,
  (id, state) => (id ? Machine.selectOutput(id)(state) : undefined),
);

export const selectCurrentMachineProductLoses = createSelector(
  selectCurrentMachineId,
  featureSelector,
  (id, state) => (id ? Machine.selectProductLoses(id)(state) : undefined),
);

export const selectCurrentMachineProductLossCount = createSelector(
  selectCurrentMachineId,
  featureSelector,
  (id, state) => (id ? Machine.selectProductLossCount(id)(state) : undefined),
);

export const selectCurrentMachineProductLossPercentage = createSelector(
  selectCurrentMachineProductLossCount,
  selectCurrentMachineOutputCount,
  (lossCount, outputCount) => {
    if (!outputCount) {
      return 0;
    }

    const loss = Math.round(((lossCount || 0) / outputCount) * 100);
    return loss >= 100 ? 100 : loss;
  },
);

export const selectCurrentMachineFaultStops = createSelector(
  selectCurrentMachineId,
  featureSelector,
  (id, state) => (id ? Machine.selectFaultStops(id)(state) : undefined),
);

export const selectCurrentMachineFaultStopCounts = createSelector(
  selectCurrentMachineId,
  featureSelector,
  (id, state) => (id ? Machine.selectFaultStopCounts(id)(state) : undefined),
);

export const selectMachineTotalCount = (id: string) =>
  createSelector(featureSelector, state => (id ? Machine.selectTotalCount(id)(state) : undefined));

export const selectMachinePowerOnHours = (id: string) =>
  createSelector(featureSelector, state => Machine.selectPowerOnHours(id)(state));

export const selectMachineProducingHours = (id: string) =>
  createSelector(featureSelector, state => Machine.selectProducingHours(id)(state));

export const selectMachineCycles = (id: string) =>
  createSelector(featureSelector, state => Machine.selectCycles(id)(state));

export const selectDevices = createSelector(featureSelector, Machine.selectDevices);

// Util

function findAssetTreeById<T extends AssetTree<AssetType>>(
  trees: AssetTree<AssetType>[],
  id?: string,
): T | undefined {
  if (id) {
    for (const tree of trees) {
      const asset = findAssetTree(id, tree) as T | undefined;
      if (asset) {
        return asset;
      }
    }
  }
  return undefined;
}

function findAssetById<T extends AssetDto>(
  trees: AssetTree<AssetType>[],
  id?: string,
): T | undefined {
  if (id) {
    const asset = findAssetTreeById(trees, id);
    return asset ? treeToAsset<T>(asset) : undefined;
  }
  return undefined;
}

function findAssetTree(id: string, tree: AssetTree<AssetType>): AssetDto | undefined {
  if (tree.id === id) {
    return tree;
  }
  for (const child of tree.children || []) {
    const asset = findAssetTree(id, child as AssetTree<AssetType>);
    if (asset) {
      return asset;
    }
  }
  return undefined;
}

function findAssetTreeIncluding<T extends AssetTree<AssetType>>(
  trees: AssetTree<AssetType>[],
  id: string,
): T | undefined {
  return trees.find(tree => findAssetTree(id, tree)) as T;
}

function findAssetTreesByType<T extends AssetTree<AssetType>>(
  type: AssetType,
  tree: AssetTree<AssetType>,
): T[] {
  if (tree.type === type) {
    return [tree] as T[];
  }
  return tree.children.reduce(
    (curr, prev) => curr.concat(findAssetTreesByType(type, prev as AssetTree<AssetType>)),
    [] as T[],
  );
}

function findParent<T extends AssetDto>(tree: AssetTree<AssetType>, id: string): T | undefined {
  const found = !!tree.children.find(child => child.id === id);
  if (found) {
    return treeToAsset<T>(tree);
  }
  for (const child of tree.children) {
    const result = findParent(child as AssetTree<AssetType>, id);
    if (result) {
      return result as T;
    }
  }
  return undefined;
}

function findParentTree(tree: AssetTree<AssetType>, id: string): AssetTree<AssetType> | undefined {
  const found = !!tree.children.find(child => child.id === id);
  if (found) {
    return tree;
  }
  for (const child of tree.children) {
    const result = findParentTree(child as AssetTree<AssetType>, id);
    if (result) {
      return result as AssetTree<AssetType>;
    }
  }
  return undefined;
}

function treeToAsset<T extends AssetDto>(tree: AssetTree<AssetType>): T {
  const excludes: (keyof AssetTree<AssetType>)[] = ['parent', 'children'];
  const asset = {};
  Object.keys(tree)
    .filter(k => !excludes.includes(k as keyof AssetTree<AssetType>))
    .forEach(k => (asset[k] = tree[k]));
  return asset as T;
}

function flattenTree(tree: AssetTree<AssetType>): AssetDto[] {
  const arr = [treeToAsset(tree)];
  if (!tree.children.length) {
    return arr;
  }
  return arr.concat(
    tree.children.reduce(
      (curr, prev) => curr.concat(flattenTree(prev as AssetTree<AssetType>)),
      [] as AssetDto[],
    ),
  );
}

function getHierarchyOrder(tree: AssetTree<AssetType>, asset: AssetDto): AssetDto[] {
  const parentTree = findParentTree(tree, asset.id);
  if (!parentTree) {
    return [asset];
  }
  return [...getHierarchyOrder(tree, treeToAsset(parentTree)), asset];
}
