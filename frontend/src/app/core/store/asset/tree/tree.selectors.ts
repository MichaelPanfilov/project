import { AssetDto, AssetTree, AssetType } from '@common';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import { key, selectAll, State } from './tree.reducer';

export const featureSelector = createFeatureSelector<State>(key);

export const selectCurrentId = createSelector(featureSelector, state => state.selectedId);

export const selectTree = <T extends AssetTree<AssetType>>(id: string) =>
  createSelector(
    featureSelector,
    state =>
      selectAll(state).find(tree => findAsset(id, tree as AssetTree<AssetType>)) as T | undefined,
  );

export const selectAsset = <T extends AssetDto>(id: string) =>
  createSelector(selectTree(id), tree => tree && treeToAsset<T>(tree));

function findAsset(id: string, tree: AssetTree<AssetType>): AssetDto | undefined {
  if (tree.id === id) {
    return tree;
  }
  return (tree.children || []).find(child => findAsset(id, child as AssetTree<AssetType>));
}

function treeToAsset<T extends AssetDto>(tree: AssetTree<AssetType>): T {
  const excludes: (keyof AssetTree<AssetType>)[] = ['parent', 'children'];
  const asset = {};
  Object.keys(tree)
    .filter(k => !excludes.includes(k as keyof AssetTree<AssetType>))
    .forEach(k => (asset[k] = tree[k]));
  return asset as T;
}
