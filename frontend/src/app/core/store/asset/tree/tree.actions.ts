import { AssetDto, FactoryTreeDto } from '@common';
import { createAction, props } from '@ngrx/store';

import {
  propsWithBlocking,
  propsWithBlockingComplete,
  propsWithLoading,
  propsWithLoadingComplete,
} from '../../util';

export const setCurrent = createAction(
  '[Asset/API] Select current asset tree',
  props<{ id?: string }>(),
);

export const loadTrees = createAction('[Asset/API] Load asset trees', propsWithLoading());

export const loadedTrees = createAction(
  '[Asset/API] Loaded asset trees',
  propsWithLoadingComplete<{ trees: FactoryTreeDto[] }>(loadTrees),
);

export const failedLoadingTrees = createAction(
  '[Asset/API] Failed loading asset trees',
  propsWithLoadingComplete<{ error: Error }>(loadTrees),
);

export const updateParent = createAction(
  '[Asset/API] Update asset parent',
  propsWithBlocking<{ asset: AssetDto; parentId: string }>(),
);

export const updatedParent = createAction(
  '[Asset/API] Updated asset parent',
  propsWithBlockingComplete(updateParent),
);

export const failedUpdatingParent = createAction(
  '[Asset/API] Failed updating asset parent',
  propsWithBlockingComplete<{ error: Error }>(updateParent),
);
