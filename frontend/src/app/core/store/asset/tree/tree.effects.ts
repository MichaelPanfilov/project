import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, switchMap } from 'rxjs/operators';

import { AssetService } from '../../../services/api/asset.service';

import * as TreeActions from './tree.actions';

@Injectable()
export class TreeEffects {
  loadTrees = createEffect(() =>
    this.actions$.pipe(
      ofType(TreeActions.loadTrees),
      exhaustMap(() =>
        this.assetService.getAssetTrees().pipe(
          map(trees => TreeActions.loadedTrees({ trees })),
          catchError(error => of(TreeActions.failedLoadingTrees({ error }))),
        ),
      ),
    ),
  );

  updateParent = createEffect(() =>
    this.actions$.pipe(
      ofType(TreeActions.updateParent),
      exhaustMap(({ asset, parentId }) =>
        this.assetService.setAssetParent(asset.id, parentId).pipe(
          switchMap(() => [TreeActions.updatedParent({}), TreeActions.loadTrees({})]),
          catchError(error => of(TreeActions.failedLoadingTrees({ error }))),
        ),
      ),
    ),
  );

  constructor(private actions$: Actions, private assetService: AssetService) {}
}
