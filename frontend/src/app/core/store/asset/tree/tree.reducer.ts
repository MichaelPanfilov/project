import { FactoryTreeDto } from '@common';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import * as TreeActions from './tree.actions';

export const key = 'trees';

export interface State extends EntityState<FactoryTreeDto> {
  selectedId?: string;
  loadedOnce: boolean;
}

export const adapter: EntityAdapter<FactoryTreeDto> = createEntityAdapter<FactoryTreeDto>();

export const initialState: State = adapter.getInitialState({
  loadedOnce: false,
});

const treeReducer = createReducer(
  initialState,
  on(TreeActions.loadedTrees, (state, { trees }) => ({
    ...adapter.addAll(trees, state),
    loadedOnce: true,
  })),
  on(TreeActions.setCurrent, (state, { id }) => ({ ...state, selectedId: id })),
);

export function reducer(state: State | undefined, action: Action) {
  return treeReducer(state, action);
}

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
