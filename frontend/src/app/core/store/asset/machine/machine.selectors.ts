import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State } from './machine.reducer';
import { key } from './machine.reducer';

export const featureSelector = createFeatureSelector<State>(key);

export const selectCurrentId = createSelector(featureSelector, state => state.selectedId);

export const selectTimeline = (id: string) =>
  createSelector(featureSelector, state => state.timelines[id]);

export const selectStatus = (id: string) =>
  createSelector(featureSelector, state => state.states[id]);

export const selectOutput = (id: string) =>
  createSelector(featureSelector, state => state.outputs[id]);

export const selectProductLossCount = (id: string) =>
  createSelector(featureSelector, state => state.productLosesCount[id]);

export const selectProductLoses = (id: string) =>
  createSelector(featureSelector, state => state.productLoses[id]);

export const selectFaultStops = (id: string) =>
  createSelector(featureSelector, state => state.faultStops[id]);

export const selectFaultStopCounts = (id: string) =>
  createSelector(featureSelector, state => state.faultStopCounts[id]);

export const selectTotalCount = (id: string) =>
  createSelector(featureSelector, state => state.outputs[id]);

export const selectPowerOnHours = (id: string) =>
  createSelector(featureSelector, state => state.powerOnHours[id]);

export const selectProducingHours = (id: string) =>
  createSelector(featureSelector, state => state.producingHours[id]);

export const selectCycles = (id: string) =>
  createSelector(featureSelector, state => state.cycles[id]);

export const selectDevices = createSelector(featureSelector, state => state.devices);
