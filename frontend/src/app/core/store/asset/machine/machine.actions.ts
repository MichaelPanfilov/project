import {
  FaultStopDto,
  MachineDto,
  ProductLossDto,
  SynDisconnectedState,
  SynMachineState,
  TimelineDto,
} from '@common';
import { createAction, props } from '@ngrx/store';

import { propsWithLoading, propsWithLoadingComplete } from '../../util';

export const setCurrent = createAction(
  '[Asset/API] Select current machine',
  props<{ id?: string }>(),
);

// Status

export const loadStatus = createAction(
  "[Asset/API] Load machine's status",
  propsWithLoading<{ machine: MachineDto }>(true),
);
export const loadedStatus = createAction(
  "[Asset/API] Loaded machine's status",
  propsWithLoadingComplete<{ machine: MachineDto; status: SynMachineState | SynDisconnectedState }>(
    loadStatus,
  ),
);
export const failedLoadingStatus = createAction(
  "[Asset/API] Failed loading machine's status",
  propsWithLoadingComplete<{ error: Error }>(loadStatus),
);

// Output

export const loadOutput = createAction(
  "[Asset/API] Load machine's output",
  propsWithLoading<{ machine: MachineDto; from: Date; to: Date }>(true),
);
export const loadedOutput = createAction(
  "[Asset/API] Loaded machine's output",
  propsWithLoadingComplete<{ machine: MachineDto; output: number }>(loadOutput),
);
export const failedLoadingOutput = createAction(
  "[Asset/API] Failed loading machine's output",
  propsWithLoadingComplete<{ error: Error }>(loadOutput),
);

// Timeline

export const loadTimeline = createAction(
  "[Asset/API] Load machine's timeline",
  propsWithLoading<{ machine: MachineDto; from: Date; to: Date }>(true),
);
export const loadedTimeline = createAction(
  "[Asset/API] Loaded  machine's timeline",
  propsWithLoadingComplete<{ machine: MachineDto; timeline: TimelineDto[] }>(loadTimeline),
);
export const failedLoadingTimeline = createAction(
  "[Asset/API] Failed loading  machine's timeline",
  propsWithLoadingComplete<{ error: Error }>(loadTimeline),
);

// Product loss

export const loadProductLoses = createAction(
  '[Asset/API] Load product loses',
  propsWithLoading<{ machine: MachineDto; from: Date; to: Date }>(true),
);
export const loadedProductLoses = createAction(
  '[Asset/API] Loaded product loses',
  propsWithLoadingComplete<{ machine: MachineDto; productLoses: ProductLossDto[] }>(
    loadProductLoses,
  ),
);
export const failedLoadingProductLoses = createAction(
  '[Asset/API] Failed loading product loses',
  propsWithLoadingComplete<{ error: Error }>(loadProductLoses),
);

// Product loss count

export const loadProductLossCount = createAction(
  '[Asset/API] Load product loss count',
  propsWithLoading<{ machine: MachineDto; from: Date; to: Date }>(true),
);
export const loadedProductLossCount = createAction(
  '[Asset/API] Loaded product loss count',
  propsWithLoadingComplete<{ machine: MachineDto; count: number }>(loadProductLossCount),
);
export const failedLoadingProductLossCount = createAction(
  '[Asset/API] Failed loading product loss count',
  propsWithLoadingComplete<{ error: Error }>(loadProductLossCount),
);

// Fault stops

export const loadFaultStops = createAction(
  '[Asset/API] Load fault stops',
  propsWithLoading<{ machine: MachineDto; from: Date; to: Date }>(true),
);
export const loadedFaultStops = createAction(
  '[Asset/API] Loaded fault stops',
  propsWithLoadingComplete<{ machine: MachineDto; faultStops: FaultStopDto[] }>(loadFaultStops),
);
export const failedLoadingFaultStops = createAction(
  '[Asset/API] Failed loading fault stops',
  propsWithLoadingComplete<{ error: Error }>(loadFaultStops),
);

// Fault stop counts

export const loadFaultStopCounts = createAction(
  '[Asset/API] Load fault stop counts',
  propsWithLoading<{ machine: MachineDto; from: Date; to: Date }>(true),
);
export const loadedFaultStopCounts = createAction(
  '[Asset/API] Loaded fault stop counts',
  propsWithLoadingComplete<{
    machine: MachineDto;
    counts: { label: string; count: number }[];
  }>(loadFaultStopCounts),
);
export const failedLoadingFaultStopCounts = createAction(
  '[Asset/API] Failed loading fault stop counts',
  propsWithLoadingComplete<{ error: Error }>(loadFaultStopCounts),
);

// Power on hours

export const loadPowerOnHours = createAction(
  '[Asset/API] Load power on hours',
  propsWithLoading<{ machine: MachineDto }>(true),
);
export const loadedPowerOnHours = createAction(
  '[Asset/API] Loaded power on hours',
  propsWithLoadingComplete<{ machine: MachineDto; hours: number }>(loadPowerOnHours),
);
export const failedLoadingPowerOnHours = createAction(
  '[Asset/API] Failed loading power on hours',
  propsWithLoadingComplete<{ error: Error }>(loadPowerOnHours),
);

// Producing hours

export const loadProducingHours = createAction(
  '[Asset/API] Load producing hours',
  propsWithLoading<{ machine: MachineDto }>(true),
);
export const loadedProducingHours = createAction(
  '[Asset/API] Loaded producing hours',
  propsWithLoadingComplete<{ machine: MachineDto; hours: number }>(loadProducingHours),
);
export const failedLoadingProducingHours = createAction(
  '[Asset/API] Failed loading producing hours',
  propsWithLoadingComplete<{ error: Error }>(loadProducingHours),
);

// Cycles

export const loadCycles = createAction(
  '[Asset/API] Load cycles',
  propsWithLoading<{ machine: MachineDto }>(true),
);
export const loadedCycles = createAction(
  '[Asset/API] Loaded cycles',
  propsWithLoadingComplete<{ machine: MachineDto; cycles: number }>(loadCycles),
);
export const failedLoadingCycles = createAction(
  '[Asset/API] Failed loading cycles',
  propsWithLoadingComplete<{ error: Error }>(loadCycles),
);

// devices

export const loadDevices = createAction('[Asset/API] Load devices', propsWithLoading());

export const loadedDevices = createAction(
  '[Asset/API] Loaded devices',
  propsWithLoadingComplete<{ devices: string[] }>(loadDevices),
);

export const failedLoadingDevices = createAction(
  '[Asset/API] Failed loading devices',
  propsWithLoadingComplete<{ error: Error }>(loadDevices),
);
