import {
  FaultStopDto,
  ProductLossDto,
  SynDisconnectedState,
  SynMachineState,
  TimelineDto,
} from '@common';
import { Dictionary } from '@ngrx/entity';
import { Action, ActionReducer, createReducer, on } from '@ngrx/store';

import * as MachineActions from './machine.actions';

export const key = 'machines';

export interface State {
  selectedId?: string;
  timelines: Dictionary<TimelineDto[]>;
  states: Dictionary<SynMachineState | SynDisconnectedState>;
  outputs: Dictionary<number>;
  productLosesCount: Dictionary<number>;
  productLoses: Dictionary<ProductLossDto[]>;
  faultStops: Dictionary<FaultStopDto[]>;
  faultStopCounts: Dictionary<{ label: string; count: number }[]>;
  powerOnHours: Dictionary<number>;
  producingHours: Dictionary<number>;
  cycles: Dictionary<number>;
  devices: string[];
}

export const initialState: State = {
  timelines: {},
  states: {},
  outputs: {},
  productLoses: {},
  faultStops: {},
  productLosesCount: {},
  faultStopCounts: {},
  powerOnHours: {},
  producingHours: {},
  cycles: {},
  devices: [],
};

export const machineReducer: ActionReducer<State, Action> = createReducer(
  initialState,
  on(MachineActions.setCurrent, (state, { id }) => ({ ...state, selectedId: id })),
  on(MachineActions.loadedTimeline, (state, { machine, timeline }) => ({
    ...state,
    timelines: { ...state.timelines, [machine.id]: timeline },
  })),
  on(MachineActions.loadedStatus, (state, { machine, status }) => ({
    ...state,
    states: { ...state.states, [machine.id]: status },
  })),
  on(MachineActions.loadedOutput, (state, { machine, output }) => ({
    ...state,
    outputs: { ...state.outputs, [machine.id]: output },
  })),
  on(MachineActions.loadedProductLossCount, (state, { machine, count }) => ({
    ...state,
    productLosesCount: { ...state.productLosesCount, [machine.id]: count },
  })),
  on(MachineActions.loadedProductLoses, (state, { machine, productLoses }) => ({
    ...state,
    productLoses: { ...state.productLoses, [machine.id]: productLoses },
  })),
  on(MachineActions.loadedFaultStops, (state, { machine, faultStops }) => ({
    ...state,
    faultStops: { ...state.faultStops, [machine.id]: faultStops },
  })),
  on(MachineActions.loadedFaultStopCounts, (state, { machine, counts }) => ({
    ...state,
    faultStopCounts: { ...state.faultStopCounts, [machine.id]: counts },
  })),
  on(MachineActions.loadedPowerOnHours, (state, { machine, hours }) => ({
    ...state,
    powerOnHours: { ...state.powerOnHours, [machine.id]: hours },
  })),
  on(MachineActions.loadedProducingHours, (state, { machine, hours }) => ({
    ...state,
    producingHours: { ...state.producingHours, [machine.id]: hours },
  })),
  on(MachineActions.loadedCycles, (state, { machine, cycles }) => ({
    ...state,
    cycles: { ...state.cycles, [machine.id]: cycles },
  })),
  on(MachineActions.loadedDevices, (state, { devices }) => ({ ...state, devices })),
);

export function reducer(state: State | undefined, action: Action) {
  return machineReducer(state, action);
}
