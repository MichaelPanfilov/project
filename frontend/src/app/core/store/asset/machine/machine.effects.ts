import { Injectable } from '@angular/core';
import { SynDisconnectedState, SynMachineState } from '@common';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { DatapointService } from '../../../services/api';

import * as MachineActions from './machine.actions';

@Injectable()
export class MachineEffects {
  loadTimeline = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadTimeline),
      mergeMap(({ machine, from, to }) =>
        (machine.device
          ? this.datapointService.getMachinePerformance(machine.device, from, to)
          : of([])
        ).pipe(
          map(timeline => MachineActions.loadedTimeline({ machine, timeline })),
          catchError(error => of(MachineActions.failedLoadingTimeline({ error }))),
        ),
      ),
    ),
  );

  loadStatus = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadStatus),
      mergeMap(({ machine }) =>
        (machine.device
          ? this.datapointService.getStatus(machine.device)
          : (of('Disconnected') as Observable<SynDisconnectedState | SynMachineState>)
        ).pipe(
          map(status => MachineActions.loadedStatus({ machine, status })),
          catchError(error => of(MachineActions.failedLoadingStatus({ error }))),
        ),
      ),
    ),
  );

  loadOutput = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadOutput),
      mergeMap(({ machine, from, to }) =>
        (machine.device
          ? this.datapointService.getOutputCount(machine.device, from, to)
          : of(0)
        ).pipe(
          map(output => MachineActions.loadedOutput({ machine, output })),
          catchError(error => of(MachineActions.failedLoadingOutput({ error }))),
        ),
      ),
    ),
  );

  loadProductLoses = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadProductLoses),
      mergeMap(({ machine, from, to }) =>
        (machine.device
          ? this.datapointService.getProductLoses(machine.device, from, to)
          : of([])
        ).pipe(
          map(productLoses => MachineActions.loadedProductLoses({ machine, productLoses })),
          catchError(error => of(MachineActions.failedLoadingProductLoses({ error }))),
        ),
      ),
    ),
  );

  loadProductLossCount = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadProductLossCount),
      mergeMap(({ machine, from, to }) =>
        (machine.device
          ? this.datapointService.getProductLossCount(machine.device, from, to)
          : of(0)
        ).pipe(
          map(count => MachineActions.loadedProductLossCount({ machine, count })),
          catchError(error => of(MachineActions.failedLoadingProductLossCount({ error }))),
        ),
      ),
    ),
  );

  loadFaultStops = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadFaultStops),
      mergeMap(({ machine, from, to }) =>
        (machine.device
          ? this.datapointService.getFaultStops(machine.device, from, to)
          : of([])
        ).pipe(
          map(faultStops => MachineActions.loadedFaultStops({ machine, faultStops })),
          catchError(error => of(MachineActions.failedLoadingFaultStops({ error }))),
        ),
      ),
    ),
  );

  loadFaultStopCounts = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadFaultStopCounts),
      mergeMap(({ machine, from, to }) =>
        (machine.device
          ? this.datapointService.getFaultStopCounts(machine.device, from, to)
          : of([])
        ).pipe(
          map(counts => MachineActions.loadedFaultStopCounts({ machine, counts })),
          catchError(error => of(MachineActions.failedLoadingFaultStopCounts({ error }))),
        ),
      ),
    ),
  );

  loadPowerOnHours = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadPowerOnHours),
      mergeMap(({ machine }) =>
        (machine.device ? this.datapointService.getPowerOnHours(machine.device) : of(0)).pipe(
          map(hours => MachineActions.loadedPowerOnHours({ machine, hours })),
          catchError(error => of(MachineActions.failedLoadingPowerOnHours({ error }))),
        ),
      ),
    ),
  );

  loadProducingHours = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadProducingHours),
      mergeMap(({ machine }) =>
        (machine.device ? this.datapointService.getProducingHours(machine.device) : of(0)).pipe(
          map(hours => MachineActions.loadedProducingHours({ machine, hours })),
          catchError(error => of(MachineActions.failedLoadingProducingHours({ error }))),
        ),
      ),
    ),
  );

  loadCycles = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadCycles),
      mergeMap(({ machine }) =>
        (machine.device ? this.datapointService.getCycles(machine.device) : of(0)).pipe(
          map(cycles => MachineActions.loadedCycles({ machine, cycles })),
          catchError(error => of(MachineActions.failedLoadingCycles({ error }))),
        ),
      ),
    ),
  );

  loadDevices = createEffect(() =>
    this.actions$.pipe(
      ofType(MachineActions.loadDevices),
      mergeMap(() =>
        this.datapointService.getDevices().pipe(
          map(devices => MachineActions.loadedDevices({ devices })),
          catchError(error => of(MachineActions.failedLoadingDevices({ error }))),
        ),
      ),
    ),
  );

  constructor(private actions$: Actions, private datapointService: DatapointService) {}
}
