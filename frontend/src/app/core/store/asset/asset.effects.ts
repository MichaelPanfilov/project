import { Injectable } from '@angular/core';
import { ASSET_FILE_TAG } from '@common';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { forkJoin, of } from 'rxjs';
import { catchError, exhaustMap, map, switchMap, withLatestFrom } from 'rxjs/operators';

import { AssetService, FileService } from '../../services/api';
import * as ImageActions from '../image/image.actions';
import { State } from '../state';
import * as TaskActions from '../task/task.actions';
import * as TaskSelectors from '../task/task.selectors';

import * as AssetActions from './asset.actions';

export { LineEffects } from './line/line.effects';
export { MachineEffects } from './machine/machine.effects';
export { TreeEffects as AssetTreeEffect } from './tree/tree.effects';

@Injectable()
export class AssetEffects {
  loadAsset = createEffect(() =>
    this.actions$.pipe(
      ofType(AssetActions.loadAsset),
      exhaustMap(({ id }) =>
        this.assetService.getAsset(id).pipe(
          map(asset => AssetActions.loadedAsset({ asset })),
          catchError(error => of(AssetActions.failedLoadingAsset({ error }))),
        ),
      ),
    ),
  );

  updateAsset = createEffect(() =>
    this.actions$.pipe(
      ofType(AssetActions.updateAsset),
      exhaustMap(({ asset }) =>
        this.assetService.updateAsset(asset).pipe(
          switchMap(updatedAsset => [
            AssetActions.updatedAsset({ asset: updatedAsset }),
            AssetActions.TreeActions.loadTrees({}),
          ]),
          catchError(error => of(AssetActions.failedUpdatingAsset({ error }))),
        ),
      ),
    ),
  );

  // TODO: rework into one action
  updateAssetImage = createEffect(() =>
    this.actions$.pipe(
      ofType(AssetActions.updateAssetImage),
      exhaustMap(({ image, prevImageId, asset }) =>
        forkJoin([
          this.fileService.deleteFile(prevImageId || '').pipe(catchError(() => of(null))),
          this.fileService.uploadFile(image, asset.id, ASSET_FILE_TAG),
        ]).pipe(
          switchMap(([_, uploadedImg]) => [
            AssetActions.updatedAssetImage({ asset }),
            ImageActions.loadedImageData({ imageData: uploadedImg }),
            ImageActions.removedFile({ imageId: prevImageId }),
            ImageActions.loadImageFile({ imageData: uploadedImg }),
          ]),
          catchError(error => of(AssetActions.failedUpdatingAssetImage({ error }))),
        ),
      ),
    ),
  );

  uploadAssetImage = createEffect(() =>
    this.actions$.pipe(
      ofType(AssetActions.uploadAssetImage),
      exhaustMap(({ image, asset }) =>
        this.fileService.uploadFile(image, asset.id, ASSET_FILE_TAG).pipe(
          switchMap(uploadedImg => [
            AssetActions.uploadedAssetImage({ asset }),
            ImageActions.loadedImageData({ imageData: uploadedImg }),
            ImageActions.loadImageFile({ imageData: uploadedImg }),
          ]),
          catchError(error => of(AssetActions.failedUploadingAssetImage({ error }))),
        ),
      ),
    ),
  );

  createAsset = createEffect(() =>
    this.actions$.pipe(
      ofType(AssetActions.createAsset),
      exhaustMap(({ asset }) =>
        this.assetService.createAsset(asset).pipe(
          switchMap(createdAsset => [
            AssetActions.createdAsset({ asset: createdAsset }),
            AssetActions.TreeActions.loadTrees({}),
          ]),
          catchError(error => of(AssetActions.failedCreatingAsset({ error }))),
        ),
      ),
    ),
  );

  deleteAsset = createEffect(() =>
    this.actions$.pipe(
      ofType(AssetActions.deleteAsset),
      exhaustMap(({ asset }) =>
        this.assetService.deleteAsset(asset.id).pipe(
          withLatestFrom(this.store.select(TaskSelectors.selectTasksOfAsset(asset.id))),
          switchMap(([, tasks]) => [
            AssetActions.deletedAsset({}),
            AssetActions.TreeActions.loadTrees({}),
            // Reload the tasks that had this asset assigned.
            // These will now be unassigned, by reloading we update these tasks in the store.
            TaskActions.loadTasksByIds(tasks.map(task => task.id)),
          ]),
          catchError(error => of(AssetActions.failedCreatingAsset({ error }))),
        ),
      ),
    ),
  );

  constructor(
    private store: Store<State>,
    private actions$: Actions,
    private assetService: AssetService,
    private fileService: FileService,
  ) {}
}
