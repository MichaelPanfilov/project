import { Action, ActionReducerMap, combineReducers } from '@ngrx/store';

import * as Line from './line';
import * as Machine from './machine';
import * as Tree from './tree';

export const key = 'assets';

export interface State {
  [Line.key]: Line.State;
  [Machine.key]: Machine.State;
  [Tree.key]: Tree.State;
}

export const initialState: State = {
  [Line.key]: Line.initialState,
  [Machine.key]: Machine.initialState,
  [Tree.key]: Tree.initialState,
};

export const reducerMap: ActionReducerMap<State> = {
  [Line.key]: Line.reducer,
  [Machine.key]: Machine.reducer,
  [Tree.key]: Tree.reducer,
};

const assetReducer = combineReducers(reducerMap, initialState);

export function reducer(state: State | undefined, action: Action) {
  return assetReducer(state, action);
}
