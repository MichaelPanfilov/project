import { createAction } from '@ngrx/store';

export const resetStore = createAction('[Meta] reset Store');
