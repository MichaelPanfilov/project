import { FileDto, FilesFilter } from '@common';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import { createTableSelectors } from '../util';

import { key, selectAll, State } from './document.reducer';

const featureSelector = createFeatureSelector<State>(key);

export const selectDocuments = createSelector(featureSelector, selectAll);

export const selectDocumentsByRefId = (refId: string) =>
  createSelector(featureSelector, state =>
    selectAll(state).filter(({ refIds }) => refIds && refIds.includes(refId)),
  );

export const { selectTableItems, selectTableMeta } = createTableSelectors<
  FileDto,
  FilesFilter,
  State
>(featureSelector);
