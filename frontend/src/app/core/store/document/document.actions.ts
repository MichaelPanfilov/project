import { FileDto, FilesFilter, PagingResponseMeta, Sorting } from '@common';
import { createAction, props } from '@ngrx/store';

import {
  propsWithBlocking,
  propsWithBlockingComplete,
  propsWithLoading,
  propsWithLoadingComplete,
} from '../util';

// Load documents

export const loadDocuments = createAction(
  '[File/API] load documents',
  propsWithLoading<{ filter?: FilesFilter }>(),
);
export const loadedDocuments = createAction(
  '[File/API] loaded documents',
  propsWithLoadingComplete<{ documents: FileDto[] }>(loadDocuments),
);
export const failedLoadingDocuments = createAction(
  '[File/API] failed loading documents',
  propsWithLoadingComplete<{ error: Error }>(loadDocuments),
);

// load tasks by ids

export const loadDocumentsByIds = (ids: string[]) => loadDocuments({ filter: { id: ids } });

// Load document by refIds

export const loadDocumentsByRefIds = (refIds: string[]) =>
  loadDocuments({ filter: { refId: refIds } });

// Upload document

export const uploadDocument = createAction(
  '[File/API] upload document',
  propsWithBlocking<{ file: File; title: string; refIds?: string[]; machineTypeTags?: string[] }>(),
);
export const uploadedDocument = createAction(
  '[File/API] uploaded document',
  propsWithBlockingComplete<{ document: FileDto }>(uploadDocument, {
    success: true,
    message: 'DOCUMENT.ACTION.CREATED',
  }),
);
export const failedUploadingDocument = createAction(
  '[File/API] failed uploading document',
  propsWithBlockingComplete<{ error: Error }>(uploadDocument),
);

// Update document

export const updateDocument = createAction(
  '[File/API] update document',
  propsWithBlocking<{
    documentId: string;
    title: string;
    refIds?: string[];
    machineTypeTags?: string[];
  }>(),
);
export const updatedDocument = createAction(
  '[File/API] updated document',
  propsWithBlockingComplete<{ document: FileDto }>(updateDocument, {
    success: true,
    message: 'DOCUMENT.ACTION.UPDATED',
  }),
);
export const failedUpdatingDocument = createAction(
  '[File/API] failed updating document',
  propsWithBlockingComplete<{ error: Error }>(updateDocument),
);

// Update document file

export const updateDocumentFile = createAction(
  '[File/API] update document file',
  propsWithBlocking<{ documentId: string; file: File }>(),
);
export const updatedDocumentFile = createAction(
  '[File/API] updated document file',
  propsWithBlockingComplete<{ document: FileDto; previousId: string }>(updateDocumentFile),
);
export const failedUpdatedDocumentFile = createAction(
  '[File/API] failed updating document file',
  propsWithBlockingComplete<{ error: Error }>(updateDocumentFile),
);

// Delete document

export const deleteDocument = createAction(
  '[File/API] delete document',
  propsWithBlocking<{ documentId: string }>(),
);
export const deletedDocument = createAction(
  '[File/API] deleted document',
  propsWithBlockingComplete<{ documentId: string }>(deleteDocument, {
    success: true,
    message: 'DOCUMENT.ACTION.DELETED',
  }),
);
export const failedDeletingDocument = createAction(
  '[File/API] failed deleting document',
  propsWithBlockingComplete<{ error: Error }>(deleteDocument),
);

// assign document to task

export const assignToTask = createAction(
  '[File/API] assign to task',
  propsWithBlocking<{ documents: FileDto[]; taskId: string }>(),
);
export const assignedToTask = createAction(
  '[File/API] assigned to task',
  propsWithBlockingComplete<{ documents: FileDto[] }>(assignToTask),
);
export const failedAssigningToTask = createAction(
  '[File/API] failed assigning to task',
  propsWithBlockingComplete<{ error: Error }>(assignToTask),
);

// unassign document from task

export const unassignFromTask = createAction(
  '[File/API] unassign from task',
  propsWithBlocking<{ documents: FileDto[]; taskId: string }>(),
);
export const unassignedFromTask = createAction(
  '[File/API] unassigned from task',
  propsWithBlockingComplete<{ documents: FileDto[] }>(unassignFromTask),
);
export const failedUnassigningFromTask = createAction(
  '[File/API] failed unassigning from task',
  propsWithBlockingComplete<{ error: Error }>(unassignFromTask),
);

// table

export const loadDocumentTable = createAction(
  '[File/API] load document table',
  propsWithLoading<{
    tableId?: string;
    filter?: FilesFilter;
    sorting?: Sorting;
  }>(),
);

export const loadedDocumentTable = createAction(
  '[File/API] loaded document table',
  propsWithLoadingComplete<{
    documents: FileDto[];
    tableId?: string;
    filter?: FilesFilter;
    paging?: Partial<PagingResponseMeta & { limit: number }>;
    sorting?: Sorting;
  }>(loadDocumentTable),
);

export const failedLoadingDocumentTable = createAction(
  '[File/API] failed loading document table',
  propsWithLoadingComplete<{ error: Error }>(loadDocumentTable),
);

export const removeDocumentTable = createAction(
  '[File/API] remove document table',
  props<{ tableId: string }>(),
);

export const uploadDocumentStarted = createAction(
  '[File/API] upload document started',
  props<{}>(),
);
