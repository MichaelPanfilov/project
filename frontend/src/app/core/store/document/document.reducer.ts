import { FileDto, FilesFilter } from '@common';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import { removeTable, TableState, upsertManyAndTable } from '../util';

import * as DocumentActions from './document.actions';

export const key = 'documents';

export interface State extends EntityState<FileDto>, TableState<FileDto, FilesFilter> {}

const adapter: EntityAdapter<FileDto> = createEntityAdapter<FileDto>();

const initialState: State = adapter.getInitialState({
  tables: {},
  tableIds: [],
});

const documentReducer = createReducer(
  initialState,
  on(DocumentActions.loadedDocuments, (state, { documents }) =>
    adapter.upsertMany(documents, state),
  ),
  on(DocumentActions.uploadedDocument, (state, { document }) => adapter.addOne(document, state)),
  on(DocumentActions.updatedDocument, DocumentActions.updatedDocumentFile, (state, { document }) =>
    adapter.updateOne({ id: document.id, changes: document }, state),
  ),
  on(DocumentActions.deletedDocument, (state, { documentId }) =>
    adapter.removeOne(documentId, state),
  ),
  on(DocumentActions.assignedToTask, DocumentActions.unassignedFromTask, (state, { documents }) =>
    adapter.updateMany(
      documents.map(({ id, refIds }) => ({ id, changes: { refIds } })),
      state,
    ),
  ),
  // table
  // table
  on(
    DocumentActions.loadedDocumentTable,
    (state, { tableId, documents, filter, paging, sorting }) =>
      upsertManyAndTable(
        adapter.upsertMany,
        state,
        {
          items: documents,
          filter: filter || {},
          paging: paging || {},
          sorting,
        },
        tableId,
      ),
  ),
  on(DocumentActions.removeDocumentTable, (state, { tableId }) =>
    removeTable<FileDto, FilesFilter, State>(tableId, state),
  ),
);

export function reducer(state: State | undefined, action: Action) {
  return documentReducer(state, action);
}

export const { selectAll } = adapter.getSelectors();
