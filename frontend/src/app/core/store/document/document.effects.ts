import { HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataResponse, DOCUMENT_FILE_TAG, FileDto } from '@common';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { forkJoin, Observable, of } from 'rxjs';
// avoid shadowing variables
import { filter as rxFilter } from 'rxjs/operators';
import { catchError, exhaustMap, map, mergeMap, switchMap, take, tap } from 'rxjs/operators';

import { FileService } from '../../services/api/file.service';
import * as SettingsActions from '../settings/settings.actions';
import { State } from '../state';

import * as DocumentActions from './document.actions';
import * as DocumentSelectors from './document.selectors';

// avoid circular dependency

@Injectable()
export class DocumentEffects {
  private addTask(taskId: string, docs?: FileDto[]): Observable<FileDto[]> {
    if (docs && docs.length) {
      return forkJoin(
        docs.map(doc => {
          const refIds = doc.refIds ? [...doc.refIds, taskId] : [taskId];
          return this.fileService.updateFile({ refIds }, doc.id);
        }),
      );
    }
    return of([]);
  }

  private removeTask(taskId: string, docs?: FileDto[]): Observable<FileDto[]> {
    if (docs && docs.length) {
      return forkJoin(
        docs.map(doc => {
          const refIds = doc.refIds ? [...doc.refIds.filter(id => id !== taskId)] : undefined;
          return this.fileService.updateFile({ refIds }, doc.id);
        }),
      );
    }
    return of([]);
  }

  private pipeProgress(
    obs$: Observable<HttpEvent<DataResponse<FileDto>>>,
  ): Observable<DataResponse<FileDto>> {
    return obs$.pipe(
      tap(e => {
        if (e.type === HttpEventType.UploadProgress) {
          const uploadProgress = Math.round((100 * e.loaded) / (e.total || 0));
          this.store.dispatch(SettingsActions.updateFileUploadProgress({ uploadProgress }));
        }
      }),
      rxFilter(e => e.type === HttpEventType.Response),
      map(res => (res as HttpResponse<DataResponse<FileDto>>).body as DataResponse<FileDto>),
      tap(() => SettingsActions.updateFileUploadProgress({ uploadProgress: 0 })),
    );
  }

  loadDocuments = createEffect(() =>
    this.actions$.pipe(
      ofType(DocumentActions.loadDocuments),
      mergeMap(({ filter }) =>
        this.fileService.getDocuments({ ...filter, tag: [DOCUMENT_FILE_TAG] }),
      ),
      map(({ documents }) => DocumentActions.loadedDocuments({ documents })),
      catchError(error => of(DocumentActions.failedLoadingDocuments({ error }))),
    ),
  );

  deleteDocument = createEffect(() =>
    this.actions$.pipe(
      ofType(DocumentActions.deleteDocument),
      exhaustMap(({ documentId }) =>
        this.fileService.deleteFile(documentId).pipe(
          switchMap(() => [
            DocumentActions.deletedDocument({ documentId }),
            DocumentActions.loadDocumentTable({}),
          ]),
          catchError(error => of(DocumentActions.failedDeletingDocument({ error }))),
        ),
      ),
    ),
  );

  uploadDocument = createEffect(() =>
    this.actions$.pipe(
      ofType(DocumentActions.uploadDocument),
      exhaustMap(({ title, refIds, machineTypeTags, file }) =>
        this.pipeProgress(
          this.fileService.uploadFileProgress(
            file,
            refIds,
            DOCUMENT_FILE_TAG,
            title,
            machineTypeTags,
          ),
        ).pipe(
          switchMap(res => [
            DocumentActions.uploadedDocument({
              document: res.data,
            }),
            DocumentActions.loadDocumentTable({}),
          ]),
          catchError(error => of(DocumentActions.failedUploadingDocument({ error }))),
        ),
      ),
    ),
  );

  updateDocument = createEffect(() =>
    this.actions$.pipe(
      ofType(DocumentActions.updateDocument),
      exhaustMap(({ title, refIds, machineTypeTags, documentId }) => {
        return this.fileService
          .updateFile(
            { refIds, tags: [DOCUMENT_FILE_TAG], title, machineTypeTags } as Partial<FileDto>,
            documentId,
          )
          .pipe(
            switchMap(document => [
              DocumentActions.updatedDocument({ document }),
              DocumentActions.loadDocumentTable({}),
            ]),
            catchError(error => of(DocumentActions.failedUpdatingDocument({ error }))),
          );
      }),
    ),
  );

  assignToTask = createEffect(() =>
    this.actions$.pipe(
      ofType(DocumentActions.assignToTask),
      exhaustMap(({ documents, taskId }) => this.addTask(taskId, documents)),
      map(updatedDocuments => DocumentActions.assignedToTask({ documents: updatedDocuments })),
      catchError(error => of(DocumentActions.failedAssigningToTask({ error }))),
    ),
  );

  unassignFromTask = createEffect(() =>
    this.actions$.pipe(
      ofType(DocumentActions.unassignFromTask),
      exhaustMap(({ documents, taskId }) => this.removeTask(taskId, documents)),
      map(updatedDocuments => DocumentActions.unassignedFromTask({ documents: updatedDocuments })),
      catchError(error => of(DocumentActions.failedUnassigningFromTask({ error }))),
    ),
  );

  updateDocumentFile = createEffect(() =>
    this.actions$.pipe(
      ofType(DocumentActions.updateDocumentFile),
      exhaustMap(({ documentId, file }) =>
        this.pipeProgress(this.fileService.updateVersion(documentId, file)).pipe(
          switchMap(res => [
            DocumentActions.updatedDocumentFile({ document: res.data, previousId: documentId }),
            DocumentActions.loadDocumentTable({}),
          ]),
          catchError(error => of(DocumentActions.failedUpdatedDocumentFile({ error }))),
        ),
      ),
    ),
  );

  loadDocumentTable = createEffect(() =>
    this.actions$.pipe(
      ofType(DocumentActions.loadDocumentTable),
      switchMap(({ tableId, filter, sorting }) =>
        this.store.select(DocumentSelectors.selectTableMeta(tableId)).pipe(
          take(1),
          switchMap(meta =>
            this.fileService.getDocuments(filter || meta.filter, sorting || meta.sorting).pipe(
              map(res =>
                DocumentActions.loadedDocumentTable({
                  tableId,
                  documents: res.documents,
                  filter: filter || meta.filter,
                  paging: {
                    ...res.meta,
                    limit: filter ? filter.limit || meta.paging.limit : meta.paging.limit,
                  },
                  sorting: sorting || meta.sorting,
                }),
              ),
              catchError(error => of(DocumentActions.failedLoadingDocumentTable({ error }))),
            ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private store: Store<State>,
    private fileService: FileService,
  ) {}
}
