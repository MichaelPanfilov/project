import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { AssetDto, AssetType } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';

import { State } from '../store';
import * as Asset from '../store/asset';

@Injectable({ providedIn: 'root' })
export class AssetResolver implements Resolve<AssetDto> {
  constructor(private store: Store<State>, private actions$: Actions) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<AssetDto> {
    const [id] = ['lineId', 'machineId']
      .map(param => route.paramMap.get(param))
      .filter(param => !!param);

    if (!id) {
      return throwError(new Error());
    }

    return this.store.select(Asset.selectLoadedOnce).pipe(
      filter(loaded => loaded),
      take(1),
      switchMap(() =>
        this.store.select(Asset.selectAllAssets).pipe(
          map(assets => assets.find(a => a.id === id)),
          switchMap(asset => {
            if (asset) {
              return of(asset);
            }
            this.store.dispatch(Asset.loadAsset({ id }));
            return this.actions$.pipe(
              ofType(Asset.loadedAsset, Asset.failedLoadingAsset),
              take(1),
              map(res => {
                if (res.type === Asset.failedLoadingAsset.type) {
                  throw res.error;
                }
                return res.asset;
              }),
            );
          }),
          take(1),
          tap(asset => this.selectAssetsAndAncestors(asset)),
        ),
      ),
    );
  }

  private selectAssetsAndAncestors(asset: AssetDto) {
    // Make sure the resolved asset is set in store and if its a descendant select all ancestors.
    const payload = { id: asset.id };
    if (asset.type === AssetType.MACHINE) {
      this.store.dispatch(Asset.MachineActions.setCurrent(payload));
      combineLatest([
        this.store.select(Asset.selectParent(asset.id)),
        this.store.select(Asset.selectCurrentLine),
      ])
        .pipe(take(1))
        .subscribe(([parent, line]) => {
          if (parent && (!line || (parent && line.id !== parent.id))) {
            this.store.dispatch(Asset.LineActions.setCurrent({ id: parent.id }));
          }
        });
    } else if (asset.type === AssetType.LINE) {
      this.store.dispatch(Asset.MachineActions.setCurrent({}));
      this.store.dispatch(Asset.LineActions.setCurrent(payload));
      combineLatest([
        this.store.select(Asset.selectParent(asset.id)),
        this.store.select(Asset.selectCurrentFactory),
      ])
        .pipe(take(1))
        .subscribe(([parent, factory]) => {
          if (parent && (!factory || (parent && factory.id !== parent.id))) {
            this.store.dispatch(Asset.TreeActions.setCurrent({ id: parent.id }));
          }
        });
    } else if (asset.type === AssetType.FACTORY) {
      this.store.dispatch(Asset.TreeActions.setCurrent(payload));
      this.store.dispatch(Asset.LineActions.setCurrent({}));
      this.store.dispatch(Asset.MachineActions.setCurrent({}));
    }
  }
}
