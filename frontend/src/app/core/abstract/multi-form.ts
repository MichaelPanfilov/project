import { AfterViewInit, Directive } from '@angular/core';

@Directive()
export abstract class MultiForm implements AfterViewInit {
  disableAnimation = true;
  // Workaround for angular component issue https://github.com/angular/components/issues/13870
  // for reference https://stackblitz.com/edit/angular-issue13870-workaround

  ngAfterViewInit(): void {
    // timeout required to avoid the dreaded 'ExpressionChangedAfterItHasBeenCheckedError'
    setTimeout(() => (this.disableAnimation = false));
  }
}
