import { Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AssetDto, AssetType, ASSET_TYPE_LABELS, BaseAssetTree, toTranslatable } from '@common';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, startWith, switchMap, take, takeUntil } from 'rxjs/operators';
import { buildForm, DEFAULT_VALIDATORS, FormConfig } from 'src/app/util';

import { Asset, State } from '../store';

import { FormComponent } from './form';

@Directive()
export abstract class AssetForm<T extends AssetDto> extends FormComponent<T>
  implements OnInit, OnDestroy {
  protected abstract type: AssetType;

  private asset$ = new BehaviorSubject<T | undefined>(undefined);

  protected assetFormConfig = {
    id: [{ value: '', disabled: true }],
    type: ['', Validators.required],
    name: ['', DEFAULT_VALIDATORS],
    description: [''],
    position: [0],
  } as FormConfig<T>;

  @Input()
  set asset(asset: T | undefined) {
    this.asset$.next(asset);
  }

  get asset() {
    return this.asset$.value;
  }

  @Input()
  isEditMode!: boolean;

  assetType = toTranslatable(ASSET_TYPE_LABELS);

  parent$: Observable<BaseAssetTree | undefined> = of(undefined);
  siblings$: Observable<AssetDto[]> = of([]);
  predecessor$: Observable<AssetDto | undefined> = of(undefined);

  get parent() {
    const ctrl = this.getParentControl();
    return ctrl ? ctrl.value : undefined;
  }

  constructor(protected fb: FormBuilder, protected store: Store<State>, private actions$: Actions) {
    super(fb);
  }

  ngOnInit(): void {
    super.ngOnInit();
    // Fix the type, it cannot be changed by the user.
    this.getControl('type').patchValue(this.type);
    this.updateCopy();
    this.asset$.pipe(takeUntil(this.destroy$)).subscribe(asset => {
      buildForm(this.fb, this.formConfig, asset);
      this.initObs();
    });
  }

  setParent(parent: AssetDto) {
    const ctrl = this.getParentControl();
    if (ctrl && parent.type === AssetType.FACTORY) {
      ctrl.setValue(parent.id);
    }
  }

  getParentControl(): AbstractControl | undefined {
    // Not available for factories.
    return this.form.controls['parent'];
  }

  protected provideFormObj() {
    return this.asset;
  }

  protected handleSubmit(payload: T): Observable<T> {
    const action = payload.id ? Asset.updateAsset : Asset.createAsset;
    this.store.dispatch(action({ asset: payload }));

    return this.actions$.pipe(
      ofType(Asset.createdAsset, Asset.updatedAsset),
      take(1),
      map(({ asset }) => asset as T),
    );
  }

  private initObs() {
    // Not available for factories.
    const parentCtrl = this.getParentControl();
    if (parentCtrl) {
      this.parent$ = parentCtrl.valueChanges.pipe(
        startWith(this.parent),
        switchMap((parent: string) =>
          parent ? this.store.select(Asset.selectAssetTree(parent)) : of(undefined),
        ),
      );
    }

    this.siblings$ = this.parent$.pipe(
      switchMap(parent => {
        if (this.type === 'factory') {
          return this.store.select(Asset.selectAllAssetTrees);
        }
        return of(parent ? parent.children || [] : []);
      }),
      map(siblings => [...siblings].filter(sibling => !this.asset || this.asset.id !== sibling.id)),
    );

    this.predecessor$ = this.siblings$.pipe(
      map(siblings =>
        siblings
          .reverse()
          .find(
            sibling => this.asset && this.asset.position && this.asset.position > sibling.position,
          ),
      ),
    );
  }
}
