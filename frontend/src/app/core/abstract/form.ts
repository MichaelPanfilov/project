import { Directive, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { isEqual } from 'lodash';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  startWith,
  take,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { buildForm, FormConfig } from 'src/app/util';

@Directive()
export abstract class FormComponent<TForm extends object, TObject extends object = TForm>
  implements OnInit, OnDestroy {
  private copy?: TForm;
  protected destroy$ = new Subject<void>();
  initialized$ = new Subject<void>();

  protected abstract formConfig: FormConfig<TForm>;

  @Input()
  formId!: string;

  @Output()
  created = new EventEmitter<TObject>();

  @Output()
  valid = new EventEmitter<boolean>();

  @Output()
  changed = new EventEmitter<boolean>();

  form!: FormGroup;

  constructor(protected fb: FormBuilder) {}

  protected abstract provideFormObj(): Partial<TForm> | undefined;
  protected abstract handleSubmit(payload: TForm): Observable<TObject | undefined>;

  // tslint:disable-next-line
  protected patchValue(key: keyof TForm, value: any) {
    this.form.patchValue({ [key]: value });
  }

  ngOnInit() {
    this.form = buildForm<TForm>(this.fb, this.formConfig, this.provideFormObj());

    this.form.markAllAsTouched();
    this.form.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(100),
        tap(() => this.changed.emit(this.hasBeenUpdated())),
        startWith(null),
        map(() => this.form.valid),
        distinctUntilChanged(),
        debounceTime(10),
      )
      .subscribe(valid => this.valid.emit(valid));

    this.copy = this.form.value;
    this.initialized$.next();
    this.initialized$.complete();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getControl(key: keyof TForm): AbstractControl {
    return this.form.controls[key as string];
  }

  hasError(prop: string): boolean {
    const control = this.form.controls[prop];
    if (control) {
      return (control.dirty || control.touched) && !!control.errors;
    }
    return false;
  }

  onSubmit() {
    const payload = this.form.getRawValue() as TForm;
    this.handleSubmit(payload)
      .pipe(take(1))
      .subscribe(result => {
        if (result) {
          this.created.emit(result);
        }
      });
  }

  resetToInitialValue() {
    this.form.reset(this.provideFormObj());
  }

  updateCopy() {
    this.copy = this.form.value;
    this.changed.emit(false);
  }

  hasBeenUpdated() {
    return !!this.copy && !isEqual(this.copy, this.form.value);
  }
}
