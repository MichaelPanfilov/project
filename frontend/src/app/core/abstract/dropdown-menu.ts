import { ConnectedPosition, Overlay, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import {
  Directive,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, take, takeUntil } from 'rxjs/operators';

@Directive()
export class DropdownMenu implements OnInit, OnDestroy {
  private overlayRef?: OverlayRef;
  protected destroy$ = new Subject<void>();
  protected positions: ConnectedPosition[] = [
    { originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'top' },
    { originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'bottom' },
  ];
  protected defaultOffsetX = 0;

  @ViewChild('dropdownTemplate', { static: true })
  dropdownTemplate!: TemplateRef<HTMLDivElement>;

  @ViewChild('dropdownContainer')
  dropdownContainer?: ElementRef<HTMLDivElement>;

  isOpen = false;
  facesDown = true;

  constructor(
    private overlay: Overlay,
    private viewContainerRef: ViewContainerRef,
    private router: Router,
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(
        filter(ev => ev instanceof NavigationEnd),
        take(1),
      )
      .subscribe(() => this.hideMenu());
  }

  ngOnDestroy() {
    this.hideMenu();
    this.destroy$.next();
    this.destroy$.complete();
  }

  onOpenDropdown(e: Event) {
    e.preventDefault();
    e.stopPropagation();
    if (this.isOpen || !this.dropdownContainer) {
      return this.hideMenu();
    }

    this.isOpen = true;
    const positionStrategy = this.overlay
      .position()
      .flexibleConnectedTo(this.dropdownContainer)
      .withPositions(this.positions)
      .withViewportMargin(10)
      .withDefaultOffsetX(this.defaultOffsetX)
      .withPush(false);

    this.overlayRef = this.overlay.create({
      positionStrategy,
      scrollStrategy: this.overlay.scrollStrategies.reposition(),
      hasBackdrop: true,
      backdropClass: 'cdk-overlay-transparent-backdrop',
    });

    positionStrategy.positionChanges
      .pipe(
        takeUntil(this.destroy$),
        map(change => change.connectionPair.originY === 'top'),
        distinctUntilChanged(),
      )
      .subscribe(facesDown => (this.facesDown = facesDown));

    const dropdownPortal = new TemplatePortal(this.dropdownTemplate, this.viewContainerRef);

    this.overlayRef.attach(dropdownPortal);
    this.syncWidth();
  }

  hideMenu() {
    this.isOpen = false;

    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }

  @HostListener('window:resize')
  syncWidth() {
    if (!this.overlayRef || !this.dropdownContainer) {
      return;
    }

    const refRect = this.dropdownContainer.nativeElement.getBoundingClientRect();
    this.overlayRef.updateSize({ width: refRect.width });
    this.overlayRef.updatePosition();
  }
}
