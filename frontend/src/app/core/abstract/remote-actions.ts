import { AfterViewInit, Directive, OnDestroy, TemplateRef, ViewChild } from '@angular/core';

import { NavbarActionsViewService } from '../services/navbar-actions-view.service';

@Directive()
export class RemoteActions implements AfterViewInit, OnDestroy {
  @ViewChild('navbarActionsView')
  navbarActionsView?: TemplateRef<HTMLElement>;

  constructor(private navActions: NavbarActionsViewService) {}

  ngAfterViewInit() {
    const view = this.navbarActionsView;
    if (view) {
      // Prevents some 'ExpressionChangedAfterItHasBeenCheckedError'
      requestAnimationFrame(() => this.navActions.insertActionTemplate(view));
    }
  }

  ngOnDestroy() {
    if (this.navbarActionsView) {
      this.navActions.clearActionView();
    }
  }
}
