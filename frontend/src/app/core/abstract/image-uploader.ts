import { ElementRef, Renderer2 } from '@angular/core';
import { getExtension, isImage } from '@common';
import { of } from 'rxjs';
import { catchError, take } from 'rxjs/operators';

import { FileService } from '../services/api/file.service';
import { NotificationService, NotificationVariants } from '../services/notification.service';

export abstract class ImageUploader {
  protected uploadedImage?: File;
  protected backgroundSize = 'contain';

  abstract imgRef: ElementRef;

  imageId?: string;

  get hasImage() {
    return !!(this.imageId || this.uploadedImage);
  }

  constructor(
    protected fileService: FileService,
    protected notificationService: NotificationService,
    protected renderer: Renderer2,
  ) {}

  protected abstract getPlaceholder(hasImg: boolean): string;

  onFileChange(files: File[]) {
    const file = files[0];

    if (file) {
      const ext = getExtension(file.name);
      if (!isImage(ext)) {
        this.onError('COMMON.ERROR.UNSUPPORTED_IMAGE_TYPE');
        return;
      }
      if (file.size > 5120000) {
        this.onError('COMMON.ERROR.MAX_FILE_SIZE_EXCEEDED');
        return;
      }

      this.uploadedImage = file;

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => this.setImg(reader.result as ArrayBuffer);
    }
  }

  protected initImage(refId: string, tags?: string) {
    this.fileService
      .getFileByFilter(refId, tags)
      .pipe(
        take(1),
        catchError(() => of(undefined)),
      )
      .subscribe(img => {
        if (img) {
          this.imageId = img.id;
          this.setImg(img.publicUrl);
        }
      });
  }

  protected resetImg() {
    this.uploadedImage = undefined;
  }

  protected setImg(input: string | ArrayBuffer): void {
    const el = this.imgRef.nativeElement;
    this.renderer.setStyle(
      el,
      'background',
      `url(${input}) 50% 50% no-repeat, ${this.getPlaceholder(!!input)}`,
    );
    this.renderer.setStyle(el, 'background-size', this.backgroundSize);
  }

  protected onError(message: string) {
    this.notificationService.notify({
      message,
      variant: NotificationVariants.ERROR,
    });
  }
}
