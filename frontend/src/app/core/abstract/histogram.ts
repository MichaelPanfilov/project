import { Directive, Input, ViewChild } from '@angular/core';
import { TimeRange } from '@common';
import { ChartDataSets, ChartOptions } from 'chart.js';
import * as moment from 'moment';
import { BaseChartDirective } from 'ng2-charts';

const DEFAULT_OPTIONS: ChartOptions = {
  legend: {
    display: false,
    labels: {
      fontFamily: 'Syntegon, Roboto, Helvetica Neue, sans-serif',
    },
  },
  scales: {
    xAxes: [
      {
        type: 'linear',
        stacked: true,
      },
    ],
    yAxes: [
      {
        ticks: {
          stepSize: 1,
        },
      },
    ],
  },
  tooltips: {
    enabled: false,
  },
  animation: {
    duration: 0,
  },
  responsive: true,
  maintainAspectRatio: false,
};

@Directive()
export abstract class HistogramComponent<T> {
  protected data: T[] = [];
  protected timeRange: TimeRange = { from: new Date(), to: new Date() };
  protected highlighted: string | null = null;

  @ViewChild(BaseChartDirective, { static: true })
  chart!: BaseChartDirective;

  @Input()
  numSegments = 20;

  @Input()
  set range(range: TimeRange) {
    this.timeRange = range;
    const timeSegments = this.getTimeSegments(range, this.numSegments);
    this.labels = timeSegments.map((seg, i, arr) => {
      const next = arr[i + 1] || range.to;
      return `${seg.toISOString()} ${next.toISOString()}`;
    });
    this.buildHistogram(this.data, range);
  }

  get range() {
    return this.timeRange;
  }

  @Input()
  set highlightedReason(reason: string | null) {
    this.highlighted = reason;
    this.buildHistogram(this.data, this.range);
  }

  labels: string[] = [];
  datasets: ChartDataSets[] = [
    {
      data: [],
    },
  ];
  options: ChartOptions = {
    ...DEFAULT_OPTIONS,
  };

  protected abstract getChartDataSets(data: T[], range: TimeRange): ChartDataSets[];

  protected buildHistogram(data: T[], range: TimeRange) {
    if (!data || data.length < 1) {
      return;
    }

    this.datasets = this.getChartDataSets(data, range);

    const options: ChartOptions = {
      scales: {
        xAxes: [
          {
            id: 'xAxis1',
            offset: true,
            stacked: true,
            distribution: 'linear',
            gridLines: {
              display: false,
            },
            ticks: {
              autoSkip: false,
              padding: 5,
              lineHeight: 2,
              min: range.from,
              max: range.to,
              callback: (label: string) => {
                const [from, to] = label.split(' ').map(l => moment(l));
                return `${from.format('HH:mm')} - ${to.format('HH:mm')}`;
              },
            },
          },
        ],
        yAxes: [
          {
            stacked: true,
            ticks: {
              maxTicksLimit: Math.floor(this.numSegments / 2),
            },
          },
        ],
      },
    };

    if (this.spansMultipleDays() && options.scales && options.scales.xAxes) {
      options.scales.xAxes.push({
        id: 'xAxis2',
        offset: true,
        stacked: true,
        distribution: 'linear',
        gridLines: {
          drawOnChartArea: false,
        },
        ticks: {
          autoSkip: true,
          maxTicksLimit: this.numSegments,
          padding: 10,
          min: range.from,
          max: range.to,
          maxRotation: 0,
          callback: (label: string, index: number, values: string[]) => {
            const next = values[index + 1];
            if (next) {
              const [from, to] = next.split(' ').map(l => moment(l));
              if (!from.startOf('day').isSame(to.startOf('day'))) {
                return from.format('DD/MM');
              }
            }

            return '';
          },
        },
      });
    }
    this.updateChartOptions(options);

    this.chart.update();
  }

  protected updateChartOptions(opts: ChartOptions) {
    this.options = {
      ...this.options,
      ...opts,
    };
  }

  protected getTimeSegments(range: TimeRange, num: number): Date[] {
    const firstTs = range.from.getTime();
    const durationMs = range.to.getTime() - firstTs;
    return [...new Array(num).keys()].map(
      key => new Date((Number(key) / num) * durationMs + firstTs),
    );
  }

  protected spansMultipleDays(): boolean {
    return this.range.to.getTime() - this.range.from.getTime() > 24 * 60 * 60 * 1000;
  }
}
