import { Directive, Input } from '@angular/core';
import {
  AssetDto,
  AssetType,
  ASSET_TYPE_LABELS,
  BaseAssetTree,
  flattenAssets,
  sortByPosition,
} from '@common';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest, of } from 'rxjs';
import { filter, map, startWith, switchMap } from 'rxjs/operators';

import { Asset, State } from '../store';

@Directive()
export abstract class AbstractAssetView<T extends AssetDto> {
  asset$ = new BehaviorSubject<T | undefined>(undefined);

  @Input()
  set asset(asset: T | undefined) {
    this.asset$.next(asset);
  }

  get asset(): T | undefined {
    return this.asset$.value;
  }

  type$ = this.asset$.pipe(
    filter(asset => !!asset),
    map(asset => ASSET_TYPE_LABELS[(asset as T).type]),
    startWith(''),
  );

  parent$ = this.asset$.pipe(
    filter(asset => !!asset),
    switchMap(asset => this.store.select(Asset.selectParentTree((asset as T).id))),
    startWith(undefined),
  );

  predecessor$ = combineLatest([this.asset$, this.parent$]).pipe(
    filter(([asset]) => !!asset),
    switchMap(([asset, parent]) => {
      if ((asset as T).type === AssetType.FACTORY) {
        return this.store.select(Asset.selectAllAssetTrees);
      }
      return of(parent ? parent.children || [] : []);
    }),
    map((siblings: BaseAssetTree[]) =>
      [...siblings]
        .sort(sortByPosition)
        .find(sibling => this.asset && this.asset.position > sibling.position),
    ),
  );

  children$ = this.asset$.pipe(
    filter(asset => !!asset),
    switchMap(asset =>
      this.store.select(Asset.selectAssetTree((asset as T).id)).pipe(
        map(tree => (tree && tree.children ? flattenAssets(tree.children) : [])),
        map(assets => assets.map(a => a.name)),
      ),
    ),
  );

  constructor(protected store: Store<State>) {}
}
