import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { NotificationService, NotificationVariants } from '../services/notification.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {}

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(req).pipe(catchError(err => this.tryRenew(err, req)));
  }

  private tryRenew(
    err: HttpErrorResponse,
    req: HttpRequest<unknown>,
  ): Observable<HttpEvent<unknown>> {
    if (![401, 403].includes(err.status) || req.url !== `${environment.aclServiceUrl}/v1/renew`) {
      const message =
        err.error.error && err.error.error.message
          ? err.error.error.message
          : 'COMMON.ERROR.FAILED_REQUEST';

      this.injector
        .get(NotificationService)
        .notify({ message, variant: NotificationVariants.ERROR });
    }
    return throwError(err);
  }
}
