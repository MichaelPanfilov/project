import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JWTPayload } from '@common';
import { from, Observable, throwError } from 'rxjs';
import { catchError, finalize, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { loginPath } from '../navigation';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private init: Promise<void>;
  private token?: string;
  private isRetry = false;

  constructor(private authService: AuthService, private router: Router) {
    this.init = new Promise(res => {
      this.authService.token$.subscribe(token => {
        this.token = token;
        res();
      });
    });
  }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return from(this.init).pipe(
      switchMap(() => {
        if (this.token) {
          req = req.clone({ headers: req.headers.set('Authorization', `Bearer ${this.token}`) });
        }
        return next.handle(req).pipe(catchError(err => this.tryRenew(err, req, next)));
      }),
    );
  }

  private tryRenew(
    err: HttpErrorResponse,
    req: HttpRequest<unknown>,
    next: HttpHandler,
  ): Observable<HttpEvent<unknown>> {
    if (![401, 403].includes(err.status)) {
      // Only handle unauthorized errors from acl service.
      return throwError(err);
    }
    if (
      !this.isRetry &&
      req.url !== `${environment.aclServiceUrl}/v1/renew` &&
      req.url !== `${environment.aclServiceUrl}/v1/logout_all`
    ) {
      this.isRetry = true;
      return this.authService.jwt$.pipe(switchMap(this.renewTokenIfValid(err, req, next)));
    }
    return throwError(err);
  }

  private renewTokenIfValid(err: HttpErrorResponse, req: HttpRequest<unknown>, next: HttpHandler) {
    return (tokenData?: JWTPayload) => {
      if (!tokenData || tokenData.exp * 1000 >= Date.now()) {
        this.isRetry = false;
        return throwError(err);
      }

      return this.authService.renew().pipe(
        switchMap(() => {
          this.isRetry = false;
          return next.handle(req);
        }),
        catchError(e => {
          this.isRetry = false;
          return this.authService.logout().pipe(
            finalize(() => this.router.navigate(loginPath())),
            switchMap(() => throwError(e)),
          );
        }),
      );
    };
  }
}
