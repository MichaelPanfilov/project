import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatNumber',
})
export class FormatNumberPipe implements PipeTransform {
  transform(value: number | null): string {
    if (!value) {
      value = 0;
    }
    return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1'");
  }
}
