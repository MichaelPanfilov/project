import { ChangeDetectorRef, OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

const DEFAULT_MAX_DISPLAYED = 2;

@Pipe({
  name: 'hideMore',
  pure: false,
})
export class HideMorePipe implements PipeTransform, OnDestroy {
  private sub?: Subscription;
  private value = '';

  constructor(private translate: TranslateService, private cdRef: ChangeDetectorRef) {}

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  transform(value: string | string[] | null | undefined, maxDisplayed?: string | number): string {
    if (!value) {
      this.value = '';
    } else if (!Array.isArray(value)) {
      this.value = value;
    } else {
      const max = maxDisplayed ? strToNumber(maxDisplayed) : DEFAULT_MAX_DISPLAYED;
      const str = [...value].splice(0, max).join(', ');
      this.sub = this.translate
        .get('COMMON.LABEL.MORE')
        .pipe(map(trans => (value.length > max ? str + `, + ${value.length - max} ${trans}` : str)))
        .subscribe(res => {
          this.value = res;
          this.cdRef.markForCheck();
        });
    }
    return this.value;
  }
}

function strToNumber(value: number | string): number {
  // tslint:disable-next-line
  if (typeof value === 'string' && !isNaN(Number(value) - parseFloat(value))) {
    return Number(value);
  }
  if (typeof value !== 'number') {
    throw new Error(`${value} is not a number`);
  }
  return value;
}
