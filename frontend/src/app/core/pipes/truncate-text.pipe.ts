import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncateText',
})
export class TruncateTextPipe implements PipeTransform {
  transform(value: string | null, limit = 40, trail = '…'): string {
    let result = value || '';
    if (value) {
      if (value.length > limit) {
        if (limit < 0) {
          limit *= -1;
          result = trail + value.slice(value.length - limit, value.length);
        } else {
          result = value.slice(0, limit) + trail;
        }
      }
    }
    return result;
  }
}
