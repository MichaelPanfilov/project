import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { createConnection, QueryRunner } from 'typeorm';

import { ConfigService } from '../src/config/config.service';

async function startConnection(config: ConfigService): Promise<QueryRunner> {
  const conn = await createConnection({
    type: 'mysql',
    host: config.database.host,
    port: config.database.port,
    username: config.database.user,
    password: config.database.pass,
    database: config.database.name,
    ssl: config.database.ssl,
    namingStrategy: new SnakeNamingStrategy(),
  });

  // Apparently not required? But causes sporadic errors when not present...
  // await conn.connect();

  return conn.createQueryRunner();
}

async function endConnection(queryRunner: QueryRunner): Promise<void> {
  const conn = queryRunner.connection;
  await queryRunner.release();
  await conn.close();
}

export async function createTestDatabase(config: ConfigService): Promise<string> {
  const testDbName = '___CHANGEME___test_' + Math.round(Math.random() * 1e9);
  const queryRunner = await startConnection(config);

  await queryRunner.createDatabase(testDbName);

  await endConnection(queryRunner);

  return testDbName;
}

export async function dropTestDatabase(dbName: string, config: ConfigService): Promise<void> {
  const queryRunner = await startConnection(config);

  await queryRunner.dropDatabase(dbName);

  await endConnection(queryRunner);
}
