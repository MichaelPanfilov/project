import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { Connection, Repository, getConnection } from 'typeorm';
import * as moment from 'moment';

import { TodoEntity } from '../../src/todo/todo/todo.entity';
import { AppModule } from '../../src/app.module';
import { ConfigService } from '../../src/config/config.service';
import { createTestDatabase, dropTestDatabase } from '../util';

describe('TodoController (e2e)', () => {
  let testDbName: string;
  let app: INestApplication;
  let connection: Connection;
  let todoRepo: Repository<TodoEntity>;

  beforeEach(async () => {
    testDbName = await createTestDatabase(new ConfigService());

    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    connection = getConnection();

    todoRepo = connection.getRepository(TodoEntity);

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterEach(async () => {
    await app.close();

    await dropTestDatabase(testDbName, new ConfigService());
  });

  describe('GET /todo/:todoId', () => {
    let insertedTodo: TodoEntity;

    beforeEach(async () => {
      insertedTodo = await todoRepo.save({
        title: 'title',
        description: 'description',
        dueDate: moment()
          .add(1, 'd')
          .toISOString(),
      });
    });

    it('should return 200', async () => {
      return request(app.getHttpServer())
        .get('/todos/' + insertedTodo.id)
        .expect(200);
    });

    it('should return a dataresponse containing the todo', async () => {
      return request(app.getHttpServer())
        .get('/todos/' + insertedTodo.id)
        .expect(response => {
          expect(response.body).toMatchObject({
            data: expect.objectContaining({
              title: 'title',
              description: 'description',
              dueDate: moment.utc(insertedTodo.dueDate).toISOString(),
            }),
          });
        });
    });
  });
});
