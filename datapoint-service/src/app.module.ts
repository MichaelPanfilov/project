import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import * as config from '../ormconfig';

import { DatapointModule } from './datapoint/datapoint.module';
import { LicenseController } from './license.controller';
import { MappingModule } from './mapping/mapping.module';

@Module({
  imports: [TypeOrmModule.forRoot(config), DatapointModule, MappingModule],
  providers: [],
  controllers: [LicenseController],
  exports: [],
})
export class AppModule {}
