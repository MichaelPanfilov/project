/* eslint-disable no-console */ // We don't have a logger available everywhere in this file.

require('module-alias/register');
require('dotenv/config');

process.env.TZ = 'UTC';

import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

import { AppModule } from './app.module';
import { ResponseInterceptor, authMiddleware } from './util';
import { DatapointModule } from './datapoint/datapoint.module';
import { MappingModule } from './mapping/mapping.module';

console.log(`${new Date().toISOString()} Starting up`);
console.log(
  `${new Date().toISOString()} NODE_ENV=${process.env.NODE_ENV}`,
  `LOG_LEVEL=${process.env.LOG_LEVEL}`,
);

(async (): Promise<void> => {
  const app = await NestFactory.create(AppModule, { cors: true });

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
    }),
  );

  authMiddleware(app);
  app.useGlobalInterceptors(new ResponseInterceptor());

  const options1 = new DocumentBuilder()
    .setTitle('Shopfloor Datapoint service')
    .setDescription('API documentation of all integrated modules')
    .addBearerAuth()
    .setVersion('3.0')
    .build();
  SwaggerModule.setup(
    '/docs/datapoints',
    app,
    SwaggerModule.createDocument(app, options1, {
      include: [DatapointModule],
    }),
  );

  const options2 = new DocumentBuilder()
    .setTitle('Shopfloor Mapping service')
    .setDescription('API documentation of all integrated modules')
    .addBearerAuth()
    .setVersion('3.0')
    .build();
  SwaggerModule.setup(
    '/docs/mapping',
    app,
    SwaggerModule.createDocument(app, options2, {
      include: [MappingModule],
    }),
  );

  const port = (process.env.APP_PORT as string) || '4040';
  await app.listen(port);
  console.log(`[${new Date()}] Listening on port ${port}`);
})().catch(err => {
  console.error(`${new Date().toISOString()} Fatal error during startup`, err);
  process.exit(1);
});
