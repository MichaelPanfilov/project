import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFile,
  Param,
  Put,
  Get,
  BadRequestException,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { MappingType } from '@common';
import {
  ApiOkResponse,
  ApiCreatedResponse,
  ApiConsumes,
  ApiTags,
  ApiBearerAuth,
} from '@nestjs/swagger';

import { getResponseFor, ApiFile } from '../util';

import { MappingService } from './mapping.service';
import { Mapping } from './entities/mapping.entity';

export interface UploadedFile {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  size: number;
  destination: string;
  filename: string;
  path: string;
}

@Controller('v1/mappings')
@ApiTags('Mapping')
@ApiBearerAuth()
export class MappingController {
  constructor(private mapperService: MappingService) {}

  @Get()
  @ApiOkResponse()
  getMappings(): Promise<Mapping[]> {
    return this.mapperService.getMappings();
  }

  @Post(':deviceId/upload/reject-reasons')
  @UseInterceptors(FileInterceptor('file'))
  @ApiFile()
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({ type: getResponseFor(Object) })
  uploadRejectReasonsMap(
    @Param('deviceId') device: string,
    @UploadedFile() file: UploadedFile,
  ): Promise<void> {
    this.assertFile(file);
    return this.mapperService.saveFromCsv(MappingType.REJECT_REASONS, device, file.path);
  }

  @Post(':deviceId/upload/fault-stop-reasons')
  @UseInterceptors(FileInterceptor('file'))
  @ApiFile()
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({ type: getResponseFor(Object) })
  uploadStopReasonsMap(
    @Param('deviceId') device: string,
    @UploadedFile() file: UploadedFile,
  ): Promise<void> {
    this.assertFile(file);
    return this.mapperService.saveFromCsv(MappingType.STOP_REASONS, device, file.path);
  }

  @Put(':id')
  @UseInterceptors(FileInterceptor('file'))
  @ApiFile()
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({ type: getResponseFor(Object) })
  updateStopReasonsMap(@Param('id') id: string, @UploadedFile() file: UploadedFile): Promise<void> {
    this.assertFile(file);
    return this.mapperService.updateFromCsv(id, file.path);
  }

  private assertFile(file: UploadedFile) {
    if (!file) {
      throw new BadRequestException('File missing');
    }
  }
}
