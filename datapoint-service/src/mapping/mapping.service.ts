import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import * as csv from 'csvtojson';
import { MappingData, MappingType } from '@common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindConditions, MoreThan } from 'typeorm';

import { Mapping } from './entities/mapping.entity';

interface CsvFormatData {
  ID: string;
  EN: string;
  [key: string]: string;
}

@Injectable()
export class MappingService {
  constructor(@InjectRepository(Mapping) private repo: Repository<Mapping>) {}

  getMappings(): Promise<Mapping[]> {
    return this.repo.find();
  }

  async findMappings(type: MappingType, device: string, since?: Date): Promise<Mapping[]> {
    const where: FindConditions<Mapping> = { type, device };
    if (since) {
      where.createdAt = MoreThan(since);
    }

    const mappings = await this.repo.find({ where, order: { createdAt: 'DESC' } });
    // Sort mappings from newest to oldest.
    mappings.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
    return mappings;
  }

  async saveFromCsv(type: MappingType, device: string, filePath: string): Promise<void> {
    const result: CsvFormatData[] = await csv({ output: 'json', delimiter: ';' }).fromFile(
      filePath,
    );

    const data = this.parseCsv(result);
    await this.repo.save({ type, device, data });
  }

  async updateFromCsv(id: string, filePath: string): Promise<void> {
    await this.repo.findOneOrFail(id);
    const result: CsvFormatData[] = await csv({ output: 'json', delimiter: ';' }).fromFile(
      filePath,
    );

    const data = this.parseCsv(result);
    await this.repo.update(id, { data });
  }

  private parseCsv(data: CsvFormatData[]): MappingData {
    // Check data to have the minimum required properties.
    const isInvalid = !!data.find(row => !row.ID || !row.EN);
    if (isInvalid) {
      throw new UnprocessableEntityException('Corrupted file');
    }

    return data.reduce((prev, curr) => {
      // We need to normalize the id from the csv.
      // In the csv it looks like '01.021.005', the machine however sends '1021005'.
      // We remove . and leading 0.
      const { ID, EN, ...rest } = curr;
      const id = this.replaceLeadingZero(ID.trim().replace(/\./g, ''));
      return {
        ...prev,
        [Number(id)]: {
          en: EN,
          ...this.getAdditionalLangs(rest),
        },
      };
    }, {} as MappingData);
  }

  private replaceLeadingZero(id: string): string {
    if (id.startsWith('0')) {
      id = id.substr(1);
      return this.replaceLeadingZero(id);
    }
    return id;
  }

  private getAdditionalLangs(obj: { [key: string]: string }): { [key: string]: string } {
    return Object.entries(obj).reduce(
      (prev, [key, value]) => ({
        ...prev,
        [key.toLowerCase()]: value,
      }),
      {},
    );
  }
}
