import * as path from 'path';

import { Module, BadRequestException } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MulterModule } from '@nestjs/platform-express';

import { MappingService } from './mapping.service';
import { MappingController } from './mapping.controller';
import { Mapping } from './entities/mapping.entity';

const ALLOWED_FILE_TYPES = ['csv'];

@Module({
  imports: [
    TypeOrmModule.forFeature([Mapping]),
    MulterModule.register({
      dest: path.join(__dirname, '../../data'),
      fileFilter: (req, file, cb) => {
        const isAllowed = ALLOWED_FILE_TYPES.some(
          type => file.mimetype.includes(type) || file.originalname.endsWith(type),
        );
        if (isAllowed) {
          cb(null, true);
        } else {
          cb(new BadRequestException('Unsupported file type'), false);
        }
      },
      limits: {
        fileSize: process.env.MAX_FILE_SIZE ? Number(process.env.MAX_FILE_SIZE) : 20 * 1048576, // 20MB default
      },
    }),
  ],
  providers: [MappingService],
  controllers: [MappingController],
  exports: [MappingService],
})
export class MappingModule {}
