import { MappingDto, MappingType, MappingData } from '@common';
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
} from 'typeorm';
import { ApiResponseProperty, ApiProperty } from '@nestjs/swagger';

@Entity()
export class Mapping implements MappingDto {
  @PrimaryGeneratedColumn('uuid')
  @ApiResponseProperty()
  id!: string;

  @ApiResponseProperty()
  @CreateDateColumn()
  createdAt!: Date;

  @ApiResponseProperty()
  @UpdateDateColumn()
  updatedAt!: Date;

  @ApiResponseProperty()
  @Column()
  device!: string;

  @ApiResponseProperty()
  @Column({ type: 'enum', enum: MappingType })
  type!: MappingType;

  @ApiResponseProperty()
  @Column({ type: 'json' })
  data!: MappingData;
}
