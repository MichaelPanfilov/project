import { SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';

export interface Output {
  timestamp: Date;
  id: number;
}

export class FaultStopPipeline extends Pipeline<SynBaseInput<string>, Output> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return !!input.topic.match(/StopReason\/ID/) && Number(input.value) > 0;
  }

  transformData(input: SynBaseInput<string>): Output {
    return {
      timestamp: input.timestamp,
      id: Number(input.value),
    };
  }
}
