import { Injectable } from '@nestjs/common';

import { BaseService } from '../base.service';
import { SynTopicExtractor, SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';
import { DatabaseService } from '../database.service';

import { FaultStopPipeline, Output } from './fault-stop.pipeline';

@Injectable()
export class FaultStopService extends BaseService<SynBaseInput, Output> {
  readonly name = 'fault_stop';

  constructor(private database: DatabaseService) {
    super(new SynTopicExtractor(), [new FaultStopPipeline()]);
  }

  protected async handleInput(
    input: SynBaseInput,
    pipe: Pipeline<SynBaseInput, Output>,
  ): Promise<boolean> {
    const output = pipe.transform(input);

    if (output) {
      const topic = input.topic;
      await this.addFaultStop(this.topicExtractor.extractDeviceId(topic), output);
      return true;
    }
    return false;
  }

  protected async addFaultStop(deviceId: string, output: Output) {
    // @todo
    this.database.createMeasurePoint(
      { measurement: 'stop_reason', deviceId, timestamp: output.timestamp },
      {
        name: 'id',
        value: output.id,
        type: 'string',
      },
    );
  }
}
