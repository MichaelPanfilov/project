import { SynBaseInput, extractIndex } from '../../helper';
import { Pipeline } from '../pipeline';

export interface Output {
  timestamp: Date;
  value: number;
  index: number;
  unit: string;
}

export class ProductLossDetailPipeline extends Pipeline<SynBaseInput<string>, Output> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return !!input.topic.match(/ProdDefectiveCount\[[1-9]\d*\]\/AccCount/);
  }

  transformData(input: SynBaseInput<string>): Output {
    return {
      timestamp: input.timestamp,
      value: Number(input.value),
      index: Number(extractIndex(input.topic)),
      unit: input.unit,
    };
  }
}
