import { Injectable } from '@nestjs/common';

import { BaseService } from '../base.service';
import { SynTopicExtractor, SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';
import { DatabaseService } from '../database.service';

import { ProductLossDetailPipeline, Output } from './product-loss-detail.pipeline';

@Injectable()
export class ProductLossDetailService extends BaseService<SynBaseInput, Output> {
  readonly name = 'product_loss_detail';

  constructor(private database: DatabaseService) {
    super(new SynTopicExtractor(), [new ProductLossDetailPipeline()]);
  }

  protected async handleInput(
    input: SynBaseInput,
    pipe: Pipeline<SynBaseInput, Output>,
  ): Promise<boolean> {
    const output = pipe.transform(input);

    if (output) {
      const topic = input.topic;
      await this.addProductCount(this.topicExtractor.extractDeviceId(topic), output);
      return true;
    }
    return false;
  }

  protected async addProductCount(deviceId: string, output: Output) {
    this.database.createMeasurePoint(
      { measurement: 'defect_count_detail', deviceId, timestamp: output.timestamp },
      [
        {
          name: 'value',
          value: output.value,
          type: 'int',
        },
        {
          name: 'index',
          value: output.index,
          isIndex: true,
        },
      ],
    );
  }
}
