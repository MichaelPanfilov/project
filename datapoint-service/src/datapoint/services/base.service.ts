import { Logger } from '@nestjs/common';

import { SynTopicExtractor, Topic } from '../helper';

import { Pipeline } from './pipeline';

export abstract class BaseService<TInput extends Topic, TOutput> {
  abstract readonly name: string;

  protected logger = new Logger(BaseService.name);

  constructor(
    protected topicExtractor: SynTopicExtractor,
    private pipes: Pipeline<TInput, TOutput>[],
  ) {}

  getDeviceId(topic: string): string {
    return this.topicExtractor.extractDeviceId(topic);
  }

  protected abstract handleInput(input: TInput, pipe: Pipeline<TInput>): Promise<boolean>;
  async init(): Promise<void> {}

  async processMessage(input: TInput): Promise<boolean> {
    const result = await Promise.all(
      this.pipes.map(pipe =>
        this.handleInput(input, pipe).catch(err => {
          this.logger.error(`Error in ${this.name} handeling input: ${JSON.stringify(input)}`);
          this.logger.error(err);
          return false;
        }),
      ),
    );
    // Message is handled successfully if at least one responses was positive.
    return result.includes(true) ? true : false;
  }
}
