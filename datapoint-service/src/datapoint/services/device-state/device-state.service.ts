import { Injectable, Logger } from '@nestjs/common';
import { SynMachineState } from '@common';

import {
  PackML3StateType,
  SynModeType,
  SynBaseInput,
  OFFLINE_TIMEOUT_SEC,
  SynTopicExtractor,
} from '../../helper';
import { Pipeline } from '../pipeline';
import { BaseService } from '../base.service';
import { DatabaseService } from '../database.service';

import {
  BlockedPipeline,
  ModePipeline,
  Output,
  PackMlPipeline,
  StarvedPipeline,
  WatchdogPipeline,
} from './device-state.pipeline';

interface DeviceProps {
  packMlState: PackML3StateType;
  machineMode: SynModeType;
  machineBlocked: boolean;
  machineStarved: boolean;
  watchdogCount: number;
  init?: { type: SynMachineState; setters: Set<string> };
}

@Injectable()
export class DeviceStateService extends BaseService<SynBaseInput, Output> {
  readonly name = 'device_state';

  protected logger = new Logger(DeviceStateService.name);

  private lastStatesMap = new Map<string, SynMachineState>();
  private devicePropsMaps = new Map<string, DeviceProps>();
  private offlineTimeout = OFFLINE_TIMEOUT_SEC * 1000;
  private offlineState: SynMachineState = 'Offline';
  private timeouts = new Map<string, NodeJS.Timeout>();

  constructor(private database: DatabaseService) {
    super(new SynTopicExtractor(), [
      new PackMlPipeline(),
      new ModePipeline(),
      new StarvedPipeline(),
      new BlockedPipeline(),
      new WatchdogPipeline(),
    ]);
  }

  async init() {}

  protected async handleInput(
    input: SynBaseInput,
    pipe: Pipeline<SynBaseInput, Output>,
  ): Promise<boolean> {
    const output = pipe.transform(input);

    if (output) {
      const topic = input.topic;
      const deviceId = this.topicExtractor.extractDeviceId(topic);
      const state = this.mapStateType(output, deviceId);
      await this.handleStateChange(deviceId, state, output.timestamp);
      return true;
    }
    return false;
  }

  mapStateType(output: Output, deviceId: string): SynMachineState {
    if (!this.devicePropsMaps.has(deviceId)) {
      // Create offline check for new device.
      this.scheduleOfflineCheck(deviceId);
    }
    const props = this.devicePropsMaps.get(deviceId) || this.getDefaultDeviceProps();
    if (props.init) {
      props.init.setters.add(output.type);
    }

    switch (output.type) {
      case 'state':
        props.packMlState = output.payload;
        break;
      case 'mode':
        props.machineMode = output.payload;
        break;
      case 'blocked':
        props.machineBlocked = output.payload;
        break;
      case 'starved':
        props.machineStarved = output.payload;
        break;
      case 'watchdog':
        if (props.watchdogCount !== output.payload) {
          this.scheduleOfflineCheck(deviceId);
        }
        props.watchdogCount = output.payload;
        break;
      default:
        this.logger.log('Unknown message: ' + output);
        break;
    }

    this.devicePropsMaps.set(deviceId, props);
    return this.getStateName(props);
  }

  private getDefaultDeviceProps(): Omit<DeviceProps, 'initStateType'> {
    return {
      packMlState: 'Undefined',
      machineMode: 'Invalid',
      machineBlocked: false,
      machineStarved: false,
      watchdogCount: 0,
    };
  }

  private getStateName(props: DeviceProps): SynMachineState {
    // To prevent the service from changing device states to the one inferred by the default props
    // We wait for all the props to be set once before actually checking.
    if (props.init && props.init.setters.size < 5) {
      return props.init.type;
    }
    // (machineMode<>PRODUCTION)
    if (props.machineMode !== 'Production') {
      return 'Stopped';
    }

    if (props.machineMode === 'Production') {
      // machineMode=PRODUCTION and machineState=(STARTING or EXECUTE or COMPLETING or HOLDING or HELD or UNHOLDING)
      if (
        ['Starting', 'Execute', 'Completing', 'Holding', 'Held', 'UnHolding'].includes(
          props.packMlState,
        )
      ) {
        return 'Running';
      }

      // machineMode=PRODUCTION and machineState=(ABORTING or ABORTED or CLEARING)
      if (['Aborting', 'Aborted', 'Clearing'].includes(props.packMlState)) {
        return 'Faulted';
      }

      // machineMode=PRODUCTION and (machineState=(SUSPENDING or SUSPENDED or UNSUSPENDING) and machineBlocked)
      // machineMode=PRODUCTION and (machineState=(SUSPENDING or SUSPENDED or UNSUSPENDING) and machineStarved)
      if (['Suspending', 'Suspended', 'Unsuspending'].includes(props.packMlState)) {
        if (props.machineBlocked) {
          return 'Blocked';
        }
        if (props.machineStarved) {
          return 'Starved';
        }
      }

      // ((machineMode=PRODUCTION) and (NOT Running and NOT Faulted and NOT Blocked and NOT Starved and NOT offlline))
      if (['Stopped', 'Idle', 'Stopping', 'Resetting', 'Complete'].includes(props.packMlState)) {
        return 'Stopped';
      }
    }

    return 'Unknown';
  }

  private async scheduleOfflineCheck(deviceId: string) {
    const timeout = this.timeouts.get(deviceId);
    if (timeout) {
      clearTimeout(timeout);
    }
    this.timeouts.set(
      deviceId,
      setTimeout(() => {
        this.logger.log(
          'Device ' + deviceId + ' has not responded in time and will be marked as offline.',
        );
        this.handleStateChange(deviceId, this.offlineState, new Date());
      }, this.offlineTimeout),
    );
  }

  private async handleStateChange(
    deviceId: string,
    state: SynMachineState,
    timestamp: Date,
  ): Promise<void> {
    const lastState = this.lastStatesMap.get(deviceId);
    if (lastState && lastState === state) {
      return;
    }

    this.lastStatesMap.set(deviceId, state);
    this.database.createMeasurePoint(
      { measurement: 'state', deviceId, timestamp },
      {
        name: 'value',
        value: state,
        type: 'string',
      },
    );
  }
}
