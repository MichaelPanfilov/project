import { coerceBoolean } from '@common';

import {
  Topic,
  PackML3StateType,
  SynModeType,
  SynBaseInput,
  PACKML3_STATES_MAP,
  SYN_MODES_MAP,
} from '../../helper';
import { Pipeline } from '../pipeline';

interface TypedMessage<TType, TPayload> extends Topic {
  type: TType;
  payload: TPayload;
  timestamp: Date;
}

type StateMessage = TypedMessage<'state', PackML3StateType>;
type ModeMessage = TypedMessage<'mode', SynModeType>;
type BlockedMessage = TypedMessage<'blocked', boolean>;
type StarvedMessage = TypedMessage<'starved', boolean>;
type WatchdogMessage = TypedMessage<'watchdog', number>;

export type Output = StateMessage | ModeMessage | BlockedMessage | StarvedMessage | WatchdogMessage;

export class PackMlPipeline extends Pipeline<SynBaseInput<string>, StateMessage> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return input.topic.endsWith('StateCurrent');
  }

  transformData(input: SynBaseInput<string>): StateMessage {
    return {
      type: 'state',
      payload: PACKML3_STATES_MAP[input.value],
      topic: input.topic,
      timestamp: input.timestamp,
    };
  }
}

export class ModePipeline extends Pipeline<SynBaseInput<string>, ModeMessage> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return input.topic.endsWith('UnitModeCurrent');
  }

  transformData(input: SynBaseInput<string>): ModeMessage {
    return {
      type: 'mode',
      payload: SYN_MODES_MAP[input.value],
      topic: input.topic,
      timestamp: input.timestamp,
    };
  }
}

export class BlockedPipeline extends Pipeline<SynBaseInput<string>, BlockedMessage> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return input.topic.endsWith('Blocked');
  }

  transformData(input: SynBaseInput<string>): BlockedMessage {
    return {
      type: 'blocked',
      payload: coerceBoolean(input.value),
      topic: input.topic,
      timestamp: input.timestamp,
    };
  }
}

export class StarvedPipeline extends Pipeline<SynBaseInput<string>, StarvedMessage> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return input.topic.endsWith('Starved');
  }

  transformData(input: SynBaseInput<string>): StarvedMessage {
    return {
      type: 'starved',
      payload: coerceBoolean(input.value),
      topic: input.topic,
      timestamp: input.timestamp,
    };
  }
}

export class WatchdogPipeline extends Pipeline<SynBaseInput<string>, WatchdogMessage> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return input.topic.endsWith('Watchdog');
  }

  transformData(input: SynBaseInput<string>): WatchdogMessage {
    return {
      type: 'watchdog',
      payload: Number(input.value),
      topic: input.topic,
      timestamp: input.timestamp,
    };
  }
}
