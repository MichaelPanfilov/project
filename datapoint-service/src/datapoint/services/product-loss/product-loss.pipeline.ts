import { SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';

export interface Output {
  timestamp: Date;
  value: number;
  unit: string;
}

export class ProductLossPipeline extends Pipeline<SynBaseInput<string>, Output> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return !!input.topic.match(/ProdDefectiveCount\[0\]\/AccCount/);
  }

  transformData(input: SynBaseInput<string>): Output {
    return {
      timestamp: input.timestamp,
      value: Number(input.value),
      unit: input.unit,
    };
  }
}
