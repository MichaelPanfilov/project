import { Injectable, Inject } from '@nestjs/common';
import { InfluxDB as InfluxClient, WriteApi, QueryApi, Point } from '@influxdata/influxdb-client';
import { coerceBoolean } from '@common';

import { Measurement } from '../helper';

export const INFLUX_OPTIONS_TOKEN = 'InfluxOptionsToken';

export interface InfluxOptions {
  url: string;
  username?: string;
  password?: string;
  database: string;
  retentionPolicy: string;
}

export interface CreatePointOptions {
  measurement: Measurement;
  deviceId: string;
  timestamp: Date;
  timeOffset?: string;
}

@Injectable({ scope: 0 })
export class DatabaseService {
  private readonly influxClient: InfluxClient;
  private readonly writeApi: WriteApi;
  private readonly readApi: QueryApi;

  constructor(@Inject(INFLUX_OPTIONS_TOKEN) options: InfluxOptions) {
    this.influxClient = new InfluxClient({
      url: options.url,
      token:
        options.username && options.password
          ? `${options.username}:${options.password}`
          : undefined,
    });
    const bucket = `${options.database}/${options.retentionPolicy}`;
    this.writeApi = this.influxClient.getWriteApi('', bucket);
    this.readApi = this.influxClient.getQueryApi('');
  }

  createMeasurePoint(opts: CreatePointOptions, columns: Column[] | Column): void {
    let point = new Point(opts.measurement)
      .tag('deviceId', opts.deviceId)
      .timestamp(opts.timestamp);

    if (opts.timeOffset) {
      point = point.stringField('timeOffset', opts.timeOffset);
    }
    if (!Array.isArray(columns)) {
      columns = [columns];
    }

    columns.forEach(column => {
      if (isTagColumn(column)) {
        point = point.tag(column.name, String(column.value));
      } else {
        const { name, value, type } = column;
        switch (type) {
          case 'int':
            point = point.intField(name, Math.trunc(Number(value)));
            break;
          case 'float':
            point = point.floatField(name, Number(value));
            break;
          case 'string':
            point = point.stringField(name, String(value));
            break;
          case 'boolean':
            point = point.booleanField(name, coerceBoolean(value));
            break;
        }
      }
    });

    this.writePoint(point);
  }

  writePoint(point: Point): void {
    this.writeApi.writePoint(point);
  }

  writePoints(points: Point[]): void {
    this.writeApi.writePoints(points);
  }

  async readPoints<T>(query: string): Promise<T[]> {
    const result = [] as T[];
    return new Promise((resolve, reject) =>
      this.readApi.queryRows(query, {
        next: (row, tableMeta) => result.push(tableMeta.toObject(row) as T),
        error: reject,
        complete: () => resolve(result),
      }),
    );
  }
}

interface ColumnBase {
  name: string;
  value: string | number | boolean;
}

interface TagColumn extends ColumnBase {
  isIndex: true;
}

interface FieldColumn extends ColumnBase {
  type: 'int' | 'float' | 'string' | 'boolean';
}

function isTagColumn(column: Column): column is TagColumn {
  return 'isIndex' in column && column.isIndex === true;
}

export type Column = TagColumn | FieldColumn;
