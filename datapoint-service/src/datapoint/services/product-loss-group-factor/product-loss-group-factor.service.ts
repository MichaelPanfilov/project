import { Injectable } from '@nestjs/common';

import { BaseService } from '../base.service';
import { SynTopicExtractor, SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';
import { DatabaseService } from '../database.service';

import { ProductLossGroupFactorPipeline, Output } from './product-loss-group-factor.pipeline';

@Injectable()
export class ProductLossGroupFactorService extends BaseService<SynBaseInput, Output> {
  readonly name = 'product_loss_group_factor';

  constructor(private database: DatabaseService) {
    super(new SynTopicExtractor(), [new ProductLossGroupFactorPipeline()]);
  }

  protected async handleInput(
    input: SynBaseInput,
    pipe: Pipeline<SynBaseInput, Output>,
  ): Promise<boolean> {
    const output = pipe.transform(input);

    if (output) {
      const topic = input.topic;
      await this.addProductCount(this.topicExtractor.extractDeviceId(topic), output);
      return true;
    }
    return false;
  }

  protected async addProductCount(deviceId: string, output: Output) {
    this.database.createMeasurePoint(
      { measurement: 'helper__defect_group_factor', deviceId, timestamp: output.timestamp },
      [
        {
          name: 'factor',
          value: output.factor,
          type: 'int',
        },
        {
          name: 'group',
          value: output.group,
          isIndex: true,
        },
      ],
    );
  }
}
