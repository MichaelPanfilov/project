import { SynBaseInput, extractIndex } from '../../helper';
import { Pipeline } from '../pipeline';

export interface Output {
  timestamp: Date;
  factor: number;
  group: number;
  unit: string;
}

export class ProductLossGroupFactorPipeline extends Pipeline<SynBaseInput<string>, Output> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return !!input.topic.match(/Parameter\[([8-9]|10)\]\/Value/);
  }

  transformData(input: SynBaseInput<string>): Output {
    return {
      timestamp: input.timestamp,
      factor: Number(input.value),
      // Convert the index to one of the reject groups 1, 2 or 3.
      group: Number(extractIndex(input.topic)) - 7,
      unit: input.unit,
    };
  }
}
