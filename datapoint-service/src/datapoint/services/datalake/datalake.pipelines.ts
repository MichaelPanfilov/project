import { Pipeline } from '../pipeline';
import { DataLakePoint, SynBaseInput } from '../../helper';

export class DataLakePipe extends Pipeline<SynBaseInput<string>, DataLakePoint> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return true;
  }

  transformData(input: SynBaseInput<string>): DataLakePoint {
    return input;
  }
}
