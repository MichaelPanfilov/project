import { Injectable } from '@nestjs/common';

import { BaseService } from '../base.service';
import { SynTopicExtractor } from '../../helper/topic-extractor';
import { Pipeline } from '../pipeline';
import { DataLakePoint, SynBaseInput } from '../../helper';
import { DatabaseService } from '../database.service';

import { DataLakePipe } from './datalake.pipelines';

@Injectable()
export class DataLakeService extends BaseService<SynBaseInput, DataLakePoint> {
  readonly name = 'datalake';

  constructor(private database: DatabaseService) {
    super(new SynTopicExtractor(), [new DataLakePipe()]);
  }

  protected async handleInput(
    input: SynBaseInput,
    pipe: Pipeline<SynBaseInput, DataLakePoint>,
  ): Promise<boolean> {
    const output = pipe.transform(input);

    if (output) {
      const topic = input.topic;
      await this.addDataPoint(
        this.topicExtractor.extractDeviceId(topic),
        this.topicExtractor.extractSensorId(topic),
        output,
      );
      return true;
    }
    return false;
  }

  protected async addDataPoint(deviceId: string, sensorId: string, output: DataLakePoint) {
    const { dataType, unit, value, timestamp, timeOffset } = output;
    this.database.createMeasurePoint(
      { measurement: 'data_lake', deviceId, timestamp, timeOffset },
      [
        { name: 'sensorId', isIndex: true, value: sensorId },
        { name: 'dataType', type: 'string', value: dataType },
        { name: 'unit', type: 'string', value: unit },
        { name: 'value', type: 'string', value },
      ],
    );
  }
}
