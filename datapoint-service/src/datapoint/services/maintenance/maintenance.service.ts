import { Injectable } from '@nestjs/common';

import { BaseService } from '../base.service';
import { SynTopicExtractor, SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';
import { DatabaseService } from '../database.service';

import { MaintenancePipeline, Output } from './maintenance.pipeline';

@Injectable()
export class MaintenanceService extends BaseService<SynBaseInput, Output> {
  readonly name = 'maintenance';

  constructor(private database: DatabaseService) {
    super(new SynTopicExtractor(), [new MaintenancePipeline()]);
  }

  protected async handleInput(
    input: SynBaseInput,
    pipe: Pipeline<SynBaseInput, Output>,
  ): Promise<boolean> {
    const output = pipe.transform(input);

    if (output) {
      const topic = input.topic;
      await this.addMaintenanceCount(this.topicExtractor.extractDeviceId(topic), output);
      return true;
    }
    return false;
  }

  protected async addMaintenanceCount(deviceId: string, output: Output) {
    this.database.createMeasurePoint(
      { measurement: 'accumulated_maintenance_count', deviceId, timestamp: output.timestamp },
      [
        {
          name: 'value',
          value: output.value,
          type: 'int',
        },
        {
          name: 'index',
          value: output.index,
          isIndex: true,
        },
      ],
    );
  }
}
