import { Injectable } from '@nestjs/common';

import { BaseService } from '../base.service';
import { SynTopicExtractor, SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';
import { DatabaseService } from '../database.service';

import { ProductInputOutputFactorPipeline, Output } from './product-input-output-factor.pipeline';

@Injectable()
export class ProductInputOutputFactorService extends BaseService<SynBaseInput, Output> {
  readonly name = 'product_input_output_factor';

  constructor(private database: DatabaseService) {
    super(new SynTopicExtractor(), [new ProductInputOutputFactorPipeline()]);
  }

  protected async handleInput(
    input: SynBaseInput,
    pipe: Pipeline<SynBaseInput, Output>,
  ): Promise<boolean> {
    const output = pipe.transform(input);

    if (output) {
      const topic = input.topic;
      await this.addProductCount(this.topicExtractor.extractDeviceId(topic), output);
      return true;
    }
    return false;
  }

  protected async addProductCount(deviceId: string, output: Output) {
    this.database.createMeasurePoint(
      { measurement: 'helper__product_input_output_factor', deviceId, timestamp: output.timestamp },
      {
        name: 'value',
        value: output.factor,
        type: 'int',
      },
    );
  }
}
