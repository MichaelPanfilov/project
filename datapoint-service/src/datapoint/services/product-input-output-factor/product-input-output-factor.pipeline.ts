import { SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';

export interface Output {
  timestamp: Date;
  factor: number;
  unit: string;
}

export class ProductInputOutputFactorPipeline extends Pipeline<SynBaseInput<string>, Output> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return !!input.topic.match(/Parameter\[11\]\/Value/);
  }

  transformData(input: SynBaseInput<string>): Output {
    return {
      timestamp: input.timestamp,
      factor: Number(input.value),
      unit: input.unit,
    };
  }
}
