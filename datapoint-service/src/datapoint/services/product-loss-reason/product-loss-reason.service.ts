import { Injectable } from '@nestjs/common';

import { BaseService } from '../base.service';
import { SynTopicExtractor, SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';
import { DatabaseService } from '../database.service';

import { ProductLossReasonPipeline, Output } from './product-loss-reason.pipeline';

@Injectable()
export class ProductLossReasonService extends BaseService<SynBaseInput, Output> {
  readonly name = 'product_loss_reason';

  constructor(private database: DatabaseService) {
    super(new SynTopicExtractor(), [new ProductLossReasonPipeline()]);
  }

  protected async handleInput(
    input: SynBaseInput,
    pipe: Pipeline<SynBaseInput, Output>,
  ): Promise<boolean> {
    const output = pipe.transform(input);

    if (output) {
      const topic = input.topic;
      await this.addProductCount(this.topicExtractor.extractDeviceId(topic), output);
      return true;
    }
    return false;
  }

  protected async addProductCount(deviceId: string, output: Output) {
    this.database.createMeasurePoint(
      { measurement: 'defect_reason', deviceId, timestamp: output.timestamp },
      [
        {
          name: 'id',
          value: output.id,
          type: 'int',
        },
        {
          name: 'index',
          value: output.index,
          isIndex: true,
        },
      ],
    );
  }
}
