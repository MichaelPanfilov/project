import { SynBaseInput, extractIndex } from '../../helper';
import { Pipeline } from '../pipeline';

export interface Output {
  timestamp: Date;
  id: number;
  index: number;
  unit: string;
}

export class ProductLossReasonPipeline extends Pipeline<SynBaseInput<string>, Output> {
  canTransform(input: SynBaseInput<string>): input is SynBaseInput<string> {
    return !!input.topic.match(/ProdDefectiveCount\[[1-9]\d*\]\/ID/) && Number(input.value) > 0;
  }

  transformData(input: SynBaseInput<string>): Output {
    return {
      timestamp: input.timestamp,
      id: Number(input.value),
      index: Number(extractIndex(input.topic)),
      unit: input.unit,
    };
  }
}
