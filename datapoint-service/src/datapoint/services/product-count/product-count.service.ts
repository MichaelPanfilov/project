import { Injectable } from '@nestjs/common';

import { BaseService } from '../base.service';
import { SynTopicExtractor, SynBaseInput } from '../../helper';
import { Pipeline } from '../pipeline';
import { DatabaseService } from '../database.service';

import { ProductCountPipeline, Output } from './product-count.pipeline';

@Injectable()
export class ProductCountService extends BaseService<SynBaseInput, Output> {
  readonly name = 'product_processed';

  constructor(private database: DatabaseService) {
    super(new SynTopicExtractor(), [new ProductCountPipeline()]);
  }

  protected async handleInput(
    input: SynBaseInput,
    pipe: Pipeline<SynBaseInput, Output>,
  ): Promise<boolean> {
    const output = pipe.transform(input);

    if (output) {
      const topic = input.topic;
      await this.addProductCount(this.topicExtractor.extractDeviceId(topic), output);
      return true;
    }
    return false;
  }

  protected async addProductCount(deviceId: string, output: Output) {
    this.database.createMeasurePoint(
      { measurement: 'accumulated_product_count', deviceId, timestamp: output.timestamp },
      {
        name: 'value',
        value: output.value,
        type: 'int',
      },
    );
  }
}
