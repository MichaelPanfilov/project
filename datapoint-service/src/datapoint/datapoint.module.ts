import { Module } from '@nestjs/common';
import { coerceBoolean } from '@common';

import { MappingModule } from '../mapping/mapping.module';

import { QueryController } from './query.controller';
import { QueryService } from './query.service';
import { SynConnector } from './connectors/iot-hub/connector';
import {
  DataLakeService,
  DeviceStateService,
  FaultStopService,
  ProductCountService,
  ProductLossService,
  ProductLossDetailService,
  ProductLossGroupFactorService,
  ProductLossReasonService,
  DatabaseService,
  INFLUX_OPTIONS_TOKEN,
  MaintenanceService,
} from './services';

@Module({
  imports: [MappingModule],
  controllers: [QueryController],
  providers: [
    QueryService,
    SynConnector,
    DataLakeService,
    DeviceStateService,
    FaultStopService,
    ProductCountService,
    ProductLossService,
    ProductLossDetailService,
    ProductLossGroupFactorService,
    ProductLossReasonService,
    MaintenanceService,
    DatabaseService,
    {
      provide: INFLUX_OPTIONS_TOKEN,
      useValue: {
        url: process.env.APP_INFLUX_URL || `http://${process.env.APP_INFLUX_HOST_DATAPOINT}:8086`,
        database: process.env.APP_INFLUX_NAME as string,
        // TODO: Move to env variable
        retentionPolicy: 'one_day',
        username: process.env.APP_INFLUX_USER,
        password: process.env.APP_INFLUX_PASS,
      },
    },
  ],
})
export class DatapointModule {
  constructor(
    dataLakeService: DataLakeService,
    stateService: DeviceStateService,
    faultStopService: FaultStopService,
    productCountService: ProductCountService,
    productLossService: ProductLossService,
    productLossDetailService: ProductLossDetailService,
    productLossGroupFactorService: ProductLossGroupFactorService,
    productLossReasonService: ProductLossReasonService,
    maintenanceService: MaintenanceService,
    connector: SynConnector,
  ) {
    if (!coerceBoolean(process.env.IOT_HUB_LISTENER_DISABLED)) {
      connector.addService(dataLakeService);
      connector.addService(stateService);
      connector.addService(faultStopService);
      connector.addService(productCountService);
      connector.addService(productLossService);
      connector.addService(productLossDetailService);
      connector.addService(productLossGroupFactorService);
      connector.addService(productLossReasonService);
      connector.addService(maintenanceService);
      connector.start();
    }
  }
}
