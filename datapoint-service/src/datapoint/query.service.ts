import { Injectable } from '@nestjs/common';
import {
  SynMachineState,
  FaultStopDto,
  ProductLossDto,
  SYN_STATES,
  MappingType,
  TimeRange,
} from '@common';
import { Mapping } from 'src/mapping/entities/mapping.entity';

import { MappingService } from '../mapping/mapping.service';

import { DatabaseService } from './services';

export interface StateResponse {
  from: Date;
  to: Date;
  status: SynMachineState;
}

interface ReasonIndexMap {
  [index: number]: { id: number; start: Date }[];
}

@Injectable()
export class QueryService {
  constructor(private database: DatabaseService, private mappingService: MappingService) {
    /*
    [1000, 1001, 1002, 1003, 1004, 2000, 2001, 2002, 2003, 3000].forEach((id, i) => {
      this.database.createMeasurePoint('defect_reason', 'TTM-Simulation-1', new Date(), [
        {
          name: 'id',
          value: id,
          type: 'int',
        },
        {
          name: 'index',
          value: i + 1,
          isIndex: true,
        },
      ]);
    });
    */
  }

  async getDevices(): Promise<string[]> {
    const query = this.buildDevicesQuery();
    const points = await this.database.readPoints<{ deviceId: string }>(query);
    return points.map(p => p.deviceId);
  }

  async getTimeline(deviceId: string, from: Date, to: Date): Promise<StateResponse[]> {
    const query = this.buildStateChangeQuery(deviceId, from, to);
    const points = await this.database.readPoints<{ state: SynMachineState; start: string }>(query);

    if (!points.length) {
      return [{ from, to, status: await this.getCurrentStatus(deviceId) }];
    }
    // Convert state changes to an timeline of states.
    const res: StateResponse[] = [];
    points.forEach((point, index, arr) => {
      const next = arr[index + 1];
      const lastAdded = res[res.length - 1];
      if (index < 1) {
        res.push({
          from,
          to: next ? new Date(next.start) : to,
          status: point.state,
        });
        return;
      }

      if (next && next.state !== point.state) {
        res.push({
          from: new Date(point.start),
          to: new Date(next.start),
          status: point.state,
        });
      } else if (!next) {
        res.push({
          from: new Date(point.start),
          to,
          status: point.state,
        });
      } else {
        lastAdded.to = next ? new Date(point.start) : to;
      }
    });

    return res;
  }

  async getDurationOfState(
    deviceId: string,
    state: SynMachineState,
    from: Date,
    to: Date,
  ): Promise<number> {
    const points = await this.database.readPoints<{ state_duration: number }>(
      this.buildStateDurationQuery(deviceId, state, from, to),
    );
    return points[0].state_duration / 1000;
  }

  async getPartOutputGoods(deviceId: string, from: Date, to: Date): Promise<number> {
    const factorsChanges = await this.getInputOutputFactors(deviceId, from, to);
    if (!factorsChanges.length) {
      // We assume factor 1 if none is in the database yet.
      factorsChanges.push({
        factor: 1,
        start: from,
      });
    }

    const counts = await Promise.all(
      factorsChanges.map(async (changes, index, arr) => {
        const next = arr[index + 1];
        // The first factor might be before the queried time, we restrict it to nor less than from
        // to not query more points than requested.
        const start = changes.start < from ? from : changes.start;
        const [goods, defects] = await Promise.all([
          this.getPartOutputCount(deviceId, start, next ? next.start : to),
          this.getDefectCount(deviceId, start, next ? next.start : to),
        ]);
        if (defects > goods || goods < 1) {
          return 0;
        }
        return (goods - defects) / changes.factor;
      }),
    );

    return counts.reduce((prev, curr) => prev + curr, 0);
  }

  async getDefectCount(deviceId: string, from: Date, to: Date): Promise<number> {
    const query = this.buildCountQuery(deviceId, 'accumulated_defect_count', from, to);
    const [diff] = await this.database.readPoints<{ _value: number }>(query);
    return diff ? Number(diff._value) : 0;
  }

  async getCurrentStatus(deviceId: string): Promise<SynMachineState> {
    const [last] = await this.database.readPoints<{ _value: SynMachineState }>(
      this.buildLastStateQuery(deviceId),
    );

    return last ? last._value : 'Unknown';
  }

  async getFaultStops(
    deviceId: string,
    from: Date,
    to: Date,
    lang: string,
  ): Promise<FaultStopDto[]> {
    const timeline = await this.getTimeline(deviceId, from, to);
    const faulted = timeline.filter(item => item.status === 'Faulted');
    const reasons = await this.findFaultStopMessages(deviceId, from, to, lang);

    return faulted.reduce((prev, curr) => {
      // Sometimes stop messages arrive before the state changes so we add some margin here.
      // Also there can be multiple reasons within a faulted time span so we need to split it.
      const reasonsWithin = reasons.filter(reason =>
        this.isWithinMargin(reason.timestamp, curr, 1000),
      );

      // We missed the message but lets at least return the time span.
      if (!reasonsWithin.length) {
        return prev.concat({
          from: curr.from,
          to: curr.to,
          reason: 'Unknown Reason',
        });
      }

      const faultStopsWithin = reasonsWithin.map((reason, i, arr) => {
        const next = arr[i + 1];
        // Ensure we do not exceed boundaries of query.
        const start = new Date(Math.max(reason.timestamp.getTime(), curr.from.getTime()));
        const end = new Date(Math.min((next ? next.timestamp : curr.to).getTime(), to.getTime()));
        return { from: start, to: end, reason: reason.text };
      });

      return prev.concat(faultStopsWithin);
    }, [] as FaultStopDto[]);
  }

  async getFaultStopCounts(
    deviceId: string,
    from: Date,
    to: Date,
    lang: string,
  ): Promise<{ label: string; count: number }[]> {
    const messages = await this.findFaultStopMessages(deviceId, from, to, lang);
    const result: { [key: string]: number } = {};
    messages.forEach(msg => {
      const current = result[msg.text] || 0;
      result[msg.text] = current + 1;
    });
    return Object.keys(result).map(label => ({ label, count: result[label] }));
  }

  async getProductLosses(
    deviceId: string,
    from: Date,
    to: Date,
    lang: string,
  ): Promise<ProductLossDto[]> {
    const query = this.buildDefectCountDetailQuery(deviceId, from, to);
    const losses = await this.database.readPoints<{
      index: string;
      _value: string;
      _time: string;
    }>(query);

    const reasonMap = await this.getLossReasons(deviceId, to);
    const factors = await this.findDefectGroupFactors(deviceId, from, to);

    const lossesMap = losses.reduce((prev, curr, i, arr) => {
      const next = arr[i + 1];
      const nextCount = Number(next ? next._value : curr._value);
      const index = Number(curr.index);
      const existing = prev[index];
      const timestamp = new Date(curr._time);
      const saveReasonArray = reasonMap[index] || [];

      const reason =
        saveReasonArray.find(r => r.start < timestamp) ||
        saveReasonArray[saveReasonArray.length - 1];
      const factor = reason
        ? factors.find(f => Math.floor(reason.id / 1000) === f.group && f.start < timestamp)
        : undefined;

      const item = {
        count: Math.max(nextCount - Number(curr._value), 0),
        reasonId: reason ? reason.id : undefined,
        factor: factor ? factor.factor : 1,
        timestamp,
      };

      if (!existing) {
        return { ...prev, [curr.index]: [item] };
      }
      existing.push(item);
      return prev;
    }, {} as { [index: number]: { count: number; reasonId?: number; factor: number; timestamp: Date }[] });

    // We dont fail the request if the map is missing
    const mappings = await this.mappingService
      .findMappings(MappingType.REJECT_REASONS, deviceId)
      .catch(() => [] as Mapping[]);

    // TODO: Add factors.
    return Object.values(lossesMap).reduce(
      (prev, curr) =>
        prev.concat(
          curr.map(c => {
            const map = mappings.find(m => m.createdAt < c.timestamp) || mappings[0];
            const entry = c.reasonId && map ? map.data[c.reasonId] : undefined;
            return {
              reason: entry ? entry[lang] : `Unknown Reason (${c.reasonId})`,
              time: c.timestamp,
            };
          }),
        ),
      [] as ProductLossDto[],
    );
  }

  async getPowerOnHours(deviceId: string): Promise<number> {
    const query = this.buildMaintenanceCountQuery(deviceId, 0);
    const [diff] = await this.database.readPoints<{ _value: number }>(query);
    return diff ? Number(diff._value) : 0;
  }

  async getProducingHours(deviceId: string): Promise<number> {
    const query = this.buildMaintenanceCountQuery(deviceId, 1);
    const [diff] = await this.database.readPoints<{ _value: number }>(query);
    return diff ? Number(diff._value) : 0;
  }

  async getCycles(deviceId: string): Promise<number> {
    const query = this.buildMaintenanceCountQuery(deviceId, 2);
    const [diff] = await this.database.readPoints<{ _value: number }>(query);
    return diff ? Number(diff._value) : 0;
  }

  private async getPartOutputCount(deviceId: string, from: Date, to: Date): Promise<number> {
    const query = this.buildCountQuery(deviceId, 'accumulated_product_count', from, to);
    const [diff] = await this.database.readPoints<{ _value: number }>(query);
    return diff ? Number(diff._value) : 0;
  }

  private async getInputOutputFactors(
    deviceId: string,
    from: Date,
    to: Date,
  ): Promise<{ factor: number; start: Date }[]> {
    const query = this.buildInputOutputFactorQuery(deviceId, from, to);
    const queryBefore = this.buildInputOutputFactorBeforeQuery(deviceId, from);

    const points = await this.database.readPoints<{ _value: string; _time: string }>(query);
    // This must be of length 1 or 0.
    const pointsBefore = await this.database.readPoints<{ _value: string; _time: string }>(
      queryBefore,
    );

    return [...pointsBefore, ...points].map(p => ({
      factor: Number(p._value),
      start: new Date(p._time),
    }));
  }

  private async findFaultStopMessages(
    deviceId: string,
    from: Date,
    to: Date,
    lang: string,
  ): Promise<{ text: string; timestamp: Date }[]> {
    const query = this.buildStopReasonIdQuery(deviceId, from, to);
    const points = await this.database.readPoints<{ _value: string; _time: string }>(query);

    // We dont fail the request if the map is missing
    const mappings = await this.mappingService
      .findMappings(MappingType.STOP_REASONS, deviceId)
      .catch(() => [] as Mapping[]);

    return points.map(({ _value, _time }) => {
      const time = new Date(_time);
      const map = mappings.find(m => m.createdAt < time) || mappings[0];
      const entry = map && map.data[Number(_value)];
      return {
        text: (entry && entry[lang]) || `Unknown Reason (${_value})`,
        timestamp: time,
      };
    });
  }

  private async findDefectGroupFactors(
    deviceId: string,
    from: Date,
    to: Date,
  ): Promise<{ group: number; factor: number; start: Date }[]> {
    const query = this.buildDefectGroupFactorQuery(deviceId, from, to);
    const points = await this.database.readPoints<{ _value: string; group: string; _time: string }>(
      query,
    );
    return points.map(p => ({
      group: Number(p.group),
      factor: Number(p._value),
      start: new Date(p._time),
    }));
  }

  private async getLossReasons(deviceId: string, to: Date): Promise<ReasonIndexMap> {
    const reasonIdQuery = this.buildDefectReasonQuery(deviceId, to);

    // These Ids get assigned only once at the startup of an edge device.
    // We query them to get the the id (which we map later to a human readable text) and
    // the index property which maps to the amount of products lost due to that reason.
    const reasons = await this.database.readPoints<{
      index: string;
      _value: string;
      _time: string;
    }>(reasonIdQuery);

    // TODO: Remove this after influx retention policy is configured
    if (!reasons.length) {
      [1000, 1001, 1002, 1003, 1004, 2000, 2001, 2002, 2003, 3000].forEach((id, i) => {
        reasons.push({
          index: String(i + 1),
          _time: new Date(0).toISOString(),
          _value: String(id),
        });
      });
    }

    // This map holds an array of changes of the reason id for one index.
    // When looking for a product loss reason, one has to simply use the index as key for
    // the map and then look up the last id change for given timestamp.
    const map = reasons.reduce((prev, curr) => {
      const index = Number(curr.index);
      const existing = prev[index];
      const item = { id: Number(curr._value), start: new Date(curr._time) };

      if (!existing) {
        return { ...prev, [curr.index]: [item] };
      } else {
        return { ...prev, [curr.index]: [...existing, item] };
      }
    }, {} as ReasonIndexMap);

    Object.values(map).forEach((r: { id: number; start: Date }[]) =>
      r.sort((a, b) => a.start.getTime() - b.start.getTime()),
    );

    return map;
  }

  private isWithinMargin(timestamp: Date, range: TimeRange, marginMs: number): boolean {
    const fromMs = range.from.getTime();
    const toMs = range.to.getTime();
    // Make sure the margin is not to big for the span.
    if (marginMs * 2 >= toMs - fromMs) {
      marginMs = 0;
    }
    return timestamp.getTime() + marginMs >= fromMs && timestamp.getTime() - marginMs <= toMs;
  }

  private buildDevicesQuery(): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: 0)
      |> filter(fn: (r) => r._measurement == "state")
      |> group(columns: ["deviceId"], mode: "by")
      |> last()
    `;
  }

  private buildStateChangeQuery(deviceId: string, from: Date, to: Date): string {
    return `
    import "strings"

    states = "${[...SYN_STATES].join(',')}"

    points = from(bucket: "app/one_day")
        |> range(start: ${from.toISOString()}, stop: ${to.toISOString()})
        |> filter(fn: (r) =>
          (r._measurement == "state" and r.deviceId == "${deviceId}"))


    before = from(bucket: "app/one_day")
        |> range(start: 0, stop: ${from.toISOString()})
        |> filter(fn: (r) =>
          (r._measurement == "state" and r.deviceId == "${deviceId}"))
        |> last()

    union(tables: [before, points])
        |> map(fn: (r) =>
          ({r with state_id: strings.index(v: states, substr: r._value)}))
        |> sort(columns: ["_time"], desc: false)
        |> difference(columns: ["state_id"], keepFirst: true)
        |> fill(column: "state_id", value: 1)
        |> filter(fn: (r) =>
          (r.state_id != 0))
        |> map(fn: (r) =>
          ({state: r._value, start: r._time}))
    `;
  }

  private buildStateDurationQuery(
    deviceId: string,
    state: SynMachineState,
    from: Date,
    to: Date,
  ): string {
    return ` 
   from(bucket: "app/one_day")
      |> range(start: ${from.toISOString()}, stop: ${to.toISOString()})
      |> filter(fn: (r) => (r._measurement == "state" and r.deviceId == "${deviceId}"))
      |> sort(columns: ["_time"], desc: false)
      |> stateDuration(
        fn: (r) => r._value == "${state}",
        column: "state_duration",
        unit: 1s
      )
      |> sum(column: "state_duration")
    `;
  }

  private buildCountQuery(
    deviceId: string,
    count: 'accumulated_defect_count' | 'accumulated_product_count',
    from: Date,
    to: Date,
  ): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: ${from.toISOString()}, stop: ${to.toISOString()})
      |> filter(fn: (r) => (r._measurement == "${count}" and r.deviceId == "${deviceId}"))
      |> sort(columns: ["_time"], desc: false)
      |> spread()
    `;
  }

  private buildLastStateQuery(deviceId: string): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: 0)
      |> filter(fn: (r) => (r._measurement == "state" and r.deviceId == "${deviceId}"))
      |> last()
    `;
  }

  private buildInputOutputFactorQuery(deviceId: string, from: Date, to: Date): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: ${from.toISOString()}, stop: ${to.toISOString()})
      |> filter(fn: (r) => (r._measurement == "helper__product_input_output_factor" and r.deviceId == "${deviceId}"))
    `;
  }

  private buildInputOutputFactorBeforeQuery(deviceId: string, before: Date): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: 0, stop: ${before.toISOString()})
      |> filter(fn: (r) => (r._measurement == "helper__product_input_output_factor" and r.deviceId == "${deviceId}"))
      |> last()
    `;
  }

  private buildStopReasonIdQuery(deviceId: string, from: Date, to: Date): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: ${from.toISOString()}, stop: ${to.toISOString()})
      |> filter(fn: (r) => (r._measurement == "stop_reason" and r.deviceId == "${deviceId}"))
      |> sort(columns: ["_time"], desc: false)
    `;
  }

  private buildDefectCountDetailQuery(deviceId: string, from: Date, to: Date): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: ${from.toISOString()}, stop: ${to.toISOString()})
      |> filter(fn: (r) => (r._measurement == "defect_count_detail" and r.deviceId == "${deviceId}"))
    `;
  }

  private buildDefectGroupFactorQuery(deviceId: string, from: Date, to: Date): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: ${from.toISOString()}, stop: ${to.toISOString()})
      |> filter(fn: (r) => (r._measurement == "helper__defect_group_factor"  and r.deviceId == "${deviceId}"))
      |> group(columns: ["group"], mode: "by")
    `;
  }

  private buildDefectReasonQuery(deviceId: string, to: Date): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: 0, stop: ${to.toISOString()})
      |> filter(fn: (r) => (r._measurement == "defect_reason"  and r.deviceId == "${deviceId}"))
    `;
  }

  private buildMaintenanceCountQuery(deviceId: string, index: number): string {
    return `
    from(bucket: "app/one_day")
      |> range(start: 0)
      |> filter(fn: (r) => (r._measurement == "accumulated_maintenance_count" and r.deviceId == "${deviceId}" and r.index == "${index}"))
      |> last()
    `;
  }
}
