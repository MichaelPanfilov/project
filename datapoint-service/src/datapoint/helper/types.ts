export interface Topic {
  topic: string;
}

export interface Insert {
  table: Measurement;
}

export interface Payload<T = unknown> {
  value: T;
  unit: string;
  dataType: string;
  sourceTimestamp: string;
}

export interface MeasurementBase {
  timestamp: Date;
  timeOffset: string;
}

export interface SynBaseInput<T = unknown>
  extends Omit<Payload<T>, 'sourceTimestamp'>,
    Topic,
    MeasurementBase {}

export interface DataLakePoint extends MeasurementBase {
  value: string;
  unit: string;
  dataType: string;
}

export const PACKML3_STATES = [
  'Undefined',
  'Clearing',
  'Stopped',
  'Starting',
  'Idle',
  'Suspended',
  'Execute',
  'Stopping',
  'Aborting',
  'Aborted',
  'Holding',
  'Held',
  'UnHolding',
  'Suspending',
  'Unsuspending',
  'Resetting',
  'Completing',
  'Complete',
] as const;

export type PackML3StateType = typeof PACKML3_STATES[number];

export const PACKML3_STATES_MAP: { [key: string]: PackML3StateType } = {
  '0': 'Undefined',
  '1': 'Clearing',
  '2': 'Stopped',
  '3': 'Starting',
  '4': 'Idle',
  '5': 'Suspended',
  '6': 'Execute',
  '7': 'Stopping',
  '8': 'Aborting',
  '9': 'Aborted',
  '10': 'Holding',
  '11': 'Held',
  '12': 'UnHolding',
  '13': 'Suspending',
  '14': 'Unsuspending',
  '15': 'Resetting',
  '16': 'Completing',
  '17': 'Complete',
};

export const SYN_MODES = ['Invalid', 'Production', 'Maintenance', 'Manual'] as const;

export type SynModeType = typeof SYN_MODES[number];

export const SYN_MODES_MAP: { [key: string]: SynModeType } = {
  '0': 'Invalid',
  '1': 'Production',
  '2': 'Maintenance',
  '3': 'Manual',
};

export const OFFLINE_TIMEOUT_SEC = 10;

export type Measurement =
  | 'data_lake'
  | 'state'
  | 'accumulated_product_count'
  | 'accumulated_defect_count'
  | 'defect_count_detail'
  | 'defect_reason'
  | 'stop_reason'
  | 'helper__product_input_output_factor'
  | 'helper__defect_group_factor'
  | 'accumulated_maintenance_count';
