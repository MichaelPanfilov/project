export class SynTopicExtractor {
  extractDeviceId(topic: string) {
    return topic.split('/')[2];
  }

  extractSensorId(topic: string) {
    return topic
      .split('/')
      .splice(3)
      .join('-');
  }
}

export function extractIndex(name: string): string {
  const matches = name.match(/\[[0-9]\d*\]/);
  if (matches && matches.length) {
    return matches[matches.length - 1].replace('[', '').replace(']', '');
  }
  throw new Error('Unable to extract index from string');
}
