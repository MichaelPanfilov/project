import { EventHubClient, EventPosition, MessagingError } from '@azure/event-hubs';
import { Injectable } from '@nestjs/common';
import * as moment from 'moment';

import { SynBaseInput, Payload } from '../../helper';
import { Connector } from '../connector';

interface Characteristic {
  name: string;
  payload: Payload;
  transmitTimestamp: string;
}

interface IoTMessage {
  body: { characteristics: Characteristic[] };
}

const CONNECTION_TIMEOUT = 1000 * 10;
const WATCHDOG_TIMEOUT = 1000 * 60;

@Injectable()
export class SynConnector extends Connector<SynBaseInput> {
  private client?: EventHubClient;
  private timeout?: NodeJS.Timeout;

  protected async init(): Promise<void> {
    // In case we cant connect to the hub, the lib fails silently so we need to manually add a timeout.
    this.client = await Promise.race<EventHubClient>([
      EventHubClient.createFromIotHubConnectionString(
        process.env.IOT_HUB_CONNECTION_STRING as string,
      ),
      new Promise<EventHubClient>((_, reject) =>
        setTimeout(() => reject(new Error('IOT Hub connection timeout')), CONNECTION_TIMEOUT),
      ),
    ]);
  }

  async listen(): Promise<void> {
    if (!this.client) {
      return;
    }

    const partitionIds = await this.client.getPartitionIds();
    for (const id of partitionIds) {
      this.logger.log(`Subscribing to partition ${id}`);
      this.client.receive(
        id,
        msg => this.handleMessage(msg),
        err => this.handleError(err),
        { eventPosition: EventPosition.fromEnqueuedTime(Date.now()) },
      );
    }
  }

  async unlisten(): Promise<void> {
    if (!this.client) {
      return;
    }

    try {
      await this.client.close();
    } catch (e) {
      this.logger.warn('Error while closing IOT Hub client');
      this.logError(e, 'warn');
    }
    this.client = undefined;
  }

  private async handleMessage({ body }: IoTMessage): Promise<void> {
    this.scheduleOfflineCheck();

    const characteristics = body.characteristics;
    for (const characteristic of characteristics) {
      const topic = characteristic.name;

      // Prevent faulty topics from polluting the devices extracted.
      if (topic.split('/').length < 6) {
        return;
      }

      const { sourceTimestamp } = characteristic.payload;
      const timeOffset = moment.parseZone(sourceTimestamp).format('Z');

      this.processMessage({
        ...characteristic.payload,
        topic,
        timestamp: new Date(sourceTimestamp),
        timeOffset,
      });
    }
  }

  private async handleError(err: Error | MessagingError): Promise<void> {
    this.logError(err);
    if (err instanceof MessagingError && !err.retryable) {
      if (this.timeout) {
        clearTimeout(this.timeout);
      }
      this.tryReconnect();
    }
  }

  private async scheduleOfflineCheck() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(async () => {
      this.logger.warn(`IOT Hub has not sent a message within the last ${WATCHDOG_TIMEOUT}ms.`);
      await this.tryReconnect();
    }, WATCHDOG_TIMEOUT);
  }

  private async tryReconnect() {
    this.logger.log(`Disconnecting IOT Hub.`);
    // This function should not throw because we need it to set client to undefined.
    await this.stop();

    try {
      this.logger.log(`Reconnecting IOT Hub.`);
      await this.init();

      this.logger.log(`Listening to IOT Hub.`);
      await this.start();
    } catch (e) {
      this.logger.error('Fatal Error while reconnecting IOT Hub.');
      this.logError(e);

      this.logger.log(`Trying reconnect in ${WATCHDOG_TIMEOUT}ms.`);
      await new Promise(res => setTimeout(res, WATCHDOG_TIMEOUT));
      this.tryReconnect();
    }
  }

  private logError(err: unknown, type: 'error' | 'warn' = 'error') {
    this.logger[type](err);
    if (err instanceof Error) {
      this.logger[type](err.name, err.message, err.stack);
    }
  }
}
