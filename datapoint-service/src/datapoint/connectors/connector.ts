import { Logger } from '@nestjs/common';

import { BaseService } from '../services/base.service';
import { Topic } from '../helper/types';

export abstract class Connector<T extends Topic = Topic> {
  private listening = false;
  private services = new Set<BaseService<T, unknown>>();
  protected logger = new Logger(Connector.name);

  protected abstract async init(): Promise<void>;
  protected abstract async listen(): Promise<void>;
  protected abstract async unlisten(): Promise<void>;

  addService(service: BaseService<T, unknown>): void {
    this.services.add(service);
  }

  getServices(): BaseService<T, unknown>[] {
    return Array.from(this.services);
  }

  async start(): Promise<void> {
    if (!this.listening) {
      await this.init();
      await Promise.all(this.getServices().map(service => service.init()));
      await this.listen();
      this.listening = true;
    }
  }

  async stop(): Promise<void> {
    if (this.listening) {
      await this.unlisten();
      this.listening = false;
    }
  }

  protected async processMessage(data: T): Promise<void> {
    const result = await Promise.all(
      this.getServices().map(service => service.processMessage(data)),
    );
    if (!result.includes(true)) {
      // this.logger.log(`Could not handle message: ${JSON.stringify(data)}`);
    }
  }
}
