import { FaultStopDto, ProductLossDto, SynMachineState, DEFAULT_LANG, TimeRange } from '@common';
import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOkResponse } from '@nestjs/swagger';

import { QueryService } from './query.service';

// 1 hour
const DEFAULT_SPAN = 60 * 60 * 1000;

@Controller('/v1/datapoints/performance-queries')
@ApiTags('Performance Queries')
@ApiBearerAuth()
export class QueryController {
  constructor(private queryService: QueryService) {}

  @Get('/devices')
  @ApiOkResponse({ type: String, isArray: true })
  async getDevices(): Promise<string[]> {
    return this.queryService.getDevices();
  }

  @Get('/part-output-count/:deviceId')
  @ApiOkResponse({ type: Number })
  async getPartOutputCount(
    @Param('deviceId') deviceId: string,
    @Query('from') from?: string,
    @Query('to') to?: string,
  ): Promise<number> {
    const dates = this.parseDates(from, to);
    return this.queryService.getPartOutputGoods(deviceId, dates.from, dates.to);
  }

  @Get('/part-defect-count/:deviceId')
  @ApiOkResponse({ type: Number })
  async getDefectCount(
    @Param('deviceId') deviceId: string,
    @Query('from') from?: string,
    @Query('to') to?: string,
  ): Promise<number> {
    const dates = this.parseDates(from, to);
    return this.queryService.getDefectCount(deviceId, dates.from, dates.to);
  }

  @Get('/status/:deviceId')
  @ApiOkResponse({
    status: 200,
    type: String,
    description: 'Get an array of jams with start & end dates',
  })
  async getStatus(@Param('deviceId') deviceId: string): Promise<SynMachineState> {
    return this.queryService.getCurrentStatus(deviceId);
  }

  @Get('/status-history/:deviceId')
  @ApiOkResponse({
    status: 200,
    isArray: true,
    description: 'Get an array of jams with start & end dates',
  })
  async getStatusHistory(
    @Param('deviceId') deviceId: string,
    @Query('from') from?: string,
    @Query('to') to?: string,
  ) {
    const dates = this.parseDates(from, to);
    return this.queryService.getTimeline(deviceId, dates.from, dates.to);
  }

  @Get('/fault-stops/:deviceId')
  @ApiOkResponse({
    status: 200,
    isArray: true,
    description: 'Get an array of jams with start & end dates',
  })
  async getFaultStops(
    @Param('deviceId') deviceId: string,
    @Query('from') from?: string,
    @Query('to') to?: string,
    @Query('lang') lang = DEFAULT_LANG,
  ): Promise<FaultStopDto[]> {
    const dates = this.parseDates(from, to);
    return this.queryService.getFaultStops(deviceId, dates.from, dates.to, lang);
  }

  @Get('/fault-stops/:deviceId/counts')
  async getFaultStopCounts(
    @Param('deviceId') deviceId: string,
    @Query('from') from?: string,
    @Query('to') to?: string,
    @Query('lang') lang = DEFAULT_LANG,
  ): Promise<{ label: string; count: number }[]> {
    const dates = this.parseDates(from, to);
    return this.queryService.getFaultStopCounts(deviceId, dates.from, dates.to, lang);
  }

  @Get('/product-losses/:deviceId')
  @ApiOkResponse({
    status: 200,
    isArray: true,
    description: 'Get an array of jams with start & end dates',
  })
  async getProductLosses(
    @Param('deviceId') deviceId: string,
    @Query('from') from?: string,
    @Query('to') to?: string,
    @Query('lang') lang = DEFAULT_LANG,
  ): Promise<ProductLossDto[]> {
    const dates = this.parseDates(from, to);
    return this.queryService.getProductLosses(deviceId, dates.from, dates.to, lang);
  }

  @Get('/power-on-time/:deviceId')
  @ApiOkResponse({
    status: 200,
  })
  async getPowerOnTime(@Param('deviceId') deviceId: string): Promise<number> {
    return this.queryService.getPowerOnHours(deviceId);
  }

  @Get('/producing-time/:deviceId')
  @ApiOkResponse({
    status: 200,
  })
  async getProducingTime(@Param('deviceId') deviceId: string): Promise<number> {
    return this.queryService.getProducingHours(deviceId);
  }

  @Get('/cycles/:deviceId')
  @ApiOkResponse({
    status: 200,
  })
  async getCycles(@Param('deviceId') deviceId: string): Promise<number> {
    return this.queryService.getCycles(deviceId);
  }

  private parseDates(from?: string, to?: string): TimeRange {
    const now = new Date();
    const toDate = to ? new Date(to) : now;
    const toFinal = toDate > now ? now : toDate;
    const fromFinal = from ? new Date(from) : new Date(toFinal.getTime() - DEFAULT_SPAN);
    return {
      from: fromFinal,
      to: toFinal,
    };
  }
}
