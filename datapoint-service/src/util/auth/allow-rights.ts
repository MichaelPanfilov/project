import { Acl } from '@common';
import { CustomDecorator, SetMetadata } from '@nestjs/common';

type RightDecorator = (acl: Acl[]) => CustomDecorator;
export const RIGHTS_KEY = 'requiredRights';
export const AllowRights: RightDecorator = (acl: Acl[]) => SetMetadata(RIGHTS_KEY, acl);
