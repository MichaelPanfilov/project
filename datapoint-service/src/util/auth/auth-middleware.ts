import * as fs from 'fs';
import * as path from 'path';

import { JWTPayload, SESSION_COOKIE_NAME } from '@common';
import { UnauthorizedException } from '@nestjs/common';
import { INestApplication } from '@nestjs/common/interfaces';
import * as cookieParser from 'cookie-parser';
import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';

// TODO: Get this from some config service.
const certPath = process.env.NODE_ENV === 'production' ? 'production' : 'dev';
const publicKey = fs.readFileSync(path.join(process.cwd(), `/data/certs/${certPath}/key.pub`));

const PUBLIC_ROUTES = ['/docs/', '/v1/licenses'];

function verifyToken(token: string): boolean {
  try {
    return !!jwt.verify(token, publicKey, { algorithms: ['RS512'] });
  } catch {
    return false;
  }
}

function decodeToken(token: string): JWTPayload {
  return jwt.decode(token, { json: true }) as JWTPayload;
}

function AuthorizationMiddleware(req: Request, res: Response, next: NextFunction) {
  if (PUBLIC_ROUTES.some(route => req.url.startsWith(route))) {
    next();
    return;
  }
  // Somehow the headers are not written by nest in this middleware so we have to do it.
  res.header('Access-Control-Allow-Origin', '*');
  if (req.method.toLowerCase() === 'options') {
    next();
    return;
  }
  const authRequest = req.headers['authorization'] || req.cookies[SESSION_COOKIE_NAME];

  if (typeof authRequest !== 'string') {
    next(new UnauthorizedException('No authorization token provided'));
    return;
  }

  const token = authRequest.toLowerCase().startsWith('bearer ')
    ? authRequest.substring(7)
    : authRequest;

  if (!verifyToken(token)) {
    next(new UnauthorizedException('Token invalid'));
    return;
  }

  // @ts-ignore
  req.auth = decodeToken(token);
  next();
}

export function authMiddleware(app: INestApplication): void {
  app.use(cookieParser());
  app.use(AuthorizationMiddleware);
}
