import { Acl, hasAclResource, matchesAcl } from '@common';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { Observable } from 'rxjs';

import { RIGHTS_KEY } from './allow-rights';

abstract class AclGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  abstract hasRight(reqRights: string[], routeAcls: Acl[]): boolean;

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const routeAcls = this.reflector.get<Acl[] | undefined>(RIGHTS_KEY, context.getHandler());

    if (!routeAcls) {
      return true;
    }

    const req = context.switchToHttp().getRequest() as Request;
    // @ts-ignore
    const reqRights = req.auth.rights;
    if (Array.isArray(reqRights)) {
      if (this.hasRight(reqRights, routeAcls)) {
        return true;
      }
    }

    return false;
  }
}

@Injectable()
export class ResourceGuard extends AclGuard {
  constructor(reflector: Reflector) {
    super(reflector);
  }

  hasRight(reqRights: string[], routeAcls: Acl[]): boolean {
    return hasAclResource(reqRights, routeAcls);
  }
}

@Injectable()
export class RightGuard extends AclGuard {
  constructor(reflector: Reflector) {
    super(reflector);
  }

  hasRight(reqRights: string[], routeAcls: Acl[]): boolean {
    return matchesAcl(reqRights, routeAcls);
  }
}
