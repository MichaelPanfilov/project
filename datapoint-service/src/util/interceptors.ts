import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataResponse, asResponse } from '@common';

@Injectable()
export class ResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<DataResponse<unknown>> {
    const now = Date.now();
    return next.handle().pipe(map(res => asResponse(res, { responseTime: Date.now() - now })));
  }
}
