# datapoint-service

## db schema & migrations
- `npm run migration:run` - create or update the db schema

## init influxdb
- `npm run shell`
- outside of the shell: `docker exec -it <influx docker container> bash`
- `influx -username app -password app`
- copy&paste init.iql


