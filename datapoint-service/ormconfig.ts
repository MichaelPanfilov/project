import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { ConnectionOptions } from 'typeorm';
import * as path from 'path';

const config: ConnectionOptions = {
  type: 'mysql',
  host: String(process.env.APP_DB_HOST_DATAPOINT || process.env.APP_DB_HOST),
  port: Number(process.env.APP_DB_PORT) || 3306,
  username: String(process.env.APP_DB_USER),
  password: String(process.env.APP_DB_PASS),
  database: String(process.env.APP_DB_NAME),
  ssl: process.env.NODE_ENV !== 'development',
  extra: { insecureAuth: true },
  synchronize: false,
  namingStrategy: new SnakeNamingStrategy(),
  entities: [path.join(__dirname, 'src/**/*.entity.{ts,js}')],
  migrations: [path.join(__dirname, 'migrations/*{.js,.ts}')],
  maxQueryExecutionTime: 5000,
};

export = config;
