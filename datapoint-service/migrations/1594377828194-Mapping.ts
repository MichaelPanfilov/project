import {MigrationInterface, QueryRunner} from "typeorm";

export class Mapping1594377828194 implements MigrationInterface {
    name = 'Mapping1594377828194'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `mapping` (`id` varchar(36) NOT NULL, `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `device` varchar(255) NOT NULL, `type` enum ('reject_reasons', 'stop_reasons') NOT NULL, `data` json NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `mapping`", undefined);
    }

}
